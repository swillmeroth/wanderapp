package regionale.whs.wanderapp.tools.converter;

import android.content.Context;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Map;

import regionale.whs.wanderapp.R;
import regionale.whs.wanderapp.objects.enums.POIType;

/**
 * Since the android enum object doesn't allow values to be stored with the keys, I had to implement this converter class.
 * It holds a map of key/value-pairs that associate a POIType with the corresponding R.string id.
 * By giving a context to the constructor, this association is also context-aware and by that localized.
 */
public class POITypeStringConverter
{
    private ArrayList<Map.Entry<POIType, String>> convertMap;

    /**
     * Standard constructor which holds the key/value pairs.
     *
     * @param context Context needed for getting the right String-resource
     */
    public POITypeStringConverter(Context context)
    {
        convertMap = new ArrayList<>();

        // this is the place to add new type/string combinations!
        convertMap.add(new AbstractMap.SimpleEntry<>(POIType.ABBEY, context.getString(R.string.abbey)));    // abbey
        convertMap.add(new AbstractMap.SimpleEntry<>(POIType.CHAPEL, context.getString(R.string.chapel)));    // chapel
        convertMap.add(new AbstractMap.SimpleEntry<>(POIType.CHURCH, context.getString(R.string.church)));    // church
        convertMap.add(new AbstractMap.SimpleEntry<>(POIType.CROSS, context.getString(R.string.cross)));    // cross
        convertMap.add(new AbstractMap.SimpleEntry<>(POIType.GRAVEYARD, context.getString(R.string.graveyard)));    // graveyard
        convertMap.add(new AbstractMap.SimpleEntry<>(POIType.JEWISH_GRAVEYARD, context.getString(R.string.jewish_graveyard)));    // jewish graveyard
        convertMap.add(new AbstractMap.SimpleEntry<>(POIType.MONUMENT, context.getString(R.string.monument)));    // monument
        convertMap.add(new AbstractMap.SimpleEntry<>(POIType.STATUE, context.getString(R.string.statue)));    // statue

    }

    /**
     * Converts the enum value to a localized string
     *
     * @param type enum value
     * @return string representation of enum value
     */
    public String convertPOITypeToString(POIType type)
    {
        for (int i = 0; i < convertMap.size(); i++)
        {
            Map.Entry entry = convertMap.get(i);
            if (entry.getKey().equals(type))
                return (String) entry.getValue();
        }

        return null;
    }

    /**
     * Converts a string to POIType
     *
     * @param typeString localized string representation of POIType value
     * @return POIType if found, else null
     */
    public POIType convertStringToPOIType(String typeString)
    {
        for (int i = 0; i < convertMap.size(); i++)
        {
            Map.Entry entry = convertMap.get(i);
            if (entry.getValue().equals(typeString))
                return (POIType) entry.getKey();
        }
        return null;
    }
}
