package regionale.whs.wanderapp.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import regionale.whs.wanderapp.R;
import regionale.whs.wanderapp.adapter.CommentsAdapter;
import regionale.whs.wanderapp.adapter.POIDetailTabsPagerAdapter;
import regionale.whs.wanderapp.fragments.CommentsFragment;
import regionale.whs.wanderapp.objects.POI;
import regionale.whs.wanderapp.service.WanderWSClient;
import regionale.whs.wanderapp.tools.AppSettings;
import regionale.whs.wanderapp.wspojos.WanderKommentar;

/**
 * Activity that shows all details of a POI. One tab for main data and one tab for comments.
 */
public class POIDetailActivity extends StandardActivity implements ActionBar.TabListener, View.OnClickListener, CommentsFragment.OnCommentSelectedListener, StandardActivity.ProposeChangeListener {

    private POI mPOI;
    private List<WanderKommentar> mComments;
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_poidetail);

        mPOI = this.getIntent().getParcelableExtra("poi");
        getComments();

        setProposeChangeListener(this);
        showChangeProposalItem(true);

        final ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        CommentsAdapter commentsAdapter = new CommentsAdapter(this, mComments);

        POIDetailTabsPagerAdapter pagerAdapter = new POIDetailTabsPagerAdapter(getSupportFragmentManager(), mPOI, commentsAdapter, this);

        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(pagerAdapter);

        mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener()
        {
            @Override
            public void onPageSelected(int position)
            {
                actionBar.setSelectedNavigationItem(position);
            }
        });

        actionBar.addTab(actionBar.newTab().setText(R.string.title_routedetail_main).setTabListener(this));
        actionBar.addTab(actionBar.newTab().setText(R.string.comments).setTabListener(this));

        //showData();
    }

    /**
     * Gets the comments for the activities poi.
     */
    private void getComments()
    {
        // TODO: add a button to view all comments or just show every comment?

        int count = mPOI.getCommentCount();
        int max = (count < AppSettings.MAX_COMMENTS) ? count : AppSettings.MAX_COMMENTS;

        WanderWSClient client = WanderWSClient.getInstance();
        mComments = client.getPOIKommentare(mPOI.getId(), 1, max + 1, this, true);

        if(mComments == null) {
            mComments = new ArrayList<>();
        }
    }

    /**
     * If the "get directions" button is clicked, a geo-URI with the POIs location is created and an action intent is called.
     * By this, all installed applications that have registered themselves for geo-Uris (e.g. Google Maps) can be opened to get directions to the POI.
     * @param view clicked button
     */
    public void onDirectionsButton(View view)
    {
        String uri;

        if (mPOI.hasAddress())
        {
            // TODO: geo with address
            // temp until real implementation
            uri = String.format(Locale.ENGLISH, "geo:0,0?q=%f,%f(" + mPOI.getName() + ")", mPOI.getLatitude(), mPOI.getLongitude());
        } else
        {
            uri = String.format(Locale.ENGLISH, "geo:0,0?q=%f,%f(" + mPOI.getName() + ")", mPOI.getLatitude(), mPOI.getLongitude());
        }

        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        this.startActivity(intent);
    }

    public POI getPOI() {
        return mPOI;
    }

    @Override
    public void onClick(View v)
    {
        // only used for comments, so safe to use like this
        TextView view_name = (TextView) v.findViewById(R.id.name);
        String name = view_name.getText().toString();

        Toast.makeText(this, name, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCommentSelected(WanderKommentar comment)
    {
        // what should happen here?
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, android.support.v4.app.FragmentTransaction fragmentTransaction)
    {
        mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, android.support.v4.app.FragmentTransaction fragmentTransaction)
    {

    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, android.support.v4.app.FragmentTransaction fragmentTransaction)
    {

    }

    /**
     * calls the POIProposalActivity to propose changes to the POI
     */
    @Override
    public void onChangeProposal() {

        Intent intent = new Intent(this, POIProposalActivity.class);
        intent.putExtra("isNew", false);
        intent.putExtra("poi", mPOI);
        startActivity(intent);
    }
}
