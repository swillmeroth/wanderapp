package regionale.whs.wanderapp.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

import regionale.whs.wanderapp.R;
import regionale.whs.wanderapp.objects.POI;
import regionale.whs.wanderapp.objects.Route;

/**
 * Fragment that shows the list of results of a poi or route search
 */
public class ResultListFragment extends Fragment implements AbsListView.OnItemClickListener
{

    private OnPOISelectedListener mPOIListener;
    private OnRouteSelectedListener mRouteListener;
    private boolean isRouteSearch;

    /**
     * The fragment's ListView/GridView.
     */
    private AbsListView mListView;

    /**
     * The Adapter which will be used to populate the ListView/GridView with
     * Views.
     */
    private ListAdapter mAdapter;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ResultListFragment()
    {
    }

    public static ResultListFragment newInstance(ArrayAdapter adapter, boolean isRouteSearch)
    {
        ResultListFragment fragment = new ResultListFragment();
        fragment.setListAdapter(adapter);
        fragment.setRouteSearch(isRouteSearch);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_resultlist_list, container, false);

        // Set the adapter
        mListView = (AbsListView) view.findViewById(android.R.id.list);
        ((AdapterView<ListAdapter>) mListView).setAdapter(mAdapter);

        // Set OnItemClickListener so we can be notified on item clicks
        mListView.setOnItemClickListener(this);

        return view;
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        try
        {
            mPOIListener = (OnPOISelectedListener) activity;
            mRouteListener = (OnRouteSelectedListener) activity;
        } catch (ClassCastException e)
        {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        mPOIListener = null;
        mRouteListener = null;
    }

    // handling clicks split by poi and route
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        if (null != mPOIListener && null != mRouteListener)
        {
            // Notify the activity that an item has been selected.

            if (isRouteSearch)
            {
                Route route;
                route = (Route) mAdapter.getItem(position);
                mRouteListener.onRouteSelected(route);
            } else
            {
                POI poi;
                poi = (POI) mAdapter.getItem(position);
                mPOIListener.onPOISelected(poi);
            }
        }
    }

    /**
     * The default content for this Fragment has a TextView that is shown when
     * the list is empty. If you would like to change the text, call this method
     * to supply the text it should use.
     */
    public void setEmptyText(CharSequence emptyText)
    {
        View emptyView = mListView.getEmptyView();

        if (emptyText instanceof TextView)
        {
            ((TextView) emptyView).setText(emptyText);
        }
    }

    public void setListAdapter(ArrayAdapter<?> adapter)
    {
        mAdapter = adapter;
    }

    public boolean isRouteSearch()
    {
        return isRouteSearch;
    }

    public void setRouteSearch(boolean isRouteSearch)
    {
        this.isRouteSearch = isRouteSearch;
    }

    // Interfaces for handling item selection in ResultActivity
    public interface OnPOISelectedListener
    {
        void onPOISelected(POI poi);
    }

    public interface OnRouteSelectedListener
    {
        void onRouteSelected(Route route);
    }
}
