package regionale.whs.wanderapp.activities;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;

import java.util.Locale;

import regionale.whs.wanderapp.R;
import regionale.whs.wanderapp.service.WanderWSClient;
import regionale.whs.wanderapp.tools.AppSettings;

/**
 * abstract activity which is super class of most activities. Handles the locale as well was the options menu.
 */
public abstract class StandardActivity extends ActionBarActivity {

    private Menu mMenu;
    private MapDataListener mMapDataListener;
    private ProposeChangeListener mChangeListener;
    private ProposeNewListener mNewListener;
    private boolean mShowOffline = false;
    private boolean mShowNewProp = false;
    private boolean mShowChangeProp = false;

    public void setMapDataListener(MapDataListener mMapDataListener) {
        this.mMapDataListener = mMapDataListener;
    }

    public void setProposeChangeListener(ProposeChangeListener listener) {
        this.mChangeListener = listener;
    }

    public void setProposeNewListener(ProposeNewListener listener) {
        this.mNewListener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        checkLocale();
        super.onCreate(savedInstanceState);
    }

    @Override
    public final boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        mMenu = menu;
        mMenu.findItem(R.id.action_gooffline).setVisible(mShowOffline);
        mMenu.findItem(R.id.action_proposal_new).setVisible(mShowNewProp);
        mMenu.findItem(R.id.action_proposal_changes).setVisible(mShowChangeProp);

        checkLogin();
        // hide menu items that should only be shown in special activities here

        return true;
    }

    /**
     * if a user is logged in, the user's language setting is used to determine the locale of the activity.
     */
    private void checkLocale()
    {
        if(WanderWSClient.getInstance().getCurrentUser() == null)
            return;

        Configuration cfg = new Configuration();

        String lang = WanderWSClient.getInstance().getCurrentUser().getLanguage();

        if (!TextUtils.isEmpty(lang)) {
            if (lang.equals(AppSettings.WANDER_LANGUAGE_ENGLISH))
                cfg.locale = Locale.ENGLISH;
            else if (lang.equals(AppSettings.WANDER_LANGUAGE_GERMAN))
                cfg.locale = Locale.GERMAN;
            else
                cfg.locale = Locale.getDefault();
        } else
            cfg.locale = Locale.getDefault();

        Locale.setDefault(cfg.locale);
        getBaseContext().getResources().updateConfiguration(cfg, getBaseContext().getResources().getDisplayMetrics());
    }

    /**
     * checks if a user is logged in. if he is, the profile menu item is shown, otherwise the login item.
     */
    private void checkLogin() {

        if(mMenu == null)
            return;

        // check if logged in
        WanderWSClient client = WanderWSClient.getInstance();

        if(client.getCurrentUser() != null) {
            mMenu.findItem(R.id.action_login).setVisible(false);
            mMenu.findItem(R.id.action_profile).setVisible(true);
        } else {
            mMenu.findItem(R.id.action_login).setVisible(true);
            mMenu.findItem(R.id.action_profile).setVisible(false);
        }
    }

    // add methods for showing special menu items here
    public void showProfileItem(boolean showItem) { mMenu.findItem(R.id.action_profile).setVisible(showItem); }
    public void showOfflineItem(boolean showItem) { this.mShowOffline = showItem; }
    public void showNewProposalItem(boolean showItem) { this.mShowNewProp = showItem; }
    public void showChangeProposalItem(boolean showItem) { this.mShowChangeProp = showItem; }

    @Override
    public final boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_login) {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            return true;
        }
        else if (id == R.id.action_profile) {
            Intent intent = new Intent(this, ProfileActivity.class);
            startActivity(intent);
            return true;
        }
        else if (id == R.id.action_gooffline) {
            WanderWSClient.getInstance().setIsOffline(true);

            if(mMapDataListener != null)
                mMapDataListener.goOffline();

            mMenu.findItem(R.id.action_goonline).setVisible(true);
            mMenu.findItem(R.id.action_gooffline).setVisible(false);
        }
        else if (id == R.id.action_goonline) {
            WanderWSClient.getInstance().setIsOffline(false);

            if(mMapDataListener != null)
                mMapDataListener.goOnline();

            mMenu.findItem(R.id.action_goonline).setVisible(false);
            mMenu.findItem(R.id.action_gooffline).setVisible(true);
        }
        else if (id == R.id.action_proposal_changes) {
            if(mChangeListener != null)
                mChangeListener.onChangeProposal();
        }
        else if (id == R.id.action_proposal_new) {
            if(mNewListener != null)
                mNewListener.onNewProposal();
        }
        // add handling for new menu items here

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        checkLocale();
        checkLogin();
        super.onResume();
    }

    public interface MapDataListener {
        void goOnline();
        void goOffline();
    }

    public interface ProposeNewListener {
        void onNewProposal();
    }

    public interface ProposeChangeListener {
        void onChangeProposal();
    }
}
