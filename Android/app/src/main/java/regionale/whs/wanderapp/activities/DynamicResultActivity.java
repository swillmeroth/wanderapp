package regionale.whs.wanderapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;

import java.util.List;

import regionale.whs.wanderapp.R;
import regionale.whs.wanderapp.fragments.MapFragment;
import regionale.whs.wanderapp.objects.POI;

/**
 * Activity that shows the user a WanderRoute that was generated server-side after requesting a dynamic route using DynamicRouteActivity.
 * Consists mostly of a MapFragments with two buttons at the bottom. One buttons brings the user back to be able to change his route settings.
 * The other one starts the navigation activity.
 */
public class DynamicResultActivity extends StandardActivity implements MapFragment.OnPOIInfoClickListener
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dynamic_result);

        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        if(fragments.size() > 0 )
        {
            MapFragment fragment = (MapFragment) fragments.get(0);
            fragment.setOnPOIInfoClickListener(this); // make POIs clickable
        }
    }

    /**
     * Method that is called for getting back to DynamicRouteActivity by pressing the back-button.
     * Sets the settings to intent and calls DynamicRouteActivity.
     * @param view
     */
    public void goBack(View view)
    {
        Intent intent = new Intent(this, DynamicRouteActivity.class);
        intent.putExtra("settings", this.getIntent().getParcelableExtra("settings"));
        startActivity(intent);
    }

    /**
     * Adds the route to intent and starts the NavigationActivity.
     * @param view
     */
    public void startNavigation(View view)
    {
        Intent intent = new Intent(this, NavigationActivity.class);
        intent.putExtra("route", this.getIntent().getParcelableExtra("route"));
        startActivity(intent);
    }

    /**
     * Starts a POIDetailActivity for the clicked POI.
     * @param poi The clicked poi.
     */
    @Override
    public void onPOIInfoClick(POI poi) {
        Intent intent = new Intent(this, POIDetailActivity.class);
        intent.putExtra("poi", poi);
        startActivity(intent);
    }
}
