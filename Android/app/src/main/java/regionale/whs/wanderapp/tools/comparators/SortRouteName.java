package regionale.whs.wanderapp.tools.comparators;

import java.util.Comparator;

import regionale.whs.wanderapp.objects.Route;

/**
 * sorts routes by name
 *
 * Created by Stephan on 31.10.2014.
 */
public class SortRouteName implements Comparator<Route>
{

    @Override
    public int compare(Route route1, Route route2)
    {
        return route1.getName().compareToIgnoreCase(route2.getName());
    }
}
