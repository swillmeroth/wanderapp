package regionale.whs.wanderapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.View;
import android.widget.Button;

import regionale.whs.wanderapp.R;
import regionale.whs.wanderapp.objects.enums.MapType;

/**
 * Simple selection activity for choosing the type of search to perform.
 */
public class POIRouteSearchActivity extends StandardActivity
{

    boolean isRouteSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_poiroutesearch);

        // when coming back via back button, this doesn't work (always false)
        if (this.getIntent().getExtras() != null)
            isRouteSearch = this.getIntent().getExtras().getBoolean("isRouteSearch");

        if (isRouteSearch)
        {
            this.setTitle(R.string.title_routesearch);
        }

        // TODO: implement advanced search
        Button advanced_button = (Button) findViewById(R.id.button_detail);
        advanced_button.setVisibility(View.GONE);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
    }

    /**
     * when NameSearch is clicked, start new Activity
     * @param view clicked button
     */
    public void onNameSearchClick(View view)
    {
        Intent intent = new Intent(this, NameSearchActivity.class);
        intent.putExtra("isRouteSearch", isRouteSearch);
        startActivity(intent);
    }

    /**
     * when TypeSearch is clicked, start new Activity
     *
     * @param view clicked button
     */
    public void onTypeSearchClick(View view)
    {
        Intent intent = new Intent(this, TypeSearchActivity.class);
        intent.putExtra("isRouteSearch", isRouteSearch);
        startActivity(intent);
    }

    /**
     * when MapSearch is clicked, start MapActivity
     *
     * @param view clicked button
     */
    public void onMapSearchClick(View view)
    {
        Intent intent = new Intent(this, MapActivity.class);
        intent.putExtra("isRouteSearch", isRouteSearch);

        if (isRouteSearch)
        {
            //ArrayList<Route> routeResults = DummyResults.getDummyRoutes(this);
            intent.putExtra("maptype", MapType.ROUTERESULTS.toString());
            //intent.putParcelableArrayListExtra("routeResults", routeResults);
        } else
        {
            //ArrayList<POI> poiResults = DummyResults.getDummyPOIs();
            intent.putExtra("maptype", MapType.POIRESULTS.toString());
            //intent.putParcelableArrayListExtra("poiResults", poiResults);
        }

        startActivity(intent);
    }
}
