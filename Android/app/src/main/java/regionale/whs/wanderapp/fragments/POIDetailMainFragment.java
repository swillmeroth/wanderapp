package regionale.whs.wanderapp.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import regionale.whs.wanderapp.R;
import regionale.whs.wanderapp.objects.POI;
import regionale.whs.wanderapp.objects.enums.POIType;
import regionale.whs.wanderapp.tools.converter.POITypeStringConverter;

/**
 * fragment that shows the data of a poi
 */
public class POIDetailMainFragment extends Fragment
{
    private POI mPOI;

    public POIDetailMainFragment()
    {
        // Required empty public constructor
    }

    public static POIDetailMainFragment newInstance(POI poi)
    {
        POIDetailMainFragment fragment = new POIDetailMainFragment();
        fragment.setPOI(poi);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_poidetail_main, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        showData();
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
    }

    public POI getPOI()
    {
        return mPOI;
    }

    public void setPOI(POI poi)
    {
        this.mPOI = poi;
    }

    /**
     * populates the layout's data fields
     */
    private void showData()
    {
        TextView textView;

        // image
        ImageView imageView = (ImageView) this.getActivity().findViewById(R.id.image);
        int id = mPOI.getDrawableId();
        if (id != 0)
        {
            imageView.setImageResource(mPOI.getDrawableId());
        } else
        {
            imageView.setVisibility(View.GONE);
        }

        // title and name
        String name = mPOI.getName();
        if (!name.equals(""))
        {
            this.getActivity().setTitle(name);
            textView = (TextView) this.getActivity().findViewById(R.id.name);
            textView.setText(name);
        }

        // rating
        RatingBar ratingBar = (RatingBar) this.getActivity().findViewById(R.id.ratingBar);
        double rating = mPOI.getRating();
        if (rating > 0)
        {
            ratingBar.setNumStars((int) rating);
            textView = (TextView) this.getActivity().findViewById(R.id.text_rating);
            textView.setText(POI.getRatingString(mPOI, this.getActivity()));
        } else
        {
            LinearLayout ratingLayout = (LinearLayout) this.getActivity().findViewById(R.id.layout_rating);
            ratingLayout.setVisibility(View.GONE);
        }

        // type
        textView = (TextView) this.getActivity().findViewById(R.id.type);
        POIType type = mPOI.getType();
        if (type != null)
        {
            POITypeStringConverter converter = new POITypeStringConverter(this.getActivity());
            textView.setText(converter.convertPOITypeToString(type));

        } else
            textView.setVisibility(View.GONE);

        // description
        textView = (TextView) this.getActivity().findViewById(R.id.description);
        String description = mPOI.getDescription();
        if (!description.equals(""))
        {
            textView.setText(description);
        } else
        {
            textView.setVisibility(View.GONE);
        }

        // opening hours
        textView = (TextView) this.getActivity().findViewById(R.id.text_opening);
        String openingTimes = mPOI.getOpeningTimes();
        if (!openingTimes.equals(""))
        {
            textView.setText(openingTimes);
        } else
        {
            textView.setVisibility(View.GONE);
            TextView title = (TextView) this.getActivity().findViewById(R.id.title_opening);
            title.setVisibility(View.GONE);
        }

        // address
        textView = (TextView) this.getActivity().findViewById(R.id.text_address);
        String address = mPOI.getAddress();
        if (!address.equals("\n "))
        {
            textView.setText(address);
        } else
        {
            textView.setVisibility(View.GONE);
            TextView title = (TextView) this.getActivity().findViewById(R.id.title_address);
            title.setVisibility(View.GONE);
        }

        // accessibility info
        textView = (TextView) this.getActivity().findViewById(R.id.text_accessibility);
        String accessibility_info = mPOI.getAccessibilityInfo();
        if (!accessibility_info.equals(""))
        {
            textView.setText(accessibility_info);
        } else
        {
            textView.setVisibility(View.GONE);
            TextView title = (TextView) this.getActivity().findViewById(R.id.title_accessibility);
            title.setVisibility(View.GONE);
        }

        // TODO: setup social buttons
        // for now, they are just deleted
        LinearLayout social_buttons = (LinearLayout) this.getActivity().findViewById(R.id.layout_social);
        social_buttons.setVisibility(View.GONE);
    }
}
