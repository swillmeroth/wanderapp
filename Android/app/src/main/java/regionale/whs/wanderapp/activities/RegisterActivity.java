package regionale.whs.wanderapp.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;

import regionale.whs.wanderapp.R;
import regionale.whs.wanderapp.service.WanderWSClient;
import regionale.whs.wanderapp.tools.Validator;
import regionale.whs.wanderapp.wspojos.WanderUser;

public class RegisterActivity extends ActionBarActivity implements DialogInterface.OnClickListener{

    private EditText mUsernameEdit;
    private EditText mEmailEdit;
    private EditText mPassword1Edit;
    private EditText mPassword2Edit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mUsernameEdit = (EditText) findViewById(R.id.edit_username);
        mEmailEdit = (EditText) findViewById(R.id.edit_email);
        mPassword1Edit = (EditText) findViewById(R.id.edit_password1);
        mPassword2Edit = (EditText) findViewById(R.id.edit_password2);
    }

    public void doRegister(View view) {
        String email, password1, password2, username;
        boolean valid = true;
        boolean password1Error = false;

        //reset errors
        mEmailEdit.setError(null);
        mUsernameEdit.setError(null);
        mPassword1Edit.setError(null);
        mPassword2Edit.setError(null);

        email = mEmailEdit.getText().toString();
        username = mUsernameEdit.getText().toString();
        password1 = mPassword1Edit.getText().toString();
        password2 = mPassword2Edit.getText().toString();

        // check for empty email
        if(TextUtils.isEmpty(email)) {
            mEmailEdit.setError(getString(R.string.error_field_required));
            valid = false;
        }

        // check for valid email
        if(!Validator.isEmailValid(email)) {
            mEmailEdit.setError(getString(R.string.error_invalid_email));
            valid = false;
        }

        // check for empty username
        if(TextUtils.isEmpty(username)) {
            mUsernameEdit.setError(getString(R.string.error_field_required));
            valid = false;
        }

        // check for valid username
        if (!Validator.isUsernameValid(username)) {
            mUsernameEdit.setError(getString(R.string.error_invalid_email));
            valid = false;
        }

        // check for empty password1
        if(TextUtils.isEmpty(password1)) {
            mPassword1Edit.setError(getString(R.string.error_field_required));
            valid = false;
            password1Error = true;
        }

        // check for valid password1
        if (!Validator.isPasswordValid(password1)) {
            mPassword1Edit.setError(getString(R.string.error_invalid_password));
            valid = false;
            password1Error = true;
        }

        // check if passwords are equal (only if no error at password1)
        if( !password1Error && !password2.equals(password1)) {
            mPassword2Edit.setError(getString(R.string.error_different_passwords));
            valid = false;
        }

        // if all checks were positive: call register
        if( valid) {
            WanderWSClient wanderWSClient = WanderWSClient.getInstance();
            boolean success = false;

            if( wanderWSClient.register(email, password1, username, this, true)) {
                wanderWSClient.setAuthenticationData(email, password1);
                WanderUser wanderUser = wanderWSClient.testAuthentication(this, true);

                if( wanderUser != null && wanderUser.getEmail().equals(email))
                    success = true;
            }

            if(success)
            {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(getString(R.string.title_activity_register));
                builder.setMessage(getString(R.string.registration_successful));
                builder.setPositiveButton(getString(R.string.ok), this);
                builder.create().show();
            }
            else
            {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(getString(R.string.error));
                builder.setMessage(getString(R.string.error_registration));
                builder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                builder.create().show();
            }
        }
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
