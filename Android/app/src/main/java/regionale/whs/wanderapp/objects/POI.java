package regionale.whs.wanderapp.objects;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import regionale.whs.wanderapp.R;
import regionale.whs.wanderapp.objects.enums.POIType;
import regionale.whs.wanderapp.service.WanderWSClient;
import regionale.whs.wanderapp.tools.AppSettings;
import regionale.whs.wanderapp.tools.GeoCalc;
import regionale.whs.wanderapp.wspojos.GeoLocation;
import regionale.whs.wanderapp.wspojos.WanderDescription;
import regionale.whs.wanderapp.wspojos.WanderPOI;
import regionale.whs.wanderapp.wspojos.WanderUser;

/**
 * Object representing a POI
 */
public class POI implements Parcelable
{

    //-----------------------------//
    //          VARIABLES          //
    //-----------------------------//



    public static final Parcelable.Creator<POI> CREATOR = new Parcelable.Creator<POI>()
    {
        public POI createFromParcel(Parcel in)
        {
            return new POI(in);
        }

        public POI[] newArray(int size)
        {
            return new POI[size];
        }
    };

    private int id;
    private String name;
    private POIType type;
    private float rating;
    private String description;
    private Date dateAdded;
    private double latitude;
    private double longitude;
    private int altitude;
    private String postalCode;
    private String city;
    private String street;
    private String accessibilityInfo; //TODO: muss in DB
    private int size;
    private String openingTimes;
    private int distance;

    private int idChanging;
    private boolean isChange;
    private boolean isProposal;
    private int commentCount;
    private int multimediaCount;
    private int todosCount;

    /**
     * Converts the poi to a WanderPOI.
     * @param user WanderUser for setting the correct Locale and Interest for description
     * @return WanderPOI
     */
    public WanderPOI toWanderPOI(WanderUser user)
    {
        WanderPOI wanderPOI = new WanderPOI();
        wanderPOI.setId(this.id);
        wanderPOI.setName(this.name);
        wanderPOI.setPoiType(getWanderTypeString());
        wanderPOI.setCity(this.city);
        wanderPOI.setKoordinaten(new GeoLocation(this.latitude, this.longitude));
        wanderPOI.setOpenTimes(this.openingTimes);
        wanderPOI.setDateAdded(this.dateAdded);
        wanderPOI.setIdChanging(this.idChanging);
        wanderPOI.setIsChange(this.isChange);
        wanderPOI.setIsProposal(this.isProposal);
        wanderPOI.setPostalCode(this.postalCode);
        wanderPOI.setCommentCount(this.commentCount);
        wanderPOI.setMultimediaCount(this.multimediaCount);
        wanderPOI.setTodosCount(this.todosCount);
        wanderPOI.setStreet(this.street);
        wanderPOI.setSize(this.size);
        wanderPOI.setRating(this.rating);

        WanderDescription description = new WanderDescription();
        if(user != null) {
            description.setLanguage(user.getLanguage());
            description.setInterest(user.getInterest());
        }
        description.setText(this.description);

        return wanderPOI;
    }

    public static ArrayList<POI> convertWanderPOIs(List<WanderPOI> wanderPOIs)
    {
        if( wanderPOIs == null)
            return null;

        ArrayList<POI> pois = new ArrayList<>();

        for( WanderPOI wanderPOI : wanderPOIs)
        {
            pois.add(new POI(wanderPOI));
        }

        return pois;
    }

    private String getWanderTypeString()
    {
        switch(this.type)
        {
            case ABBEY:
                return AppSettings.WANDER_POITYPE_ABBEY;
            case CHAPEL:
                return AppSettings.WANDER_POITYPE_CHAPEL;
            case CHURCH:
                return AppSettings.WANDER_POITYPE_CHURCH;
            case CROSS:
                return AppSettings.WANDER_POITYPE_CROSS;
            case GRAVEYARD:
                return AppSettings.WANDER_POITYPE_GRAVEYARD;
            case JEWISH_GRAVEYARD:
                return AppSettings.WANDER_POITYPE_JEWISH_GRAVEYARD;
            case MONUMENT:
                return AppSettings.WANDER_POITYPE_MONUMENT;
            case STATUE:
                return AppSettings.WANDER_POITYPE_STATUE;
            default:
                return "";
        }
    }

    private static POIType getTypeFromWanderString(String wanderString)
    {
        if(wanderString.equals(AppSettings.WANDER_POITYPE_ABBEY))
            return POIType.ABBEY;

        if(wanderString.equals(AppSettings.WANDER_POITYPE_CHAPEL))
            return POIType.CHAPEL;

        if(wanderString.equals(AppSettings.WANDER_POITYPE_CHURCH))
            return POIType.CHURCH;

        if(wanderString.equals(AppSettings.WANDER_POITYPE_CROSS))
            return POIType.CROSS;

        if(wanderString.equals(AppSettings.WANDER_POITYPE_GRAVEYARD))
            return POIType.GRAVEYARD;

        if(wanderString.equals(AppSettings.WANDER_POITYPE_JEWISH_GRAVEYARD))
            return POIType.JEWISH_GRAVEYARD;

        if(wanderString.equals(AppSettings.WANDER_POITYPE_MONUMENT))
            return POIType.MONUMENT;

        if(wanderString.equals(AppSettings.WANDER_POITYPE_STATUE))
            return POIType.STATUE;

        throw new IllegalArgumentException("Unknown typestring: " + wanderString);
    }

    //--------------------------------//
    //          CONSTRUCTORS          //
    //--------------------------------//
    public int drawableId;

    public POI(WanderPOI wanderPOI)
    {
        initialize();

        this.id = wanderPOI.getId();
        this.name = wanderPOI.getName();
        this.type = POI.getTypeFromWanderString(wanderPOI.getPoiType());
        this.city = wanderPOI.getCity();
        this.latitude = wanderPOI.getKoordinaten().getLatitude();
        this.longitude = wanderPOI.getKoordinaten().getLongitude();
        this.openingTimes = wanderPOI.getOpenTimes();
        this.dateAdded = wanderPOI.getDateAdded();
        this.idChanging = wanderPOI.getIdChanging();
        this.isChange = wanderPOI.getIsChange();
        this.isProposal = wanderPOI.getIsProposal();
        this.postalCode = wanderPOI.getPostalCode();
        this.commentCount = wanderPOI.getCommentCount();
        this.multimediaCount = wanderPOI.getMultimediaCount();
        this.todosCount = wanderPOI.getTodosCount();
        this.street = wanderPOI.getStreet();
        this.size = wanderPOI.getSize();
        this.rating = wanderPOI.getRating();

        if(wanderPOI.getDescriptions().size() > 0) {
            this.description = wanderPOI.getDescriptions().get(0).getText();

            WanderUser user = WanderWSClient.getInstance().getCurrentUser();
            if(user != null) {
                for (WanderDescription description : wanderPOI.getDescriptions()) {
                    if(user.getLanguage().equals(description.getLanguage()) && user.getInterest().equals(description.getInterest()))
                        this.description = description.getText();
                }
            }
        }
    }

    public POI()
    {
        initialize();
    }

    public POI(int id, String name, POIType type)
    {
        initialize();

        this.id = id;
        this.name = name;
        this.type = type;
    }

    public POI(String name, POIType type, int distance, int drawableId)
    {
        initialize();

        this.name = name;
        this.type = type;
        this.distance = distance;
        this.drawableId = drawableId;
    }

    public POI(int id, String name, POIType type, String description, Date dateAdded, double latitude, double longitude, int altitude, String postal_code, String city, String street, int size, String opening_times, int distance, int drawableId, String accessibility_info)
    {
        initialize();

        this.id = id;
        this.name = name;
        this.type = type;
        this.description = description;
        this.dateAdded = dateAdded;
        this.latitude = latitude;
        this.longitude = longitude;
        this.altitude = altitude;
        this.postalCode = postal_code;
        this.city = city;
        this.street = street;
        this.size = size;
        this.openingTimes = opening_times;
        this.distance = distance;
        this.drawableId = drawableId;
        this.accessibilityInfo = accessibility_info;
    }

    private POI(Parcel in)
    {
        initialize();

        id = in.readInt();
        name = in.readString();
        type = POIType.valueOf(in.readString());
        rating = in.readFloat();
        description = in.readString();
        dateAdded = (Date) in.readSerializable();
        latitude = in.readDouble();
        longitude = in.readDouble();
        altitude = in.readInt();
        postalCode = in.readString();
        city = in.readString();
        street = in.readString();
        size = in.readInt();
        openingTimes = in.readString();
        accessibilityInfo = in.readString();
        drawableId = in.readInt();
        idChanging = in.readInt();
        isChange = in.readByte() != 0;
        isProposal = in.readByte() != 0;
        commentCount = in.readInt();
        multimediaCount = in.readInt();
        todosCount = in.readInt();
    }

    //---------------------------------//
    //          LOGIC METHODS          //
    //---------------------------------//

    private void initialize()
    {
        this.id = 0;
        this.name = "";
        this.rating = 0;
        this.description = "";
        this.dateAdded = new Date();
        this.latitude = 0;
        this.longitude = 0;
        this.altitude = 0;
        this.postalCode = "";
        this.city = "";
        this.street = "";
        this.size = 0;
        this.openingTimes = "";
        this.distance = 0;
        this.accessibilityInfo = "";
        this.drawableId = 0;
        this.idChanging = 0;
        this.isChange = false;
        this.isProposal = false;
        this.commentCount = 0;
        this.multimediaCount = 0;
        this.todosCount = 0;
    }

    /**
     * Calculates the distance between the POIs coordinates and
     * the given parameters. Includes altitude.
     * By doing this also sets the distance variable in
     * POI instance.
     *
     * @param lat_home  latitude
     * @param long_home longitude
     * @param alt       altitude
     * @return distance in meters
     */
    public int calcDistance(double lat_home, double long_home, int alt)
    {
        int result = distance;

        if (latitude != 0 && longitude != 0)
        {
            result = (int) Math.round(GeoCalc.calcDistance(latitude, longitude, lat_home, long_home));
        }

        distance = result;
        return result;
    }

    /**
     * Checks if there is a street and city string set.
     * Used for assembling a usable geo-Link in POIDetailActivity.
     *
     * @return true if both strings are set.
     */
    public boolean hasAddress()
    {
        return !street.equals("") && !city.equals("");
    }

    //----------------------------------------//
    //          PARCELABLE INTERFACE          //
    //----------------------------------------//

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags)
    {
        out.writeInt(id);
        out.writeString(name);
        out.writeString(type.toString());
        out.writeFloat(rating);
        out.writeString(description);
        out.writeSerializable(dateAdded);
        out.writeDouble(latitude);
        out.writeDouble(longitude);
        out.writeInt(altitude);
        out.writeString(postalCode);
        out.writeString(city);
        out.writeString(street);
        out.writeInt(size);
        out.writeString(openingTimes);
        out.writeString(accessibilityInfo);
        out.writeInt(drawableId);
        out.writeInt(idChanging);
        out.writeByte((byte) (isChange ? 1 : 0));
        out.writeByte((byte) (isProposal ? 1 : 0));
        out.writeInt(commentCount);
        out.writeInt(multimediaCount);
        out.writeInt(todosCount);
    }

    //---------------------------------------//
    //          GETTERS AND SETTERS          //
    //---------------------------------------//

    public static String getRatingString(POI poi, Context context)
    {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append(AppSettings.RATING_FORMAT.format(poi.getRating()));
        stringBuilder.append(" (");
        stringBuilder.append(poi.getCommentCount());
        stringBuilder.append(" ");
        stringBuilder.append(context.getString(R.string.ratings));
        stringBuilder.append(")");

        return stringBuilder.toString();
    }

    public double getRating()
    {
        return rating;
    }

    public void setRating(float rating)
    {
        this.rating = rating;
    }

    public String getAccessibilityInfo()
    {
        return accessibilityInfo;
    }

    public void setAccessibilityInfo(String accessibilityInfo)
    {
        this.accessibilityInfo = accessibilityInfo;
    }

    public String getAddress()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(street);
        stringBuilder.append("\n");
        stringBuilder.append(postalCode);
        stringBuilder.append(" ");
        stringBuilder.append(city);

        //TODO: Website and phone?

        return stringBuilder.toString();
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public int getDrawableId()
    {
        return drawableId;
    }

    public void setDrawableId(int drawableId)
    {
        this.drawableId = drawableId;
    }

    public POIType getType()
    {
        return type;
    }

    public void setType(POIType type)
    {
        this.type = type;
    }

    public int getDistance()
    {
        return distance;
    }

    public void setDistance(int distance)
    {
        this.distance = distance;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public Date getDateAdded()
    {
        return dateAdded;
    }

    public void setDateAdded(Date date_added)
    {
        this.dateAdded = date_added;
    }

    public double getLatitude()
    {
        return latitude;
    }

    public void setLatitude(double latitude)
    {
        this.latitude = latitude;
    }

    public double getLongitude()
    {
        return longitude;
    }

    public void setLongitude(double longitude)
    {
        this.longitude = longitude;
    }

    public int getAltitude()
    {
        return altitude;
    }

    public void setAltitude(int altitude)
    {
        this.altitude = altitude;
    }

    public void setCoordinates(double latitude, double longitude)
    {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getCity()
    {
        return city;
    }

    public void setCity(String city)
    {
        this.city = city;
    }

    public String getStreet()
    {
        return street;
    }

    public void setStreet(String street)
    {
        this.street = street;
    }

    public int getSize()
    {
        return size;
    }

    public void setSize(int size)
    {
        this.size = size;
    }

    public String getOpeningTimes()
    {
        return openingTimes;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public int getTodosCount() {
        return todosCount;
    }

    public void setTodosCount(int todosCount) {
        this.todosCount = todosCount;
    }

    public int getMultimediaCount() {
        return multimediaCount;
    }

    public void setMultimediaCount(int multimediaCount) {
        this.multimediaCount = multimediaCount;
    }

    public int getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(int commentCount) {
        this.commentCount = commentCount;
    }

    public boolean isProposal() {
        return isProposal;
    }

    public void setIsProposal(boolean isProposal) {
        this.isProposal = isProposal;
    }

    public boolean isChange() {
        return isChange;
    }

    public void setIsChange(boolean isChange) {
        this.isChange = isChange;
    }

    public int getIdChanging() {
        return idChanging;
    }

    public void setIdChanging(int idChanging) {
        this.idChanging = idChanging;
    }

    public void setOpeningTimes(String openingTimes) {
        this.openingTimes = openingTimes;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }
}