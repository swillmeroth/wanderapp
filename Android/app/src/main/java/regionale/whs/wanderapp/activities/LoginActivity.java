package regionale.whs.wanderapp.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;

import regionale.whs.wanderapp.R;
import regionale.whs.wanderapp.service.WanderWSClient;
import regionale.whs.wanderapp.tools.AppSettings;
import regionale.whs.wanderapp.tools.Validator;
import regionale.whs.wanderapp.wspojos.WanderUser;


/**
 * A login screen that offers login via email/password. The user is also able to start the register dialog and to continue as a guest without logging in.
 * As this activity is always called when starting the app, it also checks for the existence of offline map files.
 */
public class LoginActivity extends Activity
{
    // UI references.
    private EditText mEmailView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Set up the login form.
        mEmailView = (EditText) findViewById(R.id.email);

        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        Button mLoginButton = (Button) findViewById(R.id.button_login);
        mLoginButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);

        // check
        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        File file = new File(path, AppSettings.MAPFILE);
        if(file.exists()) {
            WanderWSClient.getInstance().setHasOfflineData(true);
            Toast.makeText(this, "Offline Map data found!", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    public void attemptLogin() {

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;


        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !Validator.isPasswordValid(password))
        {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email))
        {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!Validator.isEmailValid(email))
        {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel)
        {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else
        {
            WanderWSClient wanderWSClient = WanderWSClient.getInstance();
            wanderWSClient.setAuthenticationData(email, password);
            WanderUser wanderUser = wanderWSClient.testAuthentication(this, true);

            if(wanderUser != null && wanderUser.getEmail().equals(email)) {
                // open main page
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
            } else {
                // remove authentication data from WanderWSClient
                wanderWSClient.resetAuthenticationData();

                // show error dialog
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(getString(R.string.error_incorrect_logindata));
                builder.setTitle(getString(R.string.error));
                builder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        }
    }

    /**
     * Starts next activity without making a login attempt.
     * @param view
     */
    public void doGuestContinue(View view)
    {
        // start main activity
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    /**
     * Starts the RegisterActivity allowing the user to register to WanderApp.
     * @param view
     */
    public void openRegister(View view) {
        // start register activity
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }
}



