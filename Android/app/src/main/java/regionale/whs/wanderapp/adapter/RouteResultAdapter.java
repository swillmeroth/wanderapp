package regionale.whs.wanderapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import regionale.whs.wanderapp.R;
import regionale.whs.wanderapp.objects.Route;


/**
 * Adapter for populating the data fields of a route item in the result list.
 *
 * Created by Stephan on 17.10.2014.
 */
public class RouteResultAdapter extends ArrayAdapter<Route>
{

    public RouteResultAdapter(Context context, ArrayList<Route> routes)
    {

        super(context, 0, routes);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {

        // Get the data item for this position
        Route route = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null)
        {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_result_route, parent, false);
        }
        // Lookup view for data population
        TextView view_name = (TextView) convertView.findViewById(R.id.name);
        TextView view_duration = (TextView) convertView.findViewById(R.id.durationText);
        TextView view_distance = (TextView) convertView.findViewById(R.id.distanceText);
        TextView view_length = (TextView) convertView.findViewById(R.id.lengthText);
        ImageView view_image = (ImageView) convertView.findViewById(R.id.imageView);
        // Populate the data into the template view using the data object

        view_image.setImageResource(route.getDrawableId());
        view_name.setText(route.getName());

        view_duration.setText(route.getDurationString());
        view_distance.setText(route.getDistanceString());
        view_length.setText(route.getLengthString());

        // Return the completed view to render on screen
        return convertView;
    }
}
