package regionale.whs.wanderapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.View;
import android.widget.Button;

import regionale.whs.wanderapp.R;

public class SearchActivity extends StandardActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);

        // remove history button until the functionality is implemented
        Button button_history = (Button) findViewById(R.id.button_history);
        button_history.setVisibility(View.GONE);
    }

    // when POISearch is clicked, start next Activity
    public void onPOISearchClick(View view)
    {
        Intent intent = new Intent(this, POIRouteSearchActivity.class);
        intent.putExtra("isRouteSearch", false);
        startActivity(intent);
    }

    // when RouteSearch is clicked, start next Activity
    public void onRouteSearchClick(View view)
    {
        Intent intent = new Intent(this, POIRouteSearchActivity.class);
        intent.putExtra("isRouteSearch", true);
        startActivity(intent);
    }
}
