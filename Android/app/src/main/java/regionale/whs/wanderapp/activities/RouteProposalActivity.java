package regionale.whs.wanderapp.activities;

import android.os.Bundle;

import regionale.whs.wanderapp.R;
import regionale.whs.wanderapp.objects.POI;
import regionale.whs.wanderapp.objects.Route;
import regionale.whs.wanderapp.wspojos.WanderPOI;
import regionale.whs.wanderapp.wspojos.WanderRoute;

/**
 * Activity for creating change proposal for an existing route.
 */
public class RouteProposalActivity extends StandardActivity {

    private POI mPOI;
    private WanderPOI mNewPOI;

    private Route mRoute;
    private WanderRoute mNewRoute;

    private boolean isNew;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_routeproposal);
    }

    // get POI or Route from intent (using isRoute boolean)
    // show data like DetailFragment, but in EditText
    // dialogs for multi choice things (like type)
    // layout in scroll view, button on bottom but always visible
}
