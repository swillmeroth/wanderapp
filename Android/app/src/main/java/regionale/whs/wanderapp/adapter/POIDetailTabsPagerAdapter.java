package regionale.whs.wanderapp.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import regionale.whs.wanderapp.activities.POIDetailActivity;
import regionale.whs.wanderapp.fragments.CommentsFragment;
import regionale.whs.wanderapp.fragments.POIDetailMainFragment;
import regionale.whs.wanderapp.objects.POI;

/**
 * Adapter for the tabs of the POIDetailActivity.
 * Tab 1: main details
 * Tab 2: comments
 *
 * Created by Stephan on 04.11.2014.
 */
public class POIDetailTabsPagerAdapter extends FragmentPagerAdapter
{

    private static final int ROUTE_DETAIL_TAB_COUNT = 2; // 3, if altitude profile is implemented
    private POIDetailActivity mActivity;
    private POI mPOI;
    private CommentsAdapter mAdapter;

    public POIDetailTabsPagerAdapter(FragmentManager fm, POI poi, CommentsAdapter adapter, POIDetailActivity activity)
    {
        super(fm);
        mAdapter = adapter;
        mPOI = poi;
        mActivity = activity;
    }

    @Override
    public Fragment getItem(int position)
    {
        // TODO: decide if we want to stick with the altitude profile thing
        switch (position)
        {
            case 0:
                return POIDetailMainFragment.newInstance(mPOI);
            case 1:
                return CommentsFragment.newInstance(mAdapter, mActivity);
            /*
            case 2:
                return RouteDetailMainFragment.newInstance(mPOI);*/
        }

        return null;
    }

    @Override
    public int getCount()
    {
        return ROUTE_DETAIL_TAB_COUNT;
    }
}
