package regionale.whs.wanderapp.activities;

import android.content.Intent;
import android.os.Bundle;

import regionale.whs.wanderapp.R;
import regionale.whs.wanderapp.fragments.MapFragment;
import regionale.whs.wanderapp.objects.POI;
import regionale.whs.wanderapp.objects.Route;

/**
 * Activity that serves mainly as a wrapper for a MapFragment, when is to be shown fullscreen and not as a tab.
 * Also implements the listeners for a click on the POI and Route Infos shown after tapping on an icon in the map.
 */
public class MapActivity extends StandardActivity implements MapFragment.OnPOIInfoClickListener, MapFragment.OnRouteInfoClickListener
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        MapFragment fragment = (MapFragment) getSupportFragmentManager().findFragmentById(R.id.fragment);
        fragment.setOnPOIInfoClickListener(this);
        fragment.setOnRouteInfoClickListener(this);
    }

    /**
     * Starts POIDetailactivity
     * @param poi Clicked POI
     */
    @Override
    public void onPOIInfoClick(POI poi)
    {
        Intent intent = new Intent(this, POIDetailActivity.class);
        intent.putExtra("poi", poi);
        startActivity(intent);
    }

    /**
     * Starts RouteDetailActivity
     * @param route Clicked Route
     */
    @Override
    public void onRouteInfoClick(Route route)
    {
        Intent intent = new Intent(this, RouteDetailActivity.class);
        intent.putExtra("route", route);
        startActivity(intent);
    }
}