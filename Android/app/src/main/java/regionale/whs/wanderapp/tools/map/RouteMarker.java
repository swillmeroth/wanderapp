package regionale.whs.wanderapp.tools.map;

import org.mapsforge.core.graphics.Bitmap;
import org.mapsforge.core.model.LatLong;
import org.mapsforge.core.model.Point;
import org.mapsforge.map.layer.overlay.Marker;

import regionale.whs.wanderapp.objects.Route;
import regionale.whs.wanderapp.tools.AppSettings;

/**
 * Marker variant for a route
 *
 * Created by Stephan on 18.02.2015.
 */
public class RouteMarker extends Marker
{
    private Route route;
    private OnRouteTappedListener tapListener;

    public RouteMarker(LatLong latLong, Bitmap bitmap, int horizontalOffset, int verticalOffset, OnRouteTappedListener listener, Route route)
    {
        super(latLong, bitmap, horizontalOffset, verticalOffset);
        this.route = route;
        this.tapListener = listener;
    }

    public Route getRoute()
    {
        return route;
    }

    @Override
    public boolean onTap(LatLong tapLatLong, Point layerXY, Point tapXY)
    {
        double centerX = layerXY.x + getHorizontalOffset();
        double centerY = layerXY.y + getVerticalOffset();

        double factor = 1 + (AppSettings.MARKER_TAP_TOLERANCE_PERCENTAGE / 100);
        double radiusX = (getBitmap().getWidth() / 2) * factor;
        double radiusY = (getBitmap().getHeight() / 2) * factor;

        double distX = Math.abs(centerX - tapXY.x);
        double distY = Math.abs(centerY - tapXY.y);

        if (distX < radiusX && distY < radiusY)
        {
            tapListener.onRouteTapped(route);
            return true;
        } else
            return false;   // by returning false, the next layer's onTap-Method is called
    }

    public interface OnRouteTappedListener
    {
        void onRouteTapped(Route route);
    }
}
