package regionale.whs.wanderapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;

import regionale.whs.wanderapp.R;
import regionale.whs.wanderapp.adapter.TypeSearchAdapter;
import regionale.whs.wanderapp.objects.enums.POIType;
import regionale.whs.wanderapp.objects.enums.QueryType;
import regionale.whs.wanderapp.objects.enums.RouteType;
import regionale.whs.wanderapp.tools.SelectableString;
import regionale.whs.wanderapp.tools.comparators.SortTypes;
import regionale.whs.wanderapp.tools.converter.POITypeStringConverter;
import regionale.whs.wanderapp.tools.converter.RouteTypeStringConverter;

/**
 * Activity for selecting the types of POI or route to be searched for.
 */
public class TypeSearchActivity extends StandardActivity implements AdapterView.OnItemClickListener
{
    TypeSearchAdapter mAdapter;
    boolean isRouteSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_typesearch);

        isRouteSearch = this.getIntent().getExtras().getBoolean("isRouteSearch");

        ArrayList<SelectableString> types = getTypes();
        mAdapter = new TypeSearchAdapter(this, types);
        ListView listView = (ListView) findViewById(R.id.typeList);
        listView.setAdapter(mAdapter);
        listView.setOnItemClickListener(this);
    }

    /**
     * Returns the list of Types to be selected as SelectableStrings.
     * @return list of types
     */
    private ArrayList<SelectableString> getTypes()
    {
        ArrayList<SelectableString> types = new ArrayList<>();

        if (!isRouteSearch)
        {
            for (POIType type : POIType.values())
            {
                POITypeStringConverter converter = new POITypeStringConverter(this);
                String typeString = converter.convertPOITypeToString(type);
                types.add(new SelectableString(typeString, false));
            }
        } else
        {
            for (RouteType type : RouteType.values())
            {
                RouteTypeStringConverter converter = new RouteTypeStringConverter(this);
                String typeString = converter.convertRouteTypeToString(type);
                types.add(new SelectableString(typeString, false));
            }
        }

        /* make a sorted list
        * can't rely on sorted input from enum, because the
        * localized strings can differ in their alphabetical order */
        Collections.sort(types, new SortTypes());

        return types;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkBox);
        checkBox.toggle();
        mAdapter.getItem(position).setSelected(checkBox.isChecked());
    }

    /**
     * gets the selected items, converts them to POIType and starts the result activity.
     *
     * @param view clicked button
     */
    public void onDoTypeSearchClick(View view)
    {
        ArrayList<String> selectedTypes = new ArrayList<>();

        for (int i = 0; i < mAdapter.getCount(); i++)
        {
            SelectableString pair = mAdapter.getItem(i);
            if (pair.isSelected())
            {
                String typeString = pair.getString();

                if (!isRouteSearch)
                {
                    POITypeStringConverter converter = new POITypeStringConverter(this);
                    POIType type = converter.convertStringToPOIType(typeString);
                    if (type != null)
                        selectedTypes.add(type.toString().toLowerCase());
                    else
                        Log.println(Log.ERROR, "WanderApp", "Type not found: " + typeString);
                } else
                {
                    RouteTypeStringConverter converter = new RouteTypeStringConverter(this);
                    RouteType type = converter.convertStringToRouteType(typeString);
                    if (type != null)
                        selectedTypes.add(type.toString().toLowerCase());
                    else
                        Log.println(Log.ERROR, "WanderApp", "Type not found: " + typeString);
                }
            }
        }

        if (selectedTypes.size() > 0)
        {
            QueryType queryType = QueryType.TYPE;

            Intent intent = new Intent(this, ResultActivity.class);
            intent.putExtra("isRouteSearch", isRouteSearch);
            intent.putExtra("queryType", queryType.toString());
            intent.putStringArrayListExtra("types", selectedTypes);
            startActivity(intent);
        } else
        {
            Toast.makeText(this, getString(R.string.nothing_selected), Toast.LENGTH_LONG).show();
        }
    }
}
