package regionale.whs.wanderapp.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import regionale.whs.wanderapp.R;
import regionale.whs.wanderapp.fragments.dialogs.POITypeDialog;
import regionale.whs.wanderapp.objects.POI;
import regionale.whs.wanderapp.objects.enums.POIType;
import regionale.whs.wanderapp.service.WanderWSClient;
import regionale.whs.wanderapp.tools.converter.POITypeStringConverter;
import regionale.whs.wanderapp.wspojos.WanderPOI;

/**
 * Activity for proposing changes to a given POI. These Proposals are sent to the server and can be accepted by a moderator.
 */
public class POIProposalActivity extends StandardActivity implements POITypeDialog.OnPOITypeChangeListener {

    private POI mPOI, mNewPOI;

    private boolean isNew;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_poiproposal);

        isNew = this.getIntent().getBooleanExtra("isNew", true);
        mNewPOI = new POI();

        if(!isNew) {
            mPOI = this.getIntent().getParcelableExtra("poi");
            mNewPOI.setType(mPOI.getType());
            showData();
        }
    }

    /**
     * Shows the localized string value representing of the POIs type as button text.
     * @param useNew true if the new POI's type should be used. if false, the original type is used.
     */
    private void showType(boolean useNew)
    {
        // type
        Button button = (Button) findViewById(R.id.type);

        POIType type;

        if(useNew)
            type = mNewPOI.getType();
        else
            type = mPOI.getType();

        if (type != null)
        {
            POITypeStringConverter converter = new POITypeStringConverter(this);
            button.setText(converter.convertPOITypeToString(type));

        }
    }

    /**
     * Populates the layout's fields with the POI's data.
     */
    private void showData()
    {
        EditText editText;

        // title and name
        String name = mPOI.getName();
        if (!name.equals(""))
        {
            setTitle(name);
            editText = (EditText) findViewById(R.id.name);
            editText.setText(name);
        }

        showType(false);

        // description
        editText = (EditText) findViewById(R.id.description);
        String description = mPOI.getDescription();
        if (!description.equals(""))
        {
            editText.setText(description);
        }

        // opening hours
        editText = (EditText) findViewById(R.id.text_opening);
        String openingTimes = mPOI.getOpeningTimes();
        if (!openingTimes.equals(""))
        {
            editText.setText(openingTimes);
        }

        // street
        editText = (EditText) findViewById(R.id.text_street);
        String street = mPOI.getStreet();
        if (!street.equals(""))
        {
            editText.setText(street);
        }

        // postal code
        editText = (EditText) findViewById(R.id.text_postalcode);
        String postal = mPOI.getPostalCode();
        if (!postal.equals(""))
        {
            editText.setText(postal);
        }

        // city
        editText = (EditText) findViewById(R.id.text_city);
        String city = mPOI.getCity();
        if (!city.equals("\n "))
        {
            editText.setText(city);
        }

        /* TODO: add to db or delete
        // accessibility info
        textView = (TextView) findViewById(R.id.text_accessibility);
        String accessibility_info = mPOI.getAccessibilityInfo();
        if (!accessibility_info.equals(""))
        {
            textView.setText(accessibility_info);
        } else
        {
            textView.setVisibility(View.GONE);
            TextView title = (TextView) findViewById(R.id.title_accessibility);
            title.setVisibility(View.GONE);
        }*/
    }

    /**
     * Opens a dialog to select the type of the poi.
     * @param view clicked button
     */
    public void onTypeSelectionClick(View view) {
        POITypeDialog dialog = new POITypeDialog();
        dialog.setType(mPOI.getType());
        dialog.show(getSupportFragmentManager(), "POITypeDialog");
    }

    /**
     * Sets the selected type as the new POI's type
     * @param type selected type
     */
    @Override
    public void onPOITypeChange(POIType type) {
        mNewPOI.setType(type);
        showType(true);
    }

    public void onCancel(View view) {
        onBackPressed();
    }

    /**
     * Gets the user input data and creates a new POI object with these informations.
     * This object is send to the server.
     *
     * @param view clicked button
     */
    public void onSubmit(View view) {

        EditText editText;

        // take over non changeable values
        mNewPOI.setAltitude(mPOI.getAltitude());
        mNewPOI.setLatitude(mPOI.getLatitude());
        mNewPOI.setLongitude(mPOI.getLongitude());
        mNewPOI.setSize(mPOI.getSize());
        mNewPOI.setDateAdded(mPOI.getDateAdded());
        mNewPOI.setDrawableId(mPOI.getDrawableId());

        // name
        editText = (EditText) findViewById(R.id.name);
        mNewPOI.setName(editText.getText().toString());

        // description
        editText = (EditText) findViewById(R.id.description);
        mNewPOI.setDescription(editText.getText().toString());

        // opening hours
        editText = (EditText) findViewById(R.id.text_opening);
        mNewPOI.setOpeningTimes(editText.getText().toString());

        // street
        editText = (EditText) findViewById(R.id.text_street);
        mNewPOI.setStreet(editText.getText().toString());

        // postal code
        editText = (EditText) findViewById(R.id.text_postalcode);
        mNewPOI.setPostalCode(editText.getText().toString());

        // city
        editText = (EditText) findViewById(R.id.text_city);
        mNewPOI.setCity(editText.getText().toString());

        /* TODO: add to db or delete
        // accessibility info
        textView = (TextView) findViewById(R.id.text_accessibility);
        String accessibility_info = mPOI.getAccessibilityInfo();
        if (!accessibility_info.equals(""))
        {
            textView.setText(accessibility_info);
        } else
        {
            textView.setVisibility(View.GONE);
            TextView title = (TextView) findViewById(R.id.title_accessibility);
            title.setVisibility(View.GONE);
        }*/

        // send to server
        WanderWSClient client = WanderWSClient.getInstance();
        WanderPOI newWanderPOI = mNewPOI.toWanderPOI(client.getCurrentUser());
        newWanderPOI.setIsProposal(true);

        if(isNew) {
            newWanderPOI.setIsChange(false);
        } else {
            newWanderPOI.setIdChanging(mPOI.getId());
            newWanderPOI.setIsChange(true);
        }

        client.setPOI(newWanderPOI, this, true);

        // TODO: show thank you dialog

        onBackPressed();
    }
}
