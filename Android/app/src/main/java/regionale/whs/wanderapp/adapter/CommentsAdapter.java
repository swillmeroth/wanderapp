package regionale.whs.wanderapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import regionale.whs.wanderapp.R;
import regionale.whs.wanderapp.tools.AppSettings;
import regionale.whs.wanderapp.tools.converter.POITypeStringConverter;
import regionale.whs.wanderapp.wspojos.WanderKommentar;

/**
 * Adapter to populate the comment data fields. Used in CommentsFragment.
 *
 * Created by Stephan on 19.02.2015.
 */
public class CommentsAdapter extends ArrayAdapter<WanderKommentar>
{
    public CommentsAdapter(Context context, List<WanderKommentar> comments)
    {
        super(context, 0, comments);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        POITypeStringConverter converter = new POITypeStringConverter(getContext());

        // Get the data item for this position
        WanderKommentar comment = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null)
        {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_comment, parent, false);
        }

        // Lookup view for data population
        TextView view_name = (TextView) convertView.findViewById(R.id.name);
        RatingBar view_rating = (RatingBar) convertView.findViewById(R.id.ratingBar);
        TextView view_date = (TextView) convertView.findViewById(R.id.text_date);
        TextView view_text = (TextView) convertView.findViewById(R.id.text_comment);

        // Populate the data into the template view using the data object
        view_name.setText(comment.getAuthorName());
        view_rating.setNumStars(comment.getRating());
        view_text.setText(comment.getText());

        String format_string = AppSettings.getLocaleDateFormat(Locale.getDefault());
        DateFormat formatter = new SimpleDateFormat(format_string);
        String date_formatted = formatter.format(comment.getTime());
        view_date.setText(date_formatted);


        // Return the completed view to render on screen
        return convertView;
    }
}
