package regionale.whs.wanderapp.objects.enums;

/**
 * Enum for the various modes of the MapFragment
 *
 * Created by Stephan on 04.02.2015.
 */
public enum MapType
{
    PLAIN,
    NEIGHBORHOOD,
    POIRESULTS,
    ROUTERESULTS,
    SINGLEPOI,
    SINGLEROUTE,
    NAVIGATION,
    MapType, DYNAMICROUTE
}
