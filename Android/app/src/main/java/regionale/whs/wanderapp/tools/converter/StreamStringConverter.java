package regionale.whs.wanderapp.tools.converter;

import android.os.AsyncTask;

import java.io.InputStream;
import java.util.concurrent.ExecutionException;

/**
 * Converts an InputStream to a string. This is done in an AsyncTask because the input stream can be from network sources.
 *
 * Created by Stephan on 07.04.2015.
 */
public class StreamStringConverter {

    public static String convertStreamToString(java.io.InputStream is)
    {
        AsyncConvert asyncConvert = new AsyncConvert(is);
        asyncConvert.execute();

        try {
            return asyncConvert.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            return "";
        }
    }

    private static class AsyncConvert extends AsyncTask<Void, Void, String>
    {
        private InputStream is;

        public AsyncConvert(InputStream is)
        {
            this.is = is;
        }

        @Override
        protected String doInBackground(Void... params)
        {
            java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
            return s.hasNext() ? s.next() : "";
        }
    }

}
