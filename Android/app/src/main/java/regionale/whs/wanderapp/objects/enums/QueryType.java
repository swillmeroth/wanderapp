package regionale.whs.wanderapp.objects.enums;

/**
 * The possible types of searches
 *
 * Created by Stephan on 28.11.2014.
 */
public enum QueryType
{
    NAME,
    TYPE
}
