package regionale.whs.wanderapp.fragments.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import regionale.whs.wanderapp.R;
import regionale.whs.wanderapp.tools.Validator;

/**
 * Dialog for changing the user's password
 */
public class PasswordDialog extends DialogFragment {

    private OnPasswordChangeListener mListener;
    private EditText mEditText1, mEditText2;
    private AlertDialog mDialog;

    @Override
    public void onAttach(Activity activity) {
        System.out.println("attaching");
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (OnPasswordChangeListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_password, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle(R.string.password);

        builder.setView(v);
        mEditText1 = (EditText) v.findViewById(R.id.password1);
        mEditText2 = (EditText) v.findViewById(R.id.password2);

        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String password1 = mEditText1.getText().toString();
                String password2 = mEditText2.getText().toString();

                if(!password1.equals(password2))
                    Toast.makeText(getActivity(), R.string.error_different_passwords, Toast.LENGTH_LONG).show();
                else if(!Validator.isPasswordValid(password1))
                    Toast.makeText(getActivity(), R.string.error_invalid_password, Toast.LENGTH_LONG).show();
                else
                    mListener.onPasswordChange(password1);
            }
        });

        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
            }
        });

        mDialog = builder.create();
        return mDialog;
    }

    public interface OnPasswordChangeListener {
        void onPasswordChange(String password);
    }
}
