package regionale.whs.wanderapp.tools;

/**
 * Combination of boolean and string to realize a list of selectable string values. Used in TypeSearchActivity.
 *
 * Created by Stephan on 27.11.2014.
 */
public class SelectableString
{
    private String string;
    private Boolean selected;

    public SelectableString(String string, Boolean selected)
    {
        this.string = string;
        this.selected = selected;
    }

    public String getString()
    {
        return string;
    }

    public void setString(String string)
    {
        this.string = string;
    }

    public Boolean isSelected()
    {
        return selected;
    }

    public void setSelected(Boolean selected)
    {
        this.selected = selected;
    }
}
