package regionale.whs.wanderapp.fragments.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

import regionale.whs.wanderapp.R;
import regionale.whs.wanderapp.tools.AppSettings;

/**
 * Dialog for changing the user's walking speed
 */
public class WalkingSpeedDialog extends DialogFragment implements DialogInterface.OnClickListener {

    OnWalkingSpeedChangeListener mListener;
    private int mSelected = 1;
    private String mPrevSpeed;
    private String mWalkingSpeed = "";

    public void setWalkingSpeed(String walkingSpeed) {
        mSelected = getIntForSpeedString(walkingSpeed);
        mPrevSpeed = mWalkingSpeed;
    }

    private int getIntForSpeedString(String walkingSpeed) {
        if(walkingSpeed.equals(AppSettings.WANDER_WALKINGSPEED_SLOW)) {
            mWalkingSpeed = walkingSpeed;
            return 0;
        }
        else if(walkingSpeed.equals(AppSettings.WANDER_WALKINGSPEED_NORMAL)) {
            mWalkingSpeed = walkingSpeed;
            return 1;
        }
        else if(walkingSpeed.equals(AppSettings.WANDER_WALKINGSPEED_FAST)) {
            mWalkingSpeed = walkingSpeed;
            return 2;
        }
        else {
            mWalkingSpeed = AppSettings.WANDER_WALKINGSPEED_NORMAL;
            return 1;
        }
    }

    // Override the Fragment.onAttach() method to instantiate the NoticeDialogListener
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (OnWalkingSpeedChangeListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String[] values = {getString(R.string.walking_speed_slow), getString(R.string.walking_speed_normal), getString(R.string.walking_speed_fast)};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle(R.string.walking_speed);

        builder.setSingleChoiceItems(values, mSelected, this);

        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if(!mPrevSpeed.equals(mWalkingSpeed)) {
                    mListener.onWalkingSpeedChange(mWalkingSpeed);
                }
                // User clicked OK button
            }
        });

        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
            }
        });

        return builder.create();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch(which) {
            case 0:
                mWalkingSpeed = AppSettings.WANDER_WALKINGSPEED_SLOW;
                break;
            case 1:
                mWalkingSpeed = AppSettings.WANDER_WALKINGSPEED_NORMAL;
                break;
            case 2:
                mWalkingSpeed = AppSettings.WANDER_WALKINGSPEED_FAST;
                break;
            default:
                break;
        }
    }

    public interface OnWalkingSpeedChangeListener {
        void onWalkingSpeedChange(String walkingSpeed);
    }
}
