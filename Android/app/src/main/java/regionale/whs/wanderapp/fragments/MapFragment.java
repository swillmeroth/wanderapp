package regionale.whs.wanderapp.fragments;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import org.mapsforge.core.graphics.Bitmap;
import org.mapsforge.core.model.BoundingBox;
import org.mapsforge.core.model.LatLong;
import org.mapsforge.core.util.LatLongUtils;
import org.mapsforge.map.android.graphics.AndroidGraphicFactory;
import org.mapsforge.map.android.graphics.AndroidResourceBitmap;
import org.mapsforge.map.android.layer.MyLocationOverlay;
import org.mapsforge.map.android.util.AndroidUtil;
import org.mapsforge.map.layer.cache.TileCache;
import org.mapsforge.map.layer.download.TileDownloadLayer;
import org.mapsforge.map.layer.download.tilesource.OnlineTileSource;
import org.mapsforge.map.layer.overlay.Marker;
import org.mapsforge.map.layer.overlay.Polyline;
import org.mapsforge.map.layer.renderer.TileRendererLayer;
import org.mapsforge.map.rendertheme.InternalRenderTheme;
import org.mapsforge.map.util.MapPositionUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import regionale.whs.wanderapp.R;
import regionale.whs.wanderapp.activities.StandardActivity;
import regionale.whs.wanderapp.objects.POI;
import regionale.whs.wanderapp.objects.Route;
import regionale.whs.wanderapp.objects.RoutePoint;
import regionale.whs.wanderapp.objects.enums.MapType;
import regionale.whs.wanderapp.service.WanderWSClient;
import regionale.whs.wanderapp.tools.AppSettings;
import regionale.whs.wanderapp.tools.map.MarkerUtil;
import regionale.whs.wanderapp.tools.map.MyMapView;
import regionale.whs.wanderapp.tools.map.POIMarker;
import regionale.whs.wanderapp.tools.map.RouteMarker;
import regionale.whs.wanderapp.wspojos.Kartenausschnitt;
import regionale.whs.wanderapp.wspojos.WanderPOI;
import regionale.whs.wanderapp.wspojos.WanderRoute;

/**
 * Fragment showing a map and various types of layers.
 *
 */
public class MapFragment extends Fragment implements POIMarker.OnPOITappedListener, RouteMarker.OnRouteTappedListener, MyMapView.onMapDragListener, View.OnClickListener, MyMapView.onSizeChangedListener, MyMapView.onMapsectionChangeListener, StandardActivity.MapDataListener
{

    private MyMapView mapView;
    private MyLocationOverlay myLocationOverlay;
    private TileCache tileCache;
    private TileRendererLayer tileRendererLayer;
    private TileDownloadLayer downloadLayer;
    private POI lastTappedPOI;
    private Route lastTappedRoute;
    private OnPOIInfoClickListener mPOIListener;
    private OnRouteInfoClickListener mRouteListener;
    private BoundingBox mBoundingBox;
    private BoundingBox mMapSection;
    private WanderWSClient mWanderWSClient;
    private HashMap<Integer, Marker> mPOIMarkers;
    private HashMap<Integer, Marker> mRouteMarkers;
    private boolean errorShown;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_map, container, false);

        this.mWanderWSClient = WanderWSClient.getInstance();

        this.mapView = (MyMapView) view.findViewById(R.id.mapView);
        this.mapView.setDragListener(this);
        this.mapView.setSizeChangedListener(this);
        this.mapView.setClickable(true);
        this.mapView.getMapScaleBar().setVisible(true);
        this.mapView.setBuiltInZoomControls(true);

        this.tileCache = AndroidUtil.createTileCache(this.getActivity(), "mapcache", mapView.getModel().displayModel.getTileSize(), 1f, this.mapView.getModel().frameBufferModel.getOverdrawFactor());

        setOnlineLayer();

        // set dummy location
        Location location = new Location("gps");
        location.setLatitude(51.74601111);
        location.setLongitude(7.179872222);

        LatLong LocationLatLong = new LatLong(location.getLatitude(), location.getLongitude());
        this.mapView.getModel().mapViewPosition.setCenter(LocationLatLong);
        this.mapView.getModel().mapViewPosition.setZoomLevel((byte) 12);

        chooseLayerMethod();

        return view;
    }

    @Override
    public void onAttach(Activity activity)
    {
        AndroidGraphicFactory.createInstance(activity.getApplication());

        // couldn't set the listener via ViewPager, so i had to use this workaround
        if (activity.getIntent().getBooleanExtra("routeDetail", false)) {
            setOnPOIInfoClickListener((OnPOIInfoClickListener) this.getActivity());
        }

        super.onAttach(activity);


        StandardActivity activity2 = (StandardActivity) getActivity();
        activity2.setMapDataListener(this);
        activity2.showOfflineItem(WanderWSClient.getInstance().hasOfflineData());

    }

    @Override
    public void onStart() {
        // couldn't set the listener via ViewPager, so i had to use this workaround
        if (this.getActivity().getIntent().getBooleanExtra("routeDetail", false)) {
            View navLayout = this.getActivity().findViewById(R.id.startNavLayout);
            navLayout.setVisibility(View.VISIBLE);
        }

        super.onStart();
    }

    public void setOnPOIInfoClickListener(OnPOIInfoClickListener mPOIListener)
    {
        this.mPOIListener = mPOIListener;
    }

    public void setOnRouteInfoClickListener(OnRouteInfoClickListener mRouteListener)
    {
        this.mRouteListener = mRouteListener;
    }

    /**
     * Gets the mode of the map from intent and calls methods for showing the needed data
     */
    private void chooseLayerMethod()
    {
        MapType mapType = MapType.valueOf(this.getActivity().getIntent().getExtras().getString("maptype"));

        switch (mapType)
        {
            case PLAIN:
                break;
            case DYNAMICROUTE:
            case SINGLEROUTE:
                makeSingleRouteLayer();
                break;
            case NEIGHBORHOOD:
                makeNeighborhoodLayers(true, true);
                break;
            case POIRESULTS:
                makeNeighborhoodLayers(true, false);
                break;
            case ROUTERESULTS:
                makeNeighborhoodLayers(false, true);
                break;
            case SINGLEPOI:
                makeSinglePOILayer();
                break;
            case NAVIGATION:
                makeNavigationLayers();
                break;
        }
    }

    /**
     * in navigation mode, a single route and the position is shown
     */
    private void makeNavigationLayers()
    {
        // show route
        makeSingleRouteLayer();

        // show location
        setPositionLayer(true);
    }

    /**
     * gets the route from intent and shows the route in full, not just icon
     */
    private void makeSingleRouteLayer()
    {
        Route route = this.getActivity().getIntent().getExtras().getParcelable("route");

        if(route == null)
            return;

        addRouteLayer(route, true);

        if (route.getBoundingBox().getLatitudeSpan() > 0)
        {
            mBoundingBox = route.getBoundingBox();

            //this.mapView.getModel().mapViewPosition.setMapLimit(mBoundingBox);
            this.mapView.getModel().mapViewPosition.setCenter(mBoundingBox.getCenterPoint());
        }
    }

    /**
     * gets the POI to show and calls addPOIMarker
     */
    private void makeSinglePOILayer()
    {
        POI poi = this.getActivity().getIntent().getExtras().getParcelable("poi");
        addPOIMarker(poi);
    }

    /**
     * gets the current bounding box and requests the server to get the pois and routes in that box.
     * @param showPOIs if pois should be shown
     * @param showRoutes if routes should be shown
     */
    private void makeNeighborhoodLayers(boolean showPOIs, boolean showRoutes)
    {
        // center to current location and show position layer
        centerToLocation();
        setPositionLayer(false);

        // get current map section
        mMapSection = MapPositionUtil.getBoundingBox(mapView.getModel().mapViewPosition.getMapPosition(), mapView.getDimension(), mapView.getModel().displayModel.getTileSize());

        // get objects
        Kartenausschnitt kartenausschnitt = mWanderWSClient.getKartenausschnitt(mMapSection, this.getActivity(), true);

        if(kartenausschnitt == null)
            return;

        // show pois
        if(showPOIs) {
            ArrayList<POI> pois = POI.convertWanderPOIs(kartenausschnitt.getPoiListe());
            makePOIResultLayers(pois);
        }

        // show routes
        if(showRoutes) {
            ArrayList<Route> routes = Route.convertWanderRoutes(kartenausschnitt.getRoutenListe());
            makeRouteResultLayers(routes);
        }

        // register for listener
        this.mapView.setMapsectionChangeListener(this);
    }

    /**
     * gets a list of Routes and shows them as icons
     *
     * @param routeResults list of routes
     */
    private void makeRouteResultLayers(ArrayList<Route> routeResults)
    {
        for (int i = 0; i < routeResults.size(); i++)
        {
            Route route = routeResults.get(i);

            if(mRouteMarkers == null || mRouteMarkers.get(route.getId()) == null)
                addRouteLayer(route, false);
        }
    }

    /*private void makeRouteResultLayers()
    {
        ArrayList<Route> routeResults = this.getActivity().getIntent().getExtras().getParcelableArrayList("routeResults");
        makeRouteResultLayers(routeResults);
    }*/

    /**
     * Makes a layer for a route. If only a single route is to be shown, the whole path is drawn. Otherwise only a clickable icon is added.
     * @param route route to draw
     * @param isSingle if full path should be drawn
     */
    private void addRouteLayer(Route route, boolean isSingle)
    {
        RoutePoint startPoint = route.getStartPoint();

        if (startPoint == null)
            return;

        double lat = route.getStartPoint().getLatitude();
        double lon = route.getStartPoint().getLongitude();

        if (lat == 0 || lon == 0)
            return;

        // isSingle: the whole track including start and end icon is rendered
        // !isSingle: only the start point is shown by an icon, no track and no end point
        if (isSingle)
        {
            ArrayList<RoutePoint> routePoints = route.getRoutePoints();
            ArrayList<POI> pois = route.getPOIs();

            if (routePoints == null)
                return;

            Polyline trackline = new Polyline(AppSettings.getRoutePaint(), AndroidGraphicFactory.INSTANCE);
            List<LatLong> list = trackline.getLatLongs();

            for (RoutePoint routePoint : routePoints)
            {
                lat = routePoint.getLatitude();
                lon = routePoint.getLongitude();

                LatLong latlong = new LatLong(lat, lon);

                list.add(latlong);
            }

            mapView.getLayerManager().getLayers().add(trackline);

            // start point
            lat = startPoint.getLatitude();
            lon = startPoint.getLongitude();
            LatLong latLong = new LatLong(lat, lon);
            Drawable drawable = getResources().getDrawable(R.drawable.start);
            Bitmap bitmap = AndroidGraphicFactory.convertToBitmap(drawable);
            Marker marker = new Marker(latLong, bitmap, 0, 0);
            mapView.getLayerManager().getLayers().add(marker);

            // end point
            lat = route.getEndPoint().getLatitude();
            lon = route.getEndPoint().getLongitude();
            latLong = new LatLong(lat, lon);
            drawable = getResources().getDrawable(R.drawable.end);
            bitmap = AndroidGraphicFactory.convertToBitmap(drawable);
            marker = new Marker(latLong, bitmap, 0, 0);
            mapView.getLayerManager().getLayers().add(marker);

            for (POI poi : pois)
            {
                addPOIMarker(poi);
            }


        } else
        {
            LatLong latLong = new LatLong(lat, lon);

            Drawable drawable = getResources().getDrawable(R.drawable.route);
            Bitmap bitmap = AndroidGraphicFactory.convertToBitmap(drawable);

            int offset = bitmap.getHeight() / 2;
            offset = 0 - offset;

            RouteMarker marker = new RouteMarker(latLong, bitmap, 0, offset, this, route);

            mapView.getLayerManager().getLayers().add(marker);

            if(mRouteMarkers == null)
                mRouteMarkers = new HashMap<>();

            mRouteMarkers.put(route.getId(), marker);
        }
    }

    /**
     * adds an icon for a POI. the icon represents the poi type.
     * @param poi
     */
    private void addPOIMarker(POI poi)
    {
        double lat = poi.getLatitude();
        double lon = poi.getLongitude();

        if (lat != 0 && lon != 0)
        {
            LatLong latlong = new LatLong(lat, lon);

            Drawable drawable = MarkerUtil.getDrawableForPOIType(poi.getType(), this.getActivity());
            Bitmap bitmap = AndroidGraphicFactory.convertToBitmap(drawable);

            int offset = bitmap.getHeight() / 2;
            offset = 0 - offset;

            POIMarker marker = new POIMarker(latlong, bitmap, 0, offset, this, poi);

            mapView.getLayerManager().getLayers().add(marker);

            if(mPOIMarkers == null)
                mPOIMarkers = new HashMap<>();

            mPOIMarkers.put(poi.getId(), marker);
        }
    }

    /**
     * gets a list of pois and creates icons for each of them
     * @param poiResults list of pois
     */
    private void makePOIResultLayers(ArrayList<POI> poiResults)
    {
        for (int i = 0; i < poiResults.size(); i++)
        {
            POI poi = poiResults.get(i);

            if(mPOIMarkers == null || mPOIMarkers.get(poi.getId()) == null)
                addPOIMarker(poi);
        }
    }

    /*private void makePOIResultLayers()
    {
        ArrayList<POI> poiResults = this.getActivity().getIntent().getExtras().getParcelableArrayList("poiResults");
        makePOIResultLayers(poiResults);
    }*/

    /**
     * shows the user's position as a layer. In navigation mode, a triangular navigation icon is shown, in normal mode a round icon.
     * @param isNavigation
     */
    private void setPositionLayer(boolean isNavigation)
    {
        Drawable drawable = getResources().getDrawable(R.drawable.ic_maps_indicator_current_position);

        // a marker to show at the position
        if(isNavigation)
        {
            drawable = getResources().getDrawable(R.drawable.icon_navigation);
        }

        Bitmap bitmap = AndroidGraphicFactory.convertToBitmap(drawable);

        // create the overlay and tell it to follow the location
        myLocationOverlay = new MyLocationOverlay(this.getActivity(), this.mapView.getModel().mapViewPosition, bitmap);
        myLocationOverlay.setSnapToLocationEnabled(isNavigation);
        mapView.getLayerManager().getLayers().add(myLocationOverlay);
    }

    /**
     * gets the current location and center's the map to that location.
     */
    private void centerToLocation() {
        LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        String provider = locationManager.getBestProvider(criteria, true);
        String end = null;

        Location location = locationManager.getLastKnownLocation(provider);

        if(location != null)
        {
            LatLong LocationLatLong = new LatLong(location.getLatitude(), location.getLongitude());
            this.mapView.getModel().mapViewPosition.setCenter(LocationLatLong);
            this.mapView.getModel().mapViewPosition.setZoomLevel((byte) 12);
        }
    }

    /**
     * switches the map to offline data
     */
    private void setOfflineLayer()
    {
        if(downloadLayer != null) {
            this.mapView.getLayerManager().getLayers().remove(downloadLayer);
            downloadLayer = null;
        }

        tileRendererLayer = AndroidUtil.createTileRendererLayer(this.tileCache, this.mapView.getModel().mapViewPosition, getMapFile(), InternalRenderTheme.OSMARENDER, false, true);
        this.mapView.getLayerManager().getLayers().add(tileRendererLayer);
    }

    /**
     * switches the map to online data
     */
    private void setOnlineLayer()
    {
        if(tileRendererLayer != null) {
            this.mapView.getLayerManager().getLayers().remove(tileRendererLayer);
            tileRendererLayer = null;
        }
        // using tiles from mapquest
        // http://otile1.mqcdn.com/tiles/1.0.0/map

        OnlineTileSource onlineTileSource = new OnlineTileSource(new String[]{
                "otile1.mqcdn.com", "otile2.mqcdn.com", "otile3.mqcdn.com",
                "otile4.mqcdn.com"}, 80);
        onlineTileSource.setName("MapQuest").setAlpha(false)
                .setBaseUrl("/tiles/1.0.0/map/").setExtension("png")
                .setParallelRequestsLimit(8).setProtocol("http")
                .setTileSize(256).setZoomLevelMax((byte) 19)
                .setZoomLevelMin((byte) 0);

        //this.downloadLayer = new TileDownloadLayer(this.tileCache, this.mapView.getModel().mapViewPosition, OpenStreetMapMapnik.INSTANCE, AndroidGraphicFactory.INSTANCE);
        downloadLayer = new TileDownloadLayer(this.tileCache, this.mapView.getModel().mapViewPosition, onlineTileSource, AndroidGraphicFactory.INSTANCE);

        this.mapView.getLayerManager().getLayers().add(downloadLayer);
    }

    /**
     * gets the offline map data file
     * @return file reference
     */
    public File getMapFile()
    {
        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        return new File(path, AppSettings.MAPFILE);
    }

    @Override
    public void onStop()
    {
        super.onStop();

        if (this.tileRendererLayer != null)
        {
            this.mapView.getLayerManager().getLayers().remove(this.tileRendererLayer);
            this.tileRendererLayer.onDestroy();
        }
    }

    @Override
    public void onPause()
    {
        super.onPause();

        if(this.downloadLayer != null)
            this.downloadLayer.onPause();

        if(this.myLocationOverlay != null)
            this.myLocationOverlay.disableMyLocation();
    }

    @Override
    public void onResume()
    {
        super.onResume();

        if(this.downloadLayer != null)
            this.downloadLayer.onResume();

        if(this.myLocationOverlay != null)
            this.myLocationOverlay.enableMyLocation(true);
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        this.tileCache.destroy();
        this.mapView.getModel().mapViewPosition.destroy();
        this.mapView.destroy();
        AndroidResourceBitmap.clearResourceBitmaps();
    }

    /**
     * Override Method of POIMarker's OnTapListener
     * Handles a tap on a poi by showing it's information on the bottom of the screen.
     * The view is slided in and out.
     *
     * @param poi: POI that was tapped
     */
    @Override
    public void onPOITapped(POI poi)
    {
        if (lastTappedPOI != poi)
            showPOIInfo(poi);
        else
            hidePOIInfo();
    }

    /**
     * Override Method of RouteMarker's OnTapListener
     *
     * @param route: Route that was tapped
     */
    @Override
    public void onRouteTapped(Route route)
    {
        if (lastTappedRoute != route)
            showRouteInfo(route);
        else
            hideRouteInfo();

    }

    /**
     * if the map is dragged, the info layout is hidden.
     */
    @Override
    public void onMapDrag()
    {
        hidePOIInfo();
        hideRouteInfo();
    }

    /**
     * gets the new Map section and requests the pois and routes in that section from server.
     * @param newBoundingBox
     */
    public void onMapsectionChange(BoundingBox newBoundingBox)
    {
        if(AppSettings.isNetworkAvailable(this.getActivity(), !errorShown)){
            errorShown = false;
        }
        else {
            errorShown = true;
            return;
        }

        List<WanderPOI> newWanderPOIs = null;
        List<WanderRoute> newWanderRoutes = null;

        Kartenausschnitt kartenausschnitt = mWanderWSClient.getKartenausschnitt(newBoundingBox, this.getActivity(), false);

        newWanderPOIs = kartenausschnitt.getPoiListe();
        newWanderRoutes = kartenausschnitt.getRoutenListe();

        makePOIResultLayers(POI.convertWanderPOIs(newWanderPOIs));
        makeRouteResultLayers(Route.convertWanderRoutes(newWanderRoutes));
    }

    /*@Override
    public void onMapsectionChange(BoundingBox newBoundingBox)
    {
        if(AppSettings.isNetworkAvailable(this.getActivity(), !errorShown)){
            errorShown = false;
        }
        else {
            errorShown = true;
            return;
        }
        
        
        List<WanderPOI> newWanderPOIs = null;
        List<WanderRoute> newWanderRoutes = null;

        BoundingBox wholeBox = null, latitudeBox = null, longitudeBox = null;
        Kartenausschnitt wholeSection = null, latitudeSection = null, longitudeSection = null;

        //TODO: some sort of memory balancing: too many layers or too much zoomed out?
        //TODO: was this area already covered?

        if(mMapSection != null) {

            if(mMapSection.equals(newBoundingBox))
                return;

            System.out.println("got section change");

            // check for zoom in -> no call
            if (newBoundingBox.maxLatitude < mMapSection.maxLatitude
                    && newBoundingBox.minLatitude > mMapSection.minLatitude
                    && newBoundingBox.maxLongitude < mMapSection.maxLongitude
                    && newBoundingBox.minLongitude > mMapSection.minLongitude) {

                return;
            }

            // check for zoom out -> only one call with whole box
            if (newBoundingBox.maxLatitude > mMapSection.maxLatitude
                    && newBoundingBox.minLatitude < mMapSection.minLatitude
                    && newBoundingBox.maxLongitude > mMapSection.maxLongitude
                    && newBoundingBox.minLongitude < mMapSection.minLongitude) {

                wholeBox = new BoundingBox(newBoundingBox.minLatitude, newBoundingBox.minLongitude, newBoundingBox.maxLatitude, newBoundingBox.maxLongitude);
            }

            double maxLatitude, minLatitude, minLongitude, maxLongitude;

            // calc "Latitude-Box"
            maxLatitude = (newBoundingBox.maxLatitude > mMapSection.maxLatitude) ? newBoundingBox.maxLatitude : mMapSection.minLatitude;
            minLatitude = (newBoundingBox.minLatitude < mMapSection.minLatitude) ? newBoundingBox.minLatitude : mMapSection.maxLatitude;
            maxLongitude = (newBoundingBox.maxLongitude > mMapSection.maxLongitude) ? mMapSection.maxLongitude : newBoundingBox.maxLongitude;
            minLongitude = (newBoundingBox.minLongitude < mMapSection.minLongitude) ? mMapSection.minLongitude : newBoundingBox.minLongitude;

            latitudeBox = new BoundingBox(minLatitude, minLongitude, maxLatitude, maxLongitude);

            // calc "Longitude-Box"
            maxLatitude = newBoundingBox.maxLatitude;
            minLatitude = newBoundingBox.minLatitude;
            maxLongitude = (newBoundingBox.maxLongitude > mMapSection.maxLongitude) ? newBoundingBox.maxLongitude : mMapSection.minLongitude;
            minLongitude = (newBoundingBox.minLongitude < mMapSection.minLongitude) ? newBoundingBox.minLongitude : mMapSection.maxLongitude;

            longitudeBox = new BoundingBox(minLatitude, minLongitude, maxLatitude, maxLongitude);

        }
        else {
            wholeBox = newBoundingBox;
        }

        // set new BoundingBox as old one
        mMapSection = newBoundingBox;

        if (wholeBox != null)
        {
            wholeSection = mWanderWSClient.getKartenausschnitt(wholeBox, this.getActivity(), false);

            if(wholeSection != null) {
                newWanderPOIs = wholeSection.getPoiListe();
                newWanderRoutes = wholeSection.getRoutenListe();
            }
        }
        else
        {
            latitudeSection = mWanderWSClient.getKartenausschnitt(latitudeBox, this.getActivity(), false);
            longitudeSection = mWanderWSClient.getKartenausschnitt(longitudeBox, this.getActivity(), false);

            if( latitudeSection != null && longitudeSection != null) {
                newWanderPOIs = latitudeSection.getPoiListe();
                newWanderPOIs.addAll(longitudeSection.getPoiListe());

                newWanderRoutes = latitudeSection.getRoutenListe();
                newWanderRoutes.addAll(longitudeSection.getRoutenListe());
            }
        }

        //TODO: check if layers already exist

        makePOIResultLayers(POI.convertWanderPOIs(newWanderPOIs));
        makeRouteResultLayers(Route.convertWanderRoutes(newWanderRoutes));
    }*/

    /**
     * shows an info layout for a poi
     * @param poi poi to show info for
     */
    private void showPOIInfo(POI poi)
    {
        lastTappedPOI = poi;

        View slide_view = this.getActivity().findViewById(R.id.poiInfoLayout);
        ImageView image_poi = (ImageView) this.getActivity().findViewById(R.id.image_poi);
        TextView text_title = (TextView) this.getActivity().findViewById(R.id.text_title);

        slide_view.setVisibility(View.VISIBLE);
        text_title.setText(poi.getName());

        // shows drawable. if none is found, shows placeholder
        if (poi.getDrawableId() != 0)
            image_poi.setImageResource(poi.getDrawableId());
        else
            image_poi.setImageResource(R.drawable.logo_square_trans);

        Animation slideAnimation = AnimationUtils.loadAnimation(this.getActivity(), R.anim.slide_up);
        slide_view.startAnimation(slideAnimation);

        slide_view.setOnClickListener(this);
    }

    /**
     * shows an info for a route
     * @param route route to show info for
     */
    private void showRouteInfo(Route route)
    {
        lastTappedRoute = route;

        View slide_view = this.getActivity().findViewById(R.id.routeInfoLayout);
        ImageView image_route = (ImageView) this.getActivity().findViewById(R.id.image_poi);
        TextView text_title = (TextView) this.getActivity().findViewById(R.id.text_routetitle);
        TextView text_duration = (TextView) this.getActivity().findViewById(R.id.durationText);
        TextView text_length = (TextView) this.getActivity().findViewById(R.id.lengthText);

        slide_view.setVisibility(View.VISIBLE);
        text_title.setText(route.getName());

        // shows drawable. if none is found, shows placeholder
        if (route.getDrawableId() != 0)
            image_route.setImageResource(route.getDrawableId());
        else
            image_route.setImageResource(R.drawable.logo_square_trans);

        text_length.setText(route.getLengthString());
        text_duration.setText(route.getDurationString());

        Animation slideAnimation = AnimationUtils.loadAnimation(this.getActivity(), R.anim.slide_up);
        slide_view.startAnimation(slideAnimation);

        slide_view.setOnClickListener(this);
    }

    /**
     * hides the info layout
     */
    private void hidePOIInfo()
    {
        if (lastTappedPOI != null)
        {
            lastTappedPOI = null;

            View slide_view = this.getActivity().findViewById(R.id.poiInfoLayout);

            Animation slideAnimation = AnimationUtils.loadAnimation(this.getActivity(), R.anim.slide_down_out);
            slide_view.startAnimation(slideAnimation);
            slide_view.setVisibility(View.INVISIBLE);

            slide_view.setOnClickListener(null);
        }
    }

    /**
     * hides the info layout
     */
    private void hideRouteInfo()
    {
        if (lastTappedRoute != null)
        {
            lastTappedRoute = null;

            View slide_view = this.getActivity().findViewById(R.id.routeInfoLayout);

            Animation slideAnimation = AnimationUtils.loadAnimation(this.getActivity(), R.anim.slide_down_out);
            slide_view.startAnimation(slideAnimation);
            slide_view.setVisibility(View.INVISIBLE);

            slide_view.setOnClickListener(null);
        }
    }

    @Override
    public void onClick(View v)
    {
        if (lastTappedPOI != null)
            mPOIListener.onPOIInfoClick(lastTappedPOI);
        else if (lastTappedRoute != null)
            mRouteListener.onRouteInfoClick(lastTappedRoute);
    }

    /**
     * zooms for new dimensions
     * @param width
     * @param height
     * @param oldWidth
     * @param oldHeight
     */
    @Override
    public void onSizeChanged(int width, int height, int oldWidth, int oldHeight)
    {
        if (width == oldWidth && height == oldHeight)
            return;

        if (mBoundingBox == null)
            return;

        byte zoomLevel = LatLongUtils.zoomForBounds(this.mapView.getDimension(), mBoundingBox, mapView.getModel().displayModel.getTileSize());
        this.mapView.getModel().mapViewPosition.setZoomLevel(zoomLevel);

        if(myLocationOverlay != null)
            myLocationOverlay.enableMyLocation(true);
    }

    /**
     * switches back to online mode. currently has to restart the activity...
     */
    @Override
    public void goOnline() {
        // TODO: find out why setOnlineLayer doesn't work

        getActivity().finish();
        getActivity().startActivity(getActivity().getIntent());
    }

    /**
     * switches to offline mode
     */
    @Override
    public void goOffline() {
        setOfflineLayer();
    }

    //listener for the embedding activity

    public interface OnPOIInfoClickListener
    {
        void onPOIInfoClick(POI poi);
    }

    public interface OnRouteInfoClickListener
    {
        void onRouteInfoClick(Route route);
    }
}