package regionale.whs.wanderapp.tools.comparators;

import java.util.Comparator;

import regionale.whs.wanderapp.tools.SelectableString;

/**
 * sorts selectableStrings by their string  value
 *
 * Created by Stephan on 27.11.2014.
 */
public class SortTypes implements Comparator<SelectableString>
{

    @Override
    public int compare(SelectableString lhs, SelectableString rhs)
    {
        return lhs.getString().compareTo(rhs.getString());
    }
}
