package regionale.whs.wanderapp.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.TextView;

import regionale.whs.wanderapp.R;
import regionale.whs.wanderapp.activities.NewCommentActivity;
import regionale.whs.wanderapp.activities.POIDetailActivity;
import regionale.whs.wanderapp.activities.RouteDetailActivity;
import regionale.whs.wanderapp.service.WanderWSClient;
import regionale.whs.wanderapp.wspojos.WanderKommentar;

/**
 * Fragment showing a list of comments.
 *
 * Created by Stephan on 19.02.2015.
 */
public class CommentsFragment extends Fragment implements AbsListView.OnItemClickListener, Button.OnClickListener {
    private OnCommentSelectedListener listener;

    /**
     * The fragment's ListView/GridView.
     */
    private AbsListView mListView;

    /**
     * The Adapter which will be used to populate the ListView/GridView with
     * Views.
     */
    private ListAdapter mAdapter;
    private POIDetailActivity mPOIActivity;
    private RouteDetailActivity mRouteActivty;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public CommentsFragment()
    {
    }

    public static CommentsFragment newInstance(ArrayAdapter adapter, POIDetailActivity poiActivity)
    {
        CommentsFragment fragment = new CommentsFragment();
        fragment.setListAdapter(adapter);
        fragment.setmPOIActivity(poiActivity);
        return fragment;
    }

    public static CommentsFragment newInstance(ArrayAdapter adapter, RouteDetailActivity routeActivity)
    {
        CommentsFragment fragment = new CommentsFragment();
        fragment.setListAdapter(adapter);
        fragment.setmRouteActivty(routeActivity);
        return fragment;
    }

    public POIDetailActivity getmPOIActivity() {
        return mPOIActivity;
    }

    public void setmPOIActivity(POIDetailActivity mPOIActivity) {
        this.mPOIActivity = mPOIActivity;
    }

    public RouteDetailActivity getmRouteActivty() {
        return mRouteActivty;
    }

    public void setmRouteActivty(RouteDetailActivity mRouteActivty) {
        this.mRouteActivty = mRouteActivty;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_commentlist, container, false);

        // Set the adapter
        mListView = (AbsListView) view.findViewById(android.R.id.list);
        ((AdapterView<ListAdapter>) mListView).setAdapter(mAdapter);

        // Set OnItemClickListener so we can be notified on item clicks
        mListView.setOnItemClickListener(this);

        return view;
    }



    @Override
    public void onStart() {
        super.onStart();

        // if someone is logged in
        if(WanderWSClient.getInstance().getCurrentUser() != null)
        {
            Button newCommentButton = (Button) this.getActivity().findViewById(R.id.new_comment_button);
            newCommentButton.setVisibility(View.VISIBLE);
            newCommentButton.setOnClickListener(this);
        }
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        try
        {
            listener = (OnCommentSelectedListener) activity;
        } catch (ClassCastException e)
        {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        listener = null;
    }

    // handling clicks
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        if (null != listener)
        {
            // Notify the activity that an item has been selected.
            WanderKommentar comment;
            comment = (WanderKommentar) mAdapter.getItem(position);
            listener.onCommentSelected(comment);
        }
    }

    /**
     * The default content for this Fragment has a TextView that is shown when
     * the list is empty. If you would like to change the text, call this method
     * to supply the text it should use.
     */
    public void setEmptyText(CharSequence emptyText)
    {
        View emptyView = mListView.getEmptyView();

        if (emptyText instanceof TextView)
        {
            ((TextView) emptyView).setText(emptyText);
        }
    }

    public void setListAdapter(ArrayAdapter<?> adapter)
    {
        mAdapter = adapter;
    }

    // calls newCommentActivity
    @Override
    public void onClick(View v)
    {
        Intent intent = new Intent(this.getActivity(), NewCommentActivity.class);

        boolean isPOI = (mPOIActivity != null);
        intent.putExtra("isPOI", isPOI);

        if(isPOI) {
            intent.putExtra("poi", mPOIActivity.getPOI());
        }
        else {
            intent.putExtra("route", mRouteActivty.getRoute());
        }

        startActivity(intent);
    }

    // Interface for handling item selection in DetailActivity
    public interface OnCommentSelectedListener
    {
        void onCommentSelected(WanderKommentar comment);
    }
}
