package regionale.whs.wanderapp.tools;

import android.location.Location;

/**
 * Utility methods for various coordinate-related calculations
 *
 * Created by Stephan on 14.11.2014.
 */
public class GeoCalc
{
    /*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    /*::                                                                         :*/
    /*::  This routine calculates the distance between two points (given the     :*/
    /*::  latitude/longitude of those points).                                   :*/
    /*::                                                                         :*/
    /*::  Definitions:                                                           :*/
    /*::    South latitudes are negative, east longitudes are positive           :*/
    /*::                                                                         :*/
    /*::  Passed to function:                                                    :*/
    /*::    lat1, lon1 = Latitude and Longitude of point 1 (in decimal degrees)  :*/
    /*::    lat2, lon2 = Latitude and Longitude of point 2 (in decimal degrees)  :*/
    /*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/

    public static double calcDistance(double lat1, double lon1, double lat2, double lon2)
    {
        double earth_radius = 6371000; // in metres
        double phi1 = deg2rad(lat1);
        double phi2 = deg2rad(lat2);
        double deltaphi = deg2rad(lat2-lat1);
        double deltalamba = deg2rad(lon2 - lon1);

        double a = Math.sin(deltaphi/2) * Math.sin(deltaphi/2) + Math.cos(phi1) * Math.cos(phi2) * Math.sin(deltalamba/2) * Math.sin(deltalamba/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

        return earth_radius * c;
    }

    // overloaded function that uses locations
    public static double calcDistance(Location loc1, Location loc2)
    {
        return calcDistance(loc1.getLatitude(), loc1.getLongitude(), loc2.getLatitude(), loc2.getLongitude());
    }

    // overloaded function that uses one location
    public static double calcDistance(Location loc1, double lat2, double lon2)
    {
        return calcDistance(loc1.getLatitude(), loc1.getLongitude(), lat2, lon2);
    }

    // overloaded function that uses one location
    public static double calcDistance(double lat1, double lon1, Location loc2)
    {
        return calcDistance(lat1, lon1, loc2.getLatitude(), loc2.getLongitude());
    }

    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    /*::  This function calculates the heading between two coordinates ::::::::::::::*/
    /*::                                                                           ::*/
    /*::  ? = atan2( sin ?? ? cos ?2 , cos ?1 ? sin ?2 ? sin ?1 ? cos ?2 ? cos ??) ::*/
    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    public static float calcHeading(double lat1, double lon1, double lat2, double lon2)
    {
        float heading = -1;

        double phi1 = deg2rad(lat1);
        double phi2 = deg2rad(lat2);
        double deltalamba = deg2rad(lon2 - lon1);

        double y = Math.sin(deltalamba) * Math.cos(phi2);
        double x = Math.cos(phi1)*Math.sin(phi2) - Math.sin(phi1) * Math.cos(phi2) * Math.cos(deltalamba);
        double brng = rad2deg(Math.atan2(y, x));

        heading = (float)brng;

        return heading;
    }

    // overloaded function that uses location (more convenient calling in most cases
    public static double calcHeading(Location loc1, Location loc2) {
        return calcHeading(loc1.getLatitude(), loc1.getLongitude(), loc2.getLatitude(), loc2.getLongitude());
    }

    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    /*::  This function converts decimal degrees to radians             :*/
    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    private static double deg2rad(double deg)
    {
        return (deg * Math.PI / 180.0);
    }

    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    /*::  This function converts radians to decimal degrees             :*/
    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    private static double rad2deg(double rad)
    {
        return (rad * 180 / Math.PI);
    }
}
