package regionale.whs.wanderapp.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TableLayout;
import android.widget.TextView;

import regionale.whs.wanderapp.R;
import regionale.whs.wanderapp.objects.Route;
import regionale.whs.wanderapp.objects.enums.RouteType;
import regionale.whs.wanderapp.tools.converter.CoordinateStringConverter;
import regionale.whs.wanderapp.tools.converter.RouteTypeStringConverter;

/**
 * fragment that shows the data of a route
 */
public class RouteDetailMainFragment extends Fragment
{
    private Route mRoute;

    public RouteDetailMainFragment()
    {
        // Required empty public constructor
    }

    public static RouteDetailMainFragment newInstance(Route route)
    {
        RouteDetailMainFragment fragment = new RouteDetailMainFragment();
        fragment.setRoute(route);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_routedetail_main, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        showData();
    }

    public Route getRoute()
    {
        return mRoute;
    }

    public void setRoute(Route route)
    {
        this.mRoute = route;
    }

    /**
     * populates the layout's data fields
     */
    private void showData()
    {
        TextView textView;

        // image
        ImageView imageView = (ImageView) this.getActivity().findViewById(R.id.image);
        int id = mRoute.getDrawableId();
        if (id != 0)
        {
            imageView.setImageResource(mRoute.getDrawableId());
        } else
        {
            imageView.setVisibility(View.GONE);
        }

        // rating
        RatingBar ratingBar = (RatingBar) this.getActivity().findViewById(R.id.ratingBar);
        double rating = mRoute.getRating();
        if (rating > 0)
        {
            ratingBar.setNumStars((int) rating);
            textView = (TextView) this.getActivity().findViewById(R.id.text_rating);
            textView.setText(Route.getRatingString(mRoute, this.getActivity()));
        } else
        {
            LinearLayout ratingLayout = (LinearLayout) this.getActivity().findViewById(R.id.layout_rating);
            ratingLayout.setVisibility(View.GONE);
        }

        // title and name
        String name = mRoute.getName();
        if (!name.equals(""))
        {
            this.getActivity().setTitle(name);
            textView = (TextView) this.getActivity().findViewById(R.id.name);
            textView.setText(name);
        }

        // type
        textView = (TextView) this.getActivity().findViewById(R.id.type);
        RouteType type = mRoute.getType();
        if (type != null)
        {
            RouteTypeStringConverter converter = new RouteTypeStringConverter(this.getActivity());
            textView.setText(converter.convertRouteTypeToString(type));

        } else
            textView.setVisibility(View.GONE);

        // number of POIs
        textView = (TextView) this.getActivity().findViewById(R.id.text_poiscount);
        int count = mRoute.getPOIs().size();
        if (count > 0)
        {
            textView.setText(getText(R.string.poiscount).toString() + " " + count);

        } else
            textView.setVisibility(View.GONE);


        // description
        textView = (TextView) this.getActivity().findViewById(R.id.description);
        String description = mRoute.getDescription();
        if (!description.equals(""))
        {
            textView.setText(description);
        } else
        {
            textView.setVisibility(View.GONE);
        }

        // overview
        textView = (TextView) this.getActivity().findViewById(R.id.text_duration);
        String duration = mRoute.getDurationString();
        if (!duration.equals(""))
        {
            textView.setText(duration);
            textView = (TextView) this.getActivity().findViewById(R.id.text_length);
            textView.setText(mRoute.getLengthString());
        } else
        {
            TableLayout tableLayout = (TableLayout) this.getActivity().findViewById(R.id.table_overview);
            tableLayout.setVisibility(View.GONE);
        }

        textView = (TextView) this.getActivity().findViewById(R.id.text_geo_start);
        TextView textViewEnd = (TextView) this.getActivity().findViewById(R.id.text_geo_finish);
        String start = CoordinateStringConverter.routePointToDMS(mRoute.getStartPoint());
        String end = CoordinateStringConverter.routePointToDMS(mRoute.getEndPoint());
        if (!start.equals("") && !end.equals(""))
        {
            textView.setText(start);
            textViewEnd.setText(end);
        } else
        {
            textView.setVisibility(View.GONE);
            textViewEnd.setVisibility(View.GONE);
            textView = (TextView) this.getActivity().findViewById(R.id.text_startpoint);
            textView.setVisibility(View.GONE);
            textView = (TextView) this.getActivity().findViewById(R.id.text_finish);
            textView.setVisibility(View.GONE);
            textView = (TextView) this.getActivity().findViewById(R.id.title_start_finish);
            textView.setVisibility(View.GONE);
            Button opnvButton = (Button) this.getActivity().findViewById(R.id.button_opnv);
            opnvButton.setVisibility(View.GONE);
        }

        // accessibility info
        textView = (TextView) this.getActivity().findViewById(R.id.text_accessibility);
        String accessibility_info = mRoute.getAccessibilityInfo();
        if (!accessibility_info.equals(""))
        {
            textView.setText(accessibility_info);
        } else
        {
            textView.setVisibility(View.GONE);
            TextView title = (TextView) this.getActivity().findViewById(R.id.title_accessibility);
            title.setVisibility(View.GONE);
        }

        // transportation info
        textView = (TextView) this.getActivity().findViewById(R.id.text_transportation);
        String transportation_info = mRoute.getTransportationInfo();
        if (!transportation_info.equals(""))
        {
            textView.setText(transportation_info);
        } else
        {
            textView.setVisibility(View.GONE);
            TextView title = (TextView) this.getActivity().findViewById(R.id.title_transportation);
            title.setVisibility(View.GONE);
        }

        // equipment info
        textView = (TextView) this.getActivity().findViewById(R.id.text_equipment);
        String equipment_info = mRoute.getEquipmentInfo();
        if (!equipment_info.equals(""))
        {
            textView.setText(equipment_info);
        } else
        {
            textView.setVisibility(View.GONE);
            TextView title = (TextView) this.getActivity().findViewById(R.id.title_equipment);
            title.setVisibility(View.GONE);
        }

        // TODO: setup social buttons
        // for now, they are just deleted
        LinearLayout social_buttons = (LinearLayout) this.getActivity().findViewById(R.id.layout_social);
        social_buttons.setVisibility(View.GONE);
    }
}
