package regionale.whs.wanderapp.tools.converter;

import android.content.Context;

import regionale.whs.wanderapp.R;

/**
 * Since the android enum object doesn't allow values to be stored with the keys, I had to implement this converter class.
 * It holds a map of key/value-pairs that associate a POIType with the corresponding R.string id.
 * By giving a context to the constructor, this association is also context-aware and by that localized.
 */
public class BooleanStringConverter
{

    private String trueString;
    private String falseString;

        public BooleanStringConverter(Context context)
    {
        trueString = context.getString(R.string.trueString);
        falseString = context.getString(R.string.falseString);
    }

        public String convertBooleanToString(boolean bool)
    {
        return (bool) ? trueString : falseString;
    }
}
