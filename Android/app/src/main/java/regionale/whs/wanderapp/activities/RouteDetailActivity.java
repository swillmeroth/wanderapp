package regionale.whs.wanderapp.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.view.View;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import regionale.whs.wanderapp.R;
import regionale.whs.wanderapp.adapter.CommentsAdapter;
import regionale.whs.wanderapp.adapter.RouteDetailTabsPagerAdapter;
import regionale.whs.wanderapp.fragments.CommentsFragment;
import regionale.whs.wanderapp.fragments.MapFragment;
import regionale.whs.wanderapp.objects.POI;
import regionale.whs.wanderapp.objects.Route;
import regionale.whs.wanderapp.objects.enums.MapType;
import regionale.whs.wanderapp.service.WanderWSClient;
import regionale.whs.wanderapp.tools.AppSettings;
import regionale.whs.wanderapp.tools.SwipeToggleViewPager;
import regionale.whs.wanderapp.wspojos.WanderKommentar;

/**
 * shows the route's details in three tabs.
 * tab 1: main data
 * tab 2: comments
 * tab 3: map of the route
 */
public class RouteDetailActivity extends StandardActivity implements ActionBar.TabListener, CommentsFragment.OnCommentSelectedListener, MapFragment.OnPOIInfoClickListener
{
    private Route mRoute;
    private List<WanderKommentar> mComments;
    private SwipeToggleViewPager mViewPager;
    private final int MAP_TAB_POSITION = 2;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_routedetail);

        mRoute = this.getIntent().getParcelableExtra("route");
        this.getIntent().putExtra("maptype", MapType.SINGLEROUTE.toString());
        this.getIntent().putExtra("routeDetail", true);

        // Set up the action bar.
        final ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        setTitle(mRoute.getName());

        getComments();

        // Create the adapter that will return a fragment for each tab
        RouteDetailTabsPagerAdapter routeDetailTabsPagerAdapter = new RouteDetailTabsPagerAdapter(getSupportFragmentManager(), mRoute, new CommentsAdapter(this, mComments), this);

        // Set up the ViewPager with the sections adapter.
        mViewPager = (SwipeToggleViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(routeDetailTabsPagerAdapter);
        mViewPager.setOffscreenPageLimit(routeDetailTabsPagerAdapter.getCount() - 1);

        // When swiping between different sections, select the corresponding tab
        mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener()
        {
            @Override
            public void onPageSelected(int position)
            {
                actionBar.setSelectedNavigationItem(position);
            }
        });

        MapFragment mMapFragment = (MapFragment) routeDetailTabsPagerAdapter.getItem(MAP_TAB_POSITION);
        mMapFragment.setOnPOIInfoClickListener(this);

        actionBar.addTab(actionBar.newTab().setText(R.string.title_routedetail_main).setTabListener(this));
        actionBar.addTab(actionBar.newTab().setText(R.string.comments).setTabListener(this));
        actionBar.addTab(actionBar.newTab().setText(R.string.title_routedetail_map).setTabListener(this));
        //actionBar.addTab(actionBar.newTab().setText(R.string.title_routedetail_profile).setTabListener(this));
    }

    public Route getRoute() {
        return mRoute;
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction)
    {
        // When the given tab is selected, switch to the corresponding page in
        // the ViewPager.
        mViewPager.setCurrentItem(tab.getPosition());

        // if position is 2 (map), swipe is disabled, otherwise enabled
        mViewPager.setSwipeEnabled(tab.getPosition() != MAP_TAB_POSITION);
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction)
    {
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction)
    {
    }

    /**
     * If the "get directions" button is clicked, a geo-URI with the Routes start point location is created and an action intent is called.
     * By this, all installed applications that have registered themselves for geo-Uris (e.g. Google Maps) can be opened to get directions to the POI.
     * @param view clicked button
     */
    public void onDirectionsButton(View view)
    {
        String uri;

        uri = String.format(Locale.ENGLISH, "geo:0,0?q=%f,%f(" + mRoute.getName() + ")", mRoute.getStartPoint().getLatitude(), mRoute.getStartPoint().getLongitude());

        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        this.startActivity(intent);
    }

    /**
     * gets the comments of the route
     */
    private void getComments()
    {
        // TODO: add a button to view all comments or just show every comment?

        int count = mRoute.getCommentCount();
        int max = (count > AppSettings.MAX_COMMENTS) ? count : AppSettings.MAX_COMMENTS;

        WanderWSClient client = WanderWSClient.getInstance();
        mComments = client.getRouteKommentare(mRoute.getId(), 1, max + 1, this, true);


        if(mComments == null) {
            mComments = new ArrayList<>();
        }
    }

    @Override
    public void onCommentSelected(WanderKommentar comment)
    {
        // TODO: what should happen when a comment is selected?
    }

    /**
     * opens the POIDetailActivity of the clicked POI
     * @param poi
     */
    @Override
    public void onPOIInfoClick(POI poi)
    {
        Intent intent = new Intent(this, POIDetailActivity.class);
        intent.putExtra("poi", poi);
        startActivity(intent);
    }

    /**
     * starts the navigation of the route
     * @param view
     */
    public void startNavigation(View view)
    {
        Intent intent = new Intent(this, NavigationActivity.class);
        intent.putExtra("route", this.getIntent().getParcelableExtra("route"));
        startActivity(intent);
    }
}
