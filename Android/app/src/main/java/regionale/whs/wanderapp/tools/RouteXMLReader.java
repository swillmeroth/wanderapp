package regionale.whs.wanderapp.tools;

import android.content.Context;
import android.content.res.XmlResourceParser;
import android.util.Xml;

import org.joda.time.Period;
import org.mapsforge.core.model.BoundingBox;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;

import regionale.whs.wanderapp.objects.Route;
import regionale.whs.wanderapp.objects.RoutePoint;

/**
 * Reader that reads a result XML from ORS
 *
 * Created by Stephan on 18.02.2015.
 */

enum CurrentTagType
{
    DURATION, GEOMETRY, ROUTEPOINT, BOUNDINGBOX, BOUNDINGPOS, NONE
}

public class RouteXMLReader
{
    public static Route getRouteFromAssets(Context context, String path)
    {
        Route route = new Route();
        XmlPullParser parser = Xml.newPullParser();

        try
        {
            InputStream in = context.getAssets().open(path);
            parser.setInput(in, null);
            return getRouteFromXML(parser);

        } catch (IOException | XmlPullParserException e)
        {
            e.printStackTrace();
            return null;
        }
    }

    public static Route getRouteFromResource(Context context, int id)
    {
        XmlResourceParser parser = context.getResources().getXml(id);

        if (parser == null)
            return null;

        return getRouteFromXML(parser);
    }

    private static Route getRouteFromXML(XmlPullParser parser)
    {
        Route route = new Route();

        double lon_min = 1000;
        double lon_max = 1000;
        double lat_min = 1000;
        double lat_max = 1000;

        int eventType = 0;
        try
        {
            CurrentTagType currentTagType = null;
            eventType = parser.getEventType();

            while (eventType != XmlPullParser.END_DOCUMENT)
            {
                if (eventType == XmlPullParser.START_TAG)
                {
                    if (parser.getName().equals("TotalDistance"))
                    {
                        // expects all values to be in km, not miles or something else
                        double length_double = Double.parseDouble(parser.getAttributeValue("", "value"));
                        length_double *= 1000;
                        int length = (int) length_double;
                        route.setLength(length);
                    } else if (parser.getName().equals("TotalTime"))
                    {
                        currentTagType = CurrentTagType.DURATION;
                    } else if (parser.getName().equals("BoundingBox"))
                    {
                        currentTagType = CurrentTagType.BOUNDINGBOX;
                    } else if (parser.getName().equals("RouteGeometry"))
                    {
                        currentTagType = CurrentTagType.GEOMETRY;
                    } else if (parser.getName().equals("pos") && currentTagType == CurrentTagType.BOUNDINGBOX)
                    {
                        currentTagType = CurrentTagType.BOUNDINGPOS;
                    } else if (parser.getName().equals("pos") && currentTagType == CurrentTagType.GEOMETRY)
                    {
                        currentTagType = CurrentTagType.ROUTEPOINT;
                    }
                } else if (eventType == XmlPullParser.END_TAG)
                {
                    if (currentTagType == CurrentTagType.ROUTEPOINT && parser.getName().equals("pos"))
                        currentTagType = CurrentTagType.GEOMETRY;
                    else if (currentTagType == CurrentTagType.BOUNDINGPOS && parser.getName().equals("pos"))
                        currentTagType = CurrentTagType.BOUNDINGBOX;
                    else
                        currentTagType = null;
                } else if (eventType == XmlPullParser.TEXT)
                {
                    if (currentTagType == CurrentTagType.DURATION)
                    {
                        // this uses joda time
                        // TODO: check if lib joda is still needed at release time

                        String duration_string = parser.getText();
                        Period period = Period.parse(duration_string);
                        route.setDuration(period.toStandardMinutes().getMinutes());
                    } else if (currentTagType == CurrentTagType.ROUTEPOINT || currentTagType == CurrentTagType.BOUNDINGPOS)
                    {
                        String pos_str = parser.getText();
                        String pos_arr[] = pos_str.split(" ");

                        if (pos_arr.length == 2)
                        {
                            String lon_str = pos_arr[0];
                            String lat_str = pos_arr[1];

                            double lat = Double.parseDouble(lat_str);
                            double lon = Double.parseDouble(lon_str);

                            if (currentTagType == CurrentTagType.ROUTEPOINT)
                            {
                                route.addRoutePoint(new RoutePoint(lat, lon));
                            } else
                            {
                                if (lat < lat_min)
                                    lat_min = lat;
                                else
                                    lat_max = lat;

                                if (lon < lon_min)
                                    lon_min = lon;
                                else
                                    lon_max = lon;
                            }
                        }
                    }
                }

                eventType = parser.next();
            }

        } catch (XmlPullParserException | IOException e)
        {
            e.printStackTrace();
            return null;
        }

        route.setStartPoint(route.getRoutePoints().get(0));
        route.setEndPoint(route.getRoutePoints().get(route.getRoutePoints().size() - 1));

        BoundingBox box = new BoundingBox(lat_min, lon_min, lat_max, lon_max);
        route.setBoundingBox(box);

        return route;
    }
}
