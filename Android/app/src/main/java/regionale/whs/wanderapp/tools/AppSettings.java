package regionale.whs.wanderapp.tools;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import org.mapsforge.core.graphics.Paint;
import org.mapsforge.core.graphics.Style;
import org.mapsforge.map.android.graphics.AndroidGraphicFactory;

import java.text.DecimalFormat;
import java.util.Locale;

import regionale.whs.wanderapp.R;

/**
 * This is the place to store application-wide (final) settings in one place without using the strings xml or some other localized values.
 * For dynamic content the Android preferences and intent are used.
 * <p/>
 * Created by Stephan on 22.11.2014.
 */
public final class AppSettings
{
    //-------------------------------------------------//
    //                  MISC SETTINGS                  //
    //-------------------------------------------------//

    public static final int MAX_COMMENTS = 20;

    //-------------------------------------------------//
    //                 WANDER SETTINGS                 //
    //-------------------------------------------------//


    public static final String WANDER_ROOT_URI = "http://10.0.2.2:8080/WanderWIKIa";
    public static final String WANDER_REST_URI = "/webresources/";
    public static final String WANDER_BASE_URI = WANDER_ROOT_URI + WANDER_REST_URI;
    public static final String WANDER_MAP_URI = "http://193.175.86.175:8080/ShopAssistant/resources/map/complete.map";

    public static final String WANDER_POITYPE_ABBEY = "abbey";
    public static final String WANDER_POITYPE_CHAPEL = "chapel";
    public static final String WANDER_POITYPE_CHURCH = "church";
    public static final String WANDER_POITYPE_CROSS = "cross";
    public static final String WANDER_POITYPE_GRAVEYARD = "graveyard";
    public static final String WANDER_POITYPE_JEWISH_GRAVEYARD = "jewish_graveyard";
    public static final String WANDER_POITYPE_MONUMENT = "monument";
    public static final String WANDER_POITYPE_STATUE = "statue";

    public static final String WANDER_WALKINGSPEED_SLOW = "langsam";
    public static final String WANDER_WALKINGSPEED_NORMAL = "normal";
    public static final String WANDER_WALKINGSPEED_FAST = "schnell";

    public static final String WANDER_INTEREST_HISTORY = "historisch";
    public static final String WANDER_INTEREST_RELIGION = "religi�s";
    public static final String WANDER_INTEREST_GEOGRAPHY = "geographisch";
    public static final String WANDER_INTEREST_CULTURAL = "kulturell";

    public static final String WANDER_LANGUAGE_GERMAN = "de";
    public static final String WANDER_LANGUAGE_ENGLISH = "en";

    //-------------------------------------------------//
    //                 FORMAT SETTINGS                 //
    //-------------------------------------------------//

    // localized Date formats
    public static final String DATEFORMAT_DEFAULT = "dd.MM.yyyy HH:mm";
    public static final String DATEFORMAT_DE = "dd.MM.yyyy HH:mm";
    public static final String DATEFORMAT_US = "dd/MM/yyyy hh:mm a";
    public static final String DATEFORMAT_UK = "MM/dd/yyyy hh:mm a";
    public static final DecimalFormat RATING_FORMAT = new DecimalFormat("0.00");

    public static String getDistanceString(int distance, Locale locale)
    {
        StringBuilder stringBuilder = new StringBuilder();

        /*
        *   depending on the size of the distance, it is converted to km
        *
        *   0-999:      output in m
        *   1000-9999:  output in km with 2 decimal places
        *   >10000:     output in km without decimal places
        *
        */
        if (distance > 10000)
        {
            distance = distance / 1000;
            stringBuilder.append(distance);
            stringBuilder.append(" km");
        } else if (distance > 1000)
        {
            float distance_float = (float) distance / 1000;
            stringBuilder.append(String.format(locale, "%.1f", distance_float));
            stringBuilder.append(" km");
        } else
        {
            stringBuilder.append(distance);
            stringBuilder.append(" m");
        }

        return stringBuilder.toString();
    }

    public static String getLocaleDateFormat(Locale locale)
    {
        if (locale.equals(Locale.US))
            return DATEFORMAT_US;
        else if (locale.equals(Locale.ENGLISH))
            return DATEFORMAT_UK;
        else if (locale.equals(Locale.GERMAN))
            return DATEFORMAT_DE;
        else
            return DATEFORMAT_DEFAULT;
    }

    //-------------------------------------------------//
    //                 MAPS SETTINGS                   //
    //-------------------------------------------------//

    public static final double MARKER_TAP_TOLERANCE_PERCENTAGE = 10.0;
    public static final String MAPFILE = "wandermap.map";

    public static Paint getRoutePaint()
    {
        Paint paint = AndroidGraphicFactory.INSTANCE.createPaint();
        paint.setStyle(Style.STROKE);
        paint.setColor(AndroidGraphicFactory.INSTANCE.createColor(192, 50, 119, 255));
        paint.setStrokeWidth(8);

        return paint;
    }

    //-------------------------------------------------//
    //             DYNAMIC ROUTE SETTINGS              //
    //-------------------------------------------------//

    public static final int MAX_ROUTE_HOURS = 8;
    public static final int SELECTION_GRANULARITY_MINUTES = 5;
    public static final int SELECTION_DEFAULT_HOURS = 1;
    public static final int MIN_ROUTE_MINUTES = 15;

    //-------------------------------------------------//
    //                    NETWORK                      //
    //-------------------------------------------------//

    public static boolean isNetworkAvailable(Context context, boolean showAlertDialogIfNetworkNotAvailable) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        boolean available = activeNetworkInfo != null && activeNetworkInfo.isConnected();

        if(!available && showAlertDialogIfNetworkNotAvailable){
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
            dialogBuilder.setTitle(context.getString(R.string.common_google_play_services_network_error_title));
            dialogBuilder.setMessage(context.getString(R.string.network_error_text));
            dialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            dialogBuilder.create().show();
        }

        return available;
    }
}
