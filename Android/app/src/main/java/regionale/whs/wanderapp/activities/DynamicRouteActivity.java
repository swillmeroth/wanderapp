package regionale.whs.wanderapp.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;

import regionale.whs.wanderapp.R;
import regionale.whs.wanderapp.objects.DynamicRouteSettings;
import regionale.whs.wanderapp.objects.POI;
import regionale.whs.wanderapp.objects.Route;
import regionale.whs.wanderapp.objects.RoutePoint;
import regionale.whs.wanderapp.objects.enums.DynamicRouteType;
import regionale.whs.wanderapp.objects.enums.MapType;
import regionale.whs.wanderapp.service.WanderWSClient;
import regionale.whs.wanderapp.tools.AppSettings;
import regionale.whs.wanderapp.wspojos.GeoLocation;
import regionale.whs.wanderapp.wspojos.RouteRequest;
import regionale.whs.wanderapp.wspojos.WanderRoute;

/**
 * Activity for creating a dynamic route request.
 * The user can set a desired time and what type of route he would like to do: a loop or a trail to a given destination.
 */
public class DynamicRouteActivity extends StandardActivity implements SeekBar.OnSeekBarChangeListener
{
    private DynamicRouteSettings currentSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dynamic_route);

        currentSettings = new DynamicRouteSettings();
        currentSettings.setType(DynamicRouteType.loop);

        SeekBar seekBar = (SeekBar) findViewById(R.id.seekBar);
        seekBar.setOnSeekBarChangeListener(this);

        // setting default values and min max using the App Settings

        int hours_factor = 60 / AppSettings.SELECTION_GRANULARITY_MINUTES;
        int offset = AppSettings.MIN_ROUTE_MINUTES / AppSettings.SELECTION_GRANULARITY_MINUTES;
        seekBar.setMax(AppSettings.MAX_ROUTE_HOURS * hours_factor - offset);
        int progress = AppSettings.SELECTION_DEFAULT_HOURS * hours_factor - offset;
        currentSettings.setMinutes(AppSettings.SELECTION_DEFAULT_HOURS * 60);
        seekBar.setProgress(progress);
        onProgressChanged(seekBar, progress, true);

        showPreviousSettings();
    }

    /**
     * When the app is called from DynamicResultActivity, the previous settings are loaded from intent and shown to the user.
     * Otherwise he would need to set everything new.
     */
    private void showPreviousSettings()
    {
        // get previous settings
        DynamicRouteSettings settings = this.getIntent().getParcelableExtra("settings");

        if (settings == null)
            return;

        currentSettings = settings;

        // SeekBar progress and time text

        SeekBar seekBar = (SeekBar) findViewById(R.id.seekBar);
        seekBar.setProgress(minutesToProgress(settings.getMinutes()));
        onProgressChanged(seekBar, seekBar.getProgress(), true);

        // Radio Buttons
        DynamicRouteType type = settings.getType();
        EditText editView = (EditText) findViewById(R.id.edit_destination);
        RadioButton loopButton = (RadioButton) findViewById(R.id.radiobutton_loop);
        RadioButton destButton = (RadioButton) findViewById(R.id.radiobutton_destination);
        switch (type)
        {
            case loop:
                editView.setEnabled(false);
                loopButton.setChecked(true);
                destButton.setChecked(false);
                break;
            case path:
                editView.setEnabled(true);
                loopButton.setChecked(false);
                destButton.setChecked(true);
                break;
        }

        // Destination Text
        editView.setText(settings.getEndString());
    }

    /**
     * Helper method to convert the progress value of the Progressbar to minutes.
     * Uses the AppSettings that are also used to set up the Progressbar in OnCreate()
     * @param progress progress value
     * @return Minutes
     */
    private int progressToMinutes(int progress)
    {
        progress += (AppSettings.MIN_ROUTE_MINUTES / AppSettings.SELECTION_GRANULARITY_MINUTES);
        return progress * AppSettings.SELECTION_GRANULARITY_MINUTES;
    }

    /**
     * Helper method to convert Minutes to the progress value of the Progressbar.
     * Uses the AppSettings that are also used to set up the Progressbar in OnCreate()
     * @param minutes Minutes
     * @return progress value
     */
    private int minutesToProgress(int minutes)
    {
        int progress = minutes / AppSettings.SELECTION_GRANULARITY_MINUTES;
        progress -= (AppSettings.MIN_ROUTE_MINUTES / AppSettings.SELECTION_GRANULARITY_MINUTES);
        return progress;
    }

    /**
     * If the slider changes, this method is called.
     * Updates both the current settings (using minutes) and the TextView under the SeekBar that shows the selected time in the format h:mm.
     *
     * @param seekBar SeekBar
     * @param progress progress value
     * @param fromUser boolean that is only set if change was done by user interaction
     */
    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
    {
        if (!fromUser)
            return;

        TextView selection = (TextView) findViewById(R.id.text_time_selection);

        int minutes = progressToMinutes(progress);
        currentSettings.setMinutes(minutes);

        int hours = minutes / 60;
        int modminutes = minutes % 60;
        String time_string = String.format("%d:%02d h", hours, modminutes);

        selection.setText(time_string);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar)
    {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar)
    {

    }

    /**
     * Called when one of the radio buttons is clicked. Updates the type in the current settings and shows/hides the Text input field for the destination.
     * @param view standard variable for clickable UI views
     */
    public void onRadioButtonClicked(View view)
    {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();
        if (!checked)
            return;
        EditText editView = (EditText) findViewById(R.id.edit_destination);

        // Check which radio button was clicked
        switch (view.getId())
        {
            case R.id.radiobutton_loop:
                currentSettings.setType(DynamicRouteType.loop);
                editView.setEnabled(false);
                break;
            case R.id.radiobutton_destination:
                currentSettings.setType(DynamicRouteType.path);
                editView.setEnabled(true);
                editView.requestFocus();
                break;
        }
    }

    /**
     *
     *
      * @param type
     * @param minutes
     * @param start
     * @param end
     * @return
     */
    private Route getDynamicRoute(DynamicRouteType type, int minutes, RoutePoint start, RoutePoint end)
    {
        StringBuilder builder = new StringBuilder();
        builder.append("Typ: ").append(type.toString()).append("\n");
        builder.append("Dauer: ").append(minutes).append(" Minuten\n");
        builder.append("Start: ").append(start.getLatitude()).append(" ").append(start.getLongitude()).append("\n");

        if (end != null)
            builder.append("Ziel: ").append(end);

        Toast.makeText(this, builder.toString(), Toast.LENGTH_LONG).show();

        RouteRequest routeRequest = new RouteRequest();
        routeRequest.setRouteType(type.toString());
        routeRequest.setDuration(minutes);
        routeRequest.setStart(new GeoLocation(start.getLatitude(), start.getLongitude()));

        if (end != null)
            routeRequest.setEnd(new GeoLocation(end.getLatitude(), end.getLongitude()));

        WanderWSClient wanderWSClient = WanderWSClient.getInstance();

        WanderRoute dynamicRoute = wanderWSClient.getDynamicRoute(routeRequest, this, true);

        Route route;

        if(dynamicRoute != null) {
            route = new Route(dynamicRoute);
            route.setPois(POI.convertWanderPOIs(wanderWSClient.getPOIsForDynamicRoute(dynamicRoute, this, true)));
        }
        else
            route = null;

        return route;
    }

    private RoutePoint getPointFromAddress(String address)
    {
        Geocoder geocoder = new Geocoder(this);
        List<Address> addresses;
        try {
            addresses = geocoder.getFromLocationName(address, 1);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        if(addresses.size() > 0)
        {
            double latitude = addresses.get(0).getLatitude();
            double longitude = addresses.get(0).getLongitude();

            System.out.println("lat: " + latitude + " lon: " + longitude);

            return new RoutePoint(latitude, longitude);
        }
        else // temporary
            return new RoutePoint(51.739659, 7.183333);
    }

    /**
     * Called when the search button is clicked. Firstly checks if the network is avaialable. If this is the case, tries to get the current location as start point.
     * If this is not successful, an error message is shown to the user. If it's a trail, the destination input is tried to be converted to coordinates. Then
     * the server is requested to generate a route. If this is successful, the DynamicResultActivity is started, else an error is shown.
     * @param view
     */
    public void onSearchClick(View view)
    {
        // network available
        if(!AppSettings.isNetworkAvailable(this, true))
            return;

        // current location
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        String provider = locationManager.getBestProvider(criteria, true);
        String end = null;

        Location location = locationManager.getLastKnownLocation(provider);

        if (location == null)
        {
            // show no location message
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(R.string.no_location).setTitle(R.string.error);
            builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    //progressDialog.dismiss();
                    dialog.dismiss();
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
            //return;

            // temporary
            location = new Location(provider);
            location.setLatitude(51.74601111);
            location.setLongitude(7.179872222);
        }

        // set start point to current location
        RoutePoint start = new RoutePoint(location.getLatitude(), location.getLongitude());
        currentSettings.setStart(start);

        // get destination input and convert it to coordinates
        EditText editText = (EditText) findViewById(R.id.edit_destination);
        if (currentSettings.getType().equals(DynamicRouteType.path)) {
            currentSettings.setEndString(editText.getText().toString());
            currentSettings.setEnd(getPointFromAddress(currentSettings.getEndString()));
        }

        // get route from server
        Route route = getDynamicRoute(currentSettings.getType(), currentSettings.getMinutes(), currentSettings.getStart(), currentSettings.getEnd());

        // show error if no route returned
        if(route == null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.error);
            builder.setMessage(R.string.no_results);
            builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            builder.create().show();
        }
        else {
            // start DynamicResultActivity
            Intent intent = new Intent(this, DynamicResultActivity.class);
            intent.putExtra("route", route);
            intent.putExtra("settings", currentSettings);
            intent.putExtra("maptype", MapType.DYNAMICROUTE.toString());

            startActivity(intent);
        }
    }
}
