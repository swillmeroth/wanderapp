package regionale.whs.wanderapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Locale;

import regionale.whs.wanderapp.R;
import regionale.whs.wanderapp.objects.POI;
import regionale.whs.wanderapp.objects.enums.POIType;
import regionale.whs.wanderapp.tools.AppSettings;
import regionale.whs.wanderapp.tools.converter.POITypeStringConverter;


/**
 * Adapter for populating the data fields of a poi item in the result list.
 *
 * Created by Stephan on 17.10.2014.
 */
public class POIResultAdapter extends ArrayAdapter<POI>
{

    public POIResultAdapter(Context context, ArrayList<POI> pois)
    {
        super(context, 0, pois);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        POITypeStringConverter converter = new POITypeStringConverter(getContext());

        // Get the data item for this position
        POI poi = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null)
        {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_result_poi, parent, false);
        }
        // Lookup view for data population
        TextView view_name = (TextView) convertView.findViewById(R.id.name);
        TextView view_type = (TextView) convertView.findViewById(R.id.type);
        TextView view_distance = (TextView) convertView.findViewById(R.id.distance);
        ImageView view_image = (ImageView) convertView.findViewById(R.id.imageView);
        // Populate the data into the template view using the data object
        view_name.setText(poi.getName());
        POIType type = poi.getType();
        view_type.setText(converter.convertPOITypeToString(type));
        view_image.setImageResource(poi.drawableId);

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(getContext().getString(R.string.distance));
        stringBuilder.append(": ");

        int distance = poi.getDistance();
        view_distance.setText(AppSettings.getDistanceString(distance, Locale.getDefault()));

        // Return the completed view to render on screen
        return convertView;
    }
}
