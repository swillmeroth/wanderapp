package regionale.whs.wanderapp.service;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HttpContext;

import java.io.IOException;
import java.util.ArrayList;

import regionale.whs.wanderapp.R;

/**
 *  Asynchronous representation of POST
 *
 * Created by Stephan on 28.11.2014.
 */
public class AsyncHttpPost extends AsyncTask<String, Void, HttpResponse>
{
    private Header[] mHeaders;
    private String mJsonString;
    private DefaultHttpClient mHttpClient;
    private HttpContext mHttpContext;
    private boolean showProgressBar = false;
    private ProgressDialog mDialog;
    private Context mContext;

    public AsyncHttpPost(DefaultHttpClient client, HttpContext httpContext, String jsonString, ArrayList<Header> headers, boolean showProgressBar, Context context)
    {
        this.mHttpClient = client;
        this.mHttpContext = httpContext;
        this.mJsonString = jsonString;
        this.showProgressBar = showProgressBar;
        this.mContext = context;

        if( headers != null) {
            mHeaders = new Header[headers.size()];
            int i = 0;

            for (Header header : headers) {
                mHeaders[i] = header;
                i++;
            }
        }
    }

    @Override
    protected HttpResponse doInBackground(String... params)
    {
        String url = params[0];
        HttpResponse response = null;

        HttpPost httpPost = new HttpPost(url);
        Header[] headers = httpPost.getHeaders("content-type");
        if(headers.length > 0)
        {
            httpPost.addHeader("content-type", "application/json;charset=utf-8");
            for ( Header header : headers)
            {
                httpPost.removeHeader(header);
            }
        }
        else
            httpPost.addHeader("content-type", "application/json;charset=utf-8");

        if(mHeaders != null) {
            for (Header header : mHeaders) {
                httpPost.addHeader(header);
            }
        }

        try {
            httpPost.setEntity(new StringEntity( mJsonString));
            response = mHttpClient.execute(httpPost, mHttpContext);
            return response;
        } catch (IOException e)
        {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(HttpResponse httpResponse) {
        super.onPostExecute(httpResponse);

        if(showProgressBar)
            mDialog.dismiss();
    }

    @Override
    protected void onPreExecute() {

        if(showProgressBar)
            mDialog = ProgressDialog.show(mContext, mContext.getString(R.string.loading), mContext.getString(R.string.loading_detail), true, false);

        super.onPreExecute();
    }
}
