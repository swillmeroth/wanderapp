package regionale.whs.wanderapp.activities;

import android.app.AlertDialog;
import android.app.DownloadManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import java.util.Locale;

import regionale.whs.wanderapp.R;
import regionale.whs.wanderapp.fragments.dialogs.EmailDialog;
import regionale.whs.wanderapp.fragments.dialogs.InterestDialog;
import regionale.whs.wanderapp.fragments.dialogs.LanguageDialog;
import regionale.whs.wanderapp.fragments.dialogs.PasswordDialog;
import regionale.whs.wanderapp.fragments.dialogs.WalkingSpeedDialog;
import regionale.whs.wanderapp.fragments.dialogs.WheelchairDialog;
import regionale.whs.wanderapp.service.WanderWSClient;
import regionale.whs.wanderapp.tools.AppSettings;
import regionale.whs.wanderapp.tools.Validator;
import regionale.whs.wanderapp.tools.converter.BooleanStringConverter;
import regionale.whs.wanderapp.wspojos.WanderUser;

/**
 * Activity that shows the user's profile settings and offers the possibility to change these settings.
 */
public class ProfileActivity extends StandardActivity implements DialogInterface.OnClickListener,
                                                                    WalkingSpeedDialog.OnWalkingSpeedChangeListener,
                                                                    WheelchairDialog.OnWheelchairUserChangeListener,
                                                                    InterestDialog.OnInterestChangeListener,
                                                                    EmailDialog.OnMailChangeListener,
                                                                    LanguageDialog.OnLanguageChangeListener,
                                                                    PasswordDialog.OnPasswordChangeListener {

    private WanderUser mUser;
    private TextView nameView, emailView, passwordView, wheelchairView, walkingSpeedView, languageView, interestView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        nameView = (TextView) findViewById(R.id.text_name);
        emailView = (TextView) findViewById(R.id.text_email);
        passwordView = (TextView) findViewById(R.id.text_password);
        wheelchairView = (TextView) findViewById(R.id.text_wheelchair);
        walkingSpeedView = (TextView) findViewById(R.id.text_walking_speed);
        languageView = (TextView) findViewById(R.id.text_language);
        interestView = (TextView) findViewById(R.id.text_interest);

        if(WanderWSClient.getInstance().hasOfflineData())
            findViewById(R.id.button_offline).setVisibility(View.GONE);

        // get current user
        WanderWSClient wanderWSClient = WanderWSClient.getInstance();
        mUser = wanderWSClient.getCurrentUser();

        // check if a user is logged in. if not: show error message and go back
        if (mUser == null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.error);
            builder.setMessage(R.string.no_current_user);
            builder.setPositiveButton(R.string.ok, this);
        } else {
            showData();
        }
    }

    /**
     * populates the data fields in the layout
     */
    private void showData() {
        nameView.setText(mUser.getName());
        emailView.setText(mUser.getEmail());
        passwordView.setText("********");
        BooleanStringConverter converter = new BooleanStringConverter(this);
        wheelchairView.setText(converter.convertBooleanToString(mUser.getIsWheelchairUser()));
        walkingSpeedView.setText(getLocalizedWalkingSpeed());
        languageView.setText(getLocalizedLanguage());
        interestView.setText(getLocalizedInterest());
    }

    /**
     * method for converting the standardized interest value to a localized string
     * @return localized string representing the interest
     */
    private String getLocalizedInterest() {
        String interest = mUser.getInterest();

        if (interest.equals(AppSettings.WANDER_INTEREST_RELIGION))
            return getString(R.string.interest_religion);
        if (interest.equals(AppSettings.WANDER_INTEREST_CULTURAL))
            return getString(R.string.interest_cultural);
        if (interest.equals(AppSettings.WANDER_INTEREST_GEOGRAPHY))
            return getString(R.string.interest_geography);
        if (interest.equals(AppSettings.WANDER_INTEREST_HISTORY))
            return getString(R.string.interest_history);

        return "";
    }

    /**
     * method for converting the standardized language value to a localized string
     * @return localized string representing the language
     */
    private String getLocalizedLanguage() {
        String language = mUser.getLanguage();

        if (language.equals(AppSettings.WANDER_LANGUAGE_GERMAN))
            return getString(R.string.language_german);
        if (language.equals(AppSettings.WANDER_LANGUAGE_ENGLISH))
            return getString(R.string.language_english);

        return "";
    }

    /**
     * method for converting the standardized walking speed value to a localized string
     * @return localized string representing the walking speed
     */
    private String getLocalizedWalkingSpeed() {
        String speed = mUser.getWalkingSpeed();

        if (speed.equals(AppSettings.WANDER_WALKINGSPEED_SLOW))
            return getString(R.string.walking_speed_slow);
        if (speed.equals(AppSettings.WANDER_WALKINGSPEED_NORMAL))
            return getString(R.string.walking_speed_normal);
        if (speed.equals(AppSettings.WANDER_WALKINGSPEED_FAST))
            return getString(R.string.walking_speed_fast);

        return "";
    }

    // OnClick Method for Error Dialog
    @Override
    public void onClick(DialogInterface dialog, int which) {
        onBackPressed();
        dialog.dismiss();
    }

    public void onLogoutButtonPressed(View view) {
        WanderWSClient.getInstance().resetAuthenticationData();
        onBackPressed();
    }

    /*
    EMAIL
     */

    public void onEmailClick(View view) {
        EmailDialog dialog = new EmailDialog();
        dialog.setMail(mUser.getEmail(), true);
        dialog.show(getSupportFragmentManager(), "EmailDialog");
    }

    @Override
    public void onMailChange(String mail) {

        if (!Validator.isEmailValid(mail)) {
            Toast.makeText(this, R.string.error_invalid_email, Toast.LENGTH_LONG).show();
            return;
        }

        mUser.setEmail(mail);
        WanderWSClient client = WanderWSClient.getInstance();
        boolean success = client.setUser(mUser, this, true);

        if (success)
            emailView.setText(mail);
        else
            Toast.makeText(this, R.string.error_email_exists, Toast.LENGTH_LONG).show();
    }

    /*
    PASSWORD
     */

    public void onPasswordClick(View view) {
        PasswordDialog dialog = new PasswordDialog();
        dialog.show(getSupportFragmentManager(), "PasswordDialog");
    }

    @Override
    public void onPasswordChange(String password) {
        WanderWSClient client = WanderWSClient.getInstance();
        boolean success = client.changePassword(password, this, true);

        if (!success)
            Toast.makeText(this, R.string.error_password_change, Toast.LENGTH_LONG).show();
    }

    /*
    WALKING SPEED
     */

    // Method giving the chosen walking speed
    @Override
    public void onWalkingSpeedChange(String walkingSpeed) {
        mUser.setWalkingSpeed(walkingSpeed);
        walkingSpeedView.setText(getLocalizedWalkingSpeed());
        WanderWSClient client = WanderWSClient.getInstance();
        client.setUser(mUser, this, true);
    }

    public void onWalkingSpeedClick(View view) {
        WalkingSpeedDialog dialog = new WalkingSpeedDialog();
        dialog.setWalkingSpeed(mUser.getWalkingSpeed());
        dialog.show(getSupportFragmentManager(), "WalkingSpeedDialog");
    }

    /*
    INTEREST
     */

    @Override
    public void onInterestChange(String interest) {
        mUser.setInterest(interest);
        interestView.setText(getLocalizedInterest());
        WanderWSClient client = WanderWSClient.getInstance();
        client.setUser(mUser, this, true);
    }

    public void onInterestClick(View view) {
        InterestDialog dialog = new InterestDialog();
        dialog.setInterest(mUser.getInterest());
        dialog.show(getSupportFragmentManager(), "InterestDialog");
    }

    /*
    LANGUAGE
     */

    @Override
    public void onLanguageChange(String language) {
        mUser.setLanguage(language);

        // set Locale
        Configuration cfg = new Configuration();

        if (!TextUtils.isEmpty(language)) {
            if (language.equals(AppSettings.WANDER_LANGUAGE_ENGLISH))
                cfg.locale = Locale.ENGLISH;
            else if (language.equals(AppSettings.WANDER_LANGUAGE_GERMAN))
                cfg.locale = Locale.GERMAN;
            else
                cfg.locale = Locale.getDefault();
        } else
            cfg.locale = Locale.getDefault();

        getResources().updateConfiguration(cfg, null);

        languageView.setText(getLocalizedLanguage());
        WanderWSClient client = WanderWSClient.getInstance();
        client.setUser(mUser, this, true);

        finish();
        startActivity(getIntent());
    }

    public void onLanguageClick(View view) {
        LanguageDialog dialog = new LanguageDialog();
        dialog.setLanguage(mUser.getLanguage());
        dialog.show(getSupportFragmentManager(), "LanguageDialog");
    }

    /*
    WHEELCHAIR
     */

    public void onWheelchairClick(View view) {
        WheelchairDialog dialog = new WheelchairDialog();
        dialog.setIsWheelchairUser(mUser.getIsWheelchairUser());
        dialog.show(getSupportFragmentManager(), "WheelchairDialog");
    }

    @Override
    public void onWheelchairUserChange(boolean isWheelchairUser) {
        mUser.setIsWheelchairUser(isWheelchairUser);
        BooleanStringConverter converter = new BooleanStringConverter(this);
        wheelchairView.setText(converter.convertBooleanToString(isWheelchairUser));
        WanderWSClient client = WanderWSClient.getInstance();
        client.setUser(mUser, this, true);
    }

    /*
    USERNAME
     */

    public void onUsernameClick(View view) {
        Toast.makeText(this, getString(R.string.error_username_not_changeable), Toast.LENGTH_LONG).show();
    }

    /**
     * downloads the offline map data using the android download manager
     * @param view clicked buttons
     */
    public void onOfflineDownloadClick(View view) {

        if(!isDownloadManagerAvailable(this)) {
            Toast.makeText(this, "ERROR", Toast.LENGTH_LONG).show();
            return;
        }

        String url = AppSettings.WANDER_MAP_URI;
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
        request.setDescription(getString(R.string.download_offline));
        request.setTitle(getString(R.string.downloading));
        // in order for this if to run, you must use the android 3.2 to compile your app
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            request.allowScanningByMediaScanner();
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        }

        try {
            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "wandermap.map");

            // get download service and enqueue file
            DownloadManager manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
            manager.enqueue(request);
            WanderWSClient.getInstance().setHasOfflineData(true);
        } catch (IllegalStateException e) {
            e.printStackTrace();

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.error);
            builder.setMessage(R.string.no_sdcard);
        }
    }

    /**
     * checks if the download manager is availabe
     * @param context used to check the device version and DownloadManager information
     * @return true if the download manager is available
     */
    public static boolean isDownloadManagerAvailable(Context context) {
        try {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.GINGERBREAD) {
                return false;
            }
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_LAUNCHER);
            intent.setClassName("com.android.providers.downloads.ui", "com.android.providers.downloads.ui.DownloadList");
            List<ResolveInfo> list = context.getPackageManager().queryIntentActivities(intent,
                    PackageManager.MATCH_DEFAULT_ONLY);
            return list.size() > 0;
        } catch (Exception e) {
            return false;
        }
    }
}
