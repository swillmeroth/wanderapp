package regionale.whs.wanderapp.service;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HttpContext;

import java.io.IOException;

import regionale.whs.wanderapp.R;

/**
 * Asynchronous representation of DELETE
 *
 * Created by Stephan on 28.11.2014.
 */
public class AsyncHttpDelete extends AsyncTask<String, Void, HttpResponse>
{
    private DefaultHttpClient mHttpClient;
    private HttpContext mHttpContext;
    private boolean showProgressBar = false;
    private ProgressDialog mDialog;
    private Context mContext;

    public AsyncHttpDelete(DefaultHttpClient client, HttpContext httpContext, boolean showProgressBar, Context context)
    {
        this.mHttpClient = client;
        this.mHttpContext = httpContext;
        this.showProgressBar = showProgressBar;
        this.mContext = context;
    }

    @Override
    protected HttpResponse doInBackground(String... params)
    {
        String url = params[0];
        HttpResponse response = null;

        HttpDelete httpDelete = new HttpDelete(url);

        try
        {
            response = mHttpClient.execute(httpDelete, mHttpContext);
            return response;
        } catch (IOException e)
        {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(HttpResponse httpResponse) {
        super.onPostExecute(httpResponse);

        if(showProgressBar)
            mDialog.dismiss();
    }

    @Override
    protected void onPreExecute() {

        if(showProgressBar)
            mDialog = ProgressDialog.show(mContext, mContext.getString(R.string.loading), mContext.getString(R.string.loading_detail), true, false);

        super.onPreExecute();
    }
}
