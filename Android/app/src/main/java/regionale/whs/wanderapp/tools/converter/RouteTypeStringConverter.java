package regionale.whs.wanderapp.tools.converter;

import android.content.Context;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import regionale.whs.wanderapp.R;
import regionale.whs.wanderapp.objects.enums.RouteType;

/**
 * Since the android enum object doesn't allow values to be stored with the keys, I had to implement this converter class.
 * It holds a map of key/value-pairs that associate a RouteType with the corresponding R.string id.
 * By giving a context to the constructor, this association is also context-aware and by that localized.
 */
public class RouteTypeStringConverter
{
    private ArrayList<Map.Entry<RouteType, String>> convertMap;

    /**
     * Standard constructor which holds the key/value pairs.
     *
     * @param context Context needed for getting the right String-resource
     */
    public RouteTypeStringConverter(Context context)
    {
        convertMap = new ArrayList<>();

        // this is the place to add new type/string combinations!
        convertMap.add(new HashMap.SimpleEntry<>(RouteType.loop, context.getString(R.string.loop))); // loop
        convertMap.add(new HashMap.SimpleEntry<>(RouteType.path, context.getString(R.string.trail))); // trail
    }

    /**
     * Converts the enum value to a localized string
     *
     * @param type enum value
     * @return string representation of enum value
     */
    public String convertRouteTypeToString(RouteType type)
    {
        for (int i = 0; i < convertMap.size(); i++)
        {
            Map.Entry entry = convertMap.get(i);
            if (entry.getKey().equals(type))
                return (String) entry.getValue();
        }

        return null;
    }

    /**
     * Converts a string to POIType
     *
     * @param typeString localized string representation of POIType value
     * @return POIType if found, else null
     */
    public RouteType convertStringToRouteType(String typeString)
    {
        for (int i = 0; i < convertMap.size(); i++)
        {
            Map.Entry entry = convertMap.get(i);
            if (entry.getValue().equals(typeString))
                return (RouteType) entry.getKey();
        }
        return null;
    }
}
