package regionale.whs.wanderapp.fragments.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

import regionale.whs.wanderapp.R;

/**
 * Dialog for changing the user's wheelchair user setting.
 */
public class WheelchairDialog extends DialogFragment implements DialogInterface.OnClickListener {

    OnWheelchairUserChangeListener mListener;
    private int mSelected = 0;
    private boolean mPrevSetting;
    private boolean mIsWheelchairUser = false;

    public void setIsWheelchairUser(boolean isWheelchairUser) {
        mIsWheelchairUser = isWheelchairUser;
        mPrevSetting = mIsWheelchairUser;
    }

    // Override the Fragment.onAttach() method to instantiate the NoticeDialogListener
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (OnWheelchairUserChangeListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String[] values = {getString(R.string.falseString), getString(R.string.trueString)};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle(R.string.wheelchair);

        builder.setSingleChoiceItems(values, mSelected, this);

        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if (mPrevSetting != mIsWheelchairUser) {
                    mListener.onWheelchairUserChange(mIsWheelchairUser);
                }
                // User clicked OK button
            }
        });

        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
            }
        });

        return builder.create();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch(which) {
            case 0:
                mIsWheelchairUser = false;
                break;
            case 1:
                mIsWheelchairUser = true;
                break;
            default:
                break;
        }
    }

    public interface OnWheelchairUserChangeListener {
        void onWheelchairUserChange(boolean isWheelchairUser);
    }
}
