package regionale.whs.wanderapp.activities;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.Display;
import android.view.WindowManager;
import android.widget.Toast;

import org.mapsforge.core.graphics.Bitmap;
import org.mapsforge.core.model.BoundingBox;
import org.mapsforge.core.model.LatLong;
import org.mapsforge.core.util.LatLongUtils;
import org.mapsforge.map.android.graphics.AndroidGraphicFactory;
import org.mapsforge.map.android.layer.MyLocationOverlay;
import org.mapsforge.map.android.util.AndroidUtil;
import org.mapsforge.map.android.view.MapView;
import org.mapsforge.map.layer.Layers;
import org.mapsforge.map.layer.cache.TileCache;
import org.mapsforge.map.layer.download.TileDownloadLayer;
import org.mapsforge.map.layer.download.tilesource.OnlineTileSource;
import org.mapsforge.map.layer.overlay.Marker;
import org.mapsforge.map.layer.overlay.Polyline;
import org.mapsforge.map.layer.renderer.TileRendererLayer;
import org.mapsforge.map.rendertheme.InternalRenderTheme;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import regionale.whs.wanderapp.R;
import regionale.whs.wanderapp.fragments.MapFragment;
import regionale.whs.wanderapp.objects.POI;
import regionale.whs.wanderapp.objects.Route;
import regionale.whs.wanderapp.objects.RoutePoint;
import regionale.whs.wanderapp.service.WanderWSClient;
import regionale.whs.wanderapp.tools.AppSettings;
import regionale.whs.wanderapp.tools.GeoCalc;
import regionale.whs.wanderapp.tools.LocationUtil;
import regionale.whs.wanderapp.tools.map.MarkerUtil;
import regionale.whs.wanderapp.tools.map.MyMapView;
import regionale.whs.wanderapp.tools.map.POIMarker;
import regionale.whs.wanderapp.tools.map.RouteMarker;

/**
 * Activity for navigating along a route. In theory and in the current state of implementation, this is not much more than a stripped down version of MapFragment.
 * MapFragment is not used, because it was planned to make the whole map rotating to the current user's heading. This didn't work as planned so it had to be removed.
 */
public class NavigationActivity extends StandardActivity  implements POIMarker.OnPOITappedListener, RouteMarker.OnRouteTappedListener, MyMapView.onSizeChangedListener, MyMapView.onMapDragListener, LocationListener, MapFragment.OnPOIInfoClickListener, StandardActivity.MapDataListener {

    //private RotateView mRotateView;
    private MapView mMapView;
    private TileCache tileCache;
    private LocationManager locationManager;
    private MyLocationOverlay myLocationOverlay;
    private TileDownloadLayer downloadLayer;
    private Route mRoute;
    private Location lastLocation;
    private POI mNextPOI;
    private Marker mNextPOIMarker;

    private BoundingBox mBoundingBox;
    private TileRendererLayer tileRendererLayer;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        AndroidGraphicFactory.createInstance(getApplication());

        //mRotateView = new RotateView(this);
        mMapView = new MapView(this);
        //TODO: find a way to get it rotating
        //mRotateView.addView(mMapView);
        setContentView(mMapView);

        //mMapView.setDragListener(this);
        //mMapView.setSizeChangedListener(this);
        mMapView.setClickable(true);
        mMapView.setEnabled(true);
        mMapView.getMapScaleBar().setVisible(true);
        mMapView.setBuiltInZoomControls(true);

        makeNavigationLayers();

        //this.tileCache = AndroidUtil.createTileCache(this, "mapcache", mMapView.getModel().displayModel.getTileSize(), 1f, mMapView.getModel().frameBufferModel.getOverdrawFactor());

        createTileCache();
        setOnlineLayer();
        float lastHeading = 0;


        //addOverlayLayers(mMapView.getLayerManager().getLayers());
        setPositionLayer(true);

        Location location = new Location("gps");
        location.setLatitude(51.74601111);
        location.setLongitude(7.179872222);
        lastLocation = location;
        LatLong LocationLatLong = new LatLong(location.getLatitude(), location.getLongitude());
        mMapView.getModel().mapViewPosition.setCenter(LocationLatLong);
        mMapView.getModel().mapViewPosition.setZoomLevel((byte) 12);

        makeNavigationLayers();
        //mRotateView.setHeading(lastHeading);
        //mSensorManager.registerListener(mRotateView, SensorManager.SENSOR_ORIENTATION, SensorManager.SENSOR_DELAY_UI);
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        this.downloadLayer.onResume();
        myLocationOverlay.enableMyLocation(true);

        showOfflineItem(WanderWSClient.getInstance().hasOfflineData());
        setMapDataListener(this);
    }

    /**
     * calls other methods to create all layers necessary for navigation
     */
    private void makeNavigationLayers()
    {
        // show route
        makeSingleRouteLayer();

        boolean mHasPOIs = mRoute.getPOIs().size() > 0;

        if(mHasPOIs) {
            mNextPOI = mRoute.getPOIs().get(0);
            addPOIMarker(mNextPOI, true);
        }

        // show location
        setPositionLayer(true);

        // snap to location

        // zoom?
    }

    /**
     * creates a layer for a single route
     */
    private void makeSingleRouteLayer()
    {
        Route route = this.getIntent().getExtras().getParcelable("route");
        mRoute = route;
        addRouteLayer(route, true);

        if (route.getBoundingBox().getLatitudeSpan() > 0)
        {
            mBoundingBox = route.getBoundingBox();

            mMapView.getModel().mapViewPosition.setCenter(mBoundingBox.getCenterPoint());
        }
    }

    /**
     * adds a layer for a route. If multiple routes are shown, only a route marker is placed. Otherwise the full route including the POIs is drawn.
     * @param route Route to draw
     * @param isSingle if only one route is shown on the map
     */
    private void addRouteLayer(Route route, boolean isSingle)
    {
        RoutePoint startPoint = route.getStartPoint();

        if (startPoint == null)
            return;

        double lat = route.getStartPoint().getLatitude();
        double lon = route.getStartPoint().getLongitude();

        if (lat == 0 || lon == 0)
            return;

        // isSingle: the whole track including start and end icon is rendered
        // !isSingle: only the start point is shown by an icon, no track and no end point
        if (isSingle)
        {
            ArrayList<RoutePoint> routePoints = route.getRoutePoints();
            ArrayList<POI> pois = route.getPOIs();

            if (routePoints == null)
                return;

            Polyline trackline = new Polyline(AppSettings.getRoutePaint(), AndroidGraphicFactory.INSTANCE);
            List<LatLong> list = trackline.getLatLongs();

            for (RoutePoint routePoint : routePoints)
            {
                lat = routePoint.getLatitude();
                lon = routePoint.getLongitude();

                LatLong latlong = new LatLong(lat, lon);

                list.add(latlong);
            }

            mMapView.getLayerManager().getLayers().add(trackline);

            // start point
            lat = startPoint.getLatitude();
            lon = startPoint.getLongitude();
            LatLong latLong = new LatLong(lat, lon);
            Drawable drawable = getResources().getDrawable(R.drawable.start);
            Bitmap bitmap = AndroidGraphicFactory.convertToBitmap(drawable);
            Marker marker = new Marker(latLong, bitmap, 0, 0);
            mMapView.getLayerManager().getLayers().add(marker);

            // end point
            lat = route.getEndPoint().getLatitude();
            lon = route.getEndPoint().getLongitude();
            latLong = new LatLong(lat, lon);
            drawable = getResources().getDrawable(R.drawable.end);
            bitmap = AndroidGraphicFactory.convertToBitmap(drawable);
            marker = new Marker(latLong, bitmap, 0, 0);
            mMapView.getLayerManager().getLayers().add(marker);

            for (POI poi : pois)
            {
                addPOIMarker(poi, false);
            }


        } else
        {
            LatLong latLong = new LatLong(lat, lon);

            Drawable drawable = getResources().getDrawable(R.drawable.route);
            Bitmap bitmap = AndroidGraphicFactory.convertToBitmap(drawable);

            int offset = bitmap.getHeight() / 2;
            offset = 0 - offset;

            RouteMarker marker = new RouteMarker(latLong, bitmap, 0, offset, this, route);

            mMapView.getLayerManager().getLayers().add(marker);
        }
    }

    /**
     * adds a marker for a poi. If it is the next poi on the route, an icon different to the usual one is used
     * @param poi the POI to show
     * @param isNextPOI if the poi is next on the route
     */
    private void addPOIMarker(POI poi, boolean isNextPOI)
    {
        double lat = poi.getLatitude();
        double lon = poi.getLongitude();

        if (lat != 0 && lon != 0)
        {
            LatLong latlong = new LatLong(lat, lon);
            Drawable drawable;

            if(isNextPOI) {
                if(mNextPOIMarker != null)
                    mMapView.getLayerManager().getLayers().remove(mNextPOIMarker);

                drawable = MarkerUtil.getDrawableForPOIType(poi.getType(), this);
            }
            else
                drawable = MarkerUtil.getDrawableForPOITypeFollowing(poi.getType(), this);

            Bitmap bitmap = AndroidGraphicFactory.convertToBitmap(drawable);

            int offset = bitmap.getHeight() / 2;
            offset = 0 - offset;

            POIMarker marker = new POIMarker(latlong, bitmap, 0, offset, this, poi);

            mMapView.getLayerManager().getLayers().add(marker);

            if(isNextPOI) {
                mNextPOIMarker = marker;
            }
        }
    }

    /**
     * creates the tile cache needed for showing the rendered or downloaded map tiles
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    @SuppressWarnings("deprecation")
    private void createTileCache() {
        boolean threaded = true;
        int queueSize = 4;

        Display display = ((WindowManager) getSystemService(Context.WINDOW_SERVICE))
                .getDefaultDisplay();
        final int hypot;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            android.graphics.Point point = new android.graphics.Point();
            display.getSize(point);
            hypot = (int) Math.hypot(point.x, point.y);
        } else {
            hypot = (int) Math.hypot(display.getWidth(), display.getHeight());
        }

        this.tileCache = AndroidUtil.createTileCache(this,
                "rotation",
                mMapView.getModel().displayModel.getTileSize(), hypot,
                hypot,
                mMapView.getModel().frameBufferModel.getOverdrawFactor(),
                threaded, queueSize);
    }

    /**
     * shows a layer that represents the user's current position
     * @param isNavigation if the map is in navigation mode, a triangular navigation item is used instead of the usual round position icon
     */
    private void setPositionLayer(boolean isNavigation)
    {

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        String provider = locationManager.getBestProvider(criteria, true);
        locationManager.requestLocationUpdates(provider, 500, 1, this);
        Drawable drawable = getResources().getDrawable(R.drawable.ic_maps_indicator_current_position);

        // a marker to show at the position
        if(isNavigation)
        {
            // icon
            drawable = getResources().getDrawable(R.drawable.icon_navigation);

            // rotation

        }

        Bitmap bitmap = AndroidGraphicFactory.convertToBitmap(drawable);

        // create the overlay and tell it to follow the location
        myLocationOverlay = new MyLocationOverlay(this, mMapView.getModel().mapViewPosition, bitmap);
        myLocationOverlay.setSnapToLocationEnabled(true);
        mMapView.getLayerManager().getLayers().add(myLocationOverlay);
    }

    /**
     * switches the map to offline data
     */
    private void setOfflineLayer()
    {
        if(downloadLayer != null)
            mMapView.getLayerManager().getLayers().remove(downloadLayer);

        tileRendererLayer = AndroidUtil.createTileRendererLayer(this.tileCache, this.mMapView.getModel().mapViewPosition, getMapFile(), InternalRenderTheme.OSMARENDER, false, true);
        this.mMapView.getLayerManager().getLayers().add(tileRendererLayer);
    }

    /**
     * switches the map to online data
     */
    private void setOnlineLayer()
    {
        if(tileRendererLayer != null)
            this.mMapView.getLayerManager().getLayers().remove(tileRendererLayer);

        // using tiles from mapquest
        // http://otile1.mqcdn.com/tiles/1.0.0/map

        OnlineTileSource onlineTileSource = new OnlineTileSource(new String[]{
                "otile1.mqcdn.com", "otile2.mqcdn.com", "otile3.mqcdn.com",
                "otile4.mqcdn.com"}, 80);
        onlineTileSource.setName("MapQuest").setAlpha(false)
                .setBaseUrl("/tiles/1.0.0/map/").setExtension("png")
                .setParallelRequestsLimit(8).setProtocol("http")
                .setTileSize(256).setZoomLevelMax((byte) 19)
                .setZoomLevelMin((byte) 0);

        //this.downloadLayer = new TileDownloadLayer(this.tileCache, this.mapView.getModel().mapViewPosition, OpenStreetMapMapnik.INSTANCE, AndroidGraphicFactory.INSTANCE);
        this.downloadLayer = new TileDownloadLayer(this.tileCache, mMapView.getModel().mapViewPosition, onlineTileSource, AndroidGraphicFactory.INSTANCE);

        mMapView.getLayerManager().getLayers().add(this.downloadLayer);
    }

    /**
     * gets the offline map data file using the path from AppSettings
     * @return File reference
     */
    public File getMapFile()
    {
        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        return new File(path, AppSettings.MAPFILE);
    }

    @Override
    public void onMapDrag()
    {

    }

    /**
     * if the size if the map changes, the zoom level is changed according to the new dimensions
     * @param width
     * @param height
     * @param oldWidth
     * @param oldHeight
     */
    @Override
    public void onSizeChanged(int width, int height, int oldWidth, int oldHeight)
    {
        if (width == oldWidth && height == oldHeight)
            return;

        if (mBoundingBox == null)
            return;

        byte zoomLevel = LatLongUtils.zoomForBounds(this.mMapView.getDimension(), mBoundingBox, mMapView.getModel().displayModel.getTileSize());
        this.mMapView.getModel().mapViewPosition.setZoomLevel(zoomLevel);

        if(myLocationOverlay != null)
            myLocationOverlay.enableMyLocation(true);

    }

    /**
     * Listener method called when the location changes. First checks if the new location is better than the old one regarding the accuracy and provider.
     * Then centers the map to the new location and checks if a poi was reached. If that is the case, a notification Toast is shown and the POI markers are updated.
     * @param location
     */
    @Override
    public void onLocationChanged(Location location) {

        // check if new location is good
        if(!LocationUtil.isBetterLocation(location, lastLocation))
            return;

        // update heading
        //float heading = (float)GeoCalc.calcHeading(location, lastLocation);
        //mRotateView.setHeading((lastHeading + heading) / 2); // uses the average of the last two headings to compensate for bad fixes
        //lastHeading = heading;

        // center location is updated via PositionLayer, so this should be deletable
        mMapView.getModel().mapViewPosition.setCenter(new LatLong(location.getLatitude(), location.getLongitude()));

        // TODO: check location fixing

        // check if POI is reached
        // if yes: show notification and set next POI as target
        double distance = GeoCalc.calcDistance(mNextPOI.getLatitude(), mNextPOI.getLongitude(), location);
        if( distance < (mNextPOI.getSize()*2))
        {
            // show notification (and vibrate) TODO
            Toast.makeText(this, "POI reached!", Toast.LENGTH_LONG).show();

            // set next POI
            ArrayList<POI> pois = mRoute.getPOIs();
            int index = pois.indexOf(mNextPOI);
            mNextPOI = pois.get( index + 1);
            addPOIMarker(mNextPOI, true);
        }

        // save location
        lastLocation = location;
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onProviderEnabled(String provider) {

        // if GPS enabled, change to GPS
        if(provider.equals(LocationManager.GPS_PROVIDER)) {
            locationManager.requestLocationUpdates(provider, 500, 1, this);
        }
    }

    @Override
    public void onProviderDisabled(String provider) {
        // if GPS disabled, change to NETWORK
        if( provider.equals(LocationManager.GPS_PROVIDER)) {
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 500, 1, this);
            Toast.makeText(this, getString(R.string.gps_lost), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onPOITapped(POI poi) {

    }

    @Override
    public void onRouteTapped(Route route) {

    }

    @Override
    public void onPOIInfoClick(POI poi) {

    }

    @Override
    public void goOnline() {



        // TODO: find out why setOnlineLayer doesn't work
        finish();
        startActivity(getIntent());
    }

    @Override
    public void goOffline() {
        setOfflineLayer();
    }
}
