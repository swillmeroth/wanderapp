package regionale.whs.wanderapp.objects;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

import regionale.whs.wanderapp.wspojos.GeoLocation;

/**
 * Object representing a RoutePoint.
 *
 * Created by Stephan on 18.02.2015.
 */
public class RoutePoint implements Parcelable
{
    public double latitude, longitude;

    public static final Parcelable.Creator<RoutePoint> CREATOR = new Parcelable.Creator<RoutePoint>()
    {
        public RoutePoint createFromParcel(Parcel in)
        {
            return new RoutePoint(in);
        }

        public RoutePoint[] newArray(int size)
        {
            return new RoutePoint[size];
        }
    };

    public RoutePoint() {
        initialize();
    }

    public RoutePoint(double latitude, double longitude)
    {
        initialize();

        this.latitude = latitude;
        this.longitude = longitude;
    }

    public RoutePoint(Parcel in)
    {
        initialize();

        this.latitude = in.readDouble();
        this.longitude = in.readDouble();
    }

    private void initialize() {
        this.latitude = 0;
        this.longitude = 0;
    }

    public GeoLocation toGeoLocation(RoutePoint routePoint) {
        GeoLocation geoLocation =  new GeoLocation();
        geoLocation.setLatitude(routePoint.getLatitude());
        geoLocation.setLongitude(routePoint.getLongitude());
        return geoLocation;
    }

    public static RoutePoint fromGeoLocation(GeoLocation geoLocation) {
        return new RoutePoint(geoLocation.getLatitude(), geoLocation.getLongitude());
    }

    public static ArrayList<RoutePoint> fromGeoLocationList(List<GeoLocation> list) {
        ArrayList<RoutePoint> returnlist = new ArrayList<>();

        for( GeoLocation location : list) {
            returnlist.add(RoutePoint.fromGeoLocation(location));
        }

        return returnlist;
    }

    public double getLatitude()
    {
        return latitude;
    }

    public void setLatitude(double latitude)
    {
        this.latitude = latitude;
    }

    public double getLongitude()
    {
        return longitude;
    }

    public void setLongitude(double longitude)
    {
        this.longitude = longitude;
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeDouble(this.latitude);
        dest.writeDouble(this.longitude);
    }

    @Override
    public String toString() {
        return "RoutePoint: Latitude " + latitude + " Longitude: " + longitude;
    }
}
