package regionale.whs.wanderapp.objects.enums;

/**
 * The possible types of a route.
 */
public enum RouteType
{
    path,
    loop
}
