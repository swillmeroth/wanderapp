package regionale.whs.wanderapp.objects;

import android.os.Parcel;
import android.os.Parcelable;

import regionale.whs.wanderapp.objects.enums.DynamicRouteType;

/**
 * Object representing the settings for a dynamic route.
 *
 * Created by Stephan on 04.03.2015.
 */
public class DynamicRouteSettings implements Parcelable
{

    public static final Parcelable.Creator<DynamicRouteSettings> CREATOR = new Parcelable.Creator<DynamicRouteSettings>()
    {
        public DynamicRouteSettings createFromParcel(Parcel in)
        {
            return new DynamicRouteSettings(in);
        }

        public DynamicRouteSettings[] newArray(int size)
        {
            return new DynamicRouteSettings[size];
        }
    };

    private DynamicRouteType type;
    private int minutes;
    private RoutePoint start;
    private RoutePoint end;
    private String endString;

    public DynamicRouteSettings()
    {
        initialize();
    }

    public DynamicRouteSettings(DynamicRouteType type, int minutes, RoutePoint start, RoutePoint end, String endString)
    {
        initialize();

        this.type = type;
        this.minutes = minutes;
        this.start = start;
        this.end = end;
        this.endString = endString;
    }

    private void initialize()
    {
        this.minutes = 0;
        this.start = new RoutePoint(0, 0);
        this.end = new RoutePoint(0,0);
        this.endString = "";
    }

    public DynamicRouteSettings(Parcel in)
    {
        this.minutes = in.readInt();
        this.end = in.readParcelable(RoutePoint.class.getClassLoader());
        this.type = DynamicRouteType.valueOf(in.readString());
        this.start = in.readParcelable(RoutePoint.class.getClassLoader());
        this.endString = in.readString();
    }

    public DynamicRouteType getType()
    {
        return type;
    }

    public void setType(DynamicRouteType type)
    {
        this.type = type;
    }

    public int getMinutes()
    {
        return minutes;
    }

    public void setMinutes(int minutes)
    {
        this.minutes = minutes;
    }

    public RoutePoint getStart()
    {
        return start;
    }

    public void setStart(RoutePoint start)
    {
        this.start = start;
    }

    public String getEndString()
    {
        return endString;
    }

    public RoutePoint getEnd() { return end; }

    public void setEnd(RoutePoint end)
    {
        this.end = end;
    }

    public void setEndString(String end) {
        this.endString = end;
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeInt(minutes);
        dest.writeParcelable(end, 0);
        dest.writeString(type.toString());
        dest.writeParcelable(start, 0);
        dest.writeString(endString);
    }
}
