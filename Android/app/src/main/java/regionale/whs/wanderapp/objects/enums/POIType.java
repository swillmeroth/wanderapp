package regionale.whs.wanderapp.objects.enums;

/**
 * The possible types of a POI
 */
public enum POIType
{
    ABBEY,
    CHAPEL,
    CHURCH,
    CROSS,
    GRAVEYARD,
    JEWISH_GRAVEYARD,
    MONUMENT,
    STATUE
}
