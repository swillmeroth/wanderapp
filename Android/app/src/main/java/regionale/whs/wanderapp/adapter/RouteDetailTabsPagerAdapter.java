package regionale.whs.wanderapp.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import regionale.whs.wanderapp.activities.RouteDetailActivity;
import regionale.whs.wanderapp.fragments.CommentsFragment;
import regionale.whs.wanderapp.fragments.MapFragment;
import regionale.whs.wanderapp.fragments.RouteDetailMainFragment;
import regionale.whs.wanderapp.objects.Route;

/**
 * Adapter for the tabs of a route detail activity.
 * Tab 1: main details
 * Tab 2: comments
 * Tab 3: map
 *
 * Created by Stephan on 04.11.2014.
 */
public class RouteDetailTabsPagerAdapter extends FragmentPagerAdapter
{

    private static final int ROUTE_DETAIL_TAB_COUNT = 3; // 4, if altitude profile is implemented
    private RouteDetailActivity mActivity;
    private Route mRoute;
    private CommentsAdapter mAdapter;

    public RouteDetailTabsPagerAdapter(FragmentManager fm, Route route, CommentsAdapter adapter, RouteDetailActivity activity)
    {
        super(fm);
        mRoute = route;
        mAdapter = adapter;
        mActivity = activity;
    }

    @Override
    public Fragment getItem(int position)
    {
        switch (position)
        {
            case 0:
                return RouteDetailMainFragment.newInstance(mRoute);
            case 1:
                return CommentsFragment.newInstance(mAdapter, mActivity);
            case 2:
                return new MapFragment();
            /*
            case 3:
                return RouteDetailMainFragment.newInstance(mRoute);*/
        }

        return null;
    }

    @Override
    public int getCount()
    {
        return ROUTE_DETAIL_TAB_COUNT;
    }
}
