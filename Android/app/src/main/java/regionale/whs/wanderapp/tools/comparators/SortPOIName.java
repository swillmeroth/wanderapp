package regionale.whs.wanderapp.tools.comparators;

import java.util.Comparator;

import regionale.whs.wanderapp.objects.POI;

/**
 * sorts pois by name
 *
 * Created by Stephan on 31.10.2014.
 */

public class SortPOIName implements Comparator<POI>
{
    @Override
    public int compare(POI poi1, POI poi2)
    {
        return poi1.getName().compareToIgnoreCase(poi2.getName());
    }
}