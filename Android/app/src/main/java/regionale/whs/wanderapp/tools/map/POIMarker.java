package regionale.whs.wanderapp.tools.map;

import org.mapsforge.core.graphics.Bitmap;
import org.mapsforge.core.model.LatLong;
import org.mapsforge.core.model.Point;
import org.mapsforge.map.layer.overlay.Marker;

import regionale.whs.wanderapp.objects.POI;
import regionale.whs.wanderapp.tools.AppSettings;

/**
 * Marker variant for a POI.
 *
 * Created by Stephan on 04.02.2015.
 */
public class POIMarker extends Marker
{
    private POI poi;
    private OnPOITappedListener tapListener;

    public POIMarker(LatLong latLong, Bitmap bitmap, int horizontalOffset, int verticalOffset, OnPOITappedListener listener, POI poi)
    {
        super(latLong, bitmap, horizontalOffset, verticalOffset);
        this.poi = poi;
        this.tapListener = listener;
    }

    public POI getPoi()
    {
        return poi;
    }

    @Override
    public boolean onTap(LatLong tapLatLong, Point layerXY, Point tapXY)
    {
        double centerX = layerXY.x + getHorizontalOffset();
        double centerY = layerXY.y + getVerticalOffset();

        double factor = 1 + (AppSettings.MARKER_TAP_TOLERANCE_PERCENTAGE / 100);
        double radiusX = (getBitmap().getWidth() / 2) * factor;
        double radiusY = (getBitmap().getHeight() / 2) * factor;

        double distX = Math.abs(centerX - tapXY.x);
        double distY = Math.abs(centerY - tapXY.y);

        if (distX < radiusX && distY < radiusY)
        {
            tapListener.onPOITapped(poi);
            return true;
        } else
            return false;   // by returning false, the next layer's onTap-Method is called
    }

    public interface OnPOITappedListener
    {
        void onPOITapped(POI poi);
    }
}
