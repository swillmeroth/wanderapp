package regionale.whs.wanderapp.tools.map;

import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import org.mapsforge.core.model.BoundingBox;
import org.mapsforge.map.android.view.MapView;
import org.mapsforge.map.util.MapPositionUtil;

/**
 * MapView-class that implements various listeners needed for map interaction.
 *
 * Created by Stephan on 05.02.2015.
 */
public class MyMapView extends MapView
{
    private onMapDragListener onMapDragListener;
    private onMapsectionChangeListener onMapsectionChangeListener;
    private onSizeChangedListener onSizeChangedListener;

    public MyMapView(Context context)
    {
        super(context);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            setLayerType(View.LAYER_TYPE_SOFTWARE, null);
    }

    public MyMapView(Context context, AttributeSet attributeSet)
    {
        super(context, attributeSet);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            setLayerType(View.LAYER_TYPE_SOFTWARE, null);
    }

    public void setMapsectionChangeListener(onMapsectionChangeListener listener)
    {
        onMapsectionChangeListener = listener;
    }

    public void setDragListener(onMapDragListener listener)
    {
        onMapDragListener = listener;
    }

    public void setSizeChangedListener(onSizeChangedListener onSizeChangedListener)
    {
        this.onSizeChangedListener = onSizeChangedListener;
    }

    @Override
    protected void onSizeChanged(int width, int height, int oldWidth, int oldHeight)
    {
        super.onSizeChanged(width, height, oldWidth, oldHeight);

        onSizeChangedListener.onSizeChanged(width, height, oldWidth, oldHeight);
    }

    @Override
    public boolean onTouchEvent(MotionEvent motionEvent)
    {
        int action = motionEvent.getAction();

        if (action == MotionEvent.ACTION_MOVE)
            onMapDragListener.onMapDrag();

        if (action == MotionEvent.ACTION_UP && onMapsectionChangeListener != null) {
            BoundingBox mapSection = MapPositionUtil.getBoundingBox(getModel().mapViewPosition.getMapPosition(), getDimension(), getModel().displayModel.getTileSize());
            onMapsectionChangeListener.onMapsectionChange( mapSection);
        }

        return super.onTouchEvent(motionEvent);
    }

    public interface onMapsectionChangeListener
    {
        void onMapsectionChange(BoundingBox mapSection);
    }

    public interface onMapDragListener
    {
        void onMapDrag();
    }

    public interface onSizeChangedListener
    {
        void onSizeChanged(int width, int height, int oldWidth, int oldHeight);
    }
}
