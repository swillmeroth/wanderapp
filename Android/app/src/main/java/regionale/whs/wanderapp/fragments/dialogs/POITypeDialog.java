package regionale.whs.wanderapp.fragments.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

import regionale.whs.wanderapp.R;
import regionale.whs.wanderapp.objects.enums.POIType;

/**
 * Dialog for changing the poi type.
 */
public class POITypeDialog extends DialogFragment implements DialogInterface.OnClickListener {

    OnPOITypeChangeListener mListener;
    private int mSelected = 0;
    private POIType mPrevType;
    private POIType mType;

    public void setType(POIType poiType) {
        mSelected = getIntForType(poiType);
        mPrevType = mType;
    }

    private int getIntForType(POIType type) {
        if(type == POIType.ABBEY) {
            mType = type;
            return 0;
        }
        else if(type == POIType.CHAPEL) {
            mType = type;
            return 1;
        }
        else if(type == POIType.CHURCH) {
            mType = type;
            return 2;
        }
        else if(type == POIType.CROSS) {
            mType = type;
            return 3;
        }
        else if(type == POIType.GRAVEYARD) {
            mType = type;
            return 4;
        }
        else if(type == POIType.JEWISH_GRAVEYARD) {
            mType = type;
            return 5;
        }
        else if(type == POIType.MONUMENT) {
            mType = type;
            return 6;
        }
        else if(type == POIType.STATUE) {
            mType = type;
            return 7;
        }
        else {
            mType = POIType.CHURCH;
            return 2;
        }
    }

    // Override the Fragment.onAttach() method to instantiate the NoticeDialogListener
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (OnPOITypeChangeListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String[] values = {getString(R.string.abbey), getString(R.string.chapel), getString(R.string.church), getString(R.string.cross), getString(R.string.graveyard), getString(R.string.jewish_graveyard), getString(R.string.monument), getString(R.string.statue)};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle(R.string.type);

        builder.setSingleChoiceItems(values, mSelected, this);

        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if(mPrevType != mType) {
                    mListener.onPOITypeChange(mType);
                }
                // User clicked OK button
            }
        });

        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
            }
        });

        return builder.create();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch(which) {
            case 0:
                mType = POIType.ABBEY;
                break;
            case 1:
                mType = POIType.CHAPEL;
                break;
            case 2:
                mType = POIType.CHURCH;
                break;
            case 3:
                mType = POIType.CROSS;
                break;
            case 4:
                mType = POIType.GRAVEYARD;
                break;
            case 5:
                mType = POIType.JEWISH_GRAVEYARD;
                break;
            case 6:
                mType = POIType.MONUMENT;
                break;
            case 7:
                mType = POIType.STATUE;
                break;
            default:
                break;
        }
    }

    public interface OnPOITypeChangeListener {
        void onPOITypeChange(POIType type);
    }
}
