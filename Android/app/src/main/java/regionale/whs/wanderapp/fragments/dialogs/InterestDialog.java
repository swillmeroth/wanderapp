package regionale.whs.wanderapp.fragments.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

import regionale.whs.wanderapp.R;
import regionale.whs.wanderapp.tools.AppSettings;

/**
 * Dialog for changing the user's interest
 */
public class InterestDialog extends DialogFragment implements DialogInterface.OnClickListener {

    OnInterestChangeListener mListener;
    private int mSelected = 0;
    private String mPrevInterest;
    private String mInterest = "";

    public void setInterest(String interest) {
        mSelected = getIntForInterestString(interest);
        mPrevInterest = mInterest;
    }

    private int getIntForInterestString(String walkingSpeed) {
        if(walkingSpeed.equals(AppSettings.WANDER_INTEREST_HISTORY)) {
            mInterest = walkingSpeed;
            return 0;
        }
        else if(walkingSpeed.equals(AppSettings.WANDER_INTEREST_GEOGRAPHY)) {
            mInterest = walkingSpeed;
            return 1;
        }
        else if(walkingSpeed.equals(AppSettings.WANDER_INTEREST_RELIGION)) {
            mInterest = walkingSpeed;
            return 2;
        }
        else if(walkingSpeed.equals(AppSettings.WANDER_INTEREST_CULTURAL)) {
            mInterest = walkingSpeed;
            return 3;
        }
        else {
            mInterest = AppSettings.WANDER_INTEREST_HISTORY;
            return 0;
        }
    }

    // Override the Fragment.onAttach() method to instantiate the NoticeDialogListener
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (OnInterestChangeListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String[] values = {getString(R.string.interest_history), getString(R.string.interest_geography), getString(R.string.interest_religion), getString(R.string.interest_cultural)};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle(R.string.interest);

        builder.setSingleChoiceItems(values, mSelected, this);

        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if(!mPrevInterest.equals(mInterest)) {
                    mListener.onInterestChange(mInterest);
                }
                // User clicked OK button
            }
        });

        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
            }
        });

        return builder.create();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch(which) {
            case 0:
                mInterest = AppSettings.WANDER_INTEREST_HISTORY;
                break;
            case 1:
                mInterest = AppSettings.WANDER_INTEREST_GEOGRAPHY;
                break;
            case 2:
                mInterest = AppSettings.WANDER_INTEREST_RELIGION;
                break;
            case 3:
                mInterest = AppSettings.WANDER_INTEREST_CULTURAL;
                break;
            default:
                break;
        }
    }

    public interface OnInterestChangeListener {
        void onInterestChange(String interest);
    }
}
