package regionale.whs.wanderapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RatingBar;

import java.util.Date;

import regionale.whs.wanderapp.R;
import regionale.whs.wanderapp.objects.POI;
import regionale.whs.wanderapp.objects.Route;
import regionale.whs.wanderapp.service.WanderWSClient;
import regionale.whs.wanderapp.wspojos.WanderKommentar;

/**
 * Activity for creating a new comment and persisting it to the server.
 */
public class NewCommentActivity extends StandardActivity {

    private POI mPOI;
    private Route mRoute;
    private WanderKommentar mComment;
    private boolean isPOI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_comment);

        RatingBar ratingBar = (RatingBar) findViewById(R.id.ratingBar);
        ratingBar.setMax(5);
        ratingBar.setStepSize(1);

        Intent intent = getIntent();
        isPOI = intent.getBooleanExtra("isPOI", false);

        if(isPOI)
            mPOI = intent.getParcelableExtra("poi");
        else
            mRoute = intent.getParcelableExtra("routes");
    }

    public POI getPOI() {
        return mPOI;
    }

    public void setPOI(POI mPOI) {
        this.mPOI = mPOI;
    }

    public Route getRoute() {
        return mRoute;
    }

    public void setRoute(Route mRoute) {
        this.mRoute = mRoute;
    }

    public WanderKommentar getComment() {
        return mComment;
    }

    public void setComment(WanderKommentar mComment) {
        this.mComment = mComment;
    }

    /**
     * Method called by clicking the submit button.
     * Creates a WanderKommentar-object from the user input and user data and sends it to the server.
     * After that, calls the POIDetailActivity to update the comment list.
     * @param view view tha twas clicked
     */
    public void submitComment(View view)
    {
        WanderKommentar wanderComment = new WanderKommentar();

        EditText commentView = (EditText) findViewById(R.id.commentText);
        RatingBar ratingBar = (RatingBar) findViewById(R.id.ratingBar);

        String commentText = commentView.getText().toString();
        int rating = Math.round(ratingBar.getRating());

        wanderComment.setText(commentText);
        wanderComment.setAuthorId(WanderWSClient.getInstance().getCurrentUser().getId());

        if(isPOI) {
            wanderComment.setPlaceId(mPOI.getId());
        } else {
            wanderComment.setRouteId(mRoute.getId());
        }

        wanderComment.setTime(new Date());
        wanderComment.setRating(rating);

        WanderWSClient.getInstance().setKommentar(wanderComment, this, true);

        // TODO: clear last two backstack elements or find better way

        if(isPOI) {
            Intent intent = new Intent(this, POIDetailActivity.class);
            intent.putExtra("poi", mPOI);
            startActivity(intent);
        }
        else {
            Intent intent = new Intent(this, RouteDetailActivity.class);
            intent.putExtra("route", mRoute);
            startActivity(intent);
        }
    }
}
