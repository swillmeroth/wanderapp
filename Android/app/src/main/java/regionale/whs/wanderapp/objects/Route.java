package regionale.whs.wanderapp.objects;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import org.mapsforge.core.model.BoundingBox;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import regionale.whs.wanderapp.R;
import regionale.whs.wanderapp.objects.enums.RouteType;
import regionale.whs.wanderapp.service.WanderWSClient;
import regionale.whs.wanderapp.tools.AppSettings;
import regionale.whs.wanderapp.tools.GeoCalc;
import regionale.whs.wanderapp.wspojos.GeoLocation;
import regionale.whs.wanderapp.wspojos.WanderDescription;
import regionale.whs.wanderapp.wspojos.WanderRoute;
import regionale.whs.wanderapp.wspojos.WanderUser;

/**
 * Object representing a route.
 *
 * Created by Stephan on 17.10.2014.
 */
public class Route implements Parcelable
{

    public static final Parcelable.Creator<Route> CREATOR = new Parcelable.Creator<Route>()
    {
        public Route createFromParcel(Parcel in)
        {
            return new Route(in);
        }

        public Route[] newArray(int size)
        {
            return new Route[size];
        }
    };

    public Route()
    {
        initialize();
    }

    private int id;
    private String name;
    private RouteType type;
    private int duration;
    private int distance;
    private int length;
    private int drawableId;
    private float rating;
    private String description; // TODO: klaeren, was ich bekomme
    private String accessibility_info;
    private String transportation_info;
    private String equipment;
    private ArrayList<RoutePoint> routePoints;
    private RoutePoint startPoint;
    private RoutePoint endPoint;
    private BoundingBox boundingBox;
    private ArrayList<POI> pois;

    private int difficulty;
    private int commentCount;
    private int todosCount;

    public int lastPointIndex;

    public Route(int id, String name, RouteType type, int duration, int distance, int length, int drawableId, float rating, int commentCount, String description, String accessibility_info, String transportation_info, String equipment, ArrayList<RoutePoint> routePoints, RoutePoint startPoint, RoutePoint endPoint, BoundingBox boundingBox, ArrayList<POI> pois)
    {
        initialize();

        this.id = id;
        this.name = name;
        this.type = type;
        this.duration = duration;
        this.distance = distance;
        this.length = length;
        this.drawableId = drawableId;
        this.rating = rating;
        this.commentCount = commentCount;
        this.description = description;
        this.accessibility_info = accessibility_info;
        this.transportation_info = transportation_info;
        this.equipment = equipment;
        this.routePoints = routePoints;
        this.startPoint = startPoint;
        this.endPoint = endPoint;
        this.boundingBox = boundingBox;
        this.pois = pois;
        calcBoundingBox();
    }

    public Route(String name, RouteType type, int duration, int distance, int length, int drawableId, ArrayList<RoutePoint> routePoints, RoutePoint startPoint, RoutePoint endPoint)
    {
        initialize();

        this.name = name;
        this.type = type;
        this.duration = duration;
        this.distance = distance;
        this.length = length;
        this.drawableId = drawableId;
        this.routePoints = routePoints;
        this.startPoint = startPoint;
        this.endPoint = endPoint;

        calcBoundingBox();
    }

    public Route( WanderRoute wanderRoute)
    {
        initialize();

        this.id = wanderRoute.getId();
        this.name = wanderRoute.getName();
        this.type = RouteType.valueOf(wanderRoute.getRouteType());
        this.duration = wanderRoute.getDuration();
        this.length = wanderRoute.getDistance();
        this.rating = wanderRoute.getRating();
        this.commentCount = wanderRoute.getCommentCount();
        //TODO: this.description = wanderRoute.getD;
        //TODO: this.accessibility_info = wanderRoute.get;
        //TODO: this.transportation_info = wanderRoute.get;
        this.equipment = wanderRoute.getEquipment();
        GeoLocation point = wanderRoute.getRoutePoints().get(0);
        this.startPoint = RoutePoint.fromGeoLocation(point);
        point = wanderRoute.getRoutePoints().get(wanderRoute.getRoutePoints().size() - 1);
        this.endPoint = RoutePoint.fromGeoLocation(point);
        this.routePoints = RoutePoint.fromGeoLocationList(wanderRoute.getRoutePoints());
        this.pois = new ArrayList<>(); //TODO: get POIs
        this.lastPointIndex = 0;
        this.boundingBox = new BoundingBox(0, 0, 0, 0);

        this.difficulty = wanderRoute.getDifficulty();
        this.todosCount = wanderRoute.getTodosCount();


        if(wanderRoute.getDescriptions().size() > 0) {
            this.description = wanderRoute.getDescriptions().get(0).getText();

            WanderUser user = WanderWSClient.getInstance().getCurrentUser();
            if(user != null) {
                for (WanderDescription description : wanderRoute.getDescriptions()) {
                    if(user.getLanguage().equals(description.getLanguage()) && user.getInterest().equals(description.getInterest()))
                        this.description = description.getText();
                }
            }
        }

        calcBoundingBox();
    }

    private Route(Parcel in)
    {
        /*
        out.writeInt(this.id);
        out.writeString(this.name);
        out.writeString(this.type.toString());
        out.writeInt(this.duration);
        out.writeInt(this.length);
        out.writeInt(this.drawableId);
        out.writeDouble(this.rating);
        out.writeInt(this.commentCount);
        out.writeString(this.description);
        out.writeString(this.accessibility_info);
        out.writeString(this.transportation_info);
        out.writeString(this.equipment);
        out.writeDouble(this.boundingBox.minLatitude);
        out.writeDouble(this.boundingBox.maxLatitude);
        out.writeDouble(this.boundingBox.minLongitude);
        out.writeDouble(this.boundingBox.maxLongitude);
        out.writeParcelable(this.startPoint, 0);
        out.writeParcelable(this.endPoint, 0);
        out.writeTypedList(this.routePoints);
        out.writeTypedList(this.pois);
        out.writeInt(this.difficulty);
        out.writeInt(this.todosCount);
         */
        initialize();

        this.id = in.readInt();
        this.name = in.readString();
        this.type = RouteType.valueOf(in.readString());
        this.duration = in.readInt();
        this.length = in.readInt();
        this.drawableId = in.readInt();
        this.rating = in.readFloat();
        this.commentCount = in.readInt();
        this.description = in.readString();
        this.accessibility_info = in.readString();
        this.transportation_info = in.readString();
        this.equipment = in.readString();

        double lat_min, lat_max, lon_min, lon_max;
        lat_min = in.readDouble();
        lat_max = in.readDouble();
        lon_min = in.readDouble();
        lon_max = in.readDouble();
        this.boundingBox = new BoundingBox(lat_min, lon_min, lat_max, lon_max);

        this.startPoint = in.readParcelable(RoutePoint.class.getClassLoader());
        this.endPoint = in.readParcelable(RoutePoint.class.getClassLoader());
        in.readTypedList(this.routePoints, RoutePoint.CREATOR);
        in.readTypedList(this.pois, POI.CREATOR);
    }

    private void initialize()
    {
        this.id = 0;
        this.name = "";
        this.type = RouteType.path;
        this.duration = 0;
        this.distance = 0;
        this.length = 0;
        this.drawableId = 0;
        this.rating = 0;
        this.commentCount = 0;
        this.description = "";
        this.accessibility_info = "";
        this.transportation_info = "";
        this.equipment = "";
        this.startPoint = new RoutePoint(0, 0);
        this.endPoint = new RoutePoint(0, 0);
        this.routePoints = new ArrayList<>();
        this.pois = new ArrayList<>();
        this.lastPointIndex = 0;
        this.boundingBox = new BoundingBox(0, 0, 0, 0);

        this.difficulty = 0;
        this.todosCount = 0;
    }

    public static ArrayList<Route> convertWanderRoutes(List<WanderRoute> wanderRoutes)
    {
        ArrayList<Route> routes = new ArrayList<>();

        for( WanderRoute wanderRoute : wanderRoutes)
        {
            routes.add(new Route(wanderRoute));
        }

        return routes;
    }

    public String getDurationString()
    {
        int hours = duration / 60;
        int minutes = duration % 60;
        return String.format("%d:%02d h", hours, minutes);
    }

    public String getDistanceString()
    {
        return AppSettings.getDistanceString(distance, Locale.getDefault());
    }

    public BoundingBox calcBoundingBox()
    {
        double minLat, minLon, maxLat, maxLon;

        if(routePoints == null || routePoints.size() <= 0)
            return null;
        else {
            RoutePoint point = routePoints.get(0);
            minLat = point.getLatitude();
            maxLat = point.getLatitude();
            minLon = point.getLongitude();
            maxLon = point.getLongitude();
        }

        for(int i = 1; i < routePoints.size(); i++)
        {
            RoutePoint point = routePoints.get(i);
            double lat = point.getLatitude();
            double lon = point.getLongitude();

            if( lat < minLat) minLat = lat;
            else if( lat > maxLat) maxLat = lat;

            if( lon < minLon) minLon = lon;
            else if( lon > maxLon) maxLon = lon;
        }

        this.boundingBox = new BoundingBox(minLat, minLon, maxLat, maxLon);

        return this.boundingBox;
    }

    public String getLengthString()
    {
        return AppSettings.getDistanceString(length, Locale.getDefault());
    }

    public static String getRatingString(Route route, Context context)
    {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append(AppSettings.RATING_FORMAT.format(route.getRating()));
        stringBuilder.append(" (");
        stringBuilder.append(route.getCommentCount());
        stringBuilder.append(" ");
        stringBuilder.append(context.getString(R.string.ratings));
        stringBuilder.append(")");

        return stringBuilder.toString();
    }

    public int getLength()
    {
        return length;
    }

    public void setLength(int length)
    {
        this.length = length;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public int getDuration()
    {
        return duration;
    }

    public void setDuration(int duration)
    {
        this.duration = duration;
    }

    public int getDistance()
    {
        return distance;
    }

    public void setDistance(int distance)
    {
        this.distance = distance;
    }

    public int getDrawableId()
    {
        return drawableId;
    }

    public void setDrawableId(int drawableId)
    {
        this.drawableId = drawableId;
    }

    public ArrayList<RoutePoint> getRoutePoints()
    {
        return routePoints;
    }

    public BoundingBox getBoundingBox()
    {
        return boundingBox;
    }

    public void setBoundingBox(BoundingBox boundingBox)
    {
        this.boundingBox = boundingBox;
    }

    public RoutePoint getStartPoint()
    {
        return startPoint;
    }

    public void setStartPoint(RoutePoint startPoint)
    {
        this.startPoint = startPoint;
    }

    public RoutePoint getEndPoint()
    {
        return endPoint;
    }

    public void setEndPoint(RoutePoint endPoint)
    {
        this.endPoint = endPoint;
    }

    public double getRating()
    {
        return rating;
    }

    public void setRating(float rating)
    {
        this.rating = rating;
    }

    public int getTodosCount() {
        return todosCount;
    }

    public void setTodosCount(int todosCount) {
        this.todosCount = todosCount;
    }

    public int getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(int commentCount) {
        this.commentCount = commentCount;
    }

    public int getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(int difficulty) {
        this.difficulty = difficulty;
    }

    public ArrayList<POI> getPois() {
        return pois;
    }

    public String getEquipment() {
        return equipment;
    }

    public void setEquipment(String equipment) {
        this.equipment = equipment;
    }

    public String getTransportation_info() {
        return transportation_info;
    }

    public void setTransportation_info(String transportation_info) {
        this.transportation_info = transportation_info;
    }

    public String getAccessibility_info() {
        return accessibility_info;
    }

    public void setAccessibility_info(String accessibility_info) {
        this.accessibility_info = accessibility_info;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getAccessibilityInfo()
    {
        return accessibility_info;
    }

    public void setAccessibilityInfo(String accessibility_info)
    {
        this.accessibility_info = accessibility_info;
    }

    public String getTransportationInfo()
    {
        return transportation_info;
    }

    public void setTransportationInfo(String transportation_info)
    {
        this.transportation_info = transportation_info;
    }

    public String getEquipmentInfo()
    {
        return equipment;
    }

    public void setEquipmentInfo(String equipment_info)
    {
        this.equipment = equipment_info;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public RouteType getType()
    {
        return type;
    }

    public void setType(RouteType type)
    {
        this.type = type;
    }

    public ArrayList<POI> getPOIs()
    {
        return pois;
    }

    public void setPois(ArrayList<POI> pois)
    {
        this.pois = pois;
    }

    public void addPOI(POI poi)
    {
        this.pois.add(poi);
    }

    public void addRoutePoint(RoutePoint routePoint)
    {
        if (this.routePoints == null)
            this.routePoints = new ArrayList<RoutePoint>();

        routePoints.add(routePoint);
    }

    public RoutePoint getNextRoutePoint()
    {
        RoutePoint routePoint = this.routePoints.get(this.lastPointIndex);
        this.lastPointIndex++;

        return routePoint;
    }

    public void setRoutePoints(ArrayList<RoutePoint> routePoints)
    {
        this.routePoints = routePoints;
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags)
    {
        out.writeInt(this.id);
        out.writeString(this.name);
        out.writeString(this.type.toString());
        out.writeInt(this.duration);
        out.writeInt(this.length);
        out.writeInt(this.drawableId);
        out.writeFloat(this.rating);
        out.writeInt(this.commentCount);
        out.writeString(this.description);
        out.writeString(this.accessibility_info);
        out.writeString(this.transportation_info);
        out.writeString(this.equipment);
        out.writeDouble(this.boundingBox.minLatitude);
        out.writeDouble(this.boundingBox.maxLatitude);
        out.writeDouble(this.boundingBox.minLongitude);
        out.writeDouble(this.boundingBox.maxLongitude);
        out.writeParcelable(this.startPoint, 0);
        out.writeParcelable(this.endPoint, 0);
        out.writeTypedList(this.routePoints);
        out.writeTypedList(this.pois);
        out.writeInt(this.difficulty);
        out.writeInt(this.todosCount);
    }

    /**
     * Calculates the distance between the Routes coordinates and
     * the given parameters. Includes altitude.
     * By doing this also sets the distance variable in
     * Route instance.
     *
     * @param lat_home  latitude
     * @param long_home longitude
     * @param alt       altitude
     */
    public void calcDistance(double lat_home, double long_home, int alt)
    {
        if (startPoint == null)
            return;

        int result = distance;
        double latitude = startPoint.getLatitude();
        double longitude = startPoint.getLongitude();

        if (latitude != 0 && longitude != 0)
        {
            result = (int) Math.round(GeoCalc.calcDistance(latitude, longitude, lat_home, long_home));
        }

        distance = result;
    }
}
