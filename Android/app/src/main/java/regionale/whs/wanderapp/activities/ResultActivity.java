package regionale.whs.wanderapp.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.widget.ArrayAdapter;

import java.util.ArrayList;
import java.util.Collections;

import regionale.whs.wanderapp.R;
import regionale.whs.wanderapp.adapter.POIResultAdapter;
import regionale.whs.wanderapp.adapter.ResultTabsPagerAdapter;
import regionale.whs.wanderapp.adapter.RouteResultAdapter;
import regionale.whs.wanderapp.fragments.ResultListFragment;
import regionale.whs.wanderapp.objects.POI;
import regionale.whs.wanderapp.objects.Route;
import regionale.whs.wanderapp.objects.enums.QueryType;
import regionale.whs.wanderapp.service.WanderWSClient;
import regionale.whs.wanderapp.tools.comparators.SortPOIDistance;
import regionale.whs.wanderapp.tools.comparators.SortPOIName;
import regionale.whs.wanderapp.tools.comparators.SortRouteDistance;
import regionale.whs.wanderapp.tools.comparators.SortRouteDuration;
import regionale.whs.wanderapp.tools.comparators.SortRouteLength;
import regionale.whs.wanderapp.tools.comparators.SortRouteName;

/**
 * method that performs the requested search and displays it's results as a list. This list is embedded in a tabbed layout to be able to sort the results.
 */
public class ResultActivity extends StandardActivity implements ActionBar.TabListener, ResultListFragment.OnPOISelectedListener, ResultListFragment.OnRouteSelectedListener, LocationListener, DialogInterface.OnClickListener, StandardActivity.ProposeNewListener {
    boolean isRouteSearch;
    QueryType queryType;
    ArrayList<Route> routeResults;
    ArrayList<POI> poiResults;
    private ViewPager viewPager;
    private ActionBar actionBar;
    private LocationManager locationManager;
    private Criteria criteria;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        /* TODO: make dialogs to propose a new POI or Route
        * setProposeNewListener(this);
        * showNewProposalItem(true);
        */

        isRouteSearch = this.getIntent().getExtras().getBoolean("isRouteSearch");
        queryType = QueryType.valueOf(this.getIntent().getExtras().getString("queryType"));

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        criteria = new Criteria();

        locationManager.requestLocationUpdates(1000, 1, criteria, this, null);

        handleQuery();

        // Initilization
        viewPager = (ViewPager) findViewById(R.id.pager);
        actionBar = getSupportActionBar();

        actionBar.setHomeButtonEnabled(false);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        if (isRouteSearch)
            createRouteTabs();
        else
            createPOITabs();

        // Listener to select the correct tab when changing pages by swiping
        viewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener()
        {
            @Override
            public void onPageSelected(int position)
            {
                actionBar.setSelectedNavigationItem(position);
            }
        });
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction)
    {
        viewPager.setCurrentItem(tab.getPosition());

        if (isRouteSearch)
        {
            // sort results
            switch (tab.getPosition())
            {
                case 0:
                    Collections.sort(routeResults, new SortRouteDuration());
                    break;
                case 1:
                    Collections.sort(routeResults, new SortRouteLength());
                    break;
                case 2:
                    Collections.sort(routeResults, new SortRouteName());
                    break;
                case 3:
                    Collections.sort(routeResults, new SortRouteDistance());
                    break;
            }
            ArrayAdapter adapter = new RouteResultAdapter(this, routeResults);
            setAdapter(adapter);
        } else
        {
            // sort results
            switch (tab.getPosition())
            {
                case 0:
                    Collections.sort(poiResults, new SortPOIName());
                    break;
                case 1:
                    Collections.sort(poiResults, new SortPOIDistance());
                    break;
            }

            ArrayAdapter adapter = new POIResultAdapter(this, poiResults);
            setAdapter(adapter);
        }
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction)
    {

    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction)
    {

    }

    // If poi is selected
    @Override
    public void onPOISelected(POI poi)
    {
        Intent intent = new Intent(this, POIDetailActivity.class);
        intent.putExtra("poi", poi);
        startActivity(intent);
    }

    // If route is selected
    @Override
    public void onRouteSelected(Route route)
    {
        Intent intent = new Intent(this, RouteDetailActivity.class);
        intent.putExtra("route", route);
        startActivity(intent);
    }

    private void createRouteTabs()
    {
        ArrayAdapter adapter = new RouteResultAdapter(this, routeResults);
        setAdapter(adapter);

        actionBar.addTab(actionBar.newTab().setText(R.string.duration).setTabListener(this));
        actionBar.addTab(actionBar.newTab().setText(R.string.length).setTabListener(this));
        actionBar.addTab(actionBar.newTab().setText(R.string.name).setTabListener(this));
        actionBar.addTab(actionBar.newTab().setText(R.string.distance).setTabListener(this));
    }

    private void createPOITabs()
    {
        ArrayAdapter adapter = new POIResultAdapter(this, poiResults);
        setAdapter(adapter);

        actionBar.addTab(actionBar.newTab().setText(R.string.name).setTabListener(this));
        actionBar.addTab(actionBar.newTab().setText(R.string.distance).setTabListener(this));
    }

    // sets the PagerAdapter and passes the POI/RouteAdapter to it
    private void setAdapter(ArrayAdapter adapter)
    {
        ResultTabsPagerAdapter mAdapter = new ResultTabsPagerAdapter(getSupportFragmentManager(), isRouteSearch);
        mAdapter.setAdapter(adapter);
        int current_item = viewPager.getCurrentItem();
        viewPager.setAdapter(mAdapter);
        viewPager.setCurrentItem(current_item);
    }

    /**
     * method that handles the queries with their parameters
     */
    private void handleQuery()
    {
        switch (queryType)
        {
            case NAME:
                handleNameQuery();
                break;
            case TYPE:
                handleTypeQuery();
                break;
            default:
        }
    }

    /**
     * performs a query by type
     */
    private void handleTypeQuery()
    {
        ArrayList<String> types = this.getIntent().getStringArrayListExtra("types");

        // get results from server
        WanderWSClient wanderWSClient = WanderWSClient.getInstance();

        if (isRouteSearch)
            //routeResults = DummyResults.getDummyRoutes(this);
            routeResults = Route.convertWanderRoutes(wanderWSClient.searchRoutesByType(types, this, true));
        else
            poiResults = POI.convertWanderPOIs(wanderWSClient.searchPOIsByType(types, this, true));

        if(isRouteSearch && ( routeResults == null || (routeResults != null && routeResults.size() == 0))) {
            routeResults = new ArrayList<>();
            showNoResultsError();
        }

        if(!isRouteSearch && ( poiResults == null || (poiResults != null && poiResults.size() == 0))) {
            poiResults = new ArrayList<>();
            showNoResultsError();
        }

        Location location = locationManager.getLastKnownLocation(locationManager.getBestProvider(criteria, true));
        calcDistances(location);
    }

    /**
     * performs a query by name
     */
    private void handleNameQuery()
    {
        // set title
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(getString(R.string.results_for));
        stringBuilder.append(" \"");
        String searchString = this.getIntent().getStringExtra("searchString");
        stringBuilder.append(searchString);
        stringBuilder.append("\"");
        setTitle(stringBuilder.toString());

        // get results from server
        WanderWSClient wanderWSClient = WanderWSClient.getInstance();

        if (isRouteSearch)
            routeResults = Route.convertWanderRoutes(wanderWSClient.searchRoutesByName(searchString, this, true));
        else
            poiResults = POI.convertWanderPOIs(wanderWSClient.searchPOIsByName(searchString, this, true));

        if(isRouteSearch && routeResults == null) {
            routeResults = new ArrayList<>();
            showNoResultsError();
        }

        if(!isRouteSearch && poiResults == null) {
            poiResults = new ArrayList<>();
            showNoResultsError();
        }

        Location location = locationManager.getLastKnownLocation(locationManager.getBestProvider(criteria, true));
        calcDistances(location);
    }

    /**
     * shows an error dialog informing the user that no results were returned
     */
    private void showNoResultsError() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.error);
        builder.setMessage(R.string.no_results);
        builder.setPositiveButton(R.string.ok, this);
        builder.create().show();
    }

    /**
     * Method that gets dummy results

    private void getDummyResults()
    {
        if (isRouteSearch)
            routeResults = DummyResults.getDummyRoutes(this);
        else
            poiResults = DummyResults.getDummyPOIs();
    }*/

    /**
     * gets a location and calcs the distance of each POI or route to that location
     * @param location location to use
     */
    private void calcDistances(Location location)
    {
        double latitude = 0;
        double longitude = 0;
        int altitude = 0;

        if (location != null)
        {
            latitude = location.getLatitude();
            longitude = location.getLongitude();
            altitude = (int) location.getAltitude();
        }

        if (isRouteSearch)
        {
            for (Route route : routeResults)
            {
                route.calcDistance(latitude, longitude, altitude);
            }
        } else
        {
            for (POI poi : poiResults)
            {
                poi.calcDistance(latitude, longitude, altitude);
            }
        }
    }

    @Override
    public void onLocationChanged(Location location)
    {
        calcDistances(location);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras)
    {

    }

    @Override
    public void onProviderEnabled(String provider)
    {

    }

    @Override
    public void onProviderDisabled(String provider)
    {

    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        dialog.dismiss();
        onBackPressed();
    }

    @Override
    public void onNewProposal() {
        // TODO: set intent and start proposalactivity
    }
}
