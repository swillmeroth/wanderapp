package regionale.whs.wanderapp.fragments.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import regionale.whs.wanderapp.R;

/**
 * Dialog for changing the user's email
 */
public class EmailDialog extends DialogFragment {

    private OnMailChangeListener mListener;
    private String mPrevMail;
    private String mMail = "";
    private EditText mEditText;
    private AlertDialog mDialog;

    public void setMail(String email, boolean changePrev) {
        mMail = email;

        if(changePrev)
            mPrevMail = mMail;
    }

    @Override
    public void onAttach(Activity activity) {
        System.out.println("attaching");
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (OnMailChangeListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_email, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle(R.string.email);

        builder.setView(v);
        mEditText = (EditText) v.findViewById(R.id.email);
        mEditText.setText(mMail);

        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(!mPrevMail.equals(mEditText.getText().toString()))
                    mListener.onMailChange(mEditText.getText().toString());
            }
        });

        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
            }
        });

        mDialog = builder.create();
        return mDialog;
    }

    public interface OnMailChangeListener {
        void onMailChange(String mail);
    }
}
