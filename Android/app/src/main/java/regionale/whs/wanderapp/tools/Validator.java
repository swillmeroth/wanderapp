package regionale.whs.wanderapp.tools;

/**
 * Validator used during login and register.
 *
 * Created by Stephan on 08.04.2015.
 */
public class Validator {

    public static boolean isEmailValid(String email)
    {
        return (email.contains("@") && email.length() >= 5 && email.length() < 255);
    }

    public static boolean isPasswordValid(String password)
    {
        return (password.length() >= 5 && password.length() <= 255);
    }

    public static boolean isUsernameValid(String username)
    {
        return (username.length() >= 5 && username.length() <= 255);
    }
}
