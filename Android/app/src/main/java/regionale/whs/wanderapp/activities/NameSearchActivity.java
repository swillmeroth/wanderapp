package regionale.whs.wanderapp.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;

import regionale.whs.wanderapp.R;
import regionale.whs.wanderapp.objects.enums.QueryType;

/**
 * Activity for searching for a poi or a route by name. Gets isRouteSearch from Intent.
 */
public class NameSearchActivity extends StandardActivity
{

    boolean isRouteSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_name_search);

        isRouteSearch = this.getIntent().getExtras().getBoolean("isRouteSearch");
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);

        EditText editText = (EditText) findViewById(R.id.searchfield);
        editText.setOnKeyListener(new View.OnKeyListener()
        {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event)
            {
                if (event.getAction() == KeyEvent.ACTION_DOWN)
                {
                    switch (keyCode)
                    {
                        case KeyEvent.KEYCODE_DPAD_CENTER:
                        case KeyEvent.KEYCODE_ENTER:
                            onSearchClick(v);
                            return true;
                        default:
                            break;
                    }
                }
                return false;
            }
        });
    }

    /**
     * called when search is clicked. Gets the input and checks for empty. If empty, shows an alert to the user.
     * Otherwise the settings are put to intent and ResultActivity is started.
      * @param view
     */
    public void onSearchClick(View view)
    {
        EditText editText = (EditText) findViewById(R.id.searchfield);
        String searchString = editText.getText().toString();
        if (searchString.equals("")) // no query entered
        {
            // build a message and show it
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(R.string.please_enter_query);
            builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener()
            {
                public void onClick(DialogInterface dialog, int id)
                {
                    dialog.dismiss();
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        } else
        {

            // start result activity with needed info
            Intent intent = new Intent(this, ResultActivity.class);
            intent.putExtra("isRouteSearch", isRouteSearch);
            intent.putExtra("searchString", searchString);
            intent.putExtra("queryType", QueryType.NAME.toString());

            startActivity(intent);
        }
    }
}
