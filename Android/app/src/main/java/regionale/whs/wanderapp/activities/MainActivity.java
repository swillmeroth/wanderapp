package regionale.whs.wanderapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import regionale.whs.wanderapp.R;
import regionale.whs.wanderapp.objects.enums.MapType;

/**
 * After logging in or continuing as a guest, this activity is called. It consists of the logo and three buttons:
 *  - search: Search for POIs and Routes
 *  - sightseeing: create a dynamic route
 *  - map: starts the map and shows the surrounding pois and routes
 */
public class MainActivity extends StandardActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /**
     * Starts SearchActivity
     * @param view
     */
    public void onSearchClick(View view)
    {
        Intent intent = new Intent(this, SearchActivity.class);
        startActivity(intent);
    }

    /**
     * Starts DynamicRouteActivity
     * @param view
     */
    public void onSightseeingClick(View view)
    {
        Intent intent = new Intent(this, DynamicRouteActivity.class);
        startActivity(intent);
    }

    /**
     * Starts MapActivity. Sets maptype to Neigborhhod for use in MapFragment.
     * @param view
     */
    public void onMapClick(View view)
    {
        Intent intent = new Intent(this, MapActivity.class);
        intent.putExtra("maptype", MapType.NEIGHBORHOOD.toString());
        startActivity(intent);
    }
}
