package regionale.whs.wanderapp.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.widget.ArrayAdapter;

import regionale.whs.wanderapp.fragments.ResultListFragment;

/**
 * Adapter for the tabs used in the result list.
 *
 * Created by Stephan on 17.10.2014.
 */
public class ResultTabsPagerAdapter extends FragmentPagerAdapter
{

    int count;
    boolean isRouteSearch;
    ArrayAdapter adapter;

    public ResultTabsPagerAdapter(FragmentManager fm, boolean isRouteSearch)
    {
        super(fm);

        this.isRouteSearch = isRouteSearch;

        if (isRouteSearch)
            count = 4;
        else
            count = 2;
    }

    public ArrayAdapter getAdapter()
    {
        return adapter;
    }

    public void setAdapter(ArrayAdapter adapter)
    {
        this.adapter = adapter;
    }

    @Override
    public Fragment getItem(int index)
    {

        if (index < 4)
        {
            return ResultListFragment.newInstance(adapter, isRouteSearch);
        }

        return null;
    }

    @Override
    public int getCount()
    {
        // get item count - equal to number of tabs
        return count;
    }

}
