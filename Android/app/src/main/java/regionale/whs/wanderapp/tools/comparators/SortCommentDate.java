package regionale.whs.wanderapp.tools.comparators;

import java.util.Comparator;

import regionale.whs.wanderapp.wspojos.WanderKommentar;

/**
 * sorts a comment by time
 *
 * Created by Stephan on 21.11.2014.
 */
public class SortCommentDate implements Comparator<WanderKommentar>
{
    @Override
    public int compare(WanderKommentar comment1, WanderKommentar comment2)
    {
        return comment1.getTime().compareTo(comment2.getTime());
    }
}
