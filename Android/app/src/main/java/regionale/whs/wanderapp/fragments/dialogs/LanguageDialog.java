package regionale.whs.wanderapp.fragments.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

import regionale.whs.wanderapp.R;
import regionale.whs.wanderapp.tools.AppSettings;

/**
 * Dialog for changing the user's language
 */
public class LanguageDialog extends DialogFragment implements DialogInterface.OnClickListener {

    OnLanguageChangeListener mListener;
    private int mSelected = 0;
    private String mPrevLanguage;
    private String mLanguage = "";

    public void setLanguage(String language) {
        mSelected = getIntForLanguageString(language);
        mPrevLanguage = mLanguage;
    }

    private int getIntForLanguageString(String language) {
        if(language.equals(AppSettings.WANDER_LANGUAGE_GERMAN)) {
            mLanguage = language;
            return 0;
        }
        else if(language.equals(AppSettings.WANDER_LANGUAGE_ENGLISH)) {
            mLanguage = language;
            return 1;
        }
        else {
            mLanguage = AppSettings.WANDER_LANGUAGE_ENGLISH;
            return 0;
        }
    }

    // Override the Fragment.onAttach() method to instantiate the NoticeDialogListener
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (OnLanguageChangeListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String[] values = {getString(R.string.language_german), getString(R.string.language_english)};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle(R.string.language);

        builder.setSingleChoiceItems(values, mSelected, this);

        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if(!mPrevLanguage.equals(mLanguage)) {
                    mListener.onLanguageChange(mLanguage);
                }
                // User clicked OK button
            }
        });

        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
            }
        });

        return builder.create();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch(which) {
            case 0:
                mLanguage = AppSettings.WANDER_LANGUAGE_GERMAN;
                break;
            case 1:
                mLanguage = AppSettings.WANDER_LANGUAGE_ENGLISH;
                break;
            default:
                break;
        }
    }

    public interface OnLanguageChangeListener {
        void onLanguageChange(String language);
    }
}
