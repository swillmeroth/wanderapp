package regionale.whs.wanderapp.objects.enums;

/**
 * Type for the dynamic route to request.
 */
public enum DynamicRouteType
{
    loop,
    path
}
