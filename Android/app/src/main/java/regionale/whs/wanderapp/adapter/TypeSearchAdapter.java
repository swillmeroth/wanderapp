package regionale.whs.wanderapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.ArrayList;

import regionale.whs.wanderapp.R;
import regionale.whs.wanderapp.tools.SelectableString;

/**
 * Adapter for the list of types that can be selected in TypeSearchActivity.
 *
 * Created by Stephan on 27.11.2014.
 */
public class TypeSearchAdapter extends ArrayAdapter<SelectableString>
{
    public TypeSearchAdapter(Context context, ArrayList<SelectableString> types)
    {
        super(context, 0, types);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        // Get the data item for this position
        final SelectableString pair = getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null)
        {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_type, parent, false);
        }

        // Lookup view for data population
        TextView view_name = (TextView) convertView.findViewById(R.id.typeText);
        CheckBox view_checkbox = (CheckBox) convertView.findViewById(R.id.checkBox);

        /*// set listener for handling checks
        view_checkbox.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                CheckBox checkBox = (CheckBox)v;

                // debug
                boolean selected = checkBox.isSelected();
                if(selected)
                    Toast.makeText(getContext(), "Selected: " + pair.getString(), Toast.LENGTH_SHORT);
                else
                    Toast.makeText(getContext(), "Unselected: " + pair.getString(), Toast.LENGTH_SHORT);

                pair.setSelected(checkBox.isSelected());
            }
        });*/

        // Populate the data into the template view using the data object
        view_name.setText(pair.getString());
        view_checkbox.setChecked(pair.isSelected());

        // Return the completed view to render on screen
        return convertView;
    }


}
