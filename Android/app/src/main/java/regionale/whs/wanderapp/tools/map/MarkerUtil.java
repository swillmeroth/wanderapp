package regionale.whs.wanderapp.tools.map;

import android.content.Context;
import android.graphics.drawable.Drawable;

import regionale.whs.wanderapp.R;
import regionale.whs.wanderapp.objects.enums.POIType;

/**
 * Utility class for getting the right drawable for a poi type.
 *
 * Created by Stephan on 31.03.2015.
 */
public class MarkerUtil {
    public static Drawable getDrawableForPOIType(POIType poiType, Context context)
    {
        switch (poiType)
        {
            case CHAPEL:
                return context.getResources().getDrawable(R.drawable.chapel);
            case ABBEY:
                return context.getResources().getDrawable(R.drawable.abbey);
            case CHURCH:
                return context.getResources().getDrawable(R.drawable.church);
            case CROSS:
                return context.getResources().getDrawable(R.drawable.cross);
            case GRAVEYARD:
                return context.getResources().getDrawable(R.drawable.graveyard);
            case JEWISH_GRAVEYARD:
                return context.getResources().getDrawable(R.drawable.jewish_graveyard);
            case MONUMENT:
                return context.getResources().getDrawable(R.drawable.monument);
            case STATUE:
                return context.getResources().getDrawable(R.drawable.statue);
            default:
                return context.getResources().getDrawable(R.drawable.star);
        }
    }

    public static Drawable getDrawableForPOITypeFollowing(POIType poiType, Context context)
    {
        switch (poiType)
        {
            case CHAPEL:
                return context.getResources().getDrawable(R.drawable.chapel_following);
            case ABBEY:
                return context.getResources().getDrawable(R.drawable.abbey_following);
            case CHURCH:
                return context.getResources().getDrawable(R.drawable.church_following);
            case CROSS:
                return context.getResources().getDrawable(R.drawable.cross_following);
            case GRAVEYARD:
                return context.getResources().getDrawable(R.drawable.graveyard_following);
            case JEWISH_GRAVEYARD:
                return context.getResources().getDrawable(R.drawable.jewish_graveyard_following);
            case MONUMENT:
                return context.getResources().getDrawable(R.drawable.monument_following);
            case STATUE:
                return context.getResources().getDrawable(R.drawable.statue_following);
            default:
                return context.getResources().getDrawable(R.drawable.star_following);
        }
    }
}
