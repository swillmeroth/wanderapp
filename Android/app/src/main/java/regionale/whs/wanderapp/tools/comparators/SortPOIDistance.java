package regionale.whs.wanderapp.tools.comparators;

import java.util.Comparator;

import regionale.whs.wanderapp.objects.POI;

/**
 * sorts pois by distance
 *
 * Created by Stephan on 31.10.2014.
 */
public class SortPOIDistance implements Comparator<POI>
{
    @Override
    public int compare(POI poi1, POI poi2)
    {
        return poi1.getDistance() - poi2.getDistance();
    }
}