package regionale.whs.wanderapp.tools.comparators;

import java.util.Comparator;

import regionale.whs.wanderapp.objects.Route;

/**
 * sorts route by length
 *
 * Created by Stephan on 31.10.2014.
 */
public class SortRouteLength implements Comparator<Route>
{
    public int compare(Route route1, Route route2)
    {
        return route1.getLength() - route2.getLength();
    }
}
