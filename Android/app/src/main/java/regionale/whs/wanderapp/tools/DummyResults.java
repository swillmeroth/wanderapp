package regionale.whs.wanderapp.tools;

import android.content.Context;

import java.util.ArrayList;

import regionale.whs.wanderapp.R;
import regionale.whs.wanderapp.objects.POI;
import regionale.whs.wanderapp.objects.Route;
import regionale.whs.wanderapp.objects.enums.POIType;
import regionale.whs.wanderapp.objects.enums.RouteType;

/**
 * Method that gets dummy results for testing purposes.
 *
 * Created by Stephan on 04.02.2015.
 */
public class DummyResults
{
    public static ArrayList<POI> getDummyPOIs()
    {
        POI poi;
        ArrayList<POI> poiResults = new ArrayList<>();

        // Erlöserkirche
        poi = new POI();
        poi.setName("Erlöserkirche");
        poi.setDescription("Die Grundsteinlegung dieser neugotischen mit Schiefer gedeckten evangelischen Kirche erfolgte 1911. Die Farbfenster datieren aus dem Jahr 1912. Die im Turm angebrachte Uhr aus dem frühen 20. Jahrhundert ist noch voll funktionsfähig und kann besichtigt werden. Im Inneren befindet sich ein Jugendstilsaal mit an die Seite gerücktem Turm und einhüftiger Emporenanlage.");
        poi.setType(POIType.CHURCH);
        poi.setAltitude(43);
        poi.setCity("Haltern");
        poi.setPostalCode("45721");
        poi.setStreet("Hennewiger Weg 2");
        poi.setSize(30);
        poi.setLatitude(51.74601111);
        poi.setLongitude(7.179872222);
        poi.setDrawableId(R.drawable.kirche);
        poi.setRating(4.01f);
        poi.setCommentCount(3);

        poiResults.add(poi);

        // jüdischer Friedhof Haltern
        poi = new POI();
        poi.setName("Jüdischer Friedhof Haltern");
        poi.setDescription("Dieser Friedhof befindet sich seit dem Jahre 1767 an dieser Stelle unmittelbar außerhalb des alten Stadtgrabens. Am 26. Januar 1997 wurde für die während der NS-Gewaltherrschaft verfolgten jüdischen Mitbürger ein Gedenkstein enthüllt, der in der Form einer alttestamentarischen Gesetzestafel von einer Halterner Steinbildhauerei gestaltet wurde. Auf dem Grabstein sind neben einem hebräischen Segensspruch die Namen der von 1933 bis 1945 verfolgten Mitbürger und ein Klagelied zu lesen.");
        poi.setType(POIType.JEWISH_GRAVEYARD);
        poi.setAltitude(43);
        poi.setCity("Haltern");
        poi.setPostalCode("45721");
        poi.setStreet("Richthof");
        poi.setSize(15);
        poi.setLatitude(51.74188056);
        poi.setLongitude(7.189077778);

        poiResults.add(poi);

        // Kardinal-von-Galen-Denkmal
        poi = new POI();
        poi.setName("Kardinal-von-Galen-Denkmal");
        poi.setDescription("Der Kardinal-von-Galen-Park ist die grüne Oase am Rande der Altstadt. Seinen Namen hat der Park vom Münsteraner Kardinal Clemens August Graf von Galen (siehe Informationstafel vor dem Denkmal), dem hier für sein Eintreten für Verfolgte während der nationalsozialistischen Gewaltherrschaft ein Denkmal errichtet wurde. Das Monument wurde von einem in Haltern vertretenen Unternehmen im Jubiläumsjahr 1989 gestiftet. Im gleichen Jahr wurden im Rahmen eines Symposiums die Kunstwerke im Park an Ort und Stelle geschaffen.");
        poi.setType(POIType.MONUMENT);
        poi.setAltitude(43);
        poi.setCity("Haltern");
        poi.setPostalCode("45721");
        poi.setStreet("Kardinal-von-Galen-Park");
        poi.setSize(12);
        poi.setLatitude(51.74196944);
        poi.setLongitude(7.18365);

        poiResults.add(poi);

        // St. Anna-Kapelle
        poi = new POI();
        poi.setName("St. Anna-Kapelle");
        poi.setDescription("Erstmals wurde eine Kapelle auf dem Annaberg im Jahre 1378 urkundlich erwähnt. Ab 1556 fanden die ersten Wallfahrten statt. Die heutige alte Kapelle datiert aus dem Jahre 1967. In der Nähe befindet sich eine Quelle, der zu der damaligen Zeit eine heilkräftige Wirkung nachgesagt wurde. Einige weitere kleine Bauwerke, Gedenkstätten, Kreuzwegstationen und eine Steele können auf dem weitläufigen Areal des Annabergs besucht werden. Viele Vertriebene aus Schlesien haben ihre traditionelle Annaberg-Wallfahrt hierher übertragen.");
        poi.setType(POIType.MONUMENT);
        poi.setAltitude(60);
        poi.setCity("Haltern");
        poi.setPostalCode("45721");
        poi.setStreet("Annaberg");
        poi.setSize(15);
        poi.setLatitude(51.725875);
        poi.setLongitude(7.155316667);

        poiResults.add(poi);

        // stubs without all information set
        poiResults.add(new POI("Stephansdom", POIType.CHURCH, 216, R.drawable.kirche));
        poiResults.add(new POI("Kapelle 1", POIType.CHAPEL, 513, R.drawable.kirche));
        poiResults.add(new POI("Kloster 1", POIType.ABBEY, 16, R.drawable.kirche));
        poiResults.add(new POI("Kloster 2", POIType.ABBEY, 5345, R.drawable.kirche));
        poiResults.add(new POI("Friedhof", POIType.JEWISH_GRAVEYARD, 345, R.drawable.kirche));

        return poiResults;
    }

    public static ArrayList<Route> getDummyRoutes(Context context)
    {
        ArrayList<Route> routeResults = new ArrayList<>();

        // Route from dummy xml-file
        Route route;
        route = RouteXMLReader.getRouteFromAssets(context, "testRoute.xml");
        route.setName("Jakobsweg");
        route.setType(RouteType.loop);
        route.setRating(4.01f);
        route.setCommentCount(3);
        route.setDescription("Der uralte Pilgerweg durch malerische Landschaften.");
        route.setAccessibilityInfo("Jakob war Rollstuhlfahrer, deshalb ist der Weg barrierefrei!");
        route.setTransportationInfo("Fuß, Rad, Pferd");
        route.setEquipmentInfo("Nach Regentagen ist es manchmal nass. Generell sollte festes Schuhwerk getragen werden.");

        ArrayList<POI> pois = getDummyPOIs();

        for (POI poi : pois)
        {
            if (poi.getName() == "Erlöserkirche" || poi.getName() == "Kardinal-von-Galen-Denkmal")
                route.addPOI(poi);
        }

        routeResults.add(route);

        routeResults.add(new Route("Route 1", RouteType.path, 30, 60000, 5345, R.drawable.kirche, null, null, null));
        routeResults.add(new Route("Route 2", RouteType.loop, 60, 45056, 216, R.drawable.kirche, null, null, null));
        routeResults.add(new Route("Route 3", RouteType.path, 45, 3445, 16, R.drawable.kirche, null, null, null));

        return routeResults;
    }
}
