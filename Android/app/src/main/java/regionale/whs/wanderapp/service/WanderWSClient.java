package regionale.whs.wanderapp.service;

/**
 * Client for server client communication. Consists of two layers:
 * Layer 1: named method called from app
 * Layer 2: post/get/delete Methods calling the asynchronous methods
 *
 * Created by Stephan on 07.04.2015.
 */


import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.CookieStore;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.mapsforge.core.model.BoundingBox;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import regionale.whs.wanderapp.tools.AppSettings;
import regionale.whs.wanderapp.tools.converter.StreamStringConverter;
import regionale.whs.wanderapp.wspojos.Credentials;
import regionale.whs.wanderapp.wspojos.Kartenausschnitt;
import regionale.whs.wanderapp.wspojos.PasswordUpdate;
import regionale.whs.wanderapp.wspojos.RegistrationData;
import regionale.whs.wanderapp.wspojos.RouteRequest;
import regionale.whs.wanderapp.wspojos.WanderDescription;
import regionale.whs.wanderapp.wspojos.WanderKommentar;
import regionale.whs.wanderapp.wspojos.WanderMedia;
import regionale.whs.wanderapp.wspojos.WanderPOI;
import regionale.whs.wanderapp.wspojos.WanderRoute;
import regionale.whs.wanderapp.wspojos.WanderUser;

/**
 * A class representing an implementation for a RESTful WanderApp client.
 * @author MartinM
 */
public class WanderWSClient {

    public boolean mProbableNetworkError = false;
    private boolean hasOfflineData = false;
    private boolean isOffline = false;
    private static WanderWSClient mInstance = null;
    private DefaultHttpClient mHttpClient = null;
    private HttpContext mHttpContext = null;
    private ArrayList<Header> mHeaders = null;
    private WanderUser mUser = null;

    private Credentials authCredentials;

    public boolean isProbableNetworkError() {
        return mProbableNetworkError;
    }

    public boolean hasOfflineData() {
        return hasOfflineData;
    }

    public void setHasOfflineData(boolean hasOfflineData) {
        this.hasOfflineData = hasOfflineData;
    }

    /**
     * Creates a new WanderWSClient using BASE_URI as the address of the web
     * service.
     */



    private void getClient()
    {
        HttpParams httpParameters = new BasicHttpParams();
        // Set the timeout in milliseconds until a connection is established.
        // The default value is zero, that means the timeout is not used.
        int timeoutConnection = 2000;
        HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
        // Set the default socket timeout (SO_TIMEOUT)
        // in milliseconds which is the timeout for waiting for data.
        int timeoutSocket = 60000;
        HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
        mHttpClient = new DefaultHttpClient(httpParameters);
        // Create a local instance of cookie store
        CookieStore cookieStore = new BasicCookieStore();
        // Create local HTTP context
        mHttpContext = new BasicHttpContext();
        // Bind custom cookie store to the local context
        mHttpContext.setAttribute(ClientContext.COOKIE_STORE, cookieStore);
    }

    private WanderWSClient() {
        resetAuthenticationData();
        getClient();
    }

    public static WanderWSClient getInstance()
    {
        if (mInstance == null)
        {
            mInstance = new WanderWSClient();
        }
        return mInstance;
    }

    private <T> T getWanderObjektFromResponse(HttpResponse response, Class<T> tClass)
    {
        if( response == null || response.getEntity() == null)
            return null;

        String jsonString = null;
        try {
            jsonString = StreamStringConverter.convertStreamToString(response.getEntity().getContent());
            response.getEntity().consumeContent();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.excludeFieldsWithoutExposeAnnotation().create();

        return gson.fromJson(jsonString, tClass);
    }

    /**
     * Creates a new WanderWSClient using a custom address for the web service.
     * @param customWSURI URI pointing to the RESTful WanderApp web service.
     */
    /*public WanderWSClient(String customWSURI) {
        useURI = customWSURI;
        client = new WebServiceClient();
        resetAuthenticationData();
    }*/

    /**
     * Purges any authentication data stored within this WanderWSClient object.
     * This disables it from performing successful requests to resources which
     * require authorization.
     */
    public final void resetAuthenticationData() {

        mHeaders = new ArrayList<>();

        Header header = new BasicHeader("x-wapp-email", "");
        mHeaders.add(header);

        header = new BasicHeader("x-wapp-password", "");
        mHeaders.add(header);

        authCredentials = null;
        mUser = null;
    }

    /**
     * Sends a request to the server which urges it to create a user account
     * using the provided credentials and displayname. After a successful
     * call to this function, a user account with the given email and
     * displayname will exist. If it already existed prior to calling this
     * function, it is by no means possible to tell whether that was the case.
     * @param email E-Mail address to use
     * @param password Password to use
     * @param displayname Unique screen name to use
     */
    public boolean register(String email, String password, String displayname, Context context, boolean showDialog) {
        HttpResponse response = post(new RegistrationData(email, password, displayname), "users", context, showDialog, false);
        return isAccepted(response);
    }

    /**
     * Sets the authentication data that this client sends along with its
     * requests. This should us the same information as was given to the
     * register()-method sometime before.
     * @param email E-Mail address
     * @param password Matching password
     */
    public void setAuthenticationData(String email, String password) {
        authCredentials = new Credentials();
        authCredentials.setEmail(email);
        authCredentials.setPassword(password);

        mHeaders = new ArrayList<>();

        Header header = new BasicHeader("x-wapp-email", email);
        mHeaders.add(header);

        header = new BasicHeader("x-wapp-password", password);
        mHeaders.add(header);
    }

    /**
     * Requests that the server returns a WanderUser object matching the
     * credentials supplied via setAuthenticationData. Using either a non-
     * existent email or an invalid password results in failure.
     * @return WanderUser object that matches the provided credentials
     * or no account with the given email address exists.
     * null, zero in length or 255+ characters long.
     */
    public WanderUser testAuthentication(Context context, boolean showDialog) {
        HttpResponse response = post(authCredentials, "users/authentication", context, showDialog, false);
        mUser = getWanderObjektFromResponse(response, WanderUser.class);

        return mUser;
    }

    public boolean changePassword(String newpassword, Context context, boolean showDialog) {
        PasswordUpdate update = new PasswordUpdate();
        update.setNewPassword(newpassword);

        HttpResponse response = post(update, "users/password", context, showDialog, false);

        return isAccepted(response);
    }

    /**
     * Returns the WanderPOI matching the given id.
     * @param poiId An id greater than 0.
     * @return WanderPOI corresponding to the given id.
     */
    public WanderPOI getPOI(int poiId, Context context, boolean showDialog) {
        HttpResponse response = get("places/" + poiId, context, showDialog, false);
        return getWanderObjektFromResponse(response, WanderPOI.class);
    }

    public List<WanderPOI> searchPOIsByName(String text, Context context, boolean showDialog) {
        HttpResponse response = get("places/containing-" + text, context, showDialog, false);

        if( response == null || response.getEntity() == null)
            return null;

        String jsonString = null;
        try {
            jsonString = StreamStringConverter.convertStreamToString(response.getEntity().getContent());
            response.getEntity().consumeContent();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.excludeFieldsWithoutExposeAnnotation().create();
        return gson.fromJson(jsonString, new TypeToken<List<WanderPOI>>() {
        }.getType());
    }

    // TODO: convert typestrings, convert Routes to WanderRoutes and backwards
    public List<WanderRoute> searchRoutesByType(ArrayList<String> types, Context context, boolean showDialog)
    {
        HttpResponse response = get("places/containing-" + types, context, showDialog, false);

        if( response == null || response.getEntity() == null)
            return null;

        String jsonString = null;
        try {
            jsonString = StreamStringConverter.convertStreamToString(response.getEntity().getContent());
            response.getEntity().consumeContent();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.excludeFieldsWithoutExposeAnnotation().create();
        return gson.fromJson(jsonString, new TypeToken<List<WanderPOI>>(){}.getType());
    }

    public List<WanderPOI> searchPOIsByType(List<String> types, Context context, boolean showDialog)
    {
        HttpResponse response = post(types, "places/types", context, showDialog, false);

        if( response == null || response.getEntity() == null)
            return null;

        String jsonString = null;
        try {
            jsonString = StreamStringConverter.convertStreamToString(response.getEntity().getContent());
            response.getEntity().consumeContent();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.excludeFieldsWithoutExposeAnnotation().create();
        return gson.fromJson(jsonString, new TypeToken<List<WanderPOI>>() {
        }.getType());
    }

    public List<WanderPOI> getPOIsForRoute(WanderRoute route, Context context, boolean showDialog)
    {
        HttpResponse response = get("routes/" + route.getId() + "/places", context, showDialog, false);

        if( response == null || response.getEntity() == null)
            return null;

        String jsonString = null;
        try {
            jsonString = StreamStringConverter.convertStreamToString(response.getEntity().getContent());
            response.getEntity().consumeContent();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.excludeFieldsWithoutExposeAnnotation().create();
        return gson.fromJson(jsonString, new TypeToken<List<WanderPOI>>() {
        }.getType());
    }

    public List<WanderPOI> getPOIsForDynamicRoute(WanderRoute route, Context context, boolean showDialog)
    {
        HttpResponse response = post(route.getRoutePlacesIds(), "places/bylist", context, showDialog, false);

        if( response == null || response.getEntity() == null)
            return null;

        String jsonString = null;
        try {
            jsonString = StreamStringConverter.convertStreamToString(response.getEntity().getContent());
            response.getEntity().consumeContent();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.excludeFieldsWithoutExposeAnnotation().create();
        return gson.fromJson(jsonString, new TypeToken<List<WanderPOI>>() {
        }.getType());
    }

    /**
     * Adds or updates the given WanderPOI, which is identified by its id.
     * @param poi WanderPOI object to add or update.
     * @return The Id of the added or updated object.
     */
    public int setPOI(WanderPOI poi, Context context, boolean showDialog) {
        HttpResponse response;

        if (poi.isNewObject())
            response = post(poi, "places", context, showDialog, false);
        else
            response= post(poi, "places/" + poi.getId(), context, showDialog, false);

        Integer integer = getWanderObjektFromResponse(response, Integer.class);
        return (integer == null) ? 0 : integer;
    }

    /**
     * Deletes the POI with the given id.
     * @param id Id of the POI to delete.
     */
    public void deletePOI(int id, Context context, boolean showDialog) {
        delete("places/" + id, context, showDialog, false);
    }

    public WanderUser getUser(int userId, Context context, boolean showDialog) {
        HttpResponse response = get("users/" + userId, context, showDialog, false);
        return getWanderObjektFromResponse(response, WanderUser.class);
    }

    public boolean setUser(WanderUser user, Context context, boolean showDialog) {
        HttpResponse response = post(user, "users/" + user.getId(), context, showDialog, false);
        return isAccepted(response);
    }

    public void deleteUser(int userId, Context context, boolean showDialog) {
        delete("users/" + userId, context, showDialog, false);
    }

    public WanderRoute getRoute(int routeId, Context context, boolean showDialog) {
        HttpResponse response = get("routes/" + routeId, context, showDialog, false);
        return getWanderObjektFromResponse(response, WanderRoute.class);
    }

    public List<WanderRoute> searchRoutesByName(String text, Context context, boolean showDialog) {
        HttpResponse response = get("routes/containing-" + text, context, showDialog, false);

        if( response == null || response.getEntity() == null)
            return null;


        String jsonString = null;
        try {
            jsonString = StreamStringConverter.convertStreamToString(response.getEntity().getContent());
            response.getEntity().consumeContent();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.excludeFieldsWithoutExposeAnnotation().create();
        return gson.fromJson(jsonString, new TypeToken<List<WanderPOI>>() {
        }.getType());
    }

    public int setRoute(WanderRoute route, Context context, boolean showDialog) {
        HttpResponse response = post(route, "routes", context, showDialog, false);

        Integer integer = getWanderObjektFromResponse(response, Integer.class);
        return (integer == null) ? 0 : integer;
    }

    public void deleteRoute(int routeId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public WanderRoute getDynamicRoute(RouteRequest routeRequest, Context context, boolean showDialog) {
        HttpResponse response = post(routeRequest, "map/routegenerator", context, showDialog, true);
        return getWanderObjektFromResponse(response, WanderRoute.class);
    }

    public int setDescription(WanderDescription description, Context context, boolean showDialog) {
        if (!description.isNewObject())
            if (description.hasValidId()) {
                HttpResponse response = post(description, "descriptions/" + description.getId(), context, showDialog, false);
                Integer integer = getWanderObjektFromResponse(response, Integer.class);
                return (integer == null) ? 0 : integer;
            }
            else throw new RuntimeException("Called setDescription() to update a WanderDescription object but id was not valid.");
        else if (description.getIdPlace()> 0) {
            HttpResponse response = post(description, "places/" + description.getIdPlace() + "/descriptions", context, showDialog, false);
            Integer integer = getWanderObjektFromResponse(response, Integer.class);
            return (integer == null) ? 0 : integer;
        } else if (description.getIdRoute()> 0) {
            HttpResponse response = post(description, "routes/" + description.getIdRoute() + "/descriptions", context, showDialog, false);
            Integer integer = getWanderObjektFromResponse(response, Integer.class);
            return (integer == null) ? 0 : integer;
        }
        throw new RuntimeException("Called setDescription() with new WanderDescription object but neither valid place id nor valid route id.");
    }

    public Kartenausschnitt getKartenausschnitt( BoundingBox box, Context context, boolean showDialog) {
        HttpResponse response = get("map/latitude-" + box.minLatitude
                                        + "-to-" + box.maxLatitude
                                        + "-and-longitude-" + box.minLongitude
                                        + "-to-" + box.maxLongitude, context, showDialog, false);

        return getWanderObjektFromResponse(response, Kartenausschnitt.class);
    }

    public WanderDescription getDescription(int descriptionId, Context context, boolean showDialog) {
        HttpResponse response = get("descriptions/" + descriptionId, context, showDialog, false);
        return getWanderObjektFromResponse(response, WanderDescription.class);
    }

    public WanderDescription getPOIDescription(int poiId, String forInterest, String inLanguage, Context context, boolean showDialog) {
        HttpResponse response = get("places/" + poiId + "/descriptions/" + forInterest + "-interest-in-" + inLanguage + "-language", context, showDialog, false);
        return getWanderObjektFromResponse(response, WanderDescription.class);
    }

    public WanderDescription getRouteDescription(int routeId, String forInterest, String inLanguage, Context context, boolean showDialog) {
        HttpResponse response = get("routes/" + routeId + "/descriptions/" + forInterest + "-interest-in-" + inLanguage + "-language", context, showDialog, false);
        return getWanderObjektFromResponse(response, WanderDescription.class);
    }

    public void deleteDescription(int descriptionId, Context context, boolean showDialog) {
        delete("descriptions/" + descriptionId, context, showDialog, false);
    }

    public WanderKommentar getKommentar(int commentId, Context context, boolean showDialog) {
        HttpResponse response = get("comments/" + commentId, context, showDialog, false);
        return getWanderObjektFromResponse(response, WanderKommentar.class);
    }

    public WanderKommentar getPOIKommentar(int poiId, int kommentarNummer, Context context, boolean showDialog) {
        HttpResponse response = get("places/" + poiId + "/comments/" + kommentarNummer, context, showDialog, false);
        return getWanderObjektFromResponse(response, WanderKommentar.class);
    }

    public List<WanderKommentar> getPOIKommentare(int poiId, int vonNummer, int bisNummer, Context context, boolean showDialog) {
        String request = "places/" + poiId + "/comments/" + vonNummer + "-" + bisNummer;
        HttpResponse response = get(request, context, showDialog, false);

        if( response == null || response.getEntity() == null)
            return null;


        String jsonString = null;
        try {
            jsonString = StreamStringConverter.convertStreamToString(response.getEntity().getContent());
            response.getEntity().consumeContent();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.excludeFieldsWithoutExposeAnnotation().create();
        return gson.fromJson(jsonString, new TypeToken<List<WanderKommentar>>(){}.getType());
    }

    public WanderKommentar getRouteKommentar(int routeId, int kommentarNummer, Context context, boolean showDialog) {
        HttpResponse response = get("routes/" + routeId + "/comments/" + kommentarNummer, context, showDialog, false);
        return getWanderObjektFromResponse(response, WanderKommentar.class);
    }

    public List<WanderKommentar> getRouteKommentare(int routeId, int vonNummer, int bisNummer, Context context, boolean showDialog) {
        HttpResponse response = get("routes/" + routeId + "/comments/" + vonNummer + "-" + bisNummer, context, showDialog, false);

        if( response == null || response.getEntity() == null)
            return null;


        String jsonString = null;
        try {
            jsonString = StreamStringConverter.convertStreamToString(response.getEntity().getContent());
            response.getEntity().consumeContent();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.excludeFieldsWithoutExposeAnnotation().create();
        return gson.fromJson(jsonString, new TypeToken<List<WanderKommentar>>(){}.getType());
    }

    public int setKommentar(WanderKommentar comment, Context context, boolean showDialog) {
        if (!comment.isNewObject())
            if (comment.hasValidId()) {
                HttpResponse response = post(comment, "comments/" + comment.getId(), context, showDialog, false);
                Integer integer = getWanderObjektFromResponse(response, Integer.class);
                return (integer == null) ? 0 : integer;
            } else
                throw new RuntimeException("Called setKommentar() to update a WanderKommentar object but id was not valid.");
        else if (comment.getPlaceId() > 0) {
            HttpResponse response = post(comment, "places/" + comment.getPlaceId() + "/comments", context, showDialog, false);
            Integer integer = getWanderObjektFromResponse(response, Integer.class);
            return (integer == null) ? 0 : integer;
        } else if (comment.getRouteId() > 0) {
            HttpResponse response = post(comment, "routes/" + comment.getRouteId() + "/comments", context, showDialog, false);
            Integer integer = getWanderObjektFromResponse(response, Integer.class);
            return (integer == null) ? 0 : integer;
        } throw new RuntimeException("Called setKommentar() with new WanderKommentar object but neither valid place id nor valid route id.");
    }

    public void deleteKommentar(int commentId, Context context, boolean showDialog) {
        delete("comments/" + commentId, context, showDialog, false);
    }

    public WanderMedia getPOIMedia(int poiId, int medienNummer) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public WanderMedia[] getPOIMediaThumbs(int poiId, int vonNummer, int bisNummer) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public WanderMedia getPOIMediaThumb(int poiId, int thumbNummer) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public WanderMedia getRouteMedia(int poiId, int medienNummer) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public WanderMedia[] getRouteMediaThumbs(int poiId, int vonNummer, int bisNummer) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public WanderMedia getRouteMediaThumb(int poiId, int thumbNummer) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public WanderUser getCurrentUser() {
        return mUser;
    }

    // ------------------------------------------------------------------------
    //
    // HELPER FUNCTIONS
    //

    public HttpResponse get(String atPath, Context context, boolean showDialog, boolean showProgressBar) {

        if(AppSettings.isNetworkAvailable(context, (showDialog && !isOffline)))
        {
            HttpResponse httpResponse = null;
            AsyncHttpGet asyncHttpGet = new AsyncHttpGet(mHttpClient, mHttpContext, showProgressBar, context);

            String url = AppSettings.WANDER_BASE_URI + atPath;

            asyncHttpGet.execute(url);

            try {
                httpResponse = asyncHttpGet.get();

                System.out.println("Response: " + httpResponse);
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }

            if (isAccepted(httpResponse)) {
                return httpResponse;
            } else {
                return null;
            }
        }
        else
            return null;
    }

    public HttpResponse post(Object jsonObject, String atPath, Context context, boolean showDialog, boolean showProgressBar) {

        if(AppSettings.isNetworkAvailable(context, (showDialog && !isOffline))) {
            HttpResponse httpResponse = null;

            GsonBuilder gsonBuilder = new GsonBuilder();
            Gson gson = gsonBuilder.excludeFieldsWithoutExposeAnnotation().create();
            String jsonString = gson.toJson(jsonObject);

            AsyncHttpPost asyncHttpPost;

            asyncHttpPost = new AsyncHttpPost(mHttpClient, mHttpContext, jsonString, mHeaders, showProgressBar, context);

            String url = AppSettings.WANDER_BASE_URI + atPath;

            asyncHttpPost.execute(url);

            try {
                httpResponse = asyncHttpPost.get();

                System.out.println("Response: " + httpResponse);
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }

            return isAccepted(httpResponse) ? httpResponse : null;
        }
        else
            return null;
    }

    public HttpResponse delete(String atPath, Context context, boolean showDialog, boolean showProgressBar) {

        if(AppSettings.isNetworkAvailable(context, (showDialog && !isOffline))) {
            HttpResponse httpResponse = null;
            AsyncHttpDelete asyncHttpDelete = new AsyncHttpDelete(mHttpClient, mHttpContext, showProgressBar, context);

            String url = AppSettings.WANDER_BASE_URI + atPath;

            asyncHttpDelete.execute(url);

            try {
                httpResponse = asyncHttpDelete.get();

                System.out.println("Response: " + httpResponse);
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }

            return isAccepted(httpResponse) ? httpResponse : null;
        }
        else
            return null;
    }

    private boolean isAccepted(HttpResponse r) {
        if(r == null) {
            mProbableNetworkError = true;
            return false;
        }

        int responseCode = r.getStatusLine().getStatusCode();
        return (responseCode >= 200) && (responseCode < 300);
    }

    public boolean isOffline() {
        return isOffline;
    }

    public void setIsOffline(boolean isOffline) {
        this.isOffline = isOffline;
    }
}
