package regionale.whs.wanderapp.tools;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Swipeable ViewPager
 *
 * Created by Stephan on 19.02.2015.
 */
public class SwipeToggleViewPager extends ViewPager
{
    private boolean swipeEnabled = false;

    public SwipeToggleViewPager(Context context)
    {
        super(context);
    }

    public SwipeToggleViewPager(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public void setSwipeEnabled(boolean swipeEnabled)
    {
        this.swipeEnabled = swipeEnabled;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev)
    {
        return swipeEnabled && super.onInterceptTouchEvent(ev);
    }
}
