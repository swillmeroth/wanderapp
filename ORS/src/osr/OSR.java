/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package osr;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.xml.sax.SAXException;
import osr.WayPoint.PointType;

/**
 *
 * @author Andre
 */
public class OSR {
    
    public static void main(String[] args) throws IOException, SAXException{
        
        List<GeoPoint> geoPoints = new ArrayList();
        List<WayPoint> punkte = new ArrayList();
        GeoPoint geo1 = new GeoPoint();
        geo1.setLongitude(7.187205556);
        geo1.setLatitude(51.74277778);
        WayPoint p1 = new WayPoint(1,geo1, PointType.START_POINT);
        punkte.add(p1);
        geoPoints.add(geo1);
        
        GeoPoint geo2 = new GeoPoint();
        geo2.setLongitude(7.190002778);
        geo2.setLatitude(51.74551389);
        WayPoint p2 = new WayPoint(2,geo2, PointType.WAY_POINT);
        punkte.add(p2);
        geoPoints.add(geo2);
        
        GeoPoint geo3 = new GeoPoint();
        geo3.setLongitude(7.185936111);
        geo3.setLatitude(51.74744167);
        WayPoint p3 = new WayPoint(3,geo3, PointType.END_POINT);
        punkte.add(p3);
        geoPoints.add(geo3);
        
        GeoPoint geo4 = new GeoPoint();
        geo4.setLongitude(7.187205556);
        geo4.setLatitude(51.74277778);
        WayPoint p4 = new WayPoint(4,geo4, PointType.START_POINT);
        punkte.add(p4);
        geoPoints.add(geo4);
        Geo g = new Geo();
        for(int i=0; i< geoPoints.size(); i++){
            System.out.println("Distanz in km: " + g.getDistance(geo1, geoPoints.get(i)));
        }
        
        OpenRouteService osr = new OpenRouteService();
        //value=1 : fastest
        //value=2 : pedestrian
        //value=3 : BicycleSafety
        //value=4 : Bicycle
        //value=5 : BicycleRacer
        //de:deutsch, it:italienisch
        osr.TestOSR(punkte,4,"de");
        
        System.out.println("\n\nAusgabe:");
        System.out.println("Distanz: " + osr.getTotalDistance());
        System.out.println("Time:" + osr.getTotalTime());
        
        List<WayPoint> list = osr.getWegPunkte();
        List<Instruction> inst = osr.getInstructions();
        
        System.out.println("Anzahl Instruktionen: " + inst.size());
        System.out.println("Anzahl Wegpunkte: " + list.size());
        
        for(int i=0;i<list.size();i++)
        {
            System.out.println("PointType: " + osr.getWegPunkte().get(i).getPointType());
            System.out.println("OrderID: " + osr.getWegPunkte().get(i).getOrderID());
            System.out.println("Latitude: " + osr.getWegPunkte().get(i).getGeoPoint().getLatitude());
            System.out.println("Longitude: " + osr.getWegPunkte().get(i).getGeoPoint().getLongitude());
            System.out.println("Altitude: " + osr.getWegPunkte().get(i).getGeoPoint().getAltitude());
            System.out.println("Range : " + osr.getWegPunkte().get(i).getRange());            
        }
        
        for(int i=0;i<inst.size();i++)
        {
            System.out.println("Beschreibung: " + inst.get(i).getDescription());
            System.out.println("Instruktion: " + inst.get(i).getInstruction());
        }
        
        System.out.println("DONE");
        //System.out.println("Time: " + hdl.getContentHandler().getTotalTime());
    }
}
