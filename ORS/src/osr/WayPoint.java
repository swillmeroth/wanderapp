package osr;

//import DTO.GeoPoint;

/**
 * Definiert einen Punkt der Tour.
 */
public class WayPoint implements Comparable<WayPoint> {

    /* ====================================================================== */
    /*                       Konstanten                                       */
    /* ====================================================================== */
    /** Standard-Reichweite */
    private static final int STANDARD_RANGE = 10;
    
    /* ====================================================================== */
    /*                       Instanzvariablen                                 */
    /* ====================================================================== */
    /** Rang in der Reihenfolge der Wegpunkte. */
    private int orderID;
    /** Reichweite um den Wegpunkt herum in Meter. (Standard sind 5 m) */
    private int range;
    /** Geografische Koordinate. */
    private GeoPoint geoPoint;
    /** Gibt den Typen des Wegpunktes an. */
    private PointType pointType;

    /* ====================================================================== */
    /*                       Konstruktoren                                    */
    /* ====================================================================== */
    /**
     * Erzeugt einen Weg-Punkt.
     */
    public WayPoint() {
    }

    /**
     * Erzeugt einen Weg-Punkt.
     * @param orderID Rang in der Reihenfolge der Wegpunkte
     * @param geoPoint geografische Koordinate
     * @param isTargetPoint handelt es sich um ein Zielpunkt
     */
    public WayPoint(int orderID, GeoPoint geoPoint, PointType pointType) {
        this.orderID = orderID;
        this.range = STANDARD_RANGE;
        this.geoPoint = geoPoint;
        this.pointType = pointType;
    }

    /* ====================================================================== */
    /*                       Getter & Setter                                  */
    /* ====================================================================== */
    /**
     * Gibt den Rang in der Reihenfolge der Wegpunkte.
     * @return Rang in der Reihenfolge
     */
    public int getOrderID() {
        return orderID;
    }

    /**
     * Setzt den Rang in der Reihenfolge der Wegpunkte.
     * @param orderID Rang in der Reihenfolge
     */
    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }
    
    /* ---------------------------------------------------------------------- */
    /**
     * Gibt die Reichweite um den Wegpunkt herum in Meter. (Standard sind 5 m)
     * @return Reichweite
     */
    public int getRange() {
        return range;
    }

    /**
     * Setzt die Reichweite um den Wegpunkt herum in Meter. (Standard sind 5 m)
     * @param range Reichweite
     */
    public void setRange(int range) {
        this.range = range;
    }
    
    /* ---------------------------------------------------------------------- */
    /**
     * Gibt die geografische Koordinate.
     * @return geografische Koordinate.
     */
    public GeoPoint getGeoPoint() {
        return geoPoint;
    }

    /**
     * Setzt die geografische Koordinate.
     * @param geoPoint geografische Koordinate.
     */
    public void setGeoPoint(GeoPoint geoPoint) {
        this.geoPoint = geoPoint;
    }
    
    /* ---------------------------------------------------------------------- */
    /**
     * Gibt den Typen des Punktes an.
     * @return Typen des Punktes
     */
    public PointType getPointType() {
        return pointType;
    }

    /**
     * Setzt den Typen des Punktes.
     * @param pointType Typen des Punktes
     */
    public void setPointType(PointType pointType) {
        this.pointType = pointType;
    }
    
    /* ====================================================================== */
    /*                       toString, equals, ...                            */
    /* ====================================================================== */
    /**
     * Gibt den Wegpunkt als String an.
     * @return Wegpunkt als String
     */
    @Override
    public String toString() {
        return "WayPoint (" + "orderID = " + orderID + ")";
    }
    
    /**
     * Gibt den kompletten Wegpunkt als String an.
     * @return Wegpunkt als String
     */
    public String toStringCompletely() {
        return "WayPoint{" + "orderID=" + orderID + ", range=" + range 
                + ", geoPoint=" + geoPoint + ", isTargetPoint=" 
                + pointType + '}';
    }

    /**
     * Vergleicht ein Wegpunkt-Objekt mit einem Objekt. Wenn die Koordinaten
     * gleich sind, sind die Objekte gleich.
     * @param obj Objekt
     * @return true, wenn gleich
     */
    @Override
    public boolean equals(Object obj) {
        return obj instanceof WayPoint 
                && this.geoPoint.equals(((WayPoint) obj).geoPoint);
    }

    /**
     * Gibt den HashCode.
     * @return hashCode
     */
    @Override
    public int hashCode() {
        return this.geoPoint.hashCode();
    }

    /**
     * Vergleicht den Rang der Reihenfolge des Wegpunktes.
     * @param wayPoint Wegpunkt mit dem verglichen werden soll
     * @return -1 von Rang vor übergebenen Namen liegt, 0 wenn gleich, 1 wenn 
     * übergebener Rang vor Rang liegt
     */
    @Override
    public int compareTo(WayPoint wayPoint) {
        return this.orderID < wayPoint.orderID 
                ? -1 
                : this.orderID < wayPoint.orderID 
                ? 0 
                : 1;
    }
    
    /* ====================================================================== */
    /*                       Hilfsklasse                                      */
    /* ====================================================================== */
    /**
     * Definiert den Typen des Punktes.
     */
    public enum PointType {
        /** Einfacher Wegpunkt der Tour. */
        WAY_POINT,
        /** Zielpunkt der der Tour. */
        TARGET_POINT,
        /** Startpunkt der der Tour. */
        START_POINT,
        /** Endpunkt der der Tour. */
        END_POINT;
    }
}
