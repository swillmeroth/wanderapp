package osr;

import org.simpleframework.xml.Element;


/**
 * Repräsentiert eine Geo-Koordinate.
 */
public class GeoPoint {
    /* ====================================================================== */
    /* Instanzvariablen */
    /* ====================================================================== */
    /** Breitengrad der Position. */
    @Element
    private double latitude;
    /** Längengrad der Position. */
    @Element
    private double longitude;
    /** Höhe der Position. */
    @Element
    private double altitude;

    /* ====================================================================== */
    /* Konstruktoren */
    /* ====================================================================== */
    /**
     * Erzeugt eine Geo-Position.
     * @param latitude Breitengrad der Position
     * @param longitude Längengrad der Position
     * @param altitude Höhe der Position
     */
    public GeoPoint(double latitude, double longitude, double altitude) {

        this.latitude = latitude;
        this.longitude = longitude;
        this.altitude = altitude;
    }

    /**
     * Erzeugt eine Geo-Position.
     * @param latitude Breitengrad der Position
     * @param longitude Längengrad der Position
     */
    public GeoPoint(double latitude, double longitude) {

        this.latitude = latitude;
        this.longitude = longitude;
        this.altitude = 0;
    }

    /**
     * Erzeugt eine Geo-Position.
     */
    public GeoPoint() {
    }

    /* ====================================================================== */
    /* Getter und Setter */
    /* ====================================================================== */
    /**
     * Gibt den Breitengrad der Position.
     * 
     * @return Breitengrad der Position
     */
    public double getLatitude() {
        return latitude;
    }

    /**
     * Setzt den Breitengrad der Position
     * 
     * @param latitude
     *            Breitengrad der Position
     */
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    /* ---------------------------------------------------------------------- */
    /**
     * Gibt den Längengrad der Position.
     * 
     * @return Längengrad der Position
     */
    public double getLongitude() {
        return longitude;
    }

    /**
     * Setzt den Längengrad der Position.
     * 
     * @param longitude
     */
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    /* ---------------------------------------------------------------------- */
    /**
     * Gibt die Höhe der Position.
     * 
     * @return Höhe der Position
     */
    public double getAltitude() {
        return altitude;
    }

    /**
     * Setzt die Höhe der Position.
     * 
     * @param altitude
     *            Höhe der Position
     */
    public void setAltitude(double altitude) {
        this.altitude = altitude;
    }

    /* ====================================================================== */
    /* toString */
    /* ====================================================================== */
    /**
     * Gibt die Position als String.
     * 
     * @return Position als String
     */
    @Override
    public String toString() {
        return "Breitengrad: " + latitude + ", Längengrad: " + longitude
                + ", Höhe: " + altitude;
    }
    
    /**
     * Vergleicht ein GeoPoint-Objekt mit einem Objekt. Wenn Längengrad und 
     * Breitengrad gleich sind, sind die Objekte gleich.
     * @param obj Objekt
     * @return true, wenn gleich
     */
    @Override
    public boolean equals(Object obj) {
        
        return obj instanceof GeoPoint 
                && this.latitude == ((GeoPoint) obj).latitude 
                && this.longitude == ((GeoPoint) obj).longitude;
    }

    /**
     * Gibt den HashCode.
     * @return hashCode
     */
    @Override
    public int hashCode() {
        return (int) (latitude + longitude);
    }
}
