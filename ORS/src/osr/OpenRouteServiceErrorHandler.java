
package osr;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/** 
 * Liefert eine Fehlermeldung, falls das XML-Dokument oder das XML-Schema
 * nicht korrekt ist.
 */
public class OpenRouteServiceErrorHandler implements ErrorHandler {

    /**
     * Empfangen einer Warnungs-Meldung. Das Parsen der Daten muss nicht 
     * abgebrochen werden.
     * @param exception Fehlermeldung
     * @throws SAXException SAX-Parser-Fehler
     */
    @Override
    public void warning(SAXParseException exception) throws SAXException {
       throw new SAXException("Warnung: " + exception.toString());
    }

    /**
     * Empfangen von Informationen über einen behebbaren Fehler. Tritt bei der 
     * Verletzung von Gültigkeit auf. Der Parsing-Vorgang muss nicht abgebrochen
     * werden.
     * @param exception Fehlermeldung
     * @throws SAXException SAX-Parser-Fehler
     */
    @Override
    public void error(SAXParseException exception) throws SAXException {
        throw new SAXException("Fehler: " + exception.toString());
    }

    /**
     * Empfangen von Informationen über einen nicht behebbaren Fehler. Der 
     * Parsing-Vorgang wird abgebrochen.
     * @param exception Fehlermeldung
     * @throws SAXException SAX-Parser-Fehler
     */
    @Override
    public void fatalError(SAXParseException exception) throws SAXException {
        throw new SAXException("fataler Fehler: " + exception.toString());
    }
}
