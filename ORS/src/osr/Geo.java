package osr;

/**
 * Klasse zur Berechnung von Abständen und Winkel zwischen Geo-Koordinaten.
 */
public class Geo {
    /** Radius der Erdkugel. */
    private static final float EARTH_RADIUS = 6378.137f;

    /**
     * Ermittelt den ungefähren Abstand zwischen zwei Positionen in km.
     * @param geoPoint1 Position 1
     * @param geoPoint2 Position 2
     * @return Distanz
     */
    public double getDistance(GeoPoint geoPoint1,
            GeoPoint geoPoint2) {

        /* 6370 entspricht dem Radius der Erdkugel. */
        return Math.acos(Math.sin(decimalAngle2RadAngle(geoPoint1.getLatitude()))
                * Math.sin(decimalAngle2RadAngle(geoPoint2.getLatitude()))
                + Math.cos(decimalAngle2RadAngle(geoPoint1.getLatitude()))
                * Math.cos(decimalAngle2RadAngle(geoPoint2.getLatitude()))
                * Math.cos(decimalAngle2RadAngle(geoPoint2.getLongitude()
                - geoPoint1.getLongitude())))
                * EARTH_RADIUS;
    }

    /**
     * Berechnet Dezimalgrad in Bogenmaß um.
     * @param DecimalAngle
     * @return Grad in Bogenmaß
     */
    public double decimalAngle2RadAngle(double decimalAngle) {
        return decimalAngle * Math.PI / 180;
    }

    /**
     * Rechnet Bogenmaß in Dezimalgrad um.
     * @param RadAngle
     * @return Grad in Dezimalgrad
     */
    public double radAngle2DecAngle(double radAngle) {
        return radAngle * 180 / Math.PI;
    }

    /**
     * Berechnet den Winkel zwischen 2 Geo-Koordinaten in Abhängigkeit zum 
     * Nordpol.
     * @param geoPoint1 Position 1
     * @param geoPoint2 Position 2
     * @return Winkel in Grad
     */
    public float getAngle(GeoPoint geoPoint1, GeoPoint geoPoint2) {

        double dLon = (geoPoint2.getLongitude() - geoPoint1.getLongitude());

        double y = Math.sin(dLon) * Math.cos(geoPoint2.getLatitude());
        double x = Math.cos(geoPoint1.getLatitude()) * Math.sin(geoPoint2.getLatitude())
                - Math.sin(geoPoint1.getLatitude()) * Math.cos(geoPoint2.getLatitude()) * Math.cos(dLon);

        /* Peilung bestimmen */
        return (float) radAngle2DecAngle(Math.atan2(y, x));
    }
}
