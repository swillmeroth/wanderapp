package osr;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

/**
 * Stellt Methoden zur Verfügung, um sich eine Route von OpenRouteService (ORS)
 * übergeben zu lassen.
 */
//public class OpenRouteService implements IRouteService {

public class OpenRouteService{
    public double totalDistance;
    public int totalTime;
    public List<WayPoint> WegPunkte;
    public List<Instruction> instructions;
    public String RoutePreferenz;
    public String Sprache;

    public String getSprache() {
        return Sprache;
    }

    public void setSprache(String Sprache) {
        this.Sprache = Sprache;
    }

    public String getRoutePreferenz() {
        return RoutePreferenz;
    }

    public void setRoutePreferenz(String RoutePreferenz) {
        this.RoutePreferenz = RoutePreferenz;
    }
    public List<Instruction> getInstructions() {
        return instructions;
    }

    public void setInstructions(List<Instruction> instructions) {
        this.instructions = instructions;
    }

    public List<WayPoint> getWegPunkte() {
        return WegPunkte;
    }

    public void setWegPunkte(List<WayPoint> WegPunkte) {
        this.WegPunkte = WegPunkte;
    }

    public int getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(int totalTime) {
        this.totalTime = totalTime;
    }
    
    public double getTotalDistance() {
        return totalDistance;
    }

    public void setTotalDistance(double totalDistance) {
        this.totalDistance = totalDistance;
    }
    
    
    

    /* ====================================================================== */
    /*                       Konstanten                                       */
    /* ====================================================================== */
    /** URL zu Open Route Service. */
    private static final String OPEN_ROUTE_SERVICE_URL =
            "http://openls.geog.uni-heidelberg.de/fh-gelsenkirchen/route";
    /** XML Reader. */
    private static final String XML_READER =
            "org.apache.xerces.parsers.SAXParser";
    /** XML Feature. */
    private static final String XML_FEATURE =
            "http://apache.org/xml/features/validation/schema";
    /* ---------------------------------------------------------------------- */
 

    public OpenRouteServiceContentHandler contentHandler;

    /** OpenRouteService-Parser. */
    private XMLReader parser;
    

    /* ====================================================================== */
    /*                       Konstruktor                                      */
    /* ====================================================================== */
    public OpenRouteService() throws SAXException {

        /* Parser initialisieren */
        this.parser = XMLReaderFactory.createXMLReader(XML_READER);
        /* Handler initialisieren */
        this.contentHandler = new OpenRouteServiceContentHandler();
        /* Feature setzen */
        this.parser.setFeature(XML_FEATURE, true);
        /* Content Handler setzen */
        this.parser.setContentHandler(contentHandler);
        /* Content Handler setzen */
        this.parser.setErrorHandler(new OpenRouteServiceErrorHandler());
    }
    
    //Setzen der Routen Preferenz
    public void TestOSR(List<WayPoint> targetPoints, int value, String lang) throws MalformedURLException, IOException, SAXException{
        this.setSprache(lang);
        if(value==1){
                this.setRoutePreferenz("Fastest");
                }else{
                    if(value==2){
                        this.setRoutePreferenz("Pedestrian"); 
                    }else{
                          if(value==3){
                            this.setRoutePreferenz("BicycleSafety");
                          }else{
                              if(value==4){
                                  this.setRoutePreferenz("Bicycle");
                              }else{
                                    if(value==5){
                                        this.setRoutePreferenz("BicycleRacer");
                                    }
                              }
                          }
                    }
            }
            //Aufruf der URL des OpenRouteService
            URL url = new URL(OPEN_ROUTE_SERVICE_URL);

            //Verbindung herstellen und Übermitllung der XML Daten
            URLConnection connection = url.openConnection();
            connection.setAllowUserInteraction(true);
            connection.setDoOutput(true);
            
            OutputStreamWriter writer =
                    new OutputStreamWriter(connection.getOutputStream());
            writer.write(generateRequest(targetPoints));
            writer.flush();
            System.out.println(connection.getInputStream());
            
            //Antwort des Services parsen und Informationen setzen
            //erfolgt über den ContentHandler
            parser.parse(new InputSource(connection.getInputStream()));
            setTotalDistance(this.contentHandler.getTotalDistance());
            setTotalTime(this.contentHandler.getTotalTime());
            setWegPunkte(this.contentHandler.getWayPoints());
            setInstructions(this.contentHandler.getInstructions());
            
            
//Umleitung in Datei
            
//            InputStream inputStream = null;
//	
//            OutputStream outputStream = null;
//                // read this file into InputStream
//		inputStream = connection.getInputStream();
// 
//		// write the inputStream to a FileOutputStream
//		outputStream = new FileOutputStream(new File("/Users/Andre/Desktop/response2.txt"));
// 
//		int read = 0;
//		byte[] bytes = new byte[1024];
// 
//		while ((read = inputStream.read(bytes)) != -1) {
//			outputStream.write(bytes, 0, read);
//		}
 
		System.out.println("Done!");
                writer.close();
    }
            
    //Generiert die xml Anfrage mit den gewünschten Inhalten an Geodaten
    private String generateRequest(List<WayPoint> targetPoints) 
    {
        String start="<gml:pos>"+targetPoints.get(0).getGeoPoint().getLongitude()+" "+targetPoints.get(0).getGeoPoint().getLatitude()+"</gml:pos>";
        System.out.println("Start: " + start);
        String end="<gml:pos>"+targetPoints.get(targetPoints.size()-1).getGeoPoint().getLongitude()+" "+targetPoints.get(targetPoints.size()-1).getGeoPoint().getLatitude()+"</gml:pos>";
        System.out.println("End: " + end);
        return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                + "<xls:XLS xmlns:xls=\"http://www.opengis.net/xls\" xmlns:sch=\"http://www.ascc.net/xml/schematron\" "
                + "xmlns:gml=\"http://www.opengis.net/gml\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" "
                + "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.opengis.net/xls "
                + "http://schemas.opengis.net/ols/1.1.0/RouteService.xsd\" version=\"1.1\" xls:lang=\"" + this.getSprache() + "\">"
                + "<xls:RequestHeader/>"
                + "<xls:Request methodName=\"RouteRequest\" requestID=\"123456789\" version=\"1.1\">"
                + "<xls:DetermineRouteRequest distanceUnit=\"KM\">"
                + "<xls:RoutePlan>"
                /* Route-Preferenzen */
                + "<xls:RoutePreference>" + this.getRoutePreferenz() + "</xls:RoutePreference>"
                /* Zielpunkte */
                /* Startposition */
                + "<xls:WayPointList>"
                    + "<xls:StartPoint>"
                        + "<xls:Position>"                
                            + "<gml:Point srsName=\"EPSG:4326\">"
                                + start
                            + "</gml:Point>"
                        + "</xls:Position>"
                    + "</xls:StartPoint>"
                /* Wegpunkte */
                    + getViaPoints(targetPoints, 1)
                /* Endposition */
                    + "<xls:EndPoint>"
                        + "<xls:Position>"                
                            + "<gml:Point srsName=\"EPSG:4326\">"
                               + end
                            + "</gml:Point>"
                        + "</xls:Position>"
                    + "</xls:EndPoint>"
                + "</xls:WayPointList>"
                + "</xls:RoutePlan>"
                + "<xls:RouteInstructionsRequest/>"
                + "<xls:RouteGeometryRequest/>"
                + "</xls:DetermineRouteRequest>"
                + "</xls:Request>"
                + "</xls:XLS>";
    }

    
    //Hinzufügen von Wegpunkten, wenn es sich nicht nur um Start und enpunkt handelt
    private String getViaPoints(List<WayPoint> targetPoints, int index) {
        return index >= targetPoints.size() - 1
                ? ""
                : "<xls:ViaPoint>"
                + "<xls:Position>"
                + "<gml:Point srsName=\"EPSG:4326\">"
                + "<gml:pos>"+ targetPoints.get(index).getGeoPoint().getLongitude()+ " "+ targetPoints.get(index).getGeoPoint().getLatitude()+ "</gml:pos>"
                + "</gml:Point>"
                + "</xls:Position>"
                + "</xls:ViaPoint>"
                + getViaPoints(targetPoints, index + 1);
    }
}
