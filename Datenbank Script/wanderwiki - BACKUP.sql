-- MySQL dump 10.13  Distrib 5.6.17, for Win64 (x86_64)
--
-- Host: localhost    Database: wanderwiki
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `idComments` int(11) NOT NULL,
  `idPlace` int(11) DEFAULT NULL,
  `idRoute` int(11) DEFAULT NULL,
  `idUser` int(11) DEFAULT NULL,
  `time` datetime DEFAULT NULL,
  `author` varchar(45) DEFAULT NULL,
  `text` text,
  PRIMARY KEY (`idComments`),
  KEY `place_idx` (`idPlace`),
  KEY `route_idx` (`idRoute`),
  KEY `user_idx` (`idUser`),
  CONSTRAINT `place` FOREIGN KEY (`idPlace`) REFERENCES `places` (`idPlaces`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `route` FOREIGN KEY (`idRoute`) REFERENCES `routes` (`idRoutes`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `user` FOREIGN KEY (`idUser`) REFERENCES `users` (`userid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `descriptions`
--

DROP TABLE IF EXISTS `descriptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `descriptions` (
  `idDescriptions` int(11) NOT NULL,
  `idRoute` int(11) DEFAULT NULL,
  `idPlace` int(11) DEFAULT NULL,
  `DE_standard` text,
  `DE_cultural` text,
  `DE_religious` text,
  `EN_standard` text,
  PRIMARY KEY (`idDescriptions`),
  KEY `route_idx` (`idRoute`),
  KEY `place_idx` (`idPlace`),
  CONSTRAINT `place1` FOREIGN KEY (`idPlace`) REFERENCES `places` (`idPlaces`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `route1` FOREIGN KEY (`idRoute`) REFERENCES `routes` (`idRoutes`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `descriptions`
--

LOCK TABLES `descriptions` WRITE;
/*!40000 ALTER TABLE `descriptions` DISABLE KEYS */;
INSERT INTO `descriptions` VALUES (1,NULL,1,'Die Grundsteinlegung dieser neugotischen mit Schiefer gedeckten evangelischen Kirche erfolgte 1911. Die Farbfenster datieren aus dem Jahr 1912. Die im Turm angebrachte Uhr aus dem frühen 20. Jahrhundert ist noch voll funktionsfähig und kann besichtigt werden. Im Inneren befindet sich ein Jugendstilsaal mit an die Seite gerücktem Turm und einhüftiger Emporenanlage.',NULL,NULL,NULL),(2,NULL,2,'Die erste Kirche in Hamm, Harne - das bedeutet \"Winkel am Fluss\" - dürfte eine Kapelle des adeligen Hauses Hame oder Hamma gewesen sein. Das Haus Hame entstand auf einem Reichshof (Königshof) Karls des Großen am Fuße des Hammer Berges. Über der Lippenniederung gelegen, ist die Kirche heute Mittelpunkt einer in diesem Bereich noch intakten Streusiedlung. Die Kirche wurde im 12. Jahrhundert im romanischen Stil errichtet. Dieses war zur Zeit der Kreuzüge, daher auch der Name Heilig Kreuz. Dieses Baudenkmal ist gut über den Kanaluferweg per Fahrrad oder zu Fuß zu erreichen.',NULL,NULL,NULL),(3,NULL,3,NULL,NULL,NULL,NULL),(4,NULL,4,NULL,NULL,NULL,NULL),(5,NULL,5,'Die Flaesheimer Pfarrkirche Sankt Maria Magdalena gehört zu den bemerknswertesten und ältesten Kirchenbauten im Halterner Raum. Der romanische Turm stammt mit Sicherheit noch vom Gründungsbau des 1166 von Graf Otto dem Ersten von Ravensberg gegründeten und dem Kloster Knechtsteden unterstellten Prämonstrantenser-Nonnenklosters. Das Kloster wirde 1550 freiweltliches Stift, die Stiftsgebäude sind 1790 abgebrannt. Trotz umfangreicher Literatur ist die Baugeschichte der heute einschiffigen Kirche noch nicht in allen Teilen eindeutig geklärt.',NULL,NULL,NULL),(6,NULL,6,'Im Jahr 1895 beauftragte der Kirchvorstand den Architekten Wilhelm Ricklake aus Münster mit der Planung dieser Kirche. Es entstand ein eindrucksvolles Bauwerk in einer neugotischen Backsteinarchitektur mit 2 Jochen, einem Querschiff und einem 65 m hohen Westturm. Am 21. September 1897 wurde die Kirche feierlich eingeweiht. Bei Restaurierungsarbeiten entdeckte die Restaurateurin an den spätgotischen Altarbildern (um 1500), dass sich unter der zweiten Farbschicht in der \"Geißelung\" ein zweites Gesicht befindet. ',NULL,NULL,NULL),(7,NULL,7,'Die Pfarrkirche St. Sixtus ist nach Osten ausgerichtet. Zum Inventar gehört das berühmte Gabelkruzifix aus Eichenholz, das um 1330/40 entstanden sein dürfte (Restauration 1961). Es wird alljährlich bei der Kreuzerhöhungsprozession durch die Straßen der Stadt getragen. Diese Tradition geht auf das Jahr 1736 zurück. Daneben ist das 1710 entstandene Epitaph eines der bedeutendstend Kunstschätze dieser Kirche.',NULL,NULL,NULL),(8,NULL,8,'Erstmals wurde eine Kapelle auf dem Annaberg im Jahre 1378 urkundlich erwähnt. Ab 1556 fanden die ersten Wallfahrten statt. Die heutige alte Kapelle datiert aus dem Jahre 1967. In der Nähe befindet sich eine Quelle, der zu der damaligen Zeit eine heilkräftige Wirkung nachgesagt wurde. Einige weitere kleine Bauwerke, Gedenkstätten, Kreuzwegstationen und eine Steele können auf dem weitläufigen Areal des Annabergs besucht werden. Viele Vertriebene aus Schlesien haben ihre traditionelle Annaberg-Wallfahrt hierher übertragen.',NULL,NULL,NULL),(9,NULL,9,'Die Pfarrkirche Sankt Antonius gibt sich in barocken Formen, entstand aber erst in den Jahren 1921 bis 1924 nach Plänen des münsterischen Dombaumeisters Sunder-Plaßmann.',NULL,NULL,NULL),(10,NULL,10,'Die Pfarrkirch Sankt Joseph feierte ihre Gründung im Jahr 1909. Wegen des rapiden Bevölkerungswachstums nach dem 2. Weltkrieg war die Kirche bald zu klein und erfuhr 1959 eine große Erweiterung.\n\n\n\n\n\n\n\n\n\n\n\n',NULL,NULL,NULL),(11,NULL,11,'Die Grundsteinlegung erfolgte am 17. Juli 1954. Nach alter Tradition weist die Fassade nach Westen, während der Altar nach Osten ausgerichtet ist. Die Kirche trägt den Namen des heiligen Laurentius, des Mannes, der als Diakon seinem väterlichen Freund, dem Papst Sixtus, treu ergeben war. \n\n',NULL,NULL,NULL),(12,NULL,12,'Die Kirche von Lippramsdorf mittem im Herz des Dorfes.\n\n\n\n\n\n\n\n\n\n\n\n',NULL,NULL,NULL),(13,NULL,13,'Der einschiffige Bau stammt ursprünglich aus dem Jahr 1467. Er wurde 1652 nach Westen erweitert. An der Nordseite schließt sich eine kleine Sakristei von 1805 an. Bemerkenswert sind die restaurierten Wandmalereien, welche lange Zeit durch Farbschichten überdeckt waren, die aus dem späten 15. Jahrhundert stammen. Die Bilder wurden seinerzeit auf den frischen Verputz (daher: Fresco) gemalt. Erst in den dreißiger Jahren des 20. Jahrhunderts wurden sie wiederentdeckt. Heute dient die Kapelle neben der ursprünglichen Bestimmung als Gotteshaus auch als Denkmal für die Gefallenen der letzten beiden Weltkriege. Im Innern der Kapelle sind Gedenktafeln mit den Namen aller in den beiden Kriegen gefallenen und vermissten Soldaten aus Lavesum angebracht.\n\n',NULL,NULL,NULL),(14,NULL,14,'Die kleine Bruchsteinkapelle auf dem Tannenberg bei der Bauernschaft gleichen Namens wurde die ihrer heutigen Form 1962 an St. Peter und Paul eingeweiht. Es gibt aber Belege dafür, dass hier schon vor Jahrhunderten eine Kapelle stand. So schrieb der Halterner Pfarrer Hermann Boeker im Jahre 1662, dass die ehedem bei dem Weseler Weg gestandene Kapelle bereits vor Jahrzenten zerstört worden und nur noch in Ruinen vorhanden sei. Der Aufforderungen des Bischofs Christian Bernhard von Galen aus dem Jahre 1669, die Kapelle wieder herzustellen, sollen die Bauern damals nicht nachgekommen sein.\n\n',NULL,NULL,NULL),(15,NULL,15,'Erstmals wurde eine Kapelle auf dem Annaberg im Jahre 1378 urkundlich erwähnt. Ab 1556 fanden die ersten Wallfahrten statt. Die heutige alte Kapelle datiert aus dem Jahre 1967. In der Nähe befindet sich eine Quelle, der zu der damaligen Zeit eine heilkräftige Wirkung nachgesagt wurde. Einige weitere kleine Bauwerke, Gedenkstätten, Kreuzwegstationen und eine Steele können auf dem weitläufigen Areal des Annabergs besucht werden. Viele Vertriebene aus Schlesien haben ihre traditionelle Annaberg-Wallfahrt hierher übertragen.\n\n',NULL,NULL,NULL),(16,NULL,16,'Diese Kapelle gilt als ältester Steinbau im Vest. Dem kleinen, flachgedeckten Saalbau mit gerade geschlossenem Chor - vermutlich aus dem 11. Jahrhundert - folgte als spätere Erweiterung das Kirchenschiff mit einem Barockaltar, der mit 1744 - 1746 bezeichnet ist.\n',NULL,NULL,NULL),(17,NULL,17,'Die kleine Kapelle auf dem Prozessionsweg.\n\n\n',NULL,NULL,NULL),(18,NULL,18,'Dieser Friedhof befindet sich seit dem Jahre 1767 an dieser Stelle unmittelbar außerhalb des alten Stadtgrabens. Am 26. Januar 1997 wurde für die während der NS-Gewaltherrschaft verfolgten jüdischen Mitbürger ein Gedenkstein enthüllt, der in der Form einer alttestamentarischen Gesetzestafel von einer Halterner Steinbildhauerei gestaltet wurde. Auf dem Grabstein sind neben einem hebräischen Segensspruch die Namen der von 1933 bis 1945 verfolgten Mitbürger und ein Klagelied zu lesen.\n\n',NULL,NULL,NULL),(19,NULL,19,'Der erste Spatenstich des an der Sundernstraße gelegenen Waldfriedhofes Sundern erfolgte am 19.01.1979. Die Planung beinhaltete für den 1. Abschnitt die Anlegung von 700 Doppelgräbern, 400 Einzelgräbern, 50 Kindergräbern, 50 Urnengräbern und den Bau einer Trauerhalle für ca. 150 Trauergäste. Die Trauerhalle, deren Baukosten ca. 2,4 Millionen Mark betrugen, wurde 1981 fertiggestellt. Die Gesamtfläche des am Waldrand gelegenen Friedhofes umfasst jetzt nach der Erweiterung im Jahre 1998 ca. 52.000 m². \n\n',NULL,NULL,NULL),(20,NULL,20,'Die Einsegnung des an der Antruper/Westruper Straße gelegenen Friedhofes Hullern erfolgte am 26.01.1979. Im Jahre 1986 begann die Planung einer Trauerhalle, welche im Jahre 1988 fertiggestellt wurde. Die Baukosten für dieses Objekt beliefen sich auf ca. 610.000 DM. Die Gesamtfläche des am Waldrand gelegenen Friedhofes beträgt ca. 10.000 m². \n\n\n',NULL,NULL,NULL),(21,NULL,21,'Die Einweihung des am Brinkweg gelegenen Friedhofes Sythen erfolgte am 28.06.1960. Im Jahre 1965 wurde dann die Trauerhalle, deren Baukosten sich auf ca. 138.000 DM beliefen, fertiggestellt. Die Gesamtfläche des Friedhofes umfasst ca. 29.000 m². \n\n',NULL,NULL,NULL),(22,NULL,22,'Die Einweihung des an der Otto-von-Ravensberg-Straße gelegenen Friedhofes Flaesheim erfolgte am 08.10.1967. Im Jahre 1971 wurde dann die Trauerhalle, deren Baukosten sich auf ca. 120.000 DM beliefen, fertiggestellt. Die Gesamtfläche des Friedhofes umfasst ca. 24.000 m².\n',NULL,NULL,NULL),(24,NULL,24,'Auf diesem Friedhof steht die sehr bekannte St. Katharinen-Kapelle.\n\n',NULL,NULL,NULL),(25,NULL,25,NULL,NULL,NULL,NULL),(26,NULL,26,'\nIn der Nähe dieses Wegekreuzes liegt die Marienkirche.\n\n\n',NULL,NULL,NULL),(27,NULL,27,'Am Prozessionsweg der Stadt Haltern am See sind mehrere Wegekreuze, Standbilder, etc. zu finden.\n\n',NULL,NULL,NULL),(28,NULL,28,'Das Mutter-Anna-Standbild wurde in einer Halterner Bildhauerwerkstatt hergestellt. Alljährlich im September, am traditionellen Fest der \"Kreuzerhöhung\", zieht hier auch die Halterner Kreuztracht mit dem fast 700 Jahre alten Gabelkreuz der Sixtuskirche vorbei. Im Gebäude \"Muttergottesstiege\" hinter der Statue befinden sich u.a. die Zweigstelle der Volkshochschule Dülmen/ Haltern am See/ Havixbeck sowie Büros der Stadtverwaltung Haltern am See.\n\n',NULL,NULL,NULL),(29,NULL,29,'Dieses ist eine seltene Darstellung in dieser Gegend. Das Standbild zeigt den heiligen Ludgerus im Bischofsformat und zu seinen Füßen eine Gans. Der Legende nach soll der Bischof die Bauern, als sie ihn um entsprechenden Rat baten, angewiesen haben, an dieser Stelle nach einem Brunnen zu graben. Sie befolgten seinen Rat, und angeblich ist dieser Brunnen noch niemals versiegt und hat auch in den trockensten Sommern stets Wasser gegeben.\n\n',NULL,NULL,NULL),(30,NULL,30,'In der Nähe liegt die St. Antonius Kirche von Lavesum.\n',NULL,NULL,NULL),(31,NULL,31,'In der Nähe liegt die St. Antonius Kirche von Lavesum.\n\n\n',NULL,NULL,NULL),(32,NULL,32,'In der Nähe liegt die St. Lambertus Kirche von Lippramsdorf.\n\n',NULL,NULL,NULL),(34,NULL,34,'Der Kardinal-von-Galen-Park ist die grüne Oase am Rande der Altstadt. Seinen Namen hat der Park vom Münsteraner Kardinal Clemens August Graf von Galen (siehe Informationstafel vor dem Denkmal), dem hier für sein Eintreten für Verfolgte während der nationalsozialistischen Gewaltherrschaft ein Denkmal errichtet wurde. Das Monument wurde von einem in Haltern vertretenen Unternehmen im Jubiläumsjahr 1989 gestiftet. Im gleichen Jahr wurden im Rahmen eines Symposiums die Kunstwerke im Park an Ort und Stelle geschaffen.\n\n',NULL,NULL,NULL);
/*!40000 ALTER TABLE `descriptions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `multimedia`
--

DROP TABLE IF EXISTS `multimedia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `multimedia` (
  `idMultimedia` int(11) NOT NULL,
  `idPlace` int(11) DEFAULT NULL,
  `idRoute` int(11) DEFAULT NULL,
  `idUser` int(11) DEFAULT NULL,
  `time` datetime DEFAULT NULL,
  `author` varchar(45) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `file` blob,
  `isVR` tinyint(1) DEFAULT NULL,
  `latitude` decimal(10,8) DEFAULT NULL,
  `longtitude` decimal(10,8) DEFAULT NULL,
  `heading` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`idMultimedia`),
  KEY `place_idx` (`idPlace`),
  KEY `route_idx` (`idRoute`),
  KEY `user_idx` (`idUser`),
  CONSTRAINT `place0` FOREIGN KEY (`idPlace`) REFERENCES `places` (`idPlaces`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `route0` FOREIGN KEY (`idRoute`) REFERENCES `routes` (`idRoutes`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `user0` FOREIGN KEY (`idUser`) REFERENCES `users` (`userid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `multimedia`
--

LOCK TABLES `multimedia` WRITE;
/*!40000 ALTER TABLE `multimedia` DISABLE KEYS */;
/*!40000 ALTER TABLE `multimedia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `places`
--

DROP TABLE IF EXISTS `places`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `places` (
  `idPlaces` int(11) NOT NULL,
  `name` varchar(70) NOT NULL,
  `type` varchar(45) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `latitude` decimal(10,8) DEFAULT NULL,
  `longtitude` decimal(10,9) DEFAULT NULL,
  `altitude` int(11) DEFAULT NULL,
  `postal_code` varchar(20) DEFAULT NULL,
  `city` varchar(58) DEFAULT NULL,
  `street` varchar(45) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `open_times` text,
  `isProposal` tinyint(1) DEFAULT '0',
  `isChange` tinyint(1) DEFAULT '0',
  `idChanging` int(11) DEFAULT NULL,
  PRIMARY KEY (`idPlaces`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `places`
--

LOCK TABLES `places` WRITE;
/*!40000 ALTER TABLE `places` DISABLE KEYS */;
INSERT INTO `places` VALUES (1,'Erlöserkirche','Kirchen',NULL,51.7460111,7.179872222,47,'45721','Haltern','Hennewiger Weg 2',NULL,NULL,0,0,NULL),(2,'Heilig-Kreuz Kirche','Kirchen',NULL,51.72173611,7.168422222,42,'45721','Haltern','Kirchweg 6',NULL,NULL,0,0,NULL),(3,'Marienkirche','Kirchen',NULL,51.74744167,7.185936111,48,'45721','Haltern','Gildenstraße 22',NULL,NULL,0,0,NULL),(4,'Neuapostolische Kirche','Kirchen',NULL,51.74566389,7.177950000,47,'45721','Haltern','Holtwicker Straße 58 - 60',NULL,NULL,0,0,NULL),(5,'Pfarrkirche Sankt Maria Magdalena','Kirchen',NULL,571991389,7.233027778,44,'45721','Haltern / Flaesheim','Stiftsplatz 6',NULL,NULL,0,0,NULL),(6,'Pfarrkirche St. Andreas','Kirchen',NULL,51.73619444,7.289622222,47,'45721','Haltern','Terwellenweg 11',NULL,NULL,0,0,NULL),(7,'Sankt Sixtus Kirche','Kirchen',NULL,51.74277778,7.187205556,44,'45721','Haltern','Markt 10',NULL,NULL,0,0,NULL),(8,'Wallfahrtskapelle Pilgerkirche Sankt Anna','Kirchen',NULL,51.728525,7.155305556,80,'45721','Haltern','Annaberg',NULL,NULL,0,0,NULL),(9,'Pfarrkirche St. Antonius','Kirchen',NULL,51.77861667,7.162977778,67,'45721','Haltern Lavesum','Rekener Straße',NULL,NULL,0,0,NULL),(10,'Pfarrkirche St. Joseph','Kirchen',NULL,51.77394167,7.223061111,50,'45721','Haltern Sythen','Hellweg 11',NULL,NULL,0,0,NULL),(11,'St. Laurentius','Kirchen',NULL,51.74366111,7.172641667,59,'45721','Haltern Sythen','Augustusstr. 24',NULL,NULL,0,0,NULL),(12,'St. Lambertus','Kirchen',NULL,51.71392222,7.09657222,43,'45721','Haltern Lippramsdorf','Pastoratsweg 20',NULL,NULL,0,0,NULL),(13,' Kriegergedächtnis-Kapelle','Kapellen',NULL,51.78089722,7.162961111,67,'45721','Haltern Lavesum','Antoniusstraße 18',NULL,NULL,0,0,NULL),(14,'kleine Bruchsteinkapelle','Kapellen',NULL,51.7434667,7.114180556,83,'45721','Haltern','Tannenberg',NULL,NULL,0,0,NULL),(15,'St. Anna-Kapelle','Kapellen',NULL,51.725875,7.155316667,60,'45721','Haltern','Annaberg',NULL,NULL,0,0,NULL),(16,'St. Katharinen-Kapelle','Kapellen',NULL,51.729001389,7.189041667,41,'45721','Haltern (Hamm-Bossendorf)','Kapellenweg',NULL,NULL,0,0,NULL),(17,'Korten-Keppelken','Kapellen',NULL,51.75912778,7.189800000,66,'45721','Haltern','Prozessionsweg',NULL,NULL,0,0,NULL),(18,'jüdischer Friedhof Haltern','Jüdische Friedhöfe',NULL,51.74188056,7.189077778,43,'45721','Haltern','Richthof',NULL,NULL,0,0,NULL),(19,'Friedhof Sundern','Friedhöfe',NULL,51.75453056,7.164669444,70,'45721','Haltern','Sundernstraße',NULL,NULL,0,0,NULL),(20,'Friedhof Hullern','Friedhöfe',NULL,51.73658056,7.291730556,47,'45721','Haltern','Terwellenweg',NULL,NULL,0,0,NULL),(21,'Friedhof Sythen','Friedhöfe',NULL,51.77106111,7.2203194444,49,'45721','Haltern Sythen','Lehmbrakenerstraße',NULL,NULL,0,0,NULL),(22,'Friedhof Flaesheim (St. Maria-Magdalena)','Friedhöfe',NULL,51.71986944,7.233508333,43,'45721','Haltern','Otto-von-Ravensberg-Straße',NULL,NULL,0,0,NULL),(24,'Friedhof Bossendorf','Friedhöfe',NULL,51.72901667,7.188838889,41,'45721','Haltern','Kapellenweg',NULL,NULL,0,0,NULL),(25,'Gabelkreuz','Wegekreuze',NULL,51.74551389,7.190002778,45,'45721','Haltern','Schüttenwall / Münsterstraße / Johannesstraße',NULL,NULL,0,0,NULL),(26,'Wegekreuz Lohstraße / Gildenstraße','Wegekreuze',NULL,51.74701111,7.184141667,49,'45721','Haltern','Lohstraße / Gildenstraße',NULL,NULL,0,0,NULL),(27,'Wegekreuz am Prozessionsweg','Wegekreuze',NULL,51.75628333,7.195063889,61,'45721','Haltern','Prozessionsweg',NULL,NULL,0,0,NULL),(28,'Mutter Anna','Standbilder',NULL,51.74468056,7.183413889,46,'45721','Haltern','Rekumer Straße',NULL,NULL,0,0,NULL),(29,'Standbild des heiligen Ludgerus','Standbilder',NULL,51.74593333,7.112975000,85,'45721','Haltern','Tannenberger Straße',NULL,NULL,0,0,NULL),(30,'Standbild Rekener Straße','Standbilder',NULL,51.77927222,7.163852778,71,'45721','Haltern / Lavesum','Kardinal-von-Galen-Park',NULL,NULL,0,0,NULL),(31,'Standbild Schützenstraße / Napoleonsweg','Standbilder',NULL,51.78455278,7.163894444,68,'45721','Haltern / Lavesum','Schützenstraße / Napoleonsweg',NULL,NULL,0,0,NULL),(32,'Standbild an der Birkenstraße','Standbilder',NULL,51.71620556,7.092975000,44,'45721','Haltern / Lavesum','Birkenstraße',NULL,NULL,0,0,NULL),(34,'Kardinal-von-Galen-Denkmal','Denkmäler',NULL,51.74196944,7.183650000,43,'45721','Haltern','Kardinal-von-Galen-Park',NULL,NULL,0,0,NULL),(35,'katholischer Friedhof Pfarrgemeinde St. Sixtus','Friedhöfe',NULL,51.74242222,7.19206667,44,'45721','Haltern','Lippspieker',NULL,NULL,0,0,NULL),(36,'Standbild an der Erzbischof-Buddenbrock-Straße','Standbilder',NULL,51.71948889,7.098358333,48,'45721','Haltern / Lavesum','Schützenstraße / Napoleonsweg',NULL,NULL,0,0,NULL);
/*!40000 ALTER TABLE `places` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `placesroutes`
--

DROP TABLE IF EXISTS `placesroutes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `placesroutes` (
  `idPlacesRoutes` int(11) NOT NULL,
  `idPlace` int(11) DEFAULT NULL,
  `idRoute` int(11) DEFAULT NULL,
  PRIMARY KEY (`idPlacesRoutes`),
  UNIQUE KEY `idPlacesRoutes_UNIQUE` (`idPlacesRoutes`),
  KEY `idRoute_idx` (`idRoute`),
  KEY `idPlace_idx` (`idPlace`),
  CONSTRAINT `idPlace1` FOREIGN KEY (`idPlace`) REFERENCES `places` (`idPlaces`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `idRoute1` FOREIGN KEY (`idRoute`) REFERENCES `routes` (`idRoutes`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `placesroutes`
--

LOCK TABLES `placesroutes` WRITE;
/*!40000 ALTER TABLE `placesroutes` DISABLE KEYS */;
/*!40000 ALTER TABLE `placesroutes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questions`
--

DROP TABLE IF EXISTS `questions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questions` (
  `idquestions` int(11) NOT NULL,
  `idPlace` int(11) DEFAULT NULL,
  `question` text,
  `right` varchar(45) DEFAULT NULL,
  `wrong_1` varchar(45) DEFAULT NULL,
  `wrong_2` varchar(45) DEFAULT NULL,
  `wrong_3` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idquestions`),
  KEY `idPlace_idx` (`idPlace`),
  CONSTRAINT `idPlace` FOREIGN KEY (`idPlace`) REFERENCES `places` (`idPlaces`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questions`
--

LOCK TABLES `questions` WRITE;
/*!40000 ALTER TABLE `questions` DISABLE KEYS */;
/*!40000 ALTER TABLE `questions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `routepoints`
--

DROP TABLE IF EXISTS `routepoints`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `routepoints` (
  `idRoutePoints` int(11) NOT NULL,
  `idRoute` int(11) DEFAULT NULL,
  `latitude` decimal(10,8) DEFAULT NULL,
  `longtitude` decimal(10,8) DEFAULT NULL,
  PRIMARY KEY (`idRoutePoints`),
  KEY `idRoute_idx` (`idRoute`),
  CONSTRAINT `idRoute` FOREIGN KEY (`idRoute`) REFERENCES `routes` (`idRoutes`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `routepoints`
--

LOCK TABLES `routepoints` WRITE;
/*!40000 ALTER TABLE `routepoints` DISABLE KEYS */;
/*!40000 ALTER TABLE `routepoints` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `routes`
--

DROP TABLE IF EXISTS `routes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `routes` (
  `idRoutes` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `distance` varchar(45) DEFAULT NULL,
  `duration` varchar(45) DEFAULT NULL,
  `difficulty` int(11) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `equipment` text,
  `start` int(11) DEFAULT NULL,
  `end` int(11) DEFAULT NULL,
  `isProposed` tinyint(1) DEFAULT '0',
  `isChange` tinyint(1) DEFAULT '0',
  `idChanging` int(11) DEFAULT NULL,
  PRIMARY KEY (`idRoutes`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `routes`
--

LOCK TABLES `routes` WRITE;
/*!40000 ALTER TABLE `routes` DISABLE KEYS */;
/*!40000 ALTER TABLE `routes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `searches`
--

DROP TABLE IF EXISTS `searches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `searches` (
  `idSearches` int(11) NOT NULL,
  `idUser` int(11) DEFAULT NULL,
  `query` text,
  PRIMARY KEY (`idSearches`),
  KEY `idUser_idx` (`idUser`),
  CONSTRAINT `idUser` FOREIGN KEY (`idUser`) REFERENCES `users` (`userid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `searches`
--

LOCK TABLES `searches` WRITE;
/*!40000 ALTER TABLE `searches` DISABLE KEYS */;
/*!40000 ALTER TABLE `searches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `todos`
--

DROP TABLE IF EXISTS `todos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `todos` (
  `idToDos` int(11) NOT NULL,
  `idRoute` int(11) DEFAULT NULL,
  `idPlace` int(11) DEFAULT NULL,
  `idUser` int(11) DEFAULT NULL,
  PRIMARY KEY (`idToDos`),
  KEY `idRoute_idx` (`idRoute`),
  KEY `idPlace_idx` (`idPlace`),
  KEY `idUser_idx` (`idUser`),
  CONSTRAINT `idPlace2` FOREIGN KEY (`idPlace`) REFERENCES `places` (`idPlaces`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `idRoute2` FOREIGN KEY (`idRoute`) REFERENCES `routes` (`idRoutes`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `idUser2` FOREIGN KEY (`idUser`) REFERENCES `users` (`userid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `todos`
--

LOCK TABLES `todos` WRITE;
/*!40000 ALTER TABLE `todos` DISABLE KEYS */;
/*!40000 ALTER TABLE `todos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `userid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `password_hash` varchar(45) NOT NULL,
  `verificated` tinyint(1) DEFAULT '0',
  `interest` varchar(45) DEFAULT NULL,
  `language` varchar(45) DEFAULT NULL,
  `walking_speed` varchar(45) DEFAULT NULL,
  `wheelchair` tinyint(1) DEFAULT NULL,
  `bigfont` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-11-14 15:34:15
