-- MySQL dump 10.13  Distrib 5.6.17, for Win64 (x86_64)
--
-- Host: localhost    Database: wanderwiki
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `descriptions`
--

LOCK TABLES `descriptions` WRITE;
/*!40000 ALTER TABLE `descriptions` DISABLE KEYS */;
INSERT INTO `descriptions` VALUES (1,NULL,NULL,'Die Grundsteinlegung dieser neugotischen mit Schiefer gedeckten evangelischen Kirche erfolgte 1911. Die Farbfenster datieren aus dem Jahr 1912. Die im Turm angebrachte Uhr aus dem frühen 20. Jahrhundert ist noch voll funktionsfähig und kann besichtigt werden. Im Inneren befindet sich ein Jugendstilsaal mit an die Seite gerücktem Turm und einhüftiger Emporenanlage.',1,NULL),(2,NULL,NULL,'Die erste Kirche in Hamm, Harne - das bedeutet \"Winkel am Fluss\" - dürfte eine Kapelle des adeligen Hauses Hame oder Hamma gewesen sein. Das Haus Hame entstand auf einem Reichshof (Königshof) Karls des Großen am Fuße des Hammer Berges. Über der Lippenniederung gelegen, ist die Kirche heute Mittelpunkt einer in diesem Bereich noch intakten Streusiedlung. Die Kirche wurde im 12. Jahrhundert im romanischen Stil errichtet. Dieses war zur Zeit der Kreuzüge, daher auch der Name Heilig Kreuz. Dieses Baudenkmal ist gut über den Kanaluferweg per Fahrrad oder zu Fuß zu erreichen.',2,NULL),(3,NULL,NULL,NULL,3,NULL),(4,NULL,NULL,NULL,4,NULL),(5,NULL,NULL,'Die Flaesheimer Pfarrkirche Sankt Maria Magdalena gehört zu den bemerknswertesten und ältesten Kirchenbauten im Halterner Raum. Der romanische Turm stammt mit Sicherheit noch vom Gründungsbau des 1166 von Graf Otto dem Ersten von Ravensberg gegründeten und dem Kloster Knechtsteden unterstellten Prämonstrantenser-Nonnenklosters. Das Kloster wirde 1550 freiweltliches Stift, die Stiftsgebäude sind 1790 abgebrannt. Trotz umfangreicher Literatur ist die Baugeschichte der heute einschiffigen Kirche noch nicht in allen Teilen eindeutig geklärt.',5,NULL),(6,NULL,NULL,'Im Jahr 1895 beauftragte der Kirchvorstand den Architekten Wilhelm Ricklake aus Münster mit der Planung dieser Kirche. Es entstand ein eindrucksvolles Bauwerk in einer neugotischen Backsteinarchitektur mit 2 Jochen, einem Querschiff und einem 65 m hohen Westturm. Am 21. September 1897 wurde die Kirche feierlich eingeweiht. Bei Restaurierungsarbeiten entdeckte die Restaurateurin an den spätgotischen Altarbildern (um 1500), dass sich unter der zweiten Farbschicht in der \"Geißelung\" ein zweites Gesicht befindet. ',6,NULL),(7,NULL,NULL,'Die Pfarrkirche St. Sixtus ist nach Osten ausgerichtet. Zum Inventar gehört das berühmte Gabelkruzifix aus Eichenholz, das um 1330/40 entstanden sein dürfte (Restauration 1961). Es wird alljährlich bei der Kreuzerhöhungsprozession durch die Straßen der Stadt getragen. Diese Tradition geht auf das Jahr 1736 zurück. Daneben ist das 1710 entstandene Epitaph eines der bedeutendstend Kunstschätze dieser Kirche.',7,NULL),(8,NULL,NULL,'Erstmals wurde eine Kapelle auf dem Annaberg im Jahre 1378 urkundlich erwähnt. Ab 1556 fanden die ersten Wallfahrten statt. Die heutige alte Kapelle datiert aus dem Jahre 1967. In der Nähe befindet sich eine Quelle, der zu der damaligen Zeit eine heilkräftige Wirkung nachgesagt wurde. Einige weitere kleine Bauwerke, Gedenkstätten, Kreuzwegstationen und eine Steele können auf dem weitläufigen Areal des Annabergs besucht werden. Viele Vertriebene aus Schlesien haben ihre traditionelle Annaberg-Wallfahrt hierher übertragen.',8,NULL),(9,NULL,NULL,'Die Pfarrkirche Sankt Antonius gibt sich in barocken Formen, entstand aber erst in den Jahren 1921 bis 1924 nach Plänen des münsterischen Dombaumeisters Sunder-Plaßmann.',9,NULL),(10,NULL,NULL,'Die Pfarrkirch Sankt Joseph feierte ihre Gründung im Jahr 1909. Wegen des rapiden Bevölkerungswachstums nach dem 2. Weltkrieg war die Kirche bald zu klein und erfuhr 1959 eine große Erweiterung.',10,NULL),(11,NULL,NULL,'Die Grundsteinlegung erfolgte am 17. Juli 1954. Nach alter Tradition weist die Fassade nach Westen, während der Altar nach Osten ausgerichtet ist. Die Kirche trägt den Namen des heiligen Laurentius, des Mannes, der als Diakon seinem väterlichen Freund, dem Papst Sixtus, treu ergeben war. ',11,NULL),(12,NULL,NULL,'Die Kirche von Lippramsdorf mittem im Herz des Dorfes.',12,NULL),(13,NULL,NULL,'Der einschiffige Bau stammt ursprünglich aus dem Jahr 1467. Er wurde 1652 nach Westen erweitert. An der Nordseite schließt sich eine kleine Sakristei von 1805 an. Bemerkenswert sind die restaurierten Wandmalereien, welche lange Zeit durch Farbschichten überdeckt waren, die aus dem späten 15. Jahrhundert stammen. Die Bilder wurden seinerzeit auf den frischen Verputz (daher: Fresco) gemalt. Erst in den dreißiger Jahren des 20. Jahrhunderts wurden sie wiederentdeckt. Heute dient die Kapelle neben der ursprünglichen Bestimmung als Gotteshaus auch als Denkmal für die Gefallenen der letzten beiden Weltkriege. Im Innern der Kapelle sind Gedenktafeln mit den Namen aller in den beiden Kriegen gefallenen und vermissten Soldaten aus Lavesum angebracht.',13,NULL),(14,NULL,NULL,'Die kleine Bruchsteinkapelle auf dem Tannenberg bei der Bauernschaft gleichen Namens wurde die ihrer heutigen Form 1962 an St. Peter und Paul eingeweiht. Es gibt aber Belege dafür, dass hier schon vor Jahrhunderten eine Kapelle stand. So schrieb der Halterner Pfarrer Hermann Boeker im Jahre 1662, dass die ehedem bei dem Weseler Weg gestandene Kapelle bereits vor Jahrzenten zerstört worden und nur noch in Ruinen vorhanden sei. Der Aufforderungen des Bischofs Christian Bernhard von Galen aus dem Jahre 1669, die Kapelle wieder herzustellen, sollen die Bauern damals nicht nachgekommen sein.',14,NULL),(15,NULL,NULL,'Erstmals wurde eine Kapelle auf dem Annaberg im Jahre 1378 urkundlich erwähnt. Ab 1556 fanden die ersten Wallfahrten statt. Die heutige alte Kapelle datiert aus dem Jahre 1967. In der Nähe befindet sich eine Quelle, der zu der damaligen Zeit eine heilkräftige Wirkung nachgesagt wurde. Einige weitere kleine Bauwerke, Gedenkstätten, Kreuzwegstationen und eine Steele können auf dem weitläufigen Areal des Annabergs besucht werden. Viele Vertriebene aus Schlesien haben ihre traditionelle Annaberg-Wallfahrt hierher übertragen.',15,NULL),(16,NULL,NULL,'Diese Kapelle gilt als ältester Steinbau im Vest. Dem kleinen, flachgedeckten Saalbau mit gerade geschlossenem Chor - vermutlich aus dem 11. Jahrhundert - folgte als spätere Erweiterung das Kirchenschiff mit einem Barockaltar, der mit 1744 - 1746 bezeichnet ist.',16,NULL),(17,NULL,NULL,'Die kleine Kapelle auf dem Prozessionsweg.',17,NULL),(18,NULL,NULL,'Dieser Friedhof befindet sich seit dem Jahre 1767 an dieser Stelle unmittelbar außerhalb des alten Stadtgrabens. Am 26. Januar 1997 wurde für die während der NS-Gewaltherrschaft verfolgten jüdischen Mitbürger ein Gedenkstein enthüllt, der in der Form einer alttestamentarischen Gesetzestafel von einer Halterner Steinbildhauerei gestaltet wurde. Auf dem Grabstein sind neben einem hebräischen Segensspruch die Namen der von 1933 bis 1945 verfolgten Mitbürger und ein Klagelied zu lesen.',18,NULL),(19,NULL,NULL,'Der erste Spatenstich des an der Sundernstraße gelegenen Waldfriedhofes Sundern erfolgte am 19.01.1979. Die Planung beinhaltete für den 1. Abschnitt die Anlegung von 700 Doppelgräbern, 400 Einzelgräbern, 50 Kindergräbern, 50 Urnengräbern und den Bau einer Trauerhalle für ca. 150 Trauergäste. Die Trauerhalle, deren Baukosten ca. 2,4 Millionen Mark betrugen, wurde 1981 fertiggestellt. Die Gesamtfläche des am Waldrand gelegenen Friedhofes umfasst jetzt nach der Erweiterung im Jahre 1998 ca. 52.000 m². ',19,NULL),(20,NULL,NULL,'Die Einsegnung des an der Antruper/Westruper Straße gelegenen Friedhofes Hullern erfolgte am 26.01.1979. Im Jahre 1986 begann die Planung einer Trauerhalle, welche im Jahre 1988 fertiggestellt wurde. Die Baukosten für dieses Objekt beliefen sich auf ca. 610.000 DM. Die Gesamtfläche des am Waldrand gelegenen Friedhofes beträgt ca. 10.000 m². ',20,NULL),(21,NULL,NULL,'Die Einweihung des am Brinkweg gelegenen Friedhofes Sythen erfolgte am 28.06.1960. Im Jahre 1965 wurde dann die Trauerhalle, deren Baukosten sich auf ca. 138.000 DM beliefen, fertiggestellt. Die Gesamtfläche des Friedhofes umfasst ca. 29.000 m². ',21,NULL),(22,NULL,NULL,'Die Einweihung des an der Otto-von-Ravensberg-Straße gelegenen Friedhofes Flaesheim erfolgte am 08.10.1967. Im Jahre 1971 wurde dann die Trauerhalle, deren Baukosten sich auf ca. 120.000 DM beliefen, fertiggestellt. Die Gesamtfläche des Friedhofes umfasst ca. 24.000 m².',22,NULL),(23,NULL,NULL,'Der Friedhof der St. Sixtus Gemeinde liegt zwischen Lippspieker und Hullerner Straße.  Der im Jahr 1810 eröffnete Friedhof St. Sixtus ist mit 23.000 m² nicht nur der größte kath. Friedhof in Haltern, sondern kann heute durch Lage und Funktion auch neben dem Kommunalfriedhof als Hauptfriedhof bezeichnet werden. Das zentrale Friedhofskreuz und der Kreuzweg mit seinen 14 Stationen geben dem Friedhof einen besonderen Charakter, seine Lage am Rande der Altstadt zeugt von der alten Verbindung der Lebenden mit den Verstorbenen. ',23,NULL),(24,NULL,NULL,'Auf diesem Friedhof steht die sehr bekannte St. Katharinen-Kapelle.',24,NULL),(25,NULL,NULL,'',25,NULL),(26,NULL,NULL,'In der Nähe dieses Wegekreuzes liegt die Marienkirche.',26,NULL),(27,NULL,NULL,'Am Prozessionsweg der Stadt Haltern am See sind mehrere Wegekreuze, Standbilder, etc. zu finden.',27,NULL),(28,NULL,NULL,'Das Mutter-Anna-Standbild wurde in einer Halterner Bildhauerwerkstatt hergestellt. Alljährlich im September, am traditionellen Fest der \"Kreuzerhöhung\", zieht hier auch die Halterner Kreuztracht mit dem fast 700 Jahre alten Gabelkreuz der Sixtuskirche vorbei. Im Gebäude \"Muttergottesstiege\" hinter der Statue befinden sich u.a. die Zweigstelle der Volkshochschule Dülmen/ Haltern am See/ Havixbeck sowie Büros der Stadtverwaltung Haltern am See.',28,NULL),(29,NULL,NULL,'Dieses ist eine seltene Darstellung in dieser Gegend. Das Standbild zeigt den heiligen Ludgerus im Bischofsformat und zu seinen Füßen eine Gans. Der Legende nach soll der Bischof die Bauern, als sie ihn um entsprechenden Rat baten, angewiesen haben, an dieser Stelle nach einem Brunnen zu graben. Sie befolgten seinen Rat, und angeblich ist dieser Brunnen noch niemals versiegt und hat auch in den trockensten Sommern stets Wasser gegeben.',29,NULL),(30,NULL,NULL,'In der Nähe liegt die St. Antonius Kirche von Lavesum.',30,NULL),(31,NULL,NULL,'In der Nähe liegt die St. Antonius Kirche von Lavesum.',31,NULL),(32,NULL,NULL,'In der Nähe liegt die St. Lambertus Kirche von Lippramsdorf.',32,NULL),(33,NULL,NULL,'In der Nähe liegt die St. Antonius Kirche von Lavesum.',33,NULL),(34,NULL,NULL,'Der Kardinal-von-Galen-Park ist die grüne Oase am Rande der Altstadt. Seinen Namen hat der Park vom Münsteraner Kardinal Clemens August Graf von Galen (siehe Informationstafel vor dem Denkmal), dem hier für sein Eintreten für Verfolgte während der nationalsozialistischen Gewaltherrschaft ein Denkmal errichtet wurde. Das Monument wurde von einem in Haltern vertretenen Unternehmen im Jubiläumsjahr 1989 gestiftet. Im gleichen Jahr wurden im Rahmen eines Symposiums die Kunstwerke im Park an Ort und Stelle geschaffen.',34,NULL);
/*!40000 ALTER TABLE `descriptions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `multimedia`
--

LOCK TABLES `multimedia` WRITE;
/*!40000 ALTER TABLE `multimedia` DISABLE KEYS */;
/*!40000 ALTER TABLE `multimedia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `places`
--

LOCK TABLES `places` WRITE;
/*!40000 ALTER TABLE `places` DISABLE KEYS */;
INSERT INTO `places` VALUES (1,47,'Haltern',NULL,NULL,0,0,51.74601111,7.179872222,'Erlöserkirche',NULL,'45721',30,'Hennewiger Weg 2','church'),(2,42,'Haltern',NULL,NULL,0,0,51.72173611,7.168422222,'Heilig-Kreuz Kirche',NULL,'45721',15,'Kirchweg 6','church'),(3,48,'Haltern',NULL,NULL,0,0,51.74744167,7.185936111,'Marienkirche',NULL,'45721',25,'Gildenstraße 22','church'),(4,47,'Haltern',NULL,NULL,0,0,51.74566389,7.17795,'Neuapostolische Kirche',NULL,'45721',25,'Holtwicker Straße 58 - 60','church'),(5,44,'Haltern / Flaesheim',NULL,NULL,0,0,51.71991389,7.233027778,'Pfarrkirche Sankt Maria Magdalena',NULL,'45721',20,'Stiftsplatz 6','church'),(6,47,'Haltern',NULL,NULL,0,0,51.73619444,7.289622222,'Pfarrkirche St. Andreas',NULL,'45721',20,'Terwellenweg 11','church'),(7,44,'Haltern',NULL,NULL,0,0,51.74277778,7.187205556,'Sankt Sixtus Kirche',NULL,'45721',20,'Markt 10','church'),(8,80,'Haltern',NULL,NULL,0,0,51.728525,7.155305556,'Wallfahrtskapelle Pilgerkirche Sankt Anna',NULL,'45721',65,'Annaberg','church'),(9,67,'Haltern Lavesum',NULL,NULL,0,0,51.77861667,7.162977778,'Pfarrkirche St. Antonius',NULL,'45721',30,'Rekener Straße','church'),(10,50,'Haltern Sythen',NULL,NULL,0,0,51.77394167,7.223061111,'Pfarrkirche St. Joseph',NULL,'45721',15,'Hellweg 11','church'),(11,59,'Haltern Sythen',NULL,NULL,0,0,51.74366111,7.172641667,'St. Laurentius',NULL,'45721',25,'Augustusstr. 24','church'),(12,43,'Haltern Lippramsdorf',NULL,NULL,0,0,51.71392222,7.096597222,'St. Lambertus',NULL,'45721',15,'Pastoratsweg 20','church'),(13,67,'Haltern Lavesum',NULL,NULL,0,0,51.78089722,7.162961111,' Kriegergedächtnis-Kapelle',NULL,'45721',20,'Antoniusstraße 18','chapel'),(14,83,'Haltern',NULL,NULL,0,0,51.74346667,7.114180556,'kleine Bruchsteinkapelle',NULL,'45721',12,'Tannenberg','chapel'),(15,60,'Haltern',NULL,NULL,0,0,51.725875,7.155316667,'St. Anna-Kapelle',NULL,'45721',15,'Annaberg','chapel'),(16,41,'Haltern (Hamm-Bossendorf)',NULL,NULL,0,0,51.72901389,7.189041667,'St. Katharinen-Kapelle',NULL,'45721',15,'Kapellenweg','chapel'),(17,66,'Haltern',NULL,NULL,0,0,51.75912778,7.1898,'Korten-Keppelken',NULL,'45721',15,'Prozessionsweg','chapel'),(18,43,'Haltern',NULL,NULL,0,0,51.74188056,7.189077778,'jüdischer Friedhof Haltern',NULL,'45721',15,'Richthof','jewish_graveyard'),(19,70,'Haltern',NULL,NULL,0,0,51.75453056,7.164669444,'Friedhof Sundern',NULL,'45721',15,'Sundernstraße','graveyard'),(20,47,'Haltern',NULL,NULL,0,0,51.73658056,7.291730556,'Friedhof Hullern',NULL,'45721',15,'Terwellenweg','graveyard'),(21,49,'Haltern Sythen',NULL,NULL,0,0,51.77106111,7.220319444,'Friedhof Sythen',NULL,'45721',15,'Lehmbrakenerstraße','graveyard'),(22,43,'Haltern',NULL,NULL,0,0,51.71986944,7.233508333,'Friedhof Flaesheim (St. Maria-Magdalena)',NULL,'45721',15,'Otto-von-Ravensberg-Straße','graveyard'),(23,44,'Haltern',NULL,NULL,0,0,51.74242222,7.192066667,'katholischer Friedhof Pfarrgemeinde St. Sixtus',NULL,'45721',15,'Lippspieker','graveyard'),(24,41,'Haltern',NULL,NULL,0,0,51.72901667,7.188838889,'Friedhof Bossendorf',NULL,'45721',10,'Kapellenweg','graveyard'),(25,45,'Haltern',NULL,NULL,0,0,51.74551389,7.190002778,'Gabelkreuz',NULL,'45721',15,'Schüttenwall / Münsterstraße / Johannesstraße','cross'),(26,49,'Haltern',NULL,NULL,0,0,51.74701111,7.184141667,'Wegekreuz Lohstraße / Gildenstraße',NULL,'45721',15,'Lohstraße / Gildenstraße','cross'),(27,61,'Haltern',NULL,NULL,0,0,51.75628333,7.195063889,'Wegekreuz am Prozessionsweg',NULL,'45721',12,'Prozessionsweg','cross'),(28,46,'Haltern',NULL,NULL,0,0,51.74468056,7.183413889,'Mutter Anna',NULL,'45721',7,'Rekumer Straße','statue'),(29,85,'Haltern',NULL,NULL,0,0,51.74593333,7.112975,'Standbild des heiligen Ludgerus',NULL,'45721',12,'Tannenberger Straße','statue'),(30,71,'Haltern / Lavesum',NULL,NULL,0,0,51.77927222,7.163852778,'Standbild Rekener Straße ',NULL,'45721',7,'Kardinal-von-Galen-Park','statue'),(31,68,'Haltern / Lavesum',NULL,NULL,0,0,51.78455278,7.163894444,'Standbild Schützenstraße / Napoleonsweg',NULL,'45721',15,'Schützenstraße / Napoleonsweg','statue'),(32,44,'Haltern / Lavesum',NULL,NULL,0,0,51.71620556,7.092975,'Standbild an der Birkenstraße',NULL,'45721',11,'Birkenstraße','statue'),(33,48,'Haltern / Lavesum',NULL,NULL,0,0,51.7194889,7.098358333,'Standbild an der Erzbischof-Buddenbrock-Straße',NULL,'45721',8,'Schützenstraße / Napoleonsweg','statue'),(34,43,'Haltern',NULL,NULL,0,0,51.74196944,7.18365,'Kardinal-von-Galen-Denkmal',NULL,'45721',12,'Kardinal-von-Galen-Park','monument');
/*!40000 ALTER TABLE `places` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `placesroutes`
--

LOCK TABLES `placesroutes` WRITE;
/*!40000 ALTER TABLE `placesroutes` DISABLE KEYS */;
/*!40000 ALTER TABLE `placesroutes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `questions`
--

LOCK TABLES `questions` WRITE;
/*!40000 ALTER TABLE `questions` DISABLE KEYS */;
/*!40000 ALTER TABLE `questions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `routepoints`
--

LOCK TABLES `routepoints` WRITE;
/*!40000 ALTER TABLE `routepoints` DISABLE KEYS */;
/*!40000 ALTER TABLE `routepoints` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `routes`
--

LOCK TABLES `routes` WRITE;
/*!40000 ALTER TABLE `routes` DISABLE KEYS */;
/*!40000 ALTER TABLE `routes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `searches`
--

LOCK TABLES `searches` WRITE;
/*!40000 ALTER TABLE `searches` DISABLE KEYS */;
/*!40000 ALTER TABLE `searches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `todos`
--

LOCK TABLES `todos` WRITE;
/*!40000 ALTER TABLE `todos` DISABLE KEYS */;
/*!40000 ALTER TABLE `todos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-04-07 12:10:21
