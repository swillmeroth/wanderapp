CREATE DATABASE  IF NOT EXISTS `wanderwiki` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `wanderwiki`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win64 (x86_64)
--
-- Host: localhost    Database: wanderwiki
-- ------------------------------------------------------
-- Server version	5.6.21-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `idComments` int(11) NOT NULL AUTO_INCREMENT,
  `rating` BIGINT DEFAULT NULL,
  `text` longtext,
  `time` datetime DEFAULT NULL,
  `idPlace` int(11) DEFAULT NULL,
  `idRoute` int(11) DEFAULT NULL,
  `idUser` int(11) DEFAULT NULL,
  PRIMARY KEY (`idComments`),
  UNIQUE KEY `idComments_UNIQUE` (`idComments`),
  KEY `FK_comments_idUser` (`idUser`),
  KEY `FK_comments_idPlace` (`idPlace`),
  KEY `FK_comments_idRoute` (`idRoute`),
  CONSTRAINT `FK_comments_idPlace` FOREIGN KEY (`idPlace`) REFERENCES `places` (`idPlaces`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_comments_idRoute` FOREIGN KEY (`idRoute`) REFERENCES `routes` (`idRoutes`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_comments_idUser` FOREIGN KEY (`idUser`) REFERENCES `users` (`userid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `descriptions`
--

DROP TABLE IF EXISTS `descriptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `descriptions` (
  `idDescriptions` int(11) NOT NULL AUTO_INCREMENT,
  `interest` varchar(255) DEFAULT NULL,
  `language` varchar(255) DEFAULT NULL,
  `text` longtext,
  `idPlace` int(11) DEFAULT NULL,
  `idRoute` int(11) DEFAULT NULL,
  PRIMARY KEY (`idDescriptions`),
  UNIQUE KEY `idDescriptions_UNIQUE` (`idDescriptions`),
  KEY `FK_descriptions_idPlace` (`idPlace`),
  KEY `FK_descriptions_idRoute` (`idRoute`),
  CONSTRAINT `FK_descriptions_idPlace` FOREIGN KEY (`idPlace`) REFERENCES `places` (`idPlaces`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_descriptions_idRoute` FOREIGN KEY (`idRoute`) REFERENCES `routes` (`idRoutes`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `multimedia`
--

DROP TABLE IF EXISTS `multimedia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `multimedia` (
  `idMultimedia` int(11) NOT NULL AUTO_INCREMENT,
  `file` longblob,
  `heading` bigint(20) DEFAULT NULL,
  `isVR` tinyint(1) DEFAULT '0',
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `time` datetime DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `idPlace` int(11) DEFAULT NULL,
  `idRoute` int(11) DEFAULT NULL,
  `idUser` int(11) DEFAULT NULL,
  PRIMARY KEY (`idMultimedia`),
  UNIQUE KEY `idMultimedia_UNIQUE` (`idMultimedia`),
  KEY `FK_multimedia_idPlace` (`idPlace`),
  KEY `FK_multimedia_idRoute` (`idRoute`),
  KEY `FK_multimedia_idUser` (`idUser`),
  CONSTRAINT `FK_multimedia_idPlace` FOREIGN KEY (`idPlace`) REFERENCES `places` (`idPlaces`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_multimedia_idRoute` FOREIGN KEY (`idRoute`) REFERENCES `routes` (`idRoutes`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_multimedia_idUser` FOREIGN KEY (`idUser`) REFERENCES `users` (`userid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `places`
--

DROP TABLE IF EXISTS `places`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `places` (
  `idPlaces` int(11) NOT NULL AUTO_INCREMENT,
  `altitude` int(11) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `idChanging` int(11) DEFAULT NULL,
  `isChange` tinyint(1) DEFAULT '0',
  `isProposal` tinyint(1) DEFAULT '0',
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `open_times` longtext,
  `postal_code` varchar(255) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `street` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idPlaces`),
  UNIQUE KEY `idPlaces_UNIQUE` (`idPlaces`),
  KEY `FK_places_idChanging_idx` (`idChanging`),
  CONSTRAINT `FK_places_idChanging` FOREIGN KEY (`idChanging`) REFERENCES `places` (`idPlaces`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `placesroutes`
--

DROP TABLE IF EXISTS `placesroutes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `placesroutes` (
  `idPlacesRoutes` int(11) NOT NULL AUTO_INCREMENT,
  `idPlace` int(11) DEFAULT NULL,
  `idRoute` int(11) DEFAULT NULL,
  PRIMARY KEY (`idPlacesRoutes`),
  UNIQUE KEY `idPlacesRoutes_UNIQUE` (`idPlacesRoutes`),
  KEY `FK_placesroutes_idPlace` (`idPlace`),
  KEY `FK_placesroutes_idRoute` (`idRoute`),
  CONSTRAINT `FK_placesroutes_idPlace` FOREIGN KEY (`idPlace`) REFERENCES `places` (`idPlaces`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_placesroutes_idRoute` FOREIGN KEY (`idRoute`) REFERENCES `routes` (`idRoutes`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `questions`
--

DROP TABLE IF EXISTS `questions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questions` (
  `idquestions` int(11) NOT NULL AUTO_INCREMENT,
  `question` longtext,
  `right_ans` varchar(255) DEFAULT NULL,
  `wrong_1` varchar(255) DEFAULT NULL,
  `wrong_2` varchar(255) DEFAULT NULL,
  `wrong_3` varchar(255) DEFAULT NULL,
  `idPlace` int(11) NOT NULL,
  PRIMARY KEY (`idquestions`),
  UNIQUE KEY `idquestions_UNIQUE` (`idquestions`),
  KEY `FK_questions_idPlace` (`idPlace`),
  CONSTRAINT `FK_questions_idPlace` FOREIGN KEY (`idPlace`) REFERENCES `places` (`idPlaces`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `routepoints`
--

DROP TABLE IF EXISTS `routepoints`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `routepoints` (
  `idRoutePoints` int(11) NOT NULL AUTO_INCREMENT,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `num` int(11) DEFAULT NULL,
  `idRoute` int(11) NOT NULL,
  PRIMARY KEY (`idRoutePoints`),
  UNIQUE KEY `idRoutePoints_UNIQUE` (`idRoutePoints`),
  KEY `FK_routepoints_idRoute` (`idRoute`),
  CONSTRAINT `FK_routepoints_idRoute` FOREIGN KEY (`idRoute`) REFERENCES `routes` (`idRoutes`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `routes`
--

DROP TABLE IF EXISTS `routes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `routes` (
  `idRoutes` int(11) NOT NULL AUTO_INCREMENT,
  `difficulty` int(11) DEFAULT NULL,
  `distance` int(11) DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `end` int(11) DEFAULT NULL,
  `equipment` longtext,
  `idChanging` int(11) DEFAULT NULL,
  `isChange` tinyint(1) DEFAULT '0',
  `isProposed` tinyint(1) DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `start` int(11) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idRoutes`),
  UNIQUE KEY `idRoutes_UNIQUE` (`idRoutes`),
  KEY `FK_routes_idChanging_idx` (`idChanging`),
  CONSTRAINT `FK_routes_idChanging` FOREIGN KEY (`idChanging`) REFERENCES `routes` (`idRoutes`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `searches`
--

DROP TABLE IF EXISTS `searches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `searches` (
  `idSearches` int(11) NOT NULL AUTO_INCREMENT,
  `query` longtext,
  `idUser` int(11) DEFAULT NULL,
  PRIMARY KEY (`idSearches`),
  UNIQUE KEY `idSearches_UNIQUE` (`idSearches`),
  KEY `FK_searches_idUser` (`idUser`),
  CONSTRAINT `FK_searches_idUser` FOREIGN KEY (`idUser`) REFERENCES `users` (`userid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `todos`
--

DROP TABLE IF EXISTS `todos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `todos` (
  `idToDos` int(11) NOT NULL AUTO_INCREMENT,
  `idPlace` int(11) DEFAULT NULL,
  `idRoute` int(11) DEFAULT NULL,
  `idUser` int(11) DEFAULT NULL,
  PRIMARY KEY (`idToDos`),
  UNIQUE KEY `idToDos_UNIQUE` (`idToDos`),
  KEY `FK_todos_idPlace` (`idPlace`),
  KEY `FK_todos_idRoute` (`idRoute`),
  KEY `FK_todos_idUser` (`idUser`),
  CONSTRAINT `FK_todos_idPlace` FOREIGN KEY (`idPlace`) REFERENCES `places` (`idPlaces`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_todos_idRoute` FOREIGN KEY (`idRoute`) REFERENCES `routes` (`idRoutes`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_todos_idUser` FOREIGN KEY (`idUser`) REFERENCES `users` (`userid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `userid` int(11) NOT NULL AUTO_INCREMENT,
  `bigfont` tinyint(1) DEFAULT '0',
  `email` varchar(255) DEFAULT NULL,
  `interest` varchar(255) DEFAULT NULL,
  `language` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `password_hash` varchar(255) DEFAULT NULL,
  `verificated` tinyint(1) DEFAULT '0',
  `walking_speed` varchar(255) DEFAULT NULL,
  `wheelchair` tinyint(1) DEFAULT '0',
  `userlevel` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`userid`),
  UNIQUE KEY `userid_UNIQUE` (`userid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-04-02 12:48:01
