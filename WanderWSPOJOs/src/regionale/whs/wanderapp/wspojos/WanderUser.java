package regionale.whs.wanderapp.wspojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import regionale.whs.wanderapp.wspojos.validation.Validator;

/**
 *
 * @author MartinM
 */
public class WanderUser extends WanderObjekt {

    
    @Expose @SerializedName("bfont")
    public boolean useBigFont;
    
    @Expose @SerializedName("email")
    public String email;
    
    @Expose @SerializedName("int")
    public String interest;
    
    @Expose @SerializedName("lang")
    public String language;
    
    @Expose @SerializedName("name")
    public String name;
    
//    @Expose @SerializedName("pwh")
//    public String passwordHash;
    
    @Expose @SerializedName("valid")
    public boolean isVerified;
    
    @Expose @SerializedName("speed")
    public String walkingSpeed;
    
    @Expose @SerializedName("wchair")
    public boolean isWheelchairUser;
    
    @Expose @SerializedName("cc")
    public int commentCount;
    
    @Expose @SerializedName("mc")
    public int multimediaCount;
    
    @Expose @SerializedName("ulvl")
    public int userLevel;
    
    public boolean isUser() {
        return userLevel == 0;
    }
    
    public boolean isAdmin() {
        return userLevel >= 2;
    }
    
    public WanderUser() {
        email = "";
        interest = "";
        language = "";
        name = "";
        walkingSpeed = "normal";
    }

    public int getUserLevel() {
        return userLevel;
    }

    public void setUserLevel(int userLevel) {
        this.userLevel = userLevel;
    }

    public int getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(int commentCount) {
        this.commentCount = commentCount;
    }

    public int getMultimediaCount() {
        return multimediaCount;
    }

    public void setMultimediaCount(int multimediaCount) {
        this.multimediaCount = multimediaCount;
    }

    public boolean getUseBigFont() {
        return useBigFont;
    }

    public void setUseBigFont(boolean useBigFont) {
        this.useBigFont = useBigFont;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getInterest() {
        return interest;
    }

    public void setInterest(String interest) {
        this.interest = interest;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

//    public String getPasswordHash() {
//        return passwordHash;
//    }
//
//    public void setPasswordHash(String passwordHash) {
//        this.passwordHash = passwordHash;
//    }

    public boolean getIsVerified() {
        return isVerified;
    }

    public void setIsVerified(boolean isVerified) {
        this.isVerified = isVerified;
    }

    public String getWalkingSpeed() {
        return walkingSpeed;
    }

    public void setWalkingSpeed(String walkingSpeed) {
        this.walkingSpeed = walkingSpeed;
    }

    public boolean getIsWheelchairUser() {
        return isWheelchairUser;
    }

    public void setIsWheelchairUser(boolean isWheelchairUser) {
        this.isWheelchairUser = isWheelchairUser;
    }
    
    public static void validate(WanderUser wu) {
        if (wu == null) Validator.complain("WanderUser is null.");
        if (wu.id < 0) Validator.complain("WanderUser.id is less than 0.");
        if (!Validator.isEmail(wu.email))
            Validator.complain("WanderUser.email is null or not valid.");
        Validator.validate(wu.name, "WanderUser.name", 5, 255);
        Validator.validate(wu.userLevel, "WanderUser.userLevel", 0, 2);
        if (!Validator.isInterest(wu.interest))
            Validator.complain("WanderUser.interest is null or unknown.");
        if (!Validator.isLanguage(wu.language))
            Validator.complain("WanderUser.language is null or unknown.");
        if (!Validator.isWalkingSpeed(wu.walkingSpeed))
            Validator.complain("WanderUser.walkingSpeed is null or unknown.");
        
    }

    @Override
    public String toString() {
        return "WanderUser{" + "useBigFont=" + useBigFont + ", email=" + email + ", interest=" + interest + ", language=" + language + ", name=" + name + ", isVerified=" + isVerified + ", walkingSpeed=" + walkingSpeed + ", isWheelchairUser=" + isWheelchairUser + ", commentCount=" + commentCount + ", multimediaCount=" + multimediaCount + ", userLevel=" + userLevel + '}';
    }
}
