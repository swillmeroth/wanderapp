package regionale.whs.wanderapp.wspojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import regionale.whs.wanderapp.wspojos.exceptions.ValidationException;
import regionale.whs.wanderapp.wspojos.validation.Validator;

/**
 *
 * @author MartinM
 */
public class WanderPOI extends WanderObjekt {
    
    @Expose @SerializedName("city")
    public String city;
    
    @Expose @SerializedName("time")
    public Date dateAdded;
    
    @Expose @SerializedName("idp")
    public int idChanging; // id of the place that this is a change to
    
    @Expose @SerializedName("ch")
    public boolean isChange; // whether this is a change to an existing place
    
    @Expose @SerializedName("pp")
    public boolean isProposal; // whether this is a change that is a proposal, i.e. not (yet) accepted
    
    @Expose @SerializedName("gl")
    public GeoLocation koordinaten;
    
    @Expose @SerializedName("name")
    public String name;
    
    @Expose @SerializedName("times")
    public String openTimes;
    
    @Expose @SerializedName("postal")
    public String postalCode;
    
    @Expose @SerializedName("sz")
    public int size;
    
    @Expose @SerializedName("str")
    public String street;
    
    @Expose @SerializedName("ptype")
    public String poiType;
    
    @Expose @SerializedName("cc")
    public int commentCount;
    
    @Expose @SerializedName("mc")
    public int multimediaCount;
    
    @Expose @SerializedName("tc")
    public int todosCount;
    
    @Expose @SerializedName("score")
    public float rating;
    
    @Expose @SerializedName("descriptions")
    public List<WanderDescription> descriptions;
    
    public WanderPOI() {
        city = "";
        koordinaten = new GeoLocation();
        name = "";
        openTimes = "";
        postalCode = "";
        street = "";
        poiType = "church";
        descriptions = new LinkedList<>();
    }

    public List<WanderDescription> getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(List<WanderDescription> descriptions) {
        this.descriptions = descriptions;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public int getMultimediaCount() {
        return multimediaCount;
    }

    public void setMultimediaCount(int multimediaCount) {
        this.multimediaCount = multimediaCount;
    }

    public int getTodosCount() {
        return todosCount;
    }

    public void setTodosCount(int todosCount) {
        this.todosCount = todosCount;
    }

    public int getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(int commentCount) {
        this.commentCount = commentCount;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Date getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(Date dateAdded) {
        this.dateAdded = dateAdded;
    }

    public int getIdChanging() {
        return idChanging;
    }

    public void setIdChanging(int idChanging) {
        this.idChanging = idChanging;
    }

    public boolean getIsChange() {
        return isChange;
    }

    public void setIsChange(boolean isChange) {
        this.isChange = isChange;
    }

    public boolean getIsProposal() {
        return isProposal;
    }

    public void setIsProposal(boolean isProposal) {
        this.isProposal = isProposal;
    }

    public GeoLocation getKoordinaten() {
        return koordinaten;
    }

    public void setKoordinaten(GeoLocation koordinaten) {
        this.koordinaten = koordinaten;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOpenTimes() {
        return openTimes;
    }

    public void setOpenTimes(String openTimes) {
        this.openTimes = openTimes;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getPoiType() {
        return poiType;
    }

    public void setPoiType(String poiType) {
        this.poiType = poiType;
    }

    public static void validate(WanderPOI poi) {
        if (poi == null) Validator.complain("WanderPOI is null.");
        if (poi.id < 0) Validator.complain("WanderPOI.id is less than 0.");
        GeoLocation.validate(poi.koordinaten);
        Validator.validate(poi.city, "WanderPOI.city", 0, 255);
        Validator.validate(poi.name, "WanderPOI.name", 0, 255);
        Validator.validate(poi.street, "WanderPOI.street", 0, 255);
        Validator.validate(poi.postalCode, "WanderPOI.postalCode", 5, 5);
        if (!Validator.isPOIType(poi.poiType))
            Validator.complain("WanderPOI.poiType is null or unknown.");
        Validator.validate(poi.openTimes, "WanderPOI.openTimes", 0, 255);
        if (poi.size < 0) Validator.complain("WanderPOI.size is less than 0.");
        if (poi.idChanging < 0) Validator.complain("WanderPOI.idChanging is less than 0.");
        if (poi.descriptions == null) Validator.complain("WanderPOI.descriptions is null.");
        try {
            for (WanderDescription d : poi.descriptions) {
                WanderDescription.validate(d);
                if (poi.descriptions.indexOf(d) != poi.descriptions.lastIndexOf(d))
                    Validator.complain("Contains multiple occurences of the same description.");
            }
        } catch (ValidationException ex) {
            Validator.complain("Error in WanderPOI.descriptions: " + ex.getMessage());
        }
    }

    @Override
    public String toString() {
        return "WanderPOI{" + "city=" + city + ", dateAdded=" + dateAdded + ", idChanging=" + idChanging + ", isChange=" + isChange + ", isProposal=" + isProposal + ", koordinaten=" + koordinaten + ", name=" + name + ", openTimes=" + openTimes + ", postalCode=" + postalCode + ", size=" + size + ", street=" + street + ", poiType=" + poiType + ", commentCount=" + commentCount + ", multimediaCount=" + multimediaCount + ", todosCount=" + todosCount + ", rating=" + rating + ", descriptions=" + descriptions + '}';
    }
    
}
