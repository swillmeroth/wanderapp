package regionale.whs.wanderapp.wspojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author MartinM
 */
public class Kartenausschnitt {
    @Expose @SerializedName("pois")
    public List<WanderPOI> poiListe;
    
    @Expose @SerializedName("routes")
    public List<WanderRoute> routenListe;
    
    public Kartenausschnitt() {
        poiListe = new LinkedList<>();
        routenListe = new LinkedList<>();
    }

    public List<WanderPOI> getPoiListe() {
        return poiListe;
    }

    public void setPoiListe(List<WanderPOI> poiListe) {
        this.poiListe = poiListe;
    }

    public List<WanderRoute> getRoutenListe() {
        return routenListe;
    }

    public void setRoutenListe(List<WanderRoute> routenListe) {
        this.routenListe = routenListe;
    }

    @Override
    public String toString() {
        return "Kartenausschnitt{" + "poiListe=" + poiListe + ", routenListe=" + routenListe + '}';
    }
}
