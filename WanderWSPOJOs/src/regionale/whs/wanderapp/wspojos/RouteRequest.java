package regionale.whs.wanderapp.wspojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import regionale.whs.wanderapp.wspojos.exceptions.ValidationException;
import regionale.whs.wanderapp.wspojos.validation.Validator;

/**
 *
 * @author MartinM
 */
public class RouteRequest {
    /**
     * In minutes.
     */
    @Expose @SerializedName("dur")
    public int duration;
    
    /**
     * "path" or "loop"
     */
    @Expose @SerializedName("type")
    public String routeType;
    
    @Expose @SerializedName("from")
    public GeoLocation start;
    
    @Expose @SerializedName("to")
    public GeoLocation end;
    
    public RouteRequest() {
        duration = 60;
        routeType = "path";
        start = new GeoLocation();
        end = new GeoLocation();
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getRouteType() {
        return routeType;
    }

    public void setRouteType(String routeType) {
        this.routeType = routeType;
    }

    public GeoLocation getStart() {
        return start;
    }

    public void setStart(GeoLocation start) {
        this.start = start;
    }

    public GeoLocation getEnd() {
        return end;
    }

    public void setEnd(GeoLocation end) {
        this.end = end;
    }
    
    public boolean isPathRequest() {
        return "path".equals(routeType);
    }
    
    public boolean isLoopRequest() {
        return "loop".equals(routeType);
    }
    
    public static void validate(RouteRequest rr) {
        if (rr == null) Validator.complain("RouteRequest is null.");
        Validator.validate(rr.duration, "RouteRequest.duration", 15, 8 * 60);
        try {
            GeoLocation.validate(rr.start);
        } catch (ValidationException ex) {
            Validator.complain("Error in RouteRequest.start: " + ex.getMessage());
        }
        try {
            if (rr.routeType.equals("path")) {
                GeoLocation.validate(rr.end);
            }
        } catch (ValidationException ex) {
            Validator.complain("Error in RouteRequest.end: " + ex.getMessage());
        }
    }

    @Override
    public String toString() {
        return "RouteRequest{" + "duration=" + duration + ", routeType=" + routeType + ", start=" + start + ", end=" + end + '}';
    }
}
