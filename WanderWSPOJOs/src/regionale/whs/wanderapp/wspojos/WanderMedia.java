package regionale.whs.wanderapp.wspojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.awt.image.BufferedImage;
import java.util.Date;
import regionale.whs.wanderapp.wspojos.exceptions.ValidationException;
import regionale.whs.wanderapp.wspojos.validation.Validator;

/**
 *
 * @author MartinM
 */
public class WanderMedia extends WanderObjekt {
    
    @Expose @SerializedName("idp")
    public int idPlace;
    
    @Expose @SerializedName("idr")
    public int idRoute;
    
    @Expose @SerializedName("ida")
    public int idAuthor;
    
    @Expose @SerializedName("dat")
    public byte[] data;
    
    @Expose @SerializedName("time")
    public Date time;
    
    @Expose @SerializedName("mtype")
    public String mediaType;

    public WanderMedia() {
        data = new byte[0];
        mediaType = "image"; // TODO: Klären
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public String getMediaType() {
        return mediaType;
    }

    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    public int getIdPlace() {
        return idPlace;
    }

    public void setIdPlace(int idPlace) {
        this.idPlace = idPlace;
    }

    public int getIdRoute() {
        return idRoute;
    }

    public void setIdRoute(int idRoute) {
        this.idRoute = idRoute;
    }

    public int getIdAuthor() {
        return idAuthor;
    }

    public void setIdAuthor(int idAuthor) {
        this.idAuthor = idAuthor;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }
    
    public static void validate(WanderMedia wm) {
        if (wm == null) Validator.complain("WanderMedia is null.");
        if (wm.id < 0) Validator.complain("WanderMedia has id < 0. Id must be 0 (meaning new object) or greater than 0.");
        if (wm.data == null) Validator.complain("WanderMedia.data is null.");
        if (wm.data.length == 0) Validator.complain("WanderMedia.data.length is 0.");
        if (wm.idPlace < 0) Validator.complain("WanderMedia.idPlace is less than 0.");
        if (wm.idRoute < 0) Validator.complain("WanderMedia.idRoute is less than 0.");
        if ((wm.idPlace == 0) && (wm.idRoute == 0))
            Validator.complain("WanderMedia.idPlace and WanderMedia.idRoute are both 0.");
        if ((wm.idPlace > 0) && (wm.idRoute > 0))
            Validator.complain("WanderMedia.idPlace and WanderMedia.idRoute are both greater than 0.");
        if (!Validator.isMediaType(wm.mediaType)) Validator.complain("WanderMedia.mediaType is null or unknown.");
        if ("image".equals(wm.mediaType)) {
            BufferedImage bi = Validator.readImage(wm.data);
            if (bi == null)
                Validator.complain("WanderMedia.data is not an image.");
            if ((bi.getHeight() < 128) || (bi.getWidth() < 128))
                Validator.complain("WanderMedia.data is a too small image. Must be in range 128x128 to 8192x8192.");
            if ((bi.getHeight() > 8192) || (bi.getWidth() > 8192))
                Validator.complain("WanderMedia.data is a too large image. Must be in range 128x128 to 8192x8192.");
        } else if ("video".equals(wm.mediaType)) {
            if (Validator.readVideo(wm.data) == null)
                Validator.complain("WanderMedia.data is not a video.");
        }
    }
    
    @Override
    public String toString() {
        return "WanderMedia{" + "idPlace=" + idPlace + ", idRoute=" + idRoute + ", idAuthor=" + idAuthor + ", data=" + data + ", time=" + time + ", mediaType=" + mediaType + '}';
    }
    
    
}
