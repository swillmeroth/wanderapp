package regionale.whs.wanderapp.wspojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import regionale.whs.wanderapp.wspojos.validation.Validator;

/**
 *
 * @author MartinM
 */
public class RegistrationData {
    @Expose @SerializedName("creds")
    public Credentials credentials;
    
    @Expose @SerializedName("nick")
    public String displayname;

    public RegistrationData() {
        this.credentials = new Credentials();
        displayname = "";
    }

    public RegistrationData(String email, String password, String displayname) {
        this.credentials = new Credentials();
        this.credentials.setEmail(email);
        this.credentials.setPassword(password);
        this.displayname = displayname;
    }

    public RegistrationData(Credentials credentials, String displayname) {
        this.credentials = credentials;
        this.displayname = displayname;
    }

    public Credentials getCredentials() {
        return credentials;
    }

    public void setCredentials(Credentials credentials) {
        this.credentials = credentials;
    }

    public String getDisplayname() {
        return displayname;
    }

    public void setDisplayname(String displayname) {
        this.displayname = displayname;
    }
    
    public static void validate(RegistrationData d) {
        if (d == null) Validator.complain("RegistrationData is null.");
        Credentials.validate(d.credentials);
        Validator.validate(d.displayname, "RegistrationData.displayname", 5, 255);
    }

    @Override
    public String toString() {
        return "RegistrationData{" + "credentials=" + credentials + ", displayname=" + displayname + '}';
    }
    
    
}
