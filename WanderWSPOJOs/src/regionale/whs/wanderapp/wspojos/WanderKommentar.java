package regionale.whs.wanderapp.wspojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.Date;
import regionale.whs.wanderapp.wspojos.validation.Validator;

/**
 *
 * @author MartinM
 */
public class WanderKommentar extends WanderObjekt {
    
    @Expose @SerializedName("creator")
    public String authorName;
    
    @Expose @SerializedName("txt")
    public String text;
    
    @Expose @SerializedName("time")
    public Date time;
    
    @Expose @SerializedName("idp")
    public int placeId;
    
    @Expose @SerializedName("idr")
    public int routeId;
    
    @Expose @SerializedName("ida")
    public int authorId;
    
    @Expose @SerializedName("score")
    public int rating;

    public int getRating() {
        return rating;
    }

    public WanderKommentar() {
        text = "";
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    /**
     * Sets the rating of this comment in a range from 1 (lowest) to 5 (highest), or 0 to abstain.
     * @param rating The rating this comment applies to the object it is commenting on
     */
    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public int getPlaceId() {
        return placeId;
    }

    public void setPlaceId(int placeId) {
        this.placeId = placeId;
    }

    public int getRouteId() {
        return routeId;
    }

    public void setRouteId(int routeId) {
        this.routeId = routeId;
    }

    public int getAuthorId() {
        return authorId;
    }

    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }
    
    public static void validate(WanderKommentar wk) {
        if (wk == null) Validator.complain("WanderKommentar is null.");
        if (wk.id < 0) Validator.complain("WanderKommentar.id is less than 0.");
        Validator.validate(wk.text, "WanderKommentar.text", 1, 65535);
        Validator.validate(wk.rating, "WanderKommentar.rating", 0, 5);
        if (wk.authorId <= 0) Validator.complain("WanderKommentar.authorId is 0 or less.");
        if (wk.placeId < 0) Validator.complain("WanderKommentar.idPlace is less than 0.");
        if (wk.routeId < 0) Validator.complain("WanderKommentar.idRoute is less than 0.");
        if ((wk.placeId == 0) && (wk.routeId == 0))
            Validator.complain("WanderKommentar.placeId and WanderKommentar.routeId are both 0.");
        if ((wk.placeId > 0) && (wk.routeId > 0))
            Validator.complain("WanderKommentar.placeId and WanderKommentar.routeId are both greater than 0.");
    }

    @Override
    public String toString() {
        return "WanderKommentar{" + "text=" + text + ", time=" + time + ", placeId=" + placeId + ", routeId=" + routeId + ", authorId=" + authorId + ", rating=" + rating + '}';
    }
    
    
    
}
