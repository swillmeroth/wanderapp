package regionale.whs.wanderapp.wspojos.exceptions;

/**
 *
 * @author MartinM
 */
public class ValidationException extends RuntimeException {
    public ValidationException(String message) {
        super(message);
    }
}
