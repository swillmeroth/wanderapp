package regionale.whs.wanderapp.wspojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.LinkedList;
import java.util.List;
import regionale.whs.wanderapp.wspojos.exceptions.ValidationException;
import regionale.whs.wanderapp.wspojos.validation.Validator;

/**
 *
 * @author MartinM
 */
public class WanderRoute extends WanderObjekt {

    @Expose @SerializedName("dfc")
    public int difficulty;
    
    @Expose @SerializedName("len")
    public int distance;
    
    @Expose @SerializedName("dur")
    public int duration;
    
    @Expose @SerializedName("equip")
    public String equipment;
    
    @Expose @SerializedName("idrps")
    public int startId;
    
    @Expose @SerializedName("idrpe")
    public int endId;
    
    @Expose @SerializedName("idr")
    public int idChanging;
    
    @Expose @SerializedName("ch")
    public boolean isChange;
    
    @Expose @SerializedName("pp")
    public boolean isProposal;
    
    @Expose @SerializedName("name")
    public String name;
    
    @Expose @SerializedName("rtype")
    public String routeType;
    
    @Expose @SerializedName("rpts")
    public List<GeoLocation> routePoints;
    
    @Expose @SerializedName("rpids")
    public List<Integer> routePlacesIds;
    
    @Expose @SerializedName("cc")
    public int commentCount;
    
    @Expose @SerializedName("mc")
    public int multimediaCount;
    
    @Expose @SerializedName("tc")
    public int todosCount;
    
    @Expose @SerializedName("score")
    public float rating;
    
    @Expose @SerializedName("descriptions")
    public List<WanderDescription> descriptions;
    
    public WanderRoute() {
        equipment = "";
        name = "";
        routeType = "path";
        routePoints = new LinkedList<>();
        routePlacesIds = new LinkedList<>();
        descriptions = new LinkedList<>();
    }

    public List<WanderDescription> getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(List<WanderDescription> descriptions) {
        this.descriptions = descriptions;
    }

    public List<Integer> getRoutePlacesIds() {
        return routePlacesIds;
    }

    public void setRoutePlacesIds(List<Integer> routePlacesIds) {
        this.routePlacesIds = routePlacesIds;
    }

    public int getStartId() {
        return startId;
    }

    public void setStartId(int startId) {
        this.startId = startId;
    }

    public int getEndId() {
        return endId;
    }

    public void setEndId(int endId) {
        this.endId = endId;
    }

    public int getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(int commentCount) {
        this.commentCount = commentCount;
    }

    public int getMultimediaCount() {
        return multimediaCount;
    }

    public void setMultimediaCount(int multimediaCount) {
        this.multimediaCount = multimediaCount;
    }

    public int getTodosCount() {
        return todosCount;
    }

    public void setTodosCount(int todosCount) {
        this.todosCount = todosCount;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public List<GeoLocation> getRoutePoints() {
        return routePoints;
    }

    public void setRoutePoints(List<GeoLocation> routePoints) {
        this.routePoints = routePoints;
    }

    public int getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(int difficulty) {
        this.difficulty = difficulty;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getEquipment() {
        return equipment;
    }

    public void setEquipment(String equipment) {
        this.equipment = equipment;
    }

    public int getIdChanging() {
        return idChanging;
    }

    public void setIdChanging(int idChanging) {
        this.idChanging = idChanging;
    }

    public boolean getIsChange() {
        return isChange;
    }

    public void setIsChange(boolean isChange) {
        this.isChange = isChange;
    }

    public boolean getIsProposal() {
        return isProposal;
    }

    public void setIsProposal(boolean isProposal) {
        this.isProposal = isProposal;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRouteType() {
        return routeType;
    }

    public void setRouteType(String routeType) {
        this.routeType = routeType;
    }
    
    public static void validate(WanderRoute r) {
        if (r == null) Validator.complain("WanderRoute is null.");
        if (r.id < 0) Validator.complain("WanderRoute.id is less than 0.");
        Validator.validate(r.difficulty, "WanderRoute.difficulty", 1, 10);
        Validator.validate(r.equipment, "WanderRoute.equipment", 0, 255);
        Validator.validate(r.name, "WanderRoute.name", 1, 255);
        if (r.routePlacesIds == null) Validator.complain("WanderRoute.routePlacesIds is null.");
        if (r.routePlacesIds.size() > 100) Validator.complain("WanderRoute.routePlacesIds has over 100 entries.");
        if (r.routePoints == null) Validator.complain("WanderRoute.routePoints is null.");
        if (!Validator.isRouteType(r.routeType))
            Validator.complain("WanderRoute.routeType is null or unknown. (" + r.routeType + ")");
        if (r.idChanging < 0) Validator.complain("WanderRoute.idChanging is less than 0.");
        if (r.descriptions == null) Validator.complain("WanderRoute.descriptions is null.");
        for (Integer i : r.routePlacesIds) {
            if (i == null) Validator.complain("WanderRoute.routePlacesIds contains at least one null reference.");
            if (r.routePlacesIds.indexOf(i) != r.routePlacesIds.lastIndexOf(i))
                Validator.complain("WanderRoute.routePlacesIds contains multiple occurences of the same Place.");
        }
        try {
            for (WanderDescription d : r.descriptions) {
                WanderDescription.validate(d);
                if (r.descriptions.indexOf(d) != r.descriptions.lastIndexOf(d))
                    Validator.complain("Contains multiple occurences of the same description.");
            }
        } catch (ValidationException ex) {
            Validator.complain("Error in WanderRoute.descriptions: " + ex.getMessage());
        }
        try {
            for (GeoLocation gl : r.routePoints) {
                GeoLocation.validate(gl);
            }
        } catch (ValidationException ex) {
            Validator.complain("Error in WanderRoute.routePoints: " + ex.getMessage());
        }
    }

    @Override
    public String toString() {
        return "WanderRoute{" + "difficulty=" + difficulty + ", distance=" + distance + ", duration=" + duration + ", equipment=" + equipment + ", startId=" + startId + ", endId=" + endId + ", idChanging=" + idChanging + ", isChange=" + isChange + ", isProposal=" + isProposal + ", name=" + name + ", routeType=" + routeType + ", routePoints=" + routePoints + ", routePlacesIds=" + routePlacesIds + ", commentCount=" + commentCount + ", multimediaCount=" + multimediaCount + ", todosCount=" + todosCount + ", rating=" + rating + ", descriptions=" + descriptions + '}';
    }
    
    
    
}
