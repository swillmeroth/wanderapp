package regionale.whs.wanderapp.wspojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import regionale.whs.wanderapp.wspojos.validation.Validator;

/**
 *
 * @author MartinM
 */
public class PasswordUpdate {
    
    @Expose @SerializedName("npw")
    public String newPassword;

    public PasswordUpdate() {
        newPassword = "";
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }
    
    public static void validate(PasswordUpdate pu) {
        if (pu == null) Validator.complain("PasswordUpdate is null.");
        Validator.validate(pu.newPassword, "PasswordUpdate.newPassword", 5, 255);
    }

    @Override
    public String toString() {
        return "PasswordUpdate{" + "newPassword=" + newPassword + '}';
    }

    
}
