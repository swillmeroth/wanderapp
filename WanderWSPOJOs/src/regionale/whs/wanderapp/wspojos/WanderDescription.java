package regionale.whs.wanderapp.wspojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.Objects;
import regionale.whs.wanderapp.wspojos.validation.Validator;

/**
 *
 * @author MartinM
 */
public class WanderDescription extends WanderObjekt {
    
    @Expose @SerializedName("txt")
    public String text;
    
    @Expose @SerializedName("lang")
    public String language;
    
    @Expose @SerializedName("int")
    public String interest;
    
    @Expose @SerializedName("idp")
    public int idPlace;
    
    @Expose @SerializedName("idr")
    public int idRoute;

    public WanderDescription() {
        text = "";
        language = "de";
        interest = "historisch";
    }
    
    public int getIdPlace() {
        return idPlace;
    }

    public void setIdPlace(int idPlace) {
        this.idPlace = idPlace;
    }

    public int getIdRoute() {
        return idRoute;
    }

    public void setIdRoute(int idRoute) {
        this.idRoute = idRoute;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getInterest() {
        return interest;
    }

    public void setInterest(String interest) {
        this.interest = interest;
    }
    
    public void unsetIdPlace() {
        this.idPlace = 0;
    }
    
    public void unsetIdRoute() {
        this.idRoute = 0;
    }
    
    public static void validate(WanderDescription wd) {
        if (wd == null) Validator.complain("WanderDescription is null.");
        if (wd.id < 0) Validator.complain("WanderDescription.id is less than 0.");
        Validator.validate(wd.text, "WanderDescription.text", 1, 65535);
        Validator.validate(wd.language, "WanderDescription.language", 2, 2);
        Validator.validate(wd.interest, "WanderDescription.interest", 0, 255);
        if (wd.idPlace < 0) Validator.complain("WanderDescription.idPlace is less than 0.");
        if (wd.idRoute < 0) Validator.complain("WanderDescription.idRoute is less than 0.");
        if ((wd.idPlace == 0) && (wd.idRoute == 0))
            Validator.complain("WanderDescription.idPlace and WanderDescription.idRoute are both 0.");
        if ((wd.idPlace > 0) && (wd.idRoute > 0))
            Validator.complain("WanderDescription.idPlace and WanderDescription.idRoute are both greater than 0.");
    }

    @Override
    public String toString() {
        return "WanderDescription{" + "text=" + text + ", language=" + language + ", interest=" + interest + ", idPlace=" + idPlace + ", idRoute=" + idRoute + '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final WanderDescription other = (WanderDescription) obj;
        if (!Objects.equals(this.language, other.language)) {
            return false;
        }
        if (!Objects.equals(this.interest, other.interest)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + Objects.hashCode(this.language);
        hash = 29 * hash + Objects.hashCode(this.interest);
        return hash;
    }
    
    
    
}
