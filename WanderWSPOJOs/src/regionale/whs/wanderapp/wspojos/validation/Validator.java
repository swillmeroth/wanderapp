/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regionale.whs.wanderapp.wspojos.validation;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import javax.imageio.ImageIO;
import regionale.whs.wanderapp.wspojos.exceptions.ValidationException;

/**
 *
 * @author Zyl
 */
public class Validator {
    
    public static void validate(String string, String stringName, int min, int max) {
        if (string == null)
            throw new ValidationException(stringName + " is null.");
        
        int stringLength = string.length();
        if ((stringLength < min) || (stringLength > max))
            throw new ValidationException(stringName + ".length must be in range [" + min + "," + max + "], "
                    + "but was " + stringLength + ".");
    }
    
    public static void validate(int value, String valueName, int min, int max) {
        if ((value < min) || (value > max))
            throw new ValidationException(valueName + " must be in range [" + min + "," + max + "], "
                    + "but was " + value + ".");
    }
    
    public static void validate(double value, String valueName, double min, double max) {
        if ((value < min) || (value > max))
            throw new ValidationException(valueName + " must be in range [" + min + "," + max + "], "
                    + "but was " + value + ".");
    }
    
    public static boolean isEmail(String email) {
        if (email == null) return false;
        int len = email.length();
        if (len < 5) return false;
        if (len > 255) return false;
        int atFound = -1;
        int periodFound = -2;
        for (int i = 0; i < len; i++) {
            char c = email.charAt(i);
            if (c == '@') {
                if (atFound != -1) return false; // No two @s
                if (i == 0) return false; // No @ at pos 0
                atFound = i;
            } else if (c == '.') {
                if (atFound != -1) {
                    if (atFound == (i - 1)) return false; // No period directly after @
                    if (periodFound == (i - 1)) return false; // No consecutive periods after @
                    periodFound = i;
                }
            } else if (!Character.isLetterOrDigit(c)) return false; // No garbage
        }
        return (atFound != -1) && (periodFound > (atFound + 1)) && (periodFound != (len - 1)); // Do not end in period
    }
    
    public static boolean isRouteType(String routeType) {
        if ("path".equals(routeType)
                || "loop".equals(routeType)) {
            return true;
        }
        return false;
    }
    
    public static boolean isPOIType(String poiType) {
        if ("abbey".equals(poiType)
                || "chapel".equals(poiType)
                || "church".equals(poiType)
                || "cross".equals(poiType)
                || "graveyard".equals(poiType)
                || "jewish_graveyard".equals(poiType)
                || "monument".equals(poiType)
                || "statue".equals(poiType)) {
            return true;
        }
        return false;
    }
    
    public static boolean isInterest(String interest) {
        if ("historisch".equals(interest)
                || "religiös".equals(interest)
                || "geographisch".equals(interest)
                || "kulturell".equals(interest)) {
            return true;
        }
        return false;
    }
    
    public static boolean isLanguage(String language) {
        if ("en".equals(language)
                || "de".equals(language)) {
            return true;
        }
        return false;
    }
    
    public static boolean isWalkingSpeed(String speed) {
        if ("langsam".equals(speed)
                || "normal".equals(speed) 
                || "schnell".equals(speed)) {
            return true;
        }
        return false;
    }

    public static boolean isMediaType(String mediaType) {
        if ("image".equals(mediaType)
                || "video".equals(mediaType)) {
            return true;
        }
        return false;
    }

    public static BufferedImage readImage(byte[] data) {
        try {
            if (data == null) return null;
            if (data.length == 0) return null;
            InputStream is = new InputStream() {
                private int pos = 0;
                private final byte[] dat = data;
                private final int len = dat.length;
                
                @Override
                public int read() throws IOException {
                    if (pos == len) return -1;
                    byte read = dat[pos];
                    pos++;
                    return read;
                }
            };
            BufferedImage bi = ImageIO.read(is);
            if (bi == null) return null;
            if (bi.getWidth() <= 0) return null;
            if (bi.getHeight() <= 0) return null;
            return bi;
        } catch (IOException ex) {
            return null;
        }
    }

    public static Object readVideo(byte[] data) {
        return null;
        // TODO: Write logic.
    }
    
    public static void complain(String message) {
        throw new ValidationException(message);
    }
}
