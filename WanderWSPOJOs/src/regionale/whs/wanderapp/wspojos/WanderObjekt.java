package regionale.whs.wanderapp.wspojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import regionale.whs.wanderapp.wspojos.exceptions.ValidationException;

/**
 *
 * @author MartinM
 */
public class WanderObjekt implements Serializable {
    
    @Expose(serialize = false, deserialize = false)
    public static final transient String MEDIA_TYPE = "application/json;charset=utf-8";
    
    @Expose @SerializedName("id")
    public int id;
    
    public void setId(int id) {
        this.id = id;
    }
    
    public int getId() {
        return this.id;
    }
    
    public WanderObjekt() {
        id = 0;
    }
    
    /**
     * Marks this WanderObjekt as a new object, meaning it should result
     * in new data being inserted into the database upon transmission.
     * This state is encoded by setting the id to -1, and is the default
     * when constructing the object with the default constructor.
     */
    public void markAsNewObject() {
        this.id = 0;
    }
    
    /**
     * Returns true if this WanderObjekt is marked as a new object. This is
     * the case if its id is -1 (or, for whatever reason, less than -1).
     * @return True, if this is to be a new persistent object.
     */
    public boolean isNewObject() {
        return this.id <= 0;
    }
    
    /**
     * Checks whether the id for this object is valid, meaning it is greater
     * than 0.
     * @return True, if id greater than 0.
     */
    public boolean hasValidId() {
        return this.id > 0;
    }
    
    public static void validate(WanderObjekt wo) {
        if (wo == null) throw new ValidationException("WanderObjekt is null.");
        if (wo.id < 0) throw new ValidationException("WanderObjekt has id < 0. Id must be 0 (meaning new object) or greater than 0.");
    }

    @Override
    public String toString() {
        return "WanderObjekt{" + "id=" + id + '}';
    }
    
    
}
