package regionale.whs.wanderapp.wspojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import regionale.whs.wanderapp.wspojos.exceptions.ValidationException;
import regionale.whs.wanderapp.wspojos.validation.Validator;

/**
 *
 * @author MartinM
 */
public class GeoLocation {
    @Expose @SerializedName("lt")
    public double latitude;
    
    @Expose @SerializedName("ln")
    public double longitude;
    
    @Expose @SerializedName("al")
    public int altitude;

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = Math.min(90.0, Math.max(-90.0, latitude));
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = Math.min(180.0, Math.max(-180.0, longitude));
    }

    public int getAltitude() {
        return altitude;
    }

    public void setAltitude(int altitude) {
        this.altitude = Math.min(10000, Math.max(-10000, altitude));
    }
    
    public GeoLocation() {
        
    }
    
    public GeoLocation(GeoLocation gl) {
        latitude = gl.latitude;
        longitude = gl.longitude;
        altitude = gl.altitude;
    }
    
    public GeoLocation(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.altitude = 0;
    }
    
    public GeoLocation(double latitude, double longitude, int altitude) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.altitude = altitude;
    }
    
    /**
    * Calculates the distance in meters between two lat/long points
    * using the haversine formula.
    */
    public static double getDistance(
            double lat1, double lng1, double lat2, double lng2) {
        int r = 6371000; // average radius of the earth in m
        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lng2 - lng1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
           Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) 
          * Math.sin(dLon / 2) * Math.sin(dLon / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double d = r * c;
        return d;
    }
    
    /**
    * Calculates the distance in meters between two lat/long points
    * using the haversine formula.
    */
    public static double getDistance(GeoLocation gl1, GeoLocation gl2) {
        return getDistance(gl1.latitude, gl1.longitude, gl2.latitude, gl2.longitude);
    }
    
    public static double getDegrees(double distance) {
        int r = 6371000; // average radius of the earth in m
        return (distance) * 360 / (2 * Math.PI * r);
    }
    
    public static GeoLocation between(GeoLocation gl1, GeoLocation gl2) {
        GeoLocation between = new GeoLocation(
                (gl1.latitude + gl2.latitude) / 2,
                (gl1.longitude + gl2.longitude) / 2,
                (gl1.altitude + gl2.altitude) / 2
        );
        return between;
    }
    
    public static void validate(GeoLocation gl) {
        if (gl == null) Validator.complain("GeoLocation is null.");
        Validator.validate(gl.latitude, "GeoLocation.latitude", -90, 90);
        Validator.validate(gl.longitude, "GeoLocation.longitude", -180, 180);
        Validator.validate(gl.altitude, "GeoLocation.altitude", -10000, 10000);
    }

    @Override
    public String toString() {
        return "GeoLocation{" + "latitude=" + latitude + ", longitude=" + longitude + ", altitude=" + altitude + '}';
    }
    
    
}
