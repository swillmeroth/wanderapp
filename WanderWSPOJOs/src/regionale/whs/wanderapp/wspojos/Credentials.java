package regionale.whs.wanderapp.wspojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import regionale.whs.wanderapp.wspojos.exceptions.ValidationException;
import regionale.whs.wanderapp.wspojos.validation.Validator;


/**
 *
 * @author MartinM
 */
public class Credentials {
    @Expose @SerializedName("email")
    public String email;
    
    @Expose @SerializedName("pw")
    public String password;
    
    public Credentials() {
        email = "";
        password = "";
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    /**
     * Set the password part of this Credentials object.
     * @param password The password as it will be sent to the server.
     */
    public void setPassword(String password) {
        this.password = password;
    }
    
    public static void validate(Credentials c) {
        if (c == null) Validator.complain("Credentials is null.");
        if (!Validator.isEmail(c.email))
            Validator.complain("Credentials.email is null or not valid.");
        Validator.validate(c.password, "Credentials.password", 5, 255);
    }

    @Override
    public String toString() {
        return "Credentials{" + "email=" + email + ", password=" + password + '}';
    }
    
    
}
