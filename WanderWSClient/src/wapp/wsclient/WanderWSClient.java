package wapp.wsclient;

import java.util.List;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import wapp.wsclient.exceptions.BadRequestException;
import wapp.wsclient.exceptions.ConflictException;
import wapp.wsclient.exceptions.InsufficientPermissionsException;
import wapp.wsclient.exceptions.InternalServerErrorException;
import wapp.wsclient.exceptions.ResourceNotFoundException;
import wapp.wsclient.exceptions.UnknownErrorException;
import wapp.wsclient.exceptions.WSClientException;

/**
 * A class representing an implementation for a RESTful WanderApp client.
 * @author MartinM
 */
public class WanderWSClient {

    private Client client;
    
    /**
     * The default address of the RESTful web service.
     */
    public static final String BASE_URI = "http://localhost:8084/WanderWIKIa/webresources/";
    
    private String useURI;
    
    private Credentials authCredentials;
    
    /**
     * Creates a new WanderWSClient using BASE_URI as the address of the web
     * service.
     */
    public WanderWSClient() {
        useURI = BASE_URI;
        client = javax.ws.rs.client.ClientBuilder.newClient();
        resetAuthenticationData();
    }
    
    /**
     * Creates a new WanderWSClient using a custom address for the web service.
     * @param customWSURI URI pointing to the RESTful WanderApp web service.
     */
    public WanderWSClient(String customWSURI) {
        useURI = customWSURI;
        client = javax.ws.rs.client.ClientBuilder.newClient();
        resetAuthenticationData();
    }
    
    /**
     * Purges any authentication data stored within this WanderWSClient object.
     * This disables it from performing successful requests to resources which
     * require authorization.
     */
    public final void resetAuthenticationData() {
        authCredentials = null;
    }
    
    /**
     * Sends a request to the server which urges it to create a user account
     * using the provided credentials and displayname. After a successful
     * call to this function, a user account with the given email and
     * displayname will exist. If it already existed prior to calling this
     * function, it is by no means possible to tell whether that was the case.
     * @param email E-Mail address to use
     * @param password Password to use
     * @param displayname Unique screen name to use
     * @throws ConflictException If a user with the given displayname already exists.
     */
    public void register(String email, String password, String displayname) {
        post(new RegistrationData(email, password, displayname), "users");
    }

    /**
     * Sets the authentication data that this client sends along with its
     * requests. This should us the same information as was given to the
     * register()-method sometime before.
     * @param email E-Mail address
     * @param password Matching password
     */
    public void setAuthenticationData(String email, String password) {
        authCredentials = new Credentials(email, password);
    }

    /**
     * Requests that the server returns a WanderUser object matching the
     * credentials supplied via setAuthenticationData. Using either a non-
     * existent email or an invalid password results in failure.
     * @return WanderUser object that matches the provided credentials
     * @throws InsufficientPermissionsException If the password was wrong
     * or no account with the given email address exists.
     * @throws BadRequestException If the supplied credentials were partially
     * null, zero in length or 255+ characters long.
     */
    public WanderUser testAuthentication() {
        return post(authCredentials, "users/authentication").readEntity(WanderUser.class);
    }

    /**
     * Returns the WanderPOI matching the given id.
     * @param poiId An id greater than 0.
     * @return WanderPOI corresponding to the given id.
     * @throws ResourceNotFoundException If no POI by the given Id could be found.
     * @throws BadRequestException If the id was not greater than 0.
     */
    public WanderPOI getPOI(int poiId) {
        return get("places/" + poiId).readEntity(WanderPOI.class);
    }
    
    /**
     * Adds or updates the given WanderPOI, which is identified by its id.
     * @param poi WanderPOI object to add or update.
     * @return The Id of the added or updated object.
     */
    public int setPOI(WanderPOI poi) {
        if (poi.isNewObject()) {
            return post(poi, "places").readEntity(Integer.class);
        } else {
            return post(poi, "places/" + poi.getId()).readEntity(Integer.class);
        }
    }
    
    /**
     * Deletes the POI with the given id.
     * @param id Id of the POI to delete.
     * @throws BadRequestException If the id was not greater than 0.
     * @throws ResourceNotFoundException If no POI by the given id exists.
     */
    public void deletePOI(int id) {
        delete("places/" + id);
    }
    
    public WanderUser getUser(int userId) {
        return get("users/" + userId).readEntity(WanderUser.class);
    }

    public int setUser(WanderUser user) {
        return post(user, "users/" + user.getId()).readEntity(Integer.class);
    }

    public void deleteUser(int userId) {
        delete("users/" + userId);
    }

//    public WanderMedia getMedia(int mediaId) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//
//    public int setPOIMedia(int poiId, WanderMedia media) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//
//    public int setRouteMedia(int routeId, WanderMedia media) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//
//    public void deleteMedia(int mediaId) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }

    public WanderRoute getRoute(int routeId) {
        return get("routes/" + routeId).readEntity(WanderRoute.class);
    }

    public int setRoute(WanderRoute route) {
        return post(route, "routes").readEntity(Integer.class);
    }

    public void deletetRoute(int routeId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public int setDescription(WanderDescription description) {
        if (!description.isNewObject())
            if (description.hasValidId())
                return post(description, "descriptions/" + description.getId()).readEntity(Integer.class);
            else throw new RuntimeException("Called setDescription() to update a WanderDescription object but id was not valid.");
        else if (description.getIdPlace()> 0)
            return post(description, "places/" + description.getIdPlace()+ "/descriptions").readEntity(Integer.class);
        else if (description.getIdRoute()> 0)
            return post(description, "routes/" + description.getIdRoute()+ "/descriptions").readEntity(Integer.class);
        throw new RuntimeException("Called setDescription() with new WanderDescription object but neither valid place id nor valid route id.");
    }
    
    public WanderDescription getDescription(int descriptionId) {
        return get("descriptions/" + descriptionId).readEntity(WanderDescription.class);
    }
    
    public WanderDescription getPOIDescription(int poiId, String forInterest, String inLanguage) {
        return get("places/" + poiId + "/descriptions/" + forInterest + "-interest-in-" + inLanguage + "-language").readEntity(WanderDescription.class);
    }
    
    public WanderDescription getRouteDescription(int routeId, String forInterest, String inLanguage) {
        return get("routes/" + routeId + "/descriptions/" + forInterest + "-interest-in-" + inLanguage + "-language").readEntity(WanderDescription.class);
    }
    
    public void deleteDescription(int descriptionId) {
        delete("descriptions/" + descriptionId);
    }

    public WanderKommentar getKommentar(int commentId) {
        return get("comments/" + commentId).readEntity(WanderKommentar.class);
    }

    public WanderKommentar getPOIKommentar(int poiId, int kommentarNummer) {
        return get("places/" + poiId + "/comments/" + kommentarNummer).readEntity(WanderKommentar.class);
    }

    public List<WanderKommentar> getPOIKommentare(int poiId, int vonNummer, int bisNummer) {
        return get("places/" + poiId + "/comments/" + vonNummer + "-" + bisNummer).readEntity(new GenericType<List<WanderKommentar>>() {});
    }

    public WanderKommentar getRouteKommentar(int routeId, int kommentarNummer) {
        return get("routes/" + routeId + "/comments/" + kommentarNummer).readEntity(WanderKommentar.class);
    }

    public List<WanderKommentar> getRouteKommentare(int routeId, int vonNummer, int bisNummer) {
        return get("routes/" + routeId + "/comments/" + vonNummer + "-" + bisNummer).readEntity(new GenericType<List<WanderKommentar>>() {});
    }
    
    public int setKommentar(WanderKommentar comment) {
        if (!comment.isNewObject())
            if (comment.hasValidId())
                return post(comment, "comments/" + comment.getId()).readEntity(Integer.class);
            else throw new RuntimeException("Called setKommentar() to update a WanderKommentar object but id was not valid.");
        else if (comment.getPlaceId() > 0)
            return post(comment, "places/" + comment.getPlaceId() + "/comments").readEntity(Integer.class);
        else if (comment.getRouteId() > 0)
            return post(comment, "routes/" + comment.getRouteId() + "/comments").readEntity(Integer.class);
        throw new RuntimeException("Called setKommentar() with new WanderKommentar object but neither valid place id nor valid route id.");
    }

    public void deleteKommentar(int commentId) {
        delete("comments/" + commentId);
    }

    public WanderMedia getPOIMedia(int poiId, int medienNummer) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public WanderMedia[] getPOIMediaThumbs(int poiId, int vonNummer, int bisNummer) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public WanderMedia getPOIMediaThumb(int poiId, int thumbNummer) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public WanderMedia getRouteMedia(int poiId, int medienNummer) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public WanderMedia[] getRouteMediaThumbs(int poiId, int vonNummer, int bisNummer) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public WanderMedia getRouteMediaThumb(int poiId, int thumbNummer) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Kartenausschnitt getKartenausschnitt(GeoLocation min, GeoLocation max) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    // ------------------------------------------------------------------------
    // 
    // HELPER FUNCTIONS
    //
    
    public Response get(String atPath) {
        WebTarget wt = client.target(useURI).path(atPath);
        Response r = wt.request().accept(WanderObjekt.MEDIA_TYPE).get();
        if (isAccepted(r)) {
            return r;
        }
        throw createAppropriateException(r);
    }
    
    public Response post(Object object, String atPath) {
        WebTarget wt = client.target(useURI).path(atPath);
        Response r = wt.request().post(Entity.entity(object, WanderObjekt.MEDIA_TYPE));
        if (isAccepted(r)) {
            return r;
        }
        throw createAppropriateException(r);
    }
    
    public Response delete(String atPath) {
        WebTarget wt = client.target(useURI).path(atPath);
        Response r = wt.request().delete();
        if (isAccepted(r)) {
            return r;
        }
        throw createAppropriateException(r);
    }
    
    private RuntimeException createAppropriateException(Response r) {
        System.err.println("Bad response: " + r.getStatus() + ": " + r.getStatusInfo().getReasonPhrase());
        if (r.getStatus() == Status.FORBIDDEN.getStatusCode()) {
            return new InsufficientPermissionsException(r);
        } else if (r.getStatus() == Status.BAD_REQUEST.getStatusCode()) {
            return new BadRequestException(r);
        } else if (r.getStatus() == Status.CONFLICT.getStatusCode()) {
            return new ConflictException(r);
        } else if (r.getStatus() == Status.NOT_FOUND.getStatusCode()) {
            return new ResourceNotFoundException(r);
        } else if (r.getStatus() == Status.INTERNAL_SERVER_ERROR.getStatusCode()) {
            return new InternalServerErrorException(r);
        } else {
            return new UnknownErrorException(r);
        }
    }
    
    private boolean isAccepted(Response r) {
        int responseCode = r.getStatus();
        return (responseCode >= 200) && (responseCode < 300);
    }

}
