package wapp.wsclient;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author MartinM
 */
@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlRootElement
public class WanderRoute extends WanderObjekt {

    private int difficulty;
    private int distance;
    private int duration;
    private String equipment;
    private int startId;
    private int endId;
    private int idChanging;
    private boolean isChange;
    private boolean isProposal;
    private String name;
    private String routeType;
    private List<GeoLocation> routePoints;
    private List<Integer> routePlacesIds;
    private int commentCount;
    private int multimediaCount;
    private int todosCount;
    private float rating;

    public List<Integer> getRoutePlacesIds() {
        return routePlacesIds;
    }

    public void setRoutePlacesIds(List<Integer> routePlacesIds) {
        this.routePlacesIds = routePlacesIds;
    }

    public int getStartId() {
        return startId;
    }

    public void setStartId(int startId) {
        this.startId = startId;
    }

    public int getEndId() {
        return endId;
    }

    public void setEndId(int endId) {
        this.endId = endId;
    }

    public int getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(int commentCount) {
        this.commentCount = commentCount;
    }

    public int getMultimediaCount() {
        return multimediaCount;
    }

    public void setMultimediaCount(int multimediaCount) {
        this.multimediaCount = multimediaCount;
    }

    public int getTodosCount() {
        return todosCount;
    }

    public void setTodosCount(int todosCount) {
        this.todosCount = todosCount;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public List<GeoLocation> getRoutePoints() {
        return routePoints;
    }

    public void setRoutePoints(List<GeoLocation> routePoints) {
        this.routePoints = routePoints;
    }
    
    public WanderRoute() {
    }

    public int getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(int difficulty) {
        this.difficulty = difficulty;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getEquipment() {
        return equipment;
    }

    public void setEquipment(String equipment) {
        this.equipment = equipment;
    }

    public int getIdChanging() {
        return idChanging;
    }

    public void setIdChanging(int idChanging) {
        this.idChanging = idChanging;
    }

    public boolean getIsChange() {
        return isChange;
    }

    public void setIsChange(boolean isChange) {
        this.isChange = isChange;
    }

    public boolean getIsProposal() {
        return isProposal;
    }

    public void setIsProposal(boolean isProposal) {
        this.isProposal = isProposal;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRouteType() {
        return routeType;
    }

    public void setRouteType(String routeType) {
        this.routeType = routeType;
    }
    
    public static boolean validate(WanderRoute r) {
        return (r != null)
                && (r.routePoints != null) && (r.routePoints.size() >= 2) && (r.routePoints.size() <= 100000);
    }
    
    
    
}
