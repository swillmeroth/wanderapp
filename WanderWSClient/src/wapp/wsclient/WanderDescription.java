package wapp.wsclient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author MartinM
 */
@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlRootElement
public class WanderDescription extends WanderObjekt {
    
    private String text;
    private String language;
    private String interest;
    private int idPlace;
    private int idRoute;

    public int getIdPlace() {
        return idPlace;
    }

    public void setIdPlace(int idPlace) {
        this.idPlace = idPlace;
    }

    public int getIdRoute() {
        return idRoute;
    }

    public void setIdRoute(int idRoute) {
        this.idRoute = idRoute;
    }
    
    public WanderDescription() {
        
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getInterest() {
        return interest;
    }

    public void setInterest(String interest) {
        this.interest = interest;
    }
    
    public void unsetIdPlace() {
        this.idPlace = 0;
    }
    
    public void unsetIdRoute() {
        this.idRoute = 0;
    }
    
    public static boolean validate(WanderDescription wd) {
        return (wd != null)
                && (wd.text != null) && (wd.text.length() > 0) && (wd.text.length() < 65535)
                && (wd.language != null) && (wd.language.length() == 3)
                && (wd.interest != null) && (wd.interest.length() > 0) && (wd.interest.length() < 255)
                && ((wd.idPlace > 0) ^ (wd.idRoute > 0))
                && ((wd.idPlace == 0) ^ (wd.idRoute == 0));
    }
    
}
