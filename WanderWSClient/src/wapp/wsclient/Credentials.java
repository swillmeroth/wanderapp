package wapp.wsclient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author MartinM
 */
@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlRootElement
public class Credentials {
    private String email;
    private String password;
    
    public Credentials() {
        
    }
    
    public Credentials(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    /**
     * Set the password part of this Credentials object.
     * @param password The password as it will be sent to the server.
     */
    public void setPassword(String password) {
        this.password = password;
    }
    
    public static boolean validate(Credentials c) {
        return (c != null)
                && (c.email != null) && (c.email.length() >= 5) && (c.email.length() < 255)
                && (c.password != null) && (c.password.length() >= 6) && (c.password.length() < 255);
    }
    
}
