package wapp.wsclient.exceptions;

import javax.ws.rs.core.Response;

/**
 * Exception to be thrown if permissions were insufficient for the request operation.
 * @author MartinM
 */
public class InsufficientPermissionsException extends WSClientException {

    public InsufficientPermissionsException(Response faultyResponse) {
        super(faultyResponse);
    }
    
}
