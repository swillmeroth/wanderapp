package wapp.wsclient.exceptions;

import javax.ws.rs.core.Response;

/**
 * Thrown when the server responds with a NOT_FOUND HTTP response code.
 * This is the case if your request required access to a resource which the
 * server could not find. This can happen if you try to get, set or delete by an
 * id for which there is no match in the database, or if you tried to work with
 * a sub resource, e.g. a comment on a route, but the containing resource was
 * not found.
 * @author MartinM
 */
public class ResourceNotFoundException extends WSClientException {

    public ResourceNotFoundException(Response faultyResponse) {
        super(faultyResponse);
    }
    
}
