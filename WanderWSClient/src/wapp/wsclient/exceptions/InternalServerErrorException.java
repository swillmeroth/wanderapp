package wapp.wsclient.exceptions;

import javax.ws.rs.core.Response;

/**
 * Exceptions to be thrown if the server reported an internal server error.
 * @author MartinM
 */
public class InternalServerErrorException extends WSClientException {

    public InternalServerErrorException(Response faultyResponse) {
        super(faultyResponse);
    }
    
}
