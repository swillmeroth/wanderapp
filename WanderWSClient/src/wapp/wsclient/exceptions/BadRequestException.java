package wapp.wsclient.exceptions;

import javax.ws.rs.core.Response;

/**
 * Exception to be thrown if a request was malformed in some way, e.g.
 * requesting a resource for a negative id or posting a resource with
 * required fields being null or empty strings.
 * @author MartinM
 */
public class BadRequestException extends WSClientException {

    public BadRequestException(Response faultyResponse) {
        super(faultyResponse);
    }
    
}
