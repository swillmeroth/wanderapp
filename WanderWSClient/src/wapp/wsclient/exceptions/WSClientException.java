package wapp.wsclient.exceptions;

import javax.ws.rs.core.Response;

/**
 * Superclass for exceptions thrown by the WanderWSClient.
 * @author MartinM
 */
public class WSClientException extends RuntimeException {
    private final Response faultyResponse;

    /**
     * Construct a WSClientException with a given Response object as its cause.
     * @param faultyResponse The erroneous Response object.
     */
    public WSClientException(Response faultyResponse) {
        this.faultyResponse = faultyResponse;
    }
    
    @Override
    public String getMessage() {
        return "Code " + faultyResponse.getStatus() + ": " + faultyResponse.getStatusInfo().getReasonPhrase();
    }

    /**
     * Returns the Response object that caused the exception.
     * @return The Response object that caused the exception.
     */
    public Response getFaultyResponse() {
        return faultyResponse;
    }
    
    /**
     * Returns true if the faulty Response has response code FORBIDDEN.
     * @return Whether the faulty Response has response code FORBIDDEN.
     */
    public boolean insufficientPermissions() {
        return faultyResponse.getStatusInfo() == Response.Status.FORBIDDEN;
    }
    
    /**
     * Returns true if the faulty Response has response code BAD_REQUEST.
     * @return Whether the faulty Response has response code BAD_REQUEST.
     */
    public boolean badRequest() {
        return faultyResponse.getStatusInfo() == Response.Status.BAD_REQUEST;
    }
    
    /**
     * Returns true if the faulty Response has response code INTERNAL_SERVER_ERROR.
     * @return Whether the faulty Response has response code INTERNAL_SERVER_ERROR.
     */
    public boolean internalError() {
        return faultyResponse.getStatusInfo() == Response.Status.INTERNAL_SERVER_ERROR;
    }
    
    /**
     * Returns true if the faulty Response has response code NOT_FOUND.
     * @return Whether the faulty Response has response code NOT_FOUND.
     */
    public boolean unknownResource() {
        return faultyResponse.getStatusInfo() == Response.Status.NOT_FOUND;
    }
    
    /**
     * Returns true if the faulty Response has response code CONFLICT.
     * @return Whether the faulty Response has response code CONFLICT.
     */
    public boolean conflict() {
        return faultyResponse.getStatusInfo() == Response.Status.CONFLICT;
    }
    
}
