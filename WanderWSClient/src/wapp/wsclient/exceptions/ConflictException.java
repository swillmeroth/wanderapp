package wapp.wsclient.exceptions;

import javax.ws.rs.core.Response;

/**
 * 
 * @author MartinM
 */
public class ConflictException extends WSClientException {

    public ConflictException(Response faultyResponse) {
        super(faultyResponse);
    }
    
}
