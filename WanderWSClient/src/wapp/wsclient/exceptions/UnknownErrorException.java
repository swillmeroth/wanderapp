package wapp.wsclient.exceptions;

import javax.ws.rs.core.Response;

/**
 * Exception to be thrown if the client cannot identify the root of a problem
 * from the HTTP Response. This can happen if an error occurred within low-level
 * JAXRS logic on the server-side.
 * @author MartinM
 */
public class UnknownErrorException extends WSClientException {

    public UnknownErrorException(Response faultyResponse) {
        super(faultyResponse);
    }
    
}
