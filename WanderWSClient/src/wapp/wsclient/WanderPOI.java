package wapp.wsclient;

import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author MartinM
 */
@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlRootElement
public class WanderPOI extends WanderObjekt {
    
    private String city;
    private Date dateAdded;
    private int idChanging; // id of the place that this is a change to
    private boolean isChange; // whether this is a change to an existing place
    private boolean isProposal; // whether this is a change that is a proposal, i.e. not (yet) accepted
    private GeoLocation koordinaten;
    private String name;
    private String openTimes;
    private String postalCode;
    private int size;
    private String street;
    private String poiType;
    private int commentCount;
    private int multimediaCount;
    private int todosCount;
    private float rating;

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public int getMultimediaCount() {
        return multimediaCount;
    }

    public void setMultimediaCount(int multimediaCount) {
        this.multimediaCount = multimediaCount;
    }

    public int getTodosCount() {
        return todosCount;
    }

    public void setTodosCount(int todosCount) {
        this.todosCount = todosCount;
    }

    public int getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(int commentCount) {
        this.commentCount = commentCount;
    }
    
    public WanderPOI() {
        koordinaten = new GeoLocation();
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Date getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(Date dateAdded) {
        this.dateAdded = dateAdded;
    }

    public int getIdChanging() {
        return idChanging;
    }

    public void setIdChanging(int idChanging) {
        this.idChanging = idChanging;
    }

    public boolean getIsChange() {
        return isChange;
    }

    public void setIsChange(boolean isChange) {
        this.isChange = isChange;
    }

    public boolean getIsProposal() {
        return isProposal;
    }

    public void setIsProposal(boolean isProposal) {
        this.isProposal = isProposal;
    }

    public GeoLocation getKoordinaten() {
        return koordinaten;
    }

    public void setKoordinaten(GeoLocation koordinaten) {
        this.koordinaten = koordinaten;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOpenTimes() {
        return openTimes;
    }

    public void setOpenTimes(String openTimes) {
        this.openTimes = openTimes;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getPoiType() {
        return poiType;
    }

    public void setPoiType(String poiType) {
        this.poiType = poiType;
    }

    public static boolean validate(WanderPOI poi) {
        return (poi != null)
                && (poi.city != null) && (poi.city.length() > 0) && (poi.city.length() < 255)
                && (poi.name != null) && (poi.name.length() > 0) && (poi.name.length() < 255)
                && (poi.street != null) && (poi.street.length() > 0) && (poi.street.length() < 255)
                && (poi.postalCode != null) && (poi.postalCode.length() > 0) && (poi.postalCode.length() < 255)
                && (poi.poiType != null) && (poi.poiType.length() > 0) && (poi.poiType.length() < 255)
                && (poi.openTimes != null) && (poi.openTimes.length() < 255)
                && (poi.idChanging >= 0)
                && (poi.size >= 0)
                && GeoLocation.validate(poi.koordinaten);
    }
    
    
}
