package wapp.wsclient;

import javax.xml.bind.annotation.*;

/**
 *
 * @author MartinM
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
public class GeoLocation {
    @XmlElement(name = "lat")
    private double latitude;
    
    @XmlElement(name = "long")
    private double longitude;
    
    @XmlElement(name = "alt")
    private int altitude;

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = Math.min(90.0, Math.max(-90.0, latitude));
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = Math.min(180.0, Math.max(-180.0, longitude));
    }

    public int getAltitude() {
        return altitude;
    }

    public void setAltitude(int altitude) {
        this.altitude = Math.min(10000, Math.max(-10000, altitude));
    }
    
    public GeoLocation() {
        
    }
    
    public GeoLocation(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.altitude = 0;
    }
    
    public GeoLocation(double latitude, double longitude, int altitude) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.altitude = altitude;
    }
    
    public static boolean validate(GeoLocation gl) {
        return (gl != null)
                && (gl.altitude > -10000) && (gl.altitude < 10000)
                && (gl.latitude >= -90.0) && (gl.latitude <= 90.0)
                && (gl.longitude >= -180.0) && (gl.longitude <= 180.0);
    }
}
