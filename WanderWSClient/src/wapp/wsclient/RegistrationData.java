package wapp.wsclient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author MartinM
 */
@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlRootElement
public class RegistrationData {
    private Credentials credentials;
    private String displayname;

    public RegistrationData() {
    }

    public RegistrationData(String email, String password, String displayname) {
        this.credentials = new Credentials(email, password);
        this.displayname = displayname;
    }

    public RegistrationData(Credentials credentials, String displayname) {
        this.credentials = credentials;
        this.displayname = displayname;
    }

    public Credentials getCredentials() {
        return credentials;
    }

    public void setCredentials(Credentials credentials) {
        this.credentials = credentials;
    }

    public String getDisplayname() {
        return displayname;
    }

    public void setDisplayname(String displayname) {
        this.displayname = displayname;
    }
    
    public static boolean validate(RegistrationData d) {
        return (d != null)
                && Credentials.validate(d.credentials)
                && (d.displayname != null) && (d.displayname.length() > 0) && (d.displayname.length() < 255);
    }
    
}
