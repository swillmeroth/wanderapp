package wapp.wsclient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author MartinM
 */
@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlRootElement
public class WanderUser extends WanderObjekt {

    private boolean useBigFont;
    private String email;
    private String interest;
    private String language;
    private String name;
//    private String passwordHash;
    private boolean isVerified;
    private String walkingSpeed;
    private boolean isWheelchairUser;
    private int commentCount;
    private int multimediaCount;
    private int userLevel;

    public int getUserLevel() {
        return userLevel;
    }

    public void setUserLevel(int userLevel) {
        this.userLevel = userLevel;
    }

    public int getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(int commentCount) {
        this.commentCount = commentCount;
    }

    public int getMultimediaCount() {
        return multimediaCount;
    }

    public void setMultimediaCount(int multimediaCount) {
        this.multimediaCount = multimediaCount;
    }
    
    public WanderUser() {
    }

    public boolean getUseBigFont() {
        return useBigFont;
    }

    public void setUseBigFont(boolean useBigFont) {
        this.useBigFont = useBigFont;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getInterest() {
        return interest;
    }

    public void setInterest(String interest) {
        this.interest = interest;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

//    public String getPasswordHash() {
//        return passwordHash;
//    }
//
//    public void setPasswordHash(String passwordHash) {
//        this.passwordHash = passwordHash;
//    }

    public boolean getIsVerified() {
        return isVerified;
    }

    public void setIsVerified(boolean isVerified) {
        this.isVerified = isVerified;
    }

    public String getWalkingSpeed() {
        return walkingSpeed;
    }

    public void setWalkingSpeed(String walkingSpeed) {
        this.walkingSpeed = walkingSpeed;
    }

    public boolean getIsWheelchairUser() {
        return isWheelchairUser;
    }

    public void setIsWheelchairUser(boolean isWheelchairUser) {
        this.isWheelchairUser = isWheelchairUser;
    }
    
    
    
}
