package wapp.wsclient;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author MartinM
 */
@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlRootElement
public class Kartenausschnitt {
    private List<WanderPOI> poiListe;
    private List<WanderRoute> routenListe;

    public List<WanderPOI> getPoiListe() {
        return poiListe;
    }

    public void setPoiListe(List<WanderPOI> poiListe) {
        this.poiListe = poiListe;
    }

    public List<WanderRoute> getRoutenListe() {
        return routenListe;
    }

    public void setRoutenListe(List<WanderRoute> routenListe) {
        this.routenListe = routenListe;
    }
    
    
}
