package wapp.wsclient;

import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author MartinM
 */
@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlRootElement
public class WanderKommentar extends WanderObjekt {
    
    private String authorName;
    private String text;
    private Date time;
    private int placeId;
    private int routeId;
    private int authorId;
    private int rating;

    public int getRating() {
        return rating;
    }

    /**
     * Sets the rating of this comment in a range from 1 (lowest) to 5 (highest), or 0 to abstain.
     * @param rating The rating this comment applies to the object it is commenting on
     */
    public void setRating(int rating) {
        this.rating = rating;
    }

    public WanderKommentar() {
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public int getPlaceId() {
        return placeId;
    }

    public void setPlaceId(int placeId) {
        this.placeId = placeId;
    }

    public int getRouteId() {
        return routeId;
    }

    public void setRouteId(int routeId) {
        this.routeId = routeId;
    }

    public int getAuthorId() {
        return authorId;
    }

    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }
    
    @Override
    public String toString() {
        return "WanderKommentar[id=" + this.getId() + "; pId=" + this.getPlaceId() + "; rId=" + this.getRouteId() + "; aId=" + this.getAuthorId() + "; text=" + this.getText() + "; time=" + this.getTime() + "]";
    }
    
    public static boolean validate(WanderKommentar wk) {
        return (wk != null)
//                && (wk.authorName != null) && (wk.authorName.length() > 0) && (wk.authorName.length() < 255)
                && (wk.text != null) && (wk.text.length() > 0) && (wk.text.length() < 255)
//                && (wk.time != null)
                && (wk.authorId > 0)
                && (wk.rating >= 0) && (wk.rating <= 5)
                && ((wk.placeId > 0) ^ (wk.routeId > 0)) && ((wk.placeId == 0) ^ (wk.routeId == 0));
    }
    
}
