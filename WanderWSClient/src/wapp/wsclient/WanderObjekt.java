package wapp.wsclient;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author MartinM
 */
@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlRootElement
public class WanderObjekt {
    @XmlTransient
    public static final String MEDIA_TYPE = MediaType.APPLICATION_JSON_TYPE + ";charset=utf-8";
    
    private int id;
    
    public void setId(int id) {
        this.id = id;
    }
    
    public int getId() {
        return this.id;
    }
    
    public WanderObjekt() {
        id = -1;
    }
    
    /**
     * Marks this WanderObjekt as a new object, meaning it should result
     * in new data being inserted into the database upon transmission.
     * This state is encoded by setting the id to -1, and is the default
     * when constructing the object with the default constructor.
     */
    public void markAsNewObject() {
        this.id = -1;
    }
    
    /**
     * Returns true if this WanderObjekt is marked as a new object. This is
     * the case if its id is -1 (or, for whatever reason, less than -1).
     * @return True, if this is to be a new persistent object.
     */
    public boolean isNewObject() {
        return this.id <= -1;
    }
    
    /**
     * Checks whether the id for this object is valid, meaning it is greater
     * than 0.
     * @return True, if id greater than 0.
     */
    public boolean hasValidId() {
        return this.id > 0;
    }
    
    @Override
    public String toString() {
        return "Missing toString() impl. for " + this.getClass().getName() + ".";
    }
}
