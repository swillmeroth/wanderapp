package tests;

import java.util.List;
import wapp.wsclient.GeoLocation;
import wapp.wsclient.WanderKommentar;
import wapp.wsclient.WanderPOI;
import wapp.wsclient.WanderUser;
import wapp.wsclient.WanderWSClient;

/**
 *
 * @author MartinM
 */
public class TestComments {
    public static void main(String[] args) throws InterruptedException {
        WanderWSClient w = new WanderWSClient();
        
        WanderPOI p1 = new WanderPOI();
        p1.setName("Kirchäää");
        p1.setCity("Foo");
        p1.setIdChanging(1);
        p1.setKoordinaten(new GeoLocation(80, 33));
        p1.setOpenTimes("Niemals");
        p1.setPoiType("Kirche");
        p1.setPostalCode("12345");
        p1.setSize(4);
        p1.setStreet("streeet");
        p1.setId(w.setPOI(p1));
        
        //w.register("root@moot.com", "supergeheim", "Root");
        w.setAuthenticationData("root@moot.com", "supergeheim");
        WanderUser u1 = w.testAuthentication();
        
        WanderKommentar k1 = new WanderKommentar();
        k1.setPlaceId(p1.getId());
        k1.setText("Doofe Kirche");
        k1.setRating(1);
        k1.setAuthorId(u1.getId());
        w.setKommentar(k1);
        
        //Thread.sleep(1100);
        
        
        WanderKommentar k3 = new WanderKommentar();
        k3.setPlaceId(p1.getId());
        k3.setText("Beste Kirche");
        k3.setRating(5);
        k3.setAuthorId(u1.getId());
        w.setKommentar(k3);
        
        //Thread.sleep(1100);
        
        WanderKommentar k2 = new WanderKommentar();
        k2.setPlaceId(p1.getId());
        k2.setText("Schöne Kirche");
        k2.setRating(4);
        k2.setAuthorId(u1.getId());
        w.setKommentar(k2);
        
        p1 = w.getPOI(p1.getId());
        System.out.println("POI with id " + p1.getId() + " has rating of " + p1.getRating());
        
        List<WanderKommentar> wkl = w.getPOIKommentare(p1.getId(), 1, 10);
        for (WanderKommentar wk : wkl) {
            System.out.println("wk#" + wk.getId() + ": " + wk.getText());
        }
    }
}
