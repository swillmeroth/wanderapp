package tests;

import wapp.wsclient.Credentials;
import wapp.wsclient.WanderUser;
import wapp.wsclient.WanderWSClient;

/**
 *
 * @author MartinM
 */
public class TestUsers {
    public static void main(String[] args) {
        WanderWSClient w = new WanderWSClient();
        
        String email = "moot@root.com";
        String password = "supergeheim";
        
        w.register(email, password, "Foobar");
        w.setAuthenticationData(email, password);
        System.out.println("My user id is: " + w.testAuthentication().getId());
    }
    
}
