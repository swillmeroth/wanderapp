package tests;

import wapp.wsclient.GeoLocation;
import wapp.wsclient.WanderKommentar;
import wapp.wsclient.WanderPOI;
import wapp.wsclient.WanderWSClient;

/**
 *
 * @author MartinM
 */
public class TestPlaces {
    public static void main(String[] args) {
        WanderWSClient c = new WanderWSClient();
        
        WanderPOI poi = new WanderPOI();
        poi.getKoordinaten().setLatitude(73);
        poi.getKoordinaten().setLongitude(2);
        poi.setCity("Berlin");
        poi.setKoordinaten(new GeoLocation(1, 1, 1));
        poi.setOpenTimes("Montags");
        poi.setPoiType("Kirche");
        poi.setPostalCode("12345");
        poi.setStreet("Blubstraße");
        poi.setName("Tolle Kirche");
        System.out.println("ID: " + c.setPOI(poi));
        
        poi.setName("Schöner Friedhof");
        int id = c.setPOI(poi);
        System.out.println("ID: " + id);
        
        poi.setName("Großer Dom");
        System.out.println("ID: " + c.setPOI(poi));
        
        WanderPOI p2 = c.getPOI(id);
        if (p2 != null)
            System.out.println("Got POI #" + id + ": " + p2.getName() + " with " + p2.getCommentCount() + " comments.");
        
        p2.setId(id);
        p2.setName("Schäbiger Friedhof");
        c.setPOI(p2);
        
        p2 = c.getPOI(id);
        if (p2 != null)
            System.out.println("Got POI #" + id + ": " + p2.getName() + " with " + p2.getCommentCount() + " comments.");
        
    }
}
