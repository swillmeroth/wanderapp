package tests;

import java.util.LinkedList;
import java.util.List;
import wapp.wsclient.GeoLocation;
import wapp.wsclient.WanderRoute;
import wapp.wsclient.WanderWSClient;

/**
 *
 * @author MartinM
 */
public class TestRoutes {
    public static void main(String[] args) {
        WanderWSClient w = new WanderWSClient();
        
        WanderRoute wr = new WanderRoute();
        wr.setName("foo");
        wr.setEquipment("bar");
        
        LinkedList<GeoLocation> gll = new LinkedList<>();
        gll.add(new GeoLocation(5.345543, 10.443424, 5));
        gll.add(new GeoLocation(6.956734, 12.967434, 8));
        gll.add(new GeoLocation(7.822123, 8.905732, 16));
        gll.add(new GeoLocation(8.403945, 11.875633, 13));
        
        wr.setRoutePoints(gll);
        
        wr.setId(w.setRoute(wr));
        
        WanderRoute wr2 = w.getRoute(wr.getId());
        System.out.println("wr2: " + wr2.getName() + "; " + wr2.getEquipment());
        List<GeoLocation> gll2 = wr2.getRoutePoints();
        for (GeoLocation gl : gll2) {
            System.out.println("gl: " + gl.getLatitude() + ", " + gl.getLongitude());
        }
    }
}
