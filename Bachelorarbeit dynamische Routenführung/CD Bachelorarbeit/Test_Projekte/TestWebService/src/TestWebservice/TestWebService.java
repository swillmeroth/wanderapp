package TestWebservice;

import DTO.Geo;
import DTO.TourSettings;
import DTO.GeoPoint;
import DTO.Language;
import DTO.LevelOfDifficulty;
import DTO.TargetPoint;
import DTO.TargetPointType;
import DTO.WayPoint;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import net.sf.json.JSONSerializer;

/**
 * Testprogramm zum Testen des Web-Services.
 */
public class TestWebService {

    private static final String URL_GET_TOURS =
            "http://192.168.2.105:8080/SpiritualTours-war/resources/spiritualtours/tours";
//    private static final String URL_GET_TOURS =
//            "http://spiritualtours.dyndns-server.com:8080/SpiritualTours-war/resources/spiritualtours/settings";
    private static final String URL_GET_ROUTE =
            "http://192.168.2.105:8080/SpiritualTours-war/resources/spiritualtours/route";
    private static final String URL_GET_QUESTION =
            "http://192.168.2.105:8080/SpiritualTours-war/resources/spiritualtours/";

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        TourSettings settingsHeini = new TourSettings(
                new GeoPoint(51.59719444, 6.922013889, 0),
                TourSettings.Locomotion.HOOF_IT,
                TourSettings.HowGood.NORMAL, 120,
                new TargetPointType[]{TargetPointType.CHURCH, TargetPointType.WAYSIDE_CROSS},
                Language.GERMAN,
                true);

        TourSettings settingsHaltern1 = new TourSettings(
                new GeoPoint(51.74313007808561, 7.187139987945557, 0),
                TourSettings.Locomotion.HOOF_IT,
                TourSettings.HowGood.SLOW, 30,
                new TargetPointType[]{TargetPointType.CHURCH,
                    TargetPointType.STATUE, TargetPointType.MONUMENT,
                    TargetPointType.JEWISH_CEMETERY, TargetPointType.WAYSIDE_CROSS},
                Language.GERMAN, false);

        TourSettings settingsHaltern2 = new TourSettings(
                new GeoPoint(51.74313007808561, 7.187139987945557, 0),
                TourSettings.Locomotion.BIKE,
                TourSettings.HowGood.FAST, 120,
                new TargetPointType[]{TargetPointType.CHURCH,
                    TargetPointType.CEMETERY, TargetPointType.MONUMENT,
                    TargetPointType.JEWISH_CEMETERY},
                Language.GERMAN, false);

        TourSettings settingsHaltern3 = new TourSettings(
                new GeoPoint(51.74313007808561, 7.187139987945557, 0),
                TourSettings.Locomotion.CAR,
                TourSettings.HowGood.NORMAL, 180,
                new TargetPointType[]{TargetPointType.CHURCH,
                    TargetPointType.CEMETERY, TargetPointType.MONUMENT,
                    TargetPointType.JEWISH_CEMETERY, TargetPointType.CHAPEL,
                    TargetPointType.JEWISH_CEMETERY, TargetPointType.WAYSIDE_CROSS},
                Language.GERMAN, false);

        TourSettings settingsHaltern4 = new TourSettings(
                new GeoPoint(51.74313007808561, 7.187139987945557, 0),
                TourSettings.Locomotion.CAR,
                TourSettings.HowGood.NORMAL, 360,
                new TargetPointType[]{TargetPointType.CHURCH,
                    TargetPointType.CEMETERY, TargetPointType.JEWISH_CEMETERY,
                    TargetPointType.CHAPEL, TargetPointType.JEWISH_CEMETERY},
                Language.GERMAN, false);

        /* ------------------------------------------------------------------ */

        TourSettings settingsSythen1 = new TourSettings(
                new GeoPoint(51.77185197927215, 7.220968008041382, 0),
                TourSettings.Locomotion.HOOF_IT,
                TourSettings.HowGood.NORMAL, 60,
                new TargetPointType[]{TargetPointType.CHURCH,
                    TargetPointType.CEMETERY, TargetPointType.JEWISH_CEMETERY,
                    TargetPointType.CHAPEL, TargetPointType.JEWISH_CEMETERY},
                Language.GERMAN, false);

        TourSettings settingsSythen2 = new TourSettings(
                new GeoPoint(51.77185197927215, 7.220968008041382, 0),
                TourSettings.Locomotion.BIKE,
                TourSettings.HowGood.NORMAL, 90,
                new TargetPointType[]{TargetPointType.CHURCH,
                    TargetPointType.CEMETERY, TargetPointType.JEWISH_CEMETERY,
                    TargetPointType.CHAPEL, TargetPointType.JEWISH_CEMETERY},
                Language.GERMAN, false);

        /* ------------------------------------------------------------------ */

        TourSettings settingsLavesum1 = new TourSettings(
                new GeoPoint(51.779117718119, 7.16033935546875, 0),
                TourSettings.Locomotion.BIKE,
                TourSettings.HowGood.SLOW, 420,
                new TargetPointType[]{TargetPointType.CHURCH,
                    TargetPointType.CEMETERY, TargetPointType.JEWISH_CEMETERY,
                    TargetPointType.CHAPEL, TargetPointType.JEWISH_CEMETERY},
                Language.GERMAN, false);

        TourSettings settingsLavesum2 = new TourSettings(
                new GeoPoint(51.779117718119, 7.16033935546875, 0),
                TourSettings.Locomotion.CAR,
                TourSettings.HowGood.SLOW, 30,
                new TargetPointType[]{TargetPointType.CHURCH},
                Language.GERMAN, false);

        /* ------------------------------------------------------------------ */

        TourSettings settingsBossendorf1 = new TourSettings(
                new GeoPoint(51.72737771087995, 7.186260223388672, 0),
                TourSettings.Locomotion.HOOF_IT,
                TourSettings.HowGood.FAST, 150,
                new TargetPointType[]{TargetPointType.CHURCH,
                    TargetPointType.CHAPEL, TargetPointType.MONUMENT,
                    TargetPointType.CEMETERY, TargetPointType.WAYSIDE_CROSS},
                Language.GERMAN, false);

        TourSettings settingsBossendorf2 = new TourSettings(
                new GeoPoint(51.779117718119, 7.16033935546875, 0),
                TourSettings.Locomotion.CAR,
                TourSettings.HowGood.SLOW, 30,
                new TargetPointType[]{TargetPointType.CHURCH,
                    TargetPointType.STATUE, TargetPointType.MONUMENT,
                    TargetPointType.JEWISH_CEMETERY, TargetPointType.WAYSIDE_CROSS},
                Language.GERMAN, false);

        /* |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| */

        GeoPoint aimGeoPoint = new GeoPoint(51.74200902963067, 7.183595448732376);

        long[] aksedQuestionIDs = new long[]{};

//        getTours(URL_GET_TOURS, JSONSerializer.toJSON(settingsHaltern1).toString());
//        getRoute(URL_GET_ROUTE, JSONSerializer.toJSON(settingsHaltern1).toString(), 
//                JSONSerializer.toJSON(aimGeoPoint).toString());
        getQuestion(URL_GET_QUESTION, 49, Language.GERMAN, LevelOfDifficulty.EASY,
                JSONSerializer.toJSON(aksedQuestionIDs).toString());
    }

    /**
     * Ersetzt die Zielpunkte von aktuellem Wegpunkt bis zum nächsten Zielpunkt
     * durch die neue Route.
     * 
     * @param toSkippedTargetPoint
     *            zu überspringender Zielpunkt
     * @param route
     *            neue Route
     */
    public static List<WayPoint> replaceWayPoints(List<WayPoint> route, List<WayPoint> wayPoints,
            TargetPoint nextTargetPoint, int index) {

        /* neue Wegpunkt-Liste erstellen */
        List<WayPoint> newWayPoints = getSubWayPointList(wayPoints, 0, index);

        /* Route hinzufügen */
        newWayPoints.addAll(route);
        newWayPoints.remove(newWayPoints.size() - 1);

        /* restliche Strecke hinzufügen */
        newWayPoints.addAll(getSubWayPointList(wayPoints,
                wayPoints.indexOf(nextTargetPoint),
                wayPoints.size() - 1));

        /* neue Wegpunktliste setzen */
        return newWayPoints;
    }

    /**
     * Gibt eine Sub-Liste der Wegpunktliste.
     * 
     * @param fromIndex
     *            Index Von (0 - n)
     * @param toIndex
     *            Index Bis (n - m)
     * @return Wegpunkt-Subliste
     */
    private static List<WayPoint> getSubWayPointList(List<WayPoint> wayPoints, int fromIndex, int toIndex) {
        List<WayPoint> subWayPointList = new ArrayList<WayPoint>();
        for (int i = fromIndex; i <= toIndex; i++) {
            subWayPointList.add(wayPoints.get(i));
        }
        return subWayPointList;
    }

    /**
     * Liefert die Touren.
     * @param urlString URL
     * @param settings Tour
     */
    private static void getTours(String urlString, String settings) {

        try {
            // Create file 
            FileWriter fstream = new FileWriter("C:\\Users\\Michael\\Desktop\\tour.txt");
            BufferedWriter out = new BufferedWriter(fstream);

            // Construct data
            String data = URLEncoder.encode("settings", "UTF-8") + "=" + URLEncoder.encode(settings, "UTF-8");

            // Send data
            URL url = new URL(urlString);
            URLConnection conn = url.openConnection();
            conn.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(data);
            wr.flush();

            // Get the response
            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = rd.readLine()) != null) {
                out.write(line);
                System.out.println(line);
            }
            wr.close();
            rd.close();

            // Close the output stream
            out.close();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }

    /**
     * Liefert die Touren.
     * @param urlString URL
     * @param settings Tour
     */
    private static void getRoute(String urlString, String settings, String aim) {

        try {
            // Create file 
            FileWriter fstream = new FileWriter("C:\\Users\\Michael\\Desktop\\route.txt");
            BufferedWriter out = new BufferedWriter(fstream);

            // Construct data
            String data = URLEncoder.encode("settings", "UTF-8") + "=" + URLEncoder.encode(settings, "UTF-8");
            data += "&" + URLEncoder.encode("targetPoint", "UTF-8") + "=" + URLEncoder.encode(aim, "UTF-8");

            // Send data
            URL url = new URL(urlString);
            URLConnection conn = url.openConnection();
            conn.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(data);
            wr.flush();

            // Get the response
            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = rd.readLine()) != null) {
                out.write(line);
                System.out.println(line);
            }
            wr.close();
            rd.close();

            // Close the output stream
            out.close();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }

    /**
     * Liefert die Frage.
     * @param urlString URL
     * @param targetPointID ID
     * @param language Sprache
     * @param levelOfDifficulty Schwierigkeitsgrad
     * @param askedQuestionIDs schon gestellte Fragen
     */
    private static void getQuestion(String urlString, int targetPointID,
            Language language, LevelOfDifficulty levelOfDifficulty,
            String askedQuestionIDs) {
        try {

            // Construct data
            String data = URLEncoder.encode("askedQuestionIDs", "UTF-8")
                    + "=" + URLEncoder.encode(askedQuestionIDs, "UTF-8");

            // Send data
            URL url = new URL(urlString + targetPointID + "/" + language + "/"
                    + levelOfDifficulty.name());
            URLConnection conn = url.openConnection();
            conn.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(data);
            wr.flush();

            // Get the response
            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = rd.readLine()) != null) {
                System.out.println(line);
            }
            wr.close();
            rd.close();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }
}
