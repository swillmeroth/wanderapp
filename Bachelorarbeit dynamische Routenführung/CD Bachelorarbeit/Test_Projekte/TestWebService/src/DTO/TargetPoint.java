package DTO;


/**
 * Definiert einen Zielpunkt. Zugehörige Informationen sind Name des Objekts, 
 * die Art des Objekts, die Position, und eine Beschreibung.
 */
public class TargetPoint extends WayPoint {

    /* ====================================================================== */
    /*                       Instanzvariablen                                 */
    /* ====================================================================== */
    /** Id des Zielpunktes. */
    private long id;
    /** Name des Zielpunkts. */
    private String name;
    /** Art des Zielpunkts. */
    private TargetPointType targetPointType;
    /** Informationen zum Zielpunkt. */
    private String information;
    /** Postleitzahl. */
    private int postcode;
    /** Ort. */
    private String town;
    /** Straße/Kreuzung */
    private String street;

    /* ====================================================================== */
    /*                       Konstruktoren                                    */
    /* ====================================================================== */
    /**
     * Erzeugt einen Zielpunkt.
     * @param geoPoint geografische Koordinate
     * @param name Name des Zielpunkts
     */
    public TargetPoint(String name, GeoPoint geoPoint) {

        super(geoPoint);
        this.name = name;
    }
    
    /**
     * Erzeugt einen Zielpunkt.
     * @param orderID Rang in der Reihenfolge der Wegpunkte
     * @param range Reichweite um den Wegpunkt herum in Meter
     * @param geoPoint geografische Koordinate
     * @param postcode Postleitzahl
     * @param town Ort 
     * @param street Straße bzw. Kreuzung
     * @param id Id des Zielpunkts
     * @param name Name des Zielpunkts
     * @param targetPointType Art des Zielpunkts
     * @param information Info zum Zielpunkt
     */
    public TargetPoint(int orderID, int range, GeoPoint geoPoint, int postcode,
            String town, String street, long id, String name,
            TargetPointType targetPointType, String information) {

        super(orderID, geoPoint, true);
        this.setRange(range);

        this.postcode = postcode;
        this.town = town;
        this.street = street;

        this.id = id;
        this.name = name;
        this.targetPointType = targetPointType;
        this.information = information;
    }

    /**
     * Leerer Konstruktor.
     */
    public TargetPoint() {
    }

    /* ====================================================================== */
    /*                       Getter & Setter                                  */
    /* ====================================================================== */
    /**
     * Gibt die ID des Zielpunkts.
     * @return ID des Zielpunkts.
     */
    public long getId() {
        return id;
    }

    /**
     * Setzt die ID des Zielpunkts.
     * @param ID des Zielpunkts.
     */
    public void setId(long id) {
        this.id = id;
    }

    /* ---------------------------------------------------------------------- */
    /**
     * Gibt den Namen des Zielpunkts.
     * @return Namen des Zielpunkts.
     */
    public String getName() {
        return name;
    }

    /**
     * Setzt den Namen des Zielpunkts.
     * @param Namen des Zielpunkts.
     */
    public void setName(String name) {
        this.name = name;
    }

    /* ---------------------------------------------------------------------- */
    /**
     * Gibt den Typen des Zielpunkts.
     * @return Typ des Zielpunkts.
     */
    public TargetPointType getTargetPointType() {
        return targetPointType;
    }

    /**
     * Setzt den Typen des Zielpunkts.
     * @param Typ des Zielpunkts.
     */
    public void setTargetPointType(TargetPointType targetPointType) {
        this.targetPointType = targetPointType;
    }

    /* ---------------------------------------------------------------------- */
    /**
     * Gibt die Informationen zum Zielpunkts.
     * @return Informationen zum Zielpunkts.
     */
    public String getInformation() {
        return information;
    }

    /**
     * Setzt die Informationen zum Zielpunkts.
     * @param Informationen zum Zielpunkts.
     */
    public void setInformation(String information) {
        this.information = information;
    }

    /* ---------------------------------------------------------------------- */
    /**
     * Gibt die Postleitzahl der Position.
     * @return Postleitzahl
     */
    public int getPostcode() {
        return postcode;
    }

    /**
     * Setzt die Postleitzahl der Position.
     * @param postcode Postleitzahl der Position
     */
    public void setPostcode(int postcode) {
        this.postcode = postcode;
    }

    /* ---------------------------------------------------------------------- */
    /**
     * Gibt den Ort der Position.
     * @return Ort der Position
     */
    public String getTown() {
        return town;
    }

    /**
     * Setzt den Ort der Position
     * @param town Ort der Position
     */
    public void setTown(String town) {
        this.town = town;
    }

    /* ---------------------------------------------------------------------- */
    /**
     * Gibt die Straße der Position
     * @return Straße der Position
     */
    public String getStreet() {
        return street;
    }

    /**
     * Setzt die Straße der Position
     * @param street Straße der Position
     */
    public void setStreet(String street) {
        this.street = street;
    }

    /* ====================================================================== */
    /*                       toString, equals, ...                            */
    /* ====================================================================== */
    /**
     * Prüft, ob der Zielpunkt vollständig initialisiert wurde.
     * @return true, wenn Zielpunkt vollständig initialisiert ist
     */
    public boolean isTargetPointCompletelyInitialized() {
        return this.name != null && this.targetPointType != null
                && this.information != null && this.town != null
                && this.street != null && this.getGeoPoint() != null;
    }

    /**
     * Name des Zielpunktes.
     * @return Name des Zielpunktes
     */
    @Override
    public String toString() {
        return "TargetPoint (" + "orderID = " + getOrderID() + " name = " 
                + this.name + ")"; 
    }

    /**
     * Objekt als String, mit allen interessanten Informationen, außer den 
     * Fragen. (Name des Zielpunkts, Typ, Position, Informationen & Reichweite)
     * @return Objekt als String
     */
    public String toStringCompletely() {
        return "Name: " + name + ",\n"
                + "Typ: " + TargetPointType.getTargetPointType(this.targetPointType) + ",\n"
                + "Information: " + information + ",\n"
                + "PLZ: " + postcode + ",\n"
                + "Town: " + town + ",\n"
                + "Straße: " + street + ",\n"
                + super.toString();
    }
}
