package DTO;

/**
 * Definition der Sprachtypen.
 */
public enum Language {

    /** deutsch */
    GERMAN,
    /** englisch */
    ENGLISH,
    /** niedländisch */
    DUTCH,
    /** türkisch */
    TURKISH,
    /** Spanisch */
    SPANISH,
    /** französisch */
    FRENCH,
    /** italienisch */
    ITALIAN;
    /** Sprache: deutsch */
    private static final String LANGUAGE_GERMAN = "deutsch";
    /** Sprache: englisch */
    private static final String LANGUAGE_ENGLISH = "englisch";
    /** Sprache: englisch */
    private static final String LANGUAGE_DUTCH = "niederländisch";
    /** Sprache: englisch */
    private static final String LANGUAGE_TURKISH = "türkisch";
    /** Sprache: englisch */
    private static final String LANGUAGE_SPANISH = "spanisch";
    /** Sprache: englisch */
    private static final String LANGUAGE_FRENCH = "französisch";
    /** Sprache: englisch */
    private static final String LANGUAGE_ITALIAN = "italienisch";
    /** Sprache: unbekannt */
    private static final String LANGUAGE_UNKNOWN = "unbekannt";

    /**
     * Gibt die Sprache als String
     * @param language Sprache
     * @return Sprache
     */
    public static String getLanguage(Language language) {

        switch (language) {
            case GERMAN:
                return LANGUAGE_GERMAN;
            case ENGLISH:
                return LANGUAGE_ENGLISH;
            case DUTCH:
                return LANGUAGE_DUTCH;
            case TURKISH:
                return LANGUAGE_TURKISH;
            case SPANISH:
                return LANGUAGE_SPANISH;
            case FRENCH:
                return LANGUAGE_FRENCH;
            case ITALIAN:
                return LANGUAGE_ITALIAN;
            default:
                return LANGUAGE_UNKNOWN;
        }
    }

    /**
     * Gibt alle verfügbaren Sprachen als String-Array.
     * @return Sprachen
     */
    public static String[] getLanguages() {
        return new String[]{
                    LANGUAGE_GERMAN, LANGUAGE_ENGLISH, LANGUAGE_DUTCH,
                    LANGUAGE_TURKISH, LANGUAGE_SPANISH, LANGUAGE_FRENCH,
                    LANGUAGE_ITALIAN
                };
    }
    
    /**
     * Gibt alle verfügbaren Sprachen als String-Array.
     * @return Sprachen
     */
    public static String[] getLanguages(Language[] languages) {
        String[] languageNames = new String[languages.length];
        for (int i = 0; i < languages.length; i++) {
            languageNames[i] = getLanguage(languages[i]);
        }
        return languageNames;
    }

    /**
     * Gibt die Sprache entsprechend der Bezeichnung der Sprache.
     * @param language Bezeichnung der Sprache
     * @return Sprache
     * @throws ApplicationException bei unbekannter Sprache
     */
    public static Language getLanguageByName(String language)
            throws Exception {

        if (language.equals(LANGUAGE_GERMAN)) {
            return GERMAN;
        } else if (language.equals(LANGUAGE_ENGLISH)) {
            return ENGLISH;
        } else if (language.equals(LANGUAGE_DUTCH)) {
            return DUTCH;
        } else if (language.equals(LANGUAGE_TURKISH)) {
            return TURKISH;
        } else if (language.equals(LANGUAGE_SPANISH)) {
            return SPANISH;
        } else if (language.equals(LANGUAGE_FRENCH)) {
            return FRENCH;
        } else if (language.equals(LANGUAGE_ITALIAN)) {
            return ITALIAN;
        } else {
            throw new Exception("unbekannte Sprache");
        }
    }
}
