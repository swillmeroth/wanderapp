package DTO;

import java.util.ArrayList;
import java.util.List;

/**
 * Repräsentiert eine Tour.
 */
public class Tour {
    
    /* ====================================================================== */
    /* Instanzvariablen */
    /* ====================================================================== */
    /** Typ der Tour */
    private TourType tourType;
    /** geschätzte Länge in km */
    private double estimatedRouteLength = 0;
    /** geschätzte Dauer in Minuten */
    private int estimatedDuration = 0;
    /** Zielpunkte. */
    private List<TargetPoint> targetPoints;
    /** Wegpunkte der Tour */
    private List<WayPoint> wayPoints;
    /** Wegbeschreibung. */
    private List<Instruction> instructions;

    /* ====================================================================== */
    /* Konstruktoren */
    /* ====================================================================== */
    /**
     * Erzeugt eine Tour.
     * @param targetPoints Wegpunkte der Tour
     * @param tourType Typ der Tour
     * @param estimatedRouteLength geschätzte Länge in km
     * @param estimatedDuration geschätzte Dauer in Minuten
     * @param instructions Wegbeschreibung
     */
    public Tour(TourType tourType, 
            double estimatedRouteLength, int estimatedDuration, 
            List<TargetPoint> targetPoints, List<WayPoint> wayPoints, 
            List<Instruction> instructions) {      
        
        this.tourType = tourType;
        this.estimatedRouteLength = estimatedRouteLength;
        this.estimatedDuration = estimatedDuration;
        this.targetPoints = targetPoints;
        this.wayPoints = wayPoints;
        this.instructions = instructions;
    }
    
    /**
     * Erzeugt eine neue Tour.
     */
    public Tour() {
        
        this.tourType = null;
        this.estimatedRouteLength = 0;
        this.estimatedDuration = 0;
        this.targetPoints = new ArrayList<TargetPoint>();
        this.wayPoints = new ArrayList<WayPoint>();
        this.instructions = new ArrayList<Instruction>();
    }

    /* ====================================================================== */
    /* Getter und Setter */
    /* ====================================================================== */
    /**
     * Gibt den Typ der Tour.
     * @return Typ der Tour
     */
    public TourType getTourType() {
        return tourType;
    }

    /**
     * Setzt den Typ der Tour.
     * @param tourType Typ der Tour
     */
    public void setTourType(TourType tourType) {
        this.tourType = tourType;
    }
    
    /* ---------------------------------------------------------------------- */
    /**
     * Gibt die geschätzte Länge in km.
     * @return geschätzte Länge in km
     */
    public double getEstimatedRouteLength() {
        return estimatedRouteLength;
    }

    /**
     * Setzt die geschätzte Länge in km.
     * @param estimatedRouteLength geschätzte Länge in km
     */
    public void setEstimatedRouteLength(double estimatedRouteLength) {
        this.estimatedRouteLength = estimatedRouteLength;
    }

    /* ---------------------------------------------------------------------- */
    /**
     * Gibt die geschätzte Dauer in Minuten.
     * @return geschätzte Dauer in Minuten
     */
    public int getEstimatedDuration() {
        return estimatedDuration;
    }

    /**
     * Setzt die geschätzte Dauer in Minuten.
     * @param estimatedDuration geschätzte Dauer in Minuten
     */
    public void setEstimatedDuration(int estimatedDuration) {
        this.estimatedDuration = estimatedDuration;
    }
    
    /* ---------------------------------------------------------------------- */
    /**
     * Gibt die Wegpunkte der Tour.
     * @return Wegpunkte der Tour
     */
    public List<TargetPoint> getTargetPoints() {
        return targetPoints;
    }
    
    /**
     * Setzt die Wegpunkte der Tour.
     * @param wayPoints Wegpunkte der Tour
     */
    public void setTargetPoints(List<TargetPoint> targetPoints) {
        this.targetPoints = targetPoints;
    }
    
    
    /**
     * Fügt einen Zielpunkte der Tour hinzu.
     * @param targetPoint Zielpunkte
     */
    public void addTargetPoint(TargetPoint targetPoint) {
        this.targetPoints.add(targetPoint);
    }
    
    /* ---------------------------------------------------------------------- */
    /**
     * Gibt die Wegpunkte der Tour.
     * @return Wegpunkte der Tour
     */
    public List<WayPoint> getWayPoints() {
        return wayPoints;
    }
    
    /**
     * Setzt die Wegpunkte der Tour.
     * @param wayPoints Wegpunkte der Tour
     */
    public void setWayPoints(List<WayPoint> wayPoints) {
        this.wayPoints = wayPoints;
    }
    
    /**
     * Fügt einen Wegpunkt der Tour hinzu.
     * @param wayPoint Wegpunkt
     */
    public void addWayPoint(WayPoint wayPoint) {
        this.wayPoints.add(wayPoint);
    }
    
    /* ---------------------------------------------------------------------- */
    /**
     * Gibt die Wegbeschreibung der Tour.
     * @return Wegbeschreibung der Tour
     */
    public List<Instruction> getInstructions() {
        return instructions;
    }

    /**
     * Setzt die Wegbeschreibung der Tour.
     * @param instructions Wegbeschreibung der Tour
     */
    public void setInstructions(List<Instruction> instructions) {
        this.instructions = instructions;
    }

    /**
     * Fügt eine Anweisungen hinzu.
     * @param instruction Anweisung
     */
    public void addInstruction(Instruction instruction) {
        this.instructions.add(instruction);
    }
    
    /* ====================================================================== */
    /* toString */
    /* ====================================================================== */
    /**
     * Gibt die Tour als String.
     * @return Tour als String
     */
    @Override
    public String toString() {
        return "Tour-Typ: " + this.tourType
                + ", geschätzte Länge in km: " + this.estimatedRouteLength 
                + ", geschätzte Dauer in Minuten: " + this.estimatedDuration
                + ", Zielpunkte: " + this.targetPoints
                + ", Wegpunkte: " + this.wayPoints
                + ", Wegbeschreibung: " + this.instructions;
    }

    /* ====================================================================== */
    /* Hilfsklassen */
    /* ====================================================================== */
    /**
     * Tour-Typen. 
     */
    public enum TourType {
        
        FROM_A_TO_B, FROM_A_TO_A, PILGRIMAGE;
    }
}
