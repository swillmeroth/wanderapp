package DTO;


/**
 * Definiert einen Punkt der Tour.
 */
public class WayPoint implements Comparable<WayPoint> {

    /* ====================================================================== */
    /*                       Konstanten                                       */
    /* ====================================================================== */
    /** Standard-Reichweite */
    private static final int STANDARD_RANGE = 5;
    
    /* ====================================================================== */
    /*                       Instanzvariablen                                 */
    /* ====================================================================== */
    /** Rang in der Reihenfolge der Wegpunkte. */
    private int orderID;
    /** Reichweite um den Wegpunkt herum in Meter. (Standard sind 5 m) */
    private int range;
    /** Geografische Koordinate. */
    private GeoPoint geoPoint;
    /** Gibt an, ob es es sich um einen Zielpunkt handelt. */
    private boolean isTargetPoint;

    /* ====================================================================== */
    /*                       Konstruktoren                                    */
    /* ====================================================================== */
    /**
     * Erzeugt einen Weg-Punkt.
     */
    public WayPoint() {
    }

    /**
     * Erzeugt einen Weg-Punkt.
     * @param orderID Rang in der Reihenfolge der Wegpunkte
     * @param geoPoint geografische Koordinate
     * @param isTargetPoint handelt es sich um ein Zielpunkt
     */
    public WayPoint(int orderID, GeoPoint geoPoint, boolean isTargetPoint) {
        this.orderID = orderID;
        this.range = STANDARD_RANGE;
        this.geoPoint = geoPoint;
        this.isTargetPoint = isTargetPoint;
    }
    
    /**
     * Erzeugt einen Weg-Punkt.
     * @param geoPoint geografische Koordinate
     */
    public WayPoint(GeoPoint geoPoint) {
        this.range = STANDARD_RANGE;
        this.geoPoint = geoPoint;
    }

    /* ====================================================================== */
    /*                       Getter & Setter                                  */
    /* ====================================================================== */
    /**
     * Gibt den Rang in der Reihenfolge der Wegpunkte.
     * @return Rang in der Reihenfolge
     */
    public int getOrderID() {
        return orderID;
    }

    /**
     * Setzt den Rang in der Reihenfolge der Wegpunkte.
     * @param orderID Rang in der Reihenfolge
     */
    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }
    
    /* ---------------------------------------------------------------------- */
    /**
     * Gibt die Reichweite um den Wegpunkt herum in Meter. (Standard sind 5 m)
     * @return Reichweite
     */
    public int getRange() {
        return range;
    }

    /**
     * Setzt die Reichweite um den Wegpunkt herum in Meter. (Standard sind 5 m)
     * @param range Reichweite
     */
    public void setRange(int range) {
        this.range = range;
    }
    
    /* ---------------------------------------------------------------------- */
    /**
     * Gibt die geografische Koordinate.
     * @return geografische Koordinate.
     */
    public GeoPoint getGeoPoint() {
        return geoPoint;
    }

    /**
     * Setzt die geografische Koordinate.
     * @param geoPoint geografische Koordinate.
     */
    public void setGeoPoint(GeoPoint geoPoint) {
        this.geoPoint = geoPoint;
    }
    
    /* ---------------------------------------------------------------------- */
    /**
     * Gibt an, ob es es sich um einen Zielpunkt handelt.
     * @return true, wenn es sich um einen Zielpunkt handelt
     */
    public boolean getIsTargetPoint() {
        return isTargetPoint;
    }

    /**
     * Setzt, ob es es sich um einen Zielpunkt handelt.
     * @param isTargetPoint  true, wenn es sich um einen Zielpunkt handelt
     */
    public void setIsTargetPoint(boolean isTargetPoint) {
        this.isTargetPoint = isTargetPoint;
    }
    
    /* ====================================================================== */
    /*                       toString, equals, ...                            */
    /* ====================================================================== */
    /**
     * Gibt den Wegpunkt als String an.
     * @return Wegpunkt als String
     */
    @Override
    public String toString() {
        return "orderID=" + orderID;
    }

    /**
     * Vergleicht ein Wegpunkt-Objekt mit einem Objekt. Wenn die Koordinaten
     * gleich sind, sind die Objekte gleich.
     * @param obj Objekt
     * @return true, wenn gleich
     */
    @Override
    public boolean equals(Object obj) {
        return obj instanceof WayPoint 
                && this.geoPoint.equals(((WayPoint) obj).geoPoint);
    }

    /**
     * Gibt den HashCode.
     * @return hashCode
     */
    @Override
    public int hashCode() {
        return this.geoPoint.hashCode();
    }

    /**
     * Vergleicht den Rang der Reihenfolge des Wegpunktes.
     * @param wayPoint Wegpunkt mit dem verglichen werden soll
     * @return -1 von Rang vor übergebenen Namen liegt, 0 wenn gleich, 1 wenn 
     * übergebener Rang vor Rang liegt
     */
    @Override
    public int compareTo(WayPoint wayPoint) {
        return this.orderID < wayPoint.orderID 
                ? -1 
                : this.orderID < wayPoint.orderID 
                ? 0 
                : 1;
    }
}
