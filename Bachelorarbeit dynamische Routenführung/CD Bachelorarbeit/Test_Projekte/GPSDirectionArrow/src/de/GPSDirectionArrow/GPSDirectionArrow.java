package de.GPSDirectionArrow;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

/**
 * App zur Navigation.
 */
public class GPSDirectionArrow extends Activity implements LocationListener,
		SensorEventListener {
	
	/** Laengengrad */
	private TextView laengengrad;
	/** Breitengrad */
	private TextView breitengrad;
	/** Breitengrad */
	private TextView azimuthTextView;
	private TextView bearingTextView;

	/** Ziel Breitengrad */
	private double aimLatitude;
	/** Ziel L�ngengrad */
	private double aimLongitude;

	/** Zielname */
	private String aimName;

	/** Ziel Breitengrad */
	private double currentLatitude;
	/** Ziel L�ngengrad */
	private double currentLongitude;

	/** Himmelsrichtung. */
	private float azimuth;

	/** Kompass. */
	private Compass compass;

	/** GPS-Ziel */
	private Compass aimCompass;

	/** Ziel (Text) */
	private TextView aimTextView;

	/** Sensor */
	private SensorManager sensorManager;

	/** Ziele */
	private List<Aim> aims;

	/** Button zur Auswahl der Ziele */
	private Button chooseButton;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		LocationManager locManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000,
				1, this);

		this.sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);

		this.laengengrad = (TextView) findViewById(R.id.longitude);
		this.breitengrad = (TextView) findViewById(R.id.latitude);
		this.azimuthTextView = (TextView) findViewById(R.id.azimuth);
		this.compass = (Compass) findViewById(R.id.compass);
		this.aimCompass = (Compass) findViewById(R.id.direction);
		this.aimTextView = (TextView) findViewById(R.id.aim);
		this.bearingTextView = (TextView) findViewById(R.id.bearing);

		this.chooseButton = (Button) findViewById(R.id.choose_aim);
		this.chooseButton.setOnClickListener(bChooseListener);

		this.aims = new ArrayList<Aim>();
		this.aims.add(new Aim("Wegekreuz Hofwiese", 51.6198, 6.909972222));
		this.aims.add(new Aim("Wegekreuz Reckelsberg", 51.62615833, 6.913452778));
		this.aims.add(new Aim("Wegekreuz Scheideweg", 51.60432222, 6.908494444));
		this.aims.add(new Aim("Mahnmal in Kirchhellen", 51.6029, 6.91982));
		this.aims.add(new Aim("Sonnenuhr Hardinghausen", 51.61862778, 6.908988889));

		this.aimName = aims.get(0).getName();
		this.aimLatitude = aims.get(0).getLatitude();
		this.aimLongitude = aims.get(0).getLongitude();
		this.aimTextView.setText(aimName + " - " + getString(R.string.distance)
				+ getDistanceToNextTargetPoint());
	}

	/** Register for the updates when Activity is in foreground */
	@Override
	protected void onResume() {
		super.onResume();
		sensorManager.registerListener(this,
				sensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION),
				SensorManager.SENSOR_DELAY_NORMAL);
	}

	/** Stop the updates when Activity is paused */
	@Override
	protected void onPause() {
		super.onPause();
		sensorManager.unregisterListener(this);
	}

	public void onLocationChanged(Location loc) {
		this.currentLatitude = loc.getLatitude();
		this.currentLongitude = loc.getLongitude();

		this.laengengrad.setText("breitengrad: " + this.currentLatitude);
		this.breitengrad.setText("laengengrad " + this.currentLongitude);

		setDirection();
	}

	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
	}

	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
	}

	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
	}

	/* -------------------------------------------------------- */

	public void onAccuracyChanged(Sensor arg0, int arg1) {
		// TODO Auto-generated method stub

	}

	public void onSensorChanged(SensorEvent event) {
		this.azimuth = event.values[0];
		this.azimuthTextView
				.setText(getString(R.string.azimuth) + this.azimuth);
		this.compass.setDirection(this.azimuth);
		setDirection();
	}

	/* -------------------------------------------------------- */

	private void setDirection() {
		float angle = (float) (Geo.getAngle(this.currentLatitude,
				this.currentLongitude, aimLatitude, aimLongitude));
		this.bearingTextView.setText(getString(R.string.bearing) + angle);
		this.aimCompass.setDirection(this.azimuth, angle);
		this.aimTextView.setText(aimName + " - " + getString(R.string.distance)
				+ getDistanceToNextTargetPoint());
	}

	/* ======================================================================= */
	/* Choose-Button */
	/* ======================================================================= */
	/**
	 * OnClickListener f�r Back-Button.
	 */
	private OnClickListener bChooseListener = new OnClickListener() {

		/**
		 * Wird bei Klick auf den Button aufgerufen.
		 * 
		 * @param v
		 *            View
		 */
		public void onClick(View v) {
			onClickChoose(v);
		}
	};

	/**
	 * Activity schliessen und zur Vater-Activity zur�ck.
	 * 
	 * @param v
	 *            View
	 */
	protected void onClickChoose(View v) {

		final CharSequence[] items = { "Wegekreuz Hofwiese", "Wegekreuz Reckelsberg", "Wegekreuz Scheideweg",
				"Mahnmal in Kirchhellen", "Sonnenuhr Hardinghausen" };

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Pick a aim");
		builder.setSingleChoiceItems(items, -1,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int item) {
						aimName = aims.get(item).getName();
						aimLatitude = aims.get(item).getLatitude();
						aimLongitude = aims.get(item).getLongitude();
						aimTextView.setText(aimName + " - "
								+ getString(R.string.distance)
								+ getDistanceToNextTargetPoint());
						setDirection();
					}
				});
		AlertDialog alert = builder.create();
		alert.show();
	}

	/* ======================================================================= */
	/* Aim-Klasse */
	/* ======================================================================= */
	public class Aim {

		private String name;
		private double latitude;
		private double longitude;

		public Aim(String name, double latitude, double longitude) {
			this.name = name;
			this.latitude = latitude;
			this.longitude = longitude;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public double getLatitude() {
			return latitude;
		}

		public void setLatitude(double latitude) {
			this.latitude = latitude;
		}

		public double getLongitude() {
			return longitude;
		}

		public void setLongitude(double longitude) {
			this.longitude = longitude;
		}

		@Override
		public String toString() {
			return this.name;
		}
	}

	/**
	 * Gibt die Distanz zum n�chsten Zielpunkt in km bzw in m an. Km wenn die
	 * Distanz �ber 1000m betr�gt.
	 * 
	 * @return Distanz zum n�chsten Zielpunkt
	 */
	private String getDistanceToNextTargetPoint() {

		double distance = Geo.getRealDistance(currentLatitude,
				currentLongitude, aimLatitude, aimLongitude);

		return (distance >= 1 ? ((int) (distance * 1000)) / 1000.0 + " km"
				: (int) (distance * 1000) + " m").replace(".", ",");

	}
}