package de.GPSDirectionArrow;

/**
 * Klasse zur Berechnung von Abst�nden zwischen Geo-Koordinaten.
 */
public final class Geo {

	/**
	 * Ermittelt die Distanz zwischen zwei Positionen.
	 * 
	 * @param latitude1
	 *            Breitengrad von Position1 (in Dezimalgrad)
	 * @param longitude1
	 *            L�ngengrad von Position1 (in Dezimalgrad)
	 * @param latitude2
	 *            Breitengrad von Position2 (in Dezimalgrad)
	 * @param longitude2
	 *            L�ngengrad von Position2 (in Dezimalgrad)
	 * @return Distanz
	 */
	public static double getDistance(double latitude1, double longitude1,
			double latitude2, double longitude2) {
		return Math.sqrt(Math.pow(latitude1 - latitude2, 2)
				+ Math.pow(longitude1 - longitude2, 2));
	}

	/**
	 * Ermittelt den ungef�hren Abstand zwischen zwei Positionen in km.
	 * 
	 * @param latitude1
	 *            Breitengrad von Position1 (in Dezimalgrad)
	 * @param longitude1
	 *            L�ngengrad von Position1 (in Dezimalgrad)
	 * @param latitude2
	 *            Breitengrad von Position2 (in Dezimalgrad)
	 * @param longitude2
	 *            L�ngengrad von Position2 (in Dezimalgrad)
	 * @return Distanz
	 */
	public static double getApproximateDistance(double latitude1,
			double longitude1, double latitude2, double longitude2) {

		/* 6370 entspricht dem Radius der Erdkugel. */
		return Math.acos(Math.sin(decimalAngle2RadAngle(latitude1))
				* Math.sin(decimalAngle2RadAngle(latitude2))
				+ Math.cos(decimalAngle2RadAngle(latitude1))
				* Math.cos(decimalAngle2RadAngle(latitude2))
				* Math.cos(decimalAngle2RadAngle(longitude2 - longitude1))) * 6370;
	}

	/**
	 * Ermittelt den korrekten Abstand zwischen zwei Positionen in km.
	 * 
	 * @param latitude1
	 *            Breitengrad von Position1 (in Dezimalgrad)
	 * @param longitude1
	 *            L�ngengrad von Position1 (in Dezimalgrad)
	 * @param latitude2
	 *            Breitengrad von Position2 (in Dezimalgrad)
	 * @param longitude2
	 *            L�ngengrad von Position2 (in Dezimalgrad)
	 * @return Distanz
	 */
	public static double getRealDistance(double latitude1, double longitude1,
			double latitude2, double longitude2) {

		/* Abplattung der Erde */
		double flatteningOfTheEarth = 1 / 298.257223563;
		/* �quatorradius der Erde in Kilometern */
		double equatorialRadius = 6378137 / 1000.0;

		/* Parameter bestimmen */
		double parameterF = Math.PI * (latitude1 + latitude2) / 360;
		double parameterG = Math.PI * (latitude1 - latitude2) / 360;
		double parameterL = Math.PI * (longitude1 - longitude2) / 360;

		/* Abstand D ermitteln */
		double parameterS = Math.pow(Math.sin(parameterG), 2)
				* Math.pow(Math.cos(parameterL), 2)
				+ Math.pow(Math.cos(parameterF), 2)
				* Math.pow(Math.sin(parameterL), 2);
		double parameterC = Math.pow(Math.cos(parameterG), 2)
				* Math.pow(Math.cos(parameterL), 2)
				+ Math.pow(Math.sin(parameterF), 2)
				* Math.pow(Math.sin(parameterL), 2);
		double parameterW = Math.atan(Math.sqrt(parameterS / parameterC));
		double distance = 2 * parameterW * equatorialRadius;

		/* Abstand durch die Faktoren H1 und H2 korrigieren */
		double parameterR = Math.sqrt(parameterS * parameterC) / parameterW;
		double factorH1 = (3 * parameterR - 1) / (2 * parameterC);
		double factorH2 = (3 * parameterR + 1) / (2 * parameterS);

		/* Abstand in Kilometern berechnen. */
		return distance
				* (1 + flatteningOfTheEarth * factorH1
						* Math.pow(Math.sin(parameterF), 2)
						* Math.pow(Math.cos(parameterG), 2) - flatteningOfTheEarth
						* factorH2
						* Math.pow(Math.cos(parameterF), 2)
						* Math.pow(Math.sin(parameterG), 2));
	}

	/**
	 * Berechnet Dezimalgrad in Bogenma� um.
	 * 
	 * @param DecimalAngle
	 * @return Grad in Bogenma�
	 */
	public static double decimalAngle2RadAngle(double decimalAngle) {
		return decimalAngle * Math.PI / 180;
	}

	/**
	 * Rechnet Bogenma� in Dezimalgrad um.
	 * 
	 * @param RadAngle
	 * @return Grad in Dezimalgrad
	 */
	public static double radAngle2DecAngle(double radAngle) {
		return radAngle * 180 / Math.PI;
	}

	/**
	 * Berechnet den Winkel zwischen 2 Geo-Koordinaten in Abh�ngigkeit zum
	 * Nordpol.
	 * 
	 * @param latitude1
	 *            Breitengrad von Position1 (in Dezimalgrad)
	 * @param longitude1
	 *            L�ngengrad von Position1 (in Dezimalgrad)
	 * @param latitude2
	 *            Breitengrad von Position2 (in Dezimalgrad)
	 * @param longitude2
	 *            L�ngengrad von Position2 (in Dezimalgrad)
	 * @return Winkel in Grad
	 */
	public static float getAngle(double latitude1, double longitude1,
			double latitude2, double longitude2) {

		double dLon = longitude2 - longitude1;

		double y = Math.sin(dLon) * Math.cos(latitude2);
		double x = Math.cos(latitude1) * Math.sin(latitude2)
				- Math.sin(latitude1) * Math.cos(latitude2) * Math.cos(dLon);

		/* Peilung bestimmen */
		return (float) radAngle2DecAngle(Math.atan2(y, x));
	}
}
