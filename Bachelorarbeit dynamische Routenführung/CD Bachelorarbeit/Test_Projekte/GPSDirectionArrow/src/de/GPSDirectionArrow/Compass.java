package de.GPSDirectionArrow;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.ImageView;

public class Compass extends ImageView {

	/* ======================================================================= */
	/* Instanzvariablen */
	/* ======================================================================= */
	private float direction = 0;
	
	/* ======================================================================= */
	/* Konstruktoren */
	/* ======================================================================= */
	/**
	 * 
	 * @param context
	 */
	public Compass(Context context) {
		super(context);
	}

	public Compass(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public Compass(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	/* ======================================================================= */
	/* onDraw */
	/* ======================================================================= */
	@Override
	public void onDraw(Canvas canvas) {
		int height = this.getHeight();
		int width = this.getWidth();

		canvas.rotate(this.direction, width / 2, height / 2);
		super.onDraw(canvas);
	}
	
	/* ======================================================================= */
	/* Setter */
	/* ======================================================================= */
	public void setDirection(float azimuth) {
		this.direction = - azimuth;
		this.invalidate();
	}
	
	public void setDirection(float azimuth, float bearing) {
		this.direction = - azimuth + bearing;
		this.invalidate();
	}
}
