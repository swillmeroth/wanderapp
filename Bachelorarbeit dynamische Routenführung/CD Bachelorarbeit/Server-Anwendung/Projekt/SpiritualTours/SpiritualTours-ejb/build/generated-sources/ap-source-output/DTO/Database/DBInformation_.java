package DTO.Database;

import DTO.Database.DBTargetPoint;
import Definitions.Language;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.2.0.v20110202-r8913", date="2011-09-08T19:38:36")
@StaticMetamodel(DBInformation.class)
public class DBInformation_ { 

    public static volatile SingularAttribute<DBInformation, String> information;
    public static volatile SingularAttribute<DBInformation, DBTargetPoint> targetPoint;
    public static volatile SingularAttribute<DBInformation, Language> vernacular;

}