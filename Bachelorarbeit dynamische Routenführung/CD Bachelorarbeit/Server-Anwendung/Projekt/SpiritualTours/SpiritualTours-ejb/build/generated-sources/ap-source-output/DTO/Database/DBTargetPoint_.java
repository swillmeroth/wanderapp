package DTO.Database;

import DTO.Database.DBInformation;
import DTO.Database.DBPosition;
import DTO.Database.DBQuestion;
import Definitions.TargetPointType;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.2.0.v20110202-r8913", date="2011-09-08T19:38:36")
@StaticMetamodel(DBTargetPoint.class)
public class DBTargetPoint_ { 

    public static volatile SingularAttribute<DBTargetPoint, Long> id;
    public static volatile SingularAttribute<DBTargetPoint, DBPosition> position;
    public static volatile SingularAttribute<DBTargetPoint, Integer> range;
    public static volatile SingularAttribute<DBTargetPoint, String> name;
    public static volatile SingularAttribute<DBTargetPoint, TargetPointType> targetPointType;
    public static volatile CollectionAttribute<DBTargetPoint, DBQuestion> questions;
    public static volatile CollectionAttribute<DBTargetPoint, DBInformation> informations;

}