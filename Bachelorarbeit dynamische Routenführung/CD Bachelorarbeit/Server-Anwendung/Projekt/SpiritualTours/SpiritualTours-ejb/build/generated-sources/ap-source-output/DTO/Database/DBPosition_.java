package DTO.Database;

import DTO.Database.DBTargetPoint;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.2.0.v20110202-r8913", date="2011-09-08T19:38:36")
@StaticMetamodel(DBPosition.class)
public class DBPosition_ { 

    public static volatile SingularAttribute<DBPosition, Long> id;
    public static volatile SingularAttribute<DBPosition, Double> altitude;
    public static volatile SingularAttribute<DBPosition, String> street;
    public static volatile SingularAttribute<DBPosition, Double> longitude;
    public static volatile SingularAttribute<DBPosition, Double> latitude;
    public static volatile SingularAttribute<DBPosition, String> town;
    public static volatile SingularAttribute<DBPosition, DBTargetPoint> targetPoint;
    public static volatile SingularAttribute<DBPosition, Integer> postcode;

}