package DTO.Database;

import DTO.Database.DBTargetPoint;
import Definitions.Language;
import Definitions.LevelOfDifficulty;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.2.0.v20110202-r8913", date="2011-09-08T19:38:36")
@StaticMetamodel(DBQuestion.class)
public class DBQuestion_ { 

    public static volatile SingularAttribute<DBQuestion, Long> id;
    public static volatile SingularAttribute<DBQuestion, String> correctAnswer;
    public static volatile SingularAttribute<DBQuestion, LevelOfDifficulty> levelOfDifficulty;
    public static volatile SingularAttribute<DBQuestion, String[]> wrongAnswers;
    public static volatile SingularAttribute<DBQuestion, String> question;
    public static volatile SingularAttribute<DBQuestion, Language> vernacular;
    public static volatile SingularAttribute<DBQuestion, DBTargetPoint> targetPoint;

}