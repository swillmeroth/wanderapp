package Definitions;

/**
 * Definition der Schwierigkeitsgrade der Fragen. (Leicht, Mittel, Schwer).
 */
public enum LevelOfDifficulty {

    /** Schwierigkeitsgrad: Leicht */
    EASY,
    /** Schwierigkeitsgrad: Mittel */
    MIDDLE,
    /** Schwierigkeitsgrad: Schwer */
    DIFFICULT;
    
    /** Schwierigkeitsgrad: Leicht */
    private static final String LEVEL_OF_DIFFICULTY_EASY = "leicht";
    /** Schwierigkeitsgrad: Mittel */
    private static final String LEVEL_OF_DIFFICULTY_MIDDLE = "mittel";
    /** Schwierigkeitsgrad: Schwer */
    private static final String LEVEL_OF_DIFFICULTY_DIFFICULT = "schwer";
    /** Schwierigkeitsgrad: unbekannt */
    private static final String LEVEL_OF_DIFFICULTY_UNKNOWN = "unbekannt";

    /**
     * Gibt den Schwierigkeitsgrad als String
     * @param levelOfDifficulty Schwierigkeitsgrad
     * @return Schwierigkeitsgrad
     */
    public static String getLevelOfDifficulty(LevelOfDifficulty levelOfDifficulty) {
        if (levelOfDifficulty.equals(EASY)) {
            return LEVEL_OF_DIFFICULTY_EASY;
        } else if (levelOfDifficulty.equals(MIDDLE)) {
            return LEVEL_OF_DIFFICULTY_MIDDLE;
        } else if (levelOfDifficulty.equals(DIFFICULT)) {
            return LEVEL_OF_DIFFICULTY_DIFFICULT;
        } else {
            return LEVEL_OF_DIFFICULTY_UNKNOWN;
        }
    }
}
