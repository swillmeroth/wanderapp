package DTO.Database;

import DTO.XML.XMLPosition;
import java.io.Serializable;
import javax.persistence.*;

/**
 * Definiert eine Position.
 */
@Entity
@Table(name = "Position")
public class DBPosition implements Serializable {
    
    /* ====================================================================== */
    /*                       Instanzvariablen                                 */
    /* ====================================================================== */
    /** 
     * ID. 
     * Primaerschluessel 
     * automatisch generiert */
    @Id
    @GeneratedValue
    private long id;
    /** Breitengrad. */
    private double latitude;    
    /** Längengrad. */
    private double longitude;    
    /** Höhe. */
    private double altitude;
    /** Postleitzahl. */
    private int postcode;
    /** Ort. */
    private String town;
    /** Straße/Kreuzung */
    private String street;  
    /** 
     * Zielpunkt 
     * EAGER => wird sofort aufglöst
     */
    @OneToOne(mappedBy = "position", fetch=FetchType.EAGER)
    private DBTargetPoint targetPoint;

    /* ====================================================================== */
    /*                       Konstruktoren                                    */
    /* ====================================================================== */
    /**
     * Erzeugt eine neue Position.
     * @param latitude Breitengrad
     * @param longitude Längengrad
     * @param altitude Höhe
     * @param postcode PLZ
     * @param town Stadt
     * @param street Straße
     * @param targetPoint Zielpunkt
     */
    public DBPosition(double latitude, double longitude, double altitude, 
            int postcode, String town, String street, DBTargetPoint targetPoint) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.altitude = altitude;
        this.postcode = postcode;
        this.town = town;
        this.street = street;
        this.targetPoint = targetPoint;
    }

    /**
     * Erzeugt eine neue Position.
     * @param latitude Breitengrad
     * @param longitude Längengrad
     * @param altitude Höhe
     * @param postcode PLZ
     * @param town Stadt
     * @param street Straße
     */
    public DBPosition(double latitude, double longitude, double altitude, 
            int postcode, String town, String street) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.altitude = altitude;
        this.postcode = postcode;
        this.town = town;
        this.street = street;
    }
    
    /**
     * Erzeugt eine neue Position.
     * @param position Position
     * @param targetPoint Zielpunkt
     */
    public DBPosition(XMLPosition position, DBTargetPoint targetPoint) {
        this.id = position.getId();
        this.latitude = position.getGeoPoint().getLatitude();
        this.longitude = position.getGeoPoint().getLongitude();
        this.altitude = position.getGeoPoint().getAltitude();
        this.postcode = position.getPostcode();
        this.town = position.getTown();
        this.street = position.getStreet();
        this.targetPoint = targetPoint;
    }
      
    /**
     * Erzeugt eine neue Position.
     */
    public DBPosition() {
    }

    /* ====================================================================== */
    /*                       Getter & Setter                                  */
    /* ====================================================================== */
    /**
     * Gibt die Höhe der Position.
     * @return Höhe der Position
     */
    public double getAltitude() {
        return altitude;
    }

    /**
     * Setzt die Höhe der Position.
     * @param altitude Höhe der Position
     */
    public void setAltitude(double altitude) {
        this.altitude = altitude;
    }
    
    /* ---------------------------------------------------------------------- */

    /**
     * Gibt die Id der Position.
     * @return Id der Position
     */
    public long getId() {
        return id;
    }

    /**
     * Setzt die Id der Position.
     * @param id Id der Position
     */
    public void setId(long id) {
        this.id = id;
    }

    /* ---------------------------------------------------------------------- */

    /**
     * Gibt den Breitengrad der Position.
     * @return Breitengrad der Position
     */
    public double getLatitude() {
        return latitude;
    }

    /**
     * Setzt den Breitengrad der Position.
     * @param latitude Breitengrad der Position
     */
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    /* ---------------------------------------------------------------------- */

    /**
     * Gibt den Längengrad der Position.
     * @return Längengrad der Position
     */
    public double getLongitude() {
        return longitude;
    }

    /**
     * Setzt den Längengrad der Position.
     * @param longitude Längengrad der Position
     */
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    /* ---------------------------------------------------------------------- */

    /**
     * Gibt die PLZ der Position.
     * @return PLZ der Position
     */
    public int getPostcode() {
        return postcode;
    }

    /**
     * Setzt die PLZ der Position.
     * @param postcode PLZ der Position
     */
    public void setPostcode(int postcode) {
        this.postcode = postcode;
    }

    /* ---------------------------------------------------------------------- */

    /**
     * Gibt die Stadt der Position.
     * @return Stadt der Position
     */
    public String getTown() {
        return town;
    }

    /**
     * Setzt die Stadt der Position.
     * @param town PLZ der Stadt
     */
    public void setTown(String town) {
        this.town = town;
    }
    
    /* ---------------------------------------------------------------------- */

    /**
     * Gibt die Straße der Position.
     * @return Straße der Position
     */
    public String getStreet() {
        return street;
    }

    /**
     * Setzt die Straße der Position.
     * @param street Straße der Stadt
     */
    public void setStreet(String street) {
        this.street = street;
    }
    
    /* ---------------------------------------------------------------------- */

    /**
     * Gibt den Zielpunkt der Position.
     * @return Zielpunkt der Position
     */
    public DBTargetPoint getTargetPoint() {
        return targetPoint;
    }

    /**
     * Setzt den Zielpunkt der Position.
     * @param targetPoint Zielpunkt der Stadt
     */
    public void setTargetPoint(DBTargetPoint targetPoint) {
        this.targetPoint = targetPoint;
    }

    /* ====================================================================== */
    /*                       toString, equals, ...                            */
    /* ====================================================================== */
    /**
     * Prüft, ob alle notwendigen Informationen vorhanden sind.
     * @return true, alle notwendigen Informationen vorhanden sind
     */
    public boolean isInformationCompletelyInitialized() {
        return this.town != null 
                && this.town.replaceAll(" ", "").isEmpty()
                && this.street != null 
                && this.street.replaceAll(" ", "").isEmpty()
                && this.targetPoint != null;
    }
    
    /**
     * Vergleicht ein Positions-Objekt mit einem Objekt. Wenn die ID
     * gleich ist, sind die Objekte gleich.
     * @param obj Objekt
     * @return true, wenn gleich
     */
    @Override
    public boolean equals(Object obj) {
    
        return obj instanceof DBPosition && this.id == ((DBPosition) obj).id;
    }

    /**
     * Gibt den HashCode
     * @return HashCode
     */
    @Override
    public int hashCode() {
        
        return (int) id;
    }

    /**
     * Gibt die Position als String an.
     * @return Position als String
     */
    @Override
    public String toString() {
        return "Breitengrad: " + latitude 
                + ", Längengrad: " + longitude 
                + ", Höhe: " + altitude
                + ", Ort: " + town
                + ", Straße/Kreuzung: " + street;
    }
}