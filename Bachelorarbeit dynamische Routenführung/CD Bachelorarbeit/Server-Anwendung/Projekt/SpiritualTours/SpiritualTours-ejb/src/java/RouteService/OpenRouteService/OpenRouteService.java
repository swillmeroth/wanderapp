package RouteService.OpenRouteService;

import DTO.Web.Input.TourSettings.HowGood;
import DTO.Web.Input.TourSettings.Locomotion;
import DTO.Web.Output.TargetPoint;
import DTO.Web.Output.Tour;
import DTO.Web.Output.Tour.TourType;
import DTO.Web.Output.WayPoint;
import Definitions.Language;
import Exceptions.ConnectionException;
import RouteService.IRouteService;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

/**
 * Stellt Methoden zur Verfügung, um sich eine Route von OpenRouteService (ORS)
 * übergeben zu lassen.
 */
public class OpenRouteService implements IRouteService {

    /* ====================================================================== */
    /*                       Konstanten                                       */
    /* ====================================================================== */
    /** URL zu Open Route Service. */
    private static final String OPEN_ROUTE_SERVICE_URL =
            "http://openls.geog.uni-heidelberg.de/fh-gelsenkirchen/route";
    /** XML Reader. */
    private static final String XML_READER =
            "org.apache.xerces.parsers.SAXParser";
    /** XML Feature. */
    private static final String XML_FEATURE =
            "http://apache.org/xml/features/validation/schema";
    /* ---------------------------------------------------------------------- */
    /** Route-Preferenz für Auto */
    private static final String ROUTE_PREFERENCE_CAR = "Fastest";
    /** Route-Preferenz für Auto */
    private static final String ROUTE_PREFERENCE_PEDESTRIAN = "Pedestrian";
    /** Route-Preferenz für Auto */
    private static final String ROUTE_PREFERENCE_BICYLCE_SLOW = "BicycleSafety";
    /** Route-Preferenz für Auto */
    private static final String ROUTE_PREFERENCE_BICYLCE_NORMAL = "Bicycle";
    /** Route-Preferenz für Auto */
    private static final String ROUTE_PREFERENCE_BICYLCE_FAST = "BicycleRacer";
    /* ---------------------------------------------------------------------- */
    /** Sprache: deutsch */
    private static final String LANGUAGE_GERMAN = "de";
    /** Sprache: englisch */
    private static final String LANGUAGE_ENGLISH = "en";
    /** Sprache: englisch */
    private static final String LANGUAGE_DUTCH = "nl";
    /** Sprache: englisch */
    private static final String LANGUAGE_TURKISH = "tr";
    /** Sprache: englisch */
    private static final String LANGUAGE_SPANISH = "es";
    /** Sprache: englisch */
    private static final String LANGUAGE_FRENCH = "fr";
    /** Sprache: englisch */
    private static final String LANGUAGE_ITALIAN = "it";

    /* ====================================================================== */
    /*                       Instanzvariablen                                 */
    /* ====================================================================== */
    /** ContentHandler. */
    private OpenRouteServiceContentHandler contentHandler;
    /** OpenRouteService-Parser. */
    private XMLReader parser;

    /* ====================================================================== */
    /*                       Konstruktor                                      */
    /* ====================================================================== */
    public OpenRouteService() throws SAXException {

        /* Parser initialisieren */
        this.parser = XMLReaderFactory.createXMLReader(XML_READER);
        /* Handler initialisieren */
        this.contentHandler = new OpenRouteServiceContentHandler();
        /* Feature setzen */
        this.parser.setFeature(XML_FEATURE, true);
        /* Content Handler setzen */
        this.parser.setContentHandler(contentHandler);
        /* Content Handler setzen */
        this.parser.setErrorHandler(new OpenRouteServiceErrorHandler());
    }

    /* ====================================================================== */
    /*                       getTour                                         */
    /* ====================================================================== */
    /**
     * Erstellt eine Route durch Anfrage an Open Route Service.
     * @param targetPoints Zielpunkte
     * @param tourType Tour-Typ
     * @param locomotion Wie ist man unterwegs
     * @param howGood Wie gut ist man unterwegs
     * @param language Sprache der Wegbeschreibung (de=deutsch, en=englisch, 
     * nl=niederländisch,...)
     * @param residenceTimeAtTargetPoint Aufenthaltszeit an den Zielpunkten
     * @throws ConnectionException bei Verbindungsfehler mit dem Route Service
     */
    @Override
    public Tour getTour(List<WayPoint> targetPoints, TourType tourType,
            Locomotion locomotion, HowGood howGood, Language language,
            int residenceTimeAtTargetPoint) throws ConnectionException {

        try {
            /* URL an der die Daten gesendet werden */
            URL url = new URL(OPEN_ROUTE_SERVICE_URL);

            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);

            OutputStreamWriter writer =
                    new OutputStreamWriter(connection.getOutputStream());
            writer.write(generateRequest(targetPoints,
                    getRoutePreference(locomotion, howGood),
                    getLanguageAbbreviation(language)));
            writer.flush();

            /* Holen des Response */
            parseResponse(connection.getInputStream());

            writer.close();

        } catch (MalformedURLException ex) {
            throw new ConnectionException(ex.getMessage());
        } catch (IOException ex) {
            throw new ConnectionException(ex.getMessage());
        } catch (SAXException ex) {
            throw new ConnectionException(ex.getMessage());
        }

        /* Zielpunkt in die Wegpunktliste einfügen */
        List<WayPoint> wayPoints = this.contentHandler.getWayPoints();
        for (WayPoint tp : targetPoints) {
            int index = wayPoints.indexOf(tp);
            if (index != -1 && tp instanceof TargetPoint) {
                /* Objekt Klonen, sonst wirkt sich dieses aufgrund der Referenz
                 * auch auf die anderen Touren aus. */
                wayPoints.set(index, new TargetPoint(
                        wayPoints.get(index).getOrderID(),
                        tp.getRange(),
                        tp.getGeoPoint(),
                        ((TargetPoint) tp).getPostcode(),
                        ((TargetPoint) tp).getTown(),
                        ((TargetPoint) tp).getStreet(),
                        ((TargetPoint) tp).getId(),
                        ((TargetPoint) tp).getName(),
                        ((TargetPoint) tp).getTargetPointType(),
                        ((TargetPoint) tp).getInformation()));
            }
        }

        /* Tour erzeugen */
        return new Tour(tourType,
                this.contentHandler.getTotalDistance(),
                this.contentHandler.getTotalTime() + residenceTimeAtTargetPoint,
                wayPoints,
                this.contentHandler.getInstructions());
    }

    /* ====================================================================== */
    /*                       Hilfsmethoden                                    */
    /* ====================================================================== */
    /**
     * Gibt die Sprachkürzel für ORS.
     * @param language Sprache
     * @return Sprachkürzel
     */
    private String getLanguageAbbreviation(Language language) {
        switch (language) {
            case GERMAN:
                return LANGUAGE_GERMAN;
            case ENGLISH:
                return LANGUAGE_ENGLISH;
            case DUTCH:
                return LANGUAGE_DUTCH;
            case TURKISH:
                return LANGUAGE_TURKISH;
            case SPANISH:
                return LANGUAGE_SPANISH;
            case FRENCH:
                return LANGUAGE_FRENCH;
            case ITALIAN:
                return LANGUAGE_ITALIAN;
            default:
                return LANGUAGE_GERMAN;
        }
    }

    /**
     * Bestimmt die Route-Preferenz durch Art der Fortbewegung und wie gut man 
     * sich fortbewegt.
     * @param locomotion Art der Fortbewegung
     * @param howGood Wie gut ist man unterwegs
     * @return Route-Preferenz
     */
    private String getRoutePreference(Locomotion locomotion, HowGood howGood) {
        String routePreference = ROUTE_PREFERENCE_CAR;
        switch (locomotion) {
            case HOOF_IT:
                routePreference = ROUTE_PREFERENCE_PEDESTRIAN;
                break;
            case BIKE:
                switch (howGood) {
                    case SLOW:
                        routePreference = ROUTE_PREFERENCE_BICYLCE_SLOW;
                        break;
                    case NORMAL:
                        routePreference = ROUTE_PREFERENCE_BICYLCE_NORMAL;
                        break;
                    case FAST:
                        routePreference = ROUTE_PREFERENCE_BICYLCE_FAST;
                        break;
                }
                break;
            case CAR:
                routePreference = ROUTE_PREFERENCE_CAR;
                break;
        }
        return routePreference;
    }

    /**
     * Erzeugt den Request, der nach Open Route Service gesendet wird.
     * @param targetPoints Zielpunkte
     * @param locomotion Wie ist man unterwegs
     * @param howGood Wie gut ist man unterwegs
     * @param language Sprache der Wegbeschreibung (de=deutsch, en=englisch, 
     * nl=niederländisch,...)
     * @return Request
     */
    private String generateRequest(List<WayPoint> targetPoints,
            String routePreference, String language) {

        return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                + "<xls:XLS xmlns:xls=\"http://www.opengis.net/xls\" xmlns:sch=\"http://www.ascc.net/xml/schematron\" "
                + "xmlns:gml=\"http://www.opengis.net/gml\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" "
                + "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.opengis.net/xls "
                + "http://schemas.opengis.net/ols/1.1.0/RouteService.xsd\" version=\"1.1\" xls:lang=\"" + language + "\">"
                + "<xls:RequestHeader/>"
                + "<xls:Request methodName=\"RouteRequest\" requestID=\"123456789\" version=\"1.1\">"
                + "<xls:DetermineRouteRequest distanceUnit=\"KM\">"
                + "<xls:RoutePlan>"
                /* Route-Preferenzen */
                + "<xls:RoutePreference>" + routePreference + "</xls:RoutePreference>"
                /* Zielpunkte */
                /* Startposition */
                + "<xls:WayPointList>"
                + "<xls:StartPoint>"
                + "<xls:Position>"
                + "<gml:Point srsName=\"EPSG:4326\">"
                + "<gml:pos>"
                + targetPoints.get(0).getGeoPoint().getLongitude()
                + " "
                + targetPoints.get(0).getGeoPoint().getLatitude()
                + "</gml:pos>"
                + "</gml:Point>"
                + "</xls:Position>"
                + "</xls:StartPoint>"
                /* Wegpunkte */
                + getViaPoints(targetPoints, 1)
                /* Endposition */
                + "<xls:EndPoint>"
                + "<xls:Position>"
                + "<gml:Point srsName=\"EPSG:4326\">"
                + "<gml:pos>"
                + targetPoints.get(targetPoints.size() - 1).getGeoPoint().getLongitude()
                + " "
                + targetPoints.get(targetPoints.size() - 1).getGeoPoint().getLatitude()
                + "</gml:pos>"
                + "</gml:Point>"
                + "</xls:Position>"
                + "</xls:EndPoint>"
                + "</xls:WayPointList>"
                + "</xls:RoutePlan>"
                + "<xls:RouteInstructionsRequest/>"
                + "<xls:RouteGeometryRequest/>"
                + "</xls:DetermineRouteRequest>"
                + "</xls:Request>"
                + "</xls:XLS>";
    }

    /**
     * Liefert die Weg-Punkte, die während der Tour durchlaufen werden.
     * @param targetPoints Zielpunkte
     * @param index Index für den aktuellen Zielpunkt
     * @return Weg-Punkte
     */
    private String getViaPoints(List<WayPoint> targetPoints, int index) {
        return index >= targetPoints.size() - 1
                ? ""
                : "<xls:ViaPoint>"
                + "<xls:Position>"
                + "<gml:Point srsName=\"EPSG:4326\">"
                + "<gml:pos>"
                + targetPoints.get(index).getGeoPoint().getLongitude()
                + " "
                + targetPoints.get(index).getGeoPoint().getLatitude()
                + "</gml:pos>"
                + "</gml:Point>"
                + "</xls:Position>"
                + "</xls:ViaPoint>"
                + getViaPoints(targetPoints, index + 1);
    }

    /**
     * Parst die empfangenen XML-Daten.
     * @param response Response der Anfrage an Open Route Service
     */
    private void parseResponse(InputStream response) throws SAXException,
            IOException {

        /* Inhalt parsen */
        parser.parse(new InputSource(response));
    }
}
