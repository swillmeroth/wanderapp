package DTO.XML;

import DTO.Database.DBPosition;
import DTO.GeoPoint;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;

/**
 * Definiert eine Position.
 */
public class XMLPosition {

    /* ====================================================================== */
    /*                       Instanzvariablen                                 */
    /* ====================================================================== */
    /** Id des Zielpunktes. */
    @Attribute
    private long id;
    /** Position. */
    @Element
    private GeoPoint geoPoint;
    /** Postleitzahl. */
    @Element
    private int postcode;
    /** Ort. */
    @Element
    private String town;
    /** Straße/Kreuzung */
    @Element
    private String street;

    /* ====================================================================== */
    /*                       Konstruktoren                                    */
    /* ====================================================================== */
    /**
     * Erzeugt eine neue Position.
     * @param id ID
     * @param geoPoint Koordinate
     * @param postcode PLZ
     * @param town Stadt
     * @param street Straße
     */
    public XMLPosition(long id, GeoPoint geoPoint, int postcode, String town,
            String street) {
        this.id = id;
        this.geoPoint = geoPoint;
        this.postcode = postcode;
        this.town = town;
        this.street = street;
    }
    
    /**
     * Erzeugt eine neue Position.
     * @param position DBPosition-Objekt
     */
    public XMLPosition(DBPosition position) {
        this.id = position.getId();
        this.geoPoint = new GeoPoint(position.getLatitude(), 
                position.getLongitude(), position.getAltitude());
        this.postcode = position.getPostcode();
        this.town = position.getTown();
        this.street = position.getStreet();
    }
    
    /**
     * Erzeugt eine neue Position.
     */
    public XMLPosition() {
    }

    /* ====================================================================== */
    /*                       Getter & Setter                                  */
    /* ====================================================================== */
    /**
     * Gibt die ID der Frage.
     * @return ID der Frage
     */
    public long getId() {
        return id;
    }

    /**
     * Setzt die ID der Frage.
     * @param ID der Frage
     */
    public void setId(long id) {
        this.id = id;
    }
    
    /* ---------------------------------------------------------------------- */    
    /**
     * Gibt die geografische Koordinate.
     * @return geografische Koordinate.
     */
    public GeoPoint getGeoPoint() {
        return geoPoint;
    }

    /**
     * Setzt die geografische Koordinate.
     * @param geoPoint geografische Koordinate.
     */
    public void setGeoPoint(GeoPoint geoPoint) {
        this.geoPoint = geoPoint;
    }

    /* ---------------------------------------------------------------------- */
    /**
     * Gibt die PLZ der Position.
     * @return PLZ der Position
     */
    public int getPostcode() {
        return postcode;
    }

    /**
     * Setzt die PLZ der Position.
     * @param postcode PLZ der Position
     */
    public void setPostcode(int postcode) {
        this.postcode = postcode;
    }

    /* ---------------------------------------------------------------------- */
    /**
     * Gibt die Stadt der Position.
     * @return Stadt der Position
     */
    public String getTown() {
        return town;
    }

    /**
     * Setzt die Stadt der Position.
     * @param town PLZ der Stadt
     */
    public void setTown(String town) {
        this.town = town;
    }

    /* ---------------------------------------------------------------------- */
    /**
     * Gibt die Straße der Position.
     * @return Straße der Position
     */
    public String getStreet() {
        return street;
    }

    /**
     * Setzt die Straße der Position.
     * @param street Straße der Stadt
     */
    public void setStreet(String street) {
        this.street = street;
    }

    /* ====================================================================== */
    /*                       toString, equals, ...                            */
    /* ====================================================================== */
    /**
     * Vergleicht ein Positions-Objekt mit einem Objekt. Wenn die Position 
     * gleich ist, sind die Objekte gleich.
     * @param obj Objekt
     * @return true, wenn gleich
     */
    @Override
    public boolean equals(Object obj) {

        return obj instanceof XMLPosition && this.geoPoint
                == ((XMLPosition) obj).geoPoint;
    }

    /**
     * Gibt den HashCode
     * @return HashCode
     */
    @Override
    public int hashCode() {

        return this.geoPoint.hashCode();
    }

    /**
     * Gibt die Position als String an.
     * @return Position als String
     */
    @Override
    public String toString() {
        return "Position: " + geoPoint.toString()
                + ", Ort: " + town
                + ", Straße/Kreuzung: " + street;
    }
}