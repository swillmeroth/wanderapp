package DAO;

import DTO.Database.DBPosition;
import DTO.Database.DBQuestion;
import DTO.Database.DBTargetPoint;
import DTO.Database.DBInformation;
import DTO.GeoPoint;
import Definitions.Language;
import Utility.Geo;
import Definitions.LevelOfDifficulty;
import Definitions.TargetPointType;
import Exceptions.ApplicationException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.*;

/**
 * Schnittstelle zwischen Persistence-Kontext und Datenbank.
 */
@Stateless
public class DataAccessObjectBean implements DataAccessObjectLocal {

    @PersistenceContext(unitName = "SpiritualTours-ejbPU")
    private EntityManager em;

    /* ====================================================================== */
    /*                       Getter                                           */
    /* ====================================================================== */
    /**
     * Alle Zielpunkte.
     * @return alle Zielpunkte
     */
    @Override
    public Collection<DBTargetPoint> getAllTargetPoints() {
        return (Collection<DBTargetPoint>) em.createQuery("SELECT tp FROM DBTargetPoint tp").getResultList();
    }

    /**
     * Alle Zielpunkte der gewünschten Typen.
     * @param targetPointType Zielpunkt-Typ
     * @return Zielpunkte eines Typs
     */
    private Collection<DBTargetPoint> getAllTargetPointsOfTypes(
            List<TargetPointType> targetPointTypes) {
        Collection<DBTargetPoint> targetPoints = new ArrayList<DBTargetPoint>();

        for (TargetPointType targetPointType : targetPointTypes) {
            targetPoints.addAll(getAllTargetPointsOfType(targetPointType));
        }

        return ((Collection<DBTargetPoint>) targetPoints);
    }

    /**
     * Alle Zielpunkte eines Typs.
     * @param targetPointType Zielpunkt-Typ
     * @return Zielpunkte eines Typs
     */
    private Collection<DBTargetPoint> getAllTargetPointsOfType(
            TargetPointType targetPointType) {

        return (Collection<DBTargetPoint>) em.createQuery("SELECT tp "
                + "FROM DBTargetPoint tp WHERE tp.targetPointType=:"
                + targetPointType).setParameter(targetPointType.name(),
                targetPointType).getResultList();
    }

    /**
     * Ermittelt den Zielpunkt mit der kürzesten Distanz zur Position, welche 
     * aus Breiten- und Längengrad besteht.
     * @param latitude Breitengrad
     * @param longitude Längengrad
     * @return Zielpunkt
     */
    @Override
    public DBTargetPoint getTargetPointWithShortestDistance(GeoPoint location)
            throws ApplicationException {

        double nearestDistance = -1;
        DBPosition nearestPosition = null;

        for (DBPosition position : getAllPositions()) {
            double currentDistance = Geo.getDistance(
                    new GeoPoint(position.getLatitude(),
                    position.getLongitude()), location);

            /* Wenn noch gar keine Distanz oder Distanz kürzer ist, neuer 
             * nahester Zielpunkt */
            if (nearestDistance > currentDistance || nearestDistance == -1) {
                nearestDistance = currentDistance;
                nearestPosition = position;
            }
        }

        return nearestPosition.getTargetPoint();
    }

    /**
     * Gibt alle Zielpunkte, die innerhalb eine Distanz von einer DBPosition 
     * entfernt liegen. Die Map ist 
     * sortiert, beginnend mit der kürzesten Distanz.
     * @param location Position des Nutzers
     * @param maxDistance maximale Distanz
     * @return Zielpunkte
     * @throws ApplicationException bei fehlerhafter Verbindung zur Datenbank
     */
    @Override
    public List<DBTargetPoint> getTargetPointsWithinDistance(
            GeoPoint location, double maxDistance,
            TargetPointType[] targetPointTypes) throws ApplicationException {

        /* Zielpunkte sortiert nach Distanz */
        List<DBTargetPoint> targetPoints = new ArrayList<DBTargetPoint>();

        for (DBPosition position : getAllPositionsOfTargetPointTypes(targetPointTypes)) {
            double currentDistance = Geo.getDistance(
                    new GeoPoint(position.getLatitude(),
                    position.getLongitude()), location);

            /* Wenn noch gar keine Distanz oder Distanz kürzer ist, neuer 
             * nahester Zielpunkt */
            if (currentDistance <= maxDistance) {
                targetPoints.add(position.getTargetPoint());
            }
        }

        return targetPoints;
    }

    /**
     * Gibt den Zielpunkt passend zur ID.
     * @param targetPointID ID des Zielpunkts
     * @return Zielpunkt
     * @throws ApplicationException Problem beim Zugriff auf die Datenbank
     */
    @Override
    public DBTargetPoint getTargetPoint(long targetPointID) throws ApplicationException {

        /* Zielpunkt heraussuchen */
        DBTargetPoint targetPoint = em.find(DBTargetPoint.class, targetPointID);

        if (targetPoint == null) {
            throw new ApplicationException("Es ist ein unerwarteter Fehler "
                    + "aufgetreten. Der Befehl konnte nicht ausgeführt werden.");
        }

        return targetPoint;
    }

    /* ---------------------------------------------------------------------- */
    /**
     * Gibt alle Positionen.
     * @return alle Positionen
     */
    private Collection<DBPosition> getAllPositions() {
        return (Collection<DBPosition>) em.createQuery("SELECT pos FROM DBPosition pos").getResultList();
    }

    /**
     * Liefert alle Positionen, an dem sich ein Zielpunkt eines im Parameter 
     * übergebenen gewünschten Typs.
     * @return Positionen
     */
    private Collection<DBPosition> getAllPositionsOfTargetPointTypes(
            TargetPointType[] targetPointTypes) {

        Collection<DBPosition> positions = new ArrayList<DBPosition>();

        for (TargetPointType targetPointType : targetPointTypes) {
            positions.addAll(getAllPositionsOfTargetPointType(targetPointType));
        }

        return positions;
    }

    /**
     * Liefert alle Positionen, an dem sich ein Zielpunkt und dem gewünschten 
     * Typ entspricht.
     * @return Positionen
     */
    public Collection<DBPosition> getAllPositionsOfTargetPointType(TargetPointType targetPointType) {

        return (Collection<DBPosition>) em.createQuery("SELECT pos "
                + "FROM DBPosition pos "
                + "WHERE pos.targetPoint.targetPointType=:"
                + targetPointType).setParameter(targetPointType.name(),
                targetPointType).getResultList();
    }

    /**
     * Gibt die DBPosition eines Zielpunkts.
     * @param targetPointID Id des Zielpunkts
     * @return DBPosition des Zielpunkts
     */
    private DBPosition getPositionFor(long targetPointID) throws ApplicationException {

        DBPosition positon = (DBPosition) em.createQuery("SELECT pos "
                + "FROM DBPosition pos WHERE pos.targetPoint.id = "
                + targetPointID).getSingleResult();

        if (positon == null) {
            throw new ApplicationException("Es ist ein unerwarteter Fehler "
                    + "aufgetreten. Der Befehl konnte nicht ausgeführt werden.");
        }

        return positon;
    }

    /* ---------------------------------------------------------------------- */
    /**
     * Gibt alle Fragen.
     * @return alle Fragen
     */
    private Collection<DBQuestion> getAllQuestions() {
        return (Collection<DBQuestion>) em.createQuery("SELECT q "
                + "FROM DBQuestion q").getResultList();
    }

    /**
     * Liefert alle Fragen eines Zielpunktes.
     * @param targetPointID Id des Zielpunkts
     * @return Fragen des Zielpunkts
     */
    @Override
    public List<DBQuestion> getAllQuestionsFor(long targetPointID) {
        return em.createQuery("SELECT q FROM DBQuestion "
                + "q WHERE q.targetPoint.id = " + targetPointID).getResultList();
    }

    /**
     * Gibt alle Frage eines bestimmten Zielpunkts und einem bestimmten 
     * Schwierigkeitsgrad.
     * @param targetPointID Zielpunkt
     * @param language Sprache, in der die Frage gestellt werden soll
     * @param levelOfDifficulty Schwierigkeitsgrad
     * @param askedQuestionIDs IDs der bereits gestellten Fragen
     * @return Fragen
     */
    @Override
    public List<DBQuestion> getAllQuestionsFor(long targetPointID,
            Language language, LevelOfDifficulty levelOfDifficulty,
            long[] askedQuestionIDs) {

        /* bereits gestellte Fragen */
        String askedQuestionIDsString = askedQuestionIDs.length > 0
                ? "" + askedQuestionIDs[0] : "0";
        for (int i = 1; i < askedQuestionIDs.length; i++) {
            askedQuestionIDsString += "," + askedQuestionIDs[i];
        }

        /* mögliche Fragen holen */
        return em.createQuery("SELECT q FROM DBQuestion q"
                + " WHERE q.targetPoint.id = " + targetPointID
                + " AND q.levelOfDifficulty=:" + levelOfDifficulty
                + " AND q.vernacular=:" + language
                + " AND q.id NOT IN (" + askedQuestionIDsString + ")").
                setParameter(levelOfDifficulty.name(), levelOfDifficulty).
                setParameter(language.name(), language).
                getResultList();
    }

    /**
     * Gibt die Frage wieder.
     * @param questionID ID der Frage
     * @return Frage
     * @throws ApplicationException wenn Frage nicht vorhanden
     */
    private DBQuestion getQuestion(long questionID) throws ApplicationException {
        DBQuestion question = (DBQuestion) em.createQuery("SELECT q "
                + "FROM DBQuestion q WHERE q.id = "
                + questionID).getSingleResult();

        if (question == null) {
            throw new ApplicationException("Es ist ein unerwarteter Fehler "
                    + "aufgetreten. Der Befehl konnte nicht ausgeführt werden.");
        }

        return question;
    }

    /* ====================================================================== */
    /*                       Setter                                           */
    /* ====================================================================== */
    /**
     * Fügt neue Zielpunkte hinzu und aktualisiert schon vorhandene.
     * @param targetPoint Zielpunkt
     * @return Protokoll für fehlgeschlagene Updates.
     */
    @Override
    public String[] setTargetPoints(Collection<DBTargetPoint> targetPoints) {

        List<String> protocol = new ArrayList<String>();
        
        for (DBTargetPoint currentTP : targetPoints) {

            /* Überprüfen, ob Zielpunkt schon vorhanden ist, dann update, sonst
             * wird der Zielpunkt neu hinzugefügt */
            DBTargetPoint tp = em.find(DBTargetPoint.class, currentTP.getId());

            if (tp != null) {

                /* gelöschte Fragen bestimmen */
                Collection<DBQuestion> removedQuestions = getRemovedQuestions(
                        tp.getQuestions(), currentTP.getQuestions());

                /* gelöschte Informationen bestimmen */
                Collection<DBInformation> removedInfos = getRemovedInformations(
                        tp.getInformations(), currentTP.getInformations());

                try {
                    
                    updateTargetPoint(currentTP, removedQuestions, removedInfos);
                    
                } catch (ApplicationException ex) {
                    
                    protocol.add("Update des Zielpunktes " + currentTP.getId()
                            + "ist fehlgeschlagen.");
                }

            } else {
                addTargetPoint(currentTP);
            }
        }

        return protocol.toArray(new String[0]);
    }

    /**
     * Zielpunkt hinzufügen.
     * @param targetPoint Zielpunkt
     */
    @Override
    public void addTargetPoint(DBTargetPoint targetPoint) {
        em.persist(targetPoint);
    }

    /* ---------------------------------------------------------------------- */
    /**
     * Fragen hinzufügen.
     * @param targetPointID Id des Zielpunktes
     * @param questions Fragen
     */
    @Override
    public void addQuestions(Collection<DBQuestion> questions)
            throws ApplicationException {
        for (DBQuestion question : questions) {
            addQuestion(question);
        }
    }

    /**
     * Zielpunkt hinzufügen.
     * @param targetPoint Zielpunkt
     */
    private void addQuestion(DBQuestion question) throws ApplicationException {
        try {
            em.persist(question);

        } catch (RollbackException e) {
            throw new ApplicationException("Es ist ein "
                    + "unerwarteter Fehler aufgetreten. "
                    + "Der Befehl konnte nicht ausgeführt "
                    + "werden.");
        } catch (PersistenceException e) {

            throw new RuntimeException("Pesistence Exception");

        } catch (Exception e) {
            em.getTransaction().rollback();
            throw new ApplicationException("Es ist ein unerwarteter Fehler "
                    + "aufgetreten. Der Befehl konnte nicht ausgeführt "
                    + "werden.");
        }
    }

    /* ====================================================================== */
    /*                       Update                                           */
    /* ====================================================================== */
    /**
     * Aktualisiert einen Zielpunkt. Dabei werden Fragen, die neue hinzugekommen
     * sind hinzugefügt, gelöschte Fragen gelöscht und Fragen und DBPosition
     * ebenfalls aktualisiert.
     * @param updateTargetPoint Zielpunkt
     * @param removedQuestions Frage die gelöscht wurden
     * @param removedInformations Informationen die gelöscht wurden
     * @throws ApplicationException bei Fehler bei der Synchronisation mit der 
     * Datenbank.
     */
    @Override
    public void updateTargetPoint(DBTargetPoint updateTargetPoint,
            Collection<DBQuestion> removedQuestions,
            Collection<DBInformation> removedInformations)
            throws ApplicationException {

        DBTargetPoint targetPoint = em.find(DBTargetPoint.class, updateTargetPoint.getId());

        /* Frage, die nicht mehr zum Zielpunkt gehören löschen */
        for (DBQuestion question : removedQuestions) {
            removeQuestion(question.getId());
        }

        /* Informationen, die nicht mehr zum Zielpunkt gehören löschen */
        for (DBInformation information : removedInformations) {
            removeTargetPointInformation(information.getTargetPoint().getId(),
                    information.getLanguage());
        }

        /* Existiert der Zielpunkt, dann... , sonst Fehlermeldung */
        if (targetPoint != null) {
            /* Zielpunkt aktualisieren */
            targetPoint.setName(updateTargetPoint.getName());
            targetPoint.setTargetPointType(updateTargetPoint.getTargetPointType());
            targetPoint.setPosition(updateTargetPoint.getPosition());
            targetPoint.setInformations(updateTargetPoint.getInformations());
            targetPoint.setQuestions(updateTargetPoint.getQuestions());

            /* Daten aktualisieren */
            em.merge(targetPoint);

        } else {
            throw new ApplicationException("Der Zielpunkt mit der ID "
                    + updateTargetPoint.getId() + " existiert nicht in der "
                    + "Datenbank.");
        }
    }

    /* ====================================================================== */
    /*                       Delete                                           */
    /* ====================================================================== */
    /**
     * Zielpunkt löschen.
     * @param targetPointID ID des Zielpunktes
     * @throws ApplicationException 
     */
    @Override
    public void removeTargetPoint(long targetPointID) throws ApplicationException {

        /* Zielpunkt heraussuchen */
        DBTargetPoint targetPoint = em.find(DBTargetPoint.class, targetPointID);

        /* Existiert der Zielpunkt, dann... , sonst Fehlermeldung */
        if (targetPoint != null) {
            /* Zielpunkt löschen (Fragen und DBPosition werden automatisch mit 
             * gelöscht) */
            em.remove(targetPoint);
        } else {
            throw new ApplicationException("Der Zielpunkt mit der ID "
                    + targetPointID + " existiert nicht in der Datenbank.");
        }
    }

    /**
     * Löschen einer Frage.
     * @param questionID ID der Frage
     * @throws ApplicationException Fehler beim Löschen
     */
    private void removeQuestion(long questionID) throws ApplicationException {

        /* Frage mit angegebener ID finden */
        DBQuestion question = em.find(DBQuestion.class, questionID);

        /* Wenn Frage gefunden, dann..., sonst ApplicationException */
        if (question != null) {
            em.remove(question);
        } else {
            throw new ApplicationException("Der Frage mit der ID "
                    + questionID + " existiert nicht in der Datenbank.");
        }
    }

    /**
     * Löschen einer Frage.
     * @param targetPointID Zielpunkt-ID
     * @param language Sprache
     * @throws ApplicationException Fehler beim Löschen
     */
    private void removeTargetPointInformation(long targetPointID,
            Language language) throws ApplicationException {

        /* Frage mit angegebener ID finden */
        DBInformation information =
                em.find(DBTargetPoint.class,
                targetPointID).getInformation(language);

        /* Wenn Frage gefunden, dann..., sonst ApplicationException */
        if (information != null) {
            em.remove(information);
        } else {
            throw new ApplicationException("Der Information mit der ID "
                    + targetPointID + "/" + Language.getLanguage(language)
                    + " existiert nicht in der Datenbank.");
        }
    }

    /* ====================================================================== */
    /*                       Hilfsmethoden                                    */
    /* ====================================================================== */
    /**
     * Liefert die passende Frage aus der Collection.
     * @param questions Collection
     * @param question Frage, die gesucht wird
     * @return Frage
     */
    private DBQuestion getQuestion(DBQuestion[] questions, DBQuestion question) {

        for (DBQuestion mayQuestion : questions) {
            if (mayQuestion.equals(question)) {
                return mayQuestion;
            }
        }
        throw new RuntimeException("Frage nicht vorhanden");
    }
    
    /**
     * Bildet die Differenzmenge zwischen den beiden Collections.
     * Beispiel: q1 = A, B, C | q2 = B, C | return = A
     * @param q1 Collection 1
     * @param q2 Collection 2
     * @return Differenzmenge
     */
    private Collection<DBQuestion> getRemovedQuestions(
            Collection<DBQuestion> q1, Collection<DBQuestion> q2) {
        Collection<DBQuestion> q3 = new ArrayList<DBQuestion>(q1);
        q3.removeAll(q2);
        return q3;
    }

    /**
     * Bildet die Differenzmenge zwischen den beiden Collections.
     * Beispiel: q1 = A, B, C | q2 = B, C | return = A
     * @param q1 Collection 1
     * @param q2 Collection 2
     * @return Differenzmenge
     */
    private Collection<DBInformation> getRemovedInformations(
            Collection<DBInformation> i1, Collection<DBInformation> i2) {
        Collection<DBInformation> i3 = new ArrayList<DBInformation>(i1);
        i3.removeAll(i2);
        return i3;
    }
}
