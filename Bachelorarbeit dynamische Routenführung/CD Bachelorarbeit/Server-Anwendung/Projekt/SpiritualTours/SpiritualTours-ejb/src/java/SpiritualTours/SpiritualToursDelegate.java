package SpiritualTours;

import DTO.GeoPoint;
import DTO.Web.Output.Question;
import DTO.Web.Output.Tour;
import Definitions.LevelOfDifficulty;
import DTO.Web.Input.TourSettings;
import Definitions.Language;
import Exceptions.ConnectionException;
import Exceptions.NoQuestionFoundException;
import Exceptions.NoTargetPointsFoundException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 * Übergabe-Klasse des Spiritual-Tours-Interface.
 */
public class SpiritualToursDelegate {

    /**
     * Zugriff auf die Methoden von Spiritual Tours.
     */
    SpiritualToursLocal spiritualToursBean;

    /**
     * Erzeugt ein Übergabe-Objekt des Spiritual-Tours-Interface.
     */
    public SpiritualToursDelegate() {
        spiritualToursBean = lookupSpiritualToursBeanLocal();
    }

    /**
     * Liefert die Route.
     * @param settings Einstellungen
     * @return Touren
     * @throws NoTargetPointsFoundException keine Zielpunkte vorhanden
     * @throws ConnectionException bei Verbindungsfehler mit dem Route Service
     */
    public List<Tour> getTours(TourSettings settings)
            throws NoTargetPointsFoundException, ConnectionException {
        return spiritualToursBean.getTours(settings);
    }

    /**
     * Liefert die Route.
     * @param settings Einstellungen
     * @param targetPoint Zielpunkt
     * @return Route
     * @throws ConnectionException bei Verbindungsfehler mit dem Route Service
     */
    public Tour getRoute(TourSettings settings, GeoPoint targetPoint)
            throws ConnectionException {
        return spiritualToursBean.getRoute(settings, targetPoint);
    }
    
    /**
     * Liefert eine zufällige Frage zu einem Zielpunkt unter Berücksichtigung
     * eines Schwierigkeitsgrad.
     * @param targetPointID Zielpunkt
     * @param language Sprache, in der die Frage gestellt werden soll
     * @param levelOfDifficulty Schwierigkeitsgrad
     * @param askedQuestionIDs IDs der bereits gestellten Fragen
     * @return Frage
     * @throws Exception Falls Frage nicht vorhanden
     */
    public Question getQuestion(int targetPointID, Language language,
            LevelOfDifficulty levelOfDifficulty, long[] askedQuestionIDs) 
            throws NoQuestionFoundException {
        
        return spiritualToursBean.getQuestion(targetPointID, language, 
                levelOfDifficulty, askedQuestionIDs);
    }

    

    /**
     * Gibt den Zugriff zum SpiritualTours-Interface.
     * @return Zugriffsobjekt zum SpiritualTours-Interface
     */
    private SpiritualToursLocal lookupSpiritualToursBeanLocal() {
        try {
            Context c = new InitialContext();
            return (SpiritualToursLocal) c.lookup("java:global/SpiritualTours/SpiritualTours-ejb/SpiritualToursBean!SpiritualTours.SpiritualToursLocal");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }
    
}
