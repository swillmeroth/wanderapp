package Exceptions;

/**
 * Fehler: keine Frage vorhanden.
 */
public class NoQuestionFoundException extends Exception {
    
    /**
     * Erzeugt eine NoQuestionFoundException.
     * @param s Fehlermeldung
     */
    public NoQuestionFoundException(String s) {
        super(s);
    }
}