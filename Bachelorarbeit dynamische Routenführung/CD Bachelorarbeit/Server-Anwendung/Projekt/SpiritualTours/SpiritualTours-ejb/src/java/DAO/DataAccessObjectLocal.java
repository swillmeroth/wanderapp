package DAO;

import DTO.Database.DBQuestion;
import DTO.Database.DBTargetPoint;
import DTO.Database.DBInformation;
import DTO.GeoPoint;
import Definitions.Language;
import Definitions.LevelOfDifficulty;
import Definitions.TargetPointType;
import Exceptions.ApplicationException;
import java.util.Collection;
import java.util.List;
import javax.ejb.Local;

/**
 * Schnittstelle zwischen Persistence-Kontext und Datenbank.
 */
@Local
public interface DataAccessObjectLocal {

    /* ====================================================================== */
    /*                       Getter                                           */
    /* ====================================================================== */
    /**
     * Alle Zielpunkte.
     * @return alle Zielpunkte
     */
    public Collection<DBTargetPoint> getAllTargetPoints();

    /**
     * Ermittelt den Zielpunkt mit der kürzesten Distanz zur Position, welche 
     * aus Breiten- und Längengrad besteht.
     * @param latitude Breitengrad
     * @param longitude Längengrad
     * @return Zielpunkt
     */
    public DBTargetPoint getTargetPointWithShortestDistance(GeoPoint location)
            throws ApplicationException;

    /**
     * Gibt alle Zielpunkte, die innerhalb eine Distanz von einer DBPosition 
     * entfernt liegen. Die Map ist 
     * sortiert, beginnend mit der kürzesten Distanz.
     * @param location Position des Nutzers
     * @param maxDistance maximale Distanz
     * @return Zielpunkte
     * @throws ApplicationException bei fehlerhafter Verbindung zur Datenbank
     */
    public List<DBTargetPoint> getTargetPointsWithinDistance(GeoPoint location,
            double maxDistance, TargetPointType[] targetPointTypes)
            throws ApplicationException;

    /**
     * Gibt den Zielpunkt passend zur ID.
     * @param targetPointID ID des Zielpunkts
     * @return Zielpunkt
     * @throws ApplicationException Problem beim Zugriff auf die Datenbank
     */
    public DBTargetPoint getTargetPoint(long targetPointID)
            throws ApplicationException;

    /**
     * Gibt alle Frage eines bestimmten Zielpunkts und einem bestimmten 
     * Schwierigkeitsgrad.
     * @param targetPointID Zielpunkt
     * @param levelOfDifficulty Schwierigkeitsgrad
     * @return Fragen
     */
    public List<DBQuestion> getAllQuestionsFor(long targetPointID);

    /**
     * Gibt alle Frage eines bestimmten Zielpunkts und einem bestimmten 
     * Schwierigkeitsgrad.
     * @param targetPointID Zielpunkt
     * @param language Sprache, in der die Frage gestellt werden soll
     * @param levelOfDifficulty Schwierigkeitsgrad
     * @param askedQuestionIDs IDs der bereits gestellten Fragen
     * @return Fragen
     */
    public List<DBQuestion> getAllQuestionsFor(long targetPointID,
            Language language, LevelOfDifficulty levelOfDifficulty,
            long[] askedQuestionIDs);

    /* ====================================================================== */
    /*                       Setter                                           */
    /* ====================================================================== */
    /**
     * Zielpunkt hinzufügen.
     * @param targetPoint Zielpunkt
     */
    public void addTargetPoint(DBTargetPoint targetPoint);

    /**
     * Fügt neue Zielpunkte hinzu und aktualisiert schon vorhandene.
     * @param targetPoint Zielpunkt
     */
    public String[] setTargetPoints(Collection<DBTargetPoint> targetPoints);

    /**
     * Fragen hinzufügen.
     * @param targetPointID Id des Zielpunktes
     * @param questions Fragen
     */
    public void addQuestions(Collection<DBQuestion> questions)
            throws ApplicationException;

    /* ====================================================================== */
    /*                       Update                                           */
    /* ====================================================================== */
    /**
     * Aktualisiert einen Zielpunkt. Dabei werden Fragen, die neue hinzugekommen
     * sind hinzugefügt, gelöschte Fragen gelöscht und Fragen und DBPosition
     * ebenfalls aktualisiert.
     * @param updateTargetPoint Zielpunkt
     * @param removedQuestions Frage die gelöscht wurden
     * @param removedInformations Informationen die gelöscht wurden
     * @throws ApplicationException bei Fehler bei der Synchronisation mit der 
     * Datenbank.
     */
    public void updateTargetPoint(DBTargetPoint updateTargetPoint,
            Collection<DBQuestion> removedQuestions, 
            Collection<DBInformation> removedInformations) 
            throws ApplicationException;

    /* ====================================================================== */
    /*                       Delete                                           */
    /* ====================================================================== */
    /**
     * Zielpunkt löschen.
     * @param targetPointID ID des Zielpunktes
     * @throws ApplicationException 
     */
    public void removeTargetPoint(long targetPointID) 
            throws ApplicationException;
}
