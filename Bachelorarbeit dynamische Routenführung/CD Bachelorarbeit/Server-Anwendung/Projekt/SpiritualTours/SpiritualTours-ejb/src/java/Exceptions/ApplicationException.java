package Exceptions;

/**
 * Anwendungsfehler.
 */
public class ApplicationException extends Exception {
    
    /**
     * Erzeugt eine Anwendungsfehler-Meldung.
     * @param s Fehlermeldung
     */
    public ApplicationException(String s) {
        super(s);
    }
}
