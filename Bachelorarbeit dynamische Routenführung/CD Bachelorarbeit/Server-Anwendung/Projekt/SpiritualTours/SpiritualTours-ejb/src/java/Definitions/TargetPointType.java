package Definitions;

import Exceptions.ApplicationException;

/**
 * Definition der Zielpunkt-Typen.
 */
public enum TargetPointType {

    /** Friedhof */
    CEMETERY,
    /** Kapelle */
    CHAPEL,
    /** Kirche */
    CHURCH,
    /** juedischer Friedhof */
    JEWISH_CEMETERY,
    /** Kloster */
    MONASTERY,
    /** Denkmal */
    MONUMENT,
    /** Standbild */
    STATUE,
    /** Wegekreuz */
    WAYSIDE_CROSS;
    /** Zielpunkt: Friedhöfe */
    private static final String TARGET_POINT_TYPE_CEMETERY = "Friedhof";
    /** Zielpunkt: Kapelle */
    private static final String TARGET_POINT_TYPE_CHAPEL = "Kapelle";
    /** Zielpunkt: Kirchen */
    private static final String TARGET_POINT_TYPE_CHURCH = "Kirche";
    /** Zielpunkt: juedische Friedhoefe */
    private static final String TARGET_POINT_TYPE_JEWISH_CEMETERY = "jüdischer Friedhof";
    /** Zielpunkt: Kloester */
    private static final String TARGET_POINT_TYPE_MONASTERY = "Kloster";
    /** Zielpunkt: Denkmaeler */
    private static final String TARGET_POINT_TYPE_MONUMENT = "Denkmal";
    /** Zielpunkt: Wegekreuze */
    private static final String TARGET_POINT_TYPE_STATUE = "Standbild";
    /** Zielpunkt: Wegekreuze */
    private static final String TARGET_POINT_TYPE_WAYSIDE_CROSS = "Wegekreuz";
    /** Zielpunkt: Wegekreuze */
    private static final String TARGET_POINT_TYPE_UNKNOWN = "unbekannt";

    /**
     * Gibt den Zielpunkt-Typ als String
     * @param targetPointType Zielpunkt-Typ
     * @return Zielpunkt-Typ
     */
    public static String getTargetPointType(TargetPointType targetPointType) {

        switch (targetPointType) {
            case CEMETERY:
                return TARGET_POINT_TYPE_CEMETERY;
            case CHAPEL:
                return TARGET_POINT_TYPE_CHAPEL;
            case CHURCH:
                return TARGET_POINT_TYPE_CHURCH;
            case JEWISH_CEMETERY:
                return TARGET_POINT_TYPE_JEWISH_CEMETERY;
            case MONASTERY:
                return TARGET_POINT_TYPE_MONASTERY;
            case MONUMENT:
                return TARGET_POINT_TYPE_MONUMENT;
            case STATUE:
                return TARGET_POINT_TYPE_STATUE;
            case WAYSIDE_CROSS:
                return TARGET_POINT_TYPE_WAYSIDE_CROSS;
            default:
                return TARGET_POINT_TYPE_UNKNOWN;
        }
    }

    /**
     * Gibt alle Namen der Zielpunkttypen als String-Array außer Start und 
     * Endpunkt.
     * @return Namen der Zielpunkttypen
     */
    public static String[] getTargetPointTypes() {
        return new String[]{
                    TARGET_POINT_TYPE_CEMETERY, TARGET_POINT_TYPE_CHAPEL,
                    TARGET_POINT_TYPE_CHURCH, TARGET_POINT_TYPE_JEWISH_CEMETERY,
                    TARGET_POINT_TYPE_MONASTERY, TARGET_POINT_TYPE_MONUMENT,
                    TARGET_POINT_TYPE_STATUE, TARGET_POINT_TYPE_WAYSIDE_CROSS
                };
    }

    /**
     * Gibt den Zielpunkttypen entsprechend zum Zielpunkttypnamen.
     * @param targetPointTypeName Zielpunkttypname
     * @return Zielpunkttyp
     * @throws ApplicationException bei unbekannten Zielpunkttypnamen
     */
    public static TargetPointType getTargetPointByName(
            String targetPointTypeName) throws ApplicationException {

        if (targetPointTypeName.equals(TARGET_POINT_TYPE_CEMETERY)) {
            return CEMETERY;
        } else if (targetPointTypeName.equals(TARGET_POINT_TYPE_CHURCH)) {
            return CHURCH;
        } else if (targetPointTypeName.equals(TARGET_POINT_TYPE_CHAPEL)) {
            return CHAPEL;
        } else if (targetPointTypeName.equals(TARGET_POINT_TYPE_JEWISH_CEMETERY)) {
            return JEWISH_CEMETERY;
        } else if (targetPointTypeName.equals(TARGET_POINT_TYPE_MONASTERY)) {
            return MONASTERY;
        } else if (targetPointTypeName.equals(TARGET_POINT_TYPE_MONUMENT)) {
            return MONUMENT;
        } else if (targetPointTypeName.equals(TARGET_POINT_TYPE_STATUE)) {
            return STATUE;
        } else if (targetPointTypeName.equals(TARGET_POINT_TYPE_WAYSIDE_CROSS)) {
            return WAYSIDE_CROSS;
        } else {
            throw new ApplicationException("unbekannter Zielpunkttyp.");
        }
    }
}
