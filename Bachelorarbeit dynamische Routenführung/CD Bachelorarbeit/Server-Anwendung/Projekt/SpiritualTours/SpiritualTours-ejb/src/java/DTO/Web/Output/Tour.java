package DTO.Web.Output;

import DTO.Web.Output.WayPoint.PointType;
import Utility.Geo;
import java.util.ArrayList;
import java.util.List;

/**
 * Repräsentiert eine Tour.
 */
public class Tour {

    /* ====================================================================== */
    /* Instanzvariablen */
    /* ====================================================================== */
    /** Typ der Tour */
    private TourType tourType;
    /** geschätzte Länge in km */
    private double estimatedTourLength = 0;
    /** geschätzte Dauer in Minuten */
    private int estimatedDuration = 0;
    /** Wegpunkte der Tour */
    private List<WayPoint> wayPoints;
    /** Wegbeschreibung. */
    private List<Instruction> instructions;

    /* ====================================================================== */
    /* Konstruktoren */
    /* ====================================================================== */
    /**
     * Erzeugt eine Tour.
     * @param tourType Typ der Tour
     * @param estimatedRouteLength geschätzte Länge in km
     * @param estimatedDuration geschätzte Dauer in Minuten
     * @param wayPoints Wegpunkt der Tour
     * @param instructions Wegbeschreibung
     */
    public Tour(TourType tourType,
            double estimatedRouteLength, int estimatedDuration,
            List<WayPoint> wayPoints, List<Instruction> instructions) {

        this.tourType = tourType;
        this.estimatedTourLength = estimatedRouteLength;
        this.estimatedDuration = estimatedDuration;
        this.wayPoints = wayPoints;
        this.instructions = instructions;
    }

    /**
     * Erzeugt eine neue Tour.
     */
    public Tour() {

        this.tourType = null;
        this.estimatedTourLength = 0;
        this.estimatedDuration = 0;
        this.wayPoints = new ArrayList<WayPoint>();
        this.instructions = new ArrayList<Instruction>();
    }

    /* ====================================================================== */
    /* Getter und Setter */
    /* ====================================================================== */
    /**
     * Gibt den Typ der Tour.
     * @return Typ der Tour
     */
    public TourType getTourType() {
        return tourType;
    }

    /**
     * Setzt den Typ der Tour.
     * @param tourType Typ der Tour
     */
    public void setTourType(TourType tourType) {
        this.tourType = tourType;
    }

    /* ---------------------------------------------------------------------- */
    /**
     * Gibt die geschätzte Länge in km.
     * @return geschätzte Länge in km
     */
    public double getEstimatedTourLength() {
        return estimatedTourLength;
    }

    /**
     * Setzt die geschätzte Länge in km.
     * 
     * @param estimatedRouteLength geschätzte Länge in km
     */
    public void setEstimatedTourLength(double estimatedTourLength) {
        this.estimatedTourLength = estimatedTourLength;
    }

    /**
     * Setzt die geschätzte Länge in km.
     */
    public void setEstimatedTourLength() {
        this.estimatedTourLength = 0;
        for (int i = 0; i < this.wayPoints.size() - 1; i++) {
            this.estimatedTourLength += Geo.getDistance(
                    this.wayPoints.get(i).getGeoPoint(),
                    this.wayPoints.get(i + 1).getGeoPoint());
        }
    }

    /* ---------------------------------------------------------------------- */
    /**
     * Gibt die geschätzte Dauer in Minuten.
     * @return geschätzte Dauer in Minuten
     */
    public int getEstimatedDuration() {
        return estimatedDuration;
    }

    /**
     * Setzt die geschätzte Dauer in Minuten.
     * 
     * @param estimatedDuration Dauer
     */
    public void setEstimatedDuration(int estimatedDuration) {
        this.estimatedDuration = estimatedDuration;
    }

    /**
     * Setzt die geschätzte Dauer in Minuten.
     * 
     * @param speed Geschwindigkeit in km/h
     */
    public void setEstimatedDuration(float speed) {
        this.estimatedDuration = (int) (this.estimatedTourLength / speed) * 60;
    }

    /* ---------------------------------------------------------------------- */
    /**
     * Gibt die Wegpunkte der Tour.
     * @return Wegpunkte der Tour
     */
    public List<WayPoint> getWayPoints() {
        return wayPoints;
    }

    /**
     * Gibt die Zielpunkte der Tour.
     * @return Zielpunkt
     */
    public List<TargetPoint> deliverTargetPointsOfTour() {
        List<TargetPoint> targetPoints = new ArrayList<TargetPoint>();
        for (WayPoint wayPoint : this.wayPoints) {
            if (wayPoint.getPointType().equals(PointType.TARGET_POINT)) {
                targetPoints.add((TargetPoint) wayPoint);
            }
        }
        return targetPoints;
    }

    /**
     * Setzt die Wegpunkte der Tour.
     * @param wayPoints Wegpunkte der Tour
     */
    public void setWayPoints(List<WayPoint> wayPoints) {
        this.wayPoints = wayPoints;
    }

    /**
     * Fügt einen Wegpunkt der Tour hinzu.
     * @param wayPoint Wegpunkt
     */
    public void addWayPoint(WayPoint wayPoint) {
        this.wayPoints.add(wayPoint);
    }

    /* ---------------------------------------------------------------------- */
    /**
     * Gibt die Wegbeschreibung der Tour.
     * @return Wegbeschreibung der Tour
     */
    public List<Instruction> getInstructions() {
        return instructions;
    }

    /**
     * Setzt die Wegbeschreibung der Tour.
     * @param instructions Wegbeschreibung der Tour
     */
    public void setInstructions(List<Instruction> instructions) {
        this.instructions = instructions;
    }

    /**
     * Fügt eine Anweisungen hinzu.
     * @param instruction Anweisung
     */
    public void addInstruction(Instruction instruction) {
        this.instructions.add(instruction);
    }

    /* ====================================================================== */
    /* toString */
    /* ====================================================================== */
    /**
     * Gibt die Tour als String.
     * @return Tour als String
     */
    @Override
    public String toString() {
        return "Tour-Typ: " + this.tourType
                + ", geschätzte Länge in km: " + this.estimatedTourLength
                + ", geschätzte Dauer in Minuten: " + this.estimatedDuration
                + ", Wegpunkte: " + this.wayPoints
                + ", Wegbeschreibung: " + this.instructions;
    }

    /* ====================================================================== */
    /* Hilfsklassen */
    /* ====================================================================== */
    /**
     * Tour-Typen. 
     */
    public enum TourType {

        ROUTE, FLATLY, PILGRIMAGE;
    }
}
