package Exceptions;

/**
 * Fehler: keine Zielpunkte vorhanden.
 */
public class NoTargetPointsFoundException extends Exception {
    
    /**
     * Erzeugt eine NoTargetPointsFoundException.
     * @param s Fehlermeldung
     */
    public NoTargetPointsFoundException(String s) {
        super(s);
    }
}