package DTO.Web.Input;

import DTO.GeoPoint;
import Definitions.Language;
import Definitions.TargetPointType;

/**
 * Einstellungen der Tour.
 */
public class TourSettings {

    /* ====================================================================== */
    /* Konstanten */
    /* ====================================================================== */
    /** Durchschnittliche Geschwindigkeit eines Fußgängers (km/h). */
    private static final float SPEED_HOOF_IT = 3.75f;
    /** Durchschnittliche Geschwindigkeit eines Fahrradfahrers (km/h). */
    private static final float SPEED_BIKE = 15;
    /** Durchschnittliche Geschwindigkeit eines Autofahrers (km/h). */
    private static final float SPEED_CAR = 50;
    
    /* ====================================================================== */
    /* Instanzvariablen */
    /* ====================================================================== */
    /** Start-Position */
    private GeoPoint startPoint;
    /** Wie ist man unterwegs? zu Fuß, mit dem Fahrrad, mit dem Auto */
    private Locomotion locomotion;
    /** Wie gut ist man unterwegs? langsam, normal, schnell */
    private HowGood howGood;
    /** Wie lange unterwegs? Angabe in Minuten */
    private int howLong;
    /** Was will man sehen */
    private TargetPointType[] targetPointTypes;
    /** Sprache. */
    private Language language;
    /** Info, ob der User den Schnitzeljagd-Modus aktiviert hat. */
    private boolean isPaperChase;

    /* ====================================================================== */
    /* Konstruktoren */
    /* ====================================================================== */
    /**
     * Erzeugt ein Einstellungsobjekt zur Tour.
     */
    public TourSettings() {
    }

    /**
     * Erzeugt ein Einstellungsobjekt zur Tour.
     * @param startPoint Start-Position
     * @param locomotion Art der Fortbewegung (zu Fuß, mit Fahrrad, mit Auto)
     * @param howGood wie gut ist man unterwegs
     * @param howLong wie lange will man unterwegs sein
     * @param targetPointTypes welche Arten von Zielpunkten will man sehen
     * @param language Sprache
     * @param isPaperChase true, wenn Schnitzeljagd-Modes aktiviert
     */
    public TourSettings(GeoPoint startPoint, Locomotion locomotion,
            HowGood howGood, int howLong, TargetPointType[] targetPointTypes,
            Language language, boolean isPaperChase) {

        this.startPoint = startPoint;
        this.locomotion = locomotion;
        this.howGood = howGood;
        this.howLong = howLong;
        this.targetPointTypes = targetPointTypes;
        this.language = language;
    }

    /* ====================================================================== */
    /* Getter und Setter */
    /* ====================================================================== */
    /**
     * Gibt die Start-Position.
     * @return Start-Position
     */
    public GeoPoint getStartPoint() {
        return startPoint;
    }

    /**
     * Setzt die Start-Position.
     * @param startPoint Start-Position
     */
    public void setStartPoint(GeoPoint startPoint) {
        this.startPoint = startPoint;
    }

    /* ---------------------------------------------------------------------- */
    /**
     * Gibt: Wie ist man unterwegs? zu Fuß, mit dem Fahrrad, mit dem Auto
     * @return Wie ist man unterwegs? zu Fuß, mit dem Fahrrad, mit dem Auto
     */
    public Locomotion getLocomotion() {
        return locomotion;
    }

    /**
     * Setzt: Wie ist man unterwegs? zu Fuß, mit dem Fahrrad, mit dem Auto
     * @param howGood Wie ist man unterwegs? zu Fuß, mit dem Fahrrad, mit Auto
     */
    public void setLocomotion(Locomotion locomotion) {
        this.locomotion = locomotion;
    }

    /* ---------------------------------------------------------------------- */
    /**
     * Gibt: Wie gut ist man unterwegs? langsam, normal, schnell
     * @return Wie gut ist man unterwegs? langsam, normal, schnell
     */
    public HowGood getHowGood() {
        return howGood;
    }

    /**
     * Setzt: Wie gut ist man unterwegs? langsam, normal, schnell
     * @param howLong Wie gut ist man unterwegs? langsam, normal, schnell
     */
    public void setHowGood(HowGood howGood) {
        this.howGood = howGood;
    }

    /* ---------------------------------------------------------------------- */
    /**
     * Gibt: Wie lange unterwegs? Angabe in Minuten
     * @return Wie lange unterwegs? Angabe in Minuten
     */
    public int getHowLong() {
        return howLong;
    }

    /**
     * Setzt: Wie lange unterwegs? Angabe in Minuten
     * @param Wie lange unterwegs? Angabe in Minuten
     */
    public void setHowLong(int howLong) {
        this.howLong = howLong;
    }

    /* ---------------------------------------------------------------------- */
    /**
     * Gibt: Was will man sehen
     * @return Was will man sehen
     */
    public TargetPointType[] getTargetPointTypes() {
        return targetPointTypes;
    }

    /**
     * Setzt: Was will man sehen
     * @param targetPointTypes Was will man sehen
     */
    public void setTargetPointTypes(TargetPointType[] targetPointTypes) {
        this.targetPointTypes = targetPointTypes;
    }

    /* ---------------------------------------------------------------------- */
    /**
     * Gibt die Sprache.
     * @return Sprache
     */
    public Language getLanguage() {
        return language;
    }

    /**
     * Setzt die Sprache.
     * @param language Sprache
     */
    public void setLanguage(Language language) {
        this.language = language;
    }

    /* ---------------------------------------------------------------------- */
    /**
     * Setzt, ob Schnitzeljagd gewünscht oder nicht.
     * @param Schnitzjagd ? ja (true) oder nein (false)
     */
    public void setIsPaperChase(boolean isPaperChase) {
        this.isPaperChase = isPaperChase;
    }

    /**
     * Gibt an, ob Schnitzeljagd gewünscht ist oder nicht.
     * @return Schnitzjagd? ja (true) oder nein (false)
     */
    public boolean getIsPaperChase() {
        return this.isPaperChase;
    }
    
    /* ---------------------------------------------------------------------- */
    /**
     * Liefert die Geschwindigkeit, je nach dem, wie man unterwegs ist.
     * @param locomotion Art der Fortbewegung
     * @return Geschwindigkeit
     */
    public float getSpeed() {
        return this.locomotion.equals(Locomotion.HOOF_IT)
                ? SPEED_HOOF_IT : this.locomotion.equals(Locomotion.BIKE)
                ? SPEED_BIKE : SPEED_CAR;
    }

    /* ====================================================================== */
    /* toString */
    /* ====================================================================== */
    /**
     * Gibt die Tour-Einstellungen als String.
     * @return Tour-Einstellungen als String
     */
    @Override
    public String toString() {
        return "GeoPosition: " + this.startPoint.toString() + ", "
                + "Wie: " + this.locomotion + ", "
                + "Wie gut: " + this.howGood + ", "
                + "Wie lange: " + this.howLong + " Minuten, "
                + "Was sehen: " + getTargetPointTypeNames()
                + "Sprache: " + this.language;
    }
    
    /**
     * Gibt die gewünschten Zielpunkttypen als String.
     * @return Zielpunkttypen
     */
    private String getTargetPointTypeNames() {
        String targetPointTypeNames = this.targetPointTypes.length == 0 
                ? "" : this.targetPointTypes[0].name();
        for(int i = 1; i < this.targetPointTypes.length; i++) {
            targetPointTypeNames = ", " + this.targetPointTypes[i].name();
        }
        return targetPointTypeNames;
    }

    /* ====================================================================== */
    /* Hilfsklassen */
    /* ====================================================================== */
    /**
     * Wie ist man unterwegs.
     */
    public enum Locomotion {

        HOOF_IT, BIKE, CAR;
    }

    /**
     * Wie gut ist man unterwegs.
     */
    public enum HowGood {

        SLOW, NORMAL, FAST;
    }
}
