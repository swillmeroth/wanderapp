package DTO.XML;

import DTO.Database.DBQuestion;
import DTO.Database.DBTargetPoint;
import DTO.Database.DBInformation;
import Definitions.TargetPointType;
import java.util.ArrayList;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

/**
 * Definiert einen Zielpunkt. Zugehörige Informationen sind Name des Objekts, 
 * die Art des Objekts, die XMLPosition, und eine Beschreibung.
 */
@Root(name="targetPoint")
public class XMLTargetPoint implements Comparable<XMLTargetPoint> {

    /* ====================================================================== */
    /*                       Instanzvariablen                                 */
    /* ====================================================================== */
    /** Id des Zielpunktes. */
    @Attribute
    private long id;
    /** Name des Zielpunkts. */
    @Element
    private String name;
    /** Art des Zielpunkts. */
    @Attribute(name="type")
    private TargetPointType targetPointType;
    /** Informationen zum Zielpunkt. */
    @ElementList
    private ArrayList<XMLInformation> informations;
    /** Postion des Zielpunkts. */
    @Element
    private XMLPosition position;
    /** Reichweite in Metern um den Zielpunkt herum. */
    @Element(name="range")
    private int range;
    /** Zum Zielpunkt gehörende Fragen */
    @ElementList
    private ArrayList<XMLQuestion> questions;

    /* ====================================================================== */
    /*                       Konstruktoren                                    */
    /* ====================================================================== */
    /**
     * Erzeugt einen Zielpunkt.
     * @param id ID
     * @param name Name des Zielpunkts
     * @param targetPointType Art des Zielpunkts
     * @param position XMLPosition des Zielpunkts
     * @param information Info zum Zielpunkt
     * @param range Reichweite in Metern um den Zielpunkt herum
     * @param questions Fragen des Zielpunkts für den Schnitzeljagd-Modus
     */
    public XMLTargetPoint(long id, String name, TargetPointType targetPointType,
            ArrayList<XMLInformation> informations, XMLPosition position, 
            int range, ArrayList<XMLQuestion> questions) {
        this.id = id;
        this.name = name;
        this.targetPointType = targetPointType;
        this.informations = informations;
        this.position = position;
        this.range = range;
        this.questions = questions;
    }
    
    /**
     * Erzeugt einen Zielpunkt.
     * @param name Name des Zielpunkts
     * @param targetPointType Art des Zielpunkts
     * @param position XMLPosition des Zielpunkts
     * @param information Info zum Zielpunkt
     * @param range Reichweite in Metern um den Zielpunkt herum
     * @param questions Fragen des Zielpunkts für den Schnitzeljagd-Modus
     */
    public XMLTargetPoint(DBTargetPoint targetPoint) {
        this.id = targetPoint.getId();
        this.name = targetPoint.getName();
        this.targetPointType = targetPoint.getTargetPointType();
        this.informations = new ArrayList<XMLInformation>();
        for (DBInformation information : 
                targetPoint.getInformations()) {
            this.informations.add(new XMLInformation(information));
        }
        this.position = new XMLPosition(targetPoint.getPosition());
        this.range = targetPoint.getRange();
        this.questions = new ArrayList<XMLQuestion>();
        for (DBQuestion question : targetPoint.getQuestions()) {
            this.questions.add(new XMLQuestion(question));
        }          
    }

    /**
     * Leerer Konstruktor.
     */
    public XMLTargetPoint() {
    }

    /* ====================================================================== */
    /*                       Getter & Setter                                  */
    /* ====================================================================== */
    /**
     * Gibt die ID der Frage.
     * @return ID der Frage
     */
    public long getId() {
        return id;
    }

    /**
     * Setzt die ID der Frage.
     * @param ID der Frage
     */
    public void setId(long id) {
        this.id = id;
    }
    
    /* ---------------------------------------------------------------------- */
    /**
     * Gibt den Namen des Zielpunkts.
     * @return Namen des Zielpunkts.
     */
    public String getName() {
        return name;
    }

    /**
     * Setzt den Namen des Zielpunkts.
     * @param Namen des Zielpunkts.
     */
    public void setName(String name) {
        this.name = name;
    }

    /* ---------------------------------------------------------------------- */
    /**
     * Gibt den Typen des Zielpunkts.
     * @return Typ des Zielpunkts.
     */
    public TargetPointType getTargetPointType() {
        return targetPointType;
    }

    /**
     * Setzt den Typen des Zielpunkts.
     * @param Typ des Zielpunkts.
     */
    public void setTargetPointType(TargetPointType targetPointType) {
        this.targetPointType = targetPointType;
    }
    
    /* ---------------------------------------------------------------------- */
    /**
     * Gibt die Informationen des Zielpunkts.
     * @return Informationen des Zielpunkts.
     */
    public ArrayList<XMLInformation> getInformations() {
        return informations;
    }

    /**
     * Setzt die Informationen des Zielpunkts.
     * @param Informationen des Zielpunkts.
     */
    public void setInformation(
            ArrayList<XMLInformation> informations) {
        this.informations = informations;
    }

    /* ---------------------------------------------------------------------- */
    /**
     * Gibt die XMLPosition des Zielpunkts.
     * @return XMLPosition des Zielpunkts.
     */
    public XMLPosition getPosition() {
        return position;
    }

    /**
     * Setzt die XMLPosition des Zielpunkts.
     * @param XMLPosition des Zielpunkts.
     */
    public void setPosition(XMLPosition position) {
        this.position = position;
    }

    /* ---------------------------------------------------------------------- */
    /**
     * Gibt die Reichweite um den Zielpunkt herum in Meter an.
     * 
     * @return Reichweite
     */
    public int getRange() {
        return range;
    }
    
    /**
     * Setzt die Reichweite um den Zielpunkt herum in Meter.
     * 
     * @param range Reichweite
     */
    public void setRange(int range) {
        this.range = range;
    }
    
    /* ---------------------------------------------------------------------- */
    /**
     * Gibt die Fragen zum Zielpunkts.
     * @return Fragen zum Zielpunkts.
     */
    public ArrayList<XMLQuestion> getQuestions() {
        return questions;
    }

    /**
     * Setzt die Fragen zum Zielpunkts.
     * @param Fragen zum Zielpunkts.
     */
    public void setQuestions(ArrayList<XMLQuestion> questions) {
        this.questions = questions;
    }

    /* ====================================================================== */
    /*                       toString, ...                                    */
    /* ====================================================================== */
    /**
     * Prüft, ob der Zielpunkt vollständig initialisiert wurde.
     * @return true, wenn Zielpunkt vollständig initialisiert ist
     */
    public boolean isTargetPointCompletelyInitialized() {
        return this.name != null && this.targetPointType != null
                && this.position != null && this.informations != null  
                && this.position.getStreet() != null && this.questions != null
                && this.questions.size() > 0;
    }

    /**
     * Name des Zielpunktes.
     * @return Name des Zielpunktes
     */
    @Override
    public String toString() {
        return this.name;
    }

    /**
     * Objekt als String, mit allen interessanten Informationen. (Name des 
     * Zielpunkts, Typ, XMLPosition, Fragen, Informationen & Reichweite)
     * @return Objekt als String
     */
    public String toStringCompleteInformation() {
        return "Name: " + name + ",\n"
                + "Typ: " + TargetPointType.getTargetPointType(this.targetPointType) + ",\n"
                + "Information: " + informations + ",\n"
                + "Postion: " + position.toString() + ",\n"
                + "Reichweite: " + range + "m,\n"
                + "Fragen: " + getQuestionsToString();
    }

    /**
     * Objekt als String, mit allen interessanten Informationen, außer den 
     * Fragen. (Name des Zielpunkts, Typ, XMLPosition, Informationen & Reichweite)
     * @return Objekt als String
     */
    public String toStringWithoutQuestions() {
        return "Name: " + name + ",\n"
                + "Typ: " + TargetPointType.getTargetPointType(this.targetPointType) + ",\n"
                + "Information: " + informations  + ",\n"
                + "Postion: " + position.toString() + ",\n"
                + "Reichweite: " + range + "m";
    }

    /**
     * Fragen zusammenstellen.
     * @return Fragen als ein String.
     */
    private String getQuestionsToString() {
        String allQuestions = "";
        for (XMLQuestion question : this.questions) {
            allQuestions = allQuestions + "\n" + question.toString();
        }
        return allQuestions;
    }

    /**
     * Vergleicht den Namen des Zielpunkts.
     * @param targetPoint Zielpunkt mit dem verglichen werden soll
     * @return -1 von Name vor übergebenen Namen liegt, 0 wenn gleich1 wenn 
     * übergebener Name vor Name liegt
     */
    @Override
    public int compareTo(XMLTargetPoint targetPoint) {
        return this.name.compareTo(targetPoint.name);
    }
}
