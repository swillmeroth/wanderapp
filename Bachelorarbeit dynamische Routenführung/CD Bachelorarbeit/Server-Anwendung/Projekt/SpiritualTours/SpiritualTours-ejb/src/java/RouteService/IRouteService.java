
package RouteService;

import DTO.Web.Input.TourSettings.HowGood;
import DTO.Web.Input.TourSettings.Locomotion;
import DTO.Web.Output.Tour;
import DTO.Web.Output.Tour.TourType;
import DTO.Web.Output.WayPoint;
import Definitions.Language;
import Exceptions.ConnectionException;
import java.util.List;

/**
 * Erstellt den genauen Weg zwischen den Zielpunkten per Abfrage an einen 
 * Service.
 */
public interface IRouteService {
    
    /**
     * Erstellt eine Route durch Anfrage an Open Route Service.
     * @param targetPoints Zielpunkte
     * @param tourType Tour-Typ
     * @param locomotion Wie ist man unterwegs
     * @param howGood Wie gut ist man unterwegs
     * @param language Sprache der Wegbeschreibung (de=deutsch, en=englisch, 
     * nl=niederländisch,...)
     * @param residenceTimeAtTargetPoint Aufenthaltszeit an den Zielpunkten
     * @throws ConnectionException bei Verbindungsfehler mit dem Route Service
     */
    public Tour getTour(List<WayPoint> targetPoints, TourType tourType, 
            Locomotion locomotion, HowGood howGood, Language language, 
            int residenceTimeAtTargetPoint) throws ConnectionException;
}
