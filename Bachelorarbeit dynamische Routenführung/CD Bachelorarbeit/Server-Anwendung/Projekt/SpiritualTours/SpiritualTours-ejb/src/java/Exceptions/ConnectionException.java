
package Exceptions;

/**
 * Repräsentiert einen Verbindungsfehler.
 */
public class ConnectionException extends Exception {
    
    /**
     * Erzeugt eine Verbindungsfehler-Meldung.
     * @param s Fehlermeldung
     */
    public ConnectionException(String s) {
        super(s);
    }
}
