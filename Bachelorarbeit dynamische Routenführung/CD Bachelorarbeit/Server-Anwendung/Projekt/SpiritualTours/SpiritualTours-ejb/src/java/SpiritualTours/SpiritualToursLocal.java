package SpiritualTours;

import DTO.GeoPoint;
import DTO.Web.Output.Question;
import DTO.Web.Output.Tour;
import Definitions.LevelOfDifficulty;
import DTO.Web.Input.TourSettings;
import Definitions.Language;
import Exceptions.ConnectionException;
import Exceptions.NoQuestionFoundException;
import Exceptions.NoTargetPointsFoundException;
import java.util.List;
import javax.ejb.Local;

/**
 * Schnittstelle des EJB-Containers nach außen für mobile Geräte, zur Erstellung
 * einer Tour und zur Übergabe von Fragen.
 */
@Local
public interface SpiritualToursLocal {

    /**
     * Liefert die Touren.
     * @param settings Einstellungen
     * @return Touren
     * @throws NoTargetPointsFoundException keine Zielpunkte vorhanden
     * @throws ConnectionException bei Verbindungsfehler mit dem Route Service
     */
    public List<Tour> getTours(TourSettings settings)
            throws NoTargetPointsFoundException, ConnectionException;
    
    /**
     * Liefert die Route.
     * @param settings Einstellungen
     * @param targetPoint Zielpunkt
     * @return Route
     * @throws ConnectionException bei Verbindungsfehler mit dem Route Service
     */
    public Tour getRoute(TourSettings settings, GeoPoint targetPoint)
            throws ConnectionException;

    /**
     * Liefert eine Frage eines Zielpunktes unter Berücksichtigung des 
     * gewünschten Schwierigkeitsgrad. Aus den möglichen Fragen wird eine
     * Frage zufällig bestimmt. Existiert keine Frage zum angegebenen 
     * Schwierigkeitsgrad wird eine Frage aus dem nächst leichteren 
     * Schwierigkeitsgrad gewählt. (Außer es war bereits die leichteste 
     * Kategorie.)
     * @param targetPointID ID des Zielpunkts
     * @param language Sprache, in der die Frage gestellt werden soll
     * @param levelOfDifficulty gewünschter Schwierigkeitsgrad.
     * @param askedQuestionIDs IDs der bereits gestellten Fragen
     * @return Frage.
     */
    public Question getQuestion(int targetPointID, Language language, 
            LevelOfDifficulty levelOfDifficulty, long[] askedQuestionIDs)
            throws NoQuestionFoundException;
}
