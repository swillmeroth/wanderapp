package SpiritualTours;

import DAO.DataAccessObjectLocal;
import DTO.Database.DBTargetPoint;
import DTO.GeoPoint;
import DTO.Web.Output.Tour;
import DTO.Web.Output.Tour.TourType;
import Definitions.LevelOfDifficulty;
import Exceptions.ApplicationException;
import DTO.Web.Input.TourSettings;
import DTO.Web.Input.TourSettings.HowGood;
import DTO.Web.Output.Question;
import DTO.Web.Output.TargetPoint;
import DTO.Web.Output.WayPoint;
import DTO.Web.Output.WayPoint.PointType;
import Definitions.Language;
import Utility.Geo;
import Exceptions.ConnectionException;
import Exceptions.NoQuestionFoundException;
import Exceptions.NoTargetPointsFoundException;
import RouteService.IRouteService;
import RouteService.OpenRouteService.OpenRouteService;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import org.xml.sax.SAXException;

/**
 * Schnittstelle des EJB-Containers nach außen für mobile Geräte, zur Erstellung
 * einer Tour und zur Übergabe von Fragen. Der Algorithmus zur Erstellung des 
 * Rundeweges (getTourFromAtoA(...)) wurde von der Universität übernommen und 
 * angepasst (Link: http://www.informatik.uni-trier.de/~naeher/Professur/
 * PROJECTS/SS07/gruppe4/).
 */
@Stateless
public class SpiritualToursBean implements Serializable, SpiritualToursLocal {

    /* ====================================================================== */
    /*                       Konstanten                                       */
    /* ====================================================================== */
    /** Faktor für: Wie gut ist man unterwegs? langsam */
    private static final float FACTOR_HOW_GOOD_SLOW = 0.75f;
    /** Faktor für: Wie gut ist man unterwegs? normal */
    private static final float FACTOR_HOW_GOOD_NORMAL = 1.0f;
    /** Faktor für: Wie gut ist man unterwegs? schnell */
    private static final float FACTOR_HOW_GOOD_FAST = 1.5f;
    /** 
     * Durchschnittliche Verweildauer an einem Zielpunkt in Minuten bei einer 
     * Schnitzeljagd. 
     */
    private static final int DWELL_PAPERCHASE = 3;
    /** Durchschnittliche Verweildauer an einem Zielpunkt in Minuten */
    private static final int DWELL = 2;
    /** Fehlermeldung: Keine Zielpunkte vorhanden. */
    private static final String ERROR_MSG_NO_TARGETPOINTS = "Keine Zielpunkte "
            + "vorhanden.";
    /** Fehlermeldung: Keine Zielpunkte vorhanden. */
    private static final String ERROR_MSG_NO_QUESTION = "Keine Frage "
            + "vorhanden.";
    /* ====================================================================== */
    /*                       Instanzvariablen                                 */
    /* ====================================================================== */
    /** Schnittstelle zur Datenbank. */
    @EJB
    private DataAccessObjectLocal dao;
    /** Serice, um die Route zu erhalten. */
    private IRouteService routeService;

    /* ====================================================================== */
    /*                       getTours                                         */
    /* ====================================================================== */
    /**
     * Liefert die Route.
     * @param settings Einstellungen
     * @return Touren
     * @throws NoTargetPointsFoundException keine Zielpunkte vorhanden
     * @throws ConnectionException bei Verbindungsfehler mit dem Route Service
     */
    @Override
    public List<Tour> getTours(TourSettings settings)
            throws NoTargetPointsFoundException, ConnectionException {

        /* Liste der Touren */
        List<Tour> tours = new ArrayList<Tour>();
        /* ungefähre Geschwindigkeit der Nutzers ermitteln */
        float speed = settings.getSpeed();
        /* Strecke in km, die der User zurücklegen möchte */
        float dinstaceForTour = getMaxDinstaceForTour(settings, speed);

        /* in Frage kommende Zielpunkt (Distanz zur akt. Pos. -> Zielpunkt) */
        List<TargetPoint> eligibleTargetPoints = null;
        try {
            eligibleTargetPoints = convert2OutputTargetPoints(
                    dao.getTargetPointsWithinDistance(settings.getStartPoint(),
                    dinstaceForTour, settings.getTargetPointTypes()),
                    settings.getLanguage());
        } catch (ApplicationException ex) {
            Logger.getLogger(SpiritualToursBean.class.getName()).log(Level.SEVERE, null, ex);
            throw new NoTargetPointsFoundException(ERROR_MSG_NO_TARGETPOINTS);
        }

        /* Wenn keine Zielpunkte vorhanden sind, gibt es keine Tour. */
        if (eligibleTargetPoints.isEmpty()) {
            throw new NoTargetPointsFoundException(ERROR_MSG_NO_TARGETPOINTS);
        }

        /* Route Service erzeugen */
        try {
            this.routeService = new OpenRouteService();
        } catch (SAXException ex) {
            throw new ConnectionException(ex.getMessage());
        }

        /* Tour vom Typ Route erzeugen. */
        tours.add(getTourRoute(dinstaceForTour, speed, settings,
                eligibleTargetPoints));

        /* Tour vom Typ Rundweg erzeugen. */
        tours.add(getTourFlatly(dinstaceForTour, speed, settings,
                eligibleTargetPoints));

        return tours;
    }

    /**
     * Berechnet die maximale Distanz anhand der Art der Fortbewegung, wie gut 
     * man unterwegs ist und wie lange man unterwegs sein möchte.
     * @param settings Einstellungen, in denen die Informationen stecken
     * @return maximal Distanz
     */
    private float getMaxDinstaceForTour(TourSettings settings, float speed) {
        return settings.getHowLong() / 60f * speed;
    }

    /**
     * Konvertiert die von der Datenbank erhaltenen Zielpunkte in Zielpunkte,
     * die außerhalb der Enterprise-Application verarbeitet werden.
     * @param targetPoints Zielpunkte von der Datenbank
     * @return Zielpunkte für die "Außenwelt"
     */
    private List<TargetPoint> convert2OutputTargetPoints(
            List<DBTargetPoint> targetPoints, Language language) {

        List<TargetPoint> convertTargetPoints = new ArrayList<TargetPoint>();
        for (DBTargetPoint targetPoint : targetPoints) {
            convertTargetPoints.add(database2OutputTargetPoint(targetPoint,
                    language));
        }
        return convertTargetPoints;
    }

    /**
     * Konvertiert ein Database-TargetPoint-Objekt in ein Output-TargetPoint-
     * Objekt.
     * @param targetPoint Database-TargetPoint-Objekt
     * @param language Sprache in der die Inforamtion sein soll
     * @return Output-TargetPoint-Objekt
     */
    private TargetPoint database2OutputTargetPoint(
            DBTargetPoint targetPoint, Language language) {

        return new TargetPoint(0,
                targetPoint.getRange(),
                new GeoPoint(
                targetPoint.getPosition().getLatitude(),
                targetPoint.getPosition().getLongitude(),
                targetPoint.getPosition().getAltitude()),
                targetPoint.getPosition().getPostcode(),
                targetPoint.getPosition().getTown(),
                targetPoint.getPosition().getStreet(),
                targetPoint.getId(),
                targetPoint.getName(),
                targetPoint.getTargetPointType(),
                targetPoint.getInformationString(language));
    }

    /* ====================================================================== */
    /*                       Route                                            */
    /* ====================================================================== */
    /**
     * Liefert eine Tour von A nach B
     * @param distance Distanz, die zurückgelegt werden soll
     * @param speed Geschwindigkeit
     * @param settings Einstellungen der Tour
     * @param targetPoints infrage kommende Zielpunkte
     * @return Tour von A nach B
     * @throws ConnectionException bei Verbindungsfehler mit dem Route Service
     */
    private Tour getTourRoute(float maxDistance, float speed,
            TourSettings settings, List<TargetPoint> eligibleTargetPoints)
            throws ConnectionException {

        /* aktuelle Dauer der Tour */
        int requiredTime = 0;
        /* aktuelle Distanz */
        double totalDistance = 0;
        /* Zielpunkte */
        List<WayPoint> targetPoints = new ArrayList<WayPoint>();
        /* Startposition ist erster Zielpunkt */
        targetPoints.add(new WayPoint(1, settings.getStartPoint(), PointType.START_POINT));

        /* Zielpunktliste erstellen, bis Max-Distanz erreicht oder keine 
         * Zielpunkte mehr vorhanden sind */
        while (requiredTime <= settings.getHowLong()
                && targetPoints.size() < (eligibleTargetPoints.size() + 1)) {

            /* nächsten Zielpunkt finden */
            TargetPoint nextTargetPoint = null;
            double minDistance = Integer.MAX_VALUE;
            double realDistance = maxDistance;

            for (TargetPoint targetPoint : eligibleTargetPoints) {

                double currentDistance = Geo.getDistance(
                        targetPoints.get(targetPoints.size() - 1).getGeoPoint(),
                        targetPoint.getGeoPoint());

                if (!targetPoints.contains(targetPoint)
                        && currentDistance < minDistance) {
                    minDistance = currentDistance;
                    realDistance = currentDistance;
                    nextTargetPoint = targetPoint;
                }
            }

            /* nächster Zielpunkt wird hinzugefügt */
            targetPoints.add(nextTargetPoint);
            /* Gesamtdistanz bestimmen */
            totalDistance += realDistance;

            /* benötigte Zeit für die Tour bestimmen */
            requiredTime = calcualteRequiredMinutes(settings.getIsPaperChase(),
                    totalDistance, speed, targetPoints.size());
        }

        /* Verweilzeit an den jeweiligen Zielpunkten bestimmen */
        int residenceTimeAtTargetPoint = (targetPoints.size() - 1)
                * (settings.getIsPaperChase() ? DWELL_PAPERCHASE : DWELL);

        return this.routeService.getTour(targetPoints, TourType.ROUTE,
                settings.getLocomotion(), settings.getHowGood(),
                settings.getLanguage(), residenceTimeAtTargetPoint);
    }

    /* ====================================================================== */
    /*                       Rundweg                                          */
    /* ====================================================================== */
    /**
     * Liefert eine Tour von A nach A
     * @param maxDistance Distanz, die zurückgelegt werden soll
     * @param speed Geschwindigkeit
     * @param settings Einstellungen der Tour
     * @param targetPoints infrage kommende Zielpunkte
     * @return Tour von A nach A
     * @throws ConnectionException bei Verbindungsfehler mit dem Route Service
     */
    private Tour getTourFlatly(float maxDistance, float speed,
            TourSettings settings, List<TargetPoint> eligibleTargetPoints)
            throws ConnectionException {

        /* true, wenn die gewünschte Dauer der Tour erreicht ist */
        boolean isHowLongReached = false;
        /* Zielpunkte */
        List<WayPoint> targetPoints = new ArrayList<WayPoint>();
        /* Startposition ist erster Zielpunkt */
        targetPoints.add(new WayPoint(1, settings.getStartPoint(), PointType.START_POINT));

        /* Bei nur einem Zielpunkt, steht die Tour fest. */
        if (eligibleTargetPoints.size() <= 1) {

            targetPoints.add(eligibleTargetPoints.get(0));

        } else {

            /* Ausführen des Algorithmus Nearest Insertion */
            for (int i = 0; i < eligibleTargetPoints.size() && !isHowLongReached; i++) {

                double minDistance = maxDistance;
                double newMinDistance = maxDistance;
                int beforeTargetPointIndex = 0;
                TargetPoint newTargetPoint = null;

                /* Suche des nächsten Zielpunkts */
                for (WayPoint wayPoint : targetPoints) {
                    GeoPoint pos = wayPoint.getGeoPoint();

                    for (TargetPoint targetPoint : eligibleTargetPoints) {
                        GeoPoint pos2 = targetPoint.getGeoPoint();

                        if (!targetPoints.contains(targetPoint)
                                && Geo.getDistance(pos, pos2)
                                < minDistance) {
                            minDistance = Geo.getDistance(pos, pos2);
                            newTargetPoint = targetPoint;
                        }
                    }
                }

                /* Einfügenposition des Zielpunktes ermitteln */
                for (int a = 0; a < targetPoints.size() - 1; a++) {

                    GeoPoint p = targetPoints.get(a).getGeoPoint();
                    GeoPoint q = targetPoints.get(a + 1).getGeoPoint();
                    double distance =
                            Geo.getDistance(newTargetPoint.getGeoPoint(), p)
                            + Geo.getDistance(newTargetPoint.getGeoPoint(), q)
                            - Geo.getDistance(p, q);

                    if (distance < newMinDistance) {
                        newMinDistance = distance;

                        beforeTargetPointIndex = a;
                    }
                }

                /* Zielpunkt einfügen */
                targetPoints = insertTargetPoint(targetPoints,
                        newTargetPoint, beforeTargetPointIndex + 1);

                /* Prüft, ob gewünschte Dauer der Tour bereits erreicht ist */
                isHowLongReached = settings.getHowLong()
                        <= getMinutesForTour(settings.getIsPaperChase(),
                        targetPoints, speed);
            }
        }
        /* Endpunkt hinzufügen */
        targetPoints.add(new WayPoint(1, settings.getStartPoint(), PointType.END_POINT));

        /* Verweilzeit an den jeweiligen Zielpunkten bestimmen */
        int residenceTimeAtTargetPoint = (targetPoints.size() - 2)
                * (settings.getIsPaperChase() ? DWELL_PAPERCHASE : DWELL);

        /* Tour erzeugen */
        return this.routeService.getTour(targetPoints, TourType.FLATLY,
                settings.getLocomotion(), settings.getHowGood(),
                settings.getLanguage(), residenceTimeAtTargetPoint);
    }

    /**
     * Berechnet die Zeit in Minuten, die für die Tour benötigt werden.
     * @param tour Tour
     * @param speed Geschwindigkeit
     * @return Distanz zwischen dem letzten Zielpunkt und der Start- / 
     * Endposition
     */
    private int getMinutesForTour(boolean isPaperChase,
            List<WayPoint> targetPoints, float speed) {

        double distance = 0;

        for (int i = 0; i < targetPoints.size() - 1; i++) {
            distance += Geo.getDistance(targetPoints.get(i).getGeoPoint(),
                    targetPoints.get(i + 1).getGeoPoint());
        }
        return calcualteRequiredMinutes(isPaperChase, distance, speed,
                targetPoints.size());
    }

    /**
     * Fügt einen Zielpunkt in die Wegpunkt-Liste an einer bestimmten Stelle 
     * ein.
     * @param wayPoints Wegpunkte
     * @param targetPoint Zielpunkt
     * @param beforeTargetPointIndex
     * @return Wegpunkt-Liste
     */
    private List<WayPoint> insertTargetPoint(List<WayPoint> wayPoints,
            TargetPoint targetPoint, int beforeTargetPointIndex) {

        List<WayPoint> newTargetPoints = new ArrayList<WayPoint>();
        for (int i = 0; i < beforeTargetPointIndex; i++) {
            newTargetPoints.add(wayPoints.get(i));
        }
        newTargetPoints.add(targetPoint);
        for (int i = beforeTargetPointIndex; i < wayPoints.size(); i++) {
            newTargetPoints.add(wayPoints.get(i));
        }
        return newTargetPoints;
    }

    /* ====================================================================== */
    /*                   Hilfsmethoden zur Erzeugung der Touren               */
    /* ====================================================================== */
    /**
     * Berechnet die benötigte Zeit für die Tour.
     * @param isPaperChase Schnitzeljagd
     * @param distance Distanz
     * @param speed Geschwindigkeit
     * @param targetPointsNumber Anzahl der Zielpunkte
     * @return benötigte Zeit
     */
    private int calcualteRequiredMinutes(boolean isPaperChase, double distance,
            float speed, int targetPointsNumber) {

        /* Zeit = Zielpunkte ohne Start- und Endpunkt * Verweildauer + 
         * Zeit die für die Distanz benötigt wird. */
        return (targetPointsNumber - 1) * (isPaperChase
                ? DWELL_PAPERCHASE : DWELL)
                + calculateMinutesForDistance(distance, speed);
    }

    /**
     * Berechnet die benötigte Zeit in Minuten für die Distanz.
     * @param distance Distanz
     * @param speed Geschwindigkeit
     * @return benötigte Zeit in Minuten
     */
    private int calculateMinutesForDistance(double distance, float speed) {
        /* + Faktor 1/3 auf Grund der Luftlinie */
        return (int) ((distance / speed * 60)
                + (1 / 3) * (distance / speed * 60));
    }

    /* ====================================================================== */
    /*                       Route                                            */
    /* ====================================================================== */
    /**
     * Liefert die Route.
     * @param settings Einstellungen
     * @param targetPoint Zielpunkt
     * @return Route
     * @throws ConnectionException bei Verbindungsfehler mit dem Route Service
     */
    @Override
    public Tour getRoute(TourSettings settings, GeoPoint targetPoint)
            throws ConnectionException {

        /* Route Service erzeugen */
        try {
            this.routeService = new OpenRouteService();
        } catch (SAXException ex) {
            throw new ConnectionException(ex.getMessage());
        }

        /* Start und Endpunkt festlegen */
        List<WayPoint> targetPoints = new ArrayList<WayPoint>();
        targetPoints.add(new WayPoint(1, settings.getStartPoint(), PointType.START_POINT));
        targetPoints.add(new WayPoint(2, targetPoint, PointType.END_POINT));

        /* Tour erzeugen */
        return this.routeService.getTour(targetPoints, TourType.ROUTE,
                settings.getLocomotion(), settings.getHowGood(),
                settings.getLanguage(), 0);
    }

    /* ====================================================================== */
    /*                       Question                                         */
    /* ====================================================================== */
    /**
     * Liefert eine Frage eines Zielpunktes unter Berücksichtigung des 
     * gewünschten Schwierigkeitsgrad. Aus den möglichen Fragen wird eine
     * Frage zufällig bestimmt. Existiert keine Frage zum angegebenen 
     * Schwierigkeitsgrad wird eine Frage aus dem nächst leichteren 
     * Schwierigkeitsgrad gewählt. (Außer es war bereits die leichteste 
     * Kategorie.)
     * @param targetPointID ID des Zielpunkts
     * @param language Sprache, in der die Frage gestellt werden soll
     * @param levelOfDifficulty gewünschter Schwierigkeitsgrad.
     * @param askedQuestionIDs IDs der bereits gestellten Fragen
     * @return Frage.
     */
    @Override
    public Question getQuestion(int targetPointID, Language language,
            LevelOfDifficulty levelOfDifficulty, long[] askedQuestionIDs)
            throws NoQuestionFoundException {

        List<DTO.Database.DBQuestion> questions = dao.getAllQuestionsFor(
                targetPointID, language, levelOfDifficulty, askedQuestionIDs);

        if (questions.size() > 0) {
            return getOutputQuestion(
                    questions.get((int) (Math.random() * questions.size())));
        }

        /* Wenn eine Frage nicht zu der Schwierigkeitsgrad existiert, wird eine 
         * Frage aus dem nächst leichteren Schwierigkeitsgrad gewählt. (Außer
         * es war bereits die leichteste Kategorie.)*/
        if (levelOfDifficulty.equals(levelOfDifficulty.EASY)) {
            throw new NoQuestionFoundException(ERROR_MSG_NO_QUESTION);
        }

        return getQuestion(targetPointID, language,
                LevelOfDifficulty.values()[levelOfDifficulty.ordinal() - 1], 
                askedQuestionIDs);
    }

    /**
     * Konvertiert das Database-DBQuestion-Objekt in eine Output-DBQuestion-Objekt.
     * @param question Database-DBQuestion-Objekt
     * @return Output-DBQuestion-Objekt
     */
    private DTO.Web.Output.Question getOutputQuestion(DTO.Database.DBQuestion question) {
        return new DTO.Web.Output.Question(question.getId(), question.getQuestion(),
                question.getCorrectAnswer(), question.getWrongAnswers(),
                question.getLevelOfDifficulty());
    }
}
