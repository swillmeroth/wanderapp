package DTO.Web.Output;

import Definitions.LevelOfDifficulty;

/**
 * Definiert eine Frage mit zugehörigen Antworten. Zudem steht auch die Zuge-
 * hörigkeit des Punkts zum Zielpunkt fest.
 */
public class Question {

    /* ====================================================================== */
    /*                       Instanzvariablen                                 */
    /* ====================================================================== */
    /** ID der Frage */
    private long id;
    /** Fragestellung. */
    private String question;
    /** richtige Antwort der Frage. */
    private String correctAnswer;
    /** Falsche Antworten auf die Frage. */
    private String[] wrongAnswers;
    /** Schwierigkeitsgrad der Frage. */
    private LevelOfDifficulty levelOfDifficulty;

    /* ====================================================================== */
    /*                       Konstruktoren                                    */
    /* ====================================================================== */
    /**
     * Erzeugt eine Frage, mit Antworten und dem Zielpunkt, dem sie zugeordnet 
     * ist.
     * @param id ID der Frage
     * @param question Frage
     * @param correctAnswer richtige Antwort
     * @param wrongAnswers falsche Antworten
     * @param levelOfDifficulty Schwierigkeitsgrad der Frage
     * @param targetPoint  Zielpunkt
     */
    public Question(long id, String question, String correctAnswer, 
            String[] wrongAnswers, LevelOfDifficulty levelOfDifficulty) {
        this.id = id;
        this.question = question;
        this.correctAnswer = correctAnswer;
        this.wrongAnswers = wrongAnswers;
        this.levelOfDifficulty = levelOfDifficulty;
    }
    
    /**
     * Leerer Konstruktor.
     */
    public Question() { }    

    /* ====================================================================== */
    /*                       Getter & Setter                                  */
    /* ====================================================================== */
    /**
     * Gibt die Id der Frage.
     * @return Id der Frage
     */
    public long getId() {
        return id;
    }

    /**
     * Setzt die Id der Frage.
     * @param Id der Frage
     */
    public void setId(long id) {
        this.id = id;
    }
    
    /* ---------------------------------------------------------------------- */
    /**
     * Gibt die Frage.
     * @return Frage
     */
    public String getQuestion() {
        return question;
    }

    /**
     * Setzt die Frage.
     * @param Frage
     */
    public void setQuestion(String question) {
        this.question = question;
    }

    /* ---------------------------------------------------------------------- */
    /**
     * Gibt die richtige Antwort.
     * @return richtige Antwort
     */
    public String getCorrectAnswer() {
        return correctAnswer;
    }

    /**
     * Gibt die richtige Antwort.
     * @param correctAnswer richtige Antwort
     */
    public void setCorrectAnswer(String correctAnswer) {
        this.correctAnswer = correctAnswer;
    }

    /* ---------------------------------------------------------------------- */
    /**
     * Gibt die falschen Antworten zur Frage.
     * @return falsche Antworten zur Frage
     */
    public String[] getWrongAnswers() {
        return wrongAnswers;
    }

    /**
     * Setzt die falschen Antworten zur Frage.
     * @param falsche Antworten zur Frage
     */
    public void setWrongAnswers(String[] wrongAnswers) {
        this.wrongAnswers = wrongAnswers;
    }

    /* ---------------------------------------------------------------------- */
    /**
     * Gibt den Schwierigkeitsgrad der Frage.
     * @return Schwierigkeitsgrad
     */
    public LevelOfDifficulty getLevelOfDifficulty() {
        return levelOfDifficulty;
    }

    /**
     * Setzt den Schwierigkeitsgrad der Frage.
     * @param Schwierigkeitsgrad
     */
    public void setLevelOfDifficulty(LevelOfDifficulty levelOfDifficulty) {
        this.levelOfDifficulty = levelOfDifficulty;
    }

    /* ====================================================================== */
    /*                       toString, ...                                    */
    /* ====================================================================== */
    /**
     * Gibt die Frage.
     * @return Frage
     */
    @Override
    public String toString() {
        return this.question;
    }

    /**
     * Objekt als String, mit allen interessanten Informationen, außer dem 
     * Zielpunkt. (Frage, Antworten)
     * @return Objekt als String
     */
    public String toStringCompletely() {
        return "Frage: " + question + ",\n"
                + "richtige Antwort: " + correctAnswer + ",\n"
                + "falsche Antworten: " + getWrongAnswersToString() + ",\n"
                + "Schwierigkeitsgrad: " + LevelOfDifficulty.getLevelOfDifficulty(this.levelOfDifficulty);
    }
    
    /**
     * Hilfsmethode, die die falschen Antworten, welche in einem Array stehen, 
     * zu einen String fasst.
     * @return falsche Antworten als ein String
     */
    private String getWrongAnswersToString() {

        String answers = "";

        if (this.wrongAnswers.length >= 1) {
            answers = this.wrongAnswers[0];
        } else {
            return answers;
        }

        for (int i = 1; i < this.wrongAnswers.length; i++) {
            answers = answers + " / " + this.wrongAnswers[i];
        }

        return answers;
    }
}
