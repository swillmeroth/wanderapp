package DTO.XML;

import DTO.Database.DBQuestion;
import Definitions.Language;
import Definitions.LevelOfDifficulty;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

/**
 * Definiert eine Frage mit zugehörigen Antworten. Zudem steht auch die Zuge-
 * hörigkeit des Punkts zum Zielpunkt fest.
 */
@Root(name="question")
public class XMLQuestion {

    /* ====================================================================== */
    /*                       Instanzvariablen                                 */
    /* ====================================================================== */
    /** Id der Frage. */
    @Attribute
    private long id;
    /** Fragestellung. */
    @Element
    private String question;
    /** richtige Antwort der Frage. */
    @Element
    private String correctAnswer;
    /** Falsche Antworten auf die Frage. */
    @ElementList
    private ArrayList<String> wrongAnswers;
    /** Schwierigkeitsgrad der Frage. */
    @Attribute
    private LevelOfDifficulty levelOfDifficulty;
    /** Sprache, in der die Frage gestellt ist. */
    @Attribute
    private Language language;

    /* ====================================================================== */
    /*                       Konstruktoren                                    */
    /* ====================================================================== */    
    /**
     * Erzeugt eine Frage, mit Antworten und dem Zielpunkt, dem sie zugeordnet 
     * ist.
     * @param id ID
     * @param question Frage
     * @param correctAnswer richtige Antwort
     * @param wrongAnswers falsche Antworten
     * @param levelOfDifficulty Schwierigkeitsgrad der Frage
     * @param language Sprache, in der die Frage gestellt ist
     */
    public XMLQuestion(long id, String question, String correctAnswer, 
            ArrayList<String> wrongAnswers, 
            LevelOfDifficulty levelOfDifficulty, Language language) {
        this.id = id;
        this.question = question;
        this.correctAnswer = correctAnswer;
        this.wrongAnswers = wrongAnswers;
        this.levelOfDifficulty = levelOfDifficulty;
        this.language = language;
    }
    
    /**
     * Erzeugt eine Frage, mit Antworten und dem Zielpunkt, dem sie zugeordnet 
     * ist.
     * @param id ID
     * @param question Frage
     * @param correctAnswer richtige Antwort
     * @param wrongAnswers falsche Antworten
     * @param levelOfDifficulty Schwierigkeitsgrad der Frage
     * @param language Sprache, in der die Frage gestellt ist
     */
    public XMLQuestion(long id, String question, String correctAnswer, 
            String[] wrongAnswers, LevelOfDifficulty levelOfDifficulty,
            Language language) {
        this.id = id;
        this.question = question;
        this.correctAnswer = correctAnswer;
        this.wrongAnswers = new ArrayList<String>();
        this.wrongAnswers.addAll(Arrays.asList(wrongAnswers));       
        this.levelOfDifficulty = levelOfDifficulty;
        this.language = language;
    }
    
    /**
     * Erzeugt eine Frage, mit Antworten und dem Zielpunkt, dem sie zugeordnet 
     * ist.
     * @param question DBQuestion-Objekt
     */
    public XMLQuestion(DBQuestion question) {
        this.id = question.getId();
        this.question = question.getQuestion();
        this.correctAnswer = question.getCorrectAnswer();
        this.wrongAnswers = new ArrayList<String>();
        this.wrongAnswers.addAll(Arrays.asList(question.getWrongAnswers()));       
        this.levelOfDifficulty = question.getLevelOfDifficulty();
        this.language = question.getLanguage();
    }
    
    /**
     * Leerer Konstruktor.
     */
    public XMLQuestion() { }    

    /* ====================================================================== */
    /*                       Getter & Setter                                  */
    /* ====================================================================== */    
    /**
     * Gibt die ID der Frage.
     * @return ID der Frage
     */
    public long getId() {
        return id;
    }

    /**
     * Setzt die ID der Frage.
     * @param ID der Frage
     */
    public void setId(long id) {
        this.id = id;
    }
    
    /* ---------------------------------------------------------------------- */
    /**
     * Gibt die Frage.
     * @return Frage
     */
    public String getQuestion() {
        return question;
    }

    /**
     * Setzt die Frage.
     * @param Frage
     */
    public void setQuestion(String question) {
        this.question = question;
    }

    /* ---------------------------------------------------------------------- */
    /**
     * Gibt die richtige Antwort.
     * @return richtige Antwort
     */
    public String getCorrectAnswer() {
        return correctAnswer;
    }

    /**
     * Gibt die richtige Antwort.
     * @param correctAnswer richtige Antwort
     */
    public void setCorrectAnswer(String correctAnswer) {
        this.correctAnswer = correctAnswer;
    }

    /* ---------------------------------------------------------------------- */
    /**
     * Gibt die falschen Antworten zur Frage.
     * @return falsche Antworten zur Frage
     */
    public List<String> getWrongAnswers() {
        return wrongAnswers;
    }

    /**
     * Setzt die falschen Antworten zur Frage.
     * @param falsche Antworten zur Frage
     */
    public void setWrongAnswers(ArrayList<String> wrongAnswers) {
        this.wrongAnswers = wrongAnswers;
    }

    /* ---------------------------------------------------------------------- */
    /**
     * Gibt den Schwierigkeitsgrad der Frage.
     * @return Schwierigkeitsgrad
     */
    public LevelOfDifficulty getLevelOfDifficulty() {
        return levelOfDifficulty;
    }

    /**
     * Setzt den Schwierigkeitsgrad der Frage.
     * @param Schwierigkeitsgrad
     */
    public void setLevelOfDifficulty(LevelOfDifficulty levelOfDifficulty) {
        this.levelOfDifficulty = levelOfDifficulty;
    }
    
    /* ---------------------------------------------------------------------- */
    /**
     * Gibt die Sprache in der die Frage gestellt ist.
     * @return Sprache
     */
    public Language getLanguage() {
        return language;
    }

    /**
     * Gibt die Sprache in der die Frage gestellt ist.
     * @param language Sprache
     */
    public void setLanguage(Language language) {
        this.language = language;
    }

    /* ====================================================================== */
    /*                       toString, ...                                    */
    /* ====================================================================== */
    /**
     * Vergleicht ein Quuestion-Objekt mit eine Objekt. Wenn die ID gleich ist, 
     * sind die Objekte gleich.
     * @param obj Objekt
     * @return true, wenn gleich
     */
    @Override
    public boolean equals(Object obj) {
        
        return obj instanceof XMLQuestion 
                && this.levelOfDifficulty.equals(((XMLQuestion) obj).levelOfDifficulty)
                && this.question.equals(((XMLQuestion) obj).question)
                && this.correctAnswer.equals(((XMLQuestion) obj).correctAnswer)
                && isWrongAnswersEquals(((XMLQuestion) obj).wrongAnswers);  
    }
    
    /**
     * Prüft, ob die falschen Antworten 2 Objekte gleich sind.
     * @param wrongAnswers falsche Antworten des übergebenen Objektes
     * @return true, wenn falsche Antworten gleich
     */
    private boolean isWrongAnswersEquals(List<String> wrongAnswers) {
        if (this.wrongAnswers.size() != wrongAnswers.size()) {
            return false;
        }
        for (int i = 0; i < this.wrongAnswers.size(); i++) {
            if(!this.wrongAnswers.get(i).equals(wrongAnswers.get(i))) {
                return false;
            }
        }
        return true;
    }

    /**
     * Gibt den HashCode.
     * @return hashCode
     */
    @Override
    public int hashCode() {
        
        return this.question.hashCode();
    }

    /**
     * Gibt die Frage.
     * @return Frage
     */
    @Override
    public String toString() {
        return this.question;
    }
    
    /**
     * Objekt als String, mit allen interessanten Informationen. (Frage, 
     * Antworten und Zielpunkt)
     * @return Objekt als String
     */
    public String toStringComplete() {
        return "Frage: " + question + ",\n"
                + "richtige Antwort: " + correctAnswer + ",\n"
                + "falsche Antworten: " + wrongAnswers + ",\n"
                + "Schwierigkeitsgrad: " 
                + LevelOfDifficulty.getLevelOfDifficulty(this.levelOfDifficulty);
    }
}
