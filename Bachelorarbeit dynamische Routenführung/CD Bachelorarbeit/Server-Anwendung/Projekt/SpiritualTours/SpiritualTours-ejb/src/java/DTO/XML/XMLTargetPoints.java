package DTO.XML;

import DTO.Database.DBTargetPoint;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

/**
 * Definiert das Wurzelelement, um die Zielpunkte gekapselt als XML zu 
 * importieren und exportieren.
 */
@Root(name="targetPoints")
public class XMLTargetPoints {
    
    /* ====================================================================== */
    /*                       Instanzvariablen                                 */
    /* ====================================================================== */
    /** Liste der Zielpunkte. */
    @ElementList(name="list")
    private ArrayList<XMLTargetPoint> targetPoints;

    /* ====================================================================== */
    /*                       Konstruktoren                                    */
    /* ====================================================================== */   
    /**
     * Erzeugt eine neue Liste von Zielpunkten.
     * @param targetPoints Liste von Zielpunkte
     */    
    public XMLTargetPoints(ArrayList<XMLTargetPoint> targetPoints) {
        this.targetPoints = targetPoints;
    }
    
    /**
     * Erzeugt eine neue Liste von Zielpunkten.
     * @param targetPoints Liste von Zielpunkte
     */
    public XMLTargetPoints(Collection<DBTargetPoint> targetPoints) {
        this.targetPoints = new ArrayList<XMLTargetPoint>();
        for (DBTargetPoint targetPoint : targetPoints) {
            this.targetPoints.add(new XMLTargetPoint(targetPoint));
        }
    }
    
    /**
     * Erzeugt eine neue Liste von Zielpunkten.
     * @param targetPoints Liste von Zielpunkte
     */
    public XMLTargetPoints(DBTargetPoint[] targetPoints) {
        this.targetPoints = new ArrayList<XMLTargetPoint>();
        for (DBTargetPoint targetPoint : targetPoints) {
            this.targetPoints.add(new XMLTargetPoint(targetPoint));
        }
    }
    
    /**
     * Erzeugt ein neues Zielpunkt-Listen-Objekt.
     */
    public XMLTargetPoints() {
    }
    
    /* ====================================================================== */
    /*                       Getter & Setter                                  */
    /* ====================================================================== */
    /**
     * Gibt die Zielpunkte
     * @return Zielpunkte
     */
    public ArrayList<XMLTargetPoint> getTargetPoints() {
        return targetPoints;
    }

    /**
     * Setzt die Zielpunkte
     * @param targetPoints Zielpunkte
     */
    public void setTargetPoints(ArrayList<XMLTargetPoint> targetPoints) {
        this.targetPoints = targetPoints;
    }
    
    /* ====================================================================== */
    /*                       toDBTargetPoints                                 */
    /* ====================================================================== */
    /**
     * Wandelt von XML-TargetPoints nach DB-TargetPoints um
     * @return DB-TargetPoints
     */
    public Collection<DBTargetPoint> toDBTargetPoints() {
        List<DBTargetPoint> dbTargetPoints = new ArrayList<DBTargetPoint>();
        for (XMLTargetPoint targetPoint : this.targetPoints) {
            dbTargetPoints.add(new DBTargetPoint(targetPoint));
        }
        return dbTargetPoints;
    }
}
