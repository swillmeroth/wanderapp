package DTO.Database;

import DTO.XML.XMLQuestion;
import Definitions.Language;
import Definitions.LevelOfDifficulty;
import java.io.Serializable;
import javax.persistence.*;

/**
 * Definiert eine Frage mit zugehörigen Antworten. Zudem steht auch die Zuge-
 * hörigkeit des Punkts zum Zielpunkt fest.
 */
@Entity
@Table(name = "Question")
public class DBQuestion implements Serializable {

    /* ====================================================================== */
    /*                       Instanzvariablen                                 */
    /* ====================================================================== */
    /** 
     * ID. 
     * Primaerschluessel 
     * automatisch generiert */
    @Id
    @GeneratedValue
    private long id;
    /** Fragestellung. */
    private String question;
    /** richtige Antwort der Frage. */
    private String correctAnswer;
    /** Falsche Antworten auf die Frage. */
    private String[] wrongAnswers;
    /** Schwierigkeitsgrad der Frage. */
    @Enumerated(EnumType.STRING)
    private LevelOfDifficulty levelOfDifficulty;
    /** Sprache, in der die Frage gestellt ist. */
    @Enumerated(EnumType.STRING)
    private Language vernacular;
    /**
     * Zielpunkt, dem die Frage zugeordnet ist. 
     * EAGER => wird sofort aufglöst 
     */
    @ManyToOne(fetch = FetchType.EAGER)
    private DBTargetPoint targetPoint;

    /* ====================================================================== */
    /*                       Konstruktoren                                    */
    /* ====================================================================== */
    /**
     * Erzeugt eine Frage, mit Antworten und dem Zielpunkt, dem sie zugeordnet 
     * ist.
     * @param question Frage
     * @param correctAnswer richtige Antwort
     * @param wrongAnswers falsche Antworten
     * @param levelOfDifficulty Schwierigkeitsgrad der Frage
     * @param language Sprache, in der die Frage gestellt ist
     * @param targetPoint Zielpunkt
     */
    public DBQuestion(String question, String correctAnswer,
            String[] wrongAnswers, LevelOfDifficulty levelOfDifficulty,
            Language language, DBTargetPoint targetPoint) {
        this.question = question;
        this.correctAnswer = correctAnswer;
        this.wrongAnswers = wrongAnswers;
        this.levelOfDifficulty = levelOfDifficulty;
        this.vernacular = language;
        this.targetPoint = targetPoint;
    }

    /**
     * Erzeugt eine Frage, mit Antworten und dem Zielpunkt, dem sie zugeordnet 
     * ist.
     * @param question Frage
     * @param correctAnswer richtige Antwort
     * @param wrongAnswers falsche Antworten
     * @param levelOfDifficulty Schwierigkeitsgrad der Frage
     * @param language Sprache, in der die Frage gestellt ist
     */
    public DBQuestion(String question, String correctAnswer,
            String[] wrongAnswers, LevelOfDifficulty levelOfDifficulty,
            Language language) {
        this.question = question;
        this.correctAnswer = correctAnswer;
        this.wrongAnswers = wrongAnswers;
        this.levelOfDifficulty = levelOfDifficulty;
        this.vernacular = language;
    }

    /**
     * Erzeugt eine Frage, mit Antworten und dem Zielpunkt, dem sie zugeordnet 
     * ist.
     * @param question XMLQuestion
     * @param targetPoint Zielpunkt
     */
    public DBQuestion(XMLQuestion question, DBTargetPoint targetPoint) {
        this.id = question.getId();
        this.question = question.getQuestion();
        this.correctAnswer = question.getCorrectAnswer();
        this.wrongAnswers = new String[question.getWrongAnswers().size()];
        this.wrongAnswers = question.getWrongAnswers().toArray(wrongAnswers);
        this.levelOfDifficulty = question.getLevelOfDifficulty();
        this.vernacular = question.getLanguage();
        this.targetPoint = targetPoint;
    }

    /**
     * Erzeugt eine Frage, mit Antworten und dem Zielpunkt, dem sie zugeordnet 
     * ist.
     * @param question XMLQuestion
     * @param targetPoint Zielpunkt
     */
    public DBQuestion(DBTargetPoint targetPoint) {
        this.targetPoint = targetPoint;
    }

    /**
     * Leerer Konstruktor.
     */
    public DBQuestion() {
    }

    /* ====================================================================== */
    /*                       Getter & Setter                                  */
    /* ====================================================================== */
    /**
     * Liefert die ID des Objekts.
     * @return ID des Objekts.
     */
    public long getId() {
        return id;
    }

    /**
     * Setzt die ID des Objekts.
     * @param ID des Objekts.
     */
    public void setId(long id) {
        this.id = id;
    }

    /* ---------------------------------------------------------------------- */
    /**
     * Gibt die Frage.
     * @return Frage
     */
    public String getQuestion() {
        return question;
    }

    /**
     * Setzt die Frage.
     * @param Frage
     */
    public void setQuestion(String question) {
        this.question = question;
    }

    /* ---------------------------------------------------------------------- */
    /**
     * Gibt die richtige Antwort.
     * @return richtige Antwort
     */
    public String getCorrectAnswer() {
        return correctAnswer;
    }

    /**
     * Gibt die richtige Antwort.
     * @param correctAnswer richtige Antwort
     */
    public void setCorrectAnswer(String correctAnswer) {
        this.correctAnswer = correctAnswer;
    }

    /* ---------------------------------------------------------------------- */
    /**
     * Gibt die falschen Antworten zur Frage.
     * @return falsche Antworten zur Frage
     */
    public String[] getWrongAnswers() {
        return wrongAnswers;
    }

    /**
     * Setzt die falschen Antworten zur Frage.
     * @param falsche Antworten zur Frage
     */
    public void setWrongAnswers(String[] wrongAnswers) {
        this.wrongAnswers = wrongAnswers;
    }

    /* ---------------------------------------------------------------------- */
    /**
     * Gibt den Schwierigkeitsgrad der Frage.
     * @return Schwierigkeitsgrad
     */
    public LevelOfDifficulty getLevelOfDifficulty() {
        return levelOfDifficulty;
    }

    /**
     * Setzt den Schwierigkeitsgrad der Frage.
     * @param levelOfDifficulty Schwierigkeitsgrad
     */
    public void setLevelOfDifficulty(LevelOfDifficulty levelOfDifficulty) {
        this.levelOfDifficulty = levelOfDifficulty;
    }

    /* ---------------------------------------------------------------------- */
    /**
     * Gibt die Sprache in der die Frage gestellt ist.
     * @return Sprache
     */
    public Language getLanguage() {
        return vernacular;
    }

    /**
     * Gibt die Sprache in der die Frage gestellt ist.
     * @param language Sprache
     */
    public void setLanguage(Language language) {
        this.vernacular = language;
    }

    /* ---------------------------------------------------------------------- */
    /**
     * Gibt den Zielpunkt, dem die Frage zugeordnet ist.
     * @return Zielpunkt
     */
    public DBTargetPoint getTargetPoint() {
        return targetPoint;
    }

    /**
     * Setzt den Zielpunkt, dem die Frage zugeordnet ist.
     * @param Zielpunkt
     */
    public void setTargetPoint(DBTargetPoint targetPoint) {
        this.targetPoint = targetPoint;
    }

    /* ====================================================================== */
    /*                       toString, ...                                    */
    /* ====================================================================== */
    /**
     * Prüft, ob alle notwendigen Informationen vorhanden sind.
     * @return true, alle notwendigen Informationen vorhanden sind
     */
    public boolean isInformationCompletelyInitialized() {
        return this.question != null
                && !this.question.replaceAll(" ", "").isEmpty()
                && this.correctAnswer != null
                && !this.correctAnswer.replaceAll(" ", "").isEmpty()
                && this.wrongAnswers != null
                && this.wrongAnswers.length > 0
                && isWrongAnswersCompletelyInitialized()
                && this.vernacular != null
                && this.targetPoint != null;
    }

    /**
     * Prüft, ob alles falschen Antworten eingeben worden sind.
     * @return true, wenn alle falschen Antworten korrekt eingegeben worden sind
     */
    private boolean isWrongAnswersCompletelyInitialized() {
        for (String wrongAnswer : this.wrongAnswers) {
            if (wrongAnswer == null || wrongAnswer.replace(" ", "").isEmpty()) {
                return false;
            }
        }
        return true;
    }

    /**
     * Vergleicht ein Quuestion-Objekt mit eine Objekt. Wenn die ID gleich ist, 
     * sind die Objekte gleich.
     * @param obj Objekt
     * @return true, wenn gleich
     */
    @Override
    public boolean equals(Object obj) {
        return obj instanceof DBQuestion && this.id
                == ((DBQuestion) obj).id;
    }

    /**
     * Gibt den HashCode.
     * @return hashCode
     */
    @Override
    public int hashCode() {
        return (int) this.id;
    }

    /**
     * Gibt die Frage.
     * @return Frage
     */
    @Override
    public String toString() {
        return this.question + (this.vernacular != null
                ? " (" + Language.getLanguage(vernacular) + ")"
                : "");
    }

    /**
     * Objekt als String, mit allen interessanten Informationen. (Frage, 
     * Antworten und Zielpunkt)
     * @return Objekt als String
     */
    public String toStringComplete() {
        return "Frage: " + question + ",\n"
                + "richtige Antwort: " + correctAnswer + ",\n"
                + "falsche Antworten: " + getWrongAnswersToString() + ",\n"
                + "Schwierigkeitsgrad: "
                + LevelOfDifficulty.getLevelOfDifficulty(this.levelOfDifficulty)
                + ",\n"
                + "Sprache: " + Language.getLanguage(this.vernacular) + ",\n"
                + "zugehöriger Zielpunkt: " + targetPoint;
    }

    /**
     * Objekt als String, mit allen interessanten Informationen, außer dem 
     * Zielpunkt. (Frage, Antworten)
     * @return Objekt als String
     */
    public String toStringWithoutTargetPoint() {
        return "Frage: " + question + ",\n"
                + "richtige Antwort: " + correctAnswer + ",\n"
                + "falsche Antworten: " + getWrongAnswersToString() + ",\n"
                + "Schwierigkeitsgrad: "
                + LevelOfDifficulty.getLevelOfDifficulty(this.levelOfDifficulty)
                + "Sprache: " + Language.getLanguage(this.vernacular);
    }

    /**
     * Hilfsmethode, die die falschen Antworten, welche in einem Array stehen, 
     * zu einen String fasst.
     * @return falsche Antworten als ein String
     */
    private String getWrongAnswersToString() {

        String answers = "";

        if (this.wrongAnswers.length >= 1) {
            answers = this.wrongAnswers[0];
        } else {
            return answers;
        }

        for (int i = 1; i < this.wrongAnswers.length; i++) {
            answers = answers + " / " + this.wrongAnswers[i];
        }

        return answers;
    }
}
