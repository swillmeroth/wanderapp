package DTO.Database;

import DTO.XML.XMLInformation;
import Definitions.Language;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Definiert einen Informationstext in einer Sprache für einen Zielpunkt.
 */
@Entity
@Table(name = "Information")
public class DBInformation implements Serializable {

    /* ====================================================================== */
    /*                       Instanzvariablen                                 */
    /* ====================================================================== */
    /**
     * Zielpunkt, dem die Frage zugeordnet ist. 
     * EAGER => wird sofort aufglöst 
     */
    @Id
    @JoinColumn(name = "id")
    @ManyToOne(fetch = FetchType.EAGER)
    private DBTargetPoint targetPoint;    
    /**
     * Sprache, in der die Information verfasst worden ist.
     */
    @Id
    @Enumerated(EnumType.STRING)
    private Language vernacular;
    /** Informationen zum Zielpunkt. */
    @Lob
    private String information;

    /* ====================================================================== */
    /*                       Konstruktoren                                    */
    /* ====================================================================== */
    /**
     * Erzeugt eine Information.
     * @param information Information
     * @param speech Sprache, in der die Information verfasst wird
     * @param targetPoint Zielpunkt der Information
     */
    public DBInformation(Language speech, DBTargetPoint targetPoint,
            String information) {
        this.targetPoint = targetPoint;
        this.vernacular = speech;
        this.information = information;
    }

    /**
     * Erzeugt eine Information.
     * @param information Information
     * @param speech Sprache, in der die Information verfasst wird
     */
    public DBInformation(String information, Language speech) {
        this.vernacular = speech;
        this.information = information;
    }
    
    /**
     * Erzeugt eine Information.
     * @param targetPoint Zielpunkt
     */
    public DBInformation(DBTargetPoint targetPoint) {
        this.targetPoint = targetPoint;
    }

    /**
     * Erzeugt eine Information.
     * @param information XMLInformation-Objekt
     * @param targetPoint Zielpunkt der Information
     */
    public DBInformation(XMLInformation information,
            DBTargetPoint targetPoint) {
        this.targetPoint = targetPoint;
        this.vernacular = information.getLanguage();
        this.information = information.getInformation();
    }

    /**
     * Erzeugt eine Information.
     */
    public DBInformation() {
    }

    /* ====================================================================== */
    /*                       Getter & Setter                                  */
    /* ====================================================================== */
    /**
     * Gibt den Zielpunkt der Information.
     * @return Sprache
     */
    public DBTargetPoint getTargetPoint() {
        return targetPoint;
    }

    /**
     * Setzt den Zielpunkt der Information.
     * @param language Sprache
     */
    public void setTargetPoint(DBTargetPoint targetPoint) {
        this.targetPoint = targetPoint;
    }

    /* ---------------------------------------------------------------------- */
    /**
     * Gibt die Sprache der Information.
     * @return Sprache
     */
    public Language getLanguage() {
        return vernacular;
    }

    /**
     * Setzt die Sprache der Information.
     * @param language Sprache
     */
    public void setLanguage(Language language) {
        this.vernacular = language;
    }

    /* ---------------------------------------------------------------------- */
    /**
     * Gibt die Information zum Zielpunkt.
     * @return Information
     */
    public String getInformation() {
        return information;
    }

    /**
     * Setzt die Information zum Zielpunkt.
     * @param information Information
     */
    public void setInformation(String information) {
        this.information = information;
    }

    /* ====================================================================== */
    /*                       toString, ...                                    */
    /* ====================================================================== */
    /**
     * Prüft, ob alle notwendigen Informationen vorhanden sind.
     * @return true, alle notwendigen Informationen vorhanden sind
     */
    public boolean isInformationCompletelyInitialized() {
        return this.information != null 
                && !this.information.replaceAll(" ", "").isEmpty()
                && this.vernacular != null
                && this.targetPoint != null;
    }
    
    /**
     * Gibt die Information.
     * @return Information
     */
    @Override
    public String toString() {
        return Language.getLanguage(this.vernacular);
    }

    /**
     * Objekt als String, mit allen Informationen. (Informationen, Sprache und 
     * Zielpunkt).
     * @return Objekt als String
     */
    public String toStringCompleteInformation() {
        return "Information: " + this.information + ",\n"
                + "Sprache: " + Language.getLanguage(this.vernacular)
                + ",\n" + "Zielpunkt: " + this.targetPoint.toString();
    }

    /**
     * Vergleicht ein DBInformation-Objekt mit eine Objekt. Wenn die 
     * ID gleich ist, sind die Objekte gleich.
     * @param obj Objekt
     * @return true, wenn gleich
     */
    @Override
    public boolean equals(Object obj) {
        return obj instanceof DBInformation
                && targetPoint.equals(((DBInformation) obj).targetPoint)
                && vernacular.ordinal() == ((DBInformation) obj).vernacular.ordinal();
    }

    /**
     * Gibt den HashCode.
     * @return hashCode
     */
    @Override
    public int hashCode() {
        return this.targetPoint.hashCode() + vernacular.ordinal();
    }
}
