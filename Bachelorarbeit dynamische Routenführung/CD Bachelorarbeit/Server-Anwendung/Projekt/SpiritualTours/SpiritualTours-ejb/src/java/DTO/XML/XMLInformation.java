package DTO.XML;

import DTO.Database.DBInformation;
import Definitions.Language;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Definiert einen Informationstext in einer Sprache für einen Zielpunkt.
 */
@Root(name="information")
public class XMLInformation {

    /* ====================================================================== */
    /*                       Instanzvariablen                                 */
    /* ====================================================================== */
    /** Informationen zum Zielpunkt. */
    @Element
    private String information;
    /** Sprache, in der die Information verfasst ist. */
    @Attribute
    private Language language;

    /* ====================================================================== */
    /*                       Konstruktoren                                    */
    /* ====================================================================== */
    /**
     * Erzeugt eine Information.
     * @param information Information
     * @param language Sprache, in der die Information verfasst wird
     */
    public XMLInformation(String information, Language language) {
        this.information = information;
        this.language = language;
    }

    /**
     * Erzeugt eine Frage, mit Antworten und dem Zielpunkt, dem sie zugeordnet 
     * ist.
     * @param question DBQuestion-Objekt
     */
    public XMLInformation(DBInformation information) {
        this.information = information.getInformation();
        this.language = information.getLanguage();
    }
    
    /**
     * Erzeugt eine Information.
     */
    public XMLInformation() {
    }

    /* ====================================================================== */
    /*                       Getter & Setter                                  */
    /* ====================================================================== */
    /**
     * Gibt die Information zum Zielpunkt.
     * @return Information
     */
    public String getInformation() {
        return information;
    }

    /**
     * Setzt die Information zum Zielpunkt.
     * @param information Information
     */
    public void setInformation(String information) {
        this.information = information;
    }

    /* ---------------------------------------------------------------------- */
    /**
     * Gibt die Sprache, in der die Information zum Zielpunkt verfasst ist.
     * @return Sprache
     */
    public Language getLanguage() {
        return language;
    }

    /**
     * Setzt die Sprache, in der die Information zum Zielpunkt verfasst ist.
     * @param language Sprache
     */
    public void setLanguage(Language language) {
        this.language = language;
    }

    /* ====================================================================== */
    /*                       toString, ...                                    */
    /* ====================================================================== */
    /**
     * Gibt die Information.
     * @return Information
     */
    @Override
    public String toString() {
        return this.information;
    }

    /**
     * Objekt als String, mit allen Informationen. (Informationen, Sprache und 
     * Zielpunkt).
     * @return Objekt als String
     */
    public String toStringCompleteInformation() {
        return "Information: " + this.information + ",\n"
                + "Sprache: " + Language.getLanguage(this.language);
    }

    /**
     * Vergleicht ein XMLInformation-Objekt mit eine Objekt. Wenn die 
     * ID gleich ist, sind die Objekte gleich.
     * @param obj Objekt
     * @return true, wenn gleich
     */
    @Override
    public boolean equals(Object obj) {
        return obj instanceof XMLInformation
                && this.information.equals(((XMLInformation) obj).information);
    }

    /**
     * Gibt den HashCode.
     * @return hashCode
     */
    @Override
    public int hashCode() {
        return this.information.hashCode();
    }
}
