package Administration;

import DTO.Database.DBQuestion;
import DTO.Database.DBTargetPoint;
import DTO.Database.DBInformation;
import Definitions.Language;
import Exceptions.ApplicationException;
import java.util.Collection;
import javax.ejb.Remote;

/**
 * Schnittstelle des EJB-Containers nach außen für den Administrator, um 
 * Zielpunkte hinzuzufügen, zu bearbeiten und zu löschen.
 */
@Remote
public interface AdministrationRemote {

    /* ====================================================================== */
    /*                       Getter                                           */
    /* ====================================================================== */
    /**
     * Alle Zielpunkte.
     * @return alle Zielpunkte
     */
    public Collection<DBTargetPoint> getAllTargetPoints();
    
    /**
     * Gibt den Zielpunkt passend zur ID.
     * @param targetPointID ID des Zielpunkts
     * @return Zielpunkt
     * @throws ApplicationException Problem beim Zugriff auf die Datenbank
     */
    public DBTargetPoint getTargetPoint(long targetPointID) 
            throws ApplicationException ;

    /* ====================================================================== */
    /*                       Setter                                           */
    /* ====================================================================== */
    /**
     * Fügt neue Zielpunkte hinzu und aktualisiert schon vorhandene.
     * @param targetPoint Zielpunkt
     */
    public String[] setTargetPoints(Collection<DBTargetPoint> targetPoints);
    
    /**
     * Zielpunkt hinzufügen.
     * @param targetPoint Zielpunkt
     */
    public void addTargetPoint(DBTargetPoint targetPoint); 
    
    /**
     * Fragen hinzufügen.
     * @param targetPointID Id des Zielpunktes
     * @param questions Fragen
     */
    public void addQuestions(Collection<DBQuestion> questions) 
            throws ApplicationException;
    
    /* ====================================================================== */
    /*                       Update                                           */
    /* ====================================================================== */
    /**
     * Aktualisiert einen Zielpunkt. Dabei werden Fragen, die neue hinzugekommen
     * sind hinzugefügt, gelöschte Fragen gelöscht und Fragen und DBPosition
     * ebenfalls aktualisiert.
     * @param updateTargetPoint Zielpunkt
     * @param removedQuestions Frage die gelöscht wurden
     * @param removedInformations Informationen die gelöscht wurden
     * @throws ApplicationException bei Fehler bei der Synchronisation mit der 
     * Datenbank.
     */
    public void updateTargetPoint(DBTargetPoint updateTargetPoint,
            Collection<DBQuestion> removedQuestions, 
            Collection<DBInformation> removedInformations) 
            throws ApplicationException;

    /* ====================================================================== */
    /*                       Delete                                           */
    /* ====================================================================== */
    /**
     * Zielpunkt löschen.
     * @param targetPointID ID des Zielpunktes
     * @throws ApplicationException 
     */
    public void removeTargetPoint(long targetPointID)
            throws ApplicationException;
}
