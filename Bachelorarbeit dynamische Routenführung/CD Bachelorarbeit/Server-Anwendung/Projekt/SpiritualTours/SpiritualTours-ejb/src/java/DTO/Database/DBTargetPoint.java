package DTO.Database;

import DTO.XML.XMLQuestion;
import DTO.XML.XMLTargetPoint;
import DTO.XML.XMLInformation;
import Definitions.Language;
import Definitions.TargetPointType;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.*;

/**
 * Definiert einen Zielpunkt. Zugehörige Informationen sind Name des Objekts, 
 * die Art des Objekts, die DBPosition, und Beschreibungen in verschiedenen
 * Sprachen.
 */
@Entity
@Table(name = "TargetPoint")
public class DBTargetPoint implements Serializable, Comparable<DBTargetPoint> {

    /* ====================================================================== */
    /*                       Instanzvariablen                                 */
    /* ====================================================================== */
    /** 
     * ID. 
     * Primaerschluessel 
     * automatisch generiert */
    @Id
    @GeneratedValue
    private long id;
    /** Name des Zielpunkts. */
    private String name;
    /** Art des Zielpunkts. */
    @Enumerated(EnumType.STRING)
    private TargetPointType targetPointType;
    /**
     * OneToMany Ein Zielpunkt kann die Information in unterschiedlichen 
     * Sprachen haben
     *
     * Information ist der Owner bedeutet dass man sich hier auf die Variable
     * targetPoint in der Klasse DBInformation bezieht
     * 
     * EAGER => wird sofort aufglöst
     */
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "targetPoint", 
            fetch = FetchType.EAGER)
    private Collection<DBInformation> informations;
    /** 
     * Postion des Zielpunkts.
     * EAGER => wird sofort aufglöst
     */
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private DBPosition position;
    /** Reichweite in Metern um den Zielpunkt herum. */
    private int range;
    /**
     * OneToMany Ein Zielpunkt kann mehrere Fragen haben
     *
     * Frage ist der Owner bedeutet dass man sich hier auf die Variable
     * targetPoint in der Klasse DBQuestion bezieht
     * 
     * EAGER => wird sofort aufglöst
     */
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "targetPoint", 
            fetch = FetchType.EAGER)
    private Collection<DBQuestion> questions;

    /* ====================================================================== */
    /*                       Konstruktoren                                    */
    /* ====================================================================== */
    /**
     * Erzeugt einen Zielpunkt.
     * @param name Name des Zielpunkts
     * @param targetPointType Art des Zielpunkts
     * @param position DBPosition des Zielpunkts
     * @param informations Information in den jeweiligen Sprachen zum Zielpunkt
     * @param range Reichweite in Metern um den Zielpunkt herum
     * @param questions Fragen des Zielpunkts für den Schnitzeljagd-Modus
     */
    public DBTargetPoint(String name, TargetPointType targetPointType,
            Collection<DBInformation> informations, DBPosition position, 
            int range, Collection<DBQuestion> questions) {
        this.name = name;
        this.targetPointType = targetPointType;
        this.informations = informations;
        this.position = position;
        this.range = range;
        this.questions = questions;
    }
    
    /**
     * Erzeugt einen Zielpunkt.
     * @param name Name des Zielpunkts
     * @param targetPointType Art des Zielpunkts
     * @param range Reichweite in Metern um den Zielpunkt herum
     */
    public DBTargetPoint(String name, TargetPointType targetPointType, 
            int range) {
        this.name = name;
        this.targetPointType = targetPointType;
        this.range = range;
    }

    /**
     * Erzeugt einen Zielpunkt.
     * @param targetPoint XML-TargetPoint
     */
    public DBTargetPoint(XMLTargetPoint targetPoint) {
        this.id = targetPoint.getId();
        this.name = targetPoint.getName();
        this.targetPointType = targetPoint.getTargetPointType();
        this.informations = new ArrayList<DBInformation>();
        for (XMLInformation information : targetPoint.getInformations()) {
            this.informations.add(new DBInformation(information, this));
        }
        this.position = new DBPosition(targetPoint.getPosition(), this);
        this.range = targetPoint.getRange();
        this.questions = new ArrayList<DBQuestion>();
        for (XMLQuestion question : targetPoint.getQuestions()) {
            this.questions.add(new DBQuestion(question, this));
        }
    }

    /**
     * Erzeugt einen Zielpunkt.
     * @param id ID des Zielpunkts.
     */
    public DBTargetPoint(long id) {
        this.id = id;
    }

    /**
     * Leerer Konstruktor.
     */
    public DBTargetPoint() {
    }

    /* ====================================================================== */
    /*                       Getter & Setter                                  */
    /* ====================================================================== */
    /**
     * Gibt die ID des Zielpunkts.
     * @return ID des Zielpunkts.
     */
    public long getId() {
        return id;
    }

    /**
     * Setzt die ID des Zielpunkts.
     * @param ID des Zielpunkts.
     */
    public void setId(long id) {
        this.id = id;
    }

    /* ---------------------------------------------------------------------- */
    /**
     * Gibt den Namen des Zielpunkts.
     * @return Namen des Zielpunkts.
     */
    public String getName() {
        return name;
    }

    /**
     * Setzt den Namen des Zielpunkts.
     * @param Namen des Zielpunkts.
     */
    public void setName(String name) {
        this.name = name;
    }

    /* ---------------------------------------------------------------------- */
    /**
     * Gibt den Typen des Zielpunkts.
     * @return Typ des Zielpunkts.
     */
    public TargetPointType getTargetPointType() {
        return targetPointType;
    }

    /**
     * Setzt den Typen des Zielpunkts.
     * @param Typ des Zielpunkts.
     */
    public void setTargetPointType(TargetPointType targetPointType) {
        this.targetPointType = targetPointType;
    }

    /* ---------------------------------------------------------------------- */
    /**
     * Gibt die Informationen des Zielpunkts.
     * @return Informationen des Zielpunkts.
     */
    public Collection<DBInformation> getInformations() {
        return informations;
    }

    /**
     * Gibt die Information zum Zielpunkt in der gewünschten Sprache.
     * @param language gewünschte Sprache
     * @return Information
     */
    public DBInformation getInformation(Language language) {
        for (DBInformation information : this.informations) {
            if (information.getLanguage().ordinal() == language.ordinal()) {
                return information;
            }
        }
        return null;
    }
    
    /**
     * Gibt die Information zum Zielpunkt in der gewünschten Sprache.
     * @param language gewünschte Sprache
     * @return Information
     */
    public String getInformationString(Language language) {
        for (DBInformation information : this.informations) {
            if (information.getLanguage().ordinal() == language.ordinal()) {
                return information.getInformation();
            }
        }
        return "";
    }
    
    /**
     * Setzt die Informationen des Zielpunkts.
     * @param Informationen des Zielpunkts.
     */
    public void setInformations(
            Collection<DBInformation> informations) {
        this.informations = informations;
    }

    /* ---------------------------------------------------------------------- */
    /**
     * Gibt die DBPosition des Zielpunkts.
     * @return DBPosition des Zielpunkts.
     */
    public DBPosition getPosition() {
        return position;
    }

    /**
     * Setzt die DBPosition des Zielpunkts.
     * @param DBPosition des Zielpunkts.
     */
    public void setPosition(DBPosition position) {
        this.position = position;
    }

    /* ---------------------------------------------------------------------- */
    /**
     * Gibt die Reichweite um den Zielpunkt herum in Meter an.
     * 
     * @return Reichweite
     */
    public int getRange() {
        return range;
    }

    /**
     * Setzt die Reichweite um den Zielpunkt herum in Meter.
     * 
     * @param range Reichweite
     */
    public void setRange(int range) {
        this.range = range;
    }

    /* ---------------------------------------------------------------------- */
    /**
     * Gibt die Fragen zum Zielpunkts.
     * @return Fragen zum Zielpunkts.
     */
    public Collection<DBQuestion> getQuestions() {
        return questions;
    }

    /**
     * Setzt die Fragen zum Zielpunkts.
     * @param Fragen zum Zielpunkts.
     */
    public void setQuestions(Collection<DBQuestion> questions) {
        this.questions = questions;
    }

    /* ====================================================================== */
    /*                       toString, ...                                    */
    /* ====================================================================== */
    /**
     * Prüft, ob alle notwendigen Informationen vorhanden sind.
     * @return true, alle notwendigen Informationen vorhanden sind
     */
    public boolean isTargetPointCompletelyInitialized() {
        return this.name != null && this.targetPointType != null
                && this.position != null && this.position.getStreet() != null 
                && this.questions != null && this.informations != null;
    }

    /**
     * Vergleicht ein DBTargetPoint-Objekt mit eine Objekt. Wenn die ID gleich  
     * ist, sind die Objekte gleich.
     * @param obj Objekt
     * @return true, wenn gleich
     */
    @Override
    public boolean equals(Object obj) {
        return obj instanceof DBTargetPoint && this.id
                == ((DBTargetPoint) obj).id;
    }

    /**
     * Gibt den HashCode.
     * @return hashCode
     */
    @Override
    public int hashCode() {

        return (int) this.id;
    }

    /**
     * Name des Zielpunktes.
     * @return Name des Zielpunktes
     */
    @Override
    public String toString() {
        return this.name;
    }

    /**
     * Objekt als String, mit allen interessanten Informationen. (Name des 
     * Zielpunkts, Typ, DBPosition, Fragen, Informationen & Reichweite)
     * @return Objekt als String
     */
    public String toStringCompleteInformation() {
        return "Name: " + name + ",\n"
                + "Typ: " + TargetPointType.getTargetPointType(this.targetPointType) + ",\n"
                + "Information: " + informations + ",\n"
                + "Postion: " + position.toString() + ",\n"
                + "Reichweite: " + range + "m,\n"
                + "Fragen: " + getQuestionsToString();
    }

    /**
     * Objekt als String, mit allen interessanten Informationen, außer den 
     * Fragen. (Name des Zielpunkts, Typ, DBPosition, Informationen & Reichweite)
     * @return Objekt als String
     */
    public String toStringWithoutQuestions() {
        return "Name: " + name + ",\n"
                + "Typ: " + TargetPointType.getTargetPointType(this.targetPointType) + ",\n"
                + "Information: " + informations + ",\n"
                + "Postion: " + position.toString() + ",\n"
                + "Reichweite: " + range + "m";
    }

    /**
     * Fragen zusammenstellen.
     * @return Fragen als ein String.
     */
    private String getQuestionsToString() {
        String allQuestions = "";
        for (DBQuestion question : this.questions) {
            allQuestions = allQuestions + "\n" + question.toStringWithoutTargetPoint();
        }
        return allQuestions;
    }

    /**
     * Vergleicht den Namen des Zielpunkts.
     * @param targetPoint Zielpunkt mit dem verglichen werden soll
     * @return -1 von Name vor übergebenen Namen liegt, 0 wenn gleich1 wenn 
     * übergebener Name vor Name liegt
     */
    @Override
    public int compareTo(DBTargetPoint targetPoint) {
        return this.name.compareTo(targetPoint.name);
    }
}
