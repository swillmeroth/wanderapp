package Administration;

import DAO.DataAccessObjectLocal;
import DTO.Database.DBQuestion;
import DTO.Database.DBTargetPoint;
import DTO.Database.DBInformation;
import Definitions.Language;
import Exceptions.ApplicationException;
import java.io.Serializable;
import java.util.Collection;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 * Schnittstelle des EJB-Containers nach außen für den Administrator, um 
 * Zielpunkte hinzuzufügen, zu bearbeiten und zu löschen.
 */
@Stateless
public class AdministrationBean implements Serializable, AdministrationRemote {

    /* ====================================================================== */
    /*                       Instanzvariablen                                 */
    /* ====================================================================== */
    /**
     * Schnittstelle zur Datenbank.
     */
    @EJB
    private DataAccessObjectLocal dao;

    /* ====================================================================== */
    /*                       Getter                                           */
    /* ====================================================================== */  
    /**
     * Gibt den Zielpunkt passend zur ID.
     * @param targetPointID ID des Zielpunkts
     * @return Zielpunkt
     * @throws ApplicationException Problem beim Zugriff auf die Datenbank
     */
    @Override
    public DBTargetPoint getTargetPoint(long targetPointID) 
            throws ApplicationException {
        return this.dao.getTargetPoint(targetPointID);
    }
    
    /**
     * Alle Zielpunkte.
     * @return alle Zielpunkte
     */
    @Override
    public Collection<DBTargetPoint> getAllTargetPoints() {
        return dao.getAllTargetPoints();
    }

    /* ====================================================================== */
    /*                       Setter                                           */
    /* ====================================================================== */
    /**
     * Fügt neue Zielpunkte hinzu und aktualisiert schon vorhandene.
     * @param targetPoint Zielpunkt
     */
    @Override
    public String[] setTargetPoints(Collection<DBTargetPoint> targetPoints) {
       return this.dao.setTargetPoints(targetPoints);         
    }
    
    /**
     * Zielpunkt hinzufügen.
     * @param targetPoint Zielpunkt
     */
    @Override
    public void addTargetPoint(DBTargetPoint targetPoint) {
        dao.addTargetPoint(targetPoint);
    }

    /**
     * Fragen hinzufügen.
     * @param targetPointID Id des Zielpunktes
     * @param questions Fragen
     */
    @Override
    public void addQuestions(Collection<DBQuestion> questions) 
            throws ApplicationException {
        this.dao.addQuestions(questions);
    }
    
    /* ====================================================================== */
    /*                       Update                                           */
    /* ====================================================================== */
    /**
     * Aktualisiert einen Zielpunkt. Dabei werden Fragen, die neue hinzugekommen
     * sind hinzugefügt, gelöschte Fragen gelöscht und Fragen und DBPosition
     * ebenfalls aktualisiert.
     * @param updateTargetPoint Zielpunkt
     * @param removedQuestions Frage die gelöscht wurden
     * @param removedInformations Informationen die gelöscht wurden
     * @throws ApplicationException bei Fehler bei der Synchronisation mit der 
     * Datenbank.
     */
    @Override
    public void updateTargetPoint(DBTargetPoint updateTargetPoint,
            Collection<DBQuestion> removedQuestions, 
            Collection<DBInformation> removedInformations) 
            throws ApplicationException {        
        dao.updateTargetPoint(updateTargetPoint, removedQuestions, 
                removedInformations);
    }

    /* ====================================================================== */
    /*                       Delete                                           */
    /* ====================================================================== */
    /**
     * Zielpunkt löschen.
     * @param targetPointID ID des Zielpunktes
     * @throws ApplicationException 
     */
    @Override
    public void removeTargetPoint(long targetPointID) throws ApplicationException {
        dao.removeTargetPoint(targetPointID);
    }
}
