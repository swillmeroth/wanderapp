package DTO.Web.Output;

/**
 * Anweisung der Teilstrecken.
 */
public class Instruction {

    /* ====================================================================== */
    /*                       Instanzvariablen                                 */
    /* ====================================================================== */    
    /** Beschreibung zur Anweisung. */
    private String description;
    /** Dauer der Strecke in Minuten. */
    private int duration;
    /** Distanz in km. */
    private double distance;
    /** Anweisung. */
    private String instruction;

    /* ====================================================================== */
    /*                       Konstruktoren                                    */
    /* ====================================================================== */ 
    /**
     * Erzeugt eine neue Anweisung.
     */
    public Instruction() {
    }

    /**
     * Erzeugt eine neue Anweisung.
     * @param descriptionOrderNo Beschreibung zur Anweisung
     * @param duration zeitliche Dauer in Minuten
     * @param distance Distanz
     * @param language Sprache
     * @param instruction Anweisung
     */
    public Instruction(String description, int duration, double distance, 
            String instruction) {
        this.description = description;
        this.duration = duration;
        this.distance = distance;
        this.instruction = instruction;
    }

    /* ====================================================================== */
    /*                       Getter & Setter                                  */
    /* ====================================================================== */
    /**
     * Gibt die Beschreibung zur Anweisung.
     * @return Reihenfolge
     */
    public String getDescription() {
        return description;
    }

    /**
     * Setzt die Beschreibung zur Anweisung.
     * @param descriptionOrderNo Reihenfolge
     */
    public void setDescription(String description) {
        this.description = description;
    }
    
    /* ---------------------------------------------------------------------- */
    /**
     * Gibt die zeitliche Dauer in Minuten.
     * @return zeitliche Dauer
     */
    public int getDuration() {
        return duration;
    }

    /**
     * Setzt die zeitliche Dauer in Minuten.
     * @param duration zeitliche Dauer
     */
    public void setDuration(int duration) {
        this.duration = duration;
    }

    /* ---------------------------------------------------------------------- */
    /**
     * Gibt die Distanz.
     * @return Distanz
     */
    public double getDistance() {
        return distance;
    }

    /**
     * Setzt die Distanz.
     * @param distance Distanz
     */
    public void setDistance(double dinstance) {
        this.distance = dinstance;
    }
    
    /* ---------------------------------------------------------------------- */
    /**
     * Gibt die Anweisung.
     * @return Anweisung
     */
    public String getInstruction() {
        return instruction;
    }

    /**
     * Setzt die Anweisung.
     * @param instruction Anweisung
     */
    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }
    
    /* ====================================================================== */
    /*                       toString                                         */
    /* ====================================================================== */
    /**
     * Gibt die Anweisung als String.
     * @return Anweisung als String
     */
    @Override
    public String toString() {
        return "Beschreibung: " + description
                + ", Dauer: " + duration + " Minuten"
                + ", Distanz: " + distance + " km"
                + ", Anweisung: " + instruction;
    }
}
