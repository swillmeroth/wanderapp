package Service;

import DAO.DataAccessObject;
import DTO.GeoPoint;
import Definitions.LevelOfDifficulty;
import DTO.Web.Input.TourSettings;
import Definitions.Language;
import Definitions.TargetPointType;
import Exceptions.ConnectionException;
import Exceptions.NoQuestionFoundException;
import Exceptions.NoTargetPointsFoundException;
import javax.ws.rs.FormParam;
import javax.ws.rs.Path;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.PathParam;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import net.sf.json.JsonConfig;
import net.sf.json.util.EnumMorpher;
import net.sf.json.util.JSONUtils;

/**
 * Service für Mobil-Geräte.
 */
@Path("spiritualtours")
public class SpiritualToursService {

    /* ====================================================================== */
    /*                       Konstanten                                       */
    /* ====================================================================== */
    /** Umlaut Ä. */
    private static String UMLAUT_AE = "Ä";
    /** Umlaut ä. */
    private static String UMLAUT_ae = "ä";
    /** Umlaut Ö. */
    private static String UMLAUT_OE = "Ö";
    /** Umlaut ö. */
    private static String UMLAUT_oe = "ö";
    /** Umlaut Ü. */
    private static String UMLAUT_UE = "Ü";
    /** Umlaut ü. */
    private static String UMLAUT_ue = "ü";
    /** Umlaut ß. */
    private static String UMLAUT_SZ = "ß";
    /** Ersatz für Umlaut Ä. */
    private static String UMLAUT_HTML_AE = "&Auml;";
    /** Ersatz für  Umlaut ä. */
    private static String UMLAUT_HTML_ae = "&auml;";
    /** Ersatz für  Umlaut Ö. */
    private static String UMLAUT_HTML_OE = "&Ouml;";
    /** Ersatz für  Umlaut ö. */
    private static String UMLAUT_HTML_oe = "&ouml;";
    /** Ersatz für  Umlaut Ü. */
    private static String UMLAUT_HTML_UE = "&Uuml;";
    /** Ersatz für  Umlaut ü. */
    private static String UMLAUT_HTML_ue = "&uuml;";
    /** Ersatz für  Umlaut ß. */
    private static String UMLAUT_HTML_SZ = "&szlig;";
    /** ID für Fehlermeldung: Keine Zielpunkte vorhanden. */
    private static final String NO_TARGET_POINTS_FOUND_EXCEPTION =
            "NoTargetPointsFoundException";
    /** ID für Fehlermeldung: Keine Zielpunkte vorhanden. */
    private static final String NO_QUESTION_FOUND_EXCEPTION =
            "NoQuestionFoundException";
    /** ID für Fehlermeldung: Verbindungsfehler. */
    private static final String CONNECTION_EXCEPTION =
            "ConnectionException";
    /* ====================================================================== */
    /*                       Instanzvariablen                                 */
    /* ====================================================================== */
    /** Data Access Objekt */
    private static DataAccessObject dao = new DataAccessObject();

    /* ====================================================================== */
    /*                       Touren & Route                                   */
    /* ====================================================================== */
    /**
     * Gibt die Touren an, die die gewünschten Einstellungen mit 
     * berücksichtigen.
     * @param settings Einstellungen des Nutzers
     * @return Touren
     */
    @POST
    @Path("/tours")
    @Produces("application/json")
    public String getTours(@FormParam("settings") String settings) {
        System.out.println(settings);
        try {
            return replaceUmlauts(((JSONArray) JSONSerializer.toJSON(
                    dao.getTours(getTourSettings(settings)))).toString());
        } catch (NoTargetPointsFoundException ex) {
            return NO_TARGET_POINTS_FOUND_EXCEPTION;
        } catch (ConnectionException ex) {
            return CONNECTION_EXCEPTION;
        }
    }

    /**
     * Liefert die Route.
     * @param settings Einstellungen
     * @param aimGeoPoint Zielpunkt
     * @return Route
     * @throws ConnectionException bei Verbindungsfehler mit dem Route Service
     */
    @POST
    @Path("/route")
    @Produces("application/json")
    public String getRoute(@FormParam("settings") String settings,
            @FormParam("targetPoint") String targetPoint) {
        System.out.println(settings);
        try {
            return replaceUmlauts(((JSONObject) JSONSerializer.toJSON(
                    dao.getRoute(getTourSettings(settings),
                    getAimGeoPoint(targetPoint)))).toString());
        } catch (ConnectionException ex) {
            return CONNECTION_EXCEPTION;
        }
    }

    /**
     * Parst den erhaltenen String zu einem TourSettings-Objekt.
     * @param settingsString
     * @return 
     */
    private TourSettings getTourSettings(String settingsString) {

        JSONObject settingsJSON = JSONObject.fromObject(settingsString);

        JsonConfig jsonConfig = new JsonConfig();
        jsonConfig.setRootClass(TourSettings.class);

        /* Enumarations */
        JSONUtils.getMorpherRegistry().registerMorpher(
                new EnumMorpher(TourSettings.Locomotion.class));
        JSONUtils.getMorpherRegistry().registerMorpher(
                new EnumMorpher(TourSettings.HowGood.class));
        JSONUtils.getMorpherRegistry().registerMorpher(
                new EnumMorpher(TargetPointType.class));
        JSONUtils.getMorpherRegistry().registerMorpher(
                new EnumMorpher(Language.class));

        return (TourSettings) JSONSerializer.toJava(settingsJSON, jsonConfig);
    }

    /**
     * Gibt den Zielpunkt der Route
     * @param aimGeoPoint Zielpunkt als JSON-Objekt
     * @return Zielpunkt
     */
    private GeoPoint getAimGeoPoint(String aimGeoPointString) {

        System.out.println(aimGeoPointString);
        JSONObject aimGeoPointJSON = JSONObject.fromObject(aimGeoPointString);
        System.out.println(aimGeoPointJSON.toString());
        JsonConfig jsonConfig = new JsonConfig();
        jsonConfig.setRootClass(GeoPoint.class);
        System.out.println(((GeoPoint) JSONSerializer.toJava(aimGeoPointJSON, jsonConfig)).toString());
         
        return (GeoPoint) JSONSerializer.toJava(aimGeoPointJSON, jsonConfig);
    }

    /* ====================================================================== */
    /*                       Fragen                                           */
    /* ====================================================================== */
    /**
     * Gibt eine Frage eines Zielpunktes mit gewünschten Schwierigkeitsgrad.
     * Existiert keine Frage für diesen Schwierigkeitsgrad, wird eine leichtere
     * Frage übermittelt.
     * @param targetPointID Zielpunkt-ID
     * @param language Sprache, in der die Frage gestellt werden soll
     * @param levelOfDifficulty Schwierigkeitsgrad
     * @param askedQuestionIDs bereits gestellte Fragen
     * @return Frage
     */
    @POST
    @Path("{targetPointID}/{language}/{levelOfDifficulty}")
    @Produces("application/json")
    public String getQustionToTargetPoint(
            @PathParam("targetPointID") Integer targetPointID,
            @PathParam("language") Language language,
            @PathParam("levelOfDifficulty") LevelOfDifficulty levelOfDifficulty,
            @FormParam("askedQuestionIDs") String askedQuestionIDs) {

        try {
            return replaceUmlauts(((JSONObject) JSONSerializer.toJSON(
                    dao.getQuestion(targetPointID, language, levelOfDifficulty,
                    getAskedQuestionIDs(askedQuestionIDs)))).toString());
        } catch (NoQuestionFoundException ex) {
            return NO_QUESTION_FOUND_EXCEPTION;
        }
    }

    /**
     * Parst den erhaltenen String in die IDs der bereits gestellten Fragen.
     * @param askedQuestionIDsString String, der die bereits gestellten Fragen
     * enthält
     * @return IDs der bereits gestellten Fragen
     */
    private long[] getAskedQuestionIDs(String askedQuestionIDsString) {

        JSONArray askedQuestionIDsJSON =
                JSONArray.fromObject(askedQuestionIDsString);

        long[] askedQuestionIDs = new long[askedQuestionIDsJSON.size()];

        for (int i = 0; i < askedQuestionIDs.length; i++) {
            askedQuestionIDs[i] = askedQuestionIDsJSON.getLong(i);
        }

        return askedQuestionIDs;
    }

    /* ====================================================================== */
    /*                       allgemeine Hilfsmethoden                         */
    /* ====================================================================== */
    /**
     * Ersetzt alle Umlaute in die jeweiligen HTML-Umlauts-Zeichen.
     * @param string Zeichkette
     * @return Zeichkette mit ersetzten Zeichen
     */
    private String replaceUmlauts(String string) {

        return string.replace(UMLAUT_AE, UMLAUT_HTML_AE).
                replace(UMLAUT_ae, UMLAUT_HTML_ae).
                replace(UMLAUT_UE, UMLAUT_HTML_UE).
                replace(UMLAUT_ue, UMLAUT_HTML_ue).
                replace(UMLAUT_OE, UMLAUT_HTML_OE).
                replace(UMLAUT_oe, UMLAUT_HTML_oe).
                replace(UMLAUT_SZ, UMLAUT_HTML_SZ);
    }
}
