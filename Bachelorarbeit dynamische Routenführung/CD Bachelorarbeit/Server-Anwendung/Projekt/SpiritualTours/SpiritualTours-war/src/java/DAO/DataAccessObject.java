package DAO;

import DTO.GeoPoint;
import Definitions.LevelOfDifficulty;
import Exceptions.ApplicationException;
import DTO.Web.Input.TourSettings;
import DTO.Web.Output.Question;
import DTO.Web.Output.Tour;
import Definitions.Language;
import Exceptions.ConnectionException;
import Exceptions.NoQuestionFoundException;
import Exceptions.NoTargetPointsFoundException;
import SpiritualTours.SpiritualToursDelegate;
import java.util.List;
import javax.ejb.EJB;
import javax.transaction.UserTransaction;

/**
 * DAO, zum Zugriff auf den EJB-Container um eine Route planen
 * zu können, sowie Fragen zu Zielpunkten.
 * @param spiritualTours Zugriff zum EJB-Container
 */
public class DataAccessObject {

    /* ====================================================================== */
    /*                       Touren                                           */
    /* ====================================================================== */
    /** Zugriff zum Server */
    @EJB
    private static SpiritualToursDelegate spiritualTours;
    /** UserTransaction */
    private UserTransaction tx;

    /* ====================================================================== */
    /*                       Konstruktoren                                    */
    /* ====================================================================== */
    /**
     * Erzeugt ein neues Zugriffsobjekt auf den EJB-Container.
     */
    public DataAccessObject() {
        spiritualTours = new SpiritualToursDelegate();
    }

    /* ====================================================================== */
    /*                       Methoden zum Zugriff auf den EJB-Container       */
    /* ====================================================================== */
    /**
     * Gibt Touren, passend zu den Einstellungen des Nutzers.
     * @param tourSettings Einstellungen des Nutzers
     * @return Touren
     */
    public List<Tour> getTours(TourSettings tourSettings) 
            throws NoTargetPointsFoundException, ConnectionException {
        
        return spiritualTours.getTours(tourSettings);
    }
    
    /**
     * Liefert die Route.
     * @param settings Einstellungen
     * @param targetPoint Zielpunkt
     * @return Route
     * @throws ConnectionException bei Verbindungsfehler mit dem Route Service
     */
    public Tour getRoute(TourSettings tourSettings, GeoPoint targetPoint) 
            throws ConnectionException {
        
        return spiritualTours.getRoute(tourSettings, targetPoint);
    }

    /**
     * Lieferte eine Frage eines Zielpunktes und Berücksichtigung des 
     * gewünschten Schwierigkeitsgrad. Aus den in Frage kommenden Fragen wird 
     * eine Frage zufällig bestimmt. Existiert keine Frage zum angegebenen 
     * Schwierigkeitsgrad wird eine Frage aus allen zum Punkt gehörenden Fragen
     * gewählt.
     * @param targetPointID ID des Zielpunkts
     * @param language Sprache, in der die Frage gestellt werden soll
     * @param levelOfDifficulty gewünschter Schwierigkeitsgrad.
     * @param askedQuestionIDs IDs der bereits gestellten Fragen
     * @return Frage.
     */
    public Question getQuestion(int targetPointID, 
            Language language, LevelOfDifficulty levelOfDifficulty, 
            long[] askedQuestionIDs) throws NoQuestionFoundException {
        
        return spiritualTours.getQuestion(targetPointID, language, 
                levelOfDifficulty, askedQuestionIDs);
    }

    /* ====================================================================== */
    /*                       Zusatzmethoden                                   */
    /* ====================================================================== */
    /**
     * Wird aufgerufen sobald alle Teilaufgaben eines Bestellprozesses erledigt sind.
     * Führt commit aus, sofern die utx bis hierhin nicht abgebrochen ist.
     * @throws ApplicationException
     */
    public void commitOrder() throws ApplicationException {
        try {
            if (tx.getStatus() != javax.transaction.Status.STATUS_MARKED_ROLLBACK) {
                tx.commit();
            } else {
                tx.rollback();
            }
        } catch (Exception e) {
            throw new ApplicationException("Keine laufende Transaktion");
        }
    }

    /**
     * Bricht aktive Transaktion ab, sofern vorhanden.
     * @throws ApplicationException
     */
    public void cancelOrder() throws ApplicationException {
        try {
            tx.rollback();
        } catch (Exception ex) {
            throw new ApplicationException("Keine laufende Transaktion!");
        }
    }
}
