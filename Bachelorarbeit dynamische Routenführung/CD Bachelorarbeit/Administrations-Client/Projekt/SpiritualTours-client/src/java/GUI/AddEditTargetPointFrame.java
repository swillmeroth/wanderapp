package GUI;

import DTO.Database.DBPosition;
import DTO.Database.DBQuestion;
import DTO.Database.DBTargetPoint;
import DTO.Database.DBInformation;
import Definitions.Language;
import Definitions.TargetPointType;
import Utility.NumberDocument;
import Utility.TargetPointTypeModel;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;

/**
 * Fenster zum Hinzufügen oder Bearbeiten eines Zielpunkts.
 */
public class AddEditTargetPointFrame extends javax.swing.JDialog {

    /** Title für Fehlermeldungen. */
    private final static String MSG_ERROR_CAPTION = "Fehler";
    /** Titel für das Fenster bei Hinzufügen eines Zielpunkts. */
    private final static String TITLE_ADD = "Zielpunkt hinzufügen";
    /** Titel für das Fenster bei Bearbeiten eines Zielpunkts. */
    private final static String TITLE_EDIT = "Zielpunkt bearbeiten";
    /** Titel für den Button bei Hinzufügen eines Zielpunkts. */
    private final static String BUTTON_ADD = "Hinzufügen";
    /** Titel für den Button bei Bearbeiten eines Zielpunkts. */
    private final static String BUTTON_EDIT = "Bearbeiten";
    /** Zielpunkt der hinzugefügt oder bearbeitet wird. */
    private DBTargetPoint targetPoint;
    /** Fragen des Zielpunkts. */
    private List<DBInformation> informations;
    /** Gelöschte Fragen des Zielpunkts. */
    private Collection<DBInformation> removedInformations;
    /** Fragen des Zielpunkts. */
    private List<DBQuestion> questions;
    /** Gelöschte Fragen des Zielpunkts. */
    private Collection<DBQuestion> removedQuestions;

    /**
     * Erzeugt eine neues Zielpunkt-Hinzufügen-Fenster
     * @param parent Vater-Fenster des Fensters
     * @param modal modal?
     * @param targetPoint Zielpunkt
     */
    public AddEditTargetPointFrame(java.awt.Frame parent, boolean modal,
            DBTargetPoint targetPoint) {

        super(parent, modal);

        initComponents();

        this.removedInformations = new ArrayList<DBInformation>();
        this.removedQuestions = new ArrayList<DBQuestion>();

        setGUIProperties(targetPoint);

        this.setTitle(TITLE_ADD);
        this.addEditTargetPointButton.setText(BUTTON_ADD);
    }

    /**
     * Erzeugt eine neues Zielpunkt-Bearbeiten-Fenster
     * @param parent Vater-Fenster
     * @param modal 
     * @param targetPoint Zielpunkt der bearbeitet wird.
     * @param removedQuestions Fragen, die gelöscht werden.
     */
    public AddEditTargetPointFrame(java.awt.Frame parent, boolean modal,
            DBTargetPoint targetPoint, Collection<DBQuestion> removedQuestions,
            Collection<DBInformation> removedInformations) {

        super(parent, modal);

        initComponents();

        this.removedInformations = removedInformations;
        this.removedQuestions = removedQuestions;

        setGUIProperties(targetPoint);

        this.setTitle(TITLE_EDIT);
        this.addEditTargetPointButton.setText(BUTTON_EDIT);

        setData();
    }

    /**
     * Setzen von Eigenschaften der GUI-Elemente.
     */
    private void setGUIProperties(DBTargetPoint targetPoint) {

        this.targetPoint = targetPoint;
        this.informations = new ArrayList<DBInformation>();
        this.questions = new ArrayList<DBQuestion>();

        /* Eigenschaften für Info */
        this.informationTextArea.setLineWrap(true);
        this.informationTextArea.setWrapStyleWord(true);
        this.informationsList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        /* Eigenschaften für Typ */
        this.typeComboBox.setModel(new TargetPointTypeModel(TargetPointType.values()));
        this.typeComboBox.setSelectedIndex(0);

        /* Eigentschaften für Frageliste */
        this.questionsList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }

    /**
     * Textfelder mit Daten füllen
     */
    private void setData() {
        if (this.targetPoint != null) {
            this.idLable.setText("" + this.targetPoint.getId());
            if (this.targetPoint.getName() != null) {
                this.nameTextField.setText(this.targetPoint.getName());
            }
            if (this.targetPoint.getTargetPointType() != null) {
                this.typeComboBox.setSelectedIndex(this.targetPoint.getTargetPointType().ordinal());
            }
            if (this.targetPoint.getInformations() != null) {
                this.informationsList.setListData(this.targetPoint.getInformations().toArray(new DBInformation[0]));
                for (DBInformation information : this.targetPoint.getInformations()) {
                    this.informations.add(information);
                }
                if (!this.targetPoint.getInformations().isEmpty()) {
                    this.informationsList.setSelectedIndex(0);
                }
            }
            this.rangeTextField.setText("" + this.targetPoint.getRange());
            if (this.targetPoint.getPosition() != null) {
                this.longitudeTextField.setText("" + this.targetPoint.getPosition().getLongitude());
                this.latitudeTextField.setText("" + this.targetPoint.getPosition().getLatitude());
                this.altitudeTextField.setText("" + this.targetPoint.getPosition().getAltitude());
                if (this.targetPoint.getPosition().getTown() != null) {
                    this.townTextField.setText(this.targetPoint.getPosition().getTown());
                }
                this.postcodeTextField.setText("" + this.targetPoint.getPosition().getPostcode());
                if (this.targetPoint.getPosition().getStreet() != null) {
                    this.streetTextField.setText(this.targetPoint.getPosition().getStreet());
                }
            }
            if (this.targetPoint.getQuestions() != null) {
                this.questionsList.setListData(this.targetPoint.getQuestions().toArray(new DBQuestion[0]));
                for (DBQuestion question : this.targetPoint.getQuestions()) {
                    this.questions.add(question);
                }
                if (!this.targetPoint.getQuestions().isEmpty()) {
                    this.questionsList.setSelectedIndex(0);
                }
            }
        }

    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        questionsPanel = new javax.swing.JPanel();
        questionsScrollPane = new javax.swing.JScrollPane();
        questionsList = new javax.swing.JList();
        addQuestionButton = new javax.swing.JButton();
        editQuestionButton = new javax.swing.JButton();
        deleteQuestionButton = new javax.swing.JButton();
        targetPointPanel = new javax.swing.JPanel();
        nameLabel = new javax.swing.JLabel();
        nameTextField = new javax.swing.JTextField();
        typeLabel = new javax.swing.JLabel();
        typeComboBox = new javax.swing.JComboBox();
        infoLabel = new javax.swing.JLabel();
        rangeLabel = new javax.swing.JLabel();
        rangeTextField = new javax.swing.JTextField();
        rangeMeterLabel = new javax.swing.JLabel();
        idTextLable = new javax.swing.JLabel();
        idLable = new javax.swing.JLabel();
        editInformationButton = new javax.swing.JButton();
        deleteInformationButton = new javax.swing.JButton();
        addInformationButton = new javax.swing.JButton();
        informationScrollPane = new javax.swing.JScrollPane();
        informationTextArea = new javax.swing.JTextArea();
        informationsScrollPane = new javax.swing.JScrollPane();
        informationsList = new javax.swing.JList();
        positionPanel = new javax.swing.JPanel();
        latitudeLabel = new javax.swing.JLabel();
        longitudeLabel = new javax.swing.JLabel();
        altitudeLabel = new javax.swing.JLabel();
        latitudeTextField = new javax.swing.JTextField();
        longitudeTextField = new javax.swing.JTextField();
        altitudeTextField = new javax.swing.JTextField();
        streetLabel = new javax.swing.JLabel();
        streetTextField = new javax.swing.JTextField();
        townLabel = new javax.swing.JLabel();
        townTextField = new javax.swing.JTextField();
        postcodeLabel = new javax.swing.JLabel();
        postcodeTextField = new javax.swing.JTextField();
        altitudeMeterLabel = new javax.swing.JLabel();
        cancelButton = new javax.swing.JButton();
        addEditTargetPointButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(857, 570));

        questionsPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Fragen"));

        questionsList.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                questionsListValueChanged(evt);
            }
        });
        questionsScrollPane.setViewportView(questionsList);

        addQuestionButton.setText("Hinzufügen");
        addQuestionButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addQuestionButtonActionPerformed(evt);
            }
        });

        editQuestionButton.setText("Bearbeiten");
        editQuestionButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editQuestionButtonActionPerformed(evt);
            }
        });

        deleteQuestionButton.setText("Löschen");
        deleteQuestionButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteQuestionButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout questionsPanelLayout = new javax.swing.GroupLayout(questionsPanel);
        questionsPanel.setLayout(questionsPanelLayout);
        questionsPanelLayout.setHorizontalGroup(
            questionsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(questionsPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(questionsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(questionsScrollPane, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 339, Short.MAX_VALUE)
                    .addGroup(questionsPanelLayout.createSequentialGroup()
                        .addComponent(addQuestionButton, javax.swing.GroupLayout.DEFAULT_SIZE, 107, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(editQuestionButton, javax.swing.GroupLayout.DEFAULT_SIZE, 107, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(deleteQuestionButton, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        questionsPanelLayout.setVerticalGroup(
            questionsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, questionsPanelLayout.createSequentialGroup()
                .addComponent(questionsScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 396, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(questionsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(deleteQuestionButton)
                    .addComponent(editQuestionButton)
                    .addComponent(addQuestionButton))
                .addContainerGap())
        );

        targetPointPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Zielpunkt"));

        nameLabel.setText("Name");

        nameTextField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                nameTextFieldFocusGained(evt);
            }
        });

        typeLabel.setText("Typ");

        typeComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Friedhof", "Kapelle", "Kirche", "jüd. Friedhof", "Kloster", "Denkmal", "Standbild", "Wegekreuz" }));

        infoLabel.setText("Information");

        rangeLabel.setText("Reichweite");

        rangeTextField.setDocument(new NumberDocument("0123456789", 0));
        rangeTextField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                rangeTextFieldFocusGained(evt);
            }
        });

        rangeMeterLabel.setText("m");

        idTextLable.setText("ID");

        idLable.setText("wird automatisch erzeugt");

        editInformationButton.setText("Bearbeiten");
        editInformationButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editInformationButtonActionPerformed(evt);
            }
        });

        deleteInformationButton.setText("Löschen");
        deleteInformationButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteInformationButtonActionPerformed(evt);
            }
        });

        addInformationButton.setText("Hinzufügen");
        addInformationButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addInformationButtonActionPerformed(evt);
            }
        });

        informationTextArea.setColumns(20);
        informationTextArea.setEditable(false);
        informationTextArea.setRows(5);
        informationScrollPane.setViewportView(informationTextArea);

        informationsList.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                informationsListValueChanged(evt);
            }
        });
        informationsScrollPane.setViewportView(informationsList);

        javax.swing.GroupLayout targetPointPanelLayout = new javax.swing.GroupLayout(targetPointPanel);
        targetPointPanel.setLayout(targetPointPanelLayout);
        targetPointPanelLayout.setHorizontalGroup(
            targetPointPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(targetPointPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(targetPointPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(targetPointPanelLayout.createSequentialGroup()
                        .addGroup(targetPointPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(nameLabel)
                            .addComponent(typeLabel)
                            .addComponent(infoLabel)
                            .addComponent(idTextLable))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(targetPointPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(idLable)
                            .addComponent(typeComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(targetPointPanelLayout.createSequentialGroup()
                                .addComponent(addInformationButton, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(editInformationButton, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(deleteInformationButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(targetPointPanelLayout.createSequentialGroup()
                                .addComponent(informationsScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(informationScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 238, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(nameTextField)))
                    .addGroup(targetPointPanelLayout.createSequentialGroup()
                        .addComponent(rangeLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(rangeTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(rangeMeterLabel)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        targetPointPanelLayout.setVerticalGroup(
            targetPointPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(targetPointPanelLayout.createSequentialGroup()
                .addGroup(targetPointPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(idTextLable)
                    .addComponent(idLable))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(targetPointPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nameLabel)
                    .addComponent(nameTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(targetPointPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(typeLabel)
                    .addComponent(typeComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(targetPointPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(infoLabel)
                    .addGroup(targetPointPanelLayout.createSequentialGroup()
                        .addGroup(targetPointPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(informationsScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(informationScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(targetPointPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(addInformationButton)
                            .addComponent(deleteInformationButton)
                            .addComponent(editInformationButton))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(targetPointPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(rangeLabel)
                    .addComponent(rangeMeterLabel)
                    .addComponent(rangeTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        positionPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Position"));

        latitudeLabel.setText("Breitengrad");

        longitudeLabel.setText("Längengrad");

        altitudeLabel.setText("Höhe");

        latitudeTextField.setDocument(new NumberDocument(".0123456789", 0));
        latitudeTextField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                latitudeTextFieldFocusGained(evt);
            }
        });

        longitudeTextField.setDocument(new NumberDocument(".0123456789", 0));
        longitudeTextField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                longitudeTextFieldFocusGained(evt);
            }
        });

        altitudeTextField.setDocument(new NumberDocument(".0123456789", 0));
        altitudeTextField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                altitudeTextFieldFocusGained(evt);
            }
        });

        streetLabel.setText("Straße/Kreuzung");

        townLabel.setText("Ort");

        postcodeLabel.setText("PLZ");

        postcodeTextField.setDocument(new NumberDocument("0123456789", 5));

        altitudeMeterLabel.setText("m");

        javax.swing.GroupLayout positionPanelLayout = new javax.swing.GroupLayout(positionPanel);
        positionPanel.setLayout(positionPanelLayout);
        positionPanelLayout.setHorizontalGroup(
            positionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(positionPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(positionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(townLabel)
                    .addComponent(streetLabel)
                    .addComponent(postcodeLabel)
                    .addComponent(altitudeLabel)
                    .addComponent(longitudeLabel)
                    .addComponent(latitudeLabel))
                .addGap(18, 18, 18)
                .addGroup(positionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(positionPanelLayout.createSequentialGroup()
                        .addGroup(positionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(latitudeTextField)
                            .addComponent(longitudeTextField)
                            .addComponent(altitudeTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(altitudeMeterLabel))
                    .addComponent(postcodeTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(streetTextField, javax.swing.GroupLayout.DEFAULT_SIZE, 282, Short.MAX_VALUE)
                    .addComponent(townTextField, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 282, Short.MAX_VALUE))
                .addContainerGap())
        );
        positionPanelLayout.setVerticalGroup(
            positionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(positionPanelLayout.createSequentialGroup()
                .addGroup(positionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(latitudeTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(latitudeLabel))
                .addGap(6, 6, 6)
                .addGroup(positionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(longitudeTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(longitudeLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(positionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(altitudeTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(altitudeMeterLabel)
                    .addComponent(altitudeLabel))
                .addGap(18, 18, 18)
                .addGroup(positionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(postcodeLabel)
                    .addComponent(postcodeTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(positionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(townTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(townLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(positionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(streetTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(streetLabel))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        cancelButton.setText("Abbrechen");
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });

        addEditTargetPointButton.setText("Hinzufügen");
        addEditTargetPointButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addEditTargetPointButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(positionPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(targetPointPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(questionsPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(addEditTargetPointButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(cancelButton)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(questionsPanel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addComponent(targetPointPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(positionPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 12, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(addEditTargetPointButton, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cancelButton, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * Liste für die Fragen hat den Fokus erhalten.
     * @param evt Event
     */
    /**
     * Hinzufügen einer Frage.
     * @param evt Event
     */
    private void addQuestionButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addQuestionButtonActionPerformed
        DBQuestion question = new DBQuestion(this.targetPoint);
        AddEditQuestionFrame addEditQuestionFrame = new AddEditQuestionFrame(null, true, question, "Frage hinzufügen", "Hinzufügen");
        addEditQuestionFrame.setBounds(this.getBounds().x + 50, this.getBounds().y + 50,
                addEditQuestionFrame.getWidth(), addEditQuestionFrame.getHeight());
        addEditQuestionFrame.setVisible(true);

        if (question.isInformationCompletelyInitialized()) {
            this.questions.add(question);
            this.questionsList.setListData(
                    this.questions.toArray(new DBQuestion[0]));
        }
}//GEN-LAST:event_addQuestionButtonActionPerformed

    /**
     * Bearbeiten einer Frage.
     * @param evt Event
     */
    private void editQuestionButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editQuestionButtonActionPerformed
        AddEditQuestionFrame addEditQuestionFrame = new AddEditQuestionFrame(null, true, this.questions.get(this.questionsList.getSelectedIndex()), "Frage bearbeiten", "Bearbeiten");
        addEditQuestionFrame.setBounds(this.getBounds().x + 50, this.getBounds().y + 50,
                addEditQuestionFrame.getWidth(), addEditQuestionFrame.getHeight());
        addEditQuestionFrame.setVisible(true);
        this.questionsList.setListData(this.questions.toArray(new DBQuestion[0]));

}//GEN-LAST:event_editQuestionButtonActionPerformed

    /**
     * Löschen einer Frage.
     * @param evt Event
     */
    private void deleteQuestionButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteQuestionButtonActionPerformed
        
        int result = JOptionPane.showConfirmDialog(null, "Wollen Sie die Frage wirklich löschen?",
                "Löschen", JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE);
        
        if (result == JOptionPane.YES_OPTION) {
            this.removedQuestions.add((DBQuestion) this.questionsList.getSelectedValue());
            this.questions.remove(this.questionsList.getSelectedIndex());
            this.questionsList.setListData(this.questions.toArray(new DBQuestion[0]));
        }
}//GEN-LAST:event_deleteQuestionButtonActionPerformed

    /**
     * TextFeld für den Namen des Zielpunkts hat den Fokus erhalten.
     * @param evt Event
     */
    private void nameTextFieldFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_nameTextFieldFocusGained
        /* Rahmen auf Standard-Farbe setzen */
        this.nameTextField.setBorder(BorderFactory.createEtchedBorder());
}//GEN-LAST:event_nameTextFieldFocusGained

    /**
     * TextFeld für den Breitengrad des Zielpunkts hat den Fokus erhalten.
     * @param evt Event
     */
    private void latitudeTextFieldFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_latitudeTextFieldFocusGained
        /* Rahmen auf Standard-Farbe setzen */
        this.latitudeTextField.setBorder(BorderFactory.createEtchedBorder());
}//GEN-LAST:event_latitudeTextFieldFocusGained

    /**
     * TextFeld für den Längengrad des Zielpunkts hat den Fokus erhalten.
     * @param evt Event
     */
    private void longitudeTextFieldFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_longitudeTextFieldFocusGained
        /* Rahmen auf Standard-Farbe setzen */
        this.longitudeTextField.setBorder(BorderFactory.createEtchedBorder());
}//GEN-LAST:event_longitudeTextFieldFocusGained

    /**
     * TextFeld für die Höhe des Zielpunkts hat den Fokus erhalten.
     * @param evt Event
     */
    private void altitudeTextFieldFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_altitudeTextFieldFocusGained
        /* Rahmen auf Standard-Farbe setzen */
        this.altitudeTextField.setBorder(BorderFactory.createEtchedBorder());
}//GEN-LAST:event_altitudeTextFieldFocusGained

    /**
     * Klick auf den Abbrechen-Button des Fensters -> Fenster wird geschlossen.
     * @param evt Event
     */
    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        this.setVisible(false);
}//GEN-LAST:event_cancelButtonActionPerformed

    /**
     * Hinzufügen bzw Bearbeiten des Zielpunkts.
     * @param evt Event
     */
    private void addEditTargetPointButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addEditTargetPointButtonActionPerformed

        if (isInputCorrect()) {
            this.targetPoint.setName(this.nameTextField.getText());
            this.targetPoint.setTargetPointType(((TargetPointTypeModel) (typeComboBox.getModel())).getSelectedTargetPointType());
            this.targetPoint.setInformations(this.informations);
            this.targetPoint.setRange(Integer.valueOf(this.rangeTextField.getText()));
            DBPosition position = this.targetPoint.getPosition() != null
                    ? this.targetPoint.getPosition() : new DBPosition();
            position.setLongitude(Double.valueOf(this.longitudeTextField.getText()));
            position.setLatitude(Double.valueOf(this.latitudeTextField.getText()));
            position.setAltitude(Double.valueOf(this.altitudeTextField.getText()));
            position.setTown(this.townTextField.getText());
            position.setPostcode(Integer.valueOf(this.postcodeTextField.getText()));
            position.setStreet(this.streetTextField.getText());
            this.targetPoint.setPosition(position);
            this.targetPoint.setQuestions(this.questions);

            this.targetPoint.getPosition().setTargetPoint(this.targetPoint);
            for (DBQuestion question : this.questions) {
                question.setTargetPoint(this.targetPoint);
            }

            this.setVisible(false);
        }
}//GEN-LAST:event_addEditTargetPointButtonActionPerformed

    /**
     * TextFeld für die Reichweite hat den Fokus erhalten.
     * @param evt Event
     */
    private void rangeTextFieldFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_rangeTextFieldFocusGained
        this.rangeTextField.setBorder(BorderFactory.createEtchedBorder());
    }//GEN-LAST:event_rangeTextFieldFocusGained

    /**
     * Hinzufügen einer Information in einer Sprache.
     * @param evt Event
     */
    private void addInformationButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addInformationButtonActionPerformed

        DBInformation information = new DBInformation(this.targetPoint);
        AddEditInformationFrame addInformationFrame =
                new AddEditInformationFrame(null, true, information,
                "Information hinzufügen", "Hinzufügen", getLanguages(null));
        addInformationFrame.setBounds(this.getBounds().x + 50, this.getBounds().y + 50,
                addInformationFrame.getWidth(), addInformationFrame.getHeight());
        addInformationFrame.setVisible(true);

        if (information.isInformationCompletelyInitialized()) {
            this.informations.add(information);
            this.informationsList.setListData(this.informations.toArray(
                    new DBInformation[0]));
        }
    }//GEN-LAST:event_addInformationButtonActionPerformed

    /**
     * Gibt alle Sprachen, in denen die Information noch nicht verfasst wurde.
     * @param isEdit wenn ungleich null, wird die Information bearbeitet, d.h. 
     * auch die Sprache in der die Info verfasst wurde wird hinzugefügt
     * @return Sprachen
     */
    private Language[] getLanguages(Language language) {
        List<Language> languages = new ArrayList<Language>();
        languages.addAll(Arrays.asList(Language.values()));

        for (DBInformation info : this.informations) {
            languages.remove(info.getLanguage());
        }

        if (language != null) {
            languages.add(0, language);
        }

        return languages.toArray(new Language[0]);
    }

    /**
     * Bearbeiten einer Information in einer Sprache.
     * @param evt Event
     */
    private void editInformationButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editInformationButtonActionPerformed

        DBInformation info =
                this.informations.get(this.informationsList.getSelectedIndex());
        AddEditInformationFrame editInformationFrame =
                new AddEditInformationFrame(null, true,
                info, "Information bearbeiten", "Bearbeiten",
                getLanguages(info.getLanguage()));
        editInformationFrame.setBounds(this.getBounds().x + 50, this.getBounds().y + 50,
                editInformationFrame.getWidth(), editInformationFrame.getHeight());
        editInformationFrame.setVisible(true);
        this.informationsList.setListData(this.informations.toArray(
                new DBInformation[0]));

    }//GEN-LAST:event_editInformationButtonActionPerformed

    /**
     * Löschen einer Information in einer Sprache.
     * @param evt Event
     */
    private void deleteInformationButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteInformationButtonActionPerformed
        int result = JOptionPane.showConfirmDialog(null, "Wollen Sie die "
                + "Information wirklich löschen?", "Löschen",
                JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if (result == JOptionPane.YES_OPTION) {
            this.removedInformations.add((DBInformation) this.informationsList.getSelectedValue());
            this.informations.remove(this.informationsList.getSelectedIndex());
            this.informationsList.setListData(this.informations.toArray(
                    new DBInformation[0]));
        }

    }//GEN-LAST:event_deleteInformationButtonActionPerformed

    /**
     * Ändern der Information entsprechend der ausgewählten Sprache. 
     * @param evt Event
     */
    private void informationsListValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_informationsListValueChanged

        int index = this.informationsList.getSelectedIndex();
        if (index != -1) {
            this.informationTextArea.setText(
                    this.informations.get(index).getInformation());
            this.editInformationButton.setEnabled(true);
            this.deleteInformationButton.setEnabled(true);
        } else {
            this.informationTextArea.setText("");
            this.editInformationButton.setEnabled(false);
            this.deleteInformationButton.setEnabled(false);
        }
    }//GEN-LAST:event_informationsListValueChanged

    private void questionsListValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_questionsListValueChanged

        if (this.questionsList.getSelectedIndex() != -1) {
            this.editQuestionButton.setEnabled(true);
            this.deleteQuestionButton.setEnabled(true);
        } else {
            this.editQuestionButton.setEnabled(false);
            this.deleteQuestionButton.setEnabled(false);
        }
    }//GEN-LAST:event_questionsListValueChanged

    /**
     * Prüft. ob alle Eingabe vom Nutzer richtig eingegeben worden sind.
     * @return true, wenn alles korrekt ist
     */
    private boolean isInputCorrect() {
        boolean inputCorrect = true;
        if (this.nameTextField.getText().replace(" ", "").isEmpty()) {
            inputCorrect = false;
            this.nameTextField.setBorder(BorderFactory.createLineBorder(Color.RED));
            JOptionPane.showMessageDialog(null, "Bitte einen Namen für den Zielpunkt angeben!",
                    MSG_ERROR_CAPTION, JOptionPane.ERROR_MESSAGE);
            this.nameTextField.requestFocusInWindow();
        } else if (this.rangeTextField.getText().replace(" ", "").isEmpty()
                || Integer.valueOf(this.rangeTextField.getText()) == 0) {
            inputCorrect = false;
            this.rangeTextField.setBorder(BorderFactory.createLineBorder(Color.RED));
            JOptionPane.showMessageDialog(null, "Bitte eine Mindest-Reichweite zum Zielpunkt angeben.",
                    MSG_ERROR_CAPTION, JOptionPane.ERROR_MESSAGE);
            this.rangeTextField.requestFocusInWindow();
        } else if (this.latitudeTextField.getText().replace(" ", "").isEmpty()) {
            inputCorrect = false;
            this.latitudeTextField.setBorder(BorderFactory.createLineBorder(Color.RED));
            JOptionPane.showMessageDialog(null, "Bitte den Längengrad für die Position angeben!",
                    MSG_ERROR_CAPTION, JOptionPane.ERROR_MESSAGE);
            this.latitudeTextField.requestFocusInWindow();
        } else if (this.longitudeTextField.getText().replace(" ", "").isEmpty()) {
            inputCorrect = false;
            this.longitudeTextField.setBorder(BorderFactory.createLineBorder(Color.RED));
            JOptionPane.showMessageDialog(null, "Bitte den Breitengrad für die Position angeben!",
                    MSG_ERROR_CAPTION, JOptionPane.ERROR_MESSAGE);
            this.longitudeTextField.requestFocusInWindow();
        } else if (this.altitudeTextField.getText().replace(" ", "").isEmpty()) {
            inputCorrect = false;
            this.altitudeTextField.setBorder(BorderFactory.createLineBorder(Color.RED));
            JOptionPane.showMessageDialog(null, "Bitte den Höhe für die Position angeben!",
                    MSG_ERROR_CAPTION, JOptionPane.ERROR_MESSAGE);
            this.altitudeTextField.requestFocusInWindow();
        } else if (this.townTextField.getText().replace(" ", "").isEmpty()) {
            inputCorrect = false;
            this.townTextField.setBorder(BorderFactory.createLineBorder(Color.RED));
            JOptionPane.showMessageDialog(null, "Bitte die Stadt angeben!",
                    MSG_ERROR_CAPTION, JOptionPane.ERROR_MESSAGE);
            this.townTextField.requestFocusInWindow();
        } else if (this.postcodeTextField.getText().replace(" ", "").isEmpty()) {
            inputCorrect = false;
            this.postcodeTextField.setBorder(BorderFactory.createLineBorder(Color.RED));
            JOptionPane.showMessageDialog(null, "Bitte eine Postleitzahl angeben!",
                    MSG_ERROR_CAPTION, JOptionPane.ERROR_MESSAGE);
            this.postcodeTextField.requestFocusInWindow();
        } else if (this.streetTextField.getText().replace(" ", "").isEmpty()) {
            inputCorrect = false;
            this.streetTextField.setBorder(BorderFactory.createLineBorder(Color.RED));
            JOptionPane.showMessageDialog(null, "Bitte eine Straße angeben!",
                    MSG_ERROR_CAPTION, JOptionPane.ERROR_MESSAGE);
            this.streetTextField.requestFocusInWindow();
        }
        return inputCorrect;
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addEditTargetPointButton;
    private javax.swing.JButton addInformationButton;
    private javax.swing.JButton addQuestionButton;
    private javax.swing.JLabel altitudeLabel;
    private javax.swing.JLabel altitudeMeterLabel;
    private javax.swing.JTextField altitudeTextField;
    private javax.swing.JButton cancelButton;
    private javax.swing.JButton deleteInformationButton;
    private javax.swing.JButton deleteQuestionButton;
    private javax.swing.JButton editInformationButton;
    private javax.swing.JButton editQuestionButton;
    private javax.swing.JLabel idLable;
    private javax.swing.JLabel idTextLable;
    private javax.swing.JLabel infoLabel;
    private javax.swing.JScrollPane informationScrollPane;
    private javax.swing.JTextArea informationTextArea;
    private javax.swing.JList informationsList;
    private javax.swing.JScrollPane informationsScrollPane;
    private javax.swing.JLabel latitudeLabel;
    private javax.swing.JTextField latitudeTextField;
    private javax.swing.JLabel longitudeLabel;
    private javax.swing.JTextField longitudeTextField;
    private javax.swing.JLabel nameLabel;
    private javax.swing.JTextField nameTextField;
    private javax.swing.JPanel positionPanel;
    private javax.swing.JLabel postcodeLabel;
    private javax.swing.JTextField postcodeTextField;
    private javax.swing.JList questionsList;
    private javax.swing.JPanel questionsPanel;
    private javax.swing.JScrollPane questionsScrollPane;
    private javax.swing.JLabel rangeLabel;
    private javax.swing.JLabel rangeMeterLabel;
    private javax.swing.JTextField rangeTextField;
    private javax.swing.JLabel streetLabel;
    private javax.swing.JTextField streetTextField;
    private javax.swing.JPanel targetPointPanel;
    private javax.swing.JLabel townLabel;
    private javax.swing.JTextField townTextField;
    private javax.swing.JComboBox typeComboBox;
    private javax.swing.JLabel typeLabel;
    // End of variables declaration//GEN-END:variables
}
