package Utility;

import Definitions.TargetPointType;
import Exceptions.ApplicationException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Vector;
import javax.swing.ComboBoxModel;
import javax.swing.event.ListDataListener;

/**
 * Model für Zielpunkt-Typen.
 */
public class TargetPointTypeModel implements ComboBoxModel {

    /** Liste der Zielpunkttypen. */
    private ArrayList<TargetPointType> targetPointTypes;
    /** aktuell selektierter Zielpunkttyp. */
    private TargetPointType selectedTargetPointType;
    /** Zielpunkt-Namen */
    private ArrayList<String> targetPointTypesNames;

    /**
     * Model für Zielpunkttypen erzeugen.
     * @param Strings Zielpunkttypen
     */
    public TargetPointTypeModel(TargetPointType[] targetPointTypes) {
        this.targetPointTypes = new ArrayList<TargetPointType>();
        this.targetPointTypes.addAll(Arrays.asList(targetPointTypes));
        this.targetPointTypesNames = new ArrayList<String>();
        this.targetPointTypesNames.addAll(Arrays.asList(TargetPointType.getTargetPointTypes()));
    }

    /**
     * Model für Zielpunkttypen erzeugen.
     * @param Strings Zielpunkttypen
     */
    public TargetPointTypeModel(ArrayList<TargetPointType> targetPointTypes) {
        this.targetPointTypes = targetPointTypes;
        this.targetPointTypesNames = new ArrayList<String>();
        this.targetPointTypesNames.addAll(Arrays.asList(TargetPointType.getTargetPointTypes()));
    }

    /**
     * Model für Zielpunkttypen erzeugen.
     * @param Strings Zielpunkttypen
     */
    public TargetPointTypeModel(Vector<TargetPointType> targetPointTypes) {
        this.targetPointTypes = new ArrayList<TargetPointType>(targetPointTypes);
        this.targetPointTypesNames = new ArrayList<String>();
        this.targetPointTypesNames.addAll(Arrays.asList(TargetPointType.getTargetPointTypes()));
    }

    /**
     * Setzen des aktuellen Zielpunkts.
     * @param anItem aktueller Zielpunkt
     */
    @Override
    public void setSelectedItem(Object anItem) {
        try {
            this.selectedTargetPointType = TargetPointType.getTargetPointByName((String) anItem);
        } catch (ApplicationException ex) {
            /* kann in diesem Fall nicht so zu einem Fehler führen, daher keine
             * Aktion */
        }
    }

    /**
     * Gibt den aktuellen Zielpunkt.
     * @param anItem aktueller Zielpunkt
     */
    @Override
    public String getSelectedItem() {
        return selectedTargetPointType != null
                ? TargetPointType.getTargetPointType(selectedTargetPointType)
                : "";
    }

    /**
     * Gibt den aktuellen Zielpunkt.
     * @param anItem aktueller Zielpunkt
     */
    public TargetPointType getSelectedTargetPointType() {
        return selectedTargetPointType;
    }

    /**
     * Gibt die Anzahl der Zielpunkttypen.
     * @return Anzahl der Zielpunkttypen
     */
    @Override
    public int getSize() {
        return this.targetPointTypes.size();
    }

    /**
     * Gibt den Zielpunkttypen an bestimmter Stelle.
     * @param index Stelle
     * @return Zielpunkttyp
     */
    @Override
    public String getElementAt(int index) {
        return this.targetPointTypesNames.get(index);
    }

    /**
     * Nicht verwendet.
     * @param l 
     */
    @Override
    public void addListDataListener(ListDataListener l) {
        /* nicht verwendet */
    }

    /**
     * Nicht verwendet.
     * @param l 
     */
    @Override
    public void removeListDataListener(ListDataListener l) {
        /* nicht verwendet */
    }
}
