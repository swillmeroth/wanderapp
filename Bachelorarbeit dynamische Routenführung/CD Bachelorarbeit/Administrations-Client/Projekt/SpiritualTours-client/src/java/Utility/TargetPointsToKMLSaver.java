package Utility;

import DTO.Database.DBInformation;
import DTO.Database.DBTargetPoint;
import Definitions.Language;
import Definitions.Language;
import Definitions.TargetPointType;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.GregorianCalendar;

/**
 * Bietet Methoden, die Zielpunkte als KML-Datei zu speichern.
 */
public final class TargetPointsToKMLSaver {

    /* ====================================================================== */
    /*                       Konstanten                                       */
    /* ====================================================================== */
    /** Icon der Zielpunkte. */
    private static final String ICON_URL =
            "http://maps.google.com/mapfiles/kml/pal5/icon13.png";
    /* ---------------------------------------------------------------------- */
    /** Style-Farbe Highlight für Friedhöfe. */
    private static final String CEMETERY_STYLE_COLOR = "ff000000";
    /** Style-Farbe für Kapellen. */
    private static final String CHAPEL_STYLE_COLOR = "ff008008";
    /** Style-Farbe für Kirchen. */
    private static final String CHURCH_STYLE_COLOR = "ff9900bb";
    /** Style-Farbe für jüdische Friedhöfe. */
    private static final String JEWISH_CEMETERY_STYLE_COLOR = "ff525252";
    /** Style-Farbe für Klöster. */
    private static final String MONASTERY_STYLE_COLOR = "ffee7711";
    /** Style-Farbe für Denkmäler. */
    private static final String MONUMENT_STYLE_COLOR = "ff00ffff";
    /** Style-Farbe für Standbilder. */
    private static final String STATUE_STYLE_COLOR = "ff0077ff";
    /** Style-Farbe für Wegekreuze. */
    private static final String WAYSIDE_CROSS_STYLE_COLOR = "ff20549c";
    /* ---------------------------------------------------------------------- */
    /** Style-ID Highlight für Friedhöfe. */
    private static final String CEMETERY_STYLE_H_ID = "cemeteryStyleHighlight";
    /** Style-ID Highlight für Kapellen. */
    private static final String CHAPEL_STYLE_H_ID = "chapelStyleHighlight";
    /** Style-ID Highlight für Kirchen. */
    private static final String CHURCH_STYLE_H_ID = "churchStyleHighlight";
    /** Style-ID Highlight für jüdische Friedhöfe. */
    private static final String JEWISH_CEMETERY_STYLE_H_ID =
            "jewischCemeteryStyleHighlight";
    /** Style-ID Highlight für Klöster. */
    private static final String MONASTERY_STYLE_H_ID = "monasteryStyleHighlight";
    /** Style-ID Highlight für Denkmäler. */
    private static final String MONUMENT_STYLE_H_ID = "monumentStyleHighlight";
    /** Style-ID Highlight für Standbilder. */
    private static final String STATUE_STYLE_H_ID = "statueStyleHighlight";
    /** Style-ID Highlight für Wegekreuze. */
    private static final String WAYSIDE_CROSS_STYLE_H_ID = "waysideCrossStyleHighlight";
    /* ---------------------------------------------------------------------- */
    /** Style-ID Normal für Friedhöfe. */
    private static final String CEMETERY_STYLE_N_ID = "cemeteryStyleNormal";
    /** Style-ID Normal für Kapellen. */
    private static final String CHAPEL_STYLE_N_ID = "chapelStyleNormal";
    /** Style-ID Normal für Kirchen. */
    private static final String CHURCH_STYLE_N_ID = "churchStyleNormal";
    /** Style-ID Normal für jüdische Friedhöfe. */
    private static final String JEWISH_CEMETERY_STYLE_N_ID =
            "jewischCemeteryStyleNormal";
    /** Style-ID Normal für Klöster. */
    private static final String MONASTERY_STYLE_N_ID = "monasteryStyleNormal";
    /** Style-ID Normal für Denkmäler. */
    private static final String MONUMENT_STYLE_N_ID = "monumentStyleNormal";
    /** Style-ID Normal für Standbilder. */
    private static final String STATUE_STYLE_N_ID = "statueStyleNormal";
    /** Style-ID Normal für Wegekreuze. */
    private static final String WAYSIDE_CROSS_STYLE_N_ID = "waysideCrossStyleNormal";
    /* ---------------------------------------------------------------------- */
    /** Style für Friedhöfe */
    private static final String CEMETERY_STYLE =
            "<Style id=\"" + CEMETERY_STYLE_H_ID + "\">\n"
            + "<IconStyle>\n"
            + "<color>" + CEMETERY_STYLE_COLOR + "</color>\n"
            + "<scale>1.3</scale>\n"
            + "<Icon>\n"
            + "<href>" + ICON_URL + "</href>\n"
            + "</Icon>\n"
            + "<hotSpot x=\"20\" y=\"2\" xunits=\"pixels\" yunits=\"pixels\"/>\n"
            + "</IconStyle>\n"
            + "</Style>\n"
            + "<Style id=\"" + CEMETERY_STYLE_N_ID + "\">\n"
            + "<IconStyle>\n"
            + "<color>" + CEMETERY_STYLE_COLOR + "</color>\n"
            + "<scale>1.1</scale>\n"
            + "<Icon>\n"
            + "<href>" + ICON_URL + "</href>\n"
            + "</Icon>\n"
            + "<hotSpot x=\"20\" y=\"2\" xunits=\"pixels\" yunits=\"pixels\"/>\n"
            + "</IconStyle>\n"
            + "</Style>";
    /** Style für Kapellen */
    private static final String CHAPEL_STYLE =
            "<Style id=\"" + CHAPEL_STYLE_H_ID + "\">\n"
            + "<IconStyle>\n"
            + "<color>" + CHAPEL_STYLE_COLOR + "</color>\n"
            + "<scale>1.3</scale>\n"
            + "<Icon>\n"
            + "<href>" + ICON_URL + "</href>\n"
            + "</Icon>\n"
            + "<hotSpot x=\"20\" y=\"2\" xunits=\"pixels\" yunits=\"pixels\"/>\n"
            + "</IconStyle>\n"
            + "</Style>\n"
            + "<Style id=\"" + CHAPEL_STYLE_N_ID + "\">\n"
            + "<IconStyle>\n"
            + "<color>" + CHAPEL_STYLE_COLOR + "</color>\n"
            + "<scale>1.1</scale>\n"
            + "<Icon>\n"
            + "<href>" + ICON_URL + "</href>\n"
            + "</Icon>\n"
            + "<hotSpot x=\"20\" y=\"2\" xunits=\"pixels\" yunits=\"pixels\"/>\n"
            + "</IconStyle>\n"
            + "</Style>";
    /** Style für Kirchen */
    private static final String CHURCH_STYLE =
            "<Style id=\"" + CHURCH_STYLE_H_ID + "\">\n"
            + "<IconStyle>\n"
            + "<color>" + CHURCH_STYLE_COLOR + "</color>\n"
            + "<scale>1.3</scale>\n"
            + "<Icon>\n"
            + "<href>" + ICON_URL + "</href>\n"
            + "</Icon>\n"
            + "<hotSpot x=\"20\" y=\"2\" xunits=\"pixels\" yunits=\"pixels\"/>\n"
            + "</IconStyle>\n"
            + "</Style>\n"
            + "<Style id=\"" + CHURCH_STYLE_N_ID + "\">\n"
            + "<IconStyle>\n"
            + "<color>" + CHURCH_STYLE_COLOR + "</color>\n"
            + "<scale>1.1</scale>\n"
            + "<Icon>\n"
            + "<href>" + ICON_URL + "</href>\n"
            + "</Icon>\n"
            + "<hotSpot x=\"20\" y=\"2\" xunits=\"pixels\" yunits=\"pixels\"/>\n"
            + "</IconStyle>\n"
            + "</Style>";
    /** Style für jüdische Friedhöfe */
    private static final String JEWISH_CEMETERY_STYLE =
            "<Style id=\"" + JEWISH_CEMETERY_STYLE_H_ID + "\">\n"
            + "<IconStyle>\n"
            + "<color>" + JEWISH_CEMETERY_STYLE_COLOR + "</color>\n"
            + "<scale>1.3</scale>\n"
            + "<Icon>\n"
            + "<href>" + ICON_URL + "</href>\n"
            + "</Icon>\n"
            + "<hotSpot x=\"20\" y=\"2\" xunits=\"pixels\" yunits=\"pixels\"/>\n"
            + "</IconStyle>\n"
            + "</Style>\n"
            + "<Style id=\"" + JEWISH_CEMETERY_STYLE_N_ID + "\">\n"
            + "<IconStyle>\n"
            + "<color>" + JEWISH_CEMETERY_STYLE_COLOR + "</color>\n"
            + "<scale>1.1</scale>\n"
            + "<Icon>\n"
            + "<href>" + ICON_URL + "</href>\n"
            + "</Icon>\n"
            + "<hotSpot x=\"20\" y=\"2\" xunits=\"pixels\" yunits=\"pixels\"/>\n"
            + "</IconStyle>\n"
            + "</Style>";
    /** Style für Klöster */
    private static final String MONASTERY_STYLE =
            "<Style id=\"" + MONASTERY_STYLE_H_ID + "\">\n"
            + "<IconStyle>\n"
            + "<color>" + MONASTERY_STYLE_COLOR + "</color>\n"
            + "<scale>1.3</scale>\n"
            + "<Icon>\n"
            + "<href>" + ICON_URL + "</href>\n"
            + "</Icon>\n"
            + "<hotSpot x=\"20\" y=\"2\" xunits=\"pixels\" yunits=\"pixels\"/>\n"
            + "</IconStyle>\n"
            + "</Style>\n"
            + "<Style id=\"" + MONASTERY_STYLE_N_ID + "\">\n"
            + "<IconStyle>\n"
            + "<color>" + MONASTERY_STYLE_COLOR + "</color>\n"
            + "<scale>1.1</scale>\n"
            + "<Icon>\n"
            + "<href>" + ICON_URL + "</href>\n"
            + "</Icon>\n"
            + "<hotSpot x=\"20\" y=\"2\" xunits=\"pixels\" yunits=\"pixels\"/>\n"
            + "</IconStyle>\n"
            + "</Style>";
    /** Style für Denkmäler */
    private static final String MONUMENT_STYLE =
            "<Style id=\"" + MONUMENT_STYLE_H_ID + "\">\n"
            + "<IconStyle>\n"
            + "<color>" + MONUMENT_STYLE_COLOR + "</color>\n"
            + "<scale>1.3</scale>\n"
            + "<Icon>\n"
            + "<href>" + ICON_URL + "</href>\n"
            + "</Icon>\n"
            + "<hotSpot x=\"20\" y=\"2\" xunits=\"pixels\" yunits=\"pixels\"/>\n"
            + "</IconStyle>\n"
            + "</Style>\n"
            + "<Style id=\"" + MONUMENT_STYLE_N_ID + "\">\n"
            + "<IconStyle>\n"
            + "<color>" + MONUMENT_STYLE_COLOR + "</color>\n"
            + "<scale>1.1</scale>\n"
            + "<Icon>\n"
            + "<href>" + ICON_URL + "</href>\n"
            + "</Icon>\n"
            + "<hotSpot x=\"20\" y=\"2\" xunits=\"pixels\" yunits=\"pixels\"/>\n"
            + "</IconStyle>\n"
            + "</Style>";
    /** Style für Standbilder */
    private static final String STATUE_STYLE =
            "<Style id=\"" + STATUE_STYLE_H_ID + "\">\n"
            + "<IconStyle>\n"
            + "<color>" + STATUE_STYLE_COLOR + "</color>\n"
            + "<scale>1.3</scale>\n"
            + "<Icon>\n"
            + "<href>" + ICON_URL + "</href>\n"
            + "</Icon>\n"
            + "<hotSpot x=\"20\" y=\"2\" xunits=\"pixels\" yunits=\"pixels\"/>\n"
            + "</IconStyle>\n"
            + "</Style>\n"
            + "<Style id=\"" + STATUE_STYLE_N_ID + "\">\n"
            + "<IconStyle>\n"
            + "<color>" + STATUE_STYLE_COLOR + "</color>\n"
            + "<scale>1.1</scale>\n"
            + "<Icon>\n"
            + "<href>" + ICON_URL + "</href>\n"
            + "</Icon>\n"
            + "<hotSpot x=\"20\" y=\"2\" xunits=\"pixels\" yunits=\"pixels\"/>\n"
            + "</IconStyle>\n"
            + "</Style>";
    /** Style für Wegekreuze */
    private static final String WAYSIDE_CROSS_STYLE =
            "<Style id=\"" + WAYSIDE_CROSS_STYLE_H_ID + "\">\n"
            + "<IconStyle>\n"
            + "<color>" + WAYSIDE_CROSS_STYLE_COLOR + "</color>\n"
            + "<scale>1.3</scale>\n"
            + "<Icon>\n"
            + "<href>" + ICON_URL + "</href>\n"
            + "</Icon>\n"
            + "<hotSpot x=\"20\" y=\"2\" xunits=\"pixels\" yunits=\"pixels\"/>\n"
            + "</IconStyle>\n"
            + "</Style>\n"
            + "<Style id=\"" + WAYSIDE_CROSS_STYLE_N_ID + "\">\n"
            + "<IconStyle>\n"
            + "<color>" + WAYSIDE_CROSS_STYLE_COLOR + "</color>\n"
            + "<scale>1.1</scale>\n"
            + "<Icon>\n"
            + "<href>" + ICON_URL + "</href>\n"
            + "</Icon>\n"
            + "<hotSpot x=\"20\" y=\"2\" xunits=\"pixels\" yunits=\"pixels\"/>\n"
            + "</IconStyle>\n"
            + "</Style>";
    /* ---------------------------------------------------------------------- */
    /** Style-Map-ID für Friedhöfe. */
    private static final String CEMETERY_STYLE_MAP_ID = "cemeteryStyleMap";
    /** Style-Map-ID für Kapellen. */
    private static final String CHAPEL_STYLE_MAP_ID = "chapelStyleMap";
    /** Style-Map-ID für Kirchen. */
    private static final String CHURCH_STYLE_MAP_ID = "churchStyleMap";
    /** Style-Map-ID für jüdische Friedhöfe. */
    private static final String JEWISH_CEMETERY_STYLE_MAP_ID =
            "jewischCemeteryStyleMap";
    /** Style-Map-ID für Klöster. */
    private static final String MONASTERY_STYLE_MAP_ID = "monasteryStyleMap";
    /** Style-Map-ID für Denkmäler. */
    private static final String MONUMENT_STYLE_MAP_ID = "monumentStyleMap";
    /** Style-Map-ID für Standbilder. */
    private static final String STATUE_STYLE_MAP_ID = "statueStyleMap";
    /** Style-Map-ID für Wegekreuze. */
    private static final String WAYSIDE_CROSS_STYLE_MAP_ID = "waysideCrossStyleMap";
    /* ---------------------------------------------------------------------- */
    /** Style-Map für Friedhöfe. */
    private static final String CEMETERY_STYLE_MAP =
            "<StyleMap id=\"" + CEMETERY_STYLE_MAP_ID + "\">\n"
            + "<Pair>\n"
            + "<key>normal</key>\n"
            + "<styleUrl>#" + CEMETERY_STYLE_N_ID + "</styleUrl>\n"
            + "</Pair>\n"
            + "<Pair>\n"
            + "<key>highlight</key>\n"
            + "<styleUrl>#" + CEMETERY_STYLE_H_ID + "</styleUrl>\n"
            + "</Pair>\n"
            + "</StyleMap>";
    /** Style-Map für Kapellen. */
    private static final String CHAPEL_STYLE_MAP =
            "<StyleMap id=\"" + CHAPEL_STYLE_MAP_ID + "\">\n"
            + "<Pair>\n"
            + "<key>normal</key>\n"
            + "<styleUrl>#" + CHAPEL_STYLE_N_ID + "</styleUrl>\n"
            + "</Pair>\n"
            + "<Pair>\n"
            + "<key>highlight</key>\n"
            + "<styleUrl>#" + CHAPEL_STYLE_H_ID + "</styleUrl>\n"
            + "</Pair>\n"
            + "</StyleMap>";
    /** Style-Map für Kirchen. */
    private static final String CHURCH_STYLE_MAP =
            "<StyleMap id=\"" + CHURCH_STYLE_MAP_ID + "\">\n"
            + "<Pair>\n"
            + "<key>normal</key>\n"
            + "<styleUrl>#" + CHURCH_STYLE_N_ID + "</styleUrl>\n"
            + "</Pair>\n"
            + "<Pair>\n"
            + "<key>highlight</key>\n"
            + "<styleUrl>#" + CHURCH_STYLE_H_ID + "</styleUrl>\n"
            + "</Pair>\n"
            + "</StyleMap>";
    /** Style-Map für jüdische Friedhöfe. */
    private static final String JEWISH_CEMETERY_STYLE_MAP =
            "<StyleMap id=\"" + JEWISH_CEMETERY_STYLE_MAP_ID + "\">\n"
            + "<Pair>\n"
            + "<key>normal</key>\n"
            + "<styleUrl>#" + JEWISH_CEMETERY_STYLE_N_ID + "</styleUrl>\n"
            + "</Pair>\n"
            + "<Pair>\n"
            + "<key>highlight</key>\n"
            + "<styleUrl>#" + JEWISH_CEMETERY_STYLE_H_ID + "</styleUrl>\n"
            + "</Pair>\n"
            + "</StyleMap>";
    /** Style-Map für Klöster. */
    private static final String MONASTERY_STYLE_MAP =
            "<StyleMap id=\"" + MONASTERY_STYLE_MAP_ID + "\">\n"
            + "<Pair>\n"
            + "<key>normal</key>\n"
            + "<styleUrl>#" + MONASTERY_STYLE_N_ID + "</styleUrl>\n"
            + "</Pair>\n"
            + "<Pair>\n"
            + "<key>highlight</key>\n"
            + "<styleUrl>#" + MONASTERY_STYLE_H_ID + "</styleUrl>\n"
            + "</Pair>\n"
            + "</StyleMap>";
    /** Style-Map für Denkmäler. */
    private static final String MONUMENT_STYLE_MAP =
            "<StyleMap id=\"" + MONUMENT_STYLE_MAP_ID + "\">\n"
            + "<Pair>\n"
            + "<key>normal</key>\n"
            + "<styleUrl>#" + MONUMENT_STYLE_N_ID + "</styleUrl>\n"
            + "</Pair>\n"
            + "<Pair>\n"
            + "<key>highlight</key>\n"
            + "<styleUrl>#" + MONUMENT_STYLE_H_ID + "</styleUrl>\n"
            + "</Pair>\n"
            + "</StyleMap>";
    /** Style-Map für Standbilder. */
    private static final String STATUE_STYLE_MAP =
            "<StyleMap id=\"" + STATUE_STYLE_MAP_ID + "\">\n"
            + "<Pair>\n"
            + "<key>normal</key>\n"
            + "<styleUrl>#" + STATUE_STYLE_N_ID + "</styleUrl>\n"
            + "</Pair>\n"
            + "<Pair>\n"
            + "<key>highlight</key>\n"
            + "<styleUrl>#" + STATUE_STYLE_H_ID + "</styleUrl>\n"
            + "</Pair>\n"
            + "</StyleMap>";
    /** Style-Map für Wegekreuze. */
    private static final String WAYSIDE_CROSS_STYLE_MAP =
            "<StyleMap id=\"" + WAYSIDE_CROSS_STYLE_MAP_ID + "\">\n"
            + "<Pair>\n"
            + "<key>normal</key>\n"
            + "<styleUrl>#" + WAYSIDE_CROSS_STYLE_N_ID + "</styleUrl>\n"
            + "</Pair>\n"
            + "<Pair>\n"
            + "<key>highlight</key>\n"
            + "<styleUrl>#" + WAYSIDE_CROSS_STYLE_H_ID + "</styleUrl>\n"
            + "</Pair>\n"
            + "</StyleMap>";
    /* ---------------------------------------------------------------------- */
    /** Anfang der KML-Datei. */
    private static final String KML_HEADER = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
            + "<kml xmlns=\"http://www.opengis.net/kml/2.2\">\n" + "<Document>";
    /** Ende der KML-Datei. */
    private static final String KML_FOOTER = "</Document>\n" + "</kml>";

    /* ====================================================================== */
    /*                       Public-Methoden                                  */
    /* ====================================================================== */
    /**
     * Speichert die Zielpunkte als KML-Datei
     * @param filename Dateiname der KML-Datei
     * @param targetPoints Zielpunkte
     */
    public static void targetPoints2KML(String filename,
            Collection<DBTargetPoint> targetPoints) throws IOException {

        if (!filename.toLowerCase().endsWith(".kml")) {
            filename += ".kml";
        }

        /* anlegen der KML-Datei auf der SD-Karte. */
        FileWriter kmlWriter = new FileWriter(filename);
        BufferedWriter out = new BufferedWriter(kmlWriter);

        /* Kopf der KML-Datei */
        out.write(KML_HEADER);
        out.newLine();

        /* Name der KML-Datei */
        out.write(getKMLName());
        out.newLine();

        /* Beschreibung der KML-Datei */
        out.write(getDescription());
        out.newLine();

        /* Beschreibung der KML-Datei */
        out.write(getStyles());
        out.newLine();

        /* Zielpunkte der KML-Datei setzen */
        out.write(getTargetPoints(targetPoints));

        /* Wegpunkte der KML-Datei setzen */
        out.write(KML_FOOTER);

        out.close();
        kmlWriter.close();
    }

    /**
     * Speichert die Zielpunkte als KML-Datei
     * @param filename Dateiname der KML-Datei
     * @param targetPoints Zielpunkte
     */
    public static void targetPoints2KML(String filename,
            DBTargetPoint[] targetPoints) throws IOException {
        
        Collection<DBTargetPoint> targetPointCollection = new ArrayList<DBTargetPoint>();
        targetPointCollection.addAll(Arrays.asList(targetPoints));

        targetPoints2KML(filename, targetPointCollection);
    }

    /**
     * Speichert die Zielpunkt als KML-Datei
     * @param filename Dateiname der KML-Datei
     * @param targetPoints Zielpunkt
     */
    public static void targetPoints2KML(String filename, DBTargetPoint targetPoint) throws IOException {

        Collection<DBTargetPoint> targetPoints = new ArrayList<DBTargetPoint>();
        targetPoints.add(targetPoint);

        targetPoints2KML(filename, targetPoints);
    }

    /* ====================================================================== */
    /*                       Hilfsmethoden                                    */
    /* ====================================================================== */
    /**
     * Gibt den Namen der KML.
     * 
     * @return Name der KML
     */
    private static String getKMLName() {
        return "<name>Zielpunkte</name>";
    }

    /**
     * Gibt die Beschreibung der KML.
     * @return Beschreibung der KML
     */
    private static String getDescription() {

        return "<description>\nSicherung vom "
                + new GregorianCalendar().getTime().toString() + "\n</description>";
    }

    /**
     * Gibt die Styles für die KML-Datei.
     * @return Styles
     */
    private static String getStyles() {
        return CEMETERY_STYLE + "\n" + CHAPEL_STYLE + "\n" + CHURCH_STYLE
                + "\n" + JEWISH_CEMETERY_STYLE + "\n" + MONASTERY_STYLE + "\n"
                + MONUMENT_STYLE + "\n" + STATUE_STYLE + "\n"
                + WAYSIDE_CROSS_STYLE + "\n" + CEMETERY_STYLE_MAP + "\n" 
                + CHAPEL_STYLE_MAP + "\n" + CHURCH_STYLE_MAP + "\n" 
                + JEWISH_CEMETERY_STYLE_MAP + "\n" + MONASTERY_STYLE_MAP + "\n" 
                + MONUMENT_STYLE_MAP + "\n" + STATUE_STYLE_MAP + "\n" 
                + WAYSIDE_CROSS_STYLE_MAP;
    }

    /**
     * Gibt die Zielpunkte.
     * 
     * @param context
     *            Context
     * @param targetPoint
     *            Zielpunkte
     * @return Zielpunkte
     */
    private static String getTargetPoints(Collection<DBTargetPoint> targetPoints) {

        String points = "";

        for (DBTargetPoint targetPoint : targetPoints) {
            points += getTargetPoint(targetPoint);
        }

        return points;
    }

    /**
     * Gibt den Zielpunkt.
     * 
     * @param targetPoint Zielpunkt
     * @return Zielpunkt
     */
    private static String getTargetPoint(DBTargetPoint targetPoint) {

        String targetPointType = TargetPointType.getTargetPointType(targetPoint.getTargetPointType());

        return "<Placemark>\n"
                + "<name>" + targetPoint.getName() + "</name>\n"
                + getStyleMap(targetPoint.getTargetPointType()) + "\n"
                + "<description>\nZielpunkttyp : "
                + targetPointType + "\n" + getTargetPointInformations(
                targetPoint.getInformations().toArray(new DBInformation[0]))
                + "\n</description>\n"
                + "<Point>\n"
                + "<coordinates>"
                + targetPoint.getPosition().getLongitude() + ", "
                + targetPoint.getPosition().getLatitude()
                + "</coordinates>\n"
                + "</Point>\n"
                + "</Placemark>\n";
    }

    /**
     * Gibt die passende StyleMap.
     * @return StyleMap
     */
    private static String getStyleMap(TargetPointType targetPointType) {
        
        String styleMap = "";
        
        switch (targetPointType) {
            case CEMETERY:
                styleMap = "<styleUrl>#" + CEMETERY_STYLE_MAP_ID + "</styleUrl>";
                break;
            case CHAPEL:
                styleMap = "<styleUrl>#" + CHAPEL_STYLE_MAP_ID + "</styleUrl>";
                break;
            case CHURCH:
                styleMap = "<styleUrl>#" + CHURCH_STYLE_MAP_ID + "</styleUrl>";
                break;
            case JEWISH_CEMETERY:
                styleMap = "<styleUrl>#" + JEWISH_CEMETERY_STYLE_MAP_ID + "</styleUrl>";
                break;
            case MONASTERY:
                styleMap = "<styleUrl>#" + MONASTERY_STYLE_MAP_ID + "</styleUrl>";
                break;
            case MONUMENT:
                styleMap = "<styleUrl>#" + MONUMENT_STYLE_MAP_ID + "</styleUrl>";
                break;
            case STATUE:
                styleMap = "<styleUrl>#" + STATUE_STYLE_MAP_ID + "</styleUrl>";
                break;
            case WAYSIDE_CROSS:
                styleMap = "<styleUrl>#" + WAYSIDE_CROSS_STYLE_MAP_ID + "</styleUrl>";
                break;
            default:
                styleMap = "";
                break;
        }
        
        return styleMap;
    }
    
    /**
     * Gibt die Informationen des Zielpunkts.
     * 
     * @param informations Informationen
     * @return Informationen
     */
    private static String getTargetPointInformations(DBInformation[] informations) {

        String informationsString = informations.length > 0
                ? "\n" + Language.getLanguage(informations[0].getLanguage()) 
                + "\n" + informations[0].getInformation() : "";

        for (int i = 1; i < informations.length; i++) {
            informationsString += "\n\n"
                    + Language.getLanguage(informations[i].getLanguage()) + "\n"
                    + informations[i].getInformation();
        }

        return informationsString;
    }
}
