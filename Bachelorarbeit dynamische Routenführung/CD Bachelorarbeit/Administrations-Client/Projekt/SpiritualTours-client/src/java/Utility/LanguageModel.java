package Utility;

import Definitions.Language;
import Exceptions.ApplicationException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Vector;
import javax.swing.ComboBoxModel;
import javax.swing.event.ListDataListener;

/**
 * Model für Zielpunkt-Typen.
 */
public class LanguageModel implements ComboBoxModel {

    /** Liste der Zielpunkttypen. */
    private ArrayList<Language> languages;
    /** aktuell selektierter Zielpunkttyp. */
    private Language selectedLanguage;
    /** Zielpunkt-Namen */
    private ArrayList<String> languagesNames;

    /**
     * Model für Zielpunkttypen erzeugen.
     * @param Strings Zielpunkttypen
     */
    public LanguageModel(Language[] languages) {
        this.languages = new ArrayList<Language>();
        this.languages.addAll(Arrays.asList(languages));
        this.languagesNames = new ArrayList<String>();
        this.languagesNames.addAll(Arrays.asList(Language.getLanguages(
                languages)));
    }

    /**
     * Model für Zielpunkttypen erzeugen.
     * @param Strings Zielpunkttypen
     */
    public LanguageModel(ArrayList<Language> languages) {
        this.languages = languages;
        this.languagesNames = new ArrayList<String>();
        this.languagesNames.addAll(Arrays.asList(Language.getLanguages(
                languages.toArray(new Language[0]))));
    }

    /**
     * Model für Zielpunkttypen erzeugen.
     * @param Strings Zielpunkttypen
     */
    public LanguageModel(Vector<Language> languages) {
        this.languages = new ArrayList<Language>(languages);
        this.languagesNames = new ArrayList<String>();
        this.languagesNames.addAll(Arrays.asList(
                Language.getLanguages(languages.toArray(new Language[0]))));
    }

    /**
     * Setzen des aktuellen Zielpunkts.
     * @param anItem aktueller Zielpunkt
     */
    @Override
    public void setSelectedItem(Object anItem) {
        try {
            this.selectedLanguage = Language.getLanguageByName((String) anItem);
        } catch (ApplicationException ex) {
            /* kann in diesem Fall nicht so zu einem Fehler führen, daher keine
             * Aktion */
        }
    }

    /**
     * Gibt den aktuellen Zielpunkt.
     * @param anItem aktueller Zielpunkt
     */
    @Override
    public String getSelectedItem() {
        return selectedLanguage != null
                ? Language.getLanguage(selectedLanguage)
                : "";
    }

    /**
     * Gibt den aktuellen Zielpunkt.
     * @param anItem aktueller Zielpunkt
     */
    public Language getSelectedLanguage() {
        return selectedLanguage;
    }

    /**
     * Gibt die Anzahl der Zielpunkttypen.
     * @return Anzahl der Zielpunkttypen
     */
    @Override
    public int getSize() {
        return this.languages.size();
    }

    /**
     * Gibt den Zielpunkttypen an bestimmter Stelle.
     * @param index Stelle
     * @return Zielpunkttyp
     */
    @Override
    public String getElementAt(int index) {
        return this.languagesNames.get(index);
    }

    /**
     * Nicht verwendet.
     * @param l 
     */
    @Override
    public void addListDataListener(ListDataListener l) {
        /* nicht verwendet */
    }

    /**
     * Nicht verwendet.
     * @param l 
     */
    @Override
    public void removeListDataListener(ListDataListener l) {
        /* nicht verwendet */
    }
}
