package Utility;

import java.awt.Toolkit;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

/**
 * Das NumberDocument lässt in einem Textfeld nur Zahlen und selbst bestimmte 
 * Zeichen in gewünschter Zeichenlänge zu. 
 */
public class NumberDocument extends PlainDocument {

    /** Erlaubte Zeichen des Textfelds. */
    private String allowedCharacters;
    /** Anzahl der erlaubten Zeichen. */
    private int number;

    /** 
     * Erzeugen eines neuen NumberDocument.
     * @param erlaubteZeichen die erlaubten Zeichen
     * @param anzahl Anzahl der Zeichen, (bei 0 keine Begrenzung)
     */
    public NumberDocument(String erlaubteZeichen, int anzahl) {
        super();
        this.allowedCharacters = erlaubteZeichen;
        this.number = anzahl;
    }

    /**
     * Prüfen, wie viele Zeichen und ob Zeichen in Ordnung.
     * @param offs Offset
     * @param str Zeichen
     * @param a AttributeSet
     * @throws BadLocationException 
     */
    @Override
    public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {

        /* Zeichenlaenge in Ordnung ? */
        if (number != 0) {
            if (getLength() + str.length() > number) {
                Toolkit.getDefaultToolkit().beep();
                return;
            }
        }

        /* Zeichen in Ordnung ? */
        for (int i = 0; i < str.length(); i++) {
            if (allowedCharacters.indexOf(str.charAt(i)) == -1) {
                Toolkit.getDefaultToolkit().beep();
                return;
            }
        }
        super.insertString(offs, str, a);
    }
}