package DAO;

import Administration.AdministrationRemote;
import DTO.Database.DBQuestion;
import DTO.Database.DBTargetPoint;
import DTO.Database.DBInformation;
import Definitions.Language;
import Exceptions.ApplicationException;
import java.util.Collection;
import javax.transaction.UserTransaction;

/**
 * Schnittstelle zur Server-Applikation.
 */
public class DataAccessObject {
    
    /** Zugriff zum Server. */
    private AdministrationRemote admin;
    /** UserTransaction. */
    private UserTransaction tx;
    
    /**
     * Erzeugt eine DAO, zum Zugriff auf den EJB-Container im Administrations-
     * Bereich.
     * @param admin Zugriff zum EJB-Container
     */
    public DataAccessObject(AdministrationRemote admin) {
        this.admin = admin;
    }
    
    /* ====================================================================== */
    /*                       Getter-Methoden zum Server                       */
    /* ====================================================================== */
    /**
     * Alle Zielpunkte.
     * @return alle Zielpunkte
     */
    public Collection<DBTargetPoint> getAllTargetPoints() {
        return this.admin.getAllTargetPoints();
    }
    
    /**
     * Gibt den Zielpunkt passend zur ID.
     * @param targetPointID ID des Zielpunkts
     * @return Zielpunkt
     * @throws ApplicationException Problem beim Zugriff auf die Datenbank
     */
    public DBTargetPoint getTargetPoint(long targetPointId) 
            throws ApplicationException {
        return this.admin.getTargetPoint(targetPointId);
    }
    
    /* ====================================================================== */
    /*                       Setter-Methoden zum Server                       */
    /* ====================================================================== */
    /**
     * Zielpunkt hinzufügen.
     * @param targetPoint Zielpunkt
     */
    public void addTargetPoint(DBTargetPoint targetPoint) {
        this.admin.addTargetPoint(targetPoint);
    }
    
    
    /**
     * Zielpunkte hinzufügen und bearbeiten.
     * @param targetPoint Zielpunkt
     */
    public String[] setTargetPoints(Collection<DBTargetPoint> targetPoints) {
        return this.admin.setTargetPoints(targetPoints);
    }
    
    /**
     * Fragen hinzufügen.
     * @param targetPointID Id des Zielpunktes
     * @param questions Fragen
     */
    public void addQuestions(Collection<DBQuestion> questions) 
            throws ApplicationException {
        this.admin.addQuestions(questions);
    }
    
    /* ====================================================================== */
    /*                       Update-Methoden zum Server                       */
    /* ====================================================================== */
    /**
     * Aktualisiert einen Zielpunkt. Dabei werden Fragen, die neue hinzugekommen
     * sind hinzugefügt, gelöschte Fragen gelöscht und Fragen und DBPosition
     * ebenfalls aktualisiert.
     * @param updateTargetPoint Zielpunkt
     * @param removedQuestions Frage die gelöscht wurden
     * @param removedInformations Informationen die gelöscht wurden
     * @throws ApplicationException bei Fehler bei der Synchronisation mit der 
     * Datenbank.
     */
    public void updateTargetPoint(DBTargetPoint updateTargetPoint,
            Collection<DBQuestion> removedQuestions, 
            Collection<DBInformation> removedInformations) 
            throws ApplicationException {
        
        this.admin.updateTargetPoint(updateTargetPoint, removedQuestions, 
                removedInformations);
    }
    
    /* ====================================================================== */
    /*                       Remove-Methoden zum Server                       */
    /* ====================================================================== */
    /**
     * Zielpunkt löschen.
     * @param targetPointID ID des Zielpunktes
     * @throws ApplicationException 
     */
    public void removeTargetPoint(long targetPointID) 
            throws ApplicationException {
        this.admin.removeTargetPoint(targetPointID);
    }
    
    /* ====================================================================== */
    /*                       sonstige Methoden                                */
    /* ====================================================================== */    
    /**
     * Wird aufgerufen sobald alle Teilaufgaben eines Bestellprozesses erledigt 
     * sind. Führt commit aus, sofern die utx bis hierhin nicht abgebrochen ist.
     * @throws ApplicationException
     */
    public void commitOrder() throws ApplicationException {
        try {
            if(tx.getStatus() != javax.transaction.Status.STATUS_MARKED_ROLLBACK) {
                tx.commit();
            } else {
                tx.rollback();
            }
        } catch (Exception e) {
            throw new ApplicationException("Keine laufende Transaktion");
        }
    }

    /**
     * Bricht aktive Transaktion ab, sofern vorhanden.
     * @throws ApplicationException
     */
    public void cancelOrder() throws ApplicationException {
        try {
            tx.rollback();
        } catch (Exception ex) {
            throw new ApplicationException("Keine laufende Transaktion!");
        }
    }
}
