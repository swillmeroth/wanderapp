package Client;

import Administration.AdministrationRemote;
import DAO.DataAccessObject;
import GUI.MainFrame;
import javax.ejb.EJB;

/**
 * Startet den Client.
 */
public class Main {

    /**
     * DataAccessObject.
     */
    @EJB
    private static AdministrationRemote admin;
    
    /**
     * Startet den MainFrame des Clients.
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        /* Datenbank mit Daten befüllen */
//        admin.setTargetPoints(SampleData.getSampleDataHaltern());
        
        /* Programm (Fenster) starten */
        MainFrame mf = new MainFrame(new DataAccessObject(admin));
        mf.setVisible(true);
    }
}
