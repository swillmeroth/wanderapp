package Client;

import DTO.Database.DBPosition;
import DTO.Database.DBQuestion;
import DTO.Database.DBTargetPoint;
import DTO.Database.DBInformation;
import Definitions.Language;
import Definitions.LevelOfDifficulty;
import Definitions.TargetPointType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Stellt Testdaten zur Befüllung der Datenbank bereit.Die Informationen zu den 
 * Zielpunkten wurden hauptsächlich aus Stadt-Plan, Internetseit und 
 * Stadtrundführer der Statd Haltern entnommen.
 */
public final class SampleData {
    
    /**
     * Gibt Beispiel-Daten von Haltern. Die Informationen zu den Zielpunkten 
     * wurden hauptsächlich aus Stadt-Plan, Internetseit und Stadtrundführer 
     * der Statd Haltern entnommen.
     * @return Beispiel-Daten
     */
    public static Collection<DBTargetPoint> getSampleDataHaltern() {
        
        /* Zielpunkte */
        Collection<DBTargetPoint> tps = new ArrayList<DBTargetPoint>();
        
        /* Fragen */
        List<Collection<DBQuestion>> questions = getQuestions();
        
        /* Kirchen ---------------------------------------------------------- */
        
        /* Erlöserkirche */
        Collection<DBInformation> iTpChurch01 = new ArrayList<DBInformation>();
        iTpChurch01.add(new DBInformation("Die Grundsteinlegung dieser neugotischen "
                + "mit Schiefer gedeckten evangelischen Kirche erfolgte 1911. "
                + "Die Farbfenster datieren aus dem Jahr 1912. Die im Turm "
                + "angebrachte Uhr aus dem frühen 20. Jahrhundert ist noch "
                + "voll funktionsfähig und kann besichtigt werden. Im Inneren "
                + "befindet sich ein Jugendstilsaal mit an die Seite gerücktem "
                + "Turm und einhüftiger Emporenanlage.", Language.GERMAN));
        DBPosition pChurch01 = new DBPosition(51.74601111, 7.179872222, 47,
                45721, "Haltern", "Hennewiger Weg 2");
        tps.add(setTargetPoint(new DBTargetPoint("Erlöserkirche", 
                TargetPointType.CHURCH, 30), pChurch01, iTpChurch01, questions.get(0)));
        
        /* Heilig-Kreuz Kirche */
        Collection<DBInformation> iTpChurch02 = new ArrayList<DBInformation>();
        iTpChurch02.add(new DBInformation("Die erste Kirche in Hamm, Harne - das "
                + "bedeutet \"Winkel am Fluss\" - dürfte eine Kapelle des "
                + "adeligen Hauses Hame oder Hamma gewesen sein. Das Haus "
                + "Hame entstand auf einem Reichshof (Königshof) Karls des "
                + "Großen am Fuße des Hammer Berges. Über der Lippenniederung "
                + "gelegen, ist die Kirche heute Mittelpunkt einer in diesem "
                + "Bereich noch intakten Streusiedlung. Die Kirche wurde im "
                + "12. Jahrhundert im romanischen Stil errichtet. Dieses war "
                + "zur Zeit der Kreuzzüge, daher auch der Name Heilig Kreuz. "
                + "Dieses Baudenkmal ist gut über den Kanaluferweg per Fahrrad "
                + "oder zu Fuß zu erreichen.", Language.GERMAN));
        DBPosition pChurch02 = new DBPosition(51.72173611, 7.168422222, 42,
                45721, "Haltern", "Kirchweg 6");
        tps.add(setTargetPoint(new DBTargetPoint("Heilig-Kreuz Kirche", 
                TargetPointType.CHURCH, 15), pChurch02, iTpChurch02, questions.get(1)));
        
        /* Marienkirche */					
	Collection<DBInformation> iTpChurch03 = new ArrayList<DBInformation>();
        iTpChurch03.add(new DBInformation("", Language.GERMAN));
        DBPosition pChurch03 = new DBPosition(51.74744167, 7.185936111, 47,
                45721, "Haltern", "Gildenstraße 22");
        tps.add(setTargetPoint(new DBTargetPoint("Marienkirche", 
                TargetPointType.CHURCH, 25), pChurch03, iTpChurch03, questions.get(2)));
        
        /* Neuapostolische Kirche */						
	Collection<DBInformation> iTpChurch04 = new ArrayList<DBInformation>();
        iTpChurch04.add(new DBInformation("", Language.GERMAN));
        DBPosition pChurch04 = new DBPosition(51.74566389, 7.17795, 44,
                45721, "Haltern", "Holtwicker Straße 58 - 60");
        tps.add(setTargetPoint(new DBTargetPoint("Neuapostolische Kirche", 
                TargetPointType.CHURCH, 25), pChurch04, iTpChurch04, questions.get(3)));
        
        /* Pfarrkirche Sankt Maria Magdalena */						
	Collection<DBInformation> iTpChurch05 = new ArrayList<DBInformation>();
        iTpChurch05.add(new DBInformation("Die Flaesheimer Pfarrkirche Sankt Maria "
                + "Magdalena gehört zu den bemerkenswertesten und ältesten "
                + "Kirchenbauten im Halterner Raum. Der romanische Turm stammt "
                + "mit Sicherheit noch vom Gründungsbau des 1166 von Graf Otto "
                + "dem Ersten von Ravensberg gegründeten und dem Kloster "
                + "Knechtsteden unterstellten prämonstratenser-Nonnenklosters. "
                + "Das Kloster wird 1550 freiweltliches Stift, die "
                + "Stiftsgebäude sind 1790 abgebrannt. Trotz umfangreicher "
                + "Literatur ist die Baugeschichte der heute einschiffigen "
                + "Kirche noch nicht in allen Teilen eindeutig geklärt.", 
                Language.GERMAN));
        DBPosition pChurch05 = new DBPosition(51.71991389, 7.233027778, 44,
                45721, "Haltern / Flaesheim", "Stiftsplatz 6");
        tps.add(setTargetPoint(new DBTargetPoint("Pfarrkirche Sankt Maria Magdalena", 
                TargetPointType.CHURCH, 20), pChurch05, iTpChurch05, questions.get(4)));
        
        /* Pfarrkirche St. Andreas */						
	Collection<DBInformation> iTpChurch06 = new ArrayList<DBInformation>();
        iTpChurch06.add(new DBInformation("Im Jahr 1895 beauftragte der "
                + "Kirchvorstand den Architekten Wilhelm Ricklake aus Münster "
                + "mit der Planung dieser Kirche. Es entstand ein "
                + "eindrucksvolles Bauwerk in einer neugotischen "
                + "Backsteinarchitektur mit 2 Jochen, einem Querschiff und "
                + "einem 65 m hohen Westturm. Am 21. September 1897 wurde die "
                + "Kirche feierlich eingeweiht. Bei Restaurierungsarbeiten "
                + "entdeckte die Restaurateurin an den spätgotischen "
                + "Altarbildern (um 1500), dass sich unter der zweiten "
                + "Farbschicht in der \"Geißelung\" ein zweites Gesicht "
                + "befindet.", Language.GERMAN));
        DBPosition pChurch06 = new DBPosition(51.73619444, 7.289622222, 47,
                45721, "Haltern", "Terwellenweg 11");
        tps.add(setTargetPoint(new DBTargetPoint("Pfarrkirche St. Andreas", 
                TargetPointType.CHURCH, 20), pChurch06, iTpChurch06, questions.get(5)));
        
        /* Sankt Sixtus Kirche */						
	Collection<DBInformation> iTpChurch07 = new ArrayList<DBInformation>();
        iTpChurch07.add(new DBInformation("Die Pfarrkirche St. Sixtus ist nach Osten "
                + "ausgerichtet. Zum Inventar gehört das berühmte "
                + "Gabelkruzifix aus Eichenholz, das um 1330/40 entstanden "
                + "sein dürfte (Restauration 1961). Es wird alljährlich bei "
                + "der Kreuzerhöhungsprozession durch die Straßen der Stadt "
                + "getragen. Diese Tradition geht auf das Jahr 1736 zurück. "
                + "Daneben ist das 1710 entstandene Epitaph eines der "
                + "bedeutendsten Kunstschätze dieser Kirche.", Language.GERMAN));
        DBPosition pChurch07 = new DBPosition(51.74277778, 7.187205556, 47,
                45721, "Haltern", "Markt 10");
        tps.add(setTargetPoint(new DBTargetPoint("Sankt Sixtus Kirche", 
                TargetPointType.CHURCH, 20), pChurch07, iTpChurch07, questions.get(6)));
        
        /* Wallfahrtskapelle Pilgerkirche Sankt Anna */						
	Collection<DBInformation> iTpChurch08 = new ArrayList<DBInformation>();
        iTpChurch08.add(new DBInformation("Erstmals wurde eine Kapelle auf dem "
                + "Annaberg im Jahre 1378 urkundlich erwähnt. Ab 1556 fanden "
                + "die ersten Wallfahrten statt. Die heutige alte Kapelle "
                + "datiert aus dem Jahre 1967. In der Nähe befindet sich eine "
                + "Quelle, der zu der damaligen Zeit eine heilkräftige Wirkung "
                + "nachgesagt wurde. Einige weitere kleine Bauwerke, "
                + "Gedenkstätten, Kreuzwegstationen und eine Steele können auf "
                + "dem weitläufigen Areal des Annabergs besucht werden. Viele "
                + "Vertriebene aus Schlesien haben ihre traditionelle "
                + "Annaberg-Wallfahrt hierher übertragen.", Language.GERMAN));
        DBPosition pChurch08 = new DBPosition(51.728525, 7.155305556, 80,
                45721, "Haltern", "Annaberg");
        tps.add(setTargetPoint(new DBTargetPoint("Wallfahrtskapelle Pilgerkirche Sankt Anna", 
                TargetPointType.CHURCH, 60), pChurch08, iTpChurch08, questions.get(7)));
        
        /* Pfarrkirche St. Antonius */						
	Collection<DBInformation> iTpChurch09 = new ArrayList<DBInformation>();
        iTpChurch09.add(new DBInformation("Die Pfarrkirche Sankt Antonius gibt sich "
                + "in barocken Formen, entstand aber erst in den Jahren 1921 "
                + "bis 1924 nach Plänen des münsterischen Dombaumeisters "
                + "Sunder-Plaßmann.", Language.GERMAN));
        DBPosition pChurch09 = new DBPosition(51.77861667, 7.162977778, 67,
                45721, "Haltern Lavesum", "Rekener Straße");
        tps.add(setTargetPoint(new DBTargetPoint("Pfarrkirche St. Antonius", 
                TargetPointType.CHURCH, 30), pChurch09, iTpChurch09, questions.get(8)));
        									
	/* Pfarrkirche St. Joseph */						
	Collection<DBInformation> iTpChurch10 = new ArrayList<DBInformation>();
        iTpChurch10.add(new DBInformation("Die Pfarrkirch Sankt Joseph feierte ihre "
                + "Gründung im Jahr 1909. Wegen des rapiden "
                + "Bevölkerungswachstums nach dem 2. Weltkrieg war die Kirche "
                + "bald zu klein und erfuhr 1959 eine große Erweiterung.",
                Language.GERMAN));
        DBPosition pChurch10 = new DBPosition(51.77394167, 7.223061111, 50,
                45721, "Haltern Sythen", "Hellweg 11");
        tps.add(setTargetPoint(new DBTargetPoint("Pfarrkirche St. Joseph", 
                TargetPointType.CHURCH, 30), pChurch10, iTpChurch10, questions.get(9)));
			
	/* St. Laurentius */						
	Collection<DBInformation> iTpChurch11 = new ArrayList<DBInformation>();
        iTpChurch11.add(new DBInformation("Die Grundsteinlegung erfolgte am 17. Juli "
                + "1954. Nach alter Tradition weist die Fassade nach Westen, "
                + "während der Altar nach Osten ausgerichtet ist. Die Kirche "
                + "trägt den Namen des heiligen Laurentius, des Mannes, der "
                + "als Diakon seinem väterlichen Freund, dem Papst Sixtus, "
                + "treu ergeben war.", Language.GERMAN));
        DBPosition pChurch11 = new DBPosition(51.74366111, 7.172641667, 59,
                45721, "Haltern Sythen", "Augustusstr. 24");
        tps.add(setTargetPoint(new DBTargetPoint("St. Laurentius", 
                TargetPointType.CHURCH, 25), pChurch11, iTpChurch11, questions.get(10)));
					
	/* St. Lambertus */						
	Collection<DBInformation> iTpChurch12 = new ArrayList<DBInformation>();
        iTpChurch12.add(new DBInformation("Die Kirche von Lippramsdorf mittem im "
                + "Herz des Dorfes.", Language.GERMAN));
        DBPosition pChurch12 = new DBPosition(51.71392222, 7.096597222, 43,
                45721, "Haltern Lippramsdorf", "Pastoratsweg 20");
        tps.add(setTargetPoint(new DBTargetPoint("St. Lambertus", 
                TargetPointType.CHURCH, 15), pChurch12, iTpChurch12, questions.get(11)));
        
        /* Kapellen --------------------------------------------------------- */
        
        /*  Kriegergedächtnis-Kapelle */						
	Collection<DBInformation> iTpChapel01 = new ArrayList<DBInformation>();
        iTpChapel01.add(new DBInformation("Der einschiffige Bau stammt ursprünglich "
                + "aus dem Jahr 1467. Er wurde 1652 nach Westen erweitert. An "
                + "der Nordseite schließt sich eine kleine Sakristei von 1805 "
                + "an. Bemerkenswert sind die restaurierten Wandmalereien, "
                + "welche lange Zeit durch Farbschichten überdeckt waren, die "
                + "aus dem späten 15. Jahrhundert stammen. Die Bilder wurden "
                + "seinerzeit auf den frischen Verputz (daher: Fresco) gemalt. "
                + "Erst in den dreißiger Jahren des 20. Jahrhunderts wurden "
                + "sie wiederentdeckt. Heute dient die Kapelle neben der "
                + "ursprünglichen Bestimmung als Gotteshaus auch als Denkmal "
                + "für die Gefallenen der letzten beiden Weltkriege. Im Innern "
                + "der Kapelle sind Gedenktafeln mit den Namen aller in den "
                + "beiden Kriegen gefallenen und vermissten Soldaten aus "
                + "Lavesum angebracht.", Language.GERMAN));
        DBPosition pChapel01 = new DBPosition(51.78089722, 7.162961111, 67,
                45721, "Haltern Lavesum", "Antoniusstraße 18");
        tps.add(setTargetPoint(new DBTargetPoint("Kriegergedächtnis-Kapelle", 
                TargetPointType.CHAPEL, 20), pChapel01, iTpChapel01, questions.get(12)));
        
        /* Antoniuskapelle Lavesum */						
	Collection<DBInformation> iTpChapel02 = new ArrayList<DBInformation>();
        iTpChapel02.add(new DBInformation("Die kleine Bruchsteinkapelle auf dem "
                + "Tannenberg bei der Bauernschaft gleichen Namens wurde die "
                + "ihrer heutigen Form 1962 an St. Peter und Paul eingeweiht. "
                + "Es gibt aber Belege dafür, dass hier schon vor "
                + "Jahrhunderten eine Kapelle stand. So schrieb der Halterner "
                + "Pfarrer Hermann Boeker im Jahre 1662, dass die ehedem bei "
                + "dem Weseler Weg gestandene Kapelle bereits vor Jahrzenten "
                + "zerstört worden und nur noch in Ruinen vorhanden sei. Der "
                + "Aufforderungen des Bischofs Christian Bernhard von Galen "
                + "aus dem Jahre 1669, die Kapelle wieder herzustellen, sollen "
                + "die Bauern damals nicht nachgekommen sein.", Language.GERMAN));
        DBPosition pChapel02 = new DBPosition(51.74346667, 7.114180556, 83,
                45721, "Haltern", "Tannenberg");
        tps.add(setTargetPoint(new DBTargetPoint("Antoniuskapelle Lavesum", 
                TargetPointType.CHAPEL, 12), pChapel02, iTpChapel02, questions.get(13)));
        
        /* St. Anna-Kapelle */						
	Collection<DBInformation> iTpChapel03 = new ArrayList<DBInformation>();
        iTpChapel03.add(new DBInformation("Erstmals wurde eine Kapelle auf dem "
                + "Annaberg im Jahre 1378 urkundlich erwähnt. Ab 1556 fanden "
                + "die ersten Wallfahrten statt. Die heutige alte Kapelle "
                + "datiert aus dem Jahre 1967. In der Nähe befindet sich eine "
                + "Quelle, der zu der damaligen Zeit eine heilkräftige Wirkung "
                + "nachgesagt wurde. Einige weitere kleine Bauwerke, "
                + "Gedenkstätten, Kreuzwegstationen und eine Steele können auf "
                + "dem weitläufigen Areal des Annabergs besucht werden. Viele "
                + "Vertriebene aus Schlesien haben ihre traditionelle Annaberg-"
                + "Wallfahrt hierher übertragen.", Language.GERMAN));
        DBPosition pChapel03 = new DBPosition(51.725875, 7.155316667, 60,
                45721, "Haltern", "Annaberg");
        tps.add(setTargetPoint(new DBTargetPoint("St. Anna-Kapelle", 
                TargetPointType.CHAPEL, 15), pChapel03, iTpChapel03, questions.get(14)));
        
        /* St. Katharinen-Kapelle */						
	Collection<DBInformation> iTpChapel04 = new ArrayList<DBInformation>();
        iTpChapel04.add(new DBInformation("Diese Kapelle gilt als ältester Steinbau "
                + "im Vest. Dem kleinen, flachgedeckten Saalbau mit gerade "
                + "geschlossenem Chor - vermutlich aus dem 11. Jahrhundert - "
                + "folgte als spätere Erweiterung das Kirchenschiff mit einem "
                + "Barockaltar, der mit 1744 - 1746 bezeichnet ist.", 
                Language.GERMAN));
        DBPosition pChapel04 = new DBPosition(51.72901389, 7.189041667, 41,
                45721, "Haltern (Hamm-Bossendorf)", "Kapellenweg");
        tps.add(setTargetPoint(new DBTargetPoint("St. Katharinen-Kapelle", 
                TargetPointType.CHAPEL, 15), pChapel04, iTpChapel04, questions.get(15)));
        
        /* Korten-Keppelken */						
	Collection<DBInformation> iTpChapel05 = new ArrayList<DBInformation>();
        iTpChapel05.add(new DBInformation("Die kleine Kapelle auf dem "
                + "Prozessionsweg.", Language.GERMAN));
        DBPosition pChapel05 = new DBPosition(51.75912778, 7.1898, 66,
                45721, "Haltern", "Prozessionsweg");
        tps.add(setTargetPoint(new DBTargetPoint("Korten-Keppelken", 
                TargetPointType.CHAPEL, 15), pChapel05, iTpChapel05, questions.get(16)));
        
        /* jüdische Friedhöfe ----------------------------------------------- */
        
        /* jüdischer Friedhof Haltern */						
	Collection<DBInformation> iTpJewishCementery01 = 
                new ArrayList<DBInformation>();
        iTpJewishCementery01.add(new DBInformation("Dieser Friedhof befindet sich seit dem "
                + "Jahre 1767 an dieser Stelle unmittelbar außerhalb des alten "
                + "Stadtgrabens. Am 26. Januar 1997 wurde für die während der "
                + "NS-Gewaltherrschaft verfolgten jüdischen Mitbürger ein "
                + "Gedenkstein enthüllt, der in der Form einer "
                + "alttestamentarischen Gesetzestafel von einer Halterner "
                + "Steinbildhauerei gestaltet wurde. Auf dem Grabstein sind "
                + "neben einem hebräischen Segensspruch die Namen der von 1933 "
                + "bis 1945 verfolgten Mitbürger und ein Klagelied zu lesen.", 
                Language.GERMAN));
        DBPosition pJewishCementery01 = new DBPosition(51.74188056, 7.189077778, 
                43, 45721, "Haltern", "Richthof");
        tps.add(setTargetPoint(new DBTargetPoint("jüdischer Friedhof Haltern", 
                TargetPointType.JEWISH_CEMETERY, 15), pJewishCementery01, 
                iTpJewishCementery01, questions.get(17)));
        
        /* Friedhöfe -------------------------------------------------------- */
        
        /* Friedhof Sundern */						
	Collection<DBInformation> iTpCementery01 = 
                new ArrayList<DBInformation>();
        iTpCementery01.add(new DBInformation("Der erste Spatenstich des an der "
                + "Sundernstraße gelegenen Waldfriedhofes Sundern erfolgte am "
                + "19.01.1979. Die Planung beinhaltete für den 1. Abschnitt "
                + "die Anlegung von 700 Doppelgräbern, 400 Einzelgräbern, 50 "
                + "Kindergräbern, 50 Urnengräbern und den Bau einer "
                + "Trauerhalle für ca. 150 Trauergäste. Die Trauerhalle, deren "
                + "Baukosten ca. 2,4 Millionen Mark betrugen, wurde 1981 "
                + "fertiggestellt. Die Gesamtfläche des am Waldrand gelegenen "
                + "Friedhofes umfasst jetzt nach der Erweiterung im Jahre 1998 "
                + "ca. 52.000 m².", 
                Language.GERMAN));
        DBPosition pCementery01 = new DBPosition(51.75453056, 7.164669444, 70,
                45721, "Haltern", "Sundernstraße");
        tps.add(setTargetPoint(new DBTargetPoint("Friedhof Sundern", 
                TargetPointType.CEMETERY, 15), pCementery01, iTpCementery01, 
                questions.get(18)));
        
        /* Friedhof Hullern */						
	Collection<DBInformation> iTpCementery02 = 
                new ArrayList<DBInformation>();
        iTpCementery02.add(new DBInformation("Die Einsegnung des an der "
                + "Antruper/Westruper Straße gelegenen Friedhofes Hullern "
                + "erfolgte am 26.01.1979. Im Jahre 1986 begann die Planung "
                + "einer Trauerhalle, welche im Jahre 1988 fertiggestellt "
                + "wurde. Die Baukosten für dieses Objekt beliefen sich auf "
                + "ca. 610.000 DM. Die Gesamtfläche des am Waldrand gelegenen "
                + "Friedhofes beträgt ca. 10.000 m².", Language.GERMAN));
        DBPosition pCementery02 = new DBPosition(51.73658056, 7.291730556, 47,
                45721, "Haltern", "Terwellenweg");
        tps.add(setTargetPoint(new DBTargetPoint("Friedhof Hullern", 
                TargetPointType.CEMETERY, 15), pCementery02, iTpCementery02, 
                questions.get(19)));
        
        /* Friedhof Sythen */						
	Collection<DBInformation> iTpCementery03 = 
                new ArrayList<DBInformation>();
        iTpCementery03.add(new DBInformation("Die Einweihung des am Brinkweg gelegenen "
                + "Friedhofes Sythen erfolgte am 28.06.1960. Im Jahre 1965 "
                + "wurde dann die Trauerhalle, deren Baukosten sich auf ca. "
                + "138.000 DM beliefen, fertiggestellt. Die Gesamtfläche des "
                + "Friedhofes umfasst ca. 29.000 m². ", Language.GERMAN));
        DBPosition pCementery03 = new DBPosition(51.77106111, 7.220319444, 47,
                45721, "Haltern Sythen", "Lehmbrakenerstraße");
        tps.add(setTargetPoint(new DBTargetPoint("Friedhof Sythen", 
                TargetPointType.CEMETERY, 15), pCementery03, iTpCementery03, 
                questions.get(20)));
        
        /* Friedhof Flaesheim (St. Maria-Magdalena) */						
	Collection<DBInformation> iTpCementery04 = 
                new ArrayList<DBInformation>();
        iTpCementery04.add(new DBInformation("Die Einweihung des an der "
                + "Otto-von-Ravensberg-Straße gelegenen Friedhofes Flaesheim "
                + "erfolgte am 08.10.1967. Im Jahre 1971 wurde dann die "
                + "Trauerhalle, deren Baukosten sich auf ca. 120.000 DM "
                + "beliefen, fertiggestellt. Die Gesamtfläche des Friedhofes "
                + "umfasst ca. 24.000 m².", Language.GERMAN));
        DBPosition pCementery04 = new DBPosition(51.71986944, 7.233508333, 43,
                45721, "Haltern Flaesheim", "Otto-von-Ravensberg-Straße");
        tps.add(setTargetPoint(new DBTargetPoint("Friedhof Flaesheim (St. Maria-Magdalena)", 
                TargetPointType.CEMETERY, 15), pCementery04, iTpCementery04, 
                questions.get(0)));
        
        /* katholischer Friedhof St. Sixtus */						
	Collection<DBInformation> iTpCementery05 = 
                new ArrayList<DBInformation>();
        iTpCementery05.add(new DBInformation("Der Friedhof der St. Sixtus "
                + "Gemeinde liegt zwischen Lippspieker und Hullerner Straße. "
                + "Der im Jahr 1810 eröffnete Friedhof St. Sixtus ist mit "
                + "23.000 m² nicht nur der größte kath. Friedhof in Haltern, "
                + "sondern kann heute durch Lage und Funktion auch neben dem "
                + "Kommunalfriedhof als Hauptfriedhof bezeichnet werden. Das "
                + "zentrale Friedhofskreuz und der Kreuzweg mit seinen 14 "
                + "Stationen geben dem Friedhof einen besonderen Charakter, "
                + "seine Lage am Rande der Altstadt zeugt von der alten "
                + "Verbindung der Lebenden mit den Verstorbenen.", 
                Language.GERMAN));
        DBPosition pCementery05 = new DBPosition(51.74242222, 7.192066667, 44,
                45721, "Haltern", "Lippspieker");
        tps.add(setTargetPoint(new DBTargetPoint("katholischer Friedhof St. Sixtus", 
                TargetPointType.CEMETERY, 15), pCementery05, iTpCementery05, 
                questions.get(1)));
        
        /* Friedhof Bossendorf */						
	Collection<DBInformation> iTpCementery06 = 
                new ArrayList<DBInformation>();
        iTpCementery06.add(new DBInformation("Auf diesem Friedhof steht die "
                + "sehr bekannte St. Katharinen-Kapelle.", 
                Language.GERMAN));
        DBPosition pCementery06 = new DBPosition(51.72901667, 7.188838889, 41,
                45721, "Haltern Bossendorf", "Kapellenweg");
        tps.add(setTargetPoint(new DBTargetPoint("Friedhof Bossendorf", 
                TargetPointType.CEMETERY, 15), pCementery06, iTpCementery06, 
                questions.get(2)));
        
        /* Wegekreuze ------------------------------------------------------- */
        
        /* Gabelkreuz */						
	Collection<DBInformation> iTpWayCross01 = 
                new ArrayList<DBInformation>();
        iTpWayCross01.add(new DBInformation("Das Wegekreuz steht an einer der "
                + "Hauptverkehrsadern der Stadt Haltern.", 
                Language.GERMAN));
        DBPosition pWayCross01 = new DBPosition(51.74551389, 7.190002778, 45,
                45721, "Haltern", "Schüttenwall / Münsterstraße / Johannesstraße");
        tps.add(setTargetPoint(new DBTargetPoint("Gabelkreuz", 
                TargetPointType.WAYSIDE_CROSS, 15), pWayCross01, iTpWayCross01, 
                questions.get(3)));
        
        /* Wegekreuz Lohstraße / Gildenstraße */						
	Collection<DBInformation> iTpWayCross02 = 
                new ArrayList<DBInformation>();
        iTpWayCross02.add(new DBInformation("In der Nähe dieses Wegekreuzes "
                + "liegt die Marienkirche.", 
                Language.GERMAN));
        DBPosition pWayCross02 = new DBPosition(51.74701111, 7.184141667, 49,
                45721, "Haltern", "Lohstraße / Gildenstraße");
        tps.add(setTargetPoint(new DBTargetPoint("Wegekreuz Lohstraße / Gildenstraße", 
                TargetPointType.WAYSIDE_CROSS, 15), pWayCross02, iTpWayCross02, 
                questions.get(4)));
        
        /* Wegekreuz am Prozessionsweg */						
	Collection<DBInformation> iTpWayCross03 =
                new ArrayList<DBInformation>();
        iTpWayCross03.add(new DBInformation("Am Prozessionsweg der Stadt "
                + "Haltern am See sind mehrere Wegekreuze, Standbilder, "
                + "etc. zu finden.", Language.GERMAN));
        DBPosition pWayCross03 = new DBPosition(51.75628333, 7.195063889, 61,
                45721, "Haltern", "Prozessionsweg");
        tps.add(setTargetPoint(new DBTargetPoint("Wegekreuz am Prozessionsweg", 
                TargetPointType.WAYSIDE_CROSS, 12), pWayCross03, iTpWayCross03, 
                questions.get(5)));
        
        /* Standbilder ------------------------------------------------------ */
        
        /* Mutter Anna */						
	Collection<DBInformation> iTpStatue01 = new ArrayList<DBInformation>();
        iTpStatue01.add(new DBInformation("Das Mutter-Anna-Standbild wurde in "
                + "einer Halterner Bildhauerwerkstatt hergestellt. Alljährlich "
                + "im September, am traditionellen Fest der \"Kreuzerhöhung\", "
                + "zieht hier auch die Halterner Kreuztracht mit dem fast 700 "
                + "Jahre alten Gabelkreuz der Sixtuskirche vorbei. Im Gebäude "
                + "\"Muttergottesstiege\" hinter der Statue befinden sich u.a. "
                + "die Zweigstelle der Volkshochschule Dülmen/ Haltern am See/ "
                + "Havixbeck sowie Büros der Stadtverwaltung Haltern am See.", 
                Language.GERMAN));
        DBPosition pStatue01 = new DBPosition(51.74468056, 7.183413889, 46,
                45721, "Haltern", "Rekumer Straße");
        tps.add(setTargetPoint(new DBTargetPoint("Mutter Anna", 
                TargetPointType.STATUE, 7), pStatue01, iTpStatue01, 
                questions.get(6)));
        
        /* Standbild des heiligen Ludgerus */						
	Collection<DBInformation> iTpStatue02 = new ArrayList<DBInformation>();
        iTpStatue02.add(new DBInformation("Dieses ist eine seltene Darstellung "
                + "in dieser Gegend. Das Standbild zeigt den heiligen Ludgerus "
                + "im Bischofsformat und zu seinen Füßen eine Gans. Der "
                + "Legende nach soll der Bischof die Bauern, als sie ihn um "
                + "entsprechenden Rat baten, angewiesen haben, an dieser "
                + "Stelle nach einem Brunnen zu graben. Sie befolgten seinen "
                + "Rat, und angeblich ist dieser Brunnen noch niemals versiegt "
                + "und hat auch in den trockensten Sommern stets Wasser "
                + "gegeben.", Language.GERMAN));
        DBPosition pStatue02 = new DBPosition(51.74593333, 7.112975, 85,
                45721, "Haltern", "Tannenberger Straße");
        tps.add(setTargetPoint(new DBTargetPoint("Standbild des heiligen Ludgerus", 
                TargetPointType.STATUE, 12), pStatue02, iTpStatue02, 
                questions.get(7)));
        
        /* Standbild an der Rekener Straße */						
	Collection<DBInformation> iTpStatue03 = new ArrayList<DBInformation>();
        iTpStatue03.add(new DBInformation("In der Nähe liegt die St. Antonius "
                + "Kirche von Lavesum.", Language.GERMAN));
        DBPosition pStatue03 = new DBPosition(51.77927222, 7.163852778, 71,
                45721, "Haltern Lavesum", "Rekener Straße");
        tps.add(setTargetPoint(new DBTargetPoint("Standbild an der Rekener Straße", 
                TargetPointType.STATUE, 7), pStatue03, iTpStatue03, 
                questions.get(8)));
        
        /* Standbild Schützenstraße / Napoleonsweg */						
	Collection<DBInformation> iTpStatue04 = new ArrayList<DBInformation>();
        iTpStatue04.add(new DBInformation("In der Nähe liegt die St. Antonius "
                + "Kirche von Lavesum.", Language.GERMAN));
        DBPosition pStatue04 = new DBPosition(51.77927222, 7.163852778, 68,
                45721, "Haltern Lavesum", "Schützenstraße / Napoleonsweg");
        tps.add(setTargetPoint(new DBTargetPoint("Standbild Schützenstraße / Napoleonsweg", 
                TargetPointType.STATUE, 15), pStatue04, iTpStatue04, 
                questions.get(9)));
        
        /* Standbild an der Birkenstraße */						
	Collection<DBInformation> iTpStatue05 = new ArrayList<DBInformation>();
        iTpStatue05.add(new DBInformation("In der Nähe liegt die St. Lambertus "
                + "Kirche von Lippramsdorf.", Language.GERMAN));
        DBPosition pStatue05 = new DBPosition(51.71620556, 7.092975, 44,
                45721, "Haltern Lippramsdorf", "Birkenstraße");
        tps.add(setTargetPoint(new DBTargetPoint("Standbild an der Birkenstraße", 
                TargetPointType.STATUE, 11), pStatue05, iTpStatue05, 
                questions.get(10)));
        
        /* Standbild an der Erzbischof-Buddenbrock-Straße */						
	Collection<DBInformation> iTpStatue06 = new ArrayList<DBInformation>();
        iTpStatue06.add(new DBInformation("In der Nähe liegt die St. Antonius "
                + "Kirche von Lavesum.", Language.GERMAN));
        DBPosition pStatue06 = new DBPosition(51.71948889, 7.098358333, 48,
                45721, "Haltern Lavesum", "Erzbischof-Buddenbrock-Straße");
        tps.add(setTargetPoint(new DBTargetPoint("Standbild an der Erzbischof-Buddenbrock-Straße", 
                TargetPointType.STATUE, 8), pStatue06, iTpStatue06, 
                questions.get(11)));
        
        /* Denkmäler -------------------------------------------------------- */
        
        /* Kardinal-von-Galen-Denkmal */						
	Collection<DBInformation> iTpMonument01 = new ArrayList<DBInformation>();
        iTpMonument01.add(new DBInformation("Der Kardinal-von-Galen-Park ist "
                + "die grüne Oase am Rande der Altstadt. Seinen Namen hat der "
                + "Park vom Münsteraner Kardinal Clemens August Graf von Galen "
                + "(siehe Informationstafel vor dem Denkmal), dem hier für "
                + "sein Eintreten für Verfolgte während der "
                + "nationalsozialistischen Gewaltherrschaft ein Denkmal "
                + "errichtet wurde. Das Monument wurde von einem in Haltern "
                + "vertretenen Unternehmen im Jubiläumsjahr 1989 gestiftet. Im "
                + "gleichen Jahr wurden im Rahmen eines Symposiums die "
                + "Kunstwerke im Park an Ort und Stelle geschaffen.", 
                Language.GERMAN));
        DBPosition pMonument01 = new DBPosition(51.74196944, 7.18365, 43,
                45721, "Haltern", "Kardinal-von-Galen-Park");
        tps.add(setTargetPoint(new DBTargetPoint("Kardinal-von-Galen-Denkmal", 
                TargetPointType.MONUMENT, 12), pMonument01, iTpMonument01, 
                questions.get(12)));
        
        return tps;
    }
    
    /**
     * Liefert 21 Fragelisten
     * @return 21 Fragelisten
     */
    private static List<Collection<DBQuestion>> getQuestions() {
        
        Collection<DBQuestion> qs01 = new ArrayList<DBQuestion>();
        Collection<DBQuestion> qs02 = new ArrayList<DBQuestion>();
        Collection<DBQuestion> qs03 = new ArrayList<DBQuestion>();
        Collection<DBQuestion> qs04 = new ArrayList<DBQuestion>();
        Collection<DBQuestion> qs05 = new ArrayList<DBQuestion>();
        Collection<DBQuestion> qs06 = new ArrayList<DBQuestion>();
        Collection<DBQuestion> qs07 = new ArrayList<DBQuestion>();
        Collection<DBQuestion> qs08 = new ArrayList<DBQuestion>();
        Collection<DBQuestion> qs09 = new ArrayList<DBQuestion>();
        Collection<DBQuestion> qs10 = new ArrayList<DBQuestion>();
        Collection<DBQuestion> qs11 = new ArrayList<DBQuestion>();
        Collection<DBQuestion> qs12 = new ArrayList<DBQuestion>();
        Collection<DBQuestion> qs13 = new ArrayList<DBQuestion>();
        Collection<DBQuestion> qs14 = new ArrayList<DBQuestion>();
        Collection<DBQuestion> qs15 = new ArrayList<DBQuestion>();
        Collection<DBQuestion> qs16 = new ArrayList<DBQuestion>();
        Collection<DBQuestion> qs17 = new ArrayList<DBQuestion>();
        Collection<DBQuestion> qs18 = new ArrayList<DBQuestion>();
        Collection<DBQuestion> qs19 = new ArrayList<DBQuestion>();
        Collection<DBQuestion> qs20 = new ArrayList<DBQuestion>();
        Collection<DBQuestion> qs21 = new ArrayList<DBQuestion>();
        
        /* ------------------------------------------------------------------ */
        
        qs01.add(new DBQuestion("Wie viel ist 1 + 1 ?", 
                "2", new String[]{"1", "3", "4"}, 
                LevelOfDifficulty.EASY, Language.GERMAN));
        qs01.add(new DBQuestion("Auge um Auge... ?", 
                "Zahn um Zahn", new String[]{"Mund um Mund", "Ohr um Ohr", "Nase um Nase"}, 
                LevelOfDifficulty.EASY, Language.GERMAN));     
        qs01.add(new DBQuestion("Wie hieß John Wayne mit richtigem Namen ?", 
                "Marion Michael Morrison", new String[]{"Mark McDaniel", "John Welch", "Johan Watson"}, 
                LevelOfDifficulty.MIDDLE, Language.GERMAN));
        qs01.add(new DBQuestion("Wo liegt die Insel Capri ?", 
                "Im Golf von Neapel", new String[]{"Im Golf von Deutschland", "Im Golf von Amerika", "In der Karibik"}, 
                LevelOfDifficulty.MIDDLE, Language.GERMAN));
        qs01.add(new DBQuestion("Mit welchem Protokoll werden u.A. Netzwerkeinstellungen an Clients versendet ?", 
                "2", new String[]{"1", "3", "4"}, 
                LevelOfDifficulty.DIFFICULT, Language.GERMAN));
        qs01.add(new DBQuestion("Welche Geschwindigkeit kann der Wanderfalke erreichen ?", 
                "350 km/h", new String[]{"250 km/h", "300 km/h", "400 km/h"}, 
                LevelOfDifficulty.DIFFICULT, Language.GERMAN));
        
        /* ------------------------------------------------------------------ */
        
        qs02.add(new DBQuestion("Asterix und ...?", 
                "Obelix", new String[]{"Untelix", "Quälix", "Felix"}, 
                LevelOfDifficulty.EASY, Language.GERMAN));
        qs02.add(new DBQuestion("Welche Geschwindigkeit braucht man, um "
                + "100km in einer Stunde zu schaffen ?", "100 km/h", 
                new String[]{"125 km/h", "200 km/h", "50 km/h"}, 
                LevelOfDifficulty.EASY, Language.GERMAN));
        qs02.add(new DBQuestion("Mit Sauriern beschäftigt sich der ...?", 
                "Paläontologe", new String[]{"Politologe", "Pulmologe", "Pathologe"}, 
                LevelOfDifficulty.MIDDLE, Language.GERMAN));        
        qs02.add(new DBQuestion("In welchem Dorf trägt Pitje Puck seine Post aus ?", 
                "Kesseldorf", new String[]{"Heidendorf", "Wiesendorf", "Neuendorf"}, 
                LevelOfDifficulty.MIDDLE, Language.GERMAN));
        qs02.add(new DBQuestion("Was bedeutet das Italienische Wort \"fazzoletti\" auf Deutsch ?", 
                "Taschentuch", new String[]{"Fratze", "Rübe", "Kopf"}, 
                LevelOfDifficulty.DIFFICULT, Language.GERMAN));
        qs02.add(new DBQuestion("Welches ist US-Staat ist flächenmäßig nicht größer als Deutschland ?", 
                "Nevade", new String[]{"Alaska", "Texas", "Kalifornien"}, 
                LevelOfDifficulty.DIFFICULT, Language.GERMAN));
        
        /* ------------------------------------------------------------------ */
        
        qs03.add(new DBQuestion("Welche Stadt hat mehr Einwohner ?", "Berlin", 
                new String[]{"Hamburg"}, LevelOfDifficulty.EASY, Language.GERMAN));        
        qs03.add(new DBQuestion("Wie nennt man die Karten eins Spielers ?", 
                "Hand", new String[]{"Arm", "Bein", "Kopf", "Fuß"}, 
                LevelOfDifficulty.EASY, Language.GERMAN));            
        qs03.add(new DBQuestion("Was ist ein Fitschenband ?", 
                "ein Türband", new String[]{"ein Fensterband", "ein Schlüsselband", "ein Zugband"}, 
                LevelOfDifficulty.MIDDLE, Language.GERMAN));
        qs03.add(new DBQuestion("Was ist Gondwana ?", 
                "ein Urkontinent", new String[]{"eine Insel", "ein Mädchenname", "ein Land"}, 
                LevelOfDifficulty.MIDDLE, Language.GERMAN));        
        qs03.add(new DBQuestion("Wann wurde der FC Schalke nicht DFB Pokalsieger ?", 
                "2005", new String[]{"2001", "2002", "2011"}, 
                LevelOfDifficulty.DIFFICULT, Language.GERMAN));
        qs03.add(new DBQuestion("Wer wurde bei der Leichtathletik-Weltmeisterschaft 2009 in Berlin Weltmeister über 100m Sprint ?", 
                "Usain Bolt", new String[]{"Tyson Gay", "Dwain Chambers", "Asafa Powell", "Maurice Greene"}, 
                LevelOfDifficulty.DIFFICULT, Language.GERMAN));
                
        /* ------------------------------------------------------------------ */
        
        qs04.add(new DBQuestion("Wie lautet die einzige gerade Primzahl ?", 
                "2", new String[]{"4", "5", "6"}, 
                LevelOfDifficulty.EASY, Language.GERMAN));
        qs04.add(new DBQuestion("Wen nannte man Ludwig der XIV. auch ?", 
                "Sonnenkönig", new String[]{"Mondkönig", "Sternkönig"}, 
                LevelOfDifficulty.EASY, Language.GERMAN));        
        qs04.add(new DBQuestion("Wie hieß der Plan für die deutsche Invasion in Frankreich im 1. Weltkrieg ?", 
                "Schlieffen Plan", new String[]{"Frankreich Plan", "Hindenburg Plan", "Barbarossa Plan"}, 
                LevelOfDifficulty.MIDDLE, Language.GERMAN));
        qs04.add(new DBQuestion("Was hat Wilhelm Schickhardt zusammen mit Kepler entwickelt ?", 
                "Rechenuhr", new String[]{"Lichterkette", "Sprengstoff", "Phosphor"}, 
                LevelOfDifficulty.MIDDLE, Language.GERMAN));        
        qs04.add(new DBQuestion("Zur welcher Stadt gehört der Ortsteil Östrich ?", 
                "Dorsten", new String[]{"Haltern", "Borken", "Bocholt"}, 
                LevelOfDifficulty.DIFFICULT, Language.GERMAN));
        qs04.add(new DBQuestion("In welcher Ort ist die Fh Gelsenkirchen nicht vetreten ?", 
                "Borken", new String[]{"Bocholt", "Recklinghausen", "Gelsenkirchen"}, 
                LevelOfDifficulty.DIFFICULT, Language.GERMAN));
        
        /* ------------------------------------------------------------------ */
        
        qs05.add(new DBQuestion("Wie heißt ein bekanntes Lied von Herber Grönemeyer ?", 
                "Bochum", new String[]{"Lüdenscheidt", "Schalke", "Duisburg"}, 
                LevelOfDifficulty.EASY, Language.GERMAN));
        qs05.add(new DBQuestion("Wo steht Schloss Neu-Schwanstein ?", 
                "Deutschland", new String[]{"Österreich", "Lichtenstein", "Schweiz"}, 
                LevelOfDifficulty.EASY, Language.GERMAN));
        qs05.add(new DBQuestion("Wie viel Finger mit Daumen haben die Simpsons ?", 
                "8", new String[]{"6", "10", "12"}, 
                LevelOfDifficulty.MIDDLE, Language.GERMAN)); 
        qs05.add(new DBQuestion("Welchem Regierungsbezirk gehört Haltern an ?", 
                "Münster", new String[]{"Essen", "Recklinghausen", "Bochum"}, 
                LevelOfDifficulty.MIDDLE, Language.GERMAN));
        qs05.add(new DBQuestion("Wie lautet die dritte Wurzel aus 27 ?", 
                "3", new String[]{"5", "7", "9"}, 
                LevelOfDifficulty.DIFFICULT, Language.GERMAN));
        qs05.add(new DBQuestion("Wo liegt der Kardinal-von-Galen-Park ?", 
                "Haltern", new String[]{"Borken", "Münster", "Dorsten-Gahlen"}, 
                LevelOfDifficulty.DIFFICULT, Language.GERMAN));
        
        /* ------------------------------------------------------------------ */
        
        qs06.add(new DBQuestion("Wo spricht man deutsch ?", 
                "Deutschland", new String[]{"USA", "Spanien"}, 
                LevelOfDifficulty.EASY, Language.GERMAN));
        qs06.add(new DBQuestion("Wie viele Seiten hat ein Würfel ?", 
                "6", new String[]{"5", "7", "8"}, LevelOfDifficulty.EASY, 
                Language.GERMAN));
        qs06.add(new DBQuestion("Worin liegt sprichwörtlich die Wahrheit ?", 
                "Wein", new String[]{"Feuer", "Sonne", "Wasser"}, 
                LevelOfDifficulty.MIDDLE, Language.GERMAN)); 
        qs06.add(new DBQuestion("Worin liegt sprichwörtlich die Wahrheit ?", 
                "Wein", new String[]{"Feuer", "Sonne", "Wasser"}, 
                LevelOfDifficulty.MIDDLE, Language.GERMAN)); 
        qs06.add(new DBQuestion("Wie heißt der berühmte Turm von Haltern am See ?", 
                "Siebenteufelsturm", new String[]{"Dreizackenturm", "schiefer Turm von Haltern", "Kronenturm"}, 
                LevelOfDifficulty.DIFFICULT, Language.GERMAN));
        qs06.add(new DBQuestion("Als was gilt Haltern im Volksmund ?", 
                "als Tor zum Münsterland", new String[]{"als Tor zum Ruhrgebiet", "als Stadt der 100 Seen", "als Stadt der 100 Kirchen"}, 
                LevelOfDifficulty.DIFFICULT, Language.GERMAN));
        
        /* ------------------------------------------------------------------ */
        
        qs07.add(new DBQuestion("Übung macht den... ", 
                "Meister", new String[]{"Idioten", "Helden"}, 
                LevelOfDifficulty.EASY, Language.GERMAN));
        qs07.add(new DBQuestion("Wo steht die Berliner Mauer ?", 
                "Berlin", new String[]{"Hamburg", "Bonn", "Dresden"}, 
                LevelOfDifficulty.EASY, Language.GERMAN));
        qs07.add(new DBQuestion("Wenn ein US-Student sehr gut ist, hat er ...?", 
                "ein A", new String[]{"ein F", "1", "6"}, 
                LevelOfDifficulty.MIDDLE, Language.GERMAN)); 
        qs07.add(new DBQuestion("Wie heißt der Moderator von Wer wird Millionär ?", 
                "Günter Jauch", new String[]{"Thomas Gottschalk", "Horst Schlemmer", 
                    "Frank Elstner"}, LevelOfDifficulty.MIDDLE, Language.GERMAN));
        qs07.add(new DBQuestion("Welcher Rapper rappte auf der Melodie des "
                + "Songs Every Breath You Take von The Police ?", 
                "Puff Daddy", new String[]{"50 Cent", "Snoop Dog"}, 
                LevelOfDifficulty.DIFFICULT, Language.GERMAN));
        qs07.add(new DBQuestion("In welcher Stadt leben/lebten die Aldi-Brüder ?", 
                "Mühlheim", new String[]{"Duisburg", "Bochum", "Oberhausen", "Essen"}, 
                LevelOfDifficulty.DIFFICULT, Language.GERMAN));
        
        /* ------------------------------------------------------------------ */
        
        qs08.add(new DBQuestion("Wann endete der 2. Weltkrieg ?", 
                "1945", new String[]{"1946", "1944", "1947"}, 
                LevelOfDifficulty.EASY, Language.GERMAN));
        qs08.add(new DBQuestion("Wo steht das Brandenburger Tor ?", 
                "Berlin", new String[]{"Essen", "Köln", "Potsdam"}, 
                LevelOfDifficulty.EASY, Language.GERMAN));
        qs08.add(new DBQuestion("Wo liegt der Gardasee ?", 
                "Italien", new String[]{"Schweiß", "Frankreich", "Österreich", "Deutschland"}, 
                LevelOfDifficulty.MIDDLE, Language.GERMAN));         
        qs08.add(new DBQuestion("In welcher Stadt spielt Romeo und Julia ?", 
                "Verona", new String[]{"Venedig", "Rom", "Venezuela", "London"}, 
                LevelOfDifficulty.MIDDLE, Language.GERMAN));
        qs08.add(new DBQuestion("In welcher Stadt spielt Romeo und Julia ?", 
                "Verona", new String[]{"Venedig", "Rom", "Venezuela", "London"}, 
                LevelOfDifficulty.DIFFICULT, Language.GERMAN));
        qs08.add(new DBQuestion("In welcher Stadt spielt Romeo und Julia ?", 
                "Verona", new String[]{"Venedig", "Rom", "Venezuela", "London"}, 
                LevelOfDifficulty.DIFFICULT, Language.GERMAN));
        
        /* ------------------------------------------------------------------ */
        
        qs09.add(new DBQuestion("Wie heißt der größte See Deutschlands ?", 
                "Bodensee", new String[]{"Chiemsee", "Heidesee", "Silbersee"}, 
                LevelOfDifficulty.EASY, Language.GERMAN));
        qs09.add(new DBQuestion("Wie heißt der größte See Deutschlands ?", 
                "Bodensee", new String[]{"Chiemsee", "Heidesee", "Silbersee"}, 
                LevelOfDifficulty.EASY, Language.GERMAN));
        qs09.add(new DBQuestion("In welchem Tatort ermitteln Professor Börne "
                + "alias Jan Josef Liefers und Frank Tiehl alias Axel Prahl ?", 
                "Tatort Münster", new String[]{"Tatort München", "Tatort Frankfurt", 
                    "Tatort Hamburg"}, 
                LevelOfDifficulty.MIDDLE, Language.GERMAN));
        qs09.add(new DBQuestion("In welcher Stadt spielt Romeo und Julia ?", 
                "Verona", new String[]{"Venedig", "Rom", "Venezuela", "London"}, 
                LevelOfDifficulty.MIDDLE, Language.GERMAN));
        qs09.add(new DBQuestion("Welches Land hat als erstes erfolgreich "
                + "einen Satelliten ins All geschossen ?", 
                "Russland", new String[]{"USA", "China", "England", "Kanada", 
                    "Frankreich"}, 
                LevelOfDifficulty.DIFFICULT, Language.GERMAN));
        qs09.add(new DBQuestion("Welches ist die Geburtsstadt von Cornelia Funke ?", 
                "Dorsten", new String[]{"Essen", "München", "Kaiserslautern"}, 
                LevelOfDifficulty.DIFFICULT, Language.GERMAN));
        
        /* ------------------------------------------------------------------ */
        
        qs10.add(new DBQuestion("Welche Stadt trägt den Spitznamen Mainhatten ?", 
                "Frankfurt", new String[]{"Nürnberg", "Mainz", "Bonn"}, 
                LevelOfDifficulty.EASY, Language.GERMAN));
        qs10.add(new DBQuestion("Wie heißt der weltbekannte Baumeister ?", 
                "Bob", new String[]{"Tob", "Jan", "Rob", "Tim", "Tod"}, 
                LevelOfDifficulty.EASY, Language.GERMAN));
        qs10.add(new DBQuestion("Wo fanden 1972 die olympischen Sommerspiele statt ?", 
                "München", new String[]{"Paris", "Berlin", "London"}, 
                LevelOfDifficulty.MIDDLE, Language.GERMAN));
        qs10.add(new DBQuestion("Wann wurde Deutschland nicht Fußball-Weltmeister ?", 
                "1986", new String[]{"1990", "1974", "1954"}, 
                LevelOfDifficulty.MIDDLE, Language.GERMAN));
        qs10.add(new DBQuestion("Was sind \"Pseudotropheus\" (lat.) ?", 
                "Buntbarsche", new String[]{"Vulkane", "Goldfische", "Erdbeben"}, 
                LevelOfDifficulty.DIFFICULT, Language.GERMAN));
        qs10.add(new DBQuestion("Schafft ein guter Sprinter über 100 m mehr oder weniger als 40 km/h ?", 
                "mehr", new String[]{"weniger"}, 
                LevelOfDifficulty.DIFFICULT, Language.GERMAN));
        
        /* ------------------------------------------------------------------ */
        
        qs11.add(new DBQuestion("Wer schrieb den Herr der Ringe ?", 
                "J.R.R. Tolkien", new String[]{"Douglas Adams", "Karl Marx", 
                    "Dan Brown"}, 
                LevelOfDifficulty.EASY, Language.GERMAN));
        qs11.add(new DBQuestion("Klappe zu ..?", 
                "...Affe tot", new String[]{"...Hunde laut", "...Ameisen platt", "...Giraffe schläft", "...Bär tanzt"}, 
                LevelOfDifficulty.EASY, Language.GERMAN));
        qs11.add(new DBQuestion("In welchem Land spielt der Herr der Ringe ?", 
                "Mittelerde", new String[]{"Europa", "Planetopia"}, 
                LevelOfDifficulty.MIDDLE, Language.GERMAN));
        qs11.add(new DBQuestion("Was heißt auf Platt Haus ?", 
                "Huus", new String[]{"Heem", "Home", "Butze"}, 
                LevelOfDifficulty.MIDDLE, Language.GERMAN));
        qs11.add(new DBQuestion("Wer oder was ist ein Buer (Plattdeutsch) ?", 
                "ein Bauer", new String[]{"ein aus Gelsenkirchen-Buer stammender", "ein Bürger"}, 
                LevelOfDifficulty.DIFFICULT, Language.GERMAN));
        qs11.add(new DBQuestion("Wie heiß der Onkel von Justus Jonas ?", 
                "Titus Jonas", new String[]{"Gustav Jonas", "Peter Jonas", "Bob Jonas"}, 
                LevelOfDifficulty.DIFFICULT, Language.GERMAN));
        
        /* ------------------------------------------------------------------ */
        
        qs12.add(new DBQuestion("Welches land ist von der Fläche größer ?", 
                "Russland", new String[]{"China", "USA", "Kanada"}, 
                LevelOfDifficulty.EASY, Language.GERMAN));
        qs12.add(new DBQuestion("Welches ist das größte Land Südamerikas ?", 
                "Brasilien", new String[]{"Chile", "Peru", "Argentinien"}, 
                LevelOfDifficulty.EASY, Language.GERMAN));
        qs12.add(new DBQuestion("Welches ist der wertvollste Euro-Schein ?", 
                "500er", new String[]{"1000er", "200er"}, 
                LevelOfDifficulty.MIDDLE, Language.GERMAN));
        qs12.add(new DBQuestion("Wieviele Einwohner hat die Stadt Haltern ca. ?", 
                "40 000", new String[]{"35 000", "45 000", "50 000"}, 
                LevelOfDifficulty.MIDDLE, Language.GERMAN));
        qs12.add(new DBQuestion("Welche Stadt hat mehr Einwohner ?", 
                "Dortmund", new String[]{"Essen"}, 
                LevelOfDifficulty.DIFFICULT, Language.GERMAN));
        qs12.add(new DBQuestion("wie heiß eine bekannte Folge der drei Fragezeichen ?", 
                "die drei ??? und der Super-Papagei", 
                new String[]{"die drei ??? und das Super-Auto", "die drei ??? und der Super-Hund"}, 
                LevelOfDifficulty.DIFFICULT, Language.GERMAN));
        
        /* ------------------------------------------------------------------ */
        
        qs13.add(new DBQuestion("Was wird in Knoten angegeben ?", 
                "Geschwindigkeit", new String[]{"Entfernung", "Zeit", "Druck"}, 
                LevelOfDifficulty.EASY, Language.GERMAN));
        qs13.add(new DBQuestion("Wie heißt eine der erlogreichsten Bands aller Zeiten ?", 
                "The Beatles", new String[]{"The Carpets", "The Snails", "The Mosquitoes"}, 
                LevelOfDifficulty.EASY, Language.GERMAN));
        qs13.add(new DBQuestion("Was bedeutet CB beim Begriff CB-Funk ?", 
                "Citizens Band", new String[]{"Computer Band", "Community Band", "Creative Band"}, 
                LevelOfDifficulty.MIDDLE, Language.GERMAN));
        qs13.add(new DBQuestion("Welcher dieser Filme ist nicht von Peter Jackson ?", 
                "The Dark Night", new String[]{"King Kong", "District 9", "Herr der Ringe"}, 
                LevelOfDifficulty.MIDDLE, Language.GERMAN));
        qs13.add(new DBQuestion("Wieviele Mägen hat eine Kuh ?", 
                "4", new String[]{"3", "2", "1", "5", "6", "7", "8"}, 
                LevelOfDifficulty.DIFFICULT, Language.GERMAN));
        qs13.add(new DBQuestion("Wie hieß der Frontmann der Band Nirvana ?", 
                "Kurt Cobain", new String[]{"Kurt Kobain", "Peter Cobain", "Peter Kobain"}, 
                LevelOfDifficulty.DIFFICULT, Language.GERMAN));
        
        /* ------------------------------------------------------------------ */
        
        qs14.add(new DBQuestion("Was ist auf einem deutschen 1 Cent-Stück abgebildet ?", 
                "ein Eichenblatt", new String[]{"das Brandenburger Tor", "Angela Merkel", 
                    "der Bundesadler"}, 
                LevelOfDifficulty.EASY, Language.GERMAN));
        qs14.add(new DBQuestion("Wer war Georg Friedrich Händel ?", "ein Komponist", 
                new String[]{"ein Autorennfahrer", "ein Politiker", 
                    "ein berühmter Schriftsteller"}, 
                LevelOfDifficulty.EASY, Language.GERMAN));
        qs14.add(new DBQuestion("Wieviel Kalorien hat natürliches Mineralwasser ungefähr?", 
                "0", new String[]{"10", "35", "50"}, 
                LevelOfDifficulty.MIDDLE, Language.GERMAN));
        qs14.add(new DBQuestion("Was ist eine bekanntes Computerspiel ?", 
                "Solitär", new String[]{"Sanitär", "Sozalitär"}, 
                LevelOfDifficulty.MIDDLE, Language.GERMAN));
        qs14.add(new DBQuestion("Wird Mais gedroschen oder gehexelt?", 
                "gehexelt", new String[]{"gedroschen"}, 
                LevelOfDifficulty.DIFFICULT, Language.GERMAN));
        qs14.add(new DBQuestion("Welches Fest feiert der Verkehrsverein Speyer jährlich ?", 
                "das Brezelfest", new String[]{"das Apfelfest", "das Oktoberfest"}, 
                LevelOfDifficulty.DIFFICULT, Language.GERMAN));
        
        /* ------------------------------------------------------------------ */
        
        qs15.add(new DBQuestion("Max und ... ?", 
                "Moritz", new String[]{"Peter", "Klaus", "Mustafa"}, 
                LevelOfDifficulty.EASY, Language.GERMAN));
        qs15.add(new DBQuestion("Wie viel liter sind 1 dm^3 ?", 
                "1", new String[]{"6", "12", "3"}, 
                LevelOfDifficulty.EASY, Language.GERMAN));
        qs15.add(new DBQuestion("Wie heißt der geizige Chef von SpongeBob?", 
                "Mr Krabs", new String[]{"Sandy", "Thaddäus Tentakel", "Patrick Star"}, 
                LevelOfDifficulty.MIDDLE, Language.GERMAN));
        qs15.add(new DBQuestion("Wie lautet ein James Bond-Film ?", 
                "Man lebt nur 2 mal", new String[]{"Man lebt nur 7 mal", "Man lebt keinmal"}, 
                LevelOfDifficulty.MIDDLE, Language.GERMAN));
        qs15.add(new DBQuestion("Welcher Fluss ist länger Rhein oder Elbe ?", 
                "Rhein", new String[]{"Elbe"}, 
                LevelOfDifficulty.DIFFICULT, Language.GERMAN));
        qs15.add(new DBQuestion("Wie heißt eine Figur aus Harry Potter ?", 
                "Nymphadora Tonks", new String[]{"Nymphadora Kronks", "Nymphadora Pinks"}, 
                LevelOfDifficulty.DIFFICULT, Language.GERMAN));
        
        /* ------------------------------------------------------------------ */
        
        qs16.add(new DBQuestion("Wer gewann die Fußballweltmeisterschaft 2010 ?", 
                "Spanien", new String[]{"Niederlande", "Deutschland", "Brasilien"}, 
                LevelOfDifficulty.EASY, Language.GERMAN));
        qs16.add(new DBQuestion("Max und ... ?", "Moritz", 
                new String[]{"Peter", "Klaus", "Mustafa"}, 
                LevelOfDifficulty.EASY, Language.GERMAN));
        qs16.add(new DBQuestion("Welcher Fluss ist der Längste der USA ?", 
                "Mississippi", new String[]{"Missouri", "Colorado"}, 
                LevelOfDifficulty.MIDDLE, Language.GERMAN));
        qs16.add(new DBQuestion("Wie lautet ein James Bond-Film ?", 
                "Man lebt nur 2 mal", new String[]{"Man lebt nur 7 mal", "Man lebt keinmal"}, 
                LevelOfDifficulty.MIDDLE, Language.GERMAN));
        qs16.add(new DBQuestion("Wie lautet in der Mathematik kein berühmter Satz ?", 
                "Der Satz des Metaphores", new String[]{"Der Satz des Phytagoras", "Der Satz des Thales"}, 
                LevelOfDifficulty.DIFFICULT, Language.GERMAN));
        qs16.add(new DBQuestion("Welcher Schauspieler war kein James Bond Darsteller ?", 
                "Tom Cruise", new String[]{"Timothy Dalton", "Pierce Brosnan", "Sean Connery", "George Lazenby", "Daniel Craig"}, 
                LevelOfDifficulty.DIFFICULT, Language.GERMAN));
        
        /* ------------------------------------------------------------------ */
        
        qs17.add(new DBQuestion("Welche Pyramide ist die Größte ?", 
                "Cheops", new String[]{"Gizeh", "Chephren", "Mykerinos"}, 
                LevelOfDifficulty.EASY, Language.GERMAN));
        qs17.add(new DBQuestion("Wie nennt man ein Fahrrad noch ?", 
                "Drahtesel", new String[]{"Drahtkuh", "Drahtziege", "Drahtpferd"}, 
                LevelOfDifficulty.EASY, Language.GERMAN));
        qs17.add(new DBQuestion("Welche Pyramide ist die Größte ?", 
                "Cheops", new String[]{"Gizeh", "Chephren", "Mykerinos"}, 
                LevelOfDifficulty.EASY, Language.GERMAN));
        qs17.add(new DBQuestion("Wie nennt man ein Fahrrad noch ?", 
                "Drahtesel", new String[]{"Drahtkuh", "Drahtziege", "Drahtpferd"}, 
                LevelOfDifficulty.EASY, Language.GERMAN));
        qs17.add(new DBQuestion("Welche Name ist nicht gleichzeitig ein Fluss und ein Bundesstaat der USA ?", 
                "Mississippi", new String[]{"Missouri", "Colorado"}, 
                LevelOfDifficulty.MIDDLE, Language.GERMAN));
        qs17.add(new DBQuestion("Wie lautet ein James Bond-Film ?", 
                "Man lebt nur 2 mal", new String[]{"Man lebt nur 7 mal", "Man lebt keinmal"}, 
                LevelOfDifficulty.MIDDLE, Language.GERMAN));
        qs17.add(new DBQuestion("Welche Dame spielte noch kein Bond-Girl ?", 
                "Julia Roberts", new String[]{"Halle Berry", "Sophie Marceau", "Eva Green"}, 
                LevelOfDifficulty.DIFFICULT, Language.GERMAN));
        qs17.add(new DBQuestion("In welcher Straße leben die Dursleys (Harry Potter) ?", 
                "Ligusterweg", new String[]{"Augustusstraße", "Löwenzahnweg", "Lagesterallee"}, 
                LevelOfDifficulty.DIFFICULT, Language.GERMAN));
        
        /* ------------------------------------------------------------------ */
        
        qs18.add(new DBQuestion("Was du nicht willst, dass man dir tut, das füg auch ...?", 
                "keinem anderen zu", new String[]{"nicht den Kindern zu", "nie mehr zu", "nicht dem Tiere zu"}, 
                LevelOfDifficulty.EASY, Language.GERMAN));
        qs18.add(new DBQuestion("Wann wurde der FC Schalke gegründet ?", 
                "1904", new String[]{"1905", "1903", "1899"}, 
                LevelOfDifficulty.EASY, Language.GERMAN));
        qs18.add(new DBQuestion("Wo steht ein Pferd ?", 
                "auf dem Flur", new String[]{"auf dem Haus", "auf dem See"}, 
                LevelOfDifficulty.MIDDLE, Language.GERMAN));
        qs18.add(new DBQuestion("Wie heißt das Pferd von Pippi Langstrumpf ?", 
                "Kleiner Onkel", new String[]{"Großer Onkel", "Kleiner Bruder", "Großer Bruder"}, 
                LevelOfDifficulty.MIDDLE, Language.GERMAN));
        qs18.add(new DBQuestion("Was feiert die katholische Kirche an Fronleichnam ?", 
                "leibliche Gegenwart Jesu Christi", new String[]{"Auferstehung Jesu Christi", "den heiligen Geist"}, 
                LevelOfDifficulty.DIFFICULT, Language.GERMAN));
        qs18.add(new DBQuestion("Wo liegt Mogadischu ?", 
                "Somalia", new String[]{"Kenia", "Ghana", "Ägypten"}, 
                LevelOfDifficulty.DIFFICULT, Language.GERMAN));
        
        /* ------------------------------------------------------------------ */
        
        qs19.add(new DBQuestion("Wer führte die Israeliten aus Ägypten ?", 
                "Moses", new String[]{"Jesus", "Noah", "Kain und Abel"}, 
                LevelOfDifficulty.EASY, Language.GERMAN));
        qs19.add(new DBQuestion("Welche Hautfarbe haben die Schlümpfe ?", 
                "Blau", new String[]{"Gelb", "Rot", "Grün", "Orange"}, 
                LevelOfDifficulty.EASY, Language.GERMAN));
        qs19.add(new DBQuestion("Unter welchem Namen ist Botulinumtoxin noch bekannt ?", 
                "Botox", new String[]{"Senfgas", "Rattengift", "LSD"}, 
                LevelOfDifficulty.MIDDLE, Language.GERMAN));
        qs19.add(new DBQuestion("Zu welcher Familie gehört die Kokosnuss ?", 
                "Strauchfrucht", new String[]{"Nuss", "Beere", "Steinfrucht"}, 
                LevelOfDifficulty.MIDDLE, Language.GERMAN));
        qs19.add(new DBQuestion("Wann geschah die erste Mondlandung ?", 
                "1969", new String[]{"1971", "1986", "1961", "1959"}, 
                LevelOfDifficulty.DIFFICULT, Language.GERMAN));
        qs19.add(new DBQuestion("Was ist ein Nippelspanner ?", 
                "Werkzeug für Fahrräder", new String[]{"Dosenöffner", "Kleidungsstück", "Erwachsenenspielzeug"}, 
                LevelOfDifficulty.DIFFICULT, Language.GERMAN));
        
        /* ------------------------------------------------------------------ */
        
        qs20.add(new DBQuestion("Wenn das Essen versalzen ist, dann ist der Koch ...?", 
                "verliebt", new String[]{"sauer", "traurig", "unaufmerksam"}, 
                LevelOfDifficulty.EASY, Language.GERMAN));
        qs20.add(new DBQuestion("Ein Fehl-Los ist eine ...?", 
                "Niete", new String[]{"Schraube", "Bolzen", "Geige", "Mutter"}, 
                LevelOfDifficulty.EASY, Language.GERMAN));
        qs20.add(new DBQuestion("Was wird in Farad angegeben ?", 
                "elektrische Kapazität", new String[]{"Frequenz", "Stromstärke"}, 
                LevelOfDifficulty.MIDDLE, Language.GERMAN));
        qs20.add(new DBQuestion("Welcher dieser Tiere hat die meisten Chromosomensätze ?", 
                "Amsel", new String[]{"Hecht", "Feuersalamander", "Grasfrosch"}, 
                LevelOfDifficulty.MIDDLE, Language.GERMAN));
        qs20.add(new DBQuestion("Welche Farbe hat die Pille, die Neo in \"the Matrix\" nimmt?", 
                "Rot", new String[]{"Gelb", "Blau", "Grün", "Orange"}, 
                LevelOfDifficulty.DIFFICULT, Language.GERMAN));
        qs20.add(new DBQuestion("Wie ist der zweite Vorname von Donald Duck ?", 
                "Fauntleroy", new String[]{"Gus", "Homer", "Wilson"}, 
                LevelOfDifficulty.DIFFICULT, Language.GERMAN));
        
        /* ------------------------------------------------------------------ */
        
        qs21.add(new DBQuestion("Was bedeutet 100 PS bei Autos ?", 
                "100 Pferdestärken", new String[]{"100 Protonenstrahlen", "100 Pfundstücke", "100 Pausenstunden"}, 
                LevelOfDifficulty.EASY, Language.GERMAN));
        qs21.add(new DBQuestion("Hans im...?", 
                "Glück", new String[]{"Pech", "Haus", "Bett", "Beet"}, 
                LevelOfDifficulty.EASY, Language.GERMAN));
        qs21.add(new DBQuestion("Was ist die Feierabendstellung ?", 
                "Frei drehender Kran", new String[]{"Die Schlange vor dem Arbeitsamt", "Gelb blinkende Ampel", "Stellung beim Schach"}, 
                LevelOfDifficulty.MIDDLE, Language.GERMAN));
        qs21.add(new DBQuestion("Welches Buch wandte sich vehement gegen die Sklaverei in den USA?", 
                "Onkel Toms Hütte", new String[]{"Black Streets", "Tom Sawyer", "Lederstrumpf"}, 
                LevelOfDifficulty.MIDDLE, Language.GERMAN));
        qs21.add(new DBQuestion("Mit welchem Computer wurde die Lorenz-Schlüsselmaschine im 2. Weltkrieg entschlüsselt ?", 
                "Colossus", new String[]{"Zuse", "Caesar", "Enigma"}, 
                LevelOfDifficulty.DIFFICULT, Language.GERMAN));
        qs21.add(new DBQuestion("Was bezeichnet keinen Instant Messanger ?", 
                "Buzz", new String[]{"Skype", "MSN", "ICQ"}, 
                LevelOfDifficulty.DIFFICULT, Language.GERMAN));
        
        /* ------------------------------------------------------------------ */
        
        
        List<Collection<DBQuestion>> cqs = 
                new ArrayList<Collection<DBQuestion>>();
        cqs.add(qs01);
        cqs.add(qs02);
        cqs.add(qs03);
        cqs.add(qs04);
        cqs.add(qs05);
        cqs.add(qs06);
        cqs.add(qs07);
        cqs.add(qs08);
        cqs.add(qs09);
        cqs.add(qs10);
        cqs.add(qs11);
        cqs.add(qs12);
        cqs.add(qs13);
        cqs.add(qs14);
        cqs.add(qs15);
        cqs.add(qs16);
        cqs.add(qs17);
        cqs.add(qs18);
        cqs.add(qs19);
        cqs.add(qs20);
        cqs.add(qs21);
        
        return cqs;
    }
    
    /**
     * Setzt die Daten des Zielpunktes richtig.
     * @param tp Zielpunkt
     * @param pos Position
     * @param infos Informationen
     * @param questions Fragen
     */
    private static DBTargetPoint setTargetPoint(DBTargetPoint tp, DBPosition pos, 
            Collection<DBInformation> infos, Collection<DBQuestion> questions) {
               
        pos.setTargetPoint(tp);
        
        for(DBInformation info : infos)
            info.setTargetPoint(tp);
        
        Collection<DBQuestion> newQuestions = new ArrayList<DBQuestion>();
        for(DBQuestion question : questions) {
            newQuestions.add(new DBQuestion(question.getQuestion(), 
                    question.getCorrectAnswer(), question.getWrongAnswers(), 
                    question.getLevelOfDifficulty(), question.getLanguage(), tp));
        }
        
        tp.setPosition(pos);
        tp.setInformations(infos);
        tp.setQuestions(newQuestions);        
        
        return tp;
    }
}
