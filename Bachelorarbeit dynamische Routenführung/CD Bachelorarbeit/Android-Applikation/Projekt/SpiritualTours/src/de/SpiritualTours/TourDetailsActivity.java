package de.SpiritualTours;

import java.util.ArrayList;

import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.PathOverlay;
import org.osmdroid.views.overlay.ScaleBarOverlay;
import org.osmdroid.views.overlay.SimpleLocationOverlay;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import de.SpiritualTours.TourObjects.Tour;
import de.SpiritualTours.TourObjects.Tour.TourType;
import de.SpiritualTours.TourObjects.WayPoint;
import de.SpiritualTours.Utility.ActivityHelper;
import de.SpiritualTours.Utility.IntentConstants;
import de.SpiritualTours.Utility.Navigate;
import de.SpiritualTours.Utility.TourSaver;
import de.SpiritualTours.Utility.ToursAdministrator;
import de.SpiritualTours.View.MapOverlay.TargetPointsOverlay;

/**
 * Zeigt die Tour in einer Karte und andere n�tzliche Informationen an.
 */
public class TourDetailsActivity extends Activity {

	/** Tour. */
	private Tour tour;
	/** Name der Tour. */
	private String tourname;
	/** ID der Tour. */
	private int tourId;

	/** Textfeld f�r den Tour-Namen. */
	private TextView tourTextView;
	/** Textfeld f�r den Tour-Typen. */
	private TextView tourTypeTextView;
	/** Textfeld f�r die Distanz der Tour. */
	private TextView lengthTextView;
	/** Textfeld f�r die Dauer der Tour. */
	private TextView timeTextView;
	/** Textfeld f�r die Zielpunkte der Tour. */
	private TextView targetPointsTextView;

	/** Image-Button zur Anzeige der Zielpunkte. */
	private ImageButton targetPointsButton;
	/** Zur�ck-Button. */
	private Button backButton;
	/** Start-Button. */
	private Button startButton;

	/** Karte. */
	private MapView mapView;
	/** Overlay f�r Scala der Map. */
	private ScaleBarOverlay scaleBarOverlay;
	/** Overlay zur Anzeige der Position in der Map. */
	private SimpleLocationOverlay locationOverlay;
	/** Overlay zur Anzeige der Strecke in der Map. */
	private PathOverlay pathOverlay;
	/** Overlay zur Anzeige der Zielpunkte in der Map. */
	private TargetPointsOverlay targetPointsOverlay;

	/** Tour-Administrator. */
	private ToursAdministrator toursAdministrator;

	/* ======================================================================= */
	/* OnCreate */
	/* ======================================================================= */
	/**
	 * Erzeugt eine Activity zur Anzeige einer Tour.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.tour_details);

		getTour();
		init();
		setTour();
	}

	/* ======================================================================= */
	/* Initialisieren */
	/* ======================================================================= */
	/**
	 * Liest die �bergebene Tour ein.
	 */
	private void getTour() {

		/* Tour aus Intent lesen */
		this.toursAdministrator = ToursAdministrator.getInstance();
		this.tourname = this.getIntent().getExtras()
				.getString(IntentConstants.TOUR_NAME);
		this.tourId = this.getIntent().getExtras()
				.getInt(IntentConstants.TOUR_ID);
		this.tour = this.toursAdministrator.getTour(tourId);
	}

	/**
	 * Initialisiert die GUI-Kompenenten und setzt die Daten.
	 */
	private void init() {
		/* Android-GUI-Elemente initialisieren */
		this.tourTextView = (TextView) findViewById(R.id.td_tourname);
		this.tourTypeTextView = (TextView) findViewById(R.id.td_type);
		this.lengthTextView = (TextView) findViewById(R.id.td_length);
		this.timeTextView = (TextView) findViewById(R.id.td_time);
		this.targetPointsTextView = (TextView) findViewById(R.id.td_targetpoints);
		this.targetPointsButton = (ImageButton) findViewById(R.id.td_targetpoints_button);
		this.targetPointsButton.setOnClickListener(bTargetPointsListener);
		this.backButton = (Button) findViewById(R.id.td_back);
		this.backButton.setOnClickListener(bBackListener);
		this.startButton = (Button) findViewById(R.id.td_start);
		this.startButton.setOnClickListener(bStartListener);

		/* Map */
		this.mapView = (MapView) this.findViewById(R.id.tpd_mapview);

		this.mapView.setTileSource(TileSourceFactory.MAPNIK);
		this.mapView.setBuiltInZoomControls(true);
		this.mapView.setMultiTouchControls(true);

		/* Mittelpunkt setzen */
		this.mapView.getController().setCenter(getMiddlePoint());

		/* Pfad setzen */
		setPathAndSetZoomToSpan();
		/* Zielpunkt setzen */
		setTargetPoints();

		/* Startposition (Person) in Karte setzen */
		GeoPoint startPosition = new GeoPoint(this.tour.getWayPoints().get(0)
				.getGeoPoint().getLatitude(), this.tour.getWayPoints().get(0)
				.getGeoPoint().getLongitude());
		this.locationOverlay = new SimpleLocationOverlay(this);
		this.mapView.getOverlays().add(locationOverlay);
		this.locationOverlay.setLocation(new GeoPoint(startPosition));

		/* Skala setzen */
		this.scaleBarOverlay = new ScaleBarOverlay(this);
		this.mapView.getOverlays().add(scaleBarOverlay);

		/* Navigation starten */
		Navigate.getInstance().startNavigation(this.tour.getWayPoints());
	}

	/**
	 * Setzt den Tourpfad in der Map.
	 */
	private void setPathAndSetZoomToSpan() {

		this.pathOverlay = new PathOverlay(Color.BLUE, this);
		this.mapView.getOverlays().add(this.pathOverlay);

		double minLatitude = Double.MAX_VALUE;
		double maxLatitude = Double.MIN_VALUE;
		double minLongitude = Double.MAX_VALUE;
		double maxLongitude = Double.MIN_VALUE;

		/* Setzen der Wegpunkte */
		for (WayPoint wayPoint : this.tour.getWayPoints()) {

			double latitude = wayPoint.getGeoPoint().getLatitude();
			double longitude = wayPoint.getGeoPoint().getLongitude();

			this.pathOverlay.addPoint(new GeoPoint(latitude, longitude));

			minLatitude = latitude < minLatitude ? latitude : minLatitude;
			maxLatitude = latitude > maxLatitude ? latitude : maxLatitude;
			minLongitude = longitude < minLongitude ? longitude : minLongitude;
			maxLongitude = longitude > maxLongitude ? longitude : maxLongitude;
		}

		zoomToSpan((int) ((maxLatitude - minLatitude) * 1E6),
				(int) ((maxLongitude - minLongitude) * 1E6));

		/* Animate to the center cluster of point */
		this.mapView.getController().setCenter(
				new GeoPoint((maxLatitude + minLatitude) / 2,
						(maxLongitude + minLongitude) / 2));

	}

	/**
	 * Zoomt passend auf die Tour.
	 * 
	 * @param reqLatSpan
	 *            Spannweite f�r Breitengrad
	 * @param reqLonSpan
	 *            Spannweite f�r L�ngengrad
	 */
	public void zoomToSpan(final int reqLatSpan, final int reqLonSpan) {

		if (reqLatSpan <= 0 || reqLonSpan <= 0) {
			return;
		}

		final int curZoomLevel = this.mapView.getZoomLevel();
		int curLatSpan = this.mapView.getBoundingBox().getLatitudeSpanE6();
		int curLonSpan = this.mapView.getBoundingBox().getLongitudeSpanE6();

		/* Eventuell ist die Map noch nicht vollst�ndig initialisiert. */
		if (curLatSpan == 0) {
			curLatSpan = 145981560;
		}
		if (curLonSpan == 0) {
			curLonSpan = 358593750;
		}

		final float diffNeededLat = (float) reqLatSpan / curLatSpan;
		final float diffNeededLon = (float) reqLonSpan / curLonSpan;
		final float diffNeeded = Math.max(diffNeededLat, diffNeededLon);

		if (diffNeeded > 1) {
			this.mapView.getController().setZoom(
					curZoomLevel - getNextSquareNumberAbove(diffNeeded));
		} else if (diffNeeded < 0.5) {
			this.mapView.getController()
					.setZoom(
							curZoomLevel
									+ getNextSquareNumberAbove(1 / diffNeeded)
									- 1);
		}
	}

	/**
	 * Hilfsmethode zum Zoomen.
	 * 
	 * @param factor
	 *            Faktor
	 * @return Wert
	 */
	private static int getNextSquareNumberAbove(final float factor) {
		int out = 0;
		int cur = 1;
		int i = 1;
		while (true) {
			if (cur > factor)
				return out;

			out = i;
			cur *= 2;
			i++;
		}
	}

	/**
	 * Setzt die Zielpunkte in der Map.
	 */
	private void setTargetPoints() {
		/* Zielpunkt */
		this.targetPointsOverlay = new TargetPointsOverlay(this,
				this.tour.getTargetPoints());
		this.mapView.getOverlays().add(this.targetPointsOverlay);
	}

	/**
	 * Setzt die Informationen zur Tour.
	 */
	private void setTour() {

		this.tourTextView.setText(this.tourname);
		this.tourTypeTextView
				.setText(getString(R.string.tour_type)
						+ " "
						+ (tour.getTourType().equals(TourType.FLATLY) ? getString(R.string.flatly)
								: getString(R.string.route)));
		this.lengthTextView
				.setText(getString(R.string.distance)
						+ " "
						+ ActivityHelper.getDistance(this.tour
								.getEstimatedTourLength()));
		this.timeTextView.setText(getString(R.string.duration)
				+ " "
				+ ActivityHelper.getTime(this,
						this.tour.getEstimatedDuration(), false));
		this.targetPointsTextView.setText(getString(R.string.targetpoints)
				+ " (" + getString(R.string.number) + " "
				+ this.tour.getTargetPoints().size() + "):");
	}

	/**
	 * Berechnet den Mittelpunkt zwischen allen Koordinatenpunkten, um die Tour
	 * zu zentieren.
	 * 
	 * @return Mittelpunkt der Tour.
	 */
	private GeoPoint getMiddlePoint() {

		double latitude = 0;
		double longitude = 0;

		for (WayPoint targetPoint : this.tour.getWayPoints()) {
			latitude += targetPoint.getGeoPoint().getLatitude();
			longitude += targetPoint.getGeoPoint().getLongitude();
		}

		return new GeoPoint(latitude / this.tour.getWayPoints().size(),
				longitude / this.tour.getWayPoints().size());
	}

	/* ======================================================================= */
	/* Men� */
	/* ======================================================================= */
	/**
	 * Anzeigen des Hauptmenues.
	 */
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.tour_details_menu, menu);
		return super.onCreateOptionsMenu(menu);
	}

	/**
	 * Event-Handling des Menues.
	 * 
	 * @param item
	 *            Menuepunkt, der gewaehlt wurde.
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		/* Handle item selection */
		switch (item.getItemId()) {
		case R.id.menu_td_help:
			onClickHelp();
			return true;
		case R.id.menu_td_save_tour:
			onClickSaveTour();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/**
	 * Aktionen bei Klick auf Settings-Button.
	 */
	protected void onClickHelp() {
		showMessageBox(getString(R.string.menu_help),
				getString(R.string.td_info));
	}

	/**
	 * Speichern der Tour.
	 */
	protected void onClickSaveTour() {
		TourSaver.saveTour(this, this.tour);
	}

	/* ======================================================================= */
	/* Zielpunkte-Button */
	/* ======================================================================= */
	/**
	 * OnClickListener f�r Settings-Button.
	 */
	private OnClickListener bTargetPointsListener = new OnClickListener() {

		/**
		 * Wird bei Klick auf den Button aufgerufen.
		 * 
		 * @param v
		 *            View
		 */
		public void onClick(View v) {
			onClickTargetPoints(v);
		}
	};

	/**
	 * Aktionen bei Klick auf Settings-Button.
	 * 
	 * @param v
	 *            View
	 */
	protected void onClickTargetPoints(View v) {

		Intent targetPoints = new Intent(this, TargetPointsActivity.class);
		targetPoints.putParcelableArrayListExtra(IntentConstants.TARGETPOINTS,
				(ArrayList<? extends Parcelable>) this.tour.getTargetPoints());
		startActivity(targetPoints);
	}

	/* ======================================================================= */
	/* Back-Button */
	/* ======================================================================= */
	/**
	 * OnClickListener f�r Back-Button.
	 */
	private OnClickListener bBackListener = new OnClickListener() {

		/**
		 * Wird bei Klick auf den Button aufgerufen.
		 * 
		 * @param v
		 *            View
		 */
		public void onClick(View v) {
			onClickBack(v);
		}
	};

	/**
	 * Activity schliessen und zur Vater-Activity zur�ck.
	 * 
	 * @param v
	 *            View
	 */
	protected void onClickBack(View v) {

		finish();
	}

	/* ======================================================================= */
	/* Start-Button */
	/* ======================================================================= */
	/**
	 * OnClickListener f�r Back-Button.
	 */
	private OnClickListener bStartListener = new OnClickListener() {

		/**
		 * Wird bei Klick auf den Button aufgerufen.
		 * 
		 * @param v
		 *            View
		 */
		public void onClick(View v) {
			onClickStart(v);
		}
	};

	/**
	 * Activity schliessen und zur Vater-Activity zur�ck.
	 * 
	 * @param v
	 *            View
	 */
	protected void onClickStart(View v) {

		Intent navigate = new Intent(this, NavigateActivity.class);
		navigate.putExtra(IntentConstants.TOUR_ID, this.tourId);
		startActivity(navigate);
	}

	/* ======================================================================= */
	/* MSG-Box */
	/* ======================================================================= */
	/**
	 * Zeigte eine Info-Fenster mit einem Ok-Button zum Schlie�en.
	 * 
	 * @param context
	 *            Context
	 * @param title
	 *            Titel
	 * @param message
	 *            Meldung
	 */
	public void showMessageBox(String title, String message) {

		AlertDialog msgBox = new AlertDialog.Builder(this).create();
		msgBox.setTitle(title);
		msgBox.setMessage(message);
		msgBox.setButton(getString(R.string.button_ok),
				new android.content.DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						/* Fenster schlie�en */
					}
				});
		msgBox.show();
	}
}
