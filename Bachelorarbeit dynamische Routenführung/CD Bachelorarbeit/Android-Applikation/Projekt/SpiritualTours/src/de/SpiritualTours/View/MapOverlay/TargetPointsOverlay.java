package de.SpiritualTours.View.MapOverlay;

import java.util.ArrayList;
import java.util.List;

import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Overlay;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Point;
import de.SpiritualTours.R;
import de.SpiritualTours.TourObjects.TargetPoint;

/**
 * Overlay, um die Zielpunkte anzuzeigen.
 */
public class TargetPointsOverlay extends Overlay {

	/**
	 * Zielpunkte (Geo-Koordinaten), die markiert werden sollen.
	 */
	private List<GeoPoint> geoPoints;

	/**
	 * Resources.
	 */
	private Resources resources;

	/**
	 * Erzeugt ein neues TargetPointsOverlay.
	 * 
	 * @param ctx
	 *            Context
	 * @param targetPoints
	 *            Zielpunkte die gel�scht werden sollen.
	 */
	public TargetPointsOverlay(Context ctx, List<TargetPoint> targetPoints) {
		super(ctx);
		this.resources = ctx.getResources();

		this.geoPoints = new ArrayList<GeoPoint>();
		for (int i = targetPoints.size() - 1; i >= 0; i--) {
			this.geoPoints.add(new GeoPoint(targetPoints.get(i).getGeoPoint()
					.getLatitude(), targetPoints.get(i).getGeoPoint()
					.getLongitude()));
		}
	}

	/**
	 * Erzeugt ein neues TargetPointsOverlay.
	 * 
	 * @param ctx
	 *            Context
	 * @param targetPoints
	 *            Zielpunkte die gel�scht werden sollen.
	 */
	public TargetPointsOverlay(Context ctx, TargetPoint targetPoint) {
		super(ctx);
		this.resources = ctx.getResources();

		this.geoPoints = new ArrayList<GeoPoint>();
		this.geoPoints.add(new GeoPoint(
				targetPoint.getGeoPoint().getLatitude(), targetPoint
						.getGeoPoint().getLongitude()));
	}

	/**
	 * Zeichnet das Overlay.
	 * 
	 * @param canvas
	 *            Canvas
	 * @param mapView
	 *            MapView
	 * @param shadow
	 *            Schatten anzeigen?
	 */
	@Override
	protected void draw(Canvas canvas, MapView mapView, boolean shadow) {

		for (GeoPoint geoPoint : this.geoPoints) {
			Point screenPts = new Point();
			mapView.getProjection().toPixels(geoPoint, screenPts);

			/* Zielflagge hinzuf�gen */
			Bitmap bmp = BitmapFactory.decodeResource(resources,
					R.drawable.map_targetpoint);

			canvas.drawBitmap(bmp, screenPts.x - 20, screenPts.y - 20, null);
		}
	}
}
