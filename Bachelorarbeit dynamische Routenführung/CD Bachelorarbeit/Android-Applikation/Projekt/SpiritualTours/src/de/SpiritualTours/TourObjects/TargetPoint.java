package de.SpiritualTours.TourObjects;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Definiert einen Zielpunkt. Zugeh�rige Informationen sind Name des Objekts,
 * die Art des Objekts, die Position, und eine Beschreibung.
 */
public class TargetPoint extends WayPoint implements Parcelable {

	/* ====================================================================== */
	/* Konstanten */
	/* ====================================================================== */
	/** Kostante f�r ID. */
	private static final String TARGETPOINT_ID = "id";
	/** Kostante f�r Name des Zielpunkts. */
	private static final String TARGETPOINT_NAME = "name";
	/** Kostante f�r Art des Zielpunkts. */
	private static final String TARGETPOINT_TYPE = "targetPointType";
	/** Kostante f�r Informationen zum Zielpunkt. */
	private static final String TARGETPOINT_INFORMATION = "information";

	/** Kostante f�r Informationen zum Zielpunkt. */
	private static final String TARGETPOINT_POSTCODE = "postcode";
	/** Kostante f�r Informationen zum Zielpunkt. */
	private static final String TARGETPOINT_TOWN = "town";
	/** Kostante f�r Informationen zum Zielpunkt. */
	private static final String TARGETPOINT_STREET = "street";

	/** Kostante f�r Informationen zum Zielpunkt. */
	private static final String TARGETPOINT_ORDER_ID = "orderID";
	/** Kostante f�r Informationen zum Zielpunkt. */
	private static final String TARGETPOINT_RANGE = "range";
	/** Kostante f�r Informationen zum Zielpunkt. */
	private static final String TARGETPOINT_GEOPOINT = "geoPoint";
	/** Kostante f�r Informationen zum Zielpunkt. */
	private static final String POINT_TYPE = "pointType";

	/* ====================================================================== */
	/* Instanzvariablen */
	/* ====================================================================== */
	/** Id des Zielpunktes. */
	private long id;
	/** Name des Zielpunkts. */
	private String name;
	/** Art des Zielpunkts. */
	private TargetPointType targetPointType;
	/** Informationen zum Zielpunkt. */
	private String information;
	/** Postleitzahl. */
	private int postcode;
	/** Ort. */
	private String town;
	/** Stra�e/Kreuzung */
	private String street;

	/* ====================================================================== */
	/* Konstruktoren */
	/* ====================================================================== */
	/**
	 * Erzeugt einen Zielpunkt.
	 * 
	 * @param orderID
	 *            Rang in der Reihenfolge der Wegpunkte
	 * @param range
	 *            Reichweite um den Wegpunkt herum in Meter
	 * @param geoPoint
	 *            geografische Koordinate
	 * @param postcode
	 *            Postleitzahl
	 * @param town
	 *            Ort
	 * @param street
	 *            Stra�e bzw. Kreuzung
	 * @param id
	 *            Id des Zielpunkts
	 * @param name
	 *            Name des Zielpunkts
	 * @param targetPointType
	 *            Art des Zielpunkts
	 * @param information
	 *            Info zum Zielpunkt
	 */
	public TargetPoint(int orderID, int range, GeoPoint geoPoint, int postcode,
			String town, String street, long id, String name,
			TargetPointType targetPointType, String information) {

		super(orderID, geoPoint, PointType.TARGET_POINT);
		this.setRange(range);

		this.postcode = postcode;
		this.town = town;
		this.street = street;

		this.id = id;
		this.name = name;
		this.targetPointType = targetPointType;
		this.information = information;
	}

	/**
	 * Erzeugt einen Zielpunkt.
	 * 
	 * @param name
	 *            Name des Zielpunktes
	 * @param wayPoint
	 *            Wegpunkt
	 * @param targetPointType
	 *            Zielpunkttyp
	 */
	public TargetPoint(String name, WayPoint wayPoint,
			TargetPointType targetPointType) {

		super(wayPoint.getOrderID(), wayPoint.getGeoPoint(),
				PointType.TARGET_POINT);
		this.setRange(wayPoint.getRange());

		this.postcode = 0;
		this.town = "";
		this.street = "";

		this.id = 0;
		this.name = name;
		this.targetPointType = targetPointType;
		this.information = "";
	}

	/**
	 * Erzeugt einen Zielpunkt.
	 * 
	 * @param jsonString
	 *            JSON-String
	 * @throws NumberFormatException
	 *             bei fehlerhaften Zahlenwerten
	 * @throws JSONException
	 *             bei fehlerhaften JSON-String
	 */
	public TargetPoint(JSONObject jsonTargetPoint)
			throws NumberFormatException, JSONException {

		super(Integer.valueOf(jsonTargetPoint.getString(TARGETPOINT_ORDER_ID)),
				new GeoPoint(
						jsonTargetPoint.getJSONObject(TARGETPOINT_GEOPOINT)),
				PointType.valueOf(jsonTargetPoint.getString(POINT_TYPE)));

		this.setRange(Integer.valueOf(jsonTargetPoint
				.getString(TARGETPOINT_RANGE)));

		this.postcode = Integer.valueOf(jsonTargetPoint
				.getString(TARGETPOINT_POSTCODE));
		this.town = jsonTargetPoint.getString(TARGETPOINT_TOWN);
		this.street = jsonTargetPoint.getString(TARGETPOINT_STREET);

		this.id = Integer.valueOf(jsonTargetPoint.getString(TARGETPOINT_ID));
		this.name = jsonTargetPoint.getString(TARGETPOINT_NAME);
		this.targetPointType = TargetPointType.valueOf(jsonTargetPoint
				.getString(TARGETPOINT_TYPE));
		this.information = jsonTargetPoint.getString(TARGETPOINT_INFORMATION);
	}

	/**
	 * Leerer Konstruktor.
	 */
	public TargetPoint() {
	}

	/* ====================================================================== */
	/* Getter & Setter */
	/* ====================================================================== */
	/**
	 * Gibt die ID des Zielpunkts.
	 * 
	 * @return ID des Zielpunkts.
	 */
	public long getId() {
		return id;
	}

	/**
	 * Setzt die ID des Zielpunkts.
	 * 
	 * @param ID
	 *            des Zielpunkts.
	 */
	public void setId(long id) {
		this.id = id;
	}

	/* ---------------------------------------------------------------------- */
	/**
	 * Gibt den Namen des Zielpunkts.
	 * 
	 * @return Namen des Zielpunkts.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Setzt den Namen des Zielpunkts.
	 * 
	 * @param Namen
	 *            des Zielpunkts.
	 */
	public void setName(String name) {
		this.name = name;
	}

	/* ---------------------------------------------------------------------- */
	/**
	 * Gibt den Typen des Zielpunkts.
	 * 
	 * @return Typ des Zielpunkts.
	 */
	public TargetPointType getTargetPointType() {
		return targetPointType;
	}

	/**
	 * Setzt den Typen des Zielpunkts.
	 * 
	 * @param Typ
	 *            des Zielpunkts.
	 */
	public void setTargetPointType(TargetPointType targetPointType) {
		this.targetPointType = targetPointType;
	}

	/* ---------------------------------------------------------------------- */
	/**
	 * Gibt die Informationen zum Zielpunkts.
	 * 
	 * @return Informationen zum Zielpunkts.
	 */
	public String getInformation() {
		return information;
	}

	/**
	 * Setzt die Informationen zum Zielpunkts.
	 * 
	 * @param Informationen
	 *            zum Zielpunkts.
	 */
	public void setInformation(String information) {
		this.information = information;
	}

	/* ---------------------------------------------------------------------- */
	/**
	 * Gibt die Postleitzahl der Position.
	 * 
	 * @return Postleitzahl
	 */
	public int getPostcode() {
		return postcode;
	}

	/**
	 * Setzt die Postleitzahl der Position.
	 * 
	 * @param postcode
	 *            Postleitzahl der Position
	 */
	public void setPostcode(int postcode) {
		this.postcode = postcode;
	}

	/* ---------------------------------------------------------------------- */
	/**
	 * Gibt den Ort der Position.
	 * 
	 * @return Ort der Position
	 */
	public String getTown() {
		return town;
	}

	/**
	 * Setzt den Ort der Position
	 * 
	 * @param town
	 *            Ort der Position
	 */
	public void setTown(String town) {
		this.town = town;
	}

	/* ---------------------------------------------------------------------- */
	/**
	 * Gibt die Stra�e der Position
	 * 
	 * @return Stra�e der Position
	 */
	public String getStreet() {
		return street;
	}

	/**
	 * Setzt die Stra�e der Position
	 * 
	 * @param street
	 *            Stra�e der Position
	 */
	public void setStreet(String street) {
		this.street = street;
	}

	/* ======================================================================= */
	/* Parcelable-Methoden */
	/* ======================================================================= */
	public static final Parcelable.Creator<TargetPoint> CREATOR = new Parcelable.Creator<TargetPoint>() {

		public TargetPoint createFromParcel(Parcel in) {
			return new TargetPoint(in);
		}

		public TargetPoint[] newArray(int size) {
			return new TargetPoint[size];
		}
	};

	private TargetPoint(final Parcel in) {
		readFromParcel(in);
	}

	public void writeToParcel(Parcel out, int flags) {

		out.writeInt(this.getOrderID());
		out.writeDouble(this.getGeoPoint().getLatitude());
		out.writeDouble(this.getGeoPoint().getLongitude());
		out.writeDouble(this.getGeoPoint().getAltitude());
		out.writeString(this.getPointType().name());
		out.writeInt(this.getRange());

		out.writeInt(this.postcode);
		out.writeString(this.town);
		out.writeString(this.street);

		out.writeLong(this.id);
		out.writeString(this.name);
		out.writeString(this.targetPointType.name());
		out.writeString(this.information);
	}

	public void readFromParcel(final Parcel in) {

		this.setOrderID(in.readInt());
		GeoPoint geoPoint = new GeoPoint();
		geoPoint.setLatitude(in.readDouble());
		geoPoint.setLongitude(in.readDouble());
		geoPoint.setAltitude(in.readDouble());
		this.setGeoPoint(geoPoint);
		this.setPointType(PointType.valueOf(in.readString()));
		this.setRange(in.readInt());

		this.postcode = in.readInt();
		this.town = in.readString();
		this.street = in.readString();

		this.id = in.readLong();
		this.name = in.readString();
		this.targetPointType = TargetPointType.valueOf(in.readString());
		this.information = in.readString();
	}

	public int describeContents() {
		return 0;
	}

	/* ====================================================================== */
	/* toString, equals, ... */
	/* ====================================================================== */
	/**
	 * Pr�ft, ob der Zielpunkt vollst�ndig initialisiert wurde.
	 * 
	 * @return true, wenn Zielpunkt vollst�ndig initialisiert ist
	 */
	public boolean isTargetPointCompletelyInitialized() {
		return this.name != null && this.targetPointType != null
				&& this.information != null && this.town != null
				&& this.street != null && this.getGeoPoint() != null;
	}

	/**
	 * Name des Zielpunktes.
	 * 
	 * @return Name des Zielpunktes
	 */
	@Override
	public String toString() {
		return this.name;
	}

	/**
	 * Objekt als String, mit allen interessanten Informationen, au�er den
	 * Fragen. (Name des Zielpunkts, Typ, Position, Informationen & Reichweite)
	 * 
	 * @return Objekt als String
	 */
	public String toStringCompletely() {
		return "Name: " + name + ",\n" + "Typ: " + targetPointType + ",\n"
				+ "Information: " + information + ",\n" + "PLZ: " + postcode
				+ ",\n" + "Town: " + town + ",\n" + "Stra�e: " + street + ",\n"
				+ super.toString();
	}

	/**
	 * Gibt das Objekt als JSON-String.
	 * 
	 * @return JSON-String
	 * @throws JSONException
	 */
	public JSONObject toJSON() throws JSONException {
		return new JSONObject("{\"geoPoint\":" + this.getGeoPoint().toJSON()
				+ ",\"id\":" + this.id + ",\"information\":\""
				+ this.information + "\",\"pointType\":" + this.getPointType()
				+ ",\"name\":\"" + this.name + "\",\"orderID\":"
				+ this.getOrderID() + ",\"postcode\":" + this.postcode
				+ ",\"range\":" + this.getRange() + ",\"street\":\""
				+ this.street + "\",\"targetPointType\":\""
				+ this.targetPointType.name() + "\",\"town\":\"" + this.town
				+ "\"}");
	}

	/* ======================================================================= */
	/* Hilfsklassen */
	/* ======================================================================= */
	/**
	 * Was will man sehen.
	 */
	public enum TargetPointType {
		/** Friedhof */
		CEMETERY,
		/** Kapelle */
		CHAPEL,
		/** Kirche */
		CHURCH,
		/** juedischer Friedhof */
		JEWISH_CEMETERY,
		/** Kloster */
		MONASTERY,
		/** Denkmal */
		MONUMENT,
		/** Standbild */
		STATUE,
		/** Wegekreuz */
		WAYSIDE_CROSS,
		/** Endpunkt */
		END_POINT;
	}
}
