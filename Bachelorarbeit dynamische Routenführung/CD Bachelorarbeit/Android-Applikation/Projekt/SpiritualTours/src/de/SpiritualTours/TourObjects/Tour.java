package de.SpiritualTours.TourObjects;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import de.SpiritualTours.TourObjects.WayPoint.PointType;
import de.SpiritualTours.Utility.Geo;
import de.SpiritualTours.R;

/**
 * Repr�sentiert eine Tour.
 */
public class Tour implements Parcelable {

	/* ====================================================================== */
	/* Konstanten */
	/* ====================================================================== */
	/** Kostante f�r Typ. */
	private static final String TOUR_TYPE = "tourType";
	/** Kostante f�r gesch�tzte L�nge in km. */
	private static final String TOUR_LENGTH = "estimatedTourLength";
	/** Kostante f�r gesch�tzte Dauer in Minuten. */
	private static final String TOUR_DURATION = "estimatedDuration";
	/** Kostante f�r Wegpunkte der Tour. */
	private static final String TOUR_WAYPOINTS = "wayPoints";
	/** Kostante f�r Wegbeschreibung der Tour. */
	private static final String TOUR_INSTRUCTIONS = "instructions";

	/* ====================================================================== */
	/* Instanzvariablen */
	/* ====================================================================== */
	/** Typ der Tour */
	private TourType tourType;
	/** gesch�tzte L�nge in km */
	private double estimatedTourLength = 0;
	/** gesch�tzte Dauer in Minuten */
	private int estimatedDuration = 0;
	/** Wegpunkte der Tour */
	private List<WayPoint> wayPoints;
	/** Wegbeschreibung. */
	private List<Instruction> instructions;

	/* ====================================================================== */
	/* Konstruktoren */
	/* ====================================================================== */
	/**
	 * Erzeugt eine Tour.
	 * 
	 * @param targetPoints
	 *            Wegpunkte der Tour
	 * @param tourType
	 *            Typ der Tour
	 * @param estimatedRouteLength
	 *            gesch�tzte L�nge in km
	 * @param estimatedDuration
	 *            gesch�tzte Dauer in Minuten
	 * @param instructions
	 *            Wegbeschreibung
	 * @param boundingBox
	 *            Begrenzungsrahmen
	 */
	public Tour(TourType tourType, double estimatedRouteLength,
			int estimatedDuration, List<WayPoint> wayPoints,
			List<Instruction> instructions) {

		this.tourType = tourType;
		this.estimatedTourLength = estimatedRouteLength;
		this.estimatedDuration = estimatedDuration;
		this.wayPoints = wayPoints;
		this.instructions = instructions;
		Collections.sort(this.wayPoints);
	}

	/**
	 * Erzeugt eine Tour.
	 * 
	 * @param jsonString
	 *            JSON-String
	 * @throws JSONException
	 *             bei fehlerhaften JSON-String
	 */
	public Tour(JSONObject jsonTour) throws JSONException {

		this.tourType = TourType.valueOf(jsonTour.getString(TOUR_TYPE));
		this.estimatedTourLength = Double.valueOf(jsonTour
				.getString(TOUR_LENGTH));
		this.estimatedDuration = Integer.valueOf(jsonTour
				.getString(TOUR_DURATION));

		/* Wegpunkte parsen */
		JSONArray jsonWaypoints = jsonTour.getJSONArray(TOUR_WAYPOINTS);
		this.wayPoints = new ArrayList<WayPoint>();
		for (int i = 0; i < jsonWaypoints.length(); i++) {

			WayPoint wp = new WayPoint(jsonWaypoints.getJSONObject(i));

			this.wayPoints.add(wp.getPointType().equals(PointType.TARGET_POINT) ? new TargetPoint(
					jsonWaypoints.getJSONObject(i)) : wp);
		}

		/* Wegbeschreibung parsen */
		JSONArray jsonInstructions = jsonTour.getJSONArray(TOUR_INSTRUCTIONS);
		this.instructions = new ArrayList<Instruction>();
		for (int i = 0; i < jsonInstructions.length(); i++) {
			this.instructions.add(new Instruction(jsonInstructions
					.getJSONObject(i)));
		}
		Collections.sort(this.wayPoints);
	}

	/**
	 * Erzeugt eine neue Tour.
	 */
	public Tour() {

		this.tourType = null;
		this.estimatedTourLength = 0;
		this.estimatedDuration = 0;
		this.wayPoints = new ArrayList<WayPoint>();
		this.instructions = new ArrayList<Instruction>();
	}

	/* ====================================================================== */
	/* Getter und Setter */
	/* ====================================================================== */
	/**
	 * Gibt den Typ der Tour.
	 * 
	 * @return Typ der Tour
	 */
	public TourType getTourType() {
		return tourType;
	}

	/**
	 * Setzt den Typ der Tour.
	 * 
	 * @param tourType
	 *            Typ der Tour
	 */
	public void setTourType(TourType tourType) {
		this.tourType = tourType;
	}

	/* ---------------------------------------------------------------------- */
	/**
	 * Gibt die gesch�tzte L�nge in km.
	 * 
	 * @return gesch�tzte L�nge in km
	 */
	public double getEstimatedTourLength() {
		return estimatedTourLength;
	}

	/**
	 * Gibt die gesch�tzte L�nge in km gerundet auf die gew�nschte
	 * Nachkommastellen-L�nge.
	 * 
	 * @param decimal
	 *            Anzahl der Nachkommastellen
	 * @return gesch�tzte L�nge in km
	 */
	public double getEstimatedTourLengthRounded(int decimal) {
		int factor = getFactor(decimal);
		return ((double) (int) (estimatedTourLength * factor + 0.5)) / factor;
	}

	/**
	 * Berechnet den Faktor, zur Berechnung der Nachkommastellen.
	 * 
	 * @param decimal
	 *            Anzahl der Nachkommastellen
	 * @return Faktor
	 */
	private int getFactor(int decimal) {
		return decimal <= 0 ? 1 : 10 * getFactor(decimal - 1);
	}

	/**
	 * Setzt die gesch�tzte L�nge in km.
	 * 
	 * @param estimatedRouteLength
	 *            gesch�tzte L�nge in km
	 */
	public void setEstimatedTourLength(double estimatedTourLength) {
		this.estimatedTourLength = estimatedTourLength;
	}

	/**
	 * Setzt die gesch�tzte L�nge in km. Dabei wird auf drei Nachkommastellen
	 * gerundet.
	 * 
	 * @param estimatedRouteLength
	 *            gesch�tzte L�nge in km
	 */
	public void setEstimatedTourLength() {
		this.estimatedTourLength = 0;
		for (int i = 0; i < this.wayPoints.size() - 1; i++) {
			this.estimatedTourLength += Geo.getDistance(this.wayPoints.get(i)
					.getGeoPoint(), this.wayPoints.get(i + 1).getGeoPoint());
		}
		this.estimatedTourLength = getEstimatedTourLengthRounded(3);
	}

	/* ---------------------------------------------------------------------- */
	/**
	 * Gibt die gesch�tzte Dauer in Minuten.
	 * 
	 * @return gesch�tzte Dauer in Minuten
	 */
	public int getEstimatedDuration() {
		return estimatedDuration;
	}

	/**
	 * Setzt die gesch�tzte Dauer in Minuten.
	 * 
	 * @param estimatedDuration
	 *            Dauer
	 */
	public void setEstimatedDuration(int estimatedDuration) {
		this.estimatedDuration = estimatedDuration;
	}

	/**
	 * Setzt die gesch�tzte Dauer in Minuten.
	 * 
	 * @param speed
	 *            Geschwindigkeit in km/h
	 */
	public void setEstimatedDuration(float speed) {
		this.estimatedDuration = (int) (this.estimatedTourLength / speed) * 60;
	}

	/* ---------------------------------------------------------------------- */
	/**
	 * Gibt die Wegpunkte der Tour.
	 * 
	 * @return Wegpunkte der Tour
	 */
	public List<WayPoint> getWayPoints() {
		return wayPoints;
	}

	/**
	 * Gibt die Wegpunkte der Tour.
	 * 
	 * @return Wegpunkte der Tour
	 */
	public List<TargetPoint> getTargetPoints() {
		List<TargetPoint> targetPoints = new ArrayList<TargetPoint>();
		for (WayPoint wayPoint : this.wayPoints) {
			if (wayPoint.getPointType().equals(PointType.TARGET_POINT)) {
				targetPoints.add((TargetPoint) wayPoint);
			}
		}
		return targetPoints;
	}

	/**
	 * Gibt alle noch verbleibenden Wegpunkte. Existiert der Wegpunkt nicht,
	 * wird null zur�ckgegeben.
	 * 
	 * @param currentWayPoint
	 *            aktueller Wegpunkt
	 * @return alle noch verbleibenden Wegpunkte
	 */
	public List<WayPoint> getRemainingWayPoints(WayPoint currentWayPoint) {
		int index = this.wayPoints.indexOf(currentWayPoint);

		return index != -1 || index < this.wayPoints.size() ? this.wayPoints
				.subList(index, this.wayPoints.size() - 1) : null;
	}

	/**
	 * Gibt den ersten Zielpunkt der Tour. Existiert kein Zielpunkt in der Tour,
	 * wird null zur�ckgegeben.
	 * 
	 * @return erster Zielpunkt der Tour
	 */
	public TargetPoint getNextTargetPoint(WayPoint currentWayPoint) {

		int index = this.wayPoints.indexOf(currentWayPoint);

		if (index != -1) {
			for (int i = 0; i < this.wayPoints.size(); i++) {
				if (this.wayPoints.get(i).getPointType().equals(PointType.TARGET_POINT)) {
					return (TargetPoint) this.wayPoints.get(i);
				}
			}
		}
		return null;
	}

	/**
	 * Gibt den n�chsten Zielpunkt. Existiert kein n�chster Zielpunkt, wird null
	 * zur�ckgegeben.
	 * 
	 * @param currentTargetPoint
	 *            aktueller Zielpunkt
	 * @return n�chster Zielpunkt
	 */
	public TargetPoint getNextTargetPoint(TargetPoint currentTargetPoint) {

		int index = this.wayPoints.indexOf(currentTargetPoint);

		/* wenn der Zielpunkt nicht vorhanden ist => abbrechen */
		if (index == -1) {
			return null;
		}

		for (int i = index + 1; i < this.wayPoints.size(); i++) {
			if (this.wayPoints.get(i).getPointType().equals(PointType.TARGET_POINT)) {
				return (TargetPoint) this.wayPoints.get(i);
			}
		}

		return null;
	}
	
	/**
	 * Gibt die Anzahl und die aktuelle Nummer der Zielpunkte (zB "(3 von 4)").
	 * 
	 * @param context Context
	 * @param currentTargetPoint
	 *            aktueller Zielpunkt
	 * @return n�chster Zielpunkt
	 */
	public String getTargetPointNumberOf(Context context, TargetPoint currentTargetPoint) {

		int targetPointsNumber = 0;
		int currentTargetPointNumber = 0;

		List<TargetPoint> targetPoints = getTargetPoints();
		targetPointsNumber = targetPoints.size();
		
		for (TargetPoint targetPoint : targetPoints) {
			currentTargetPointNumber += 1;
			if(currentTargetPoint.equals(targetPoint)) {
				break;
			}
		}

		return "(" + currentTargetPointNumber + context.getString(R.string.n_targetpoint_of) + " " + targetPointsNumber + ")";
	}

	/**
	 * Gibt den letzten Wegpunkt der Tour.
	 * 
	 * @return letzter Wegpunkt
	 */
	public WayPoint getLastWayPoint() {
		return this.wayPoints.get(this.wayPoints.size() - 1);
	}

	/**
	 * Setzt die Wegpunkte der Tour.
	 * 
	 * @param wayPoints
	 *            Wegpunkte der Tour
	 */
	public void setWayPoints(List<WayPoint> wayPoints) {
		this.wayPoints = wayPoints;
	}

	/**
	 * F�gt einen Wegpunkt der Tour hinzu.
	 * 
	 * @param wayPoint
	 *            Wegpunkt
	 */
	public void addWayPoint(WayPoint wayPoint) {
		this.wayPoints.add(wayPoint);
	}

	/* ---------------------------------------------------------------------- */
	/**
	 * Gibt die Wegbeschreibung der Tour.
	 * 
	 * @return Wegbeschreibung der Tour
	 */
	public List<Instruction> getInstructions() {
		return instructions;
	}

	/**
	 * Setzt die Wegbeschreibung der Tour.
	 * 
	 * @param instructions
	 *            Wegbeschreibung der Tour
	 */
	public void setInstructions(List<Instruction> instructions) {
		this.instructions = instructions;
	}

	/**
	 * Setzt die Wegbeschreibung der Tour.
	 * 
	 * @param instructions
	 *            Wegbeschreibung der Tour
	 */
	public void addInstructions(String title, List<Instruction> instructions) {
		this.instructions.add(new Instruction(""));
		this.instructions.add(new Instruction(""));
		this.instructions.add(new Instruction(title));
		this.instructions.add(new Instruction(""));
		this.instructions.addAll(instructions);
	}
	
	/**
	 * F�gt eine Anweisungen hinzu.
	 * 
	 * @param instruction
	 *            Anweisung
	 */
	public void addInstruction(Instruction instruction) {
		this.instructions.add(instruction);
	}

	/* ======================================================================= */
	/* Parcelable-Methoden */
	/* ======================================================================= */
	public static final Parcelable.Creator<Tour> CREATOR = new Parcelable.Creator<Tour>() {

		public Tour createFromParcel(Parcel in) {
			return new Tour(in);
		}

		public Tour[] newArray(int size) {
			return new Tour[size];
		}
	};

	private Tour(final Parcel in) {
		readFromParcel(in);
	}

	public void writeToParcel(Parcel out, int flags) {
		out.writeString(this.tourType == null ? null : this.tourType.name());
		out.writeDouble(this.estimatedTourLength);
		out.writeInt(this.estimatedDuration);
		out.writeTypedList(this.wayPoints);
		out.writeTypedList(this.instructions);
	}

	public void readFromParcel(final Parcel in) {
		this.tourType = TourType.valueOf(in.readString());
		this.estimatedTourLength = in.readDouble();
		this.estimatedDuration = in.readInt();
		this.wayPoints = new ArrayList<WayPoint>();
		in.readTypedList(this.wayPoints, WayPoint.CREATOR);
		this.instructions = new ArrayList<Instruction>();
		in.readTypedList(this.instructions, Instruction.CREATOR);
	}

	public int describeContents() {
		return 0;
	}

	/* ====================================================================== */
	/* toString */
	/* ====================================================================== */
	/**
	 * Gibt die Tour als String.
	 * 
	 * @return Tour als String
	 */
	@Override
	public String toString() {
		return "Tour-Typ: " + this.tourType + ", gesch�tzte L�nge in km: "
				+ this.estimatedTourLength + ", gesch�tzte Dauer in Minuten: "
				+ this.estimatedDuration + ", Wegpunkte: " + this.wayPoints
				+ ", Wegbeschreibung: " + this.instructions;
	}

	/**
	 * Gibt das Objekt als JSON-String.
	 * 
	 * @return JSON-String
	 * @throws JSONException
	 */
	public JSONObject toJSON() throws JSONException {
		return new JSONObject("{\"estimatedDuration\":"
				+ this.estimatedDuration + ",\"estimatedTourLength\":"
				+ this.estimatedTourLength + ",\"instructions\":"
				+ getInstructionsJson() + ",\"tourType\":\"" + this.tourType
				+ "\",\"wayPoints\":" + getWayPointsJson() + "}");
	}

	/**
	 * Parst die gew�nschten Wegpunkte nach JSON (Hilfsmethode f�r toJSON()).
	 * 
	 * @return gew�nschte Wegpunkte als JSON-String
	 * @throws JSONException
	 */
	private String getWayPointsJson() throws JSONException {
		String wayPointsString = "[";
		if (this.wayPoints.size() >= 1) {
			wayPointsString += "\"" + this.wayPoints.get(0).toJSON().toString()
					+ "\"";
		}
		for (int i = 1; i < this.wayPoints.size(); i++) {
			wayPointsString += ",\""
					+ this.wayPoints.get(i).toJSON().toString() + "\"";
		}
		return wayPointsString + "]";
	}

	/**
	 * Parst die gew�nschten Anweisungen nach JSON (Hilfsmethode f�r toJSON()).
	 * 
	 * @return gew�nschte Anweisungen als JSON-String
	 * @throws JSONException
	 */
	private String getInstructionsJson() throws JSONException {
		String instructionsString = "[";
		if (this.instructions.size() >= 1) {
			instructionsString += "\""
					+ this.instructions.get(0).toJSON().toString() + "\"";
		}
		for (int i = 1; i < this.instructions.size(); i++) {
			instructionsString += ",\""
					+ this.instructions.get(i).toJSON().toString() + "\"";
		}
		return instructionsString + "]";
	}

	/* ====================================================================== */
	/* Hilfsklassen */
	/* ====================================================================== */
	/**
	 * Tour-Typen.
	 */
	public enum TourType {

		ROUTE, FLATLY, PILGRIMAGE;
	}
}
