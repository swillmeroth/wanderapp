package de.SpiritualTours.Exceptions;

/**
 * Fehler: keine Frage vorhanden.
 */
public class NoQuestionFoundException extends Exception {

	/** Serial-Version-UID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Erzeugt eine NoQuestionFoundException.
	 * 
	 * @param s
	 *            Fehlermeldung
	 */
	public NoQuestionFoundException() {
	}
}