package de.SpiritualTours.TourObjects;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Definiert eine Frage mit zugeh�rigen Antworten. Zudem steht auch die Zuge-
 * h�rigkeit des Punkts zum Zielpunkt fest.
 */
public class Question {

	/* ====================================================================== */
	/* Konstanten */
	/* ====================================================================== */
	/** Kostante f�r Id. */
	private static final String QUESTION_ID = "id";
	/** Kostante f�r Frage. */
	private static final String QUESTION_QUESTION = "question";
	/** Kostante f�r richtige Antwort. */
	private static final String QUESTION_CORRECT_ANSWER = "correctAnswer";
	/** Kostante f�r falsche Antworten. */
	private static final String QUESTION_WRONG_ANSWERS = "wrongAnswers";
	/** Kostante f�r Schwierigkeitsgrad. */
	private static final String QUESTION_LEVELOFDIFFICULTY = "levelOfDifficulty";
	
	/* ====================================================================== */
	/* Instanzvariablen */
	/* ====================================================================== */	
    /** Fragestellung. */
    private long id;
    /** Fragestellung. */
    private String question;
    /** richtige Antwort der Frage. */
    private String correctAnswer;
    /** Falsche Antworten auf die Frage. */
    private String[] wrongAnswers;
    /** Schwierigkeitsgrad der Frage. */
    private LevelOfDifficulty levelOfDifficulty;

    /* ====================================================================== */
	/* Konstruktoren */
	/* ====================================================================== */    
    /**
     * Erzeugt eine Frage, mit Antworten.
     * @param question Frage
     * @param correctAnswer richtige Antwort
     * @param wrongAnswers falsche Antworten
     * @param levelOfDifficulty Schwierigkeitsgrad der Frage
     * @param targetPoint  Zielpunkt
     */
    public Question(long id, String question, String correctAnswer, String[] wrongAnswers,
            LevelOfDifficulty levelOfDifficulty) {
    	this.id = id;
        this.question = question;
        this.correctAnswer = correctAnswer;
        this.wrongAnswers = wrongAnswers;
        this.levelOfDifficulty = levelOfDifficulty;
    }
    
    /**
     * Erzeugt eine Frage, mit Antworten.
     * @param jsonString JSON-String.
     * @throws JSONException bei fehlerhaften JSON-String
     */
    public Question(JSONObject jsonQuestion) throws JSONException {
		
    	this.id = jsonQuestion.getLong(QUESTION_ID);
		this.question = jsonQuestion.getString(QUESTION_QUESTION);
		this.correctAnswer = jsonQuestion.getString(QUESTION_CORRECT_ANSWER); 
		
		/* falsche Antworten parsen */
		JSONArray jsonWrongAnswers = jsonQuestion
				.getJSONArray(QUESTION_WRONG_ANSWERS);
		String[] wrongAnswers = new String[jsonWrongAnswers.length()];		
		for (int i = 0; i < wrongAnswers.length; i++) {
			wrongAnswers[i] = jsonWrongAnswers.getString(i);
		}
		
		this.wrongAnswers = wrongAnswers;
		this.levelOfDifficulty = LevelOfDifficulty.valueOf(jsonQuestion
						.getString(QUESTION_LEVELOFDIFFICULTY));
    }
    
    /**
     * Leerer Konstruktor.
     */
    public Question() { }    

    /* ====================================================================== */
	/* Setter & Getter */
	/* ====================================================================== */
    /**
     * Gibt die Id der Frage.
     * @return Id der Frage
     */
    public long getId() {
        return id;
    }

    /**
     * Setzt die Id der Frage.
     * @param Id der Frage
     */
    public void setId(long id) {
        this.id = id;
    }
    
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    /**
     * Gibt die Frage.
     * @return Frage
     */
    public String getQuestion() {
        return question;
    }

    /**
     * Setzt die Frage.
     * @param Frage
     */
    public void setQuestion(String qestion) {
        this.question = qestion;
    }

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    /**
     * Gibt die richtige Antwort.
     * @return richtige Antwort
     */
    public String getCorrectAnswer() {
        return correctAnswer;
    }

    /**
     * Gibt die richtige Antwort.
     * @param correctAnswer richtige Antwort
     */
    public void setCorrectAnswer(String correctAnswer) {
        this.correctAnswer = correctAnswer;
    }

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    /**
     * Gibt die falschen Antworten zur Frage.
     * @return falsche Antworten zur Frage
     */
    public String[] getWrongAnswers() {
        return wrongAnswers;
    }

    /**
     * Setzt die falschen Antworten zur Frage.
     * @param falsche Antworten zur Frage
     */
    public void setWrongAnswers(String[] wrongAnswers) {
        this.wrongAnswers = wrongAnswers;
    }

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    /**
     * Gibt den Schwierigkeitsgrad der Frage.
     * @return Schwierigkeitsgrad
     */
    public LevelOfDifficulty getLevelOfDifficulty() {
        return levelOfDifficulty;
    }

    /**
     * Setzt den Schwierigkeitsgrad der Frage.
     * @param Schwierigkeitsgrad
     */
    public void setLevelOfDifficulty(LevelOfDifficulty levelOfDifficulty) {
        this.levelOfDifficulty = levelOfDifficulty;
    }

    /* ====================================================================== */
	/* Equals, HashCode, toString... */
	/* ====================================================================== */
    /**
     * Gibt die Frage.
     * @return Frage
     */
    @Override
    public String toString() {
        return this.question;
    }

    /**
     * Objekt als String, mit allen interessanten Informationen, au�er dem 
     * Zielpunkt. (Frage, Antworten)
     * @return Objekt als String
     */
    public String toStringCompletely() {
        return "Frage: " + question + ",\n"
                + "richtige Antwort: " + correctAnswer + ",\n"
                + "falsche Antworten: " + getWrongAnswersToString() + ",\n"
                + "Schwierigkeitsgrad: " + this.levelOfDifficulty;
    }
    
    /**
     * Hilfsmethode, die die falschen Antworten, welche in einem Array stehen, 
     * zu einen String fasst.
     * @return falsche Antworten als ein String
     */
    private String getWrongAnswersToString() {

        String answers = "";

        if (this.wrongAnswers.length >= 1) {
            answers = this.wrongAnswers[0];
        } else {
            return answers;
        }

        for (int i = 1; i < this.wrongAnswers.length; i++) {
            answers = answers + " / " + this.wrongAnswers[i];
        }

        return answers;
    }
   
    /* ====================================================================== */
	/* Hilfsklassen */
	/* ====================================================================== */    
    /**
     * Schwierigkeitsgrad.
     */
    public enum LevelOfDifficulty {
        EASY, MIDDLE, DIFFICULT;
    }
}
