package de.SpiritualTours.Exceptions;

/**
 * Repräsentiert einen Verbindungsfehler.
 */
public class ConnectionException extends Exception {

	/** Serial-Version-UID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Erzeugt eine Verbindungsfehler-Meldung.
	 * 
	 * @param s
	 *            Fehlermeldung
	 */
	public ConnectionException() {
	}
}
