package de.SpiritualTours.View;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Defeniert einen grafischen Kompass, der sich in die gewŁnschte Richtung
 * dreht. Der Code konnte aus einem Beispiel eines Forums entnommen werden.
 * Link: http://marakana.com/forums/android/examples/98.html
 */
public class Compass extends ImageView {

	/* ======================================================================= */
	/* Instanzvariablen */
	/* ======================================================================= */
	/** Richtung des Kompasses. */
	private float direction = 0;

	/* ======================================================================= */
	/* Konstruktoren */
	/* ======================================================================= */
	/**
	 * Erzeugt einen neuen Kompass.
	 * 
	 * @param context
	 *            Context
	 */
	public Compass(Context context) {
		super(context);
	}

	/**
	 * Erzeugt einen neuen Kompass.
	 * 
	 * @param context
	 *            Context
	 * @param attrs
	 *            Attribute
	 */
	public Compass(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	/**
	 * Erzeugt einen neuen Kompass.
	 * 
	 * @param context
	 *            Context
	 * @param attrs
	 *            Attribute
	 * @param defStyle
	 *            Style
	 */
	public Compass(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	/* ======================================================================= */
	/* onDraw */
	/* ======================================================================= */
	/**
	 * Zeichnet den Kompass mit der richtigen Drehung.
	 */
	@Override
	public void onDraw(Canvas canvas) {
		int height = this.getHeight();
		int width = this.getWidth();

		canvas.rotate(this.direction, width / 2, height / 2);
		super.onDraw(canvas);
	}

	/* ======================================================================= */
	/* Setter */
	/* ======================================================================= */
	/**
	 * Setzt die Richtung des Kompasses.
	 * 
	 * @param azimuth
	 *            Himmelsrichtung
	 */
	public void setDirection(float azimuth) {
		this.direction = -azimuth;
		this.invalidate();
	}

	/**
	 * Setzt die Richtung des Kompasses.
	 * 
	 * @param azimuth
	 *            Himmelsrichtung
	 * @param bearing
	 *            Abweichung zu Norden, also die Richtung, in die letzendlich
	 *            der Kompass zeigen soll
	 */
	public void setDirection(float azimuth, float bearing) {
		this.direction = -azimuth + bearing;
		this.invalidate();
	}
}
