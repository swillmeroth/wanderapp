package de.SpiritualTours.TourObjects;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;

public class GeoPoint implements Parcelable {
	
	/* ====================================================================== */
	/* Konstanten */
	/* ====================================================================== */
	/** Kostante f�r Breitengrad. */
	private static final String GEOPOINT_LATITUDE = "latitude";
	/** Kostante f�r L�ngengrad. */
	private static final String GEOPOINT_LONGITUDE = "longitude";
	/** Kostante f�r H�he. */
	private static final String GEOPOINT_ALTITUDE = "altitude";
	
	/* ====================================================================== */
	/* Instanzvariablen */
	/* ====================================================================== */
	/** Breitengrad der Position. */
	private double latitude;
	/** L�ngengrad der Position. */
	private double longitude;
	/** H�he der Position. */
	private double altitude;
	
	/* ====================================================================== */
	/* Konstruktoren */
	/* ====================================================================== */
	/**
	 * Erzeugt eine Geo-Position.
	 * @param latitude Breitengrad der Position
	 * @param longitude L�ngengrad der Position
	 * @param altitude H�he der Position
	 */
	public GeoPoint(double latitude, double longitude, double altitude) {
		super();
		this.latitude = latitude;
		this.longitude = longitude;
		this.altitude = altitude;
	}
	
	/**
	 * Erzeugt eine neue Position.
	 * 
	 * @param jsonObject
	 *            JSON-Objekt
	 * @throws JSONException
	 *             bei fehlerhaften JSON-String
	 * @throws NumberFormatException
	 *             bei fehlerhaften Zahlenwerten
	 */
	public GeoPoint(JSONObject jsonGeoPoint) throws NumberFormatException,
			JSONException {

		this.latitude = Double.valueOf(jsonGeoPoint
				.getString(GEOPOINT_LATITUDE));
		this.longitude = Double.valueOf(jsonGeoPoint
				.getString(GEOPOINT_LONGITUDE));
		this.altitude = Double.valueOf(jsonGeoPoint
				.getString(GEOPOINT_ALTITUDE));
	}
	
	/**
	 * Erzeugt eine Geo-Position.
	 */
	public GeoPoint() { }
	
	/* ====================================================================== */
	/* Getter und Setter */
	/* ====================================================================== */
	/**
	 * Gibt den Breitengrad der Position.
	 * 
	 * @return Breitengrad der Position
	 */
	public double getLatitude() {
		return latitude;
	}

	/**
	 * Setzt den Breitengrad der Position
	 * 
	 * @param latitude
	 *            Breitengrad der Position
	 */
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	/* ---------------------------------------------------------------------- */
	
	/**
	 * Gibt den L�ngengrad der Position.
	 * 
	 * @return L�ngengrad der Position
	 */
	public double getLongitude() {
		return longitude;
	}

	/**
	 * Setzt den L�ngengrad der Position.
	 * 
	 * @param longitude
	 */
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	/* ---------------------------------------------------------------------- */
	
	/**
	 * Gibt die H�he der Position.
	 * 
	 * @return H�he der Position
	 */
	public double getAltitude() {
		return altitude;
	}

	/**
	 * Setzt die H�he der Position.
	 * 
	 * @param altitude
	 *            H�he der Position
	 */
	public void setAltitude(double altitude) {
		this.altitude = altitude;
	}
	
	/* ======================================================================= */
	/* Parcelable-Methoden */
	/* ======================================================================= */
	public static final Parcelable.Creator<GeoPoint> CREATOR = new Parcelable.Creator<GeoPoint>() {

		public GeoPoint createFromParcel(Parcel in) {
			return new GeoPoint(in);
		}

		public GeoPoint[] newArray(int size) {
			return new GeoPoint[size];
		}
	};

	private GeoPoint(final Parcel in) {
		readFromParcel(in);
	}

	public void writeToParcel(Parcel out, int flags) {
		out.writeDouble(this.latitude);
		out.writeDouble(this.longitude);
		out.writeDouble(this.altitude);
	}

	public void readFromParcel(final Parcel in) {
		this.latitude = in.readDouble();
		this.longitude = in.readDouble();
		this.altitude = in.readDouble();
	}

	public int describeContents() {
		return 0;
	}
	
	/* ====================================================================== */
	/* toString */
	/* ====================================================================== */
	/**
	 * Gibt die Position als String.
	 * 
	 * @return Position als String
	 */
	@Override
	public String toString() {
		return "Breitengrad: " + latitude + ", L�ngengrad: " + longitude
				+ ", H�he: " + altitude;
	}
	
	/**
	 * Gibt das Objekt als JSON-String.
	 * 
	 * @return JSON-String
	 * @throws JSONException
	 */
	public JSONObject toJSON() throws JSONException {
		return new JSONObject("{\"" + GEOPOINT_LATITUDE + "\":" + this.latitude
				+ ",\"" + GEOPOINT_LONGITUDE + "\":" + this.longitude + ",\""
				+ GEOPOINT_ALTITUDE + "\":" + this.altitude + "}");
	}
	
	/**
     * Vergleicht ein GeoPoint-Objekt mit einem Objekt. Wenn L�ngengrad und 
     * Breitengrad gleich sind, sind die Objekte gleich.
     * @param obj Objekt
     * @return true, wenn gleich
     */
    @Override
    public boolean equals(Object obj) {
        
        return obj instanceof GeoPoint 
                && this.latitude == ((GeoPoint) obj).latitude 
                && this.longitude == ((GeoPoint) obj).longitude;
    }

    /**
     * Gibt den HashCode.
     * @return hashCode
     */
    @Override
    public int hashCode() {
        return (int) (latitude + longitude);
    }
}
