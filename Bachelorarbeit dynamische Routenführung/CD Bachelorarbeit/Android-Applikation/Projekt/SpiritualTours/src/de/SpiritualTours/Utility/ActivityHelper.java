package de.SpiritualTours.Utility;

import android.content.Context;
import android.media.MediaPlayer;
import android.os.Vibrator;
import android.widget.Toast;
import de.SpiritualTours.R;
import de.SpiritualTours.TourObjects.TargetPoint.TargetPointType;
import de.SpiritualTours.TourObjects.TourSettings.HowGood;
import de.SpiritualTours.TourObjects.TourSettings.Locomotion;

/**
 * Bietet allgemeine Hilfs-Methoden f�r Activities an.
 */
public final class ActivityHelper {

	/**
	 * Zeigt eine Toas-Meldung auf der Activity.
	 * 
	 * @param context
	 *            Context
	 * @param message
	 *            Meldung
	 */
	public static void showMessageToast(Context context, String message) {

		Toast.makeText(context.getApplicationContext(), message,
				Toast.LENGTH_LONG).show();
	}

	/**
	 * Benachrichtigt den Benutzer mittels Vibration und Ton, sofern per
	 * Parameter�bergabe erw�nscht ist.
	 * 
	 * @param context
	 *            Context
	 * @param vibrate
	 *            Mobil-Ger�t soll vibrieren
	 * @param playSound
	 *            Mobil-Ger�t soll klingeln
	 */
	public static void notifyTargetPoint(Context context, boolean vibrate,
			boolean playSound) {

		/* Sound abspielen */
		if (playSound) {
			MediaPlayer player = MediaPlayer
					.create(context, R.raw.notification);
			player.start();
		}

		/* vibrieren */
		if (vibrate) {
			((Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE))
					.vibrate(1000);
		}
	}

	/**
	 * Benachrichtigt den Benutzer mittels Vibration und Ton, sofern per
	 * Parameter�bergabe erw�nscht ist.
	 * 
	 * @param context
	 *            Context
	 * @param vibrate
	 *            Mobil-Ger�t soll vibrieren
	 * @param playSound
	 *            Mobil-Ger�t soll klingeln
	 */
	public static void notifyWayPoint(Context context, boolean vibrate,
			boolean playSound) {

		/* Sound abspielen */
		if (playSound) {
			MediaPlayer player = MediaPlayer
					.create(context, R.raw.notification);
			player.start();
		}

		/* vibrieren */
		if (vibrate) {
			((Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE))
					.vibrate(500);
		}
	}

	/**
	 * Ermittelt die Zeit als String.
	 * 
	 * @param context
	 *            Context
	 * @param time
	 *            Zeit als Int
	 * @param inToLines
	 *            true, wenn 2 zeilig
	 * @return Zeit als String
	 */
	public static String getTime(Context context, int time, boolean inToLines) {
		return time < 60 ? time + " "
				+ context.getString(R.string.time_minutes)
				: time % 60 == 0 ? time == 60 ? "1 "
						+ context.getString(R.string.time_hour) : (time / 60)
						+ " " + context.getString(R.string.time_hours)
						: time / 60 < 2 ? (time / 60) + " "
								+ context.getString(R.string.time_hour) + " "
								+ context.getString(R.string.time_and)
								+ (inToLines ? "\n" : " ") + (time % 60) + " "
								+ context.getString(R.string.time_minutes)
								: (time / 60)
										+ " "
										+ context
												.getString(R.string.time_hours)
										+ " "
										+ context.getString(R.string.time_and)
										+ (inToLines ? "\n" : " ")
										+ (time % 60)
										+ " "
										+ context
												.getString(R.string.time_minutes);
	}

	/**
	 * Gibt die Distanz in km bzw in m an. Km wenn die Distanz �ber 1000m
	 * betr�gt.
	 * 
	 * @param distance
	 *            Distanz
	 * @return Distanz als String
	 */
	public static String getDistance(double distance) {
		return (distance >= 1 ? ((int) (distance * 1000)) / 1000.0 + " km"
				: (int) (distance * 1000) + " m").replace(".", ",");
	}

	/**
	 * Gibt die Bezeichnung des Zielpunkt-Typen.
	 * 
	 * @param context
	 *            Context
	 * @param targetPointType
	 *            Zielpunkt-Typ
	 * 
	 * @return Zielpunkt-Typ
	 */
	public static String getTargetPointTypeName(Context context,
			TargetPointType targetPointType) {
		switch (targetPointType) {
		case CEMETERY:
			return context.getString(R.string.tp_cemeteries);
		case CHURCH:
			return context.getString(R.string.tp_churches);
		case JEWISH_CEMETERY:
			return context.getString(R.string.tp_jewish_cemeteries);
		case MONASTERY:
			return context.getString(R.string.tp_monasteries);
		case MONUMENT:
			return context.getString(R.string.tp_monuments);
		case WAYSIDE_CROSS:
			return context.getString(R.string.tp_wayside_crosses);
		default:
			return "";
		}
	}

	/**
	 * Gibt die Art der Fortbewegung.
	 * 
	 * @param context
	 *            Context
	 * @param locomotion
	 *            Art der Fortbewegung
	 * 
	 * @return Art der Fortbewegung
	 */
	public static String getLocomotion(Context context, Locomotion locomotion) {
		switch (locomotion) {
		case HOOF_IT:
			return context.getString(R.string.locomotion_hoof_it);
		case BIKE:
			return context.getString(R.string.locomotion_bike);
		case CAR:
			return context.getString(R.string.locomotion_car);
		default:
			return "";
		}
	}

	/**
	 * Gibt an, Wie gut ist man unterwegs?
	 * 
	 * @param context
	 *            Context
	 * @param howGood
	 *            Wie gut ist man unterwegs?
	 * 
	 * @return Wie gut ist man unterwegs?
	 */
	public static String getHowGood(Context context, HowGood howGood) {
		switch (howGood) {
		case SLOW:
			return context.getString(R.string.how_good_slow);
		case NORMAL:
			return context.getString(R.string.how_good_normal);
		case FAST:
			return context.getString(R.string.how_good_fast);
		default:
			return "";
		}
	}
}
