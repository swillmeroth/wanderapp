package de.SpiritualTours.TourObjects;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Definiert einen Punkt der Tour.
 */
public class WayPoint implements Comparable<WayPoint>, Parcelable {

	/* ====================================================================== */
	/* Konstanten */
	/* ====================================================================== */
	/** Kostante f�r Rang der Reihenfolge. */
	private static final String WAYPOINT_ORDER_ID = "orderID";
	/** Kostante f�r den Radius (Reichweite) um den Wegpunkt. */
	private static final String WAYPOINT_RANGE = "range";
	/** Kostante f�r die Geo-Koordinate. */
	private static final String WAYPOINT_GEOPOINT = "geoPoint";
	/** Kostante, ob es sich um einen Zielpunkt handelt. */
	private static final String POINT_TYPE = "pointType";
	/** Standard-Reichweite */
	private static final int STANDARD_RANGE = 10;

	/* ====================================================================== */
	/* Instanzvariablen */
	/* ====================================================================== */
	/** Rang in der Reihenfolge der Wegpunkte. */
	private int orderID;
	/** Radius (Reichweite) um den Wegpunkt in Meter. (Standard sind 5 m) */
	private int range;
	/** Geografische Koordinate. */
	private GeoPoint geoPoint;
	/** Gibt den Typen des Wegpunktes an. */
    private PointType pointType;

	/* ====================================================================== */
	/* Konstruktoren */
	/* ====================================================================== */
	/**
	 * Erzeugt einen Weg-Punkt.
	 * 
	 * @param orderID
	 *            Radius (Reichweite) um den Wegpunkt in Meter
	 * @param geoPoint
	 *            geografische Koordinate
	 * @param isTargetPoint
	 *            handelt es sich um ein Zielpunkt
	 */
	public WayPoint(int orderID, GeoPoint geoPoint, PointType pointType) {
		this.orderID = orderID;
		this.range = STANDARD_RANGE;
		this.geoPoint = geoPoint;
		this.pointType = pointType;
	}

	/**
	 * Erzeugt eine Anweisung zur Wegbeschreibung.
	 * 
	 * @param jsonString
	 *            JSON-String
	 * @throws JSONException
	 *             bei fehlerhaften JSON-String
	 */
	public WayPoint(JSONObject jsonWayPoint) throws JSONException {

		this.orderID = Integer.valueOf(jsonWayPoint
				.getString(WAYPOINT_ORDER_ID));
		this.range = Integer.valueOf(jsonWayPoint.getString(WAYPOINT_RANGE));

		this.geoPoint = new GeoPoint(
				jsonWayPoint.getJSONObject(WAYPOINT_GEOPOINT));

		this.pointType = PointType.valueOf(jsonWayPoint
				.getString(POINT_TYPE));
	}

	/**
	 * Erzeugt einen Weg-Punkt.
	 * 
	 * @param geoPoint
	 *            geografische Koordinate
	 */
	public WayPoint(GeoPoint geoPoint) {
		this.orderID = -1;
		this.range = STANDARD_RANGE;
		this.geoPoint = geoPoint;
		this.pointType = PointType.WAY_POINT;
	}
	
	/**
	 * Erzeugt einen Weg-Punkt.
	 */
	public WayPoint() {
	}

	/* ====================================================================== */
	/* Getter & Setter */
	/* ====================================================================== */
	/**
	 * Gibt den Rang in der Reihenfolge der Wegpunkte.
	 * 
	 * @return Rang in der Reihenfolge
	 */
	public int getOrderID() {
		return orderID;
	}

	/**
	 * Setzt den Rang in der Reihenfolge der Wegpunkte.
	 * 
	 * @param orderID
	 *            Rang in der Reihenfolge
	 */
	public void setOrderID(int orderID) {
		this.orderID = orderID;
	}

	/* ---------------------------------------------------------------------- */
	/**
	 * Gibt den Radius (Reichweite) um den Wegpunkt in Meter. (Standard sind 5
	 * m)
	 * 
	 * @return Reichweite
	 */
	public int getRange() {
		return range;
	}

	/**
	 * Setzt den Radius (Reichweite) um den Wegpunkt in Meter. (Standard sind 5
	 * m)
	 * 
	 * @param range
	 *            Reichweite
	 */
	public void setRange(int range) {
		this.range = range;
	}

	/* ---------------------------------------------------------------------- */
	/**
	 * Gibt die geografische Koordinate.
	 * 
	 * @return geografische Koordinate.
	 */
	public GeoPoint getGeoPoint() {
		return geoPoint;
	}

	/**
	 * Setzt die geografische Koordinate.
	 * 
	 * @param geoPoint
	 *            geografische Koordinate.
	 */
	public void setGeoPoint(GeoPoint geoPoint) {
		this.geoPoint = geoPoint;
	}

	/* ---------------------------------------------------------------------- */
    /**
     * Gibt den Typen des Punktes an.
     * @return Typen des Punktes
     */
    public PointType getPointType() {
        return pointType;
    }

    /**
     * Setzt den Typen des Punktes.
     * @param pointType Typen des Punktes
     */
    public void setPointType(PointType pointType) {
        this.pointType = pointType;
    }

	/* ======================================================================= */
	/* Parcelable-Methoden */
	/* ======================================================================= */
	public static final Parcelable.Creator<WayPoint> CREATOR = new Parcelable.Creator<WayPoint>() {

		public WayPoint createFromParcel(Parcel in) {
			return new WayPoint(in);
		}

		public WayPoint[] newArray(int size) {
			return new WayPoint[size];
		}
	};

	private WayPoint(final Parcel in) {
		readFromParcel(in);
	}

	public void writeToParcel(Parcel out, int flags) {

		out.writeInt(this.orderID);
		out.writeInt(this.range);
		out.writeDouble(this.geoPoint.getLatitude());
		out.writeDouble(this.geoPoint.getLongitude());
		out.writeDouble(this.geoPoint.getLatitude());
		out.writeString(this.pointType.name());
	}

	public void readFromParcel(final Parcel in) {
		this.orderID = in.readInt();
		this.range = in.readInt();
		this.geoPoint = new GeoPoint();
		this.geoPoint.setLatitude(in.readDouble());
		this.geoPoint.setLongitude(in.readDouble());
		this.geoPoint.setAltitude(in.readDouble());
		this.pointType = PointType.valueOf(in.readString());
	}

	public int describeContents() {
		return 0;
	}
	
	/* ====================================================================== */
	/* toString, equals, ... */
	/* ====================================================================== */
	/**
	 * Gibt den Wegpunkt als String an.
	 * 
	 * @return Wegpunkt als String
	 */
	@Override
	public String toString() {
		return "WayPoint{" + "orderID=" + orderID + ", range=" + range
				+ ", geoPoint=" + geoPoint + ", pointType=" + pointType
				+ '}';
	}

	/**
	 * Vergleicht ein Wegpunkt-Objekt mit einem Objekt. Wenn die Koordinaten
	 * gleich sind, sind die Objekte gleich.
	 * 
	 * @param obj
	 *            Objekt
	 * @return true, wenn gleich
	 */
	@Override
    public boolean equals(Object obj) {
        return obj instanceof WayPoint 
                && this.geoPoint.equals(((WayPoint) obj).geoPoint);
    }

	/**
	 * Gibt den HashCode.
	 * 
	 * @return hashCode
	 */
	@Override
    public int hashCode() {
        return this.geoPoint.hashCode();
    }

	/**
	 * Vergleicht den Rang der Reihenfolge des Wegpunktes.
	 * 
	 * @param wayPoint
	 *            Wegpunkt mit dem verglichen werden soll
	 * @return -1 von Rang vor �bergebenen Namen liegt, 0 wenn gleich, 1 wenn
	 *         �bergebener Rang vor Rang liegt
	 */
	@Override
	public int compareTo(WayPoint wayPoint) {
		return this.orderID < wayPoint.orderID ? -1
				: this.orderID < wayPoint.orderID ? 0 : 1;
	}

	/**
	 * Gibt das Objekt als JSON-String.
	 * 
	 * @return JSON-String
	 * @throws JSONException
	 */
	public JSONObject toJSON() throws JSONException {
		return new JSONObject("{\"geoPoint\":" + this.geoPoint.toJSON()
				+ ",\"pointType\":" + this.pointType + ",\"orderID\":"
				+ this.orderID + ",\"range\":" + this.range + "}");
	}
	
	/* ====================================================================== */
    /*                       Hilfsklasse                                      */
    /* ====================================================================== */
    /**
     * Definiert den Typen des Punktes.
     */
    public enum PointType {
        /** Einfacher Wegpunkt der Tour. */
        WAY_POINT,
        /** Zielpunkt der der Tour. */
        TARGET_POINT,
        /** Startpunkt der der Tour. */
        START_POINT,
        /** Endpunkt der der Tour. */
        END_POINT;
    }
}