package de.SpiritualTours.Utility;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import de.SpiritualTours.Exceptions.ConnectionException;
import de.SpiritualTours.Exceptions.NoQuestionFoundException;
import de.SpiritualTours.Exceptions.NoTargetPointsFoundException;
import de.SpiritualTours.TourObjects.Question;
import de.SpiritualTours.TourObjects.Question.LevelOfDifficulty;
import de.SpiritualTours.TourObjects.TargetPoint;
import de.SpiritualTours.TourObjects.Tour;
import de.SpiritualTours.TourObjects.TourSettings;
import de.SpiritualTours.TourObjects.TourSettings.Language;

/**
 * Stellt eine Verbindung zum WebService her, um Touren und Fragen zu
 * Zielpunkten zu bekommen.
 */
public final class WebServiceConnection {

	/* ====================================================================== */
	/* Konstanten */
	/* ====================================================================== */
//	 /** URL Webservice f�r LAN. */
//	 private static final String URL_WEBSERVICE = "http://192.168.2.105:8080/"
//	 + "SpiritualTours-war/resources/spiritualtours/";

	/** URL Webservice f�r Internet. */
	private static final String URL_WEBSERVICE = "http://spiritualtours."
			+ "dyndns-server.com:8080/SpiritualTours-war/resources/"
			+ "spiritualtours/";

	/** URL f�r Touren. */
	private static final String URL_GET_TOURS = URL_WEBSERVICE + "tours";

	/** URL f�r Fragen. */
	private static final String URL_GET_ROUTE = URL_WEBSERVICE + "route";

	/** URL f�r Fragen. */
	private static final String URL_GET_QUESTION = URL_WEBSERVICE;

	/* Key f�r Einstellungen. */
	private static final String SETTINGS = "settings";

	/* Key f�r Zielpunkt. */
	private static final String TARGET_POINT = "targetPoint";

	/* Key f�r schon gestellte Fragen. */
	private static final String ASKED_QUESTION_IDS = "askedQuestionIDs";

	/** ID f�r Fehlermeldung: Verbindungsfehler. */
	private static final String CONNECTION_EXCEPTION = "ConnectionException";

	/* --------------------------------------------------------------------- */

	/** Umlaut �. */
	private static String UMLAUT_AE = "�";
	/** Umlaut �. */
	private static String UMLAUT_ae = "�";
	/** Umlaut �. */
	private static String UMLAUT_OE = "�";
	/** Umlaut �. */
	private static String UMLAUT_oe = "�";
	/** Umlaut �. */
	private static String UMLAUT_UE = "�";
	/** Umlaut �. */
	private static String UMLAUT_ue = "�";
	/** Umlaut �. */
	private static String UMLAUT_SZ = "�";

	/* --------------------------------------------------------------------- */

	/** Ersatz f�r Umlaut �. */
	private static String UMLAUT_HTML_AE = "&Auml;";
	/** Ersatz f�r Umlaut �. */
	private static String UMLAUT_HTML_ae = "&auml;";
	/** Ersatz f�r Umlaut �. */
	private static String UMLAUT_HTML_OE = "&Ouml;";
	/** Ersatz f�r Umlaut �. */
	private static String UMLAUT_HTML_oe = "&ouml;";
	/** Ersatz f�r Umlaut �. */
	private static String UMLAUT_HTML_UE = "&Uuml;";
	/** Ersatz f�r Umlaut �. */
	private static String UMLAUT_HTML_ue = "&uuml;";
	/** Ersatz f�r Umlaut �. */
	private static String UMLAUT_HTML_SZ = "&szlig;";

	/* ====================================================================== */
	/* getTours */
	/* ====================================================================== */
	/**
	 * Stellt eine Anfrage an den WebService. Dieser schickt, sofern vorhanden,
	 * erstellte Touren anhand der Einstellungen (inklusive der aktuellen
	 * Position) des Nutzers.
	 * 
	 * @param tourSettings
	 *            Einstellun des Nutzers.
	 * @return Touren
	 * @throws JSONException
	 *             fehler haftes JSON-Objekt BZW. keine Tour vorhanden.
	 * @throws IOException
	 *             HTTP Exception
	 * @throws ClientProtocolException
	 *             HTTP Exception
	 * @throws NoTargetPointsFoundException
	 *             keine Zielpunkte vorhanden
	 * @throws ConnectionException
	 *             Verbindungsfehler zu ORS
	 */
	public static List<Tour> getTours(TourSettings tourSettings)
			throws JSONException, ClientProtocolException, IOException,
			NoTargetPointsFoundException, ConnectionException {

		List<Tour> tours = null;

		/* Erzeugen eines neuen HttpClient und Post Header */
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(URL_GET_TOURS);

		/* Tour-Einstellungen hinzuf�gen */
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
		nameValuePairs.add(new BasicNameValuePair(SETTINGS, tourSettings
				.toWebServiceJSON().toString()));
		httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

		/* HTTP Post Request ausf�hren und Antwort auswerten */
		ResponseHandler<String> responseHandler = new BasicResponseHandler();
		String response = httpclient.execute(httppost, responseHandler);

		/*
		 * Response in Touren umwandeln, bei Parserfehler Fehlermeldung
		 * schmei�en
		 */
		try {
			tours = getTours(new JSONArray(replaceUmlauts(response)));
		} catch (JSONException e) {
			if (response.equals(CONNECTION_EXCEPTION)) {
				throw new ConnectionException();
			} else {
				throw new NoTargetPointsFoundException();
			}
		}
		return tours;
	}

	/**
	 * Parst das erhaltenene JSON-Array in Tour-Objekte.
	 * 
	 * @param jsonTours
	 *            JSON-Array
	 * @return Tour-Objekte
	 * @throws JSONException
	 *             bei fehlerhaften JSON-String
	 */
	private static List<Tour> getTours(JSONArray jsonTours)
			throws JSONException {
		List<Tour> tours = new ArrayList<Tour>();
		for (int i = 0; i < jsonTours.length(); i++) {
			tours.add(new Tour(jsonTours.getJSONObject(i)));
		}
		return tours;
	}

	/* ====================================================================== */
	/* getRoute */
	/* ====================================================================== */
	/**
	 * Stellt eine Anfrage an den WebService. Dieser schickt eine Route anhand
	 * der Einstellungen (inklusive der aktuellen Position) des Nutzers und dem
	 * Zielpunkt.
	 * 
	 * @param tourSettings
	 *            Einstellun des Nutzers.
	 * @param targetPoint
	 *            Zielpunkt
	 * @return Touren
	 * @throws JSONException
	 *             fehler haftes JSON-Objekt BZW. keine Tour vorhanden.
	 * @throws IOException
	 *             HTTP Exception
	 * @throws ClientProtocolException
	 *             HTTP Exception
	 * @throws ConnectionException
	 *             Verbindungsfehler zu ORS
	 */
	public static Tour getRoute(TourSettings tourSettings,
			TargetPoint targetPoint) throws JSONException,
			ClientProtocolException, IOException,
			ConnectionException {

		Tour tour = null;

		/* Erzeugen eines neuen HttpClient und Post Header */
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(URL_GET_ROUTE);

		/* Tour-Einstellungen hinzuf�gen */
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
		nameValuePairs.add(new BasicNameValuePair(SETTINGS, tourSettings
				.toWebServiceJSON().toString()));
		nameValuePairs.add(new BasicNameValuePair(TARGET_POINT, targetPoint
				.getGeoPoint().toJSON().toString()));
		httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

		/* HTTP Post Request ausf�hren und Antwort auswerten */
		ResponseHandler<String> responseHandler = new BasicResponseHandler();
		String response = httpclient.execute(httppost, responseHandler);

		/*
		 * Response in Touren umwandeln, bei Parserfehler Fehlermeldung
		 * schmei�en
		 */
		try {
			tour = new Tour(new JSONObject(replaceUmlauts(response)));
		} catch (JSONException e) {
			throw new ConnectionException();
		}
		return tour;
	}

	/* ====================================================================== */
	/* getQuestion */
	/* ====================================================================== */
	/**
	 * Stellt eine Anfrage an der Webservice. Bei Erfolg gibt der Webservice
	 * eine Frage passend zum Zielpunkt und Schwierigkeitsgrad zur�ck.
	 * 
	 * @param urlString
	 *            URL des Webservices
	 * @return Frage
	 * @throws JSONException
	 *             fehler haftes JSON-Objekt BZW. keine Tour vorhanden.
	 * @throws IOException
	 *             HTTP Exception
	 * @throws ClientProtocolException
	 *             HTTP Exception
	 * @throws NoQuestionFoundException
	 *             keine Frage vorhanden
	 */
	public static Question getQuestion(long targetPointID, Language language,
			LevelOfDifficulty levelOfDifficulty, long[] askedQuestionIDs)
			throws JSONException, ClientProtocolException, IOException,
			NoQuestionFoundException {

		/* Erzeugen eines neuen HttpClient und Post Header */
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(URL_GET_QUESTION + targetPointID + "/"
				+ language + "/" + levelOfDifficulty);

		/* Tour-Einstellungen hinzuf�gen */
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
		nameValuePairs.add(new BasicNameValuePair(ASKED_QUESTION_IDS,
				askedQuestionIDs2JSONArray(askedQuestionIDs).toString()));
		httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

		/* HTTP Post Request ausf�hren und Antwort auswerten */
		ResponseHandler<String> responseHandler = new BasicResponseHandler();

		String response = httpclient.execute(httppost, responseHandler);

		/*
		 * Response in Touren umwandeln, bei Parserfehler Fehlermeldung
		 * schmei�en
		 */
		try {
			return new Question(new JSONObject(replaceUmlauts(response)));
		} catch (JSONException e) {
			throw new NoQuestionFoundException();
		}
	}

	/**
	 * Generiert das JSONArray f�r die bereits gestellten Fragen.
	 * 
	 * @param askedQuestionIDs
	 *            bereits gestellte Fragen
	 * @return JSONArray
	 */
	private static JSONArray askedQuestionIDs2JSONArray(long[] askedQuestionIDs) {
		JSONArray askedQuestionIDsJSON = new JSONArray();
		for (long askedQuestionID : askedQuestionIDs) {
			askedQuestionIDsJSON.put(askedQuestionID);
		}
		return askedQuestionIDsJSON;
	}

	/* ====================================================================== */
	/* Hilfsmethode */
	/* ====================================================================== */
	/**
	 * Ersetzt alle HTML-Umlauts-Zeichen in die jeweiligen Umlaute.
	 * 
	 * @param string
	 *            Zeichkette
	 * @return Zeichkette mit ersetzten Zeichen
	 */
	private static String replaceUmlauts(String string) {
		return string.replace(UMLAUT_AE, UMLAUT_HTML_AE)
				.replace(UMLAUT_HTML_ae, UMLAUT_ae)
				.replace(UMLAUT_HTML_UE, UMLAUT_UE)
				.replace(UMLAUT_HTML_ue, UMLAUT_ue)
				.replace(UMLAUT_HTML_OE, UMLAUT_OE)
				.replace(UMLAUT_HTML_oe, UMLAUT_oe)
				.replace(UMLAUT_HTML_SZ, UMLAUT_SZ);
	}
}
