package de.SpiritualTours;

import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ScaleBarOverlay;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import de.SpiritualTours.Utility.IntentConstants;
import de.SpiritualTours.Utility.ActivityHelper;
import de.SpiritualTours.View.MapOverlay.TargetPointsOverlay;
import de.SpiritualTours.TourObjects.TargetPoint;

/**
 * Zur Anzeige eines Zielpunktes mit Karte.
 */
public class TargetPointDetailsActivity extends Activity {

	/** Zielpunkt. */
	private TargetPoint targetPoint;

	/** Textfeld f�r den Name des Zielpunkts. */
	private TextView nameTextView;
	/** Textfeld f�r den Typ des Zielpunkts. */
	private TextView typeTextView;
	/** Textfeld f�r die Information zum Zielpunkts. */
	private TextView infoTextView;

	/** Zur�ck-Button. */
	private Button backButton;
	
	/** Map. */
	private MapView mapView;
	/** Overlay f�r Scala der Map. */
	private ScaleBarOverlay scaleBarOverlay;
	/** Overlay f�r Zielpunkte der Map. */
	private TargetPointsOverlay targetPointsOverlay;

	/* ======================================================================= */
	/* OnCreate */
	/* ======================================================================= */
	/**
	 * Erzeugen der Zielpunkt-Details-Anzeige.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.targetpoint_details);

		getTargetPoint();
		init();
		setTargetPoint();
	}

	/* ======================================================================= */
	/* Initialisieren */
	/* ======================================================================= */
	/**
	 * �bergebenes Zielpunkt-Objekt parsen.
	 */
	private void getTargetPoint() {
		this.targetPoint = this.getIntent().getExtras()
				.getParcelable(IntentConstants.TARGETPOINT);
	}

	/**
	 * GUI-Elemente initialisieren.
	 */
	private void init() {
		this.nameTextView = (TextView) findViewById(R.id.tpd_name);
		this.typeTextView = (TextView) findViewById(R.id.tpd_type);
		this.infoTextView = (TextView) findViewById(R.id.tpd_info);
		this.backButton = (Button) findViewById(R.id.tpd_back);
		this.backButton.setOnClickListener(bBackListener);

		this.mapView = (MapView) this.findViewById(R.id.tpd_mapview);

		this.mapView.setTileSource(TileSourceFactory.MAPNIK);
		this.mapView.setBuiltInZoomControls(true);
		this.mapView.setMultiTouchControls(true);
		this.mapView.getController().setZoom(16);
		this.mapView.getController().setCenter(
				new GeoPoint(this.targetPoint.getGeoPoint().getLatitude(),
						this.targetPoint.getGeoPoint().getLongitude()));

		/* Zielpunkt */
		this.targetPointsOverlay = new TargetPointsOverlay(this,
				this.targetPoint);
		this.mapView.getOverlays().add(this.targetPointsOverlay);

		/* Skala */
		this.scaleBarOverlay = new ScaleBarOverlay(this);
		this.mapView.getOverlays().add(scaleBarOverlay);
	}

	/**
	 * Informationen setzen.
	 */
	private void setTargetPoint() {
		this.nameTextView.setText(this.targetPoint.getName());
		this.typeTextView.setText(getString(R.string.targetpointtype)
				+ ": "
				+ ActivityHelper.getTargetPointTypeName(this,
				this.targetPoint.getTargetPointType()));
		if (this.targetPoint.getInformation().equals("")) {
			this.infoTextView.setText(getString(R.string.tp_no_info_available));
		} else {
			this.infoTextView.setText(this.targetPoint.getInformation());
		}		
	}

	/* ======================================================================= */
	/* Back-Button */
	/* ======================================================================= */
	/**
	 * OnClickListener f�r Back-Button.
	 */
	private OnClickListener bBackListener = new OnClickListener() {

		/**
		 * Wird bei Klick auf den Button aufgerufen.
		 * 
		 * @param v
		 *            View
		 */
		public void onClick(View v) {
			onClickBack(v);
		}
	};

	/**
	 * Activity schliessen und zur Vater-Activity zur�ck.
	 * 
	 * @param v
	 *            View
	 */
	protected void onClickBack(View v) {

		finish();
	}
}
