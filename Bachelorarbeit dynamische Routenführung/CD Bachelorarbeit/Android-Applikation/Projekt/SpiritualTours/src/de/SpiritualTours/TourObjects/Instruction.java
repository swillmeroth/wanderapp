package de.SpiritualTours.TourObjects;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Anweisung der Teilstrecken.
 */
public class Instruction implements Parcelable {
	
	/* ====================================================================== */
	/* Konstanten */
	/* ====================================================================== */
	/** Kostante f�r Typ. */
	private static final String INSTRUCTION_DESCRIPTION = "description";
	/** Kostante f�r gesch�tzte Dauer in Minuten. */
	private static final String INSTRUCTION_DURATION = "duration";
	/** Kostante f�r gesch�tzte L�nge in km. */
	private static final String INSTRUCTION_DISTANCE = "distance";
	/** Kostante f�r die Anweisung. */
	private static final String INSTRUCTION_INSTRUCTION = "instruction";

	/* ====================================================================== */
	/* Instanzvariablen */
	/* ====================================================================== */
	/** Beschreibung zur Anweisung. */
	private String description;
	/** Dauer der Strecke in Minuten. */
	private int duration;
	/** Distanz in km. */
	private double distance;
	/** Anweisung. */
	private String instruction;
	
	/* ====================================================================== */
	/* Konstruktoren */
	/* ====================================================================== */
	/**
	 * Erzeugt eine Anweisung zur Wegbeschreibung.
	 * 
	 * @param descriptionOrderNo
	 *            Beschreibung zur Anweisung
	 * @param duration
	 *            zeitliche Dauer in Minuten
	 * @param distance
	 *            Distanz
	 * @param language
	 *            Sprache
	 * @param instruction
	 *            Anweisung
	 */
	public Instruction(String description, int duration, double distance,
			String instruction) {
		this.description = description;
		this.duration = duration;
		this.distance = distance;
		this.instruction = instruction;
	}

	/**
	 * Erzeugt eine Anweisung zur Wegbeschreibung.
	 * 
	 * @param jsonString
	 *            JSON-String
	 * @throws JSONException
	 *             bei fehlerhaften JSON-String
	 */
	public Instruction(JSONObject jsonInstruction) throws JSONException {

		this.description = jsonInstruction.getString(INSTRUCTION_DESCRIPTION);
		this.duration = Integer.valueOf(jsonInstruction
				.getString(INSTRUCTION_DURATION));
		this.distance = Double.valueOf(jsonInstruction
				.getString(INSTRUCTION_DISTANCE));
		this.instruction = jsonInstruction.getString(INSTRUCTION_INSTRUCTION);
	}

	/**
	 * Erzeugt eine Anweisung zur Wegbeschreibung.
	 */
	public Instruction(String instruction) {
		this.instruction = instruction;
	}
	
	/**
	 * Erzeugt eine Anweisung zur Wegbeschreibung.
	 */
	public Instruction() {
	}

	/* ====================================================================== */
	/* Getter & Setter */
	/* ====================================================================== */
	/**
	 * Gibt die Beschreibung zur Anweisung.
	 * 
	 * @return Reihenfolge
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Setzt die Beschreibung zur Anweisung.
	 * 
	 * @param descriptionOrderNo
	 *            Reihenfolge
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/* ---------------------------------------------------------------------- */
	/**
	 * Gibt die zeitliche Dauer in Minuten.
	 * 
	 * @return zeitliche Dauer
	 */
	public int getDuration() {
		return duration;
	}

	/**
	 * Setzt die zeitliche Dauer in Minuten.
	 * 
	 * @param duration
	 *            zeitliche Dauer
	 */
	public void setDuration(int duration) {
		this.duration = duration;
	}

	/* ---------------------------------------------------------------------- */
	/**
	 * Gibt die Distanz.
	 * 
	 * @return Distanz
	 */
	public double getDistance() {
		return distance;
	}

	/**
	 * Setzt die Distanz.
	 * 
	 * @param distance
	 *            Distanz
	 */
	public void setDistance(double dinstance) {
		this.distance = dinstance;
	}

	/* ---------------------------------------------------------------------- */
	/**
	 * Gibt die Anweisung.
	 * 
	 * @return Anweisung
	 */
	public String getInstruction() {
		return instruction;
	}

	/**
	 * Setzt die Anweisung.
	 * 
	 * @param instruction
	 *            Anweisung
	 */
	public void setInstruction(String instruction) {
		this.instruction = instruction;
	}

	/* ======================================================================= */
	/* Parcelable-Methoden */
	/* ======================================================================= */
	public static final Parcelable.Creator<Instruction> CREATOR = new Parcelable.Creator<Instruction>() {

		public Instruction createFromParcel(Parcel in) {
			return new Instruction(in);
		}

		public Instruction[] newArray(int size) {
			return new Instruction[size];
		}
	};

	private Instruction(final Parcel in) {
		readFromParcel(in);
	}

	public void writeToParcel(Parcel out, int flags) {
		out.writeString(this.description);
		out.writeInt(this.duration);
		out.writeDouble(this.distance);
		out.writeString(this.instruction);
	}

	public void readFromParcel(final Parcel in) {
		this.description = in.readString();
		this.duration = in.readInt();
		this.distance = in.readDouble();
		this.instruction = in.readString();
	}

	public int describeContents() {
		return 0;
	}

	/* ====================================================================== */
	/* toString,... */
	/* ====================================================================== */
	/**
	 * Gibt die Anweisung als String.
	 * 
	 * @return Anweisung als String
	 */
	@Override
	public String toString() {
		return "Beschreibung: " + description + ", Dauer: " + duration
				+ " Minuten" + ", Distanz: " + distance + " km"
				+ ", Anweisung: " + instruction;
	}

	/**
	 * Gibt das Objekt als JSON-String.
	 * 
	 * @return JSON-String
	 * @throws JSONException
	 */
	public JSONObject toJSON() throws JSONException {
		return new JSONObject("{\"description\":\"" + this.description
				+ "\",\"distance\":" + this.distance + ",\"duration\":"
				+ this.duration + ",\"instruction\":\"" + this.instruction
				+ "\"}");
	}
}
