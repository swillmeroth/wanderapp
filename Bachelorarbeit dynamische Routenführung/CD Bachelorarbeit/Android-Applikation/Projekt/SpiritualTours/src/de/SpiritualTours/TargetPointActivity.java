package de.SpiritualTours;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import de.SpiritualTours.Utility.IntentConstants;
import de.SpiritualTours.TourObjects.TargetPoint;
import de.SpiritualTours.Utility.ActivityHelper;

/**
 * Zur Anzeige eines Zielpunktes ohne Karte.
 */
public class TargetPointActivity extends Activity {

	/** Zielpunkt. */
	private TargetPoint targetPoint;

	/** Textfeld f�r den Name des Zielpunkts. */
	private TextView nameTextView;
	/** Textfeld f�r den Typ des Zielpunkts. */
	private TextView typeTextView;
	/** Textfeld f�r die Information zum Zielpunkts. */
	private TextView infoTextView;
	/** Textfeld f�r den Ort des Zielpunkts. */
	private TextView townTextView;
	/** Textfeld f�r die Stra�e des Zielpunkts. */
	private TextView streetTextView;

	/** Button, um Tour fortzusetzen. */
	private Button continueButton;

	/* ======================================================================= */
	/* OnCreate */
	/* ======================================================================= */
	/**
	 * Erzeugen der Zielpunkt-Details-Anzeige.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.targetpoint);

		getTargetPoint();
		init();
		setTargetPoint();
	}

	/* ======================================================================= */
	/* Initialisieren */
	/* ======================================================================= */
	/**
	 * �bergebenes Zielpunkt-Objekt parsen.
	 */
	private void getTargetPoint() {
		this.targetPoint = this.getIntent().getExtras()
				.getParcelable(IntentConstants.TARGETPOINT);
	}

	/**
	 * GUI-Elemente initialisieren.
	 */
	private void init() {
		this.nameTextView = (TextView) findViewById(R.id.tp_name);
		this.typeTextView = (TextView) findViewById(R.id.tp_type);
		this.infoTextView = (TextView) findViewById(R.id.tp_info);
		this.townTextView = (TextView) findViewById(R.id.tp_town);
		this.streetTextView = (TextView) findViewById(R.id.tp_street);

		this.continueButton = (Button) findViewById(R.id.tp_continue);
		this.continueButton.setOnClickListener(bContinueListener);
	}

	/**
	 * Informationen setzen.
	 */
	private void setTargetPoint() {
		this.nameTextView.setText(this.targetPoint.getName());
		this.typeTextView.setText(getString(R.string.targetpointtype)
				+ ": "
				+ ActivityHelper.getTargetPointTypeName(this,
						this.targetPoint.getTargetPointType()));
		if (this.targetPoint.getInformation().equals("")) {
			this.infoTextView.setText(getString(R.string.tp_no_info_available));
		} else {
			this.infoTextView.setText(this.targetPoint.getInformation());
		}
		this.townTextView.setText(getString(R.string.town) + " "
				+ this.targetPoint.getTown());
		this.streetTextView.setText(getString(R.string.street) + " "
				+ this.targetPoint.getStreet());
	}

	/* ======================================================================= */
	/* Back-Button */
	/* ======================================================================= */
	/**
	 * OnClickListener f�r Back-Button.
	 */
	private OnClickListener bContinueListener = new OnClickListener() {

		/**
		 * Wird bei Klick auf den Button aufgerufen.
		 * 
		 * @param v
		 *            View
		 */
		public void onClick(View v) {
			onClickContinue(v);
		}
	};

	/**
	 * Activity schliessen und zur Vater-Activity zur�ck.
	 * 
	 * @param v
	 *            View
	 */
	protected void onClickContinue(View v) {
		finish();
	}
}
