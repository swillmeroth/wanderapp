package de.SpiritualTours;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import de.SpiritualTours.Adapter.QuestionAnswersAdapter;
import de.SpiritualTours.Exceptions.NoQuestionFoundException;
import de.SpiritualTours.TourObjects.Question;
import de.SpiritualTours.TourObjects.TargetPoint;
import de.SpiritualTours.TourObjects.TourSettings;
import de.SpiritualTours.Utility.IntentConstants;
import de.SpiritualTours.Utility.ToursAdministrator;
import de.SpiritualTours.Utility.WebServiceConnection;

/**
 * Activity zum Stellen einer Frage.
 */
public class QuestionerActivity extends Activity {

	/** Frage-Objekt. */
	private Question question;
	/** IDs, der bereits gestellten Fragen. */
	private long[] askedQuestionIDs;
	/** Zielpunkt zu dem die Frage gestellt werden soll */
	private TargetPoint targetPoint;
	/** Einstellungen der Tour */
	private TourSettings tourSettings;

	/** TextView zur Angabe der Frage. */
	private TextView questionTextView;
	/** Antwort-Liste. */
	private ListView answersListView;
	/** Adapter, der die Antworten der Frage verwaltet. */
	private QuestionAnswersAdapter questionAnswersAdapter;

	/** Meldung f�r: Route vom Server erhalten. */
	private final static int HANDLER_GOT_QUESTION = 0;
	/** Meldung f�r: Verbindungsfehler. */
	private final static int HANDLER_CLIENT_PROTOCOL_EXCEPTION = 1;
	/** Meldung f�r: keine Erstellung der Tour m�glich. */
	private final static int HANDLER_JSON_EXCEPTION = 2;
	/** Meldung f�r: Verbindungsfehler. */
	private final static int HANDLER_IO_EXCEPTION = 3;
	/** Meldung f�r: keine Erstellung der Tour m�glich. */
	private final static int HANDLER_NO_QUESTION_EXCEPTION = 4;

	/**
	 * Dialog zur Anzeige von Prozessen. Hier f�r Ermittlung der Position und
	 * Erstellung der Touren.
	 */
	private ProgressDialog dialog;

	/**
	 * Handler, um Nachrichten des Threads in der Activity bearbeiten zu k�nnen.
	 */
	private final Handler threadHandler = new Handler() {

		/**
		 * Handlet die Nachrichten des Threads.
		 * 
		 * @param msg
		 *            Nachricht des Threads
		 */
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case HANDLER_GOT_QUESTION:
				dialog.dismiss();
				askQuestion();
				break;
			case HANDLER_CLIENT_PROTOCOL_EXCEPTION:
				dialog.dismiss();
				showMessageBoxToFinishActiviy(
						getString(R.string.error_caption),
						getString(R.string.error_no_connection_to_server));
				break;
			case HANDLER_JSON_EXCEPTION:
				dialog.dismiss();
				showMessageBoxToFinishActiviy(
						getString(R.string.error_caption),
						getString(R.string.error_unknown));
				break;
			case HANDLER_IO_EXCEPTION:
				dialog.dismiss();
				showMessageBoxToFinishActiviy(
						getString(R.string.error_caption),
						getString(R.string.error_no_connection_to_server));
				break;
			case HANDLER_NO_QUESTION_EXCEPTION:
				dialog.dismiss();
				if (askedQuestionIDs.length == 0) {
					showMessageBoxToFinishActiviy(
							getString(R.string.error_no_question_caption),
							getString(R.string.error_no_question));
				} else {
					showMessageBoxToFinishActiviy(
							getString(R.string.error_no_question_caption),
							getString(R.string.error_no_questions));
				}
				break;
			}
		}
	};

	/* ======================================================================= */
	/* OnCreate */
	/* ======================================================================= */
	/**
	 * Erzeugen der Activity. Wird bei Erstaufruf der Activity ausgef�hrt.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.questioner);

		this.tourSettings = ToursAdministrator.getInstance().getTourSettings();
		this.askedQuestionIDs = new long[] {};

		getTargetPoint();
		init();
		this.dialog = ProgressDialog.show(this, getString(R.string.wait),
				getString(R.string.q_get_question), true, false);
		new GetQuestionThread(threadHandler).start();
	}

	/**
	 * Gibt den Zielpunkt, zu dem die Frage gestellt werden soll.
	 */
	private void getTargetPoint() {
		this.targetPoint = this.getIntent().getExtras()
				.getParcelable(IntentConstants.TARGETPOINT);
	}

	/**
	 * Initialisierung der Komponenten.
	 */
	private void init() {

		this.questionTextView = (TextView) findViewById(R.id.q_question);
		this.answersListView = (ListView) findViewById(R.id.q_answer_list);
	}

	/**
	 * Setzt die Frage in der GUI.
	 */
	private void askQuestion() {
		this.questionTextView.setText(this.question.getQuestion());
		this.questionAnswersAdapter = new QuestionAnswersAdapter(this,
				this.question.getWrongAnswers(),
				this.question.getCorrectAnswer());
		this.answersListView.setAdapter(this.questionAnswersAdapter);

		/* Listener reagiert auf Beantwortung der Frage */
		this.answersListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> a, View v, int position,
					long id) {

				if (questionAnswersAdapter.isCorrectAnswer(position)) {
					showMessageBoxToFinishActiviy(
							getString(R.string.q_answer_correct_caption),
							getString(R.string.q_answer_correct));
				} else {
					showMessageBox(getString(R.string.q_answer_wrong_caption),
							getString(R.string.q_answer_wrong));
				}
			}
		});
	}

	/**
	 * F�gt eine Question-ID der Liste f�r bereits gestellte Fragen hinzu.
	 * 
	 * @param id
	 *            Question-ID
	 */
	private void addAskedQuestionID(long id) {
		long[] newAskedQuestionIDs = new long[this.askedQuestionIDs.length + 1];
		for (int i = 0; i < this.askedQuestionIDs.length; i++) {
			newAskedQuestionIDs[i] = this.askedQuestionIDs[i];
		}
		newAskedQuestionIDs[this.askedQuestionIDs.length] = id;
		this.askedQuestionIDs = newAskedQuestionIDs;
	}

	/**
	 * Anzeigen des Zielpunktes. Nach Anzeigen des Zielpunktes geht es mit der
	 * onResume-Methode weiter.
	 */
	private void showTargetPoint() {

		finish();

		/* Intent erstellen */
		Intent targetPointDetails = new Intent(this, TargetPointActivity.class);
		targetPointDetails.putExtra(IntentConstants.TARGETPOINT,
				this.targetPoint);

		/* erreichter Zielpunkt wird angezeigt */
		startActivity(targetPointDetails);
	}

	/**
	 * Zeigte eine Info-Fenster mit einem Ok-Button zum Schlie�en. Nach dem
	 * Schlie�en der Box wird die Activity geschlossen und der Zielpunkt
	 * angezeigt.
	 * 
	 * @param context
	 *            Context
	 * @param title
	 *            Titel
	 * @param message
	 *            Meldung
	 */
	public void showMessageBoxToFinishActiviy(String title, String message) {

		AlertDialog msgBox = new AlertDialog.Builder(this).create();
		msgBox.setTitle(title);
		msgBox.setMessage(message);
		msgBox.setButton(getString(R.string.button_ok),
				new android.content.DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						showTargetPoint();
					}
				});
		msgBox.show();
	}

	/**
	 * Zeigte eine Info-Fenster mit einem Ok-Button zum Schlie�en.
	 * 
	 * @param context
	 *            Context
	 * @param title
	 *            Titel
	 * @param message
	 *            Meldung
	 */
	public void showMessageBox(String title, String message) {

		AlertDialog msgBox = new AlertDialog.Builder(this).create();
		msgBox.setTitle(title);
		msgBox.setMessage(message);
		msgBox.setButton(getString(R.string.button_ok),
				new android.content.DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						addAskedQuestionID(question.getId());
						dialog = ProgressDialog.show(QuestionerActivity.this,
								getString(R.string.wait),
								getString(R.string.n_calculate_new_route),
								true, false);
						new GetQuestionThread(threadHandler).start();
					}
				});
		msgBox.show();
	}

	/* ======================================================================= */
	/* Thread */
	/* ======================================================================= */
	/**
	 * Thread zur Ermittlung einer neuen Route zum n�chste Zielpunkt.
	 */
	public class GetQuestionThread extends Thread {

		/** Handler zum Benachrichtigen der Activity */
		private Handler handler;

		/**
		 * Erzeugt einen Thread zur Ermittlung der Touren.
		 * 
		 * @param handler
		 *            Handler zum Benachrichtigen der Activity
		 */
		public GetQuestionThread(Handler handler) {
			super();
			this.handler = handler;
		}

		/**
		 * Ausf�hren des Threads.
		 */
		public void run() {

			try {

				question = WebServiceConnection.getQuestion(
						targetPoint.getId(), tourSettings.getLanguage(),
						tourSettings.getLevelOfDifficulty(), askedQuestionIDs);

				/* Handler benachrichtigen, dass neue Route berechnet wurde */
				this.handler.sendEmptyMessage(HANDLER_GOT_QUESTION);

			} catch (ClientProtocolException e) {
				/* Fehler-Meldung anzeigen -> Verbindung mit dem Server. */
				this.handler
						.sendEmptyMessage(HANDLER_CLIENT_PROTOCOL_EXCEPTION);
			} catch (JSONException e) {
				this.handler.sendEmptyMessage(HANDLER_JSON_EXCEPTION);
			} catch (IOException e) {
				this.handler.sendEmptyMessage(HANDLER_IO_EXCEPTION);
			} catch (NoQuestionFoundException e) {
				this.handler.sendEmptyMessage(HANDLER_NO_QUESTION_EXCEPTION);
			}
		}
	}
}
