package de.SpiritualTours.Adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import de.SpiritualTours.R;
import de.SpiritualTours.TourObjects.Tour;
import de.SpiritualTours.TourObjects.Tour.TourType;
import de.SpiritualTours.Utility.ActivityHelper;

/**
 * Zur Anzeige der Touren. Der folgende Link hat bei der Implementierung der
 * Adapterklasse sehr geholfen: http://geekswithblogs.net/bosuch
 * /archive/2011/01/31/android---create-a-custom-
 * multi-line-listview-bound-to-an.aspx.
 */
public class ToursAdviserAdapter extends BaseAdapter {

	/** Mögliche Touren. */
	private List<Tour> tours;

	/** Context. */
	private Context context;

	/** LayoutInflater. */
	private LayoutInflater mInflater;

	/**
	 * Erzeugt einen ToursAdviserAdapter zur Anzeige der Touren.
	 * 
	 * @param context
	 *            Context
	 * @param tours
	 *            Touren
	 */
	public ToursAdviserAdapter(Context context, List<Tour> tours) {
		this.tours = tours;
		this.context = context;
		this.mInflater = LayoutInflater.from(context);
	}

	/**
	 * Gibt die Anzahl der Touren.
	 * 
	 * @return Anzahl
	 */
	@Override
	public int getCount() {
		return tours.size();
	}

	/**
	 * Gibt die ausgewählte Tour.
	 * 
	 * @return Tour
	 */
	@Override
	public Tour getItem(int position) {
		return tours.get(position);
	}

	/**
	 * Gibt die ID der ausgewählten Tour.
	 * 
	 * @param position
	 *            Index der Tour in der Liste
	 * @return ID
	 */
	@Override
	public long getItemId(int position) {
		return position;
	}

	/**
	 * Organiersiert die Anzeige in der grafischen Oberfläche.
	 * 
	 * @param pos
	 *            Index der Tour in der Auflistung
	 * @param convertView
	 *            View
	 * @param parent
	 *            übgeordnete Viewgruppe
	 */
	@Override
	public View getView(int pos, View convertView, ViewGroup parent) {
		ViewHolder holder;

		if (convertView == null) {

			convertView = mInflater.inflate(R.layout.tour_item, null);
			holder = new ViewHolder();
			holder.tour = (TextView) convertView.findViewById(R.id.tour);
			holder.type = (TextView) convertView.findViewById(R.id.type);
			holder.length = (TextView) convertView.findViewById(R.id.length);
			holder.time = (TextView) convertView.findViewById(R.id.time);
			holder.targetPoints = (TextView) convertView
					.findViewById(R.id.targetpoints);
			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		/* Setzen der Informationen */
		setInfo(holder, pos);

		return convertView;
	}

	/**
	 * Holder, der die GUI-Elemente zur Auflistung der Touren enthält.
	 */
	static class ViewHolder {
		TextView tour;
		TextView type;
		TextView length;
		TextView time;
		TextView targetPoints;
	}

	/**
	 * Setzt die Kern-Informationen der Tour.
	 * 
	 * @param holder
	 *            Holder
	 * @param pos
	 *            Index des Zielpunkts in der Liste
	 */
	private void setInfo(ViewHolder holder, int pos) {
		holder.tour.setText(this.context.getString(R.string.tour) + " "
				+ (pos + 1));
		holder.type
				.setText(this.context.getString(R.string.tour_type)
						+ " "
						+ (tours.get(pos).getTourType().equals(TourType.FLATLY) ? context
								.getString(R.string.flatly) : context
								.getString(R.string.route)));
		holder.length.setText(this.context.getString(R.string.distance)
				+ " "
				+ ActivityHelper.getDistance(tours.get(pos)
						.getEstimatedTourLength()));
		holder.time.setText(this.context.getString(R.string.duration)
				+ " "
				+ ActivityHelper.getTime(context, tours.get(pos)
						.getEstimatedDuration(), false));
		holder.targetPoints.setText(this.context
				.getString(R.string.ta_number_of_targetpoints)
				+ " "
				+ tours.get(pos).getTargetPoints().size());
	}
}
