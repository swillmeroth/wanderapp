package de.SpiritualTours.Utility;

import java.util.ArrayList;
import java.util.List;

import de.SpiritualTours.TourObjects.GeoPoint;
import de.SpiritualTours.TourObjects.TargetPoint;
import de.SpiritualTours.TourObjects.WayPoint;
import de.SpiritualTours.TourObjects.WayPoint.PointType;

/**
 * Stellt Methoden zur Navigation zur Verf�gung.
 */
public class Navigate {

	/* ======================================================================= */
	/* Initialisierung der Klasse (bei erstmaligen Zugriff). */
	/* ======================================================================= */
	/**
	 * Instanz der Klasse.
	 */
	private static Navigate instance = new Navigate();

	/**
	 * Default-Konstruktor, der nicht au�erhalb dieser Klasse aufgerufen werden
	 * kann.
	 */
	private Navigate() {
	}

	/**
	 * Liefert die einzige Instanz dieser Klasse zur�ck
	 */
	public static Navigate getInstance() {
		return instance;
	}

	/* ====================================================================== */
	/* Instanzvariablen */
	/* ====================================================================== */

	/** Wegpunkte der Tour */
	private List<WayPoint> wayPoints;

	/** zu letzt erreichter Wegpunkt (Index in der Wegpunktliste). */
	private int lastWayPointIndex;

	/** N�chster Wegpunkt. */
	private WayPoint nextWayPoint;

	/** Gibt der aktuellen Stand der Tour */
	private TourState currentTourState;

	/* ====================================================================== */
	/* Methoden */
	/* ====================================================================== */
	/**
	 * Startet die Navigation.
	 * 
	 * @param wayPoints
	 *            Wegpunkte �ber die navigiert werden soll
	 */
	public void startNavigation(List<WayPoint> wayPoints) {
		this.wayPoints = wayPoints;
		this.lastWayPointIndex = -1;
		this.nextWayPoint = this.wayPoints.get(0);
		this.currentTourState = TourState.NO_WAY_POINT_REACHED;
	}

	/**
	 * Liefert die Wegpunkte der Tour.
	 * 
	 * @return Wegpunkte
	 */
	public List<WayPoint> getWayPoints() {
		return this.wayPoints;
	}

	/**
	 * Gibt an, ob ein n�chster Wegpunkt erreicht wurde.
	 * 
	 * @param currentLocation
	 *            aktuelle Position
	 * @return sofern Wegpunkt erreicht, dieser, sonst null
	 */
	public TourState getIsNextWayPointReached(GeoPoint currentLocation) {

		WayPoint reacheadWayPoint = null;

		/* Pr�fen, ob ein Wegpunkt erreicht worden ist */
		for (int index = lastWayPointIndex + 1; index < wayPoints.size(); index++) {

			WayPoint currentWayPoint = wayPoints.get(index);

			if (Geo.getDistance(currentLocation, currentWayPoint.getGeoPoint()) <= (currentWayPoint
					.getRange() / 1000.0)) {
				reacheadWayPoint = currentWayPoint;
			}

			if (currentWayPoint.getPointType().equals(PointType.TARGET_POINT)) {
				break;
			}
		}

		/*
		 * Fall 1: Wegpunkt erreicht und kein Zielpunkt erreicht Fall 2:
		 * Endpunkt der Tour erreicht Fall 3: Keinen neuen Wegpunkt erreicht
		 */
		if (reacheadWayPoint != null) {

			this.lastWayPointIndex = getLastWayPointIndex(reacheadWayPoint);

			/* Wenn Tourende noch nicht erreicht, dann..., sonst... */
			if (this.lastWayPointIndex < this.wayPoints.size() - 1) {

				this.nextWayPoint = this.wayPoints
						.get(this.lastWayPointIndex + 1);

				/* Wegpunkt oder Zielpunkt */
				this.currentTourState = reacheadWayPoint.getPointType().equals(
						PointType.TARGET_POINT) ? TourState.TARGET_POINT_REACHED
						: TourState.WAY_POINT_REACHED;

				return this.currentTourState;

			} else {

				this.currentTourState = TourState.END_OF_TOUR_REACHED;
				return this.currentTourState;
			}

		} else {
			this.currentTourState = TourState.NO_WAY_POINT_REACHED;
			return this.currentTourState;
		}
	}

	/**
	 * Gibt den Index des erreichten Wegpunktes an.
	 * 
	 * @param reacheadWayPoint
	 *            erreichter Wegpunkt
	 * @return Index des erreichten Wegpunktes
	 */
	private int getLastWayPointIndex(WayPoint reacheadWayPoint) {
		int index = this.lastWayPointIndex;
		while (index < this.wayPoints.size()) {
			index++;
			if (this.wayPoints.get(index).equals(reacheadWayPoint)) {
				return index;
			}
		}
		return -1;
	}

	/**
	 * Gibt den zu letzt erreichten Wegpunkt.
	 * 
	 * @return zu letzt erreichter Wegpunkt
	 */
	public WayPoint getLastReachedWayPoint() {
		return this.lastWayPointIndex != -1 ? this.wayPoints
				.get(this.lastWayPointIndex) : null;
	}

	/**
	 * Gibt den n�chsten Wegpunkt.
	 * 
	 * @return n�chster Wegpunkt
	 */
	public WayPoint getNextWayPoint() {
		return this.nextWayPoint;
	}

	/**
	 * Gibt den Tour-Status.
	 * 
	 * @return Tour-Status
	 */
	public TourState getTourState() {
		return this.currentTourState;
	}

	/**
	 * Gibt die Distanz zum n�chsten Zielpunkt in km bzw in m an. Km wenn die
	 * Distanz �ber 1000m betr�gt.
	 * 
	 * @param currentLocation
	 *            aktuelle Position des Nutzers
	 * 
	 * @return Distanz zum n�chsten Zielpunkt
	 */
	public String getDistanceToNextTargetPoint(GeoPoint currentLocation) {

		boolean istTargetPointReached = false;

		/* Distanz von aktueller Position zum n�chsten Wegpunkt bestimmen */
		double distance = Geo.getDistance(currentLocation,
				nextWayPoint.getGeoPoint());

		/* Index des aktuelle Wegpunktes */
		int wpIndex = this.lastWayPointIndex + 1;

		/* Entfernung �ber alle Wegpunkte messen, bis Zielpunkt erreich */
		while (wpIndex < wayPoints.size() - 1 && !istTargetPointReached) {

			WayPoint wpStart = this.wayPoints.get(wpIndex);
			WayPoint wpEnd = this.wayPoints.get(wpIndex + 1);

			if (wpEnd.getPointType().equals(PointType.TARGET_POINT)) {
				istTargetPointReached = true;
			} else {
				distance += Geo.getDistance(wpStart.getGeoPoint(),
						wpEnd.getGeoPoint());
			}
			wpIndex++;
		}

		/* Distanz als String zur�ckgeben */
		return ActivityHelper.getDistance(distance);
	}

	/**
	 * Gibt die bereits zur�ckgelegte Strecke in km.
	 * 
	 * @param currentLocation
	 *            aktuelle Position des Nutzers
	 * 
	 * @return zur�ckgelegte Strecke in km
	 */
	public String getTraveledDistance(GeoPoint currentLocation) {

		/* Index des aktuelle Wegpunktes */
		int wpIndex = this.lastWayPointIndex;
		double distance = 0;

		if (wpIndex != -1) {

			distance = Geo.getDistance(currentLocation, wayPoints.get(wpIndex)
					.getGeoPoint());

			while (wpIndex > 1) {

				GeoPoint gpStart = wayPoints.get(wpIndex).getGeoPoint();
				GeoPoint gpEnd = wayPoints.get(wpIndex - 1).getGeoPoint();

				distance += Geo.getDistance(gpStart, gpEnd);

				wpIndex--;
			}
		}

		/* Distanz als String zur�ckgeben */
		return ActivityHelper.getDistance(distance);
	}

	/**
	 * �berspringen des n�chsten Zielpunktes. Der Zielpunkt wird gel�scht.
	 * 
	 * @param toSkippedTargetPoint
	 *            zu �berspringender Zielpunkt
	 */
	public void skipNextTargetPoint(TargetPoint toSkippedTargetPoint) {
		if(toSkippedTargetPoint != null) {
			this.wayPoints.remove(this.wayPoints.indexOf(toSkippedTargetPoint));
		}		
	}

	/**
	 * Ersetzt die Zielpunkte von aktuellem Wegpunkt bis zum n�chsten Zielpunkt
	 * durch die neue Route.
	 * 
	 * @param toSkippedTargetPoint
	 *            zu �berspringender Zielpunkt
	 * @param route
	 *            neue Route
	 */
	public void replaceWayPoints(List<WayPoint> route,
			TargetPoint nextTargetPoint) {

		/*
		 * neue Wegpunkt-Liste erstellen, mit den Wegpunkt bis zur aktuellen
		 * Position
		 */
		List<WayPoint> newWayPoints = new ArrayList<WayPoint>();
		for (int i = 0; i <= this.lastWayPointIndex; i++) {
			this.wayPoints.get(i).setOrderID(newWayPoints.size() + 1);
			newWayPoints.add(this.wayPoints.get(i));
		}

		/* neu berechnete Route einzuf�gen */
		for (int i = 0; i < route.size() - 1; i++) {
			route.get(i).setOrderID(newWayPoints.size() + 1);
			newWayPoints.add(route.get(i));

		}

		/*
		 * ab dem n�chsten Zielpunkt die Wegpunkte hinzuf�gen, handelt es sich
		 * um den Endpunkt, wird der letzt Punkt der Teilroute hinzugef�gt.
		 */
		if (nextTargetPoint != null) {
			for (int i = this.wayPoints.indexOf(nextTargetPoint); i < this.wayPoints.size(); i++) {
				this.wayPoints.get(i).setOrderID(newWayPoints.size() + 1);
				newWayPoints.add(this.wayPoints.get(i));
			}
		} else {
			newWayPoints.add(route.get(route.size() - 1));
		}

		/* neue Wegpunktliste setzen */
		this.wayPoints = newWayPoints;

		/* neuer n�chster Wegpunkt bestimmen */
		this.nextWayPoint = this.wayPoints.get(this.lastWayPointIndex + 1);
	}

	/**
	 * Enumaration zur Angabe des Tour-Statusses.
	 */
	public enum TourState {
		/** Kein Wegpunkt erreicht. */
		NO_WAY_POINT_REACHED,
		/** Zielpunkt erreicht. */
		TARGET_POINT_REACHED,
		/** Wegpunkt erreicht. */
		WAY_POINT_REACHED,
		/** Ende der Tour erreicht. */
		END_OF_TOUR_REACHED;
	}
}
