package de.SpiritualTours.View.MapOverlay;

import org.osmdroid.views.overlay.PathOverlay;
import android.content.Context;

/**
 * Overlay zur Anzeige der Wegstrecke auf der Karte.
 */
public class TourOverlay extends PathOverlay {

	/**
	 * Erzeugt eine neues Tour-Overlay zur Anzeige der Tour-Strecke.
	 * 
	 * @param color
	 * @param ctx
	 */
	public TourOverlay(int color, Context ctx) {
		super(color, ctx);
		mPaint.setStrokeWidth(5.0f);
		mPaint.setAlpha(175);
	}
}
