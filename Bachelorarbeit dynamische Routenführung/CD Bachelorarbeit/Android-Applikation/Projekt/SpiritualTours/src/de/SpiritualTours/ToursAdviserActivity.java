package de.SpiritualTours;

import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import de.SpiritualTours.Adapter.ToursAdviserAdapter;
import de.SpiritualTours.Utility.IntentConstants;
import de.SpiritualTours.TourObjects.Tour;
import de.SpiritualTours.Utility.ToursAdministrator;

/**
 * Activity zur Anzeige der Touren, die vorgeschlagen werden.
 */
public class ToursAdviserActivity extends Activity {

	/** Touren die dem User angeboten werden. */
	private List<Tour> tours;

	/** Zur�ck-Button. */
	private Button backButton;

	/* ======================================================================= */
	/* OnCreate */
	/* ======================================================================= */
	/**
	 * Called when the activity is first created.
	 * 
	 * @param savedInstanceState
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.tours_adviser);

		this.tours = ToursAdministrator.getInstance().getTours();

		final ListView tourList = (ListView) findViewById(R.id.ta_list);
		tourList.setAdapter(new ToursAdviserAdapter(this, this.tours));

		tourList.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> a, View v, int position,
					long id) {
				Intent tourDetails = new Intent(ToursAdviserActivity.this,
						TourDetailsActivity.class);
				tourDetails.putExtra(IntentConstants.TOUR_NAME,
						getString(R.string.tour) + " " + (position + 1));
				tourDetails.putExtra(IntentConstants.TOUR_ID, position);
				startActivity(tourDetails);
			}
		});

		this.backButton = (Button) findViewById(R.id.ta_back);
		this.backButton.setOnClickListener(bBackListener);
	}

	/* ======================================================================= */
	/* Men� */
	/* ======================================================================= */
	/**
	 * Anzeigen des Hauptmenues.
	 */
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.help_menu, menu);
		return super.onCreateOptionsMenu(menu);
	}

	/**
	 * Event-Handling des Menues.
	 * 
	 * @param item
	 *            Menuepunkt, der gewaehlt wurde.
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.menu_h_help:
			onClickHelp();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/**
	 * Aktionen bei Klick auf Settings-Button.
	 */
	protected void onClickHelp() {
		showMessageBox(getString(R.string.menu_help),
				getString(R.string.ta_help));
	}

	/* ======================================================================= */
	/* Back-Button */
	/* ======================================================================= */
	/**
	 * OnClickListener f�r Back-Button.
	 */
	private OnClickListener bBackListener = new OnClickListener() {

		/**
		 * Wird bei Klick auf den Button aufgerufen.
		 * 
		 * @param v
		 *            View
		 */
		public void onClick(View v) {
			onClickBack(v);
		}
	};

	/**
	 * Activity schliessen und zur Vater-Activity zur�ck.
	 * 
	 * @param v
	 *            View
	 */
	protected void onClickBack(View v) {

		finish();
	}

	/**
	 * Zeigte eine Info-Fenster mit einem Ok-Button zum Schlie�en.
	 * 
	 * @param context
	 *            Context
	 * @param title
	 *            Titel
	 * @param message
	 *            Meldung
	 */
	public void showMessageBox(String title, String message) {

		AlertDialog msgBox = new AlertDialog.Builder(this).create();
		msgBox.setTitle(title);
		msgBox.setMessage(message);
		msgBox.setButton(getString(R.string.button_ok),
				new android.content.DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						/* Fenster schlie�en */
					}
				});
		msgBox.show();
	}
}
