package de.SpiritualTours.Adapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import de.SpiritualTours.R;

/**
 * Zur Anzeige der Antworten einer Frage. Der folgende Link hat bei der
 * Implementierung der Adapterklasse sehr geholfen:
 * http://geekswithblogs.net/bosuch
 * /archive/2011/01/31/android---create-a-custom-
 * multi-line-listview-bound-to-an.aspx.
 */
public class QuestionAnswersAdapter extends BaseAdapter {

	/** M�gliche Antwort. */
	private List<String> answers;

	/** Korrekte Antwort. */
	private int correctAnswerIndex;

	/** LayoutInflater. */
	private LayoutInflater mInflater;

	/**
	 * Erzeugt einen QuestionAnswersAdapter zur Anzeige der m�glichen Antworten
	 * einer Frage.
	 * 
	 * @param context
	 *            Context
	 * @param wrongAnswers
	 *            Antworten
	 */
	public QuestionAnswersAdapter(Context context, String[] wrongAnswers,
			String correctAnswer) {

		this.answers = new ArrayList<String>();
		this.answers.addAll(Arrays.asList(wrongAnswers));
		this.answers.add(correctAnswer);
		/* Antworten mischen */
		Collections.shuffle(this.answers);

		this.correctAnswerIndex = this.answers.indexOf(correctAnswer);

		this.mInflater = LayoutInflater.from(context);
	}

	/**
	 * Pr�ft, ob die abgegebene Antwort korrekt ist.
	 * 
	 * @return true, wenn korrekt
	 */
	public boolean isCorrectAnswer(int correctAnswerIndex) {
		return this.correctAnswerIndex == correctAnswerIndex;
	}

	/**
	 * Gibt die Anzahl der Antworten.
	 * 
	 * @return Anzahl
	 */
	@Override
	public int getCount() {
		return answers.size();
	}

	/**
	 * Gibt die ausgew�hlten Antwort.
	 * 
	 * @return Antwort
	 */
	@Override
	public String getItem(int position) {
		return answers.get(position);
	}

	/**
	 * Gibt die ID der ausgew�hlten Antwort.
	 * 
	 * @param position
	 *            Index der Antwort in der Liste
	 * @return ID
	 */
	@Override
	public long getItemId(int position) {
		return position;
	}

	/**
	 * Organiersiert die Anzeige in der grafischen Oberfl�che.
	 * 
	 * @param pos
	 *            Index der Tour in der Auflistung
	 * @param convertView
	 *            View
	 * @param parent
	 *            �bgeordnete Viewgruppe
	 */
	@Override
	public View getView(int pos, View convertView, ViewGroup parent) {
		ViewHolder holder;

		if (convertView == null) {

			convertView = mInflater
					.inflate(R.layout.question_answer_item, null);
			holder = new ViewHolder();
			holder.answer = (TextView) convertView.findViewById(R.id.q_answer);
			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.answer.setText((pos + 1) + ") " + this.answers.get(pos));

		return convertView;
	}

	/**
	 * Holder, der die GUI-Elemente zur Auflistung der Antworten enth�lt.
	 */
	static class ViewHolder {
		TextView answer;
	}
}
