package de.SpiritualTours.Utility;

public interface IntentConstants {

	/**
	 * Key zur �bergabe des Tour-Setting-Objekts.
	 */
	static final String TOUR_SETTINGS = "tourSettings";

	/**
	 * Key zur �bergabe des Tour-Setting-Objekts.
	 */
	static final String INSTRUCTIONS = "instructions";
	
	/**
	 * Key zur �bergabe einer Tour.
	 */
	static final String TOUR_NAME = "tourname";
	
	/**
	 * Key zur �bergabe einer Tour.
	 */
	static final String TOUR_ID = "tourID";
	
	/**
	 * Key zur �bergabe mehrerer Zielpunkte.
	 */
	static final String TARGETPOINTS = "targetpoints";
	
	/**
	 * Key zur �bergabe eines Zielpunkts.
	 */
	static final String TARGETPOINT = "targetpoint";
	
	/**
	 * Key zur �bergabe der Mindest-Zeit.
	 */
	static final String MIN_TIME = "minTime";
	
	/**
	 * Key zur �bergabe der Mindest-Distanz.
	 */
	static final String MIN_DISTANCE = "minDistance";
	
	/** Key f�r Ton an/aus. */
	static final String PLAY_SOUND = "playSound";
	
	/** Key f�r Vibtration an/aus. */
	static final String VIBRATE = "vibrate";
}
