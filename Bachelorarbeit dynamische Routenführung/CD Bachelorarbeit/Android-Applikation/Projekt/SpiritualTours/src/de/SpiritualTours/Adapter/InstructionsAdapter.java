package de.SpiritualTours.Adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import de.SpiritualTours.R;
import de.SpiritualTours.TourObjects.Instruction;

/**
 * Zur Anzeige der Wegbeschreibung. Der folgende Link hat bei der
 * Implementierung der Adapterklasse sehr geholfen:
 * http://geekswithblogs.net/bosuch
 * /archive/2011/01/31/android---create-a-custom-
 * multi-line-listview-bound-to-an.aspx.
 */
public class InstructionsAdapter extends BaseAdapter {

	/** Anweisungen der Wegbeschreibung. */
	private List<Instruction> instructions;

	/** LayoutInflater. */
	private LayoutInflater mInflater;

	/**
	 * Erzeugt einen InstructionsAdapter zur Anzeige der Wegbeschreibung.
	 * 
	 * @param context
	 *            Context
	 * @param instructions
	 *            Anweisungen der Wegbeschreibung
	 */
	public InstructionsAdapter(Context context, List<Instruction> instructions) {
		this.instructions = instructions;
		this.mInflater = LayoutInflater.from(context);
	}

	/**
	 * Gibt die Anzahl der Anweisungen.
	 * 
	 * @return Anzahl
	 */
	@Override
	public int getCount() {
		return instructions.size();
	}

	/**
	 * Gibt die ausgewählte Anweisung.
	 * 
	 * @return Anweisungen
	 */
	@Override
	public Instruction getItem(int position) {
		return instructions.get(position);
	}

	/**
	 * Gibt die ID der ausgewählten Anweisung.
	 * 
	 * @param position
	 *            Index der Anweisung in der Liste
	 * @return ID
	 */
	@Override
	public long getItemId(int position) {
		return position;
	}

	/**
	 * Organiersiert die Anzeige in der grafischen Oberfläche.
	 * 
	 * @param pos
	 *            Index der Anweisung in der Auflistung
	 * @param convertView
	 *            View
	 * @param parent
	 *            übgeordnete Viewgruppe
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;

		if (convertView == null) {

			convertView = mInflater.inflate(R.layout.instruction_item, null);
			holder = new ViewHolder();
			holder.instructionTextView = (TextView) convertView
					.findViewById(R.id.in_instruction);
			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.instructionTextView.setText(this.instructions.get(position)
				.getInstruction());

		return convertView;
	}

	/**
	 * Holder, der die GUI-Elemente zur Auflistung der Instruktionen enthält.
	 */
	static class ViewHolder {
		TextView instructionTextView;
	}
}
