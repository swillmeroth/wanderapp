package de.SpiritualTours;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ScaleBarOverlay;
import org.osmdroid.views.overlay.SimpleLocationOverlay;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import de.SpiritualTours.Exceptions.ConnectionException;
import de.SpiritualTours.Exceptions.NoTargetPointsFoundException;
import de.SpiritualTours.TourObjects.GeoPoint;
import de.SpiritualTours.TourObjects.TargetPoint;
import de.SpiritualTours.TourObjects.TargetPoint.TargetPointType;
import de.SpiritualTours.TourObjects.Tour;
import de.SpiritualTours.TourObjects.Tour.TourType;
import de.SpiritualTours.TourObjects.TourSettings;
import de.SpiritualTours.TourObjects.TourSettings.Locomotion;
import de.SpiritualTours.TourObjects.WayPoint;
import de.SpiritualTours.TourObjects.WayPoint.PointType;
import de.SpiritualTours.Utility.ActivityHelper;
import de.SpiritualTours.Utility.Geo;
import de.SpiritualTours.Utility.IntentConstants;
import de.SpiritualTours.Utility.Navigate;
import de.SpiritualTours.Utility.Navigate.TourState;
import de.SpiritualTours.Utility.TourSaver;
import de.SpiritualTours.Utility.ToursAdministrator;
import de.SpiritualTours.Utility.WebServiceConnection;
import de.SpiritualTours.View.Compass;
import de.SpiritualTours.View.MapOverlay.TargetPointsOverlay;
import de.SpiritualTours.View.MapOverlay.TourOverlay;

/**
 * Activity zur Navigation des Nutzers.
 */
public class NavigateActivity extends Activity implements LocationListener,
		SensorEventListener {

	/* ======================================================================= */
	/* Konstanten */
	/* ======================================================================= */
	/**
	 * Zeitliches Minimalintervall in Millisekunden f�r eine Aktualisierung der
	 * Position f�r Fu�g�nger.
	 */
	private final static int LOCATION_MIN_TIME_PEDESTRIAN = 1000;
	/**
	 * Zeitliches Minimalintervall in Millisekunden f�r eine Aktualisierung der
	 * Position f�r Fahrradfahrer.
	 */
	private final static int LOCATION_MIN_TIME_BIKER = 250;
	/**
	 * Zeitliches Minimalintervall in Millisekunden f�r eine Aktualisierung der
	 * Position f�r Autofahrer.
	 */
	private final static int LOCATION_MIN_TIME_DRIVER = 75;
	/** Mindestabstand in Metern f�r eine Aktualisierung der Position. */
	private final static int LOCATION_MIN_DISTANCE = 1;

	/** Meldung f�r: Route vom Server erhalten. */
	private final static int HANDLER_TARGETPOINT_IS_SKIPPED = 0;
	/** Meldung f�r: Tour beenden weil kein Zielpunkt mehr und kein Rundweg. */
	private final static int HANDLER_CANCEL_SKIP_TARGETPOINT = 1;
	/** Meldung f�r: Verbindungsfehler. */
	private final static int HANDLER_CLIENT_PROTOCOL_EXCEPTION = 2;
	/** Meldung f�r: keine Erstellung der Tour m�glich. */
	private final static int HANDLER_JSON_EXCEPTION = 3;
	/** Meldung f�r: Verbindungsfehler. */
	private final static int HANDLER_IO_EXCEPTION = 4;
	/** Meldung f�r: Verbindungsfehler. */
	private final static int HANDLER_CONNECTION_TO_ORS_EXCEPTION = 5;

	/* ======================================================================= */
	/* Instanzvariablen */
	/* ======================================================================= */
	/** Tour. */
	private Tour tour;
	/** Tour-Einstellungen. */
	private TourSettings tourSettings;
	/** aktuelle Himmelsrichtung */
	private float azimuth;
	/** aktuelle Position des Users */
	private GeoPoint currentLocation;
	/** Navigation. */
	private Navigate navigate;
	/** N�chster Wegpunkt. */
	private WayPoint nextWayPoint;
	/** N�chster Zielpunkt. */
	private TargetPoint nextTargetPoint;
	/** Soll Sound-Benachrichtigung abspielen? */
	private boolean playSound;
	/** Soll Mobil-Ger�t vibrieren? */
	private boolean vibrate;

	/* --------------------------------------------------------------------- */

	/** Karte. */
	private MapView mapView;
	/** Skala auf der Karte. */
	private ScaleBarOverlay scaleBarOverlay;
	/** Position auf der Karte. */
	private SimpleLocationOverlay locationOverlay;
	/** Wegstrecke auf der Karte. */
	private TourOverlay pathOverlay;
	/** Zielpunkte auf der Karte. */
	private TargetPointsOverlay targetPointsOverlay;
	/** Pfeil, um die Richtung anzuzeigen. */
	private Compass compass;
	/** Textfeld, um den n�chsten Zielpunkt anzuzeigen. */
	private TextView nextTargetPointTextView;
	/** Textfeld, f�r die Distanz zum n�chsten Zielpunkt anzuzeigen. */
	private TextView distanceToNextTargetPointTextView;
	/** Info-Textfeld f�r die zur�ckgelegte Strecke. */
	private TextView distanceTextView;
	/** Sensor-Manager. */
	private SensorManager sensorManager;
	/** Location Manager */
	private LocationManager locationManager;

	/* --------------------------------------------------------------------- */

	/**
	 * Dialog zur Anzeige von Prozessen. Hier f�r Ermittlung der Position und
	 * Erstellung der Touren.
	 */
	private ProgressDialog dialog;

	/**
	 * Handler, um Nachrichten des Threads in der Activity bearbeiten zu k�nnen.
	 */
	private final Handler threadHandler = new Handler() {

		/**
		 * Handlet die Nachrichten des Threads.
		 * 
		 * @param msg
		 *            Nachricht des Threads
		 */
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case HANDLER_TARGETPOINT_IS_SKIPPED:
				nextTargetPointTextView.setText(nextTargetPoint.getName()
						+ " \n"
						+ tour.getTargetPointNumberOf(NavigateActivity.this,
								nextTargetPoint));
				updateDisplay();

				mapView.getOverlays().remove(pathOverlay);
				setNewPathInMap();

				mapView.getOverlays().remove(targetPointsOverlay);
				setTargetPointsInMap();

				/* Person an aktuelle Position setzen */
				mapView.getOverlays().remove(locationOverlay);
				setPerson();
				updateMap();

				/* Location Manager wieder initialisieren */
				initLocationManager();

				dialog.dismiss();
				break;
			case HANDLER_CANCEL_SKIP_TARGETPOINT:
				dialog.dismiss();
				break;
			case HANDLER_CLIENT_PROTOCOL_EXCEPTION:
				dialog.dismiss();
				showMessageBox(getString(R.string.error_caption),
						getString(R.string.error_no_connection_to_server) + " "
								+ getString(R.string.error_no_route));
				break;
			case HANDLER_JSON_EXCEPTION:
				dialog.dismiss();
				showMessageBox(getString(R.string.error_caption),
						getString(R.string.error_unknown) + " "
								+ getString(R.string.error_no_route));
				break;
			case HANDLER_IO_EXCEPTION:
				dialog.dismiss();
				showMessageBox(getString(R.string.error_caption),
						getString(R.string.error_no_connection_to_server) + " "
								+ getString(R.string.error_no_route));
				break;
			case HANDLER_CONNECTION_TO_ORS_EXCEPTION:
				dialog.dismiss();
				showMessageBox(getString(R.string.error_caption),
						getString(R.string.error_no_connection_to_ors) + " "
								+ getString(R.string.error_no_route));
				break;
			}
		}
	};

	/* ======================================================================= */
	/* OnCreate */
	/* ======================================================================= */
	/**
	 * Wird bei der Erzeugung der Activity aufgerufen.
	 * 
	 * @param savedInstanceState
	 *            Bundle
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.navigate);

		/* Komponenten initialisieren */
		init();
		setInfos();

		/* Information */
		ActivityHelper.showMessageToast(this, getString(R.string.n_start_info));
	}

	@Override
	protected void onResume() {
		super.onResume();

		/* Location Manager initialisieren */
		initLocationManager();

		/* Sensor zur Bestimmung der Himmelsrichtung registrieren. */
		this.sensorManager.registerListener(this,
				sensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION),
				SensorManager.SENSOR_DELAY_NORMAL);
	}

	/**
	 * Wird aufgerufen, wenn eine teilweise verdeckte Activity wieder
	 * vollst�ndig angezeigt wird.
	 */
	@Override
	protected void onRestart() {

		super.onRestart();

		if (this.navigate.getTourState().equals(TourState.END_OF_TOUR_REACHED)) {

			quitTour();

		} else {
			/* n�chster Zielpunkt wird angezeigt */
			this.nextTargetPointTextView.setText(this.nextTargetPoint.getName()
					+ " \n"
					+ tour.getTargetPointNumberOf(NavigateActivity.this,
							nextTargetPoint));

			/* Entfernung zum n�chsten Zielpunkt anzeigen */
			this.distanceToNextTargetPointTextView
					.setText(getString(R.string.n_distance_to_next_targetPoint)
							+ " "
							+ this.navigate
									.getDistanceToNextTargetPoint(this.currentLocation));
		}
	}

	/**
	 * Wird aufgerufen, wenn die Activity von einer anderen Acitivity �berlagert
	 * wird. Die Activity ist nach dem Aufruf dieser Methode inaktiv.
	 */
	@Override
	protected void onPause() {
		super.onPause();
		this.locationManager.removeUpdates(this);
		this.sensorManager.unregisterListener(this);
	}

	/* ======================================================================= */
	/* Initialisieren */
	/* ======================================================================= */
	/**
	 * Initialisiert die GUI-Kompenenten, den Location Listener und den Sensor
	 * Listener.
	 */
	private void init() {

		/* Tour setzen */
		this.tour = ToursAdministrator.getInstance().getTour(
				this.getIntent().getExtras().getInt(IntentConstants.TOUR_ID));

		/* Navigation */
		this.navigate = Navigate.getInstance();

		/* Einstellung setzen */
		this.tourSettings = ToursAdministrator.getInstance().getTourSettings();

		/* GUI-Komponenten initialisieren */
		this.compass = (Compass) findViewById(R.id.n_compass);
		this.nextTargetPointTextView = (TextView) findViewById(R.id.n_next_targetpoint);
		this.distanceTextView = (TextView) findViewById(R.id.n_current_distance);
		this.distanceToNextTargetPointTextView = (TextView) findViewById(R.id.n_next_targetpoint_distance);

		/* Location Listener initialisieren */
		this.locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

		/* Sensor Listener initialisierien */
		this.sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);

		/* Positionen bestimmen */
		Location location = this.locationManager
				.getLastKnownLocation(LocationManager.GPS_PROVIDER);
		this.currentLocation = new GeoPoint(location.getLatitude(),
				location.getLongitude(), location.getAltitude());

		this.nextWayPoint = this.navigate.getNextWayPoint();
		this.nextTargetPoint = this.tour.getNextTargetPoint(this.nextWayPoint);

		/* Benachrichtigungseinstellungen aus Shared Preferences laden */
		SharedPreferences prefs = this.getSharedPreferences("PREFERECES_NAME",
				Activity.MODE_WORLD_WRITEABLE);
		this.playSound = prefs.getBoolean(IntentConstants.PLAY_SOUND, true);
		this.vibrate = prefs.getBoolean(IntentConstants.VIBRATE, true);

		/* Map */
		initMap();
	}

	/**
	 * Initialisierung des Location Managers.
	 */
	private void initLocationManager() {
		/* Location Manager initialisieren */
		int locationMinTime = this.tourSettings.getLocomotion().equals(
				Locomotion.HOOF_IT) ? LOCATION_MIN_TIME_PEDESTRIAN
				: this.tourSettings.getLocomotion().equals(Locomotion.BIKE) ? LOCATION_MIN_TIME_BIKER
						: LOCATION_MIN_TIME_DRIVER;
		this.locationManager.requestLocationUpdates(
				LocationManager.GPS_PROVIDER, locationMinTime,
				LOCATION_MIN_DISTANCE, this);
	}

	/**
	 * Initialisieren der Map.
	 */
	private void initMap() {

		this.mapView = (MapView) this.findViewById(R.id.n_mapview);
		this.mapView.setTileSource(TileSourceFactory.MAPNIK);
		this.mapView.setBuiltInZoomControls(true);
		this.mapView.setMultiTouchControls(true);

		/* Zoom auf die Karten setzen */
		this.mapView.getController().setZoom(14);

		/* Skala setzen */
		this.scaleBarOverlay = new ScaleBarOverlay(this);
		this.mapView.getOverlays().add(scaleBarOverlay);

		/* Pfad setzen */
		setPathInMap();
		/* Zielpunkte setzen */
		setTargetPointsInMap();
		/* Person an aktuelle Position setzen */
		setPerson();
		updateMap();
	}

	/**
	 * Person setzten.
	 */
	private void setPerson() {
		this.locationOverlay = new SimpleLocationOverlay(this);
		this.mapView.getOverlays().add(locationOverlay);
	}

	/**
	 * Setzt den Tourpfad in der Map.
	 */
	private void setPathInMap() {

		this.pathOverlay = new TourOverlay(Color.BLUE, this);

		/* Setzen der Wegpunkte */
		for (WayPoint wayPoint : this.tour.getWayPoints()) {

			double latitude = wayPoint.getGeoPoint().getLatitude();
			double longitude = wayPoint.getGeoPoint().getLongitude();

			this.pathOverlay.addPoint(new org.osmdroid.util.GeoPoint(latitude,
					longitude));
		}

		this.mapView.getOverlays().add(this.pathOverlay);
		this.mapView.invalidate();
	}

	/**
	 * Setzt die Zielpunkte in der Map.
	 */
	private void setTargetPointsInMap() {
		/* Zielpunkt */
		this.targetPointsOverlay = new TargetPointsOverlay(this,
				this.tour.getTargetPoints());
		this.mapView.getOverlays().add(this.targetPointsOverlay);

		this.mapView.invalidate();
	}

	/**
	 * Setzt einen neuen Pfad in die Map.
	 */
	private void setNewPathInMap() {

		this.pathOverlay.clearPath();

		/* Setzen der Wegpunkte */
		for (WayPoint wayPoint : this.navigate.getWayPoints()) {

			double latitude = wayPoint.getGeoPoint().getLatitude();
			double longitude = wayPoint.getGeoPoint().getLongitude();

			this.pathOverlay.addPoint(new org.osmdroid.util.GeoPoint(latitude,
					longitude));
		}

		this.mapView.getOverlays().add(this.pathOverlay);
		this.mapView.invalidate();
	}

	/**
	 * Aktualisiert die Karte. Setzt die Person an die momentane Position und
	 * verschiebt die Karte so, dass die Person in der Mitte steht.
	 */
	private void updateMap() {

		org.osmdroid.util.GeoPoint location = new org.osmdroid.util.GeoPoint(
				this.currentLocation.getLatitude(),
				this.currentLocation.getLongitude());

		/* Startposition (Person) in Karte setzen */
		this.locationOverlay.setLocation(location);

		/* Mittelpunkt setzen */
		this.mapView.getController().setCenter(location);
	}

	/**
	 * Setzen der Informationen.
	 */
	private void setInfos() {

		this.nextTargetPointTextView.setText(this.nextTargetPoint.getName()
				+ " \n"
				+ tour.getTargetPointNumberOf(NavigateActivity.this,
						nextTargetPoint));

		/* Entfernung zum n�chsten Zielpunkt anzeigen */
		this.distanceToNextTargetPointTextView
				.setText(getString(R.string.n_distance_to_next_targetPoint)
						+ " "
						+ this.navigate
								.getDistanceToNextTargetPoint(this.currentLocation));

		/* zur�ckgelegte Distanz setzen */
		this.distanceTextView.setText(getString(R.string.n_traveled_distance)
				+ " " + this.navigate.getTraveledDistance(this.currentLocation)
				+ " " + getString(R.string.n_distance_of) + " "
				+ this.tour.getEstimatedTourLength() + " km");
	}

	/* ======================================================================= */
	/* Location-Listener */
	/* ======================================================================= */
	/**
	 * Wird aufgerufen, wenn die Position sich ge�ndert hat.
	 * 
	 * @param location
	 *            Location
	 */
	@Override
	public void onLocationChanged(Location location) {

		/* aktuelle Position */
		this.currentLocation = new GeoPoint(location.getLatitude(),
				location.getLongitude(), location.getAltitude());

		/* Map aktualisieren */
		updateMap();

		/* GUI aktualisieren */
		updateDisplay();

		/* Navigieren */
		navigate();
	}

	@Override
	public void onProviderDisabled(String provider) {
		/* f�r diese Zwecke nicht von Bedeutung */
	}

	@Override
	public void onProviderEnabled(String provider) {
		/* f�r diese Zwecke nicht von Bedeutung */
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		/* f�r diese Zwecke nicht von Bedeutung */
	}

	/* ======================================================================= */
	/* Sensor-Listener */
	/* ======================================================================= */
	public void onAccuracyChanged(Sensor arg0, int arg1) {
		/* nicht verwendet */
	}

	public void onSensorChanged(SensorEvent event) {
		this.azimuth = event.values[0];
		rotateCompass();
	}

	/* ======================================================================= */
	/* Men� */
	/* ======================================================================= */
	/**
	 * Anzeigen des Hauptmenues.
	 */
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.navigation_menu, menu);
		return super.onCreateOptionsMenu(menu);
	}

	/**
	 * Event-Handling des Menues.
	 * 
	 * @param item
	 *            Menuepunkt, der gewaehlt wurde.
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		/* Auswahl im Men� auswerten */
		switch (item.getItemId()) {
		case R.id.menu_n_skip_targetpoint:
			onClickSkipTargetPoint();
			return true;
		case R.id.menu_n_instructions:
			onClickInstructions();
			return true;
		case R.id.menu_n_volume:
			onClickVolume(item);
			return true;
		case R.id.menu_n_vibration:
			onClickVibration(item);
			return true;
		case R.id.menu_n_help:
			onClickHelp();
			return true;
		case R.id.menu_n_close_navigation:
			onCloseNavigation();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/**
	 * �berspringen des n�chsten Zielpunkts.
	 */
	protected void onClickSkipTargetPoint() {

		this.dialog = ProgressDialog.show(this, getString(R.string.wait),
				getString(R.string.n_calculate_new_route), true, false);

		showMessageBoxSkipTargetPoint();
	}

	/**
	 * Anzeigen der Wegbeschreibung.
	 */
	protected void onClickInstructions() {
		Intent instructions = new Intent(this, InstructionsActivity.class);
		instructions.putParcelableArrayListExtra(IntentConstants.INSTRUCTIONS,
				(ArrayList<? extends Parcelable>) this.tour.getInstructions());
		startActivity(instructions);
	}

	/**
	 * Ton an bzw. ausschalten.
	 */
	protected void onClickVolume(MenuItem item) {

		/* Ton umstellen (an bzw. aus) */
		this.playSound = !this.playSound;

		/* Einstellungen sichern */
		SharedPreferences prefs = this.getSharedPreferences("PREFERECES_NAME",
				Activity.MODE_WORLD_WRITEABLE);
		prefs.edit().putBoolean(IntentConstants.PLAY_SOUND, this.playSound);

		/* Icon im Menu setzen */
		item.setIcon(this.playSound ? R.drawable.menu_sound
				: R.drawable.menu_mute);
	}

	/**
	 * Vibrationsalarm an bzw. ausschalten.
	 */
	protected void onClickVibration(MenuItem item) {

		/* Ton umstellen (an bzw. aus) */
		this.vibrate = !this.vibrate;

		/* Einstellungen sichern */
		SharedPreferences prefs = this.getSharedPreferences("PREFERECES_NAME",
				Activity.MODE_WORLD_WRITEABLE);
		prefs.edit().putBoolean(IntentConstants.VIBRATE, this.vibrate);

		/* Icon im Menu setzen */
		item.setIcon(this.vibrate ? R.drawable.menu_vibrate_true
				: R.drawable.menu_vibrate_false);
	}

	/**
	 * Anzeigen der Hilfe.
	 */
	protected void onClickHelp() {
		showMessageBox(getString(R.string.menu_help),
				getString(R.string.n_help));
	}

	/**
	 * Anzeigen der Hilfe.
	 */
	protected void onCloseNavigation() {
		/* MessageBox -> Ende der Tour erreicht */
		showMessageBoxCloseNavigation();
	}

	/* ======================================================================= */
	/* Hilfsmethoden */
	/* ======================================================================= */
	/**
	 * Aktualisiert die Angaben auf dem Display.
	 */
	private void updateDisplay() {

		/* Kompass ausrichten */
		rotateCompass();

		/* Entfernung zum n�chsten Zielpunkt anzeigen */
		this.distanceToNextTargetPointTextView
				.setText(getString(R.string.n_distance_to_next_targetPoint)
						+ " "
						+ this.navigate
								.getDistanceToNextTargetPoint(this.currentLocation));

		/* zur�ckgelegte Distanz setzen */
		this.distanceTextView.setText(getString(R.string.n_traveled_distance)
				+ " " + this.navigate.getTraveledDistance(this.currentLocation)
				+ " " + getString(R.string.n_distance_of) + " "
				+ this.tour.getEstimatedTourLength() + " km");
	}

	/**
	 * Drehen des Kompasses.
	 */
	private void rotateCompass() {
		this.compass.setDirection(
				azimuth,
				Geo.getAngle(this.currentLocation,
						this.nextTargetPoint.getGeoPoint()));
	}

	/**
	 * Navigier-Methode. Pr�ft, ob ein Wegpunkt erreicht worden ist, falls ja,
	 * werden entsprechende Anweisungen durchgef�hrt.
	 */
	private void navigate() {

		/* Navigieren */
		TourState state = this.navigate
				.getIsNextWayPointReached(this.currentLocation);

		if (state.equals(TourState.WAY_POINT_REACHED)) {

//			ActivityHelper.notifyTargetPoint(this, true, true);
			this.nextWayPoint = this.navigate.getNextWayPoint();

		} else if (state.equals(TourState.TARGET_POINT_REACHED)) {

			this.nextWayPoint = this.navigate.getNextWayPoint();

			/* Benutzer benachrichtigen */
			ActivityHelper
					.notifyTargetPoint(this, this.playSound, this.vibrate);

			if (this.tourSettings.getIsPaperChase()) {
				askQuestion();
			} else {
				showTargetPoint();
			}

		} else if (state.equals(TourState.END_OF_TOUR_REACHED)) {

			/* Benutzer benachrichtigen */
			ActivityHelper
					.notifyTargetPoint(this, this.playSound, this.vibrate);

			if (this.tour.getLastWayPoint().getPointType()
					.equals(PointType.TARGET_POINT)) {

				if (this.tourSettings.getIsPaperChase()) {
					askQuestion();
				} else {
					showTargetPoint();
				}
			} else {

				quitTour();
			}
		}
	}

	/**
	 * Stellen der Frage.
	 */
	private void askQuestion() {

		/* Intent erstellen */
		Intent aksQuestion = new Intent(this, QuestionerActivity.class);
		aksQuestion.putExtra(IntentConstants.TARGETPOINT, this.nextTargetPoint);

		/* n�chster Zielpunkt wird bestimmt */
		this.nextTargetPoint = this.tour
				.getNextTargetPoint(this.nextTargetPoint);

		if (this.nextTargetPoint == null) {
			this.nextTargetPoint = new TargetPoint(
					getString(R.string.start_end), this.tour.getLastWayPoint(),
					TargetPointType.END_POINT);
		}

		/* erreichter Zielpunkt wird angezeigt */
		startActivity(aksQuestion);
	}

	/**
	 * Anzeigen des Zielpunktes. Nach Anzeigen des Zielpunktes geht es mit der
	 * onResume-Methode weiter.
	 */
	private void showTargetPoint() {

		/* Intent erstellen */
		Intent targetPointDetails = new Intent(this, TargetPointActivity.class);
		targetPointDetails.putExtra(IntentConstants.TARGETPOINT,
				this.nextTargetPoint);

		/* n�chster Zielpunkt wird bestimmt */
		this.nextTargetPoint = this.tour
				.getNextTargetPoint(this.nextTargetPoint);

		if (this.nextTargetPoint == null) {
			this.nextTargetPoint = new TargetPoint(
					getString(R.string.start_end), this.tour.getLastWayPoint(),
					TargetPointType.END_POINT);
		}

		/* erreichter Zielpunkt wird angezeigt */
		startActivity(targetPointDetails);
	}

	/**
	 * Beendet die Tour.
	 */
	private void quitTour() {
		/* MessageBox -> Ende der Tour erreicht */
		showMessageBoxTourEnd(getString(R.string.n_tour_end_congratulations),
				getString(R.string.n_tour_end_info));
	}

	/* ======================================================================= */
	/* MSG-Boxes */
	/* ======================================================================= */
	/**
	 * Zeigte eine Info-Fenster mit einem Ok-Button zum Schlie�en.
	 * 
	 * @param context
	 *            Context
	 * @param title
	 *            Titel
	 * @param message
	 *            Meldung
	 */
	public void showMessageBox(String title, String message) {

		AlertDialog msgBox = new AlertDialog.Builder(this).create();
		msgBox.setTitle(title);
		msgBox.setMessage(message);
		msgBox.setButton(getString(R.string.button_ok),
				new android.content.DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						/* Fenster wird geschlossen */
					}
				});
		msgBox.show();
	}

	/**
	 * Zeigte eine Info-Fenster mit einem Ok-Button zum Schlie�en.
	 * 
	 * @param context
	 *            Context
	 * @param title
	 *            Titel
	 * @param message
	 *            Meldung
	 */
	public void showMessageBoxTourEnd(String title, String message) {

		AlertDialog msgBox = new AlertDialog.Builder(this).create();
		msgBox.setTitle(title);
		msgBox.setMessage(message);
		msgBox.setButton(getString(R.string.button_ok),
				new android.content.DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						if (tour.getTargetPoints().isEmpty()) {
							startActivity(new Intent(NavigateActivity.this,
									StartActivity.class));
						} else {
							showMessageBoxSaveTour();
						}
					}
				});
		msgBox.show();
	}

	/**
	 * Zeigte eine Info-Fenster mit einem Ja- und einem Nein-Button zum
	 * �berspringen eines Zielpunkts.
	 * 
	 * @param context
	 *            Context
	 * @param title
	 *            Titel
	 * @param message
	 *            Meldung
	 * @retrun true, wenn ja geklickt wurde
	 */
	public void showMessageBoxSkipTargetPoint() {

		AlertDialog msgBox = new AlertDialog.Builder(this).create();
		msgBox.setTitle(getString(R.string.n_skip_targetpoint_caption));
		msgBox.setMessage(getString(R.string.n_skip_targetpoint));

		msgBox.setButton(getString(R.string.answer_yes),
				new android.content.DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {

						if (tour.getTourType().equals(TourType.ROUTE)
								&& tour.getNextTargetPoint(nextTargetPoint) == null) {
							navigate.skipNextTargetPoint(nextTargetPoint);
							quitTour();
						} else {
							locationManager
									.removeUpdates(NavigateActivity.this);

							/*
							 * �berspringen des Zielpunktes. Ist der zu
							 * �berspringende Zielpunkt nicht weiter als 100m
							 * entfernt, wird die Route nicht neu berechnet. Ist
							 * der zu �berspringende Zielpunkt weiter als 100m
							 * entfernt, verbindet sich die Applikation mit dem
							 * Server zur Neubestimmung des Weges zum neuen
							 * n�chsten Zielpunkt.
							 */
							new SkipTargetPointThread(
									threadHandler,
									Geo.getDistance(currentLocation,
											nextTargetPoint.getGeoPoint()) > 0.1)
									.start();
						}
					}
				});
		msgBox.setButton2(getString(R.string.answer_no),
				new android.content.DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						threadHandler
								.sendEmptyMessage(HANDLER_CANCEL_SKIP_TARGETPOINT);
					}
				});
		msgBox.show();
	}

	/**
	 * Zeigte eine Info-Fenster mit einem Ja- und einem Nein-Button zum
	 * Speichern der Tour.
	 * 
	 * @param context
	 *            Context
	 * @param title
	 *            Titel
	 * @param message
	 *            Meldung
	 * @retrun true, wenn ja geklickt wurde
	 */
	public void showMessageBoxCloseNavigation() {

		AlertDialog msgBox = new AlertDialog.Builder(this).create();
		msgBox.setTitle(getString(R.string.menu_finish_navigation));
		msgBox.setMessage(getString(R.string.n_finish_navigation));

		msgBox.setButton(getString(R.string.answer_yes),
				new android.content.DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						/* M�glichkeit, Tour zu speichern evtl. anzubieten. */
						if (tour.getTargetPoints().isEmpty()) {
							startActivity(new Intent(NavigateActivity.this,
									StartActivity.class));
						} else {
							showMessageBoxSaveTour();
						}
					}
				});
		msgBox.setButton2(getString(R.string.answer_no),
				new android.content.DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						/* keine Aktion */
					}
				});
		msgBox.show();
	}

	/**
	 * Zeigte eine Info-Fenster mit einem Ja- und einem Nein-Button zum
	 * Speichern der Tour.
	 * 
	 * @param context
	 *            Context
	 * @param title
	 *            Titel
	 * @param message
	 *            Meldung
	 * @retrun true, wenn ja geklickt wurde
	 */
	public void showMessageBoxSaveTour() {

		AlertDialog msgBox = new AlertDialog.Builder(this).create();
		msgBox.setTitle(getString(R.string.n_save_tour_title));
		msgBox.setMessage(getString(R.string.n_save_tour));

		msgBox.setButton(getString(R.string.answer_yes),
				new android.content.DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						TourSaver.saveTour(NavigateActivity.this, tour);
						/* und StartActivity aufrufen */
						startActivity(new Intent(NavigateActivity.this,
								StartActivity.class));
					}
				});
		msgBox.setButton2(getString(R.string.answer_no),
				new android.content.DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						/* StartActivity aufrufen */
						startActivity(new Intent(NavigateActivity.this,
								StartActivity.class));
					}
				});
		msgBox.show();
	}

	/* ======================================================================= */
	/* Thread */
	/* ======================================================================= */
	/**
	 * Thread zur Ermittlung einer neuen Route zum n�chste Zielpunkt.
	 */
	public class SkipTargetPointThread extends Thread {

		/** Handler zum Benachrichtigen der Activity */
		private Handler handler;

		/** Neue Route berchnen ? = true. */
		private boolean calculateNewRoute;

		/**
		 * Erzeugt einen Thread zur Ermittlung der Touren.
		 * 
		 * @param handler
		 *            Handler zum Benachrichtigen der Activity
		 */
		public SkipTargetPointThread(Handler handler, boolean calculateNewRoute) {
			super();
			this.handler = handler;
			this.calculateNewRoute = calculateNewRoute;
		}

		/**
		 * Ausf�hren des Threads.
		 */
		public void run() {

			/* zu �berspringenden Zielpunkt l�schen */
			TargetPoint nextTP = tour.getNextTargetPoint(nextTargetPoint);
			navigate.skipNextTargetPoint(nextTargetPoint);
			nextTargetPoint = nextTP;
			/*
			 * Wenn kein Zielpunkt mehr vorhanden ist und Tour eine Route ist,
			 * dann beenden
			 */
			if (nextTargetPoint == null) {
				nextTargetPoint = new TargetPoint(
						getString(R.string.start_end), tour.getLastWayPoint(),
						TargetPointType.END_POINT);

			}

			/* Route evt. neu berechnen */
			if (calculateNewRoute) {
				calculateNewRoute(nextTP);
			}

			/* Handler benachrichtigen, dass neue Route berechnet wurde */
			this.handler.sendEmptyMessage(HANDLER_TARGETPOINT_IS_SKIPPED);

			tour.setWayPoints(navigate.getWayPoints());
		}

		/**
		 * Neuberechnung der Route.
		 * 
		 * @param nextTP
		 *            N�chster Zielpunkt
		 * @throws ClientProtocolException
		 *             Verbindungsfehler
		 * @throws JSONException
		 *             Parserfehler
		 * @throws IOException
		 *             Verbindungsfehler
		 * @throws NoTargetPointsFoundException
		 *             keine Zielpunkte vorhanden
		 * @throws ConnectionException
		 */
		private void calculateNewRoute(TargetPoint nextTP) {

			try {
				/* aktuelle Position ist Start-Position der neuen Route */
				tourSettings.setStartPoint(currentLocation);

				/* neue Route bestimmen */
				Tour route = WebServiceConnection.getRoute(tourSettings,
						nextTargetPoint);

				/* Wegpunkte ersetzen */
				navigate.replaceWayPoints(route.getWayPoints(), nextTP);

				/* neue Zeit und Streckenl�nge berechnen. */
				tour.setEstimatedTourLength();
				tour.setEstimatedDuration(tourSettings.getSpeed());
				tour.addInstructions(
						getString(R.string.in_subroute_instructions),
						route.getInstructions());

				/* n�chster Wegpunkt neu bestimmen */
				nextWayPoint = navigate.getNextWayPoint();
			} catch (ClientProtocolException e) {
				/* Fehler-Meldung anzeigen -> Verbindung mit dem Server. */
				this.handler
						.sendEmptyMessage(HANDLER_CLIENT_PROTOCOL_EXCEPTION);
			} catch (JSONException e) {
				this.handler.sendEmptyMessage(HANDLER_JSON_EXCEPTION);
			} catch (IOException e) {
				this.handler.sendEmptyMessage(HANDLER_IO_EXCEPTION);
			} catch (ConnectionException e) {
				this.handler
						.sendEmptyMessage(HANDLER_CONNECTION_TO_ORS_EXCEPTION);
			}
		}
	}
}
