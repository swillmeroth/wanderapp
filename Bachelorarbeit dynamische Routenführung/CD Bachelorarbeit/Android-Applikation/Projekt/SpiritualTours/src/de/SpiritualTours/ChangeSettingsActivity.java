package de.SpiritualTours;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import de.SpiritualTours.Utility.IntentConstants;
import de.SpiritualTours.TourObjects.Question.LevelOfDifficulty;
import de.SpiritualTours.TourObjects.TargetPoint.TargetPointType;
import de.SpiritualTours.TourObjects.TourSettings;
import de.SpiritualTours.TourObjects.TourSettings.HowGood;
import de.SpiritualTours.TourObjects.TourSettings.Locomotion;
import de.SpiritualTours.Utility.ActivityHelper;
import de.SpiritualTours.Utility.ToursAdministrator;

/**
 * Activity auf die der Benutzer die Einstellungen zur Tour vornehmen kann.
 */
public class ChangeSettingsActivity extends Activity implements IntentConstants {

	/* ======================================================================= */
	/* Konstanten */
	/* ======================================================================= */
	/**
	 * Zeit-Startwert in Minuten.
	 */
	private static final int TIME_START = 30;

	/**
	 * Zeit-Endwert (Anzahl der Stunden * Minuten).
	 */
	private static final int TIME_END = 8 * 60;

	/**
	 * Zeitschrittweite in Minuten.
	 */
	private static final float TIME_STEP = 30;

	/**
	 * Zeitschrittweite f�r Seekbar.
	 */
	private static final float SEEKBAR_STEP = 100 / (TIME_END / TIME_STEP - 1);

	/* ======================================================================= */
	/* Instanzvariablen */
	/* ======================================================================= */

	/** Einstellungen zur Tour. */
	private TourSettings tourSettings;

	/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

	/** RadioButton zu Fu� */
	private RadioButton hoofItRadioButton;

	/** RadioButton mit dem Fahrrad */
	private RadioButton bikeRadioButton;

	/** RadioButton mit dem Auto */
	private RadioButton carRadioButton;

	/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

	/** TextView f�r Art der Fortbewegung */
	private TextView howGoodTextView;

	/** RadioButton langsam */
	private RadioButton slowRadioButton;

	/** RadioButton normal */
	private RadioButton normalRadioButton;

	/** RadioButton schnell */
	private RadioButton fastRadioButton;

	/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

	/** Text-Anzeige f�r: "Wie lange?" */
	private TextView howLongTextView;

	/** Seek Bar f�r die Frage: "Wie lange wollen Sie unterwegs sein?". */
	private SeekBar howLongSeekBar;

	/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

	/** Button f�r Zielpunkttyen hinzuf�gen/l�schen. */
	private ImageButton addTargePointTypesButton;

	/** ausgew�hlte Zielpunkttypen. */
	private TextView targetPointsTextView;

	/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

	/** CheckBox f�r Schnitzeljagd-Modus. */
	private CheckBox paperChaseCheckBox;

	/** TextView f�r Schnitzeljagd-Modus. */
	private TextView levelOfDifficultyTextView;

	/** Schwierigkeitsgrad leicht */
	private RadioButton easyRadioButton;

	/** Schwierigkeitsgrad schwer */
	private RadioButton middleRadioButton;

	/** Schwierigkeitsgrad mittel */
	private RadioButton difficultRadioButton;

	/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

	/** Button Abbrechen */
	private Button cancelButton;

	/** Button Ok */
	private Button okButton;

	/* ======================================================================= */
	/* OnCreate */
	/* ======================================================================= */
	/**
	 * Called when the activity is first created.
	 * 
	 * @param savedInstanceState
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.change_settings);

		this.tourSettings = ToursAdministrator.getInstance().getTourSettings();
		init();
		setSettings();
	}

	/* ======================================================================= */
	/* Initialisieren */
	/* ======================================================================= */
	/**
	 * Initialisieren.
	 */
	private void init() {

		this.howGoodTextView = (TextView) findViewById(R.id.cs_text_how_good_are_you_traveling);
		this.hoofItRadioButton = (RadioButton) findViewById(R.id.cs_radio_hoof_it);
		this.bikeRadioButton = (RadioButton) findViewById(R.id.cs_radio_by_bike);
		this.carRadioButton = (RadioButton) findViewById(R.id.cs_radio_by_car);
		this.hoofItRadioButton.setOnClickListener(radioGroupLocomotionListener);
		this.bikeRadioButton.setOnClickListener(radioGroupLocomotionListener);
		this.carRadioButton.setOnClickListener(radioGroupLocomotionListener);

		this.slowRadioButton = (RadioButton) findViewById(R.id.cs_radio_slow);
		this.normalRadioButton = (RadioButton) findViewById(R.id.cs_radio_normal);
		this.fastRadioButton = (RadioButton) findViewById(R.id.cs_radio_fast);

		this.howLongSeekBar = (SeekBar) findViewById(R.id.cs_seek_bar_how_long);
		this.howLongSeekBar.setOnSeekBarChangeListener(sbHowLongListener);
		this.howLongTextView = (TextView) findViewById(R.id.progress);

		this.targetPointsTextView = (TextView) findViewById(R.id.cs_target_points);
		this.addTargePointTypesButton = (ImageButton) findViewById(R.id.cs_bn_target_point_types);
		this.addTargePointTypesButton
				.setOnClickListener(bTargetPointTypesListener);

		this.paperChaseCheckBox = (CheckBox) findViewById(R.id.cs_checkbox_paper_chase);
		this.paperChaseCheckBox.setOnClickListener(levelOfDifficultyListener);

		this.levelOfDifficultyTextView = (TextView) findViewById(R.id.cs_text_level_of_difficulty);
		this.easyRadioButton = (RadioButton) findViewById(R.id.cs_radio_easy);
		this.middleRadioButton = (RadioButton) findViewById(R.id.cs_radio_middle);
		this.difficultRadioButton = (RadioButton) findViewById(R.id.cs_radio_difficult);

		this.cancelButton = (Button) findViewById(R.id.cs_bn_cancel);
		this.cancelButton.setOnClickListener(bCancelListener);

		this.okButton = (Button) findViewById(R.id.cs_bn_ok);
		this.okButton.setOnClickListener(bOkListener);
	}

	/**
	 * Einstellungen setzen.
	 */
	private void setSettings() {

		/* Wie ist man unterwegs */
		if (this.tourSettings.getLocomotion().equals(Locomotion.CAR)) {
			this.carRadioButton.setChecked(true);
			setHowGoodEnabled(true,
					getString(R.string.q_how_good_are_you_traveling_info));
		} else if (this.tourSettings.getLocomotion().equals(Locomotion.BIKE)) {
			this.bikeRadioButton.setChecked(true);
		} else {
			this.hoofItRadioButton.setChecked(true);
		}

		/* Wie gut unterwegs */
		if (this.tourSettings.getHowGood().equals(HowGood.FAST)) {
			this.fastRadioButton.setChecked(true);
		} else if (this.tourSettings.getHowGood().equals(HowGood.NORMAL)) {
			this.normalRadioButton.setChecked(true);
		} else {
			this.slowRadioButton.setChecked(true);
		}

		/* Wie lange? */
		this.howLongSeekBar.setProgress(getSeekbarValue());

		/* Zielpunkttypen */
		setTargetPointTypes();

		/* Schnitzeljagd- und Offline-Modus */
		if (this.tourSettings.getIsPaperChase()) {
			this.paperChaseCheckBox.setChecked(true);
		} else {
			setLevelOfDifficultyEnabled(false);
		}

		/* Schwierigkeitsgrad setzen */
		if (this.tourSettings.getLevelOfDifficulty().equals(
				LevelOfDifficulty.EASY)) {
			this.easyRadioButton.setChecked(true);
		} else if (this.tourSettings.getLevelOfDifficulty().equals(
				LevelOfDifficulty.MIDDLE)) {
			this.middleRadioButton.setChecked(true);
		} else {
			this.difficultRadioButton.setChecked(true);
		}
	}

	/**
	 * Setzen der Zielpunkte.
	 */
	private void setTargetPointTypes() {
		String targetPoints = "";
		if (this.tourSettings.getTargetPointTypes().contains(
				TargetPointType.CEMETERY)) {
			targetPoints = this.getString(R.string.tp_cemeteries);
		}
		if (this.tourSettings.getTargetPointTypes().contains(
				TargetPointType.CHAPEL)) {
			if (targetPoints.equals(""))
				targetPoints = this.getString(R.string.tp_chapels);
			else
				targetPoints = targetPoints + ",\n"
						+ this.getString(R.string.tp_chapels);
		}
		if (this.tourSettings.getTargetPointTypes().contains(
				TargetPointType.CHURCH)) {
			if (targetPoints.equals(""))
				targetPoints = this.getString(R.string.tp_churches);
			else
				targetPoints = targetPoints + ",\n"
						+ this.getString(R.string.tp_churches);
		}
		if (this.tourSettings.getTargetPointTypes().contains(
				TargetPointType.JEWISH_CEMETERY)) {
			if (targetPoints.equals(""))
				targetPoints = this.getString(R.string.tp_jewish_cemeteries);
			else
				targetPoints = targetPoints + ",\n"
						+ this.getString(R.string.tp_jewish_cemeteries);
		}
		if (this.tourSettings.getTargetPointTypes().contains(
				TargetPointType.MONASTERY)) {
			if (targetPoints.equals(""))
				targetPoints = this.getString(R.string.tp_monasteries);
			else
				targetPoints = targetPoints + ",\n"
						+ this.getString(R.string.tp_monasteries);
		}
		if (this.tourSettings.getTargetPointTypes().contains(
				TargetPointType.MONUMENT)) {
			if (targetPoints.equals(""))
				targetPoints = this.getString(R.string.tp_monuments);
			else
				targetPoints = targetPoints + ",\n"
						+ this.getString(R.string.tp_monuments);
		}
		if (this.tourSettings.getTargetPointTypes().contains(
				TargetPointType.STATUE)) {
			if (targetPoints.equals(""))
				targetPoints = this.getString(R.string.tp_statues);
			else
				targetPoints = targetPoints + ",\n"
						+ this.getString(R.string.tp_statues);
		}
		if (this.tourSettings.getTargetPointTypes().contains(
				TargetPointType.WAYSIDE_CROSS)) {
			if (targetPoints.equals(""))
				targetPoints = this.getString(R.string.tp_wayside_crosses);
			else
				targetPoints = targetPoints + ",\n"
						+ this.getString(R.string.tp_wayside_crosses);
		}
		this.targetPointsTextView.setText(targetPoints);
	}

	/* ======================================================================= */
	/* Men� */
	/* ======================================================================= */
	/**
	 * Anzeigen des Hauptmenues.
	 */
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.help_menu, menu);
		return super.onCreateOptionsMenu(menu);
	}

	/**
	 * Event-Handling des Menues.
	 * 
	 * @param item
	 *            Menuepunkt, der gewaehlt wurde.
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		switch (item.getItemId()) {
		case R.id.menu_h_help:
			onClickHelp();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/**
	 * Aktionen bei Klick auf Settings-Button.
	 */
	protected void onClickHelp() {
		showMessageBox(getString(R.string.menu_help),
				getString(R.string.cs_help));
	}

	/* ======================================================================= */
	/* Seek-Bar f�r "Wie lange?" */
	/* ======================================================================= */
	/**
	 * Event, bei Ver�nderung der Seekbar.
	 */
	private OnSeekBarChangeListener sbHowLongListener = new OnSeekBarChangeListener() {

		public void onStopTrackingTouch(SeekBar seekBar) {
			/* keine Aktionen */
		}

		public void onStartTrackingTouch(SeekBar seekBar) {
			/* keine Aktionen */
		}

		public void onProgressChanged(SeekBar seekBar, int progress,
				boolean fromUser) {
			howLongTextView.setText(ActivityHelper.getTime(
					ChangeSettingsActivity.this, getMinutes(), false));
		}
	};

	/* ======================================================================= */
	/* Abbrechen-Button */
	/* ======================================================================= */
	/**
	 * OnClickListener f�r Next-Button.
	 */
	private OnClickListener bCancelListener = new OnClickListener() {

		/**
		 * Wird bei Klick auf den Button aufgerufen.
		 * 
		 * @param v
		 *            View
		 */
		public void onClick(View v) {
			onClickCancel(v);
		}
	};

	/**
	 * Aktionen bei Klick auf Next-Button.
	 * 
	 * @param v
	 *            View
	 */
	protected void onClickCancel(View v) {
		finish();
	}

	/* ======================================================================= */
	/* Ok-Button */
	/* ======================================================================= */
	/**
	 * OnClickListener f�r Ok-Button.
	 */
	private OnClickListener bOkListener = new OnClickListener() {

		/**
		 * Wird bei Klick auf den Button aufgerufen.
		 * 
		 * @param v
		 *            View
		 */
		public void onClick(View v) {
			onClickOk(v);
		}
	};

	/**
	 * Aktionen bei Klick auf Cancel-Button.
	 * 
	 * @param v
	 *            View
	 */
	protected void onClickOk(View v) {
		getTourSettings();

		if (this.tourSettings.getTargetPointTypes().isEmpty()) {

			/* Fehler-Meldung anzeigen -> mindestens ein Typ ausw�hlen */
			ActivityHelper.showMessageToast(this,
					getString(R.string.cs_no_tp_type_selected));

		} else {
			/* Preferences sichern */
			SharedPreferences prefs = this.getSharedPreferences(
					"PREFERECES_NAME", Activity.MODE_WORLD_WRITEABLE);
			SharedPreferences.Editor editor = prefs.edit();

			/* Einstellungen speichern */
			try {
				editor.putString(IntentConstants.TOUR_SETTINGS,
						this.tourSettings.toJSON().toString());
			} catch (JSONException e1) {
				/* keine Aktion */
			}
			editor.commit();
			finish();
		}
	}

	/**
	 * Liest die in der GUI gemachten Einstellungen ein.
	 */
	private void getTourSettings() {
		/* Wie unterwegs */
		if (this.carRadioButton.isChecked()) {
			this.tourSettings.setLocomotion(Locomotion.CAR);
		} else if (this.bikeRadioButton.isChecked()) {
			this.tourSettings.setLocomotion(Locomotion.BIKE);
		} else {
			this.tourSettings.setLocomotion(Locomotion.HOOF_IT);
		}

		/* Wie gut unterwegs */
		if (this.fastRadioButton.isChecked()) {
			this.tourSettings.setHowGood(HowGood.FAST);
		} else if (this.normalRadioButton.isChecked()) {
			this.tourSettings.setHowGood(HowGood.NORMAL);
		} else {
			this.tourSettings.setHowGood(HowGood.SLOW);
		}

		/* Wie lange */
		this.tourSettings.setHowLong(getMinutes());

		/* Schnitzeljagd */
		if (this.paperChaseCheckBox.isChecked()) {
			this.tourSettings.setIsPaperChase(true);
		} else {
			this.tourSettings.setIsPaperChase(false);
		}

		/* Schwierigkeitsmodus der Schnitzeljagd */
		if (this.easyRadioButton.isChecked()) {
			this.tourSettings.setLevelOfDifficulty(LevelOfDifficulty.EASY);
		} else if (this.middleRadioButton.isChecked()) {
			this.tourSettings.setLevelOfDifficulty(LevelOfDifficulty.MIDDLE);
		} else {
			this.tourSettings.setLevelOfDifficulty(LevelOfDifficulty.DIFFICULT);
		}
	}

	/* ======================================================================= */
	/* Art der Fortbewegung */
	/* ======================================================================= */
	/**
	 * OnClickListener f�r Art der Fortbewegung.
	 */
	private OnClickListener radioGroupLocomotionListener = new OnClickListener() {

		/**
		 * Wird bei Klick auf den Button aufgerufen.
		 * 
		 * @param v
		 *            View
		 */
		public void onClick(View v) {
			onClickLocomotion(v);
		}
	};

	/**
	 * Aktionen bei Klick auf Radio-Buttons Art der Fortbewegung.
	 * 
	 * @param v
	 *            View
	 */
	protected void onClickLocomotion(View v) {
		if (this.carRadioButton.isChecked()) {
			setHowGoodEnabled(true,
					getString(R.string.q_how_good_are_you_traveling_info));
			this.normalRadioButton.setChecked(true);
		} else {
			setHowGoodEnabled(false,
					getString(R.string.q_how_good_are_you_traveling));
		}
	}

	/**
	 * Falls das Auto das Forbewegungsmittel ist, ist die Frage, wie gut man
	 * unterwegs ist, hinf�llig, daher werden diese Felder nicht zur Auswahl
	 * freigestellt.
	 * 
	 * @param isCarChecked
	 *            true, wenn Auto als Fortbewegungsmittel ausgew�hlt ist
	 * @param infoText
	 *            Informationstext
	 */
	private void setHowGoodEnabled(boolean isCarChecked, String infoText) {
		this.howGoodTextView.setText(infoText);
		this.howGoodTextView.setEnabled(!isCarChecked);
		this.slowRadioButton.setEnabled(!isCarChecked);
		this.normalRadioButton.setEnabled(!isCarChecked);
		this.fastRadioButton.setEnabled(!isCarChecked);
	}

	/* ======================================================================= */
	/* Zielpunkt-Typen ausw�hlen */
	/* ======================================================================= */

	/**
	 * OnClickListener f�r Zielpunkt-Typen ausw�hlen-Button.
	 */
	private OnClickListener bTargetPointTypesListener = new OnClickListener() {

		/**
		 * Wird bei Klick auf den Button aufgerufen.
		 * 
		 * @param v
		 *            View
		 */
		public void onClick(View v) {
			onClickTargetPointTypes(v);
		}
	};

	/**
	 * Aktionen bei Klick auf Zielpunkt-Typen ausw�hlen-Button.
	 * 
	 * @param v
	 *            View
	 */
	protected void onClickTargetPointTypes(View v) {

		final List<TargetPointType> targetPointTypes = new ArrayList<TargetPointType>(
				this.tourSettings.getTargetPointTypes());

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(this.getString(R.string.cs_target_point_types_title));
		builder.setMultiChoiceItems(R.array.cs_target_point_types,
				getSelectedTargetPointTypes(),
				new DialogInterface.OnMultiChoiceClickListener() {
					public void onClick(DialogInterface dialog,
							int whichButton, boolean isChecked) {
						setTargetPointTypes(whichButton, isChecked);
					}
				});

		/* Ok-Button */
		builder.setPositiveButton(getString(R.string.button_ok),
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface d, int m) {
						/* keine Aktion */
						setTargetPointTypes();
					}
				});
		/* Abbrechen-Button */
		builder.setNegativeButton(getString(R.string.button_cancel),
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface d, int m) {
						/* Liste zur�cksetzen */
						resetTargetPointTypes(targetPointTypes);
						setTargetPointTypes();
					}
				});

		AlertDialog alert = builder.create();
		alert.show();
	}

	/**
	 * Setzt die ausgew�hlten Zielpunkt-Typen wieder zur�ck.
	 * 
	 * @param targetPointTypes
	 */
	private void resetTargetPointTypes(List<TargetPointType> targetPointTypes) {
		this.tourSettings.setTargetPointTypes(targetPointTypes);
	}

	/**
	 * F�gt oder l�scht der Zielpunkt-Typ-Liste den ausgew�hlten Punkt hinzu.
	 * 
	 * @param targetPointTypeID
	 *            ID des Zielpunkt-Typs
	 * @param isAdd
	 *            hinzuf�gen = true / l�schen = false
	 */
	private void setTargetPointTypes(int targetPointTypeID, boolean isAdd) {
		if (isAdd) {
			this.tourSettings.addTargetPointType(targetPointTypeID);
		} else {
			this.tourSettings.removeTargetPointType(targetPointTypeID);
		}
	}

	/**
	 * Setzt, ob ein Zielpunkttyp ausgew�hlt ist oder nicht.
	 * 
	 * @return ausgew�hlte Zielpunkttypen
	 */
	private boolean[] getSelectedTargetPointTypes() {
		boolean[] selectedTargetPointTypes = new boolean[TargetPointType
				.values().length];
		TargetPointType[] targetPointTypes = TargetPointType.values();
		for (int i = 0; i < selectedTargetPointTypes.length; i++) {
			selectedTargetPointTypes[i] = this.tourSettings
					.getTargetPointTypes().contains(targetPointTypes[i]) ? true
					: false;
		}
		return selectedTargetPointTypes;
	}

	/* ======================================================================= */
	/* Offline-Modus */
	/* ======================================================================= */
	/**
	 * OnClickListener f�r Offline-Modus-Radio-Buttons.
	 */
	private OnClickListener levelOfDifficultyListener = new OnClickListener() {

		/**
		 * Wird bei Klick auf den Button aufgerufen.
		 * 
		 * @param v
		 *            View
		 */
		public void onClick(View v) {
			onClickLevelOfDifficulty(v);
		}
	};

	/**
	 * Aktionen bei Klick auf Offline-Modus-Radio-Buttons.
	 * 
	 * @param v
	 *            View
	 */
	protected void onClickLevelOfDifficulty(View v) {
		if (this.paperChaseCheckBox.isChecked()) {

			this.levelOfDifficultyTextView.setEnabled(true);
			this.easyRadioButton.setEnabled(true);
			this.middleRadioButton.setEnabled(true);
			this.difficultRadioButton.setEnabled(true);
		} else {

			this.levelOfDifficultyTextView.setEnabled(false);
			this.easyRadioButton.setEnabled(false);
			this.middleRadioButton.setEnabled(false);
			this.difficultRadioButton.setEnabled(false);
		}
	}

	/* ======================================================================= */
	/* allgemeine Hilfsmethoden */
	/* ======================================================================= */
	/**
	 * Gibt die Minuten, wie lange man unterwegs sein m�chte.
	 */
	private int getMinutes() {
		return (int) (((int) ((this.howLongSeekBar.getProgress() + 1) / SEEKBAR_STEP))
				* TIME_STEP + TIME_START);
	}

	/**
	 * Rechnet die Minuten der Tour-Einstellung in den Wert f�r die Seekbar um.
	 * 
	 * @return
	 */
	private int getSeekbarValue() {
		return (int) ((this.tourSettings.getHowLong() - TIME_START) / TIME_STEP * SEEKBAR_STEP);
	}

	/**
	 * Setzt GUI-Element
	 * 
	 * @param a
	 */
	private void setLevelOfDifficultyEnabled(boolean enabled) {

		this.levelOfDifficultyTextView.setEnabled(enabled);
		this.easyRadioButton.setEnabled(enabled);
		this.middleRadioButton.setEnabled(enabled);
		this.difficultRadioButton.setEnabled(enabled);
	}

	/**
	 * Zeigte eine Info-Fenster mit einem Ok-Button zum Schlie�en.
	 * 
	 * @param context
	 *            Context
	 * @param title
	 *            Titel
	 * @param message
	 *            Meldung
	 */
	public void showMessageBox(String title, String message) {

		AlertDialog msgBox = new AlertDialog.Builder(this).create();
		msgBox.setTitle(title);
		msgBox.setMessage(message);
		msgBox.setButton(getString(R.string.button_ok),
				new android.content.DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						/* Fenster schlie�en */
					}
				});
		msgBox.show();
	}
}
