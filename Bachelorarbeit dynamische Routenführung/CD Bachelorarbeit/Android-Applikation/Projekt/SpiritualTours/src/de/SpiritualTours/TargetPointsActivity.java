package de.SpiritualTours;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import de.SpiritualTours.Utility.IntentConstants;
import de.SpiritualTours.TourObjects.TargetPoint;
import de.SpiritualTours.Adapter.TargetPointsAdapter;

/**
 * Activity zur Anzeige der Zielpunkte der Tour.
 */
public class TargetPointsActivity extends Activity {

	/** Zielpunkte. */
	private List<TargetPoint> targetPoints;
	/** Liste zur Anzeige der Zielpunkte. */
	private ListView targetPointsListView;
	/** Zur�ck Button */
	private Button backButton;

	/* ======================================================================= */
	/* OnCreate */
	/* ======================================================================= */
	/** 
	 * Erzeugt die Activity zur Anzeige der Zielpunkte. 
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.targetpoints);

		getTargetPoints();
		init();
	}

	/* ======================================================================= */
	/* Initialisieren */
	/* ======================================================================= */
	/**
	 * Liest die �bergebenen Zielpunkte ein.
	 */
	private void getTargetPoints() {
		this.targetPoints = this.getIntent().getExtras()
				.getParcelableArrayList(IntentConstants.TARGETPOINTS);
	}
	
	/**
	 * Initialisiert die GUI-Kompenenten und setzt die Daten.
	 */
	private void init() {
		this.targetPointsListView = (ListView) findViewById(R.id.tp_targetpoint_list);
		this.targetPointsListView.setAdapter(new TargetPointsAdapter(this,
				this.targetPoints));

		this.targetPointsListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> a, View v, int position,
					long id) {
				Intent targetPointDetails = new Intent(
						TargetPointsActivity.this, TargetPointDetailsActivity.class);
				targetPointDetails.putExtra(IntentConstants.TARGETPOINT,
						(TargetPoint) targetPointsListView
								.getItemAtPosition(position));
				startActivity(targetPointDetails);
			}
		});
		
		this.backButton = (Button) findViewById(R.id.tp_back);
		this.backButton.setOnClickListener(bBackListener);
	}
	
	/* ======================================================================= */
	/* Back-Button */
	/* ======================================================================= */
	/**
	 * OnClickListener f�r Back-Button.
	 */
	private OnClickListener bBackListener = new OnClickListener() {

		/**
		 * Wird bei Klick auf den Button aufgerufen.
		 * 
		 * @param v
		 *            View
		 */
		public void onClick(View v) {
			onClickBack(v);
		}
	};

	/**
	 * Activity schliessen und zur Vater-Activity zur�ck.
	 * 
	 * @param v
	 *            View
	 */
	protected void onClickBack(View v) {

		finish();
	}
}
