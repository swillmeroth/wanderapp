package de.SpiritualTours;

import java.io.IOException;
import java.util.Locale;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TableRow;
import android.widget.TextView;
import de.SpiritualTours.Exceptions.ConnectionException;
import de.SpiritualTours.Exceptions.NoTargetPointsFoundException;
import de.SpiritualTours.TourObjects.GeoPoint;
import de.SpiritualTours.TourObjects.Question.LevelOfDifficulty;
import de.SpiritualTours.TourObjects.TargetPoint.TargetPointType;
import de.SpiritualTours.TourObjects.TourSettings;
import de.SpiritualTours.TourObjects.TourSettings.HowGood;
import de.SpiritualTours.TourObjects.TourSettings.Language;
import de.SpiritualTours.TourObjects.TourSettings.Locomotion;
import de.SpiritualTours.Utility.ActivityHelper;
import de.SpiritualTours.Utility.IntentConstants;
import de.SpiritualTours.Utility.ToursAdministrator;
import de.SpiritualTours.Utility.WebServiceConnection;

/**
 * Start-Activity der App. Zeigt die Einstellung zur Tour an und ermittelt die
 * aktuelle Position des Nutzers.
 */
public class StartActivity extends Activity implements IntentConstants,
		LocationListener {

	/* ======================================================================= */
	/* Konstanten */
	/* ======================================================================= */
	/**
	 * Zeitliches Minimalintervall in Millisekunden f�r eine Aktualisierung der
	 * Position.
	 */
	private final static int LOCATION_MIN_TIME = 1000;
	/** Minimalabstand in Metern f�r eine Aktualisierung der Position. */
	private final static int LOCATION_MIN_DISTANCE = 2;

	/** Meldung f�r: Position des Nutzers gefunden. */
	private final static int HANDLER_GOT_LOCATION = 0;
	/** Meldung f�r: Touren vom Server erhalten. */
	private final static int HANDLER_GOT_TOURS = 1;
	/** Meldung f�r: Verbindungsfehler. */
	private final static int HANDLER_CLIENT_PROTOCOL_EXCEPTION = 2;
	/** Meldung f�r: keine Erstellung der Tour m�glich. */
	private final static int HANDLER_JSON_EXCEPTION = 3;
	/** Meldung f�r: Verbindungsfehler. */
	private final static int HANDLER_IO_EXCEPTION = 4;
	/** Meldung f�r: keine Erstellung der Tour m�glich. */
	private final static int HANDLER_NO_TARGETPOINTS_EXCEPTION = 5;
	/** Meldung f�r: Verbindungsfehler. */
	private final static int HANDLER_CONNECTION_TO_ORS_EXCEPTION = 6;

	/* ======================================================================= */
	/* Instanzvariablen */
	/* ======================================================================= */
	/** Bewegungsmittel */
	private TextView locomotionTextView;

	/** Wie gut unterwegs */
	private TextView howGoodTextView;

	/** Wie lange unterwegs */
	private TextView howLongTextView;

	/** Zielpunkte */
	private TextView targetPointsTextView;

	/** Schnitzeljagd */
	private TextView paperChaseTextView;

	/** Schwierigkeitsgrad */
	private TextView levelOfDifficultyTextView;

	/** Schwierigkeitsgrad */
	private TableRow levelOfDifficultyRow;

	/** Button Einstellung */
	private Button buttonSettings;

	/** Button Weiter */
	private Button buttonNext;

	/** LocationManager. */
	private LocationManager locationManager;

	/** Einstellungen der Tour. */
	private TourSettings tourSettings;

	/** Aktuelle Position des Nutzers. */
	private GeoPoint currentLocation;

	/**
	 * Dialog zur Anzeige von Prozessen. Hier f�r Ermittlung der Position und
	 * Erstellung der Touren.
	 */
	private ProgressDialog dialog;

	/**
	 * Handler, um Nachrichten des Threads in der Activity bearbeiten zu k�nnen.
	 */
	private final Handler threadHandler = new Handler() {

		/**
		 * Handlet die Nachrichten des Threads.
		 * 
		 * @param msg
		 *            Nachricht des Threads
		 */
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case HANDLER_GOT_LOCATION:
				dialog.setCancelable(false);
				dialog.setMessage(getString(R.string.sf_create_tours));
				break;
			case HANDLER_GOT_TOURS:
				startActivity(new Intent(StartActivity.this,
						ToursAdviserActivity.class));
				dialog.dismiss();
				break;
			case HANDLER_CLIENT_PROTOCOL_EXCEPTION:
				dialog.dismiss();
				showMessageBox(getString(R.string.error_caption),
						getString(R.string.error_no_connection_to_server) + " "
								+ getString(R.string.error_try_again));
				break;
			case HANDLER_JSON_EXCEPTION:
				dialog.dismiss();
				showMessageBox(getString(R.string.error_caption),
						getString(R.string.error_unknown));
				break;
			case HANDLER_IO_EXCEPTION:
				dialog.dismiss();
				showMessageBox(getString(R.string.error_caption),
						getString(R.string.error_no_connection_to_server) + " "
								+ getString(R.string.error_try_again));
				break;
			case HANDLER_NO_TARGETPOINTS_EXCEPTION:
				dialog.dismiss();
				showMessageBox(getString(R.string.error_caption),
						getString(R.string.error_no_targetpoints));
				break;
			case HANDLER_CONNECTION_TO_ORS_EXCEPTION:
				dialog.dismiss();
				showMessageBox(getString(R.string.error_caption),
						getString(R.string.error_no_connection_to_ors) + " "
								+ getString(R.string.error_try_again));
				break;
			}
		}
	};

	/* ======================================================================= */
	/* OnCreate */
	/* ======================================================================= */
	/**
	 * Startet die App und zeigt zun�chst ein �bersichts-Fenster der
	 * Einstellungen.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.start);

		/* Komponenten initialisieren */
		init();
		/* Einstellungen setzen */
		setSettings();
	}

	/* ======================================================================= */
	/* OnStart */
	/* ======================================================================= */
	/**
	 * Liest die Einstellungen ein und setzt diese.
	 */
	@Override
	public void onRestart() {

		super.onRestart();

		/* Einstellungen laden */
		this.tourSettings = ToursAdministrator.getInstance().getTourSettings();
		/* Einstellungen setzen */
		setSettings();
	}

	/* ======================================================================= */
	/* Initialisieren */
	/* ======================================================================= */
	/**
	 * Initialisieren der GUI-Elemente.
	 */
	private void init() {
		this.buttonSettings = (Button) findViewById(R.id.sf_bn_edit_settings);
		this.buttonSettings.setOnClickListener(bEditSettingsListener);

		this.buttonNext = (Button) findViewById(R.id.sf_bn_next);
		this.buttonNext.setOnClickListener(bNextListener);

		this.locomotionTextView = (TextView) findViewById(R.id.sf_locomotion_hoof_it);
		this.howGoodTextView = (TextView) findViewById(R.id.sf_how_good);
		this.howLongTextView = (TextView) findViewById(R.id.sf_how_long);
		this.targetPointsTextView = (TextView) findViewById(R.id.sf_target_points);
		this.paperChaseTextView = (TextView) findViewById(R.id.sf_paper_chase);
		this.levelOfDifficultyTextView = (TextView) findViewById(R.id.sf_level_of_difficulty);
		this.levelOfDifficultyRow = (TableRow) findViewById(R.id.sf_level_of_difficulty_row);

		this.locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		this.locationManager.requestLocationUpdates(
				LocationManager.GPS_PROVIDER, LOCATION_MIN_TIME,
				LOCATION_MIN_DISTANCE, this);

		/* Einstellungen aus Shared Preferences laden */
		SharedPreferences prefs = this.getSharedPreferences("PREFERECES_NAME",
				Activity.MODE_WORLD_WRITEABLE);
		try {
			this.tourSettings = new TourSettings(new JSONObject(
					prefs.getString(IntentConstants.TOUR_SETTINGS, "")));
		} catch (JSONException e) {
			this.tourSettings = new TourSettings();
		}
		ToursAdministrator.getInstance().setTourSettings(this.tourSettings);

		/* Spracheinstellungen setzen */
		setLanguage();
	}

	/**
	 * Setzen der Einstellungen.
	 */
	private void setSettings() {

		/* Wie ist man unterwegs */
		if (this.tourSettings.getLocomotion().equals(Locomotion.CAR)) {
			this.locomotionTextView.setText(this
					.getString(R.string.locomotion_car));
		} else if (this.tourSettings.getLocomotion().equals(Locomotion.BIKE)) {
			this.locomotionTextView.setText(this
					.getString(R.string.locomotion_bike));
		} else {
			this.locomotionTextView.setText(this
					.getString(R.string.locomotion_hoof_it));
		}

		/* Wie gut unterwegs */
		if (this.tourSettings.getHowGood().equals(HowGood.FAST)) {
			this.howGoodTextView
					.setText(this.getString(R.string.how_good_fast));
		} else if (this.tourSettings.getHowGood().equals(HowGood.NORMAL)) {
			this.howGoodTextView.setText(this
					.getString(R.string.how_good_normal));
		} else {
			this.howGoodTextView
					.setText(this.getString(R.string.how_good_slow));
		}

		/* Wie lange? */
		this.howLongTextView.setText(ActivityHelper.getTime(this,
				this.tourSettings.getHowLong(), true));

		/* Was will man sehen */
		setTargetPointTypes();

		/* Schnitzeljagd */
		if (this.tourSettings.getIsPaperChase()) {
			this.paperChaseTextView
					.setText(this.getString(R.string.answer_yes));
		} else {
			this.paperChaseTextView.setText(this.getString(R.string.answer_no));
		}

		/* Schwierigkeitsgrad */
		if (this.tourSettings.getIsPaperChase()) {
			this.levelOfDifficultyRow.setVisibility(View.VISIBLE);
			if (this.tourSettings.getLevelOfDifficulty().equals(
					LevelOfDifficulty.EASY)) {
				this.levelOfDifficultyTextView.setText(this
						.getString(R.string.lod_easy));
			} else if (this.tourSettings.getLevelOfDifficulty().equals(
					LevelOfDifficulty.MIDDLE)) {
				this.levelOfDifficultyTextView.setText(this
						.getString(R.string.lod_middle));
			} else {
				this.levelOfDifficultyTextView.setText(this
						.getString(R.string.lod_difficult));
			}
		} else {
			this.levelOfDifficultyRow.setVisibility(View.GONE);
		}
	}

	/**
	 * Ermittelt und setzt die Sprache-Einstellungen des Mobil-Ger�ts.
	 */
	private void setLanguage() {

		/* Spracheinstellung des Mobil-Ger�ts auslesen */
		String language = Locale.getDefault().getDisplayLanguage();

		/* Spracheinstellung setzen */
		if (language.equals(Locale.ENGLISH.getDisplayLanguage())) {
			this.tourSettings.setLanguage(Language.ENGLISH);
		} else if (language.equals(Locale.GERMAN.getDisplayLanguage())) {
			this.tourSettings.setLanguage(Language.GERMAN);
		}
	}

	/* ======================================================================= */
	/* Men� */
	/* ======================================================================= */
	/**
	 * Anzeigen des Hauptmenues.
	 */
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main_menu, menu);
		return super.onCreateOptionsMenu(menu);
	}

	/**
	 * Event-Handling des Menues.
	 * 
	 * @param item
	 *            Menuepunkt, der gewaehlt wurde.
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.menu_m_help:
			onClickHelp();
			return true;
		case R.id.menu_m_info:
			onClickInfo();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/**
	 * Aktionen bei Klick auf Settings-Button.
	 */
	protected void onClickHelp() {
		showMessageBox(getString(R.string.menu_help),
				getString(R.string.sf_help));
	}

	/**
	 * Aktionen bei Klick auf Settings-Button.
	 */
	protected void onClickInfo() {
		showMessageBox(getString(R.string.menu_info),
				getString(R.string.sf_info));
	}

	/* ======================================================================= */
	/* Settings-Button */
	/* ======================================================================= */
	/**
	 * OnClickListener f�r Settings-Button.
	 */
	private OnClickListener bEditSettingsListener = new OnClickListener() {

		/**
		 * Wird bei Klick auf den Button aufgerufen.
		 * 
		 * @param v
		 *            View
		 */
		public void onClick(View v) {
			onClickEditSettings(v);
		}
	};

	/**
	 * Aktionen bei Klick auf Settings-Button.
	 * 
	 * @param v
	 *            View
	 */
	protected void onClickEditSettings(View v) {
		startActivity(new Intent(this, ChangeSettingsActivity.class));
	}

	/* ======================================================================= */
	/* Next-Button */
	/* ======================================================================= */
	/**
	 * OnClickListener f�r Next-Button.
	 */
	private OnClickListener bNextListener = new OnClickListener() {

		/**
		 * Wird bei Klick auf den Button aufgerufen.
		 * 
		 * @param v
		 *            View
		 */
		public void onClick(View v) {
			onClickNext(v);
		}
	};

	/**
	 * Aktionen bei Klick auf Next-Button.
	 * 
	 * @param v
	 *            View
	 */
	protected void onClickNext(View v) {

		showProgressDialog();

	}

	/**
	 * Progress-Dialog-Fenster solange anzeigen, bis Position ermittelt ist.
	 */
	private void showProgressDialog() {

		/*
		 * Porgess-Dialog-Parameter: Activity, Titel, Text, Zeitlich
		 * beschr�nkbar?, Unterbrechbar?
		 */
		this.dialog = ProgressDialog.show(this, getString(R.string.wait),
				getString(R.string.sf_locate_position), true, true);

		new GetToursThread(this.threadHandler).start();
	}

	/* ======================================================================= */
	/* Hilfsmethoden */
	/* ======================================================================= */
	/**
	 * Setzen der Zielpunkte.
	 */
	private void setTargetPointTypes() {
		String targetPoints = "";
		if (this.tourSettings.getTargetPointTypes().contains(
				TargetPointType.CEMETERY)) {
			targetPoints = this.getString(R.string.tp_cemeteries);
		}
		if (this.tourSettings.getTargetPointTypes().contains(
				TargetPointType.CHAPEL)) {
			if (targetPoints.equals(""))
				targetPoints = this.getString(R.string.tp_chapels);
			else
				targetPoints = targetPoints + ",\n"
						+ this.getString(R.string.tp_chapels);
		}
		if (this.tourSettings.getTargetPointTypes().contains(
				TargetPointType.CHURCH)) {
			if (targetPoints.equals(""))
				targetPoints = this.getString(R.string.tp_churches);
			else
				targetPoints = targetPoints + ",\n"
						+ this.getString(R.string.tp_churches);
		}
		if (this.tourSettings.getTargetPointTypes().contains(
				TargetPointType.JEWISH_CEMETERY)) {
			if (targetPoints.equals(""))
				targetPoints = this.getString(R.string.tp_jewish_cemeteries);
			else
				targetPoints = targetPoints + ",\n"
						+ this.getString(R.string.tp_jewish_cemeteries);
		}
		if (this.tourSettings.getTargetPointTypes().contains(
				TargetPointType.MONASTERY)) {
			if (targetPoints.equals(""))
				targetPoints = this.getString(R.string.tp_monasteries);
			else
				targetPoints = targetPoints + ",\n"
						+ this.getString(R.string.tp_monasteries);
		}
		if (this.tourSettings.getTargetPointTypes().contains(
				TargetPointType.MONUMENT)) {
			if (targetPoints.equals(""))
				targetPoints = this.getString(R.string.tp_monuments);
			else
				targetPoints = targetPoints + ",\n"
						+ this.getString(R.string.tp_monuments);
		}
		if (this.tourSettings.getTargetPointTypes().contains(
				TargetPointType.STATUE)) {
			if (targetPoints.equals(""))
				targetPoints = this.getString(R.string.tp_statues);
			else
				targetPoints = targetPoints + ",\n"
						+ this.getString(R.string.tp_statues);
		}
		if (this.tourSettings.getTargetPointTypes().contains(
				TargetPointType.WAYSIDE_CROSS)) {
			if (targetPoints.equals(""))
				targetPoints = this.getString(R.string.tp_wayside_crosses);
			else
				targetPoints = targetPoints + ",\n"
						+ this.getString(R.string.tp_wayside_crosses);
		}
		this.targetPointsTextView.setText(targetPoints);
	}

	/* ======================================================================= */
	/* LocationListener-Methoden */
	/* ======================================================================= */
	/**
	 * Wird aufgerufen, wenn die Position sich ge�ndert hat.
	 * 
	 * @param location
	 *            Location
	 */
	@Override
	public void onLocationChanged(Location location) {

		/* Zielpunkt erzeugen */
		this.currentLocation = new GeoPoint(location.getLatitude(),
				location.getLongitude(), location.getAltitude());
	}

	@Override
	public void onProviderDisabled(String provider) {
		/* f�r diese Zwecke nicht von Bedeutung */
	}

	@Override
	public void onProviderEnabled(String provider) {
		/* f�r diese Zwecke nicht von Bedeutung */
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		/* f�r diese Zwecke nicht von Bedeutung */
	}

	/* ======================================================================= */
	/* Msg-Box */
	/* ======================================================================= */
	/**
	 * Wird die Zur�ck-Taste bet�tigt, wird diese als Home-Button interpretiert.
	 */
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if (keyCode == KeyEvent.KEYCODE_BACK) {
	        moveTaskToBack(true);
	        return true;
	    }
	    return super.onKeyDown(keyCode, event);
	}

	/* ======================================================================= */
	/* Msg-Box */
	/* ======================================================================= */
	/**
	 * Zeigte eine Info-Fenster mit einem Ok-Button zum Schlie�en.
	 * 
	 * @param context
	 *            Context
	 * @param title
	 *            Titel
	 * @param message
	 *            Meldung
	 */
	public void showMessageBox(String title, String message) {

		AlertDialog msgBox = new AlertDialog.Builder(this).create();
		msgBox.setTitle(title);
		msgBox.setMessage(message);
		msgBox.setButton(getString(R.string.button_ok),
				new android.content.DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						/* Fenster schlie�en */
					}
				});
		msgBox.show();
	}

	/* ======================================================================= */
	/* Thread */
	/* ======================================================================= */
	/**
	 * Thread zur Ermittlung der Touren.
	 */
	public class GetToursThread extends Thread {

		/** Handler zum Benachrichtigen der Activity */
		private Handler handler;

		/**
		 * Erzeugt einen Thread zur Ermittlung der Touren.
		 * 
		 * @param handler
		 *            Handler zum Benachrichtigen der Activity
		 */
		public GetToursThread(Handler handler) {
			super();
			this.handler = handler;
		}

		/**
		 * Ausf�hren des Threads.
		 */
		public void run() {

			/* Der Thread wird solange ausgef�hrt, wie der Dialog angzeigt wird */
			while (dialog.isShowing()) {

				/*
				 * Wenn die Position gefunden wurde, werden die Touren durch den
				 * Web-Service erstellt und der Thread beendet.
				 */
				if (currentLocation != null) {
					tourSettings.setStartPoint(currentLocation);
					this.handler.sendEmptyMessage(HANDLER_GOT_LOCATION);
					if (getTours()) {
						this.handler.sendEmptyMessage(HANDLER_GOT_TOURS);
					}
					return;
				}
			}
		}

		/**
		 * Stellt eine Anfrage an den Web-Service zur Erstellung der Touren. Die
		 * Touren werden im Singleton-Objekt ToursAdministrator abgelegt.
		 */
		private boolean getTours() {
			boolean isGetToursSuccessfully = true;
			/* Anfrage an den Server */
			try {
				ToursAdministrator.getInstance().setTours(
						WebServiceConnection.getTours(tourSettings));
			} catch (ClientProtocolException e) {
				/* Fehler-Meldung anzeigen -> Verbindung mit dem Server. */
				isGetToursSuccessfully = false;
				this.handler
						.sendEmptyMessage(HANDLER_CLIENT_PROTOCOL_EXCEPTION);
			} catch (JSONException e) {
				isGetToursSuccessfully = false;
				this.handler.sendEmptyMessage(HANDLER_JSON_EXCEPTION);
			} catch (IOException e) {
				isGetToursSuccessfully = false;
				this.handler.sendEmptyMessage(HANDLER_IO_EXCEPTION);
			} catch (NoTargetPointsFoundException e) {
				isGetToursSuccessfully = false;
				this.handler
						.sendEmptyMessage(HANDLER_NO_TARGETPOINTS_EXCEPTION);
			} catch (ConnectionException e) {
				isGetToursSuccessfully = false;
				this.handler
						.sendEmptyMessage(HANDLER_CONNECTION_TO_ORS_EXCEPTION);
			}
			return isGetToursSuccessfully;
		}
	}
}