package de.SpiritualTours.Exceptions;

/**
 * Fehler: keine Zielpunkte vorhanden.
 */
public class NoTargetPointsFoundException extends Exception {

	/** Serial-Version-UID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Erzeugt eine NoTargetPointsFoundException.
	 * 
	 * @param s
	 *            Fehlermeldung
	 */
	public NoTargetPointsFoundException() {
	}
}