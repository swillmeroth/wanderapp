package de.SpiritualTours.TourObjects;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;
import de.SpiritualTours.TourObjects.Question.LevelOfDifficulty;
import de.SpiritualTours.TourObjects.TargetPoint.TargetPointType;

/**
 * Einstellungen zu einer Tour.
 */
public class TourSettings implements Parcelable {

	/* ======================================================================= */
	/* Konstanten */
	/* ======================================================================= */
	/** Kostante f�r Art der Fortbewegung. */
	private static final String SETTINGS_LOCOMOTION = "locomotion";
	/** Kostante f�r wie gut. */
	private static final String SETTINGS_HOW_GOOD = "howGood";
	/** Kostante f�r wie lang. */
	private static final String SETTINGS_HOW_LONG = "howLong";
	/** Kostante f�r Zielpunkttypen. */
	private static final String SETTINGS_TARGETPOINTTYPES = "targetPointTypes";
	/** Kostante f�r Schnitzeljagd. */
	private static final String SETTINGS_PAPERCHASE = "isPaperChase";
	/** Kostante f�r Schnitzeljagd. */
	private static final String SETTINGS_LEVEL_OF_DIFFICULTY = "levelOfDifficulty";
	
	/** Durchschnittliche Geschwindigkeit eines Fu�g�ngers (km/h). */
    private static final float SPEED_HOOF_IT = 3.75f;
    /** Durchschnittliche Geschwindigkeit eines Fahrradfahrers (km/h). */
    private static final float SPEED_BIKE = 15;
    /** Durchschnittliche Geschwindigkeit eines Autofahrers (km/h). */
    private static final float SPEED_CAR = 50;

	/* ======================================================================= */
	/* Instanzvariablen */
	/* ======================================================================= */
	/** Geo-Position. */
	private GeoPoint startPoint;
	/** Wie ist man unterwegs? zu Fu�, mit dem Fahrrad, mit dem Auto */
	private Locomotion locomotion;
	/** Wie gut ist man unterwegs? langsam, normal, schnell */
	private HowGood howGood;
	/** Wie lange unterwegs? Angabe in Minuten */
	private int howLong;
	/** Was will man sehen */
	private List<TargetPointType> targetPointTypes;
	/** Schnitzeljagd? */
	private boolean isPaperChase;
	/** Schwierigkeitsgrad der Fragen bei der Schnitzeljagd */
	private LevelOfDifficulty levelOfDifficulty;
	/** Sprache. */
	private Language language;

	/* ======================================================================= */
	/* Konstruktoren */
	/* ======================================================================= */
	/**
	 * Erzeugt ein Tour-Settings-Objekt zur Einstellung der Tour.
	 * 
	 * @param locomotion
	 *            Art der Fortbewegung
	 * @param howGood
	 *            Wie gut ist man unterwegs
	 * @param howLong
	 *            Wie lange will man unterwegs sein
	 * @param targetPointTypes
	 *            Was m�chte man sehen
	 * @param isPaperChase
	 *            Schnitzeljagd erw�nscht
	 * @param isOfflineModus
	 *            Offline-Modus
	 * @param isTourRecording
	 *            Tour aufzeichnen?
	 */
	public TourSettings(Locomotion locomotion, HowGood howGood, int howLong,
			List<TargetPointType> targetPointTypes, boolean isPaperChase,
			LevelOfDifficulty levelOfDifficulty, boolean isOfflineModus,
			Language language) {

		this.startPoint = new GeoPoint();
		this.locomotion = locomotion;
		this.howGood = howGood;
		this.howLong = howLong;
		this.targetPointTypes = targetPointTypes;
		this.isPaperChase = isPaperChase;
		this.levelOfDifficulty = levelOfDifficulty;
		this.language = language;
	}

	/**
	 * Erzeugt ein Tour-Settings-Objekt zur Einstellung der Tour mit
	 * Default-Werten.
	 * 
	 * @param jsonSettings
	 *            JSON-Objekt
	 * @throws JSONException
	 *             Bei falschem JSON-Objekt.
	 */
	public TourSettings(JSONObject jsonSettings) throws NumberFormatException,
			JSONException {

		this.startPoint = new GeoPoint();
		this.locomotion = Locomotion.valueOf(jsonSettings
				.getString(SETTINGS_LOCOMOTION));
		this.howGood = HowGood.valueOf(jsonSettings
				.getString(SETTINGS_HOW_GOOD));
		this.howLong = jsonSettings.getInt(SETTINGS_HOW_LONG);

		/* Zielpunkttypen */
		this.targetPointTypes = new ArrayList<TargetPointType>();
		JSONArray jsonTargetPointTypes = jsonSettings
				.getJSONArray(SETTINGS_TARGETPOINTTYPES);
		for (int i = 0; i < jsonTargetPointTypes.length(); i++) {
			this.targetPointTypes.add(TargetPointType
					.valueOf(jsonTargetPointTypes.getString(i)));
		}

		this.isPaperChase = jsonSettings.getBoolean(SETTINGS_PAPERCHASE);
		this.levelOfDifficulty = LevelOfDifficulty.valueOf(jsonSettings
				.getString(SETTINGS_LEVEL_OF_DIFFICULTY));
	}

	/**
	 * Erzeugt ein Tour-Settings-Objekt zur Einstellung der Tour mit
	 * Default-Werten.
	 */
	public TourSettings() {
		this.startPoint = new GeoPoint();
		this.locomotion = Locomotion.HOOF_IT;
		this.howGood = HowGood.NORMAL;
		this.howLong = 60;
		this.targetPointTypes = new ArrayList<TargetPointType>();
		addTargetPointTypes();
		this.isPaperChase = false;
		this.levelOfDifficulty = LevelOfDifficulty.MIDDLE;
		this.language = Language.GERMAN;
	}

	/**
	 * F�gt f�r Standard-Objekt alle Zielpunkttypen hinzu.
	 */
	private void addTargetPointTypes() {
		this.targetPointTypes.add(TargetPointType.CEMETERY);
		this.targetPointTypes.add(TargetPointType.CHAPEL);
		this.targetPointTypes.add(TargetPointType.CHURCH);
		this.targetPointTypes.add(TargetPointType.JEWISH_CEMETERY);
		this.targetPointTypes.add(TargetPointType.MONASTERY);
		this.targetPointTypes.add(TargetPointType.MONUMENT);
		this.targetPointTypes.add(TargetPointType.STATUE);
		this.targetPointTypes.add(TargetPointType.WAYSIDE_CROSS);
	}

	/* ====================================================================== */
	/* Getter und Setter */
	/* ====================================================================== */
	/**
	 * Gibt die Start-Position.
	 * 
	 * @return Start-Position
	 */
	public GeoPoint getStartPoint() {
		return startPoint;
	}

	/**
	 * Setzt die Start-Position.
	 * 
	 * @param startPoint
	 *            Start-Position
	 */
	public void setStartPoint(GeoPoint startPoint) {
		this.startPoint = startPoint;
	}

	/* ---------------------------------------------------------------------- */
	/**
	 * Gibt: Wie ist man unterwegs? zu Fu�, mit dem Fahrrad, mit dem Auto
	 * 
	 * @return Wie ist man unterwegs? zu Fu�, mit dem Fahrrad, mit dem Auto
	 */
	public Locomotion getLocomotion() {
		return locomotion;
	}

	/**
	 * Setzt: Wie ist man unterwegs? zu Fu�, mit dem Fahrrad, mit dem Auto
	 * 
	 * @param howGood
	 *            Wie ist man unterwegs? zu Fu�, mit dem Fahrrad, mit Auto
	 */
	public void setLocomotion(Locomotion locomotion) {
		this.locomotion = locomotion;
	}

	/* ---------------------------------------------------------------------- */
	/**
	 * Gibt: Wie gut ist man unterwegs? langsam, normal, schnell
	 * 
	 * @return Wie gut ist man unterwegs? langsam, normal, schnell
	 */
	public HowGood getHowGood() {
		return howGood;
	}

	/**
	 * Setzt: Wie gut ist man unterwegs? langsam, normal, schnell
	 * 
	 * @param howLong
	 *            Wie gut ist man unterwegs? langsam, normal, schnell
	 */
	public void setHowGood(HowGood howGood) {
		this.howGood = howGood;
	}

	/* ---------------------------------------------------------------------- */
	/**
	 * Gibt: Wie lange unterwegs? Angabe in Minuten
	 * 
	 * @return Wie lange unterwegs? Angabe in Minuten
	 */
	public int getHowLong() {
		return howLong;
	}

	/**
	 * Setzt: Wie lange unterwegs? Angabe in Minuten
	 * 
	 * @param Wie
	 *            lange unterwegs? Angabe in Minuten
	 */
	public void setHowLong(int howLong) {
		this.howLong = howLong;
	}

	/* ---------------------------------------------------------------------- */
	/**
	 * Gibt: Was will man sehen
	 * 
	 * @return Was will man sehen
	 */
	public List<TargetPointType> getTargetPointTypes() {
		return targetPointTypes;
	}

	/**
	 * F�gt einen Zielpunkt hinzu.
	 * 
	 * @param targetPointTypes
	 *            Zielpunkt
	 */
	public void addTargetPointType(TargetPointType targetPointType) {
		if (!this.targetPointTypes.contains(targetPointType))
			this.targetPointTypes.add(targetPointType);
	}

	/**
	 * F�gt einen Zielpunkt hinzu.
	 * 
	 * @param targetPointTypes
	 *            ID des Zielpunkt-Typs
	 */
	public void addTargetPointType(int targetPointTypeID)
			throws ArrayIndexOutOfBoundsException {
		if (!this.targetPointTypes
				.contains(TargetPointType.values()[targetPointTypeID]))
			this.targetPointTypes
					.add(TargetPointType.values()[targetPointTypeID]);
	}

	/**
	 * L�scht einen Zielpunkt aus der Liste.
	 * 
	 * @param targetPoint
	 *            Zielpunkt, der gel�scht werden soll
	 */
	public void removeTargetPointType(TargetPointType targetPointType) {
		this.targetPointTypes.remove(targetPointType);
	}

	/**
	 * L�scht einen Zielpunkt aus der Liste.
	 * 
	 * @param targetPoint
	 *            Zielpunkt, der gel�scht werden soll
	 */
	public void removeTargetPointType(int targetPointTypeID)
			throws ArrayIndexOutOfBoundsException {
		this.targetPointTypes
				.remove(TargetPointType.values()[targetPointTypeID]);
	}

	/**
	 * Setzt: Was will man sehen
	 * 
	 * @param targetPointTypes
	 *            Was will man sehen
	 */
	public void setTargetPointTypes(List<TargetPointType> targetPointTypes) {
		this.targetPointTypes = targetPointTypes;
	}

	/* ---------------------------------------------------------------------- */
	/**
	 * Gibt die Sprache.
	 * 
	 * @return Sprache
	 */
	public Language getLanguage() {
		return language;
	}

	/**
	 * Setzt die Sprache.
	 * 
	 * @param language
	 *            Sprache
	 */
	public void setLanguage(Language language) {
		this.language = language;
	}

	/* ---------------------------------------------------------------------- */
	/**
	 * Setzt, ob Schnitzeljagd gew�nscht oder nicht.
	 * 
	 * @param isPaperChase
	 *            ? ja (true) oder nein (false)
	 */
	public void setIsPaperChase(boolean isPaperChase) {
		this.isPaperChase = isPaperChase;
	}

	/**
	 * Gibt an, ob Schnitzeljagd gew�nscht ist oder nicht.
	 * 
	 * @return Schnitzjagd? ja (true) oder nein (false)
	 */
	public boolean getIsPaperChase() {
		return this.isPaperChase;
	}

	/* ---------------------------------------------------------------------- */
	/**
	 * Setztdie den Schwierigkeitsgrad der Schnitzeljagd.
	 * 
	 * @param levelOfDifficulty
	 *            Schwierigkeitsgrad
	 */
	public void setLevelOfDifficulty(LevelOfDifficulty levelOfDifficulty) {
		this.levelOfDifficulty = levelOfDifficulty;
	}

	/**
	 * Gibt den Schwierigkeitsgrad der Schnitzeljagd an.
	 * 
	 * @return Schwierigkeitsgrad
	 */
	public LevelOfDifficulty getLevelOfDifficulty() {
		return this.levelOfDifficulty;
	}
	
	/* ---------------------------------------------------------------------- */
	/**
     * Liefert die Geschwindigkeit, je nach dem, wie man unterwegs ist.
     * @param locomotion Art der Fortbewegung
     * @return Geschwindigkeit
     */
    public float getSpeed() {
        return this.locomotion.equals(Locomotion.HOOF_IT)
                ? SPEED_HOOF_IT : this.locomotion.equals(Locomotion.BIKE)
                ? SPEED_BIKE : SPEED_CAR;
    }

	/* ======================================================================= */
	/* Parcelable-Methoden */
	/* ======================================================================= */
	public static final Parcelable.Creator<TourSettings> CREATOR = new Parcelable.Creator<TourSettings>() {

		public TourSettings createFromParcel(Parcel in) {
			return new TourSettings(in);
		}

		public TourSettings[] newArray(int size) {
			return new TourSettings[size];
		}
	};

	private TourSettings(final Parcel in) {
		readFromParcel(in);
	}

	public void writeToParcel(Parcel out, int flags) {
		out.writeString(this.locomotion == null ? null : this.locomotion.name());
		out.writeString(this.howGood == null ? null : this.howGood.name());
		out.writeInt(this.howLong);
		out.writeStringList(getTargetPointTypesAsStringList());
		out.writeInt(this.isPaperChase ? 1 : 0);
		out.writeString(this.levelOfDifficulty == null ? null
				: this.levelOfDifficulty.name());
		out.writeDouble(this.startPoint.getLatitude());
		out.writeDouble(this.startPoint.getLongitude());
		out.writeDouble(this.startPoint.getLatitude());
		out.writeString(this.language == null ? null : this.language.name());
	}

	public void readFromParcel(final Parcel in) {
		this.locomotion = Locomotion.valueOf(in.readString());
		this.howGood = HowGood.valueOf(in.readString());
		this.howLong = in.readInt();
		List<String> targetPointsStringList = new ArrayList<String>();
		in.readStringList(targetPointsStringList);
		createTargetPointTypes(targetPointsStringList);
		this.isPaperChase = in.readInt() == 1;
		this.levelOfDifficulty = LevelOfDifficulty.valueOf(in.readString());
		this.startPoint = new GeoPoint();
		this.startPoint.setLatitude(in.readDouble());
		this.startPoint.setLongitude(in.readDouble());
		this.startPoint.setAltitude(in.readDouble());
		this.language = Language.valueOf(in.readString());
	}

	public int describeContents() {
		return 0;
	}

	private List<String> getTargetPointTypesAsStringList() {
		List<String> targetPointsStringList = new ArrayList<String>();
		for (TargetPointType targetPoint : this.targetPointTypes) {
			targetPointsStringList.add(targetPoint.name());
		}
		return targetPointsStringList;
	}

	private void createTargetPointTypes(List<String> targetPointsStringList) {
		this.targetPointTypes = new ArrayList<TargetPointType>();
		for (String targetPoint : targetPointsStringList) {
			this.targetPointTypes.add(TargetPointType.valueOf(targetPoint));
		}
	}
	
	/* ======================================================================= */
	/* toJSON, toWebServiceJSON */
	/* ======================================================================= */
	/**
	 * Gibt das Objekt als JSON-String.
	 * 
	 * @return JSON-String
	 * @throws JSONException
	 */
	public JSONObject toJSON() throws JSONException {
		return new JSONObject("{\"howGood\":\"" + this.howGood.name()
				+ "\",\"howLong\":" + this.howLong + ",\"isPaperChase\":"
				+ this.isPaperChase + ",\"levelOfDifficulty\":"
				+ this.levelOfDifficulty + ",\"locomotion\":\""
				+ this.locomotion + "\",\"startPoint\":{\"altitude\":"
				+ this.startPoint.getAltitude() + ",\"latitude\":"
				+ this.startPoint.getLatitude() + ",\"longitude\":"
				+ this.startPoint.getLongitude() + "},\"targetPointTypes\":"
				+ getTargetPointTypesForJson() + "}");
	}

	/**
	 * Gibt das Objekt als JSON-String.
	 * 
	 * @return JSON-String
	 * @throws JSONException
	 */
	public JSONObject toWebServiceJSON() throws JSONException {

		return new JSONObject("{\"howGood\":\"" + this.howGood.name()
				+ "\",\"howLong\":" + howLong + ",\"isPaperChase\":"
				+ this.isPaperChase + ",\"language\":\"" + this.language.name()
				+ "\",\"locomotion\":\"" + this.locomotion.name()
				+ "\",\"startPoint\":{\"altitude\":"
				+ this.startPoint.getAltitude() + ",\"latitude\":"
				+ this.startPoint.getLatitude() + ",\"longitude\":"
				+ this.startPoint.getLongitude() + "},\"targetPointTypes\":"
				+ getTargetPointTypesForJson() + "}");
	}

	/**
	 * Parst die gew�nschten Zielpunkttypen nach JSON (Hilfsmethode f�r
	 * toJSON()).
	 * 
	 * @return gew�nschte Zielpunkttypen als JSON-String
	 */
	private String getTargetPointTypesForJson() {
		String targetPointTypesString = "[";
		if (this.targetPointTypes.size() >= 1) {
			targetPointTypesString += "\""
					+ this.targetPointTypes.get(0).name() + "\"";
		}
		for (int i = 1; i < this.targetPointTypes.size(); i++) {
			targetPointTypesString += ",\""
					+ this.targetPointTypes.get(i).name() + "\"";
		}
		return targetPointTypesString + "]";
	}

	/* ======================================================================= */
	/* Hilfsklassen */
	/* ======================================================================= */
	/**
	 * Wie ist man unterwegs.
	 */
	public enum Locomotion {
		HOOF_IT, BIKE, CAR;
	}

	/**
	 * Wie gut ist man unterwegs.
	 */
	public enum HowGood {
		SLOW, NORMAL, FAST;
	}

	/**
	 * Definition der Sprachtypen.
	 */
	public enum Language {
		/** deutsch */
		GERMAN,
		/** englisch */
		ENGLISH,
		/** niedl�ndisch */
		DUTCH,
		/** t�rkisch */
		TURKISH,
		/** Spanisch */
		SPANISH,
		/** franz�sisch */
		FRENCH,
		/** italienisch */
		ITALIAN;
	}
}
