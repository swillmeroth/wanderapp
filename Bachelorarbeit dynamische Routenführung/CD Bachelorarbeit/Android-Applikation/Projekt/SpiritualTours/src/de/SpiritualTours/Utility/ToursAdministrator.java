package de.SpiritualTours.Utility;

import java.util.List;

import de.SpiritualTours.TourObjects.Tour;
import de.SpiritualTours.TourObjects.TourSettings;

/**
 * Singleton-Objekt, welches die Touren verwaltet, die dem User angeboten
 * werden.
 */
public class ToursAdministrator {

	/* ======================================================================= */
	/* Initialisierung der Klasse (bei erstmaligen Zugriff). */
	/* ======================================================================= */
	/**
	 * Instanz der Klasse.
	 */
	private static ToursAdministrator instance = new ToursAdministrator();

	/**
	 * Default-Konstruktor, der nicht au�erhalb dieser Klasse aufgerufen werden
	 * kann.
	 */
	private ToursAdministrator() {
	}

	/**
	 * Liefert die einzige Instanz dieser Klasse zur�ck
	 */
	public static ToursAdministrator getInstance() {
		return instance;
	}

	/* ====================================================================== */
	/* Instanzvariablen */
	/* ====================================================================== */
	/** Touren, die dem User angeboten werden. */
	private List<Tour> tours;
	
	/** Einstellungen zur Tour. */
	private TourSettings tourSettings;

	/* ====================================================================== */
	/* Getter und Setter */
	/* ====================================================================== */
	/**
	 * Gibt die Touren, die dem User angeboten werden.
	 * 
	 * @return Touren
	 */
	public List<Tour> getTours() {
		return tours;
	}

	/**
	 * Setzt die Touren, die dem User angeboten werden.
	 * 
	 * @param tours
	 */
	public void setTours(List<Tour> tours) {
		this.tours = tours;
	}

	/**
	 * Gibt die jeweilige Tour.
	 * 
	 * @param tourID
	 *            ID der gew�nschten Tour
	 * @return Tour
	 */
	public Tour getTour(int tourID) {
		return tours.get(tourID);
	}
	
	/* --------------------------------------------------------------------- */
	/**
	 * Gibt die Einstellungen zur Tour.
	 * 
	 * @return Einstellungen
	 */
	public TourSettings getTourSettings() {
		return tourSettings;
	}

	/**
	 * Setzt die Einstellungen zur Tour.
	 * 
	 * @param settings Einstellungen
	 */
	public void setTourSettings(TourSettings tourSettings) {
		this.tourSettings = tourSettings;
	}	
}
