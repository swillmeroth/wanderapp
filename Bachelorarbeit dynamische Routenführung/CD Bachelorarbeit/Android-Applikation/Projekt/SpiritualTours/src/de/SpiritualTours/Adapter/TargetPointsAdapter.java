package de.SpiritualTours.Adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import de.SpiritualTours.R;
import de.SpiritualTours.TourObjects.TargetPoint;

/**
 * Zur Anzeige der Zielpunkte. Der folgende Link hat bei der Implementierung der
 * Adapterklasse sehr geholfen: http://geekswithblogs.net/bosuch
 * /archive/2011/01/31/android---create-a-custom-
 * multi-line-listview-bound-to-an.aspx.
 */
public class TargetPointsAdapter extends BaseAdapter {

	/** Zielpunkte. */
	private List<TargetPoint> targetPoints;

	/** LayoutInflater. */
	private LayoutInflater mInflater;

	/**
	 * Erzeugt einen TargetPointsAdapter zur Anzeige der Touren.
	 * 
	 * @param context
	 *            Context
	 * @param targetPoints
	 *            Touren
	 */
	public TargetPointsAdapter(Context context, List<TargetPoint> targetPoints) {
		this.targetPoints = targetPoints;
		this.mInflater = LayoutInflater.from(context);
	}

	/**
	 * Gibt die Anzahl der Zielpunkte.
	 * 
	 * @return Anzahl
	 */
	@Override
	public int getCount() {
		return targetPoints.size();
	}

	/**
	 * Gibt den ausgewählten Zielpunkt.
	 * 
	 * @return Zielpunkt
	 */
	@Override
	public TargetPoint getItem(int position) {
		return targetPoints.get(position);
	}

	/**
	 * Gibt die ID des ausgewählten Zielpunkts.
	 * 
	 * @param position
	 *            Index des Zielpunkts in der Liste
	 * @return ID
	 */
	@Override
	public long getItemId(int position) {
		return position;
	}

	/**
	 * Organiersiert die Anzeige in der grafischen Oberfläche.
	 * 
	 * @param pos
	 *            Index der Tour in der Auflistung
	 * @param convertView
	 *            View
	 * @param parent
	 *            übgeordnete Viewgruppe
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;

		if (convertView == null) {

			convertView = mInflater.inflate(R.layout.targetpoint_item, null);
			holder = new ViewHolder();
			holder.targetPointName = (TextView) convertView
					.findViewById(R.id.tpi_targetpoint_name);
			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.targetPointName.setText(this.targetPoints.get(position)
				.getName());

		return convertView;
	}

	/**
	 * Holder, der die GUI-Elemente zur Auflistung der Zielpunkte enthält.
	 */
	static class ViewHolder {
		TextView targetPointName;
	}
}