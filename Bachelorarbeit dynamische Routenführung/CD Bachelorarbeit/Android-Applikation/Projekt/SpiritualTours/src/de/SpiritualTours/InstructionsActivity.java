package de.SpiritualTours;

import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import de.SpiritualTours.Adapter.InstructionsAdapter;
import de.SpiritualTours.TourObjects.Instruction;
import de.SpiritualTours.Utility.ActivityHelper;
import de.SpiritualTours.Utility.IntentConstants;

/**
 * Activity zur Anzeige der Wegbeschreibung.
 */
public class InstructionsActivity extends Activity {

	/** Zielpunkte. */
	private List<Instruction> instructions;
	/** Liste zur Anzeige der Zielpunkte. */
	private ListView instructionsListView;
	/** Zur�ck Button */
	private Button backButton;

	/* ======================================================================= */
	/* OnCreate */
	/* ======================================================================= */
	/**
	 * Erzeugt die Activity zur Anzeige der Zielpunkte.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.instructions);

		getInstructions();
		init();
	}

	/* ======================================================================= */
	/* Initialisieren */
	/* ======================================================================= */
	/**
	 * Liest die �bergebenen Zielpunkte ein.
	 */
	private void getInstructions() {
		this.instructions = this.getIntent().getExtras()
				.getParcelableArrayList(IntentConstants.INSTRUCTIONS);
	}

	/**
	 * Initialisiert die GUI-Kompenenten und setzt die Daten.
	 */
	private void init() {
		this.instructionsListView = (ListView) findViewById(R.id.in_instructions_list);
		this.instructionsListView.setAdapter(new InstructionsAdapter(this,
				this.instructions));

		this.instructionsListView
				.setOnItemClickListener(new OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> a, View v,
							int position, long id) {

						Instruction instruction = (Instruction) instructionsListView
								.getItemAtPosition(position);

						String msg = instruction.getInstruction()
								+ "\n"
								+ InstructionsActivity.this
										.getString(R.string.duration)
								+ " "
								+ instruction.getDuration()
								+ "\n"
								+ InstructionsActivity.this
										.getString(R.string.distance) + " "
								+ instruction.getDistance();

						ActivityHelper.showMessageToast(
								InstructionsActivity.this, msg);
					}
				});

		this.backButton = (Button) findViewById(R.id.in_back);
		this.backButton.setOnClickListener(bBackListener);
	}

	/* ======================================================================= */
	/* Back-Button */
	/* ======================================================================= */
	/**
	 * OnClickListener f�r Back-Button.
	 */
	private OnClickListener bBackListener = new OnClickListener() {

		/**
		 * Wird bei Klick auf den Button aufgerufen.
		 * 
		 * @param v
		 *            View
		 */
		public void onClick(View v) {
			onClickBack(v);
		}
	};

	/**
	 * Activity schliessen und zur Vater-Activity zur�ck.
	 * 
	 * @param v
	 *            View
	 */
	protected void onClickBack(View v) {

		finish();
	}
}
