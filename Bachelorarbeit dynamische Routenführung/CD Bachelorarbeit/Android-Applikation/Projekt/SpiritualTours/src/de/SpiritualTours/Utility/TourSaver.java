package de.SpiritualTours.Utility;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import android.content.Context;
import de.SpiritualTours.R;
import de.SpiritualTours.TourObjects.TargetPoint;
import de.SpiritualTours.TourObjects.TargetPoint.TargetPointType;
import de.SpiritualTours.TourObjects.Tour;
import de.SpiritualTours.TourObjects.Tour.TourType;
import de.SpiritualTours.TourObjects.TourSettings;
import de.SpiritualTours.TourObjects.WayPoint;

/**
 * Speichert die ausgew�hlte Tour in einer KML-Datei auf der SD-Karte ab.
 */
public final class TourSaver {

	/* ====================================================================== */
	/* Konstanten */
	/* ====================================================================== */
	/** Icon der Zielpunkte. */
	private static final String ICON_URL = "http://maps.google.com/mapfiles/kml/pal5/icon13.png";

	/* ---------------------------------------------------------------------- */
	/** Style-Farbe Highlight f�r Friedh�fe. */
	private static final String CEMETERY_STYLE_COLOR = "ff000000";
	/** Style-Farbe f�r Kapellen. */
	private static final String CHAPEL_STYLE_COLOR = "ff008008";
	/** Style-Farbe f�r Kirchen. */
	private static final String CHURCH_STYLE_COLOR = "ff9900bb";
	/** Style-Farbe f�r j�dische Friedh�fe. */
	private static final String JEWISH_CEMETERY_STYLE_COLOR = "ff525252";
	/** Style-Farbe f�r Kl�ster. */
	private static final String MONASTERY_STYLE_COLOR = "ffee7711";
	/** Style-Farbe f�r Denkm�ler. */
	private static final String MONUMENT_STYLE_COLOR = "ff00ffff";
	/** Style-Farbe f�r Standbilder. */
	private static final String STATUE_STYLE_COLOR = "ff0077ff";
	/** Style-Farbe f�r Wegekreuze. */
	private static final String WAYSIDE_CROSS_STYLE_COLOR = "ff20549c";

	/* ---------------------------------------------------------------------- */

	/** Style-ID Highlight f�r Friedh�fe. */
	private static final String CEMETERY_STYLE_H_ID = "cemeteryStyleHighlight";
	/** Style-ID Highlight f�r Kapellen. */
	private static final String CHAPEL_STYLE_H_ID = "chapelStyleHighlight";
	/** Style-ID Highlight f�r Kirchen. */
	private static final String CHURCH_STYLE_H_ID = "churchStyleHighlight";
	/** Style-ID Highlight f�r j�dische Friedh�fe. */
	private static final String JEWISH_CEMETERY_STYLE_H_ID = "jewischCemeteryStyleHighlight";
	/** Style-ID Highlight f�r Kl�ster. */
	private static final String MONASTERY_STYLE_H_ID = "monasteryStyleHighlight";
	/** Style-ID Highlight f�r Denkm�ler. */
	private static final String MONUMENT_STYLE_H_ID = "monumentStyleHighlight";
	/** Style-ID Highlight f�r Standbilder. */
	private static final String STATUE_STYLE_H_ID = "statueStyleHighlight";
	/** Style-ID Highlight f�r Wegekreuze. */
	private static final String WAYSIDE_CROSS_STYLE_H_ID = "waysideCrossStyleHighlight";

	/* ---------------------------------------------------------------------- */

	/** Style-ID Normal f�r Friedh�fe. */
	private static final String CEMETERY_STYLE_N_ID = "cemeteryStyleNormal";
	/** Style-ID Normal f�r Kapellen. */
	private static final String CHAPEL_STYLE_N_ID = "chapelStyleNormal";
	/** Style-ID Normal f�r Kirchen. */
	private static final String CHURCH_STYLE_N_ID = "churchStyleNormal";
	/** Style-ID Normal f�r j�dische Friedh�fe. */
	private static final String JEWISH_CEMETERY_STYLE_N_ID = "jewischCemeteryStyleNormal";
	/** Style-ID Normal f�r Kl�ster. */
	private static final String MONASTERY_STYLE_N_ID = "monasteryStyleNormal";
	/** Style-ID Normal f�r Denkm�ler. */
	private static final String MONUMENT_STYLE_N_ID = "monumentStyleNormal";
	/** Style-ID Normal f�r Standbilder. */
	private static final String STATUE_STYLE_N_ID = "statueStyleNormal";
	/** Style-ID Normal f�r Wegekreuze. */
	private static final String WAYSIDE_CROSS_STYLE_N_ID = "waysideCrossStyleNormal";

	/* ---------------------------------------------------------------------- */

	/** Style f�r Friedh�fe */
	private static final String CEMETERY_STYLE = "<Style id=\""
			+ CEMETERY_STYLE_H_ID
			+ "\">\n"
			+ "<IconStyle>\n"
			+ "<color>"
			+ CEMETERY_STYLE_COLOR
			+ "</color>\n"
			+ "<scale>1.3</scale>\n"
			+ "<Icon>\n"
			+ "<href>"
			+ ICON_URL
			+ "</href>\n"
			+ "</Icon>\n"
			+ "<hotSpot x=\"20\" y=\"2\" xunits=\"pixels\" yunits=\"pixels\"/>\n"
			+ "</IconStyle>\n"
			+ "</Style>\n"
			+ "<Style id=\""
			+ CEMETERY_STYLE_N_ID
			+ "\">\n"
			+ "<IconStyle>\n"
			+ "<color>"
			+ CEMETERY_STYLE_COLOR
			+ "</color>\n"
			+ "<scale>1.1</scale>\n"
			+ "<Icon>\n"
			+ "<href>"
			+ ICON_URL
			+ "</href>\n"
			+ "</Icon>\n"
			+ "<hotSpot x=\"20\" y=\"2\" xunits=\"pixels\" yunits=\"pixels\"/>\n"
			+ "</IconStyle>\n" + "</Style>";

	/** Style f�r Kapellen */
	private static final String CHAPEL_STYLE = "<Style id=\""
			+ CHAPEL_STYLE_H_ID
			+ "\">\n"
			+ "<IconStyle>\n"
			+ "<color>"
			+ CHAPEL_STYLE_COLOR
			+ "</color>\n"
			+ "<scale>1.3</scale>\n"
			+ "<Icon>\n"
			+ "<href>"
			+ ICON_URL
			+ "</href>\n"
			+ "</Icon>\n"
			+ "<hotSpot x=\"20\" y=\"2\" xunits=\"pixels\" yunits=\"pixels\"/>\n"
			+ "</IconStyle>\n"
			+ "</Style>\n"
			+ "<Style id=\""
			+ CHAPEL_STYLE_N_ID
			+ "\">\n"
			+ "<IconStyle>\n"
			+ "<color>"
			+ CHAPEL_STYLE_COLOR
			+ "</color>\n"
			+ "<scale>1.1</scale>\n"
			+ "<Icon>\n"
			+ "<href>"
			+ ICON_URL
			+ "</href>\n"
			+ "</Icon>\n"
			+ "<hotSpot x=\"20\" y=\"2\" xunits=\"pixels\" yunits=\"pixels\"/>\n"
			+ "</IconStyle>\n" + "</Style>";

	/** Style f�r Kirchen */
	private static final String CHURCH_STYLE = "<Style id=\""
			+ CHURCH_STYLE_H_ID
			+ "\">\n"
			+ "<IconStyle>\n"
			+ "<color>"
			+ CHURCH_STYLE_COLOR
			+ "</color>\n"
			+ "<scale>1.3</scale>\n"
			+ "<Icon>\n"
			+ "<href>"
			+ ICON_URL
			+ "</href>\n"
			+ "</Icon>\n"
			+ "<hotSpot x=\"20\" y=\"2\" xunits=\"pixels\" yunits=\"pixels\"/>\n"
			+ "</IconStyle>\n"
			+ "</Style>\n"
			+ "<Style id=\""
			+ CHURCH_STYLE_N_ID
			+ "\">\n"
			+ "<IconStyle>\n"
			+ "<color>"
			+ CHURCH_STYLE_COLOR
			+ "</color>\n"
			+ "<scale>1.1</scale>\n"
			+ "<Icon>\n"
			+ "<href>"
			+ ICON_URL
			+ "</href>\n"
			+ "</Icon>\n"
			+ "<hotSpot x=\"20\" y=\"2\" xunits=\"pixels\" yunits=\"pixels\"/>\n"
			+ "</IconStyle>\n" + "</Style>";

	/** Style f�r j�dische Friedh�fe */
	private static final String JEWISH_CEMETERY_STYLE = "<Style id=\""
			+ JEWISH_CEMETERY_STYLE_H_ID
			+ "\">\n"
			+ "<IconStyle>\n"
			+ "<color>"
			+ JEWISH_CEMETERY_STYLE_COLOR
			+ "</color>\n"
			+ "<scale>1.3</scale>\n"
			+ "<Icon>\n"
			+ "<href>"
			+ ICON_URL
			+ "</href>\n"
			+ "</Icon>\n"
			+ "<hotSpot x=\"20\" y=\"2\" xunits=\"pixels\" yunits=\"pixels\"/>\n"
			+ "</IconStyle>\n"
			+ "</Style>\n"
			+ "<Style id=\""
			+ JEWISH_CEMETERY_STYLE_N_ID
			+ "\">\n"
			+ "<IconStyle>\n"
			+ "<color>"
			+ JEWISH_CEMETERY_STYLE_COLOR
			+ "</color>\n"
			+ "<scale>1.1</scale>\n"
			+ "<Icon>\n"
			+ "<href>"
			+ ICON_URL
			+ "</href>\n"
			+ "</Icon>\n"
			+ "<hotSpot x=\"20\" y=\"2\" xunits=\"pixels\" yunits=\"pixels\"/>\n"
			+ "</IconStyle>\n" + "</Style>";

	/** Style f�r Kl�ster */
	private static final String MONASTERY_STYLE = "<Style id=\""
			+ MONASTERY_STYLE_H_ID
			+ "\">\n"
			+ "<IconStyle>\n"
			+ "<color>"
			+ MONASTERY_STYLE_COLOR
			+ "</color>\n"
			+ "<scale>1.3</scale>\n"
			+ "<Icon>\n"
			+ "<href>"
			+ ICON_URL
			+ "</href>\n"
			+ "</Icon>\n"
			+ "<hotSpot x=\"20\" y=\"2\" xunits=\"pixels\" yunits=\"pixels\"/>\n"
			+ "</IconStyle>\n"
			+ "</Style>\n"
			+ "<Style id=\""
			+ MONASTERY_STYLE_N_ID
			+ "\">\n"
			+ "<IconStyle>\n"
			+ "<color>"
			+ MONASTERY_STYLE_COLOR
			+ "</color>\n"
			+ "<scale>1.1</scale>\n"
			+ "<Icon>\n"
			+ "<href>"
			+ ICON_URL
			+ "</href>\n"
			+ "</Icon>\n"
			+ "<hotSpot x=\"20\" y=\"2\" xunits=\"pixels\" yunits=\"pixels\"/>\n"
			+ "</IconStyle>\n" + "</Style>";

	/** Style f�r Denkm�ler */
	private static final String MONUMENT_STYLE = "<Style id=\""
			+ MONUMENT_STYLE_H_ID
			+ "\">\n"
			+ "<IconStyle>\n"
			+ "<color>"
			+ MONUMENT_STYLE_COLOR
			+ "</color>\n"
			+ "<scale>1.3</scale>\n"
			+ "<Icon>\n"
			+ "<href>"
			+ ICON_URL
			+ "</href>\n"
			+ "</Icon>\n"
			+ "<hotSpot x=\"20\" y=\"2\" xunits=\"pixels\" yunits=\"pixels\"/>\n"
			+ "</IconStyle>\n"
			+ "</Style>\n"
			+ "<Style id=\""
			+ MONUMENT_STYLE_N_ID
			+ "\">\n"
			+ "<IconStyle>\n"
			+ "<color>"
			+ MONUMENT_STYLE_COLOR
			+ "</color>\n"
			+ "<scale>1.1</scale>\n"
			+ "<Icon>\n"
			+ "<href>"
			+ ICON_URL
			+ "</href>\n"
			+ "</Icon>\n"
			+ "<hotSpot x=\"20\" y=\"2\" xunits=\"pixels\" yunits=\"pixels\"/>\n"
			+ "</IconStyle>\n" + "</Style>";

	/** Style f�r Standbilder */
	private static final String STATUE_STYLE = "<Style id=\""
			+ STATUE_STYLE_H_ID
			+ "\">\n"
			+ "<IconStyle>\n"
			+ "<color>"
			+ STATUE_STYLE_COLOR
			+ "</color>\n"
			+ "<scale>1.3</scale>\n"
			+ "<Icon>\n"
			+ "<href>"
			+ ICON_URL
			+ "</href>\n"
			+ "</Icon>\n"
			+ "<hotSpot x=\"20\" y=\"2\" xunits=\"pixels\" yunits=\"pixels\"/>\n"
			+ "</IconStyle>\n"
			+ "</Style>\n"
			+ "<Style id=\""
			+ STATUE_STYLE_N_ID
			+ "\">\n"
			+ "<IconStyle>\n"
			+ "<color>"
			+ STATUE_STYLE_COLOR
			+ "</color>\n"
			+ "<scale>1.1</scale>\n"
			+ "<Icon>\n"
			+ "<href>"
			+ ICON_URL
			+ "</href>\n"
			+ "</Icon>\n"
			+ "<hotSpot x=\"20\" y=\"2\" xunits=\"pixels\" yunits=\"pixels\"/>\n"
			+ "</IconStyle>\n" + "</Style>";

	/** Style f�r Wegekreuze */
	private static final String WAYSIDE_CROSS_STYLE = "<Style id=\""
			+ WAYSIDE_CROSS_STYLE_H_ID
			+ "\">\n"
			+ "<IconStyle>\n"
			+ "<color>"
			+ WAYSIDE_CROSS_STYLE_COLOR
			+ "</color>\n"
			+ "<scale>1.3</scale>\n"
			+ "<Icon>\n"
			+ "<href>"
			+ ICON_URL
			+ "</href>\n"
			+ "</Icon>\n"
			+ "<hotSpot x=\"20\" y=\"2\" xunits=\"pixels\" yunits=\"pixels\"/>\n"
			+ "</IconStyle>\n"
			+ "</Style>\n"
			+ "<Style id=\""
			+ WAYSIDE_CROSS_STYLE_N_ID
			+ "\">\n"
			+ "<IconStyle>\n"
			+ "<color>"
			+ WAYSIDE_CROSS_STYLE_COLOR
			+ "</color>\n"
			+ "<scale>1.1</scale>\n"
			+ "<Icon>\n"
			+ "<href>"
			+ ICON_URL
			+ "</href>\n"
			+ "</Icon>\n"
			+ "<hotSpot x=\"20\" y=\"2\" xunits=\"pixels\" yunits=\"pixels\"/>\n"
			+ "</IconStyle>\n" + "</Style>";

	/* ---------------------------------------------------------------------- */

	/** Style-Map-ID f�r Friedh�fe. */
	private static final String CEMETERY_STYLE_MAP_ID = "cemeteryStyleMap";
	/** Style-Map-ID f�r Kapellen. */
	private static final String CHAPEL_STYLE_MAP_ID = "chapelStyleMap";
	/** Style-Map-ID f�r Kirchen. */
	private static final String CHURCH_STYLE_MAP_ID = "churchStyleMap";
	/** Style-Map-ID f�r j�dische Friedh�fe. */
	private static final String JEWISH_CEMETERY_STYLE_MAP_ID = "jewischCemeteryStyleMap";
	/** Style-Map-ID f�r Kl�ster. */
	private static final String MONASTERY_STYLE_MAP_ID = "monasteryStyleMap";
	/** Style-Map-ID f�r Denkm�ler. */
	private static final String MONUMENT_STYLE_MAP_ID = "monumentStyleMap";
	/** Style-Map-ID f�r Standbilder. */
	private static final String STATUE_STYLE_MAP_ID = "statueStyleMap";
	/** Style-Map-ID f�r Wegekreuze. */
	private static final String WAYSIDE_CROSS_STYLE_MAP_ID = "waysideCrossStyleMap";

	/* ---------------------------------------------------------------------- */

	/** Style-Map f�r Friedh�fe. */
	private static final String CEMETERY_STYLE_MAP = "<StyleMap id=\""
			+ CEMETERY_STYLE_MAP_ID + "\">\n" + "<Pair>\n"
			+ "<key>normal</key>\n" + "<styleUrl>#" + CEMETERY_STYLE_N_ID
			+ "</styleUrl>\n" + "</Pair>\n" + "<Pair>\n"
			+ "<key>highlight</key>\n" + "<styleUrl>#" + CEMETERY_STYLE_H_ID
			+ "</styleUrl>\n" + "</Pair>\n" + "</StyleMap>";

	/** Style-Map f�r Kapellen. */
	private static final String CHAPEL_STYLE_MAP = "<StyleMap id=\""
			+ CHAPEL_STYLE_MAP_ID + "\">\n" + "<Pair>\n"
			+ "<key>normal</key>\n" + "<styleUrl>#" + CHAPEL_STYLE_N_ID
			+ "</styleUrl>\n" + "</Pair>\n" + "<Pair>\n"
			+ "<key>highlight</key>\n" + "<styleUrl>#" + CHAPEL_STYLE_H_ID
			+ "</styleUrl>\n" + "</Pair>\n" + "</StyleMap>";

	/** Style-Map f�r Kirchen. */
	private static final String CHURCH_STYLE_MAP = "<StyleMap id=\""
			+ CHURCH_STYLE_MAP_ID + "\">\n" + "<Pair>\n"
			+ "<key>normal</key>\n" + "<styleUrl>#" + CHURCH_STYLE_N_ID
			+ "</styleUrl>\n" + "</Pair>\n" + "<Pair>\n"
			+ "<key>highlight</key>\n" + "<styleUrl>#" + CHURCH_STYLE_H_ID
			+ "</styleUrl>\n" + "</Pair>\n" + "</StyleMap>";

	/** Style-Map f�r j�dische Friedh�fe. */
	private static final String JEWISH_CEMETERY_STYLE_MAP = "<StyleMap id=\""
			+ JEWISH_CEMETERY_STYLE_MAP_ID + "\">\n" + "<Pair>\n"
			+ "<key>normal</key>\n" + "<styleUrl>#"
			+ JEWISH_CEMETERY_STYLE_N_ID + "</styleUrl>\n" + "</Pair>\n"
			+ "<Pair>\n" + "<key>highlight</key>\n" + "<styleUrl>#"
			+ JEWISH_CEMETERY_STYLE_H_ID + "</styleUrl>\n" + "</Pair>\n"
			+ "</StyleMap>";

	/** Style-Map f�r Kl�ster. */
	private static final String MONASTERY_STYLE_MAP = "<StyleMap id=\""
			+ MONASTERY_STYLE_MAP_ID + "\">\n" + "<Pair>\n"
			+ "<key>normal</key>\n" + "<styleUrl>#" + MONASTERY_STYLE_N_ID
			+ "</styleUrl>\n" + "</Pair>\n" + "<Pair>\n"
			+ "<key>highlight</key>\n" + "<styleUrl>#" + MONASTERY_STYLE_H_ID
			+ "</styleUrl>\n" + "</Pair>\n" + "</StyleMap>";

	/** Style-Map f�r Denkm�ler. */
	private static final String MONUMENT_STYLE_MAP = "<StyleMap id=\""
			+ MONUMENT_STYLE_MAP_ID + "\">\n" + "<Pair>\n"
			+ "<key>normal</key>\n" + "<styleUrl>#" + MONUMENT_STYLE_N_ID
			+ "</styleUrl>\n" + "</Pair>\n" + "<Pair>\n"
			+ "<key>highlight</key>\n" + "<styleUrl>#" + MONUMENT_STYLE_H_ID
			+ "</styleUrl>\n" + "</Pair>\n" + "</StyleMap>";

	/** Style-Map f�r Standbilder. */
	private static final String STATUE_STYLE_MAP = "<StyleMap id=\""
			+ STATUE_STYLE_MAP_ID + "\">\n" + "<Pair>\n"
			+ "<key>normal</key>\n" + "<styleUrl>#" + STATUE_STYLE_N_ID
			+ "</styleUrl>\n" + "</Pair>\n" + "<Pair>\n"
			+ "<key>highlight</key>\n" + "<styleUrl>#" + STATUE_STYLE_H_ID
			+ "</styleUrl>\n" + "</Pair>\n" + "</StyleMap>";

	/** Style-Map f�r Wegekreuze. */
	private static final String WAYSIDE_CROSS_STYLE_MAP = "<StyleMap id=\""
			+ WAYSIDE_CROSS_STYLE_MAP_ID + "\">\n" + "<Pair>\n"
			+ "<key>normal</key>\n" + "<styleUrl>#" + WAYSIDE_CROSS_STYLE_N_ID
			+ "</styleUrl>\n" + "</Pair>\n" + "<Pair>\n"
			+ "<key>highlight</key>\n" + "<styleUrl>#"
			+ WAYSIDE_CROSS_STYLE_H_ID + "</styleUrl>\n" + "</Pair>\n"
			+ "</StyleMap>";

	/* ---------------------------------------------------------------------- */

	/** Anfang der KML-Datei. */
	private static final String KML_HEADER = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
			+ "<kml xmlns=\"http://www.opengis.net/kml/2.2\">\n" + "<Document>";

	/** Ende der KML-Datei. */
	private static final String KML_FOOTER = "</Document>\n" + "</kml>";

	/** Ordner, in dem die KML Datein abgelegt werden. */
	private static final String KML_FOLDER = "/sdcard/spiritual_tours";

	/**
	 * Speichert die Tour als KML-Datei.
	 * 
	 * @param context
	 *            Context
	 * @param tour
	 *            Tour
	 */
	public static final void saveTour(Context context, Tour tour) {

		String filename = getFilename();

		try {
			/* anlegen der KML-Datei auf der SD-Karte. */
			FileWriter kmlWriter = new FileWriter(KML_FOLDER + "/" + filename,
					false);
			BufferedWriter out = new BufferedWriter(kmlWriter);

			/* Kopf der KML-Datei */
			out.write(KML_HEADER);
			out.newLine();

			/* Name der KML-Datei */
			out.write(getKMLName(context, tour.getTourType()));
			out.newLine();

			/* Beschreibung der KML-Datei */
			out.write(getDescription(context, tour));
			out.newLine();

			/* Style der Linien der KML-Datei */
			out.write(getLineStyle());
			out.newLine();

			/* Styles der Zielpunkte */
			out.write(getStyles());
			out.newLine();

			/* Wegpunkte der KML-Datei setzen */
			out.write(getRoute(tour.getWayPoints()));
			out.newLine();

			/* Startpunkt der KML-Datei setzen */
			out.write(getStartEndPoint(context, tour.getTourType(), tour
					.getWayPoints().get(0)));
			out.newLine();

			/* Zielpunkte der KML-Datei setzen */
			out.write(getTargetPoints(context, tour.getTargetPoints()));

			/* Wegpunkte der KML-Datei setzen */
			out.write(KML_FOOTER);

			out.close();
			kmlWriter.close();

			ActivityHelper.showMessageToast(context, context.getString(R.string.ts_save_success_part1) + " "+ filename
							+ " " + context.getString(R.string.ts_save_success_part2));

		} catch (IOException e) {
			ActivityHelper.showMessageToast(context, context.getString(R.string.ts_save_failed));
		}
	}

	/**
	 * Gibt den Namen der KML-Datei.
	 * 
	 * @return Name der KML-Datei
	 */
	private static String getFilename() {

		/* Ordner erzeugen, in der die KML-Dateien liegen */
		File kmlDirectory = new File(KML_FOLDER);
		/* sofern der Pfad noch nich existiert, wird dieser erzeugt */
		kmlDirectory.mkdirs();

		/* Beginn des Dateinamens */		
		Calendar calendar = Calendar.getInstance();
		final String filename = (calendar.get(Calendar.YEAR)) + "-"
				+ (calendar.get(Calendar.MONTH) + 1) + "-"
				+ calendar.get(Calendar.DATE);

		/* Filter zur �berpr�fung, welche Datei bereits zu diesem Datum bestehen */
		FilenameFilter filter = new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.startsWith(filename);
			}
		};

		return filename + "_" + (kmlDirectory.list(filter).length + 1) + ".kml";
	}

	/**
	 * Gibt den Namen der KML.
	 * 
	 * @param context
	 *            Context
	 * @param tourType
	 *            Typ der Tour
	 * @return Name der KML
	 */
	private static String getKMLName(Context context, TourType tourType) {
		return "<name>"
				+ (tourType.equals(TourType.FLATLY) ? context
						.getString(R.string.flatly) : context
						.getString(R.string.route)) + " - "
				+ new GregorianCalendar().getTime().toString() + "</name>";
	}

	/**
	 * Gibt die Beschreibung der KML.
	 * 
	 * @param context
	 *            Context
	 * @param tour
	 *            Tour
	 * 
	 * @return Beschreibung der KML
	 */
	private static String getDescription(Context context, Tour tour) {

		/* Tour-Einstellungen */
		String settingsDescription = "--------------------------------------------------------------\n";
		settingsDescription += context.getString(R.string.sf_l_settings) + "\n";

		TourSettings settings = ToursAdministrator.getInstance()
				.getTourSettings();

		settingsDescription += context.getString(R.string.q_how_do_you_go)
				+ " "
				+ ActivityHelper.getLocomotion(context,
						settings.getLocomotion()) + "\n";

		settingsDescription += context
				.getString(R.string.q_how_good_are_you_traveling)
				+ " "
				+ ActivityHelper.getHowGood(context, settings.getHowGood())
				+ "\n";

		settingsDescription += context
				.getString(R.string.cs_target_point_types_title)
				+ ":\n"
				+ getSelectedTargetPointTypes(context,
						settings.getTargetPointTypes()) + "\n";

		settingsDescription += context.getString(R.string.duration) + ": "
				+ ActivityHelper.getTime(context, tour.getEstimatedDuration(), false)
				+ "\n";

		settingsDescription += "--------------------------------------------------------------\n";

		return "<description>\n" + settingsDescription
				+ context.getString(R.string.distance) + ": "
				+ ActivityHelper.getDistance(tour.getEstimatedTourLength())
				+ "\n" + context.getString(R.string.targetpoints) + ": " + "\n"
				+ getTargetPointNames(tour.getTargetPoints())
				+ "\n</description>";
	}

	/**
	 * Gibt die ausgew�hlten Zielpunkttypen als String.
	 * 
	 * @param context
	 *            Context
	 * @param targetPointTypes
	 *            ausgew�hlte Zielpunkttypen
	 * @return ausgew�hlte Zielpunkttypen als String
	 */
	private static String getSelectedTargetPointTypes(Context context,
			List<TargetPointType> targetPointTypes) {

		String selectedTargetPointTypes = "";
		for (int i = 0; i < targetPointTypes.size() - 1; i++) {
			selectedTargetPointTypes += "- "
					+ ActivityHelper.getTargetPointTypeName(context,
							targetPointTypes.get(i)) + "\n";
		}
		return selectedTargetPointTypes
				+ "- "
				+ ActivityHelper.getTargetPointTypeName(context,
						targetPointTypes.get(targetPointTypes.size() - 1));
	}

	/**
	 * Gibt die Zielpunktnamen.
	 * 
	 * @param targetPoints
	 *            Zielpunkte
	 * @return Namen der Zielpunkte
	 */
	private static String getTargetPointNames(List<TargetPoint> targetPoints) {

		String targetPointNames = "";
		for (int i = 0; i < targetPoints.size() - 1; i++) {
			targetPointNames += targetPoints.get(i).getName() + "\n";
		}
		return targetPointNames
				+ targetPoints.get(targetPoints.size() - 1).getName();
	}

	/**
	 * Gibt den Style f�r die Strecke.
	 * 
	 * @return Style f�r die Strecke.
	 */
	private static String getLineStyle() {
		return "<Style id=\"lineStyle\">\n" + "<LineStyle>\n"
				+ "<color>ffffffff</color>\n" + "<width>4</width>\n"
				+ "</LineStyle>\n" + "<PolyStyle>\n"
				+ "<color>7f00ff00</color>\n" + "</PolyStyle>\n" + "</Style>";
	}

	/**
	 * Gibt die Styles f�r die KML-Datei.
	 * 
	 * @return Styles
	 */
	private static String getStyles() {
		return CEMETERY_STYLE + "\n" + CHAPEL_STYLE + "\n" + CHURCH_STYLE
				+ "\n" + JEWISH_CEMETERY_STYLE + "\n" + MONASTERY_STYLE + "\n"
				+ MONUMENT_STYLE + "\n" + STATUE_STYLE + "\n"
				+ WAYSIDE_CROSS_STYLE + "\n" + CEMETERY_STYLE_MAP + "\n"
				+ CHAPEL_STYLE_MAP + "\n" + CHURCH_STYLE_MAP + "\n"
				+ JEWISH_CEMETERY_STYLE_MAP + "\n" + MONASTERY_STYLE_MAP + "\n"
				+ MONUMENT_STYLE_MAP + "\n" + STATUE_STYLE_MAP + "\n"
				+ WAYSIDE_CROSS_STYLE_MAP;
	}

	/**
	 * Gibt die Route.
	 * 
	 * @return Route
	 */
	private static String getRoute(List<WayPoint> wayPoints) {
		String route = "<Placemark>\n" + "<name>" + "Strecke" + "</name>\n"
				+ "<description>" + "Strecke der Tour" + "</description>\n"
				+ "<styleUrl>#lineStyle</styleUrl>\n" + "<LineString>\n"
				+ "<extrude>1</extrude>\n" + "<tessellate>1</tessellate>\n"
				+ "<altitudeMode>relativeToGround</altitudeMode>\n"
				+ "<coordinates>\n";

		for (WayPoint wayPoint : wayPoints) {
			route += wayPoint.getGeoPoint().getLongitude() + ", "
					+ wayPoint.getGeoPoint().getLatitude() + "\n";
		}

		return route + "</coordinates>\n" + "</LineString>\n" + "</Placemark>";
	}

	/**
	 * Gibt den Startpunkt der Tour.
	 * 
	 * @param context
	 *            Context
	 * @param tourType
	 *            Tour-Typ
	 * @param wayPoint
	 *            Wegpunkt
	 * @return StartPunkt
	 */
	private static String getStartEndPoint(Context context, TourType tourType,
			WayPoint wayPoint) {

		String name = tourType.equals(TourType.FLATLY) ? context
				.getString(R.string.start_end) : context
				.getString(R.string.start);
		String description = tourType.equals(TourType.FLATLY) ? context
				.getString(R.string.description_start_end) : context
				.getString(R.string.description_start);

		return "<Placemark>\n" + "<name>" + name + "</name>\n"
				+ "<description>\n" + description + "\n</description>\n"
				+ "<Point\n>" + "<coordinates>"
				+ wayPoint.getGeoPoint().getLongitude() + ", "
				+ wayPoint.getGeoPoint().getLatitude() + "</coordinates>\n"
				+ "</Point>\n" + "</Placemark>";
	}

	/**
	 * Gibt die Zielpunkte.
	 * 
	 * @param context
	 *            Context
	 * @param targetPoint
	 *            Zielpunkte
	 * @return Zielpunkte
	 */
	private static String getTargetPoints(Context context,
			List<TargetPoint> targetPoints) {

		String points = "";

		for (TargetPoint targetPoint : targetPoints) {
			points += getTargetPoint(context, targetPoint);
		}

		return points;
	}

	/**
	 * Gibt den Zielpunkt.
	 * 
	 * @param context
	 *            Context
	 * @param targetPoint
	 *            Zielpunkt
	 * @return Zielpunkt
	 */
	private static String getTargetPoint(Context context,
			TargetPoint targetPoint) {

		String targetPointType = ActivityHelper.getTargetPointTypeName(context,
				targetPoint.getTargetPointType());

		return "<Placemark>\n" + "<name>" + targetPoint.getName() + "</name>\n"
				+ getStyleMap(targetPoint.getTargetPointType()) + "\n"
				+ "<description>\n"
				+ context.getString(R.string.targetpointtype) + ": "
				+ targetPointType + "\n" + targetPoint.getInformation()
				+ "\n</description>\n" + "<Point\n>" + "<coordinates>"
				+ targetPoint.getGeoPoint().getLongitude() + ", "
				+ targetPoint.getGeoPoint().getLatitude() + "</coordinates>\n"
				+ "</Point>\n" + "</Placemark>\n";
	}

	/**
	 * Gibt die passende StyleMap.
	 * 
	 * @return StyleMap
	 */
	private static String getStyleMap(TargetPointType targetPointType) {

		String styleMap = "";

		switch (targetPointType) {
		case CEMETERY:
			styleMap = "<styleUrl>#" + CEMETERY_STYLE_MAP_ID + "</styleUrl>";
			break;
		case CHAPEL:
			styleMap = "<styleUrl>#" + CHAPEL_STYLE_MAP_ID + "</styleUrl>";
			break;
		case CHURCH:
			styleMap = "<styleUrl>#" + CHURCH_STYLE_MAP_ID + "</styleUrl>";
			break;
		case JEWISH_CEMETERY:
			styleMap = "<styleUrl>#" + JEWISH_CEMETERY_STYLE_MAP_ID
					+ "</styleUrl>";
			break;
		case MONASTERY:
			styleMap = "<styleUrl>#" + MONASTERY_STYLE_MAP_ID + "</styleUrl>";
			break;
		case MONUMENT:
			styleMap = "<styleUrl>#" + MONUMENT_STYLE_MAP_ID + "</styleUrl>";
			break;
		case STATUE:
			styleMap = "<styleUrl>#" + STATUE_STYLE_MAP_ID + "</styleUrl>";
			break;
		case WAYSIDE_CROSS:
			styleMap = "<styleUrl>#" + WAYSIDE_CROSS_STYLE_MAP_ID
					+ "</styleUrl>";
			break;
		default:
			styleMap = "";
			break;
		}

		return styleMap;
	}
}
