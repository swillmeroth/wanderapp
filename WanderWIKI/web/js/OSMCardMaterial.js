/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function initialize() {

 var markers = [];
  
  var map = new google.maps.Map(document.getElementsByID('k'), {
            center: new google.maps.LatLng(51.960453, 7.625470),
            zoom: 15,
            mapTypeId: "OSM",
            streetViewControl : false            
  });

        
    map.mapTypes.set("OSM", new google.maps.ImageMapType({
        getTileUrl: function(coord, zoom) {
            return "http://tile.openstreetmap.org/" + zoom + "/" + coord.x + "/" + coord.y + ".png";
            //outdoor hike and mountain bike map
            //return "http://4umaps.eu/" + zoom + "/" + coord.x + "/" + coord.y + ".png";
        },
        tileSize: new google.maps.Size(256, 256),
        name: "OpenStreetMap",
        maxZoom: 18
    }));   
     
}