-- MySQL Script generated by MySQL Workbench
-- 07/31/14 13:21:57
-- Model: New Model    Version: 1.0
SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema wanderwiki
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `wanderwiki` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `wanderwiki` ;

-- -----------------------------------------------------
-- Table `wanderwiki`.`Places`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `wanderwiki`.`Places` (
  `idPlaces` INT NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  `type` VARCHAR(45) NULL,
  `date_added` DATETIME NULL,
  `latitude` DECIMAL(10) NULL,
  `longtitude` DECIMAL(10) NULL,
  `altitude` INT NULL,
  `postal_code` VARCHAR(20) NULL,
  `city` VARCHAR(58) NULL,
  `street` VARCHAR(45) NULL,
  `size` INT NULL,
  `open_times` TEXT NULL,
  `isProposal` TINYINT(1) NULL DEFAULT 0,
  `isChange` TINYINT(1) NULL DEFAULT 0,
  `idChanging` INT NULL,
  PRIMARY KEY (`idPlaces`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `wanderwiki`.`Routes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `wanderwiki`.`Routes` (
  `idRoutes` INT NOT NULL,
  `name` VARCHAR(45) NULL,
  `distance` VARCHAR(45) NULL,
  `duration` VARCHAR(45) NULL,
  `difficulty` INT NULL,
  `type` VARCHAR(45) NULL,
  `equipment` TEXT NULL,
  `start` INT NULL,
  `end` INT NULL,
  `isProposed` TINYINT(1) NULL DEFAULT 0,
  `isChange` TINYINT(1) NULL DEFAULT 0,
  `idChanging` INT NULL,
  PRIMARY KEY (`idRoutes`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `wanderwiki`.`RoutePoints`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `wanderwiki`.`RoutePoints` (
  `idRoutePoints` INT NOT NULL,
  `idRoute` INT NULL,
  `latitude` DECIMAL NULL,
  `longtitude` DECIMAL NULL,
  PRIMARY KEY (`idRoutePoints`),
  INDEX `idRoute_idx` (`idRoute` ASC),
  CONSTRAINT `idRoute`
    FOREIGN KEY (`idRoute`)
    REFERENCES `wanderwiki`.`Routes` (`idRoutes`)
    ON DELETE SET NULL
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `wanderwiki`.`PlacesRoutes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `wanderwiki`.`PlacesRoutes` (
  `idPlacesRoutes` INT NOT NULL,
  `idPlace` INT NULL,
  `idRoute` INT NULL,
  PRIMARY KEY (`idPlacesRoutes`),
  INDEX `idRoute_idx` (`idRoute` ASC),
  INDEX `idPlace_idx` (`idPlace` ASC),
  UNIQUE INDEX `idPlacesRoutes_UNIQUE` (`idPlacesRoutes` ASC),
  CONSTRAINT `idRoute1`
    FOREIGN KEY (`idRoute`)
    REFERENCES `wanderwiki`.`Routes` (`idRoutes`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `idPlace1`
    FOREIGN KEY (`idPlace`)
    REFERENCES `wanderwiki`.`Places` (`idPlaces`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `wanderwiki`.`Users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `wanderwiki`.`Users` (
  `userid` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `password_hash` VARCHAR(45) NOT NULL,
  `verificated` TINYINT(1) NULL DEFAULT 0,
  `interest` VARCHAR(45) NULL,
  `language` VARCHAR(45) NULL,
  `walking_speed` VARCHAR(45) NULL,
  `wheelchair` TINYINT(1) NULL,
  `bigfont` TINYINT(1) NULL,
  PRIMARY KEY (`userid`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `wanderwiki`.`Questions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `wanderwiki`.`Questions` (
  `idquestions` INT NOT NULL,
  `idPlace` INT NULL,
  `question` TEXT NULL,
  `right` VARCHAR(45) NULL,
  `wrong_1` VARCHAR(45) NULL,
  `wrong_2` VARCHAR(45) NULL,
  `wrong_3` VARCHAR(45) NULL,
  PRIMARY KEY (`idquestions`),
  INDEX `idPlace_idx` (`idPlace` ASC),
  CONSTRAINT `idPlace`
    FOREIGN KEY (`idPlace`)
    REFERENCES `wanderwiki`.`Places` (`idPlaces`)
    ON DELETE SET NULL
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `wanderwiki`.`ToDos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `wanderwiki`.`ToDos` (
  `idToDos` INT NOT NULL,
  `idRoute` INT NULL,
  `idPlace` INT NULL,
  `idUser` INT NULL,
  PRIMARY KEY (`idToDos`),
  INDEX `idRoute_idx` (`idRoute` ASC),
  INDEX `idPlace_idx` (`idPlace` ASC),
  INDEX `idUser_idx` (`idUser` ASC),
  CONSTRAINT `idRoute2`
    FOREIGN KEY (`idRoute`)
    REFERENCES `wanderwiki`.`Routes` (`idRoutes`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `idPlace2`
    FOREIGN KEY (`idPlace`)
    REFERENCES `wanderwiki`.`Places` (`idPlaces`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `idUser2`
    FOREIGN KEY (`idUser`)
    REFERENCES `wanderwiki`.`Users` (`userid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `wanderwiki`.`Comments`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `wanderwiki`.`Comments` (
  `idComments` INT NOT NULL,
  `idPlace` INT NULL,
  `idRoute` INT NULL,
  `idUser` INT NULL,
  `time` DATETIME NULL,
  `author` VARCHAR(45) NULL,
  `text` TEXT NULL,
  PRIMARY KEY (`idComments`),
  INDEX `place_idx` (`idPlace` ASC),
  INDEX `route_idx` (`idRoute` ASC),
  INDEX `user_idx` (`idUser` ASC),
  CONSTRAINT `place`
    FOREIGN KEY (`idPlace`)
    REFERENCES `wanderwiki`.`Places` (`idPlaces`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `route`
    FOREIGN KEY (`idRoute`)
    REFERENCES `wanderwiki`.`Routes` (`idRoutes`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `user`
    FOREIGN KEY (`idUser`)
    REFERENCES `wanderwiki`.`Users` (`userid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `wanderwiki`.`Multimedia`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `wanderwiki`.`Multimedia` (
  `idMultimedia` INT NOT NULL,
  `idPlace` INT NULL,
  `idRoute` INT NULL,
  `idUser` INT NULL,
  `time` DATETIME NULL,
  `author` VARCHAR(45) NULL,
  `type` VARCHAR(45) NULL,
  `file` BLOB NULL,
  `isVR` TINYINT(1) NULL,
  `latitude` DECIMAL NULL,
  `longtitude` DECIMAL NULL,
  `heading` DECIMAL NULL,
  PRIMARY KEY (`idMultimedia`),
  INDEX `place_idx` (`idPlace` ASC),
  INDEX `route_idx` (`idRoute` ASC),
  INDEX `user_idx` (`idUser` ASC),
  CONSTRAINT `place0`
    FOREIGN KEY (`idPlace`)
    REFERENCES `wanderwiki`.`Places` (`idPlaces`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `route0`
    FOREIGN KEY (`idRoute`)
    REFERENCES `wanderwiki`.`Routes` (`idRoutes`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `user0`
    FOREIGN KEY (`idUser`)
    REFERENCES `wanderwiki`.`Users` (`userid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `wanderwiki`.`Descriptions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `wanderwiki`.`Descriptions` (
  `idDescriptions` INT NOT NULL,
  `idRoute` INT NULL,
  `idPlace` INT NULL,
  `DE_standard` TEXT NULL,
  `DE_cultural` TEXT NULL,
  `DE_religious` TEXT NULL,
  `EN_standard` TEXT NULL,
  PRIMARY KEY (`idDescriptions`),
  INDEX `route_idx` (`idRoute` ASC),
  INDEX `place_idx` (`idPlace` ASC),
  CONSTRAINT `route1`
    FOREIGN KEY (`idRoute`)
    REFERENCES `wanderwiki`.`Routes` (`idRoutes`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `place1`
    FOREIGN KEY (`idPlace`)
    REFERENCES `wanderwiki`.`Places` (`idPlaces`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `wanderwiki`.`Searches`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `wanderwiki`.`Searches` (
  `idSearches` INT NOT NULL,
  `idUser` INT NULL,
  `query` TEXT NULL,
  PRIMARY KEY (`idSearches`),
  INDEX `idUser_idx` (`idUser` ASC),
  CONSTRAINT `idUser`
    FOREIGN KEY (`idUser`)
    REFERENCES `wanderwiki`.`Users` (`userid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
