package org.wanderWiki.DAO;

import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import org.wanderWiki.beans.JPAResourceBean;
import org.wanderWiki.entities.Places;

/**
 * PlacesDAOJPA wurde als erster Versuch als POI DAO erstellt. Allerdings wurde
 * sie aufgrund der eleganteren erstellung und Verwaltung über die Fassaden
 * unnötig und führte zu Fehlern
 *
 * @author Andre & Claas
 * @deprecated nicht mehr verwenden. Lieber die PlacesFacade verwenden!
 */
@ManagedBean(name = "placesDAOJPA")
@ApplicationScoped
public class PlacesDAOJPA {

    @ManagedProperty(value = "#{jpaResourceBean}")
    private JPAResourceBean jpaResourceBean;
    private EntityManager em;

    /**
     * gibt die jpaResourceBean zurück.
     *
     * @return JPAResource
     */
    public JPAResourceBean getJpaResourceBean() {
        return jpaResourceBean;
    }

    /**
     * Setzt die jpaResourceBean.
     *
     * @param jpaResourceBean
     */
    public void setJpaResourceBean(JPAResourceBean jpaResourceBean) {
        this.jpaResourceBean = jpaResourceBean;
    }

    /**
     * Gibt den EntitiyMangager zurück.
     *
     * @return EntitiyMangager
     */
    public EntityManager getEm() {
        return em;
    }

    /**
     * Konstruktor Forder einen neuen EntityManager aus der jpaResourceBean an
     * und setzt ihn.
     */
    public PlacesDAOJPA() {
        jpaResourceBean = new JPAResourceBean();
        this.em = jpaResourceBean.getEMF().createEntityManager();
    }

    /**
     * Liefert eine Liste mit allen POIs
     *
     * @author Claas
     *
     * @return Liste aller Places in der Datenbank
     */
    public List<Places> loadAllPOI() {
        List<Places> query = new ArrayList();

//       EntityTransaction tx1 = em.getTransaction();
//       tx1.begin();
        try {
            query = em.createNamedQuery("Places.findAll", Places.class).getResultList();
//             Places ort = em.createNamedQuery("Places.findAll",Places.class).getSingleResult();
//             tx1.commit();

            System.out.println("Type: " + query.get(0).getName());
//                     + query.get(0).getType());
            return query;
        } catch (Exception e) {
            System.out.println("Exception:" + e);
            System.out.println("--Places not exists--");
//           tx1.commit();
            return null;
        }
    }

    /**
     * Selektiert einen Place mit der übergebenen id
     *
     * @param id ID des zu findenen POIs
     * @return den gesuchten POI
     */
    public Places find(long id) {


        Places poi = em.find(Places.class, Integer.parseInt("" + id));
        if (poi == null) {
            return new Places();
        } else {
            System.out.println("Poi id:" + id + " -> " + poi);
            return poi;
        }

    }

    /**
     * Persistiert den übergeben Place in die Datenbank
     *
     * @param neuerPOI den zu speichernden Place
     */
    public void create(Places neuerPOI) {

        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.persist(neuerPOI);
        tx.commit();
        System.out.println("POI angelegt!");

    }

}
