/*  Klasse zur Verbindung zur Datenbank
*   dient zu:
    a) Registrierung
    b) Verwaltung eines Nutzers
    c) Ausfsuchen eines Nutzer
    verantwortlich: Andre Hantke
*/
package org.wanderWiki.DAO;


import java.util.List;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import org.wanderWiki.Helfer.PasswordGenerator;
import org.wanderWiki.beans.JPAResourceBean;
import org.wanderWiki.Helfer.MailMessage;
import org.wanderWiki.entities.Users;

/**
 *
 * @author Andre
 */
@ManagedBean
@ApplicationScoped
public class UsersDAOJPA{
    
    private static final String SQL_USER_COUNT="SELECT COUNT(*) from users";
    
    //einbindung der Bean zur Persisitierung
    //mittels einer Facotry
    @ManagedProperty(value = "#{jpaResourceBean}")
    private JPAResourceBean jpaResourceBean;    

    
    long anzahl;
    
    private EntityManager em;
    
    //Konstruktor
    //setzen des EntityMangers
    public UsersDAOJPA (){
        this.jpaResourceBean=new JPAResourceBean();
        this.em=this.jpaResourceBean.getEMF().createEntityManager();
        System.out.println("Connected to UserDAOJPA");
    }
    public JPAResourceBean getJpaResourceBean() {
        return jpaResourceBean;
    }

    public void setJpaResourceBean(JPAResourceBean jpaResourceBean) {
        this.jpaResourceBean = jpaResourceBean;
    }

    public EntityManager getEm() {
        return em;
    }

    public void setEm(EntityManager em) {
        this.em = em;
    }

    //Methode zum Erhalt der Anzahl an Nutz in der Datenbank
    //@autor: Andre Hantke
    public long getAnzahl() {
        //em=jpaResourceBean.getEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
            Query query=em.createNativeQuery(SQL_USER_COUNT);
            anzahl= (long) query.getResultList().get(0);
        tx.commit();
        return anzahl;
    }

    
    //Methode zum Suchen eines Nutzers
    //@autor: Andre Hantke
    public boolean find(Users user){
        //Transaction holen über EntityManager
        EntityTransaction tx1 = em.getTransaction();
        try{
            //Beginn
            tx1.begin();
            //Ausführen eines Statements, siehe EntityManager
            Users u = (Users) em.createNamedQuery("Users.findByEmail").setParameter("email", user.getEmail()).getSingleResult();
            //Transaction comitten
            tx1.commit();
            //wenn User existiert
            if(u!=null){
                if(u.getName().equals(user.getName())){
                    return false;
                }
            }
        }catch(javax.persistence.NoResultException e){
            System.out.println("Kein Eintrag gefunden.");
            return true;
        }catch(javax.persistence.NonUniqueResultException e){
            System.out.println("Mehere Einträge vorhanden.");
            return false;
        }
        return true;
    }
    
    //Methode zur Registrierung eines Nutzers
    //sofern er nicht existiert
    //@autor: Andre Hantke
    public boolean create(Users user) {         
        EntityTransaction tx=em.getTransaction();
        
        Boolean ok = find(user);
        if(ok){
           if(tx.isActive()){
                em.persist(user);
                tx.commit();
                return true;
           }else{
                tx.begin();
                em.persist(user);
                tx.commit();
                return true;
           }
        }else{
            return false;
        }
    }
    
   //Methode um das Passwort zu einer Mail zu bekommen 
    //@autor: Andre Hantke 
    public String getPassword(String email) {
     //  em=jpaResourceBean.getEMF().createEntityManager();
       EntityTransaction tx1 = em.getTransaction();
       tx1.begin();
       try{
             Users u = em.createNamedQuery("Users.findByEmail", Users.class).setParameter("email", email).getSingleResult();
             tx1.commit();
             return u.getPasswordHash();
       }catch(Exception e){
           System.out.println("Exception:" + e);
           System.out.println("--User not exists--");
           tx1.commit();
           return "not exists";
       }
    }
    
    
    //Methode zum Abrufen seines neu generierten Passwortes
    //@autor: Andre Hantke
    //@Param: Mailadresse
    //dieser wird dann dem Nutzer via Mail zugeschickt
    public boolean changePasword(String mail){
        EntityTransaction tx = em.getTransaction();
        try{
            tx.begin();
            Users nutzer = (Users)em.createNamedQuery("Users.findByEmail").setParameter("email", mail).getSingleResult();
            String newPasswort = PasswordGenerator.generateRandomPassword(12, PasswordGenerator.PasswordMode.SPECIAL);
            nutzer.setPasswordHash(newPasswort);
            em.merge(nutzer);
            tx.commit();
            //sende Mail
            MailMessage sender= MailMessage.getInstance();
            sender.sendPasswordMail(nutzer);
        }catch(javax.persistence.NoResultException e){
            System.out.println("User nicht gefunden! Exception: " + e);
            return false;
        }
        return true;
    }

    //Methode zum Aufspüren eines Nutzers anhand der Mail + Passwort
    //@autor: Andre Hantke
    //@Param: Mailadresse + Passwort
    //dieser wird dann dem Nutzer via Mail zugeschickt
    public Users find(String email, String pass) {
    //    this.em=this.jpaResourceBean.getEMF().createEntityManager();
        EntityTransaction tx2= em.getTransaction();
        Users nutzer=null;
        try{
            tx2.begin();
            //Aufspüren des Nutzer durch emailadresse
            List<Users> u = em.createNamedQuery("Users.findByEmail",Users.class).setParameter("email", email).getResultList();
            if(u == null){
                System.out.println("--User not exists--");
                tx2.commit();
                
            }else{
                //Überprüfen ob dann auch das Passwort übereinstimmt
                for(int i=0;i<u.size();i++){
                    if(u.get(i).getEmail().equals(email) && u.get(i).getPasswordHash().equals(pass)){
                        nutzer= new Users();
                        nutzer=u.get(i);
                        em.refresh(nutzer);
                        tx2.commit();
                        System.out.println("User vorhanden");
                        
                    }
                }
            }
        }catch(javax.persistence.NoResultException e){
            System.out.println("No Result Exception: " + e);
            tx2.commit();
            return null;
        }
        //wenn alles gut, liefer Nutzer zurück
        return nutzer;
    }
    
    //Methode zum Updaten der Nutzerdaten
    //@autor: Andre Hantke
    //@Param: komplette eingeloggte Nutzer
    public void update(Users user) {
        EntityTransaction tx = em.getTransaction();
        try{
        //Auffinden des Nutzer durch die ID -> sonst NoResultException
        Users u = (Users) em.createNamedQuery("Users.findByUserid").setParameter("userid", user.getUserid()).getSingleResult();
        //setzen der Werte Mail etc.
        u.setName(user.getName());
        u.setEmail(user.getEmail());
        u.setPasswordHash(user.getPasswordHash());
        u.setInterest(user.getInterest());
        u.setWalkingSpeed(user.getWalkingSpeed());
        u.setWheelchair(user.getWheelchair());
        u.setCommentsList(user.getCommentsList());
        u.setUserlevel(user.getUserlevel());
        u.setLanguage(user.getLanguage());
        //Persisitierung
        tx.begin();
         em.merge(u);
        tx.commit();
        }catch(javax.persistence.NoResultException e){
            System.out.println("No Result Exception by Updateing User: Exception: " + e);
        }
        
    }
}
