package org.wanderWiki.DAO;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import org.wanderWiki.entities.Comments;
//import org.wanderWiki.entities.Comments_;
import org.wanderWiki.entities.Descriptions;
import org.wanderWiki.entities.Multimedia;
import org.wanderWiki.entities.Places;
import org.wanderWiki.entities.Placesroutes;
import org.wanderWiki.entities.Questions;
import org.wanderWiki.entities.Routepoints;
import org.wanderWiki.entities.Routes;
import org.wanderWiki.entities.Searches;
import org.wanderWiki.entities.Todos;
import org.wanderWiki.entities.Users;

/**
 *
 * @author MartinM
 */
public class MartinDAO {
    
    private EntityManagerFactory emf;
    private static MartinDAO instance;
    
    private MartinDAO() {
        System.out.println("MartinDAO()");
        emf = Persistence.createEntityManagerFactory("WanderWIKIPU");
        System.out.println("Built EntityManagerFactory: " + emf.toString());
    }
    
    public static MartinDAO getInstance() {
        if (instance == null) {
            instance = new MartinDAO();
        }
        return instance;
    }
    
    private EntityManager createEntityManager() {
        return emf.createEntityManager();
    }
    
    //
    // PLACES
    //
    
    public void addPlace(Places p) {
        EntityManager em = createEntityManager();
        EntityTransaction et = em.getTransaction();
        et.begin();
        em.persist(p);
        
        List<Descriptions> dl = p.getDescriptionsList();
        for (Descriptions d : dl) {
            d.setIdRoute(null);
            d.setIdPlace(p);
            em.persist(d);
        }
        
        et.commit();
        em.close();
    }
    
    public Places getPlace(int id) {
        return (Places) get("Places", "idPlaces", id);
    }
    
    public void updatePlace(Places dest, Places source) {
        EntityManager em = createEntityManager();
        EntityTransaction et = em.getTransaction();
        et.begin();
        dest.setAltitude(source.getAltitude());
        dest.setCity(source.getCity());
        dest.setDateAdded(source.getDateAdded());
        dest.setIdChanging(source.getIdChanging());
        dest.setIsChange(source.getIsChange());
        dest.setIsProposal(source.getIsProposal());
        dest.setLatitude(source.getLatitude());
        dest.setLongitude(source.getLongitude());
        dest.setName(source.getName());
        dest.setOpenTimes(source.getOpenTimes());
        dest.setPostalCode(source.getPostalCode());
        dest.setSize(source.getSize());
        dest.setStreet(source.getStreet());
        dest.setType(source.getType());
        em.merge(dest);
        et.commit();
        em.close();
    }
    
    public void deletePlace(Places p) {
        delete(p);
    }
    
    public List<Places> getPlacesInList(List<Integer> idList) {
        EntityManager em = createEntityManager();
        TypedQuery<Places> tq = em.createQuery("SELECT P FROM Places P WHERE P.idPlaces IN :idList", Places.class).setParameter("idList", idList);
        List<Places> pl = tq.getResultList();
        em.close();
        return pl;
    }
    
    public List<Places> getPlacesMatchingName(String name, Boolean isProposal) {
        EntityManager em = createEntityManager();
        TypedQuery<Places> tq = em.createQuery("SELECT P FROM Places P "
                + "WHERE P.isProposal = :prop "
                + "AND UPPER(P.name) LIKE :name "
                + "ORDER BY P.name", Places.class)
                .setParameter("prop", isProposal)
                .setParameter("name", "%" + name.toUpperCase() + "%");
        List<Places> pl = tq.getResultList();
        em.close();
        return pl;
    }
    
    public List<Places> getPlacesMatchingType(String type, Boolean isProposal) {
        EntityManager em = createEntityManager();
        TypedQuery<Places> tq = em.createQuery("SELECT P FROM Places P "
                + "WHERE P.isProposal = :prop "
                + "AND P.type = :type "
                + "ORDER BY P.name", Places.class)
                .setParameter("prop", isProposal)
                .setParameter("type", type);
        List<Places> pl = tq.getResultList();
        em.close();
        return pl;
    }
    
    public List<Places> getPlacesMatchingTypes(List<String> typeList, Boolean isProposal) {
        EntityManager em = createEntityManager();
        TypedQuery<Places> tq = em.createQuery("SELECT P FROM Places P "
                + "WHERE P.isProposal = :prop "
                + "AND P.type IN :types "
                + "ORDER BY P.name", Places.class)
                .setParameter("prop", isProposal)
                .setParameter("types", typeList);
        List<Places> pl = tq.getResultList();
        em.close();
        return pl;
    }
    
    public List<Descriptions> getPlaceDescriptions(Places p) {
        EntityManager em = createEntityManager();
        TypedQuery<Descriptions> tq = em.createQuery("SELECT D FROM Descriptions D, Places P "
                + "WHERE P = :idPlace "
                + "AND D.idPlace = P", Descriptions.class)
                .setParameter("idPlace", p);
        List<Descriptions> dl = tq.getResultList();
        em.close();
        return dl;
    }
    
    public List<Places> getPlacesInSection(double latMin, double latMax, double longMin, double longMax, Boolean isProposal) {
        System.out.println("getPlacesInSection()");
        EntityManager em = createEntityManager();
        TypedQuery<Places> tq = em.createQuery("SELECT P FROM Places P "
                + "WHERE P.isProposal = :prop "
                + "AND P.latitude > :latmin "
                + "AND P.latitude <= :latmax "
                + "AND P.longitude > :longmin "
                + "AND P.longitude <= :longmax", Places.class)
                .setParameter("prop", isProposal)
                .setParameter("latmin", latMin)
                .setParameter("latmax", latMax)
                .setParameter("longmin", longMin)
                .setParameter("longmax", longMax);
        List<Places> pl = tq.getResultList();
        em.close();
        return pl;
    }
    
    public int getPlaceCount() {
        return getCount("idPlaces", "Places");
    }

    public int getPlaceCommentCount(Places onPlace) {
        return getCount("idComments", "Comments", "idPlace", onPlace);
    }
    
    public int getPlaceMultimediaCount(Places onPlace) {
        return getCount("idMultimedia", "Multimedia", "idPlace", onPlace);
    }
    
    public int getPlaceTodosCount(Places onPlace) {
        return getCount("idToDos", "Todos", "idPlace", onPlace);
    }
    
    public Comments getPlaceComment(Places onPlace, int commentNum) {
        List<Comments> c = getPlaceComments(onPlace, commentNum, 1);
        if (c.isEmpty())
            return null;
        else return c.get(0);
    }
    
    public List<Comments> getPlaceComments(Places onPlace, int firstRow, int maxResults) {
        System.out.println("getPlaceComments(place#" + onPlace.getIdPlaces() + ", " + firstRow + ", " + maxResults + ")");
        EntityManager em = createEntityManager();
        List<Comments> c = new ArrayList<>();
        try {
            TypedQuery<Comments> q = em.createQuery("SELECT C FROM Comments C, Places P WHERE P.idPlaces = :placeId AND C.idPlace = :place ORDER BY C.time DESC",
                    Comments.class).setParameter("placeId", onPlace.getIdPlaces()).setParameter("place", onPlace)
                    .setFirstResult(firstRow - 1).setMaxResults(maxResults);
            c = q.getResultList();
        } finally {
            em.close();
        }
        return c;
    }
    
    public Multimedia getPlaceMultimedia(Places onPlace, int mediaNum) {
        List<Multimedia> m = getPlaceMultimedia(onPlace, mediaNum, 1);
        if (m.isEmpty())
            return null;
        else return m.get(0);
    }
    
    public List<Multimedia> getPlaceMultimedia(Places onPlace, int firstRow, int maxResults) {
        EntityManager em = createEntityManager();
        List<Multimedia> m = new ArrayList();
        try {
            TypedQuery<Multimedia> q = em.createQuery("SELECT M FROM Multimedia M, Places P WHERE P.idPlaces = :placeId AND M.idPlace = :place ORDER BY M.time DESC",
                    Multimedia.class).setParameter("placeId", onPlace.getIdPlaces()).setParameter("place", onPlace)
                    .setFirstResult(firstRow - 1).setMaxResults(maxResults);
            m = q.getResultList();
        } finally {
            em.close();
        }
        return m;
    }

    public float getPlaceRating(Places fromPlace) {
        EntityManager em = createEntityManager();
        float rating = 0.0f;
        try {
            /*CriteriaBuilder qb = em.getCriteriaBuilder();
            CriteriaQuery<Number> cq = qb.createQuery(Number.class);
            Root<Comments> rc = cq.from(Comments.class);
            Predicate abstainCondition = qb.gt(rc.get(Comments_.rating), 0);
            Predicate placeIdCondition = qb.equal(rc.get(Comments_.idPlace), fromPlace);
            cq.where(qb.and(abstainCondition, placeIdCondition));
            cq.select(qb.avg(rc.get(Comments_.rating)));
            TypedQuery<Number> tq = em.createQuery(cq);
            Number n = tq.getSingleResult();*/
            
            Query q = em.createQuery("SELECT AVG(C.rating + 0.0) FROM Comments C WHERE (C.idPlace = :place) AND (C.rating > :abstainRating)")
                    .setParameter("place", fromPlace).setParameter("abstainRating", (Integer) 0);
            Number n = (Number) q.getSingleResult();
            if (n != null) {
                rating = n.floatValue();
            }
        } catch (NoResultException ex) {
        } finally {
            em.close();
        }
        return rating;
        
    }
    
    //
    // COMMENTS
    //
    
    public void addComment(Comments c) {
        add(c);
    }
    
    public Comments getComment(int id) {
        return (Comments) get("Comments", "idComments", id);
    }
    
    public void deleteComment(Comments c) {
        delete(c);
    }
    
    //
    // DESCRIPTIONS
    //
    
    public void addDescription(Descriptions d) {
        add(d);
    }
    
    public Descriptions getDescription(int id) {
        return (Descriptions) get("Descriptions", "idDescriptions", id);
    }
    
    public Descriptions getDescription(Places forPlace, String interest, String language) {
        if (interest == null) interest = "religious";
        if (language == null) language = "de";
        EntityManager em = createEntityManager();
        Descriptions d = null;
        try {
            TypedQuery<Descriptions> q = em.createQuery("SELECT D FROM Descriptions D WHERE D.idPlace = :place", Descriptions.class).setParameter("place", forPlace);
            List<Descriptions> dl = q.getResultList();
            d = selectRelevantDescription(dl, interest, language);
        } finally {
            em.close();
        }
        return d;
    }
    
    public Descriptions getDescription(Routes forRoute, String interest, String language) {
        if (interest == null) interest = "religious";
        if (language == null) language = "de";
        EntityManager em = createEntityManager();
        Descriptions d = null;
        try {
            TypedQuery<Descriptions> q = em.createQuery("SELECT D FROM Descriptions D WHERE D.idRoute = :route", Descriptions.class).setParameter("route", forRoute);
            List<Descriptions> dl = q.getResultList();
            d = selectRelevantDescription(dl, interest, language);
        } finally {
            em.close();
        }
        return d;
    }
    
    public Descriptions updateDescription(Descriptions dest, Descriptions source) {
        EntityManager em = createEntityManager();
        EntityTransaction et = em.getTransaction();
        et.begin();
        dest.setIdPlace(source.getIdPlace());
        dest.setIdRoute(source.getIdRoute());
        dest.setInterest(source.getInterest());
        dest.setLanguage(source.getLanguage());
        dest.setText(source.getText());
        em.merge(dest);
        et.commit();
        em.close();
        return dest;
    }
    
    private Descriptions selectRelevantDescription(List<Descriptions> fromList, String havingInterest, String havingLanguage) {
        if (fromList == null) return null;
        Descriptions d = null;
        for (Descriptions dld : fromList) {
            if (havingInterest.equalsIgnoreCase(dld.getInterest()) && havingLanguage.equalsIgnoreCase(dld.getLanguage())) {
                d = dld;
                break;
            }
        }
        if (d == null) {
            for (Descriptions dld : fromList) {
                if (havingLanguage.equalsIgnoreCase(dld.getLanguage())) {
                    d = dld;
                    break;
                }
            }
        }
        if (d == null) {
            for (Descriptions dld : fromList) {
                if ("eng".equalsIgnoreCase(havingLanguage)) {
                    d = dld;
                    break;
                }
            }
        }
        if (d == null) {
            for (Descriptions dld : fromList) {
                if (havingInterest.equalsIgnoreCase(dld.getInterest())) {
                    d = dld;
                    break;
                }
            }
        }
        if (d == null) {
            if (!fromList.isEmpty()) {
                d = fromList.get(0);
            }
        }
        return d;
    }
    
    public void deleteDescription(Descriptions d) {
        delete(d);
    }
    
    //
    // MULTIMEDIA
    //
    
    public void addMultimedia(Multimedia m) {
        add(m);
    }
    
    public Multimedia getMultimedia(int id) {
        return (Multimedia) get("Multimedia", "idMultimedia", id);
    }
    
    public void deleteMultimedia(Multimedia m) {
        delete(m);
    }
    
    //
    // QUESTIONS
    //
    
    public void addQuestion(Questions q) {
        add(q);
    }
    
    public Questions getQuestion(int id) {
        return (Questions) get("Questions", "idquestions", id);
    }
    
    public void deleteQuestion(Questions q) {
        delete(q);
    }
    
    //
    // ROUTES
    //
    
    public void addRoute(Routes r) {
        EntityManager em = createEntityManager();
        EntityTransaction et = em.getTransaction();
        et.begin();
        em.persist(r);
        
        List<Descriptions> dl = r.getDescriptionsList();
        for (Descriptions d : dl) {
            d.setIdRoute(r);
            d.setIdPlace(null);
            em.persist(d);
        }
        
        List<Routepoints> rpl = r.getRoutepointsList();
        for (Routepoints rp : rpl) {
            rp.setIdRoute(r);
            em.persist(rp);
        }
        
        List<Placesroutes> prl = r.getPlacesroutesList();
        for (Placesroutes pr : prl) {
            pr.setIdRoute(r);
            em.persist(pr);
        }
        
        try {
            r.setStart(rpl.get(0).getIdRoutePoints());
            r.setEnd(rpl.get(rpl.size() - 1).getIdRoutePoints());
        } catch (IndexOutOfBoundsException ex) {
            System.err.println("WARNING: addRoutes(): getRoutepointsList() returned an empty list.");
        }
        et.commit();
        em.close();
    }
    
    public void updateRoute(Routes dest, Routes source) {
        EntityManager em = createEntityManager();
        EntityTransaction et = em.getTransaction();
        et.begin();
        dest.setDifficulty(source.getDifficulty());
        dest.setDistance(source.getDistance());
        dest.setDuration(source.getDuration());
        dest.setEquipment(source.getEquipment());
        dest.setIdChanging(source.getIdChanging());
        dest.setIsChange(source.getIsChange());
        dest.setIsProposed(source.getIsProposed());
        dest.setName(source.getName());
        dest.setType(source.getType());
        
        // Delete all old routepoints
        TypedQuery<Routepoints> rpq = em.createQuery("SELECT P FROM Routepoints P WHERE P.idRoute = :route",
                Routepoints.class).setParameter("route", dest);
        List<Routepoints> rpl = rpq.getResultList();
        for (Routepoints rp : rpl) {
            em.remove(rp);
        }
        
        // Delete all old placesroutes
        TypedQuery<Placesroutes> prq = em.createQuery("SELECT P FROM Placesroutes P WHERE P.idRoute = :route",
                Placesroutes.class).setParameter("route", dest);
        List<Placesroutes> prl = prq.getResultList();
        for (Placesroutes pr : prl) {
            em.remove(pr);
        }
        
        // Add new routepoints
        List<Routepoints> rpl2 = source.getRoutepointsList();
        for (Routepoints rp : rpl2) {
            rp.setIdRoute(dest);
            em.persist(rp);
        }
        
        // Add new placesroutes
        List<Placesroutes> prl2 = source.getPlacesroutesList();
        for (Placesroutes pr : prl2) {
            pr.setIdRoute(dest);
            em.persist(pr);
        }
        
        dest.setEnd(source.getEnd());
        dest.setStart(source.getStart());
        
        em.merge(dest);
        et.commit();
        em.close();
    }
    
    public Routes getRoute(int id) {
        return (Routes) get("Routes", "idRoutes", id);
    }
    
    public int getRouteCount() {
        return getCount("idRoutes", "Routes");
    }
    
    public List<Routepoints> getRoutePoints(Routes r) {
        EntityManager em = createEntityManager();
        List<Routepoints> list = new LinkedList<>();
        try {
            TypedQuery<Routepoints> q = em.createQuery("SELECT P FROM Routepoints P WHERE P.idRoute = :route ORDER BY P.num ASC", Routepoints.class).setParameter("route", r);
            list = q.getResultList();
        } finally {
            em.close();
        }
        return list;
    }
    
    public void deleteRoute(Routes r) {
        EntityManager em = createEntityManager();
        EntityTransaction et = em.getTransaction();
        et.begin();
        // Constraint cascading should take care of this...
//        List<Routepoints> list = new LinkedList<>();
//        try {
//            TypedQuery<Routepoints> q = em.createQuery("SELECT P FROM Routepoints P WHERE P.idRoute = :route", Routepoints.class).setParameter("route", r);
//            list = q.getResultList();
//        } finally {
//            em.close();
//        }
//        for (Routepoints rp : list) {
//            em.remove(rp);
//        }
        em.remove(r);
        et.commit();
        em.close();
    }
    
    public List<Routes> getRoutesMatchingName(String name, Boolean isProposal) {
        EntityManager em = createEntityManager();
        TypedQuery<Routes> tq = em.createQuery("SELECT R FROM Routes R "
                + "WHERE R.isProposed = :prop "
                + "AND UPPER(R.name) LIKE :name ORDER BY R.name", Routes.class)
                .setParameter("prop", isProposal)
                .setParameter("name", "%" + name.toUpperCase() + "%");
        List<Routes> rl = tq.getResultList();
        em.close();
        return rl;
    }
    
    public List<Places> getRoutePlaces(Routes r) {
        EntityManager em = createEntityManager();
        TypedQuery<Places> tq = em.createQuery("SELECT P FROM Places P, Placesroutes PR, Routes R "
                + "WHERE R = :idRoute "
                + "AND PR.idRoute = R "
                + "AND PR.idPlace = P", Places.class)
                .setParameter("idRoute", r);
        List<Places> pl = tq.getResultList();
        em.close();
        return pl;
    }
    
    public List<Descriptions> getRouteDescriptions(Routes r) {
        EntityManager em = createEntityManager();
        TypedQuery<Descriptions> tq = em.createQuery("SELECT D FROM Descriptions D, Routes R "
                + "WHERE R = :idRoute "
                + "AND D.idRoute = R", Descriptions.class)
                .setParameter("idRoute", r);
        List<Descriptions> dl = tq.getResultList();
        em.close();
        return dl;
    }
    
    public List<Routes> getRoutesInSection(double latMin, double latMax, double longMin, double longMax, Boolean isProposal) {
        System.out.println("getRoutesInSection()");
        EntityManager em = createEntityManager();
        TypedQuery<Routes> tq = em.createQuery("SELECT DISTINCT R FROM Routes R, Routepoints PT "
                + "WHERE R.isProposed = :prop "
                + "AND R = PT.idRoute "
                + "AND PT.num = 0 "
                + "AND PT.latitude > :latmin "
                + "AND PT.latitude <= :latmax "
                + "AND PT.longitude > :longmin "
                + "AND PT.longitude <= :longmax", Routes.class)
                .setParameter("prop", isProposal)
                .setParameter("latmin", latMin)
                .setParameter("latmax", latMax)
                .setParameter("longmin", longMin)
                .setParameter("longmax", longMax);
        List<Routes> rl = tq.getResultList();
        em.close();
        return rl;
    }

    public int getRouteCommentCount(Routes onRoute) {
        return getCount("idComments", "Comments", "idRoute", onRoute);
    }
    
    public int getRouteMultimediaCount(Routes onRoute) {
        return getCount("idMultimedia", "Multimedia", "idRoute", onRoute);
    }
    
    public int getRouteTodosCount(Routes onRoute) {
        return getCount("idToDos", "Todos", "idRoute", onRoute);
    }
    
    public Comments getRouteComment(Routes onRoute, int commentNum) {
        List<Comments> c = getRouteComments(onRoute, commentNum, 1);
        if (c.isEmpty())
            return null;
        else return c.get(0);
    }
    
    public List<Comments> getRouteComments(Routes onRoute, int firstRow, int maxResults) {
        EntityManager em = createEntityManager();
        List<Comments> c = new ArrayList<>();
        try {
            TypedQuery<Comments> q = em.createQuery("SELECT C FROM Comments C, Routes P WHERE P.idRoutes = :routeId AND C.idRoute = :route ORDER BY C.time DESC",
                    Comments.class).setParameter("routeId", onRoute.getIdRoutes()).setParameter("route", onRoute)
                    .setFirstResult(firstRow - 1).setMaxResults(maxResults);
            c = q.getResultList();
        } finally {
            em.close();
        }
        return c;
    }
    
    public Multimedia getRouteMultimedia(Routes onRoute, int mediaNum) {
        List<Multimedia> m = getRouteMultimedia(onRoute, mediaNum, 1);
        if (m.isEmpty())
            return null;
        else return m.get(0);
    }
    
    public List<Multimedia> getRouteMultimedia(Routes onRoute, int firstRow, int maxResults) {
        EntityManager em = createEntityManager();
        List<Multimedia> m = new ArrayList();
        try {
            TypedQuery<Multimedia> q = em.createQuery("SELECT M FROM Multimedia M, Routes R WHERE R.idRoutes = :routeId AND M.idRoute = :route ORDER BY M.time DESC",
                    Multimedia.class).setParameter("routeId", onRoute.getIdRoutes()).setParameter("route", onRoute)
                    .setFirstResult(firstRow - 1).setMaxResults(maxResults);
            m = q.getResultList();
        } finally {
            em.close();
        }
        return m;
    }

    public float getRouteRating(Routes fromRoute) {
        EntityManager em = createEntityManager();
        float rating = 0.0f;
        try {
            /*CriteriaBuilder qb = em.getCriteriaBuilder();
            CriteriaQuery<Number> cq = qb.createQuery(Number.class);
            Root<Comments> rc = cq.from(Comments.class);
            Predicate abstainCondition = qb.gt(rc.get(Comments_.rating), 0);
            Predicate routeIdCondition = qb.equal(rc.get(Comments_.idRoute), fromRoute);
            cq.where(qb.and(abstainCondition, routeIdCondition));
            cq.select(qb.avg(rc.get(Comments_.rating)));
            TypedQuery<Number> tq = em.createQuery(cq);
            Number n = tq.getSingleResult();*/
            
            Query q = em.createQuery("SELECT AVG(C.rating + 0.0) FROM Comments C WHERE (C.idRoute = :route) AND (C.rating > :abstainRating)")
                    .setParameter("route", fromRoute).setParameter("abstainRating", (Integer) 0);
            Number n = (Number) q.getSingleResult();
            if (n != null) {
                rating = n.floatValue();
            }
        } catch (NoResultException ex) {
        } finally {
            em.close();
        }
        return rating;
        
    }
    
    //
    // SEARCHES
    //
    
    public void addSearch(Searches s) {
        add(s);
    }
    
    public Searches getSearch(int id) {
        return (Searches) get("Searches", "idSearches", id);
    }
    
    public void deleteSearch(Searches s) {
        delete(s);
    }
    
    //
    // TODOS
    //
    
    public void addTodo(Todos t) {
        add(t);
    }
    
    public Todos getTodo(int id) {
        return (Todos) get("Todos", "idToDos", id);
    }
    
    public void deleteTodo(Todos t) {
        delete(t);
    }
    
    //
    // USERS
    //
    
    public void addUser(Users u) {
        add(u);
    }
    
    public Users getUser(int id) {
        return (Users) get("Users", "userid", id);
    }
    
    public Users getUserByEmail(String email) {
        return (Users) get("Users", "email", email);
    }
    
    public Users getUserByName(String name) {
        return (Users) get("Users", "name", name);
    }
    
    public void deleteUser(Users u) {
        delete(u);
    }

    public int getUserCommentCount(Users onUser) {
        return getCount("idComments", "Comments", "idUser", onUser);
    }
    
    public int getUserMultimediaCount(Users onUser) {
        return getCount("idMultimedia", "Multimedia", "idUser", onUser);
    }
    
    public Comments getUserComment(Users onUser, int commentNum) {
        List<Comments> c = getUserComments(onUser, commentNum, 1);
        if (c.isEmpty())
            return null;
        else return c.get(0);
    }
    
    public List<Comments> getUserComments(Users onUser, int firstRow, int maxResults) {
        EntityManager em = createEntityManager();
        List<Comments> c = new ArrayList<>();
        try {
            TypedQuery<Comments> q = em.createQuery("SELECT C FROM Comments C, Users U WHERE U.userid = :userId AND C.idUser = :user ORDER BY C.time DESC",
                    Comments.class).setParameter("userId", onUser.getUserid()).setParameter("user", onUser)
                    .setFirstResult(firstRow - 1).setMaxResults(maxResults);
            c = q.getResultList();
        } finally {
            em.close();
        }
        return c;
    }
    
    // ------------------------------------------------------------------------
    // 
    // HELPER FUNCTIONS
    //
    
    private void add(Object entity) {
        EntityManager em = createEntityManager();
        EntityTransaction et = em.getTransaction();
        et.begin();
        em.persist(entity);
        et.commit();
        em.close();
    }
    
    private Object get(String fromTable, String idParameter, int equalsId) {
        EntityManager em = createEntityManager();
        Object o = null;
        try {
            Query q = em.createQuery("SELECT x FROM " + fromTable + " x WHERE x." + idParameter + " = :id").setParameter("id", equalsId);
            o = q.getSingleResult();
        } finally {
            em.close();
        }
        return o;
    }
    
    private Object get(String fromTable, String idParameter, String equalsId) {
        EntityManager em = createEntityManager();
        Object o = null;
        try {
            Query q = em.createQuery("SELECT x FROM " + fromTable + " x WHERE x." + idParameter + " = :id").setParameter("id", equalsId);
            o = q.getSingleResult();
        } finally {
            em.close();
        }
        return o;
    }
    
    private void delete(Object entity) {
        EntityManager em = createEntityManager();
        EntityTransaction et = em.getTransaction();
        et.begin();
        em.remove(entity);
        et.commit();
        em.close();
    }
    
    private int getCount(String ofParam, String inTable, String whereEqualsObjectRef, Object equalsObject) {
        EntityManager em = createEntityManager();
        int count = 0;
        try {
            Query q = em.createQuery("SELECT COUNT(X." + ofParam + ") FROM " + inTable + " X WHERE X." + whereEqualsObjectRef + " = :pId").setParameter("pId", equalsObject);
            count = ((Number) q.getSingleResult()).intValue();
        } catch (NoResultException ex) {
        } finally {
            em.close();
        }
        return count;
    }
    
    private int getCount(String ofParam, String inTable) {
        EntityManager em = createEntityManager();
        int count = 0;
        try {
            Query q = em.createQuery("SELECT COUNT(X." + ofParam + ") FROM " + inTable);
            count = ((Number) q.getSingleResult()).intValue();
        } catch (NoResultException ex) {
        } finally {
            em.close();
        }
        return count;
    }

    public <T> void update(T entity) {
        EntityManager em = createEntityManager();
        EntityTransaction et = em.getTransaction();
        et.begin();
        em.merge(entity);
        et.commit();
        em.close();
    }
    
}
