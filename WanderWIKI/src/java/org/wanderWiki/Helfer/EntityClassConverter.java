package org.wanderWiki.Helfer;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import org.wanderWiki.entities.Comments;
import org.wanderWiki.entities.Descriptions;
import org.wanderWiki.entities.Multimedia;
import org.wanderWiki.entities.Places;
import org.wanderWiki.entities.Placesroutes;
import org.wanderWiki.entities.Routepoints;
import org.wanderWiki.entities.Routes;
import org.wanderWiki.entities.Users;
import org.wanderWiki.rest.GenericDBAccessor;
import regionale.whs.wanderapp.wspojos.GeoLocation;
import regionale.whs.wanderapp.wspojos.WanderDescription;
import regionale.whs.wanderapp.wspojos.WanderKommentar;
import regionale.whs.wanderapp.wspojos.WanderMedia;
import regionale.whs.wanderapp.wspojos.WanderPOI;
import regionale.whs.wanderapp.wspojos.WanderRoute;
import regionale.whs.wanderapp.wspojos.WanderUser;

/**
 *
 * @author MartinM
 */
public class EntityClassConverter extends GenericDBAccessor {
    public static WanderPOI convert(Places p) {
        if (p == null) return null;
        WanderPOI poi = new WanderPOI();
        poi.setId(deref(p.getIdPlaces()));
        poi.setDateAdded(p.getDateAdded());
        if (p.getIdChanging() != null)
            poi.setIdChanging(deref(p.getIdChanging().getIdPlaces()));
        poi.setIsChange(p.getIsChange());
        poi.setIsProposal(p.getIsProposal());
        poi.setKoordinaten(new GeoLocation(p.getLatitude(), p.getLongitude(), p.getAltitude()));
        poi.setName(p.getName());
        poi.setOpenTimes(p.getOpenTimes());
        poi.setPostalCode(p.getPostalCode());
        poi.setSize(p.getSize());
        poi.setStreet(p.getStreet());
        poi.setCity(p.getCity());
        poi.setPoiType(p.getType());
        
        if (p.getIdPlaces() != null) {
            List<Descriptions> dl = dao.getPlaceDescriptions(p);
            List<WanderDescription> wdl = new LinkedList<>();
            for (Descriptions d : dl) {
                WanderDescription wk = convert(d);
                wdl.add(wk);
            }
            poi.setDescriptions(wdl);
        }
        
        // Set metadata
        poi.setCommentCount(dao.getPlaceCommentCount(p));
        poi.setMultimediaCount(dao.getPlaceMultimediaCount(p));
        poi.setTodosCount(dao.getPlaceTodosCount(p));
        poi.setRating(dao.getPlaceRating(p));
        
        return poi;
    }
    
    public static Places convert(WanderPOI poi) {
        if (poi == null) return null;
        Places p = new Places();
        p.setLatitude(poi.getKoordinaten().getLatitude());
        p.setLongitude(poi.getKoordinaten().getLongitude());
        p.setAltitude((int) poi.getKoordinaten().getAltitude());
        p.setCity(poi.getCity());
        p.setCommentsList(new ArrayList<>());
        p.setDateAdded(poi.getDateAdded());
        p.setDescriptionsList(new ArrayList<>());
        if (poi.getIdChanging() != 0)
            p.setIdChanging(dao.getPlace(poi.getIdChanging()));
        p.setIdPlaces(ref(poi.getId()));
        p.setIsChange(poi.getIsChange());
        p.setIsProposal(poi.getIsProposal());
        p.setMultimediaList(new ArrayList<>());
        p.setName(poi.getName());
        p.setOpenTimes(poi.getOpenTimes());
        p.setPlacesroutesList(new ArrayList<>());
        p.setPostalCode(poi.getPostalCode());
        p.setQuestionsList(new ArrayList<>());
        p.setSize(poi.getSize());
        p.setStreet(poi.getStreet());
        p.setTodosList(new ArrayList<>());
        p.setType(poi.getPoiType());
        return p;
    }
    
    public static WanderKommentar convert(Comments c) {
        if (c == null) return null;
        WanderKommentar wk = new WanderKommentar();
        if (c.getIdUser() != null)
            wk.setAuthorId(deref(c.getIdUser().getUserid()));
        wk.setId(c.getIdComments());
        if (c.getIdPlace() != null)
            wk.setPlaceId(deref(c.getIdPlace().getIdPlaces()));
        wk.setRating((int)c.getRating());
        if (c.getIdRoute() != null)
            wk.setRouteId(deref(c.getIdRoute().getIdRoutes()));
        wk.setText(c.getText());
        wk.setTime(c.getTime());
        wk.setAuthorName(c.getIdUser().getName());
        return wk;
    }
    
    public static Comments convert(WanderKommentar wk) {
        if (wk == null) return null;
        Comments c = new Comments();
        c.setIdComments(ref(wk.getId()));
        if (wk.getPlaceId() != 0)
            c.setIdPlace(dao.getPlace(wk.getPlaceId()));
        else if (wk.getRouteId() != 0)
            c.setIdRoute(dao.getRoute(wk.getRouteId()));
        if (wk.getAuthorId() != 0)
            c.setIdUser(dao.getUser(wk.getAuthorId()));
        c.setRating(wk.getRating());
        c.setText(wk.getText());
        c.setTime(wk.getTime());
        return c;
    }
    
    public static WanderDescription convert(Descriptions d) {
        if (d == null) return null;
        WanderDescription wd = new WanderDescription();
        wd.setInterest(d.getInterest());
        wd.setLanguage(d.getLanguage());
        wd.setText(d.getText());
        if (d.getIdPlace() != null)
            wd.setIdPlace(deref(d.getIdPlace().getIdPlaces()));
        else if (d.getIdRoute() != null)
            wd.setIdRoute(deref(d.getIdRoute().getIdRoutes()));
        return wd;
    }
    
    public static Descriptions convert(WanderDescription wd) {
        if (wd == null) return null;
        Descriptions d = new Descriptions();
        d.setIdDescriptions(ref(wd.getId()));
        if (wd.getIdPlace() != 0)
            d.setIdPlace(dao.getPlace(wd.getIdPlace()));
        else if (wd.getIdRoute() != 0)
            d.setIdRoute(dao.getRoute(wd.getIdRoute()));
        d.setInterest(wd.getInterest());
        d.setLanguage(wd.getLanguage());
        d.setText(wd.getText());
        return d;
    }
    
    public static WanderRoute convert(Routes r) {
        if (r == null) return null;
        WanderRoute wr = new WanderRoute();
        wr.setDifficulty(r.getDifficulty());
        wr.setDistance(r.getDistance());
        wr.setDuration(r.getDuration());
        wr.setEquipment(r.getEquipment());
        wr.setId(deref(r.getIdRoutes()));
        if (r.getIdChanging() != null)
            wr.setIdChanging(deref(r.getIdChanging().getIdRoutes()));
        wr.setIsChange(r.getIsChange());
        wr.setIsProposal(r.getIsProposed());
        wr.setName(r.getName());
        wr.setRating(0.0f);
        wr.setRouteType(r.getType());
        wr.setStartId(deref(r.getStart()));
        wr.setEndId(deref(r.getEnd()));
        
        LinkedList<GeoLocation> gll = new LinkedList<>();
        List<Routepoints> rpl = r.getRoutepointsList();
        for (Routepoints rp : rpl) {
            GeoLocation gl = new GeoLocation();
            gl.setAltitude(0);
            gl.setLatitude(rp.getLatitude());
            gl.setLongitude(rp.getLongitude());
            gll.add(gl);
        }
        wr.setRoutePoints(gll);
        
        LinkedList<Integer> pil = new LinkedList<>();
        List<Placesroutes> prl = r.getPlacesroutesList();
        for (Placesroutes pr : prl) {
            Integer pi = pr.getIdPlace().getIdPlaces();
            pil.add(pi);
        }
        wr.setRoutePlacesIds(pil);
        
        if (r.getIdRoutes() != null) {
            List<Descriptions> dl = dao.getRouteDescriptions(r);
            List<WanderDescription> wdl = new LinkedList<>();
            for (Descriptions d : dl) {
                WanderDescription wk = convert(d);
                wdl.add(wk);
            }
            wr.setDescriptions(wdl);
        }
        
        // Set metadata
        wr.setCommentCount(dao.getRouteCommentCount(r));
        wr.setMultimediaCount(dao.getRouteMultimediaCount(r));
        wr.setTodosCount(dao.getRouteTodosCount(r));
        wr.setRating(dao.getRouteRating(r));
        return wr;
    }
    
    public static Routes convert(WanderRoute wr) {
        if (wr == null) return null;
        Routes r = new Routes();
        r.setCommentsList(new ArrayList<>());
        r.setDescriptionsList(new ArrayList<>());
        r.setDifficulty(wr.getDifficulty());
        r.setDistance(wr.getDistance());
        r.setDuration(wr.getDuration());
        r.setEquipment(wr.getEquipment());
        if (wr.getIdChanging() != 0)
            r.setIdChanging(dao.getRoute(wr.getIdChanging()));
        r.setIdRoutes(ref(wr.getId()));
        r.setIsChange(wr.getIsChange());
        r.setIsProposed(wr.getIsProposal());
        r.setMultimediaList(new ArrayList<>());
        r.setName(wr.getName());
        r.setStart(ref(wr.getStartId()));
        r.setEnd(ref(wr.getEndId()));
        
        List<GeoLocation> gll = wr.getRoutePoints();
        LinkedList<Routepoints> rpl = new LinkedList<>();
        if (gll != null) {
            ListIterator<GeoLocation> glli = gll.listIterator();
            int i = 0;
            while (glli.hasNext()) {
                GeoLocation gl = glli.next();
                Routepoints rp = new Routepoints();
                rp.setNum(i);
                rp.setLatitude(gl.getLatitude());
                rp.setLongitude(gl.getLongitude());
                rpl.add(rp);
                i++;
            }
        }
        r.setRoutepointsList(rpl);
        
        List<Integer> pil = wr.getRoutePlacesIds();
        LinkedList<Placesroutes> prl = new LinkedList<>();
        if (pil != null) {
            ListIterator<Integer> pili = pil.listIterator();
            while (pili.hasNext()) {
                Integer pid = pili.next();
                if (pid != null) {
                    Placesroutes pr = new Placesroutes();
                    pr.setIdRoute(r);
                    pr.setIdPlace(dao.getPlace(pid));
                    prl.add(pr);
                }
            }
        }
        r.setPlacesroutesList(prl);
        
        r.setTodosList(new ArrayList<>());
        r.setType(wr.getRouteType());
        return r;
    }
    
    public static WanderUser convert(Users u) {
        if (u == null) return null;
        WanderUser wu = new WanderUser();
        wu.setEmail(u.getEmail());
        wu.setId(deref(u.getUserid()));
        wu.setInterest(u.getInterest());
        wu.setIsVerified(u.getVerificated());
        wu.setIsWheelchairUser(u.getWheelchair());
        wu.setLanguage(u.getLanguage());
        wu.setName(u.getName());
        //wu.setPasswordHash(u.getPasswordHash());
        wu.setUseBigFont(u.getBigfont());
        wu.setWalkingSpeed(u.getWalkingSpeed());
        wu.setUserLevel(u.getUserlevel());
        
        // Set metadata
        wu.setCommentCount(dao.getUserCommentCount(u));
        wu.setMultimediaCount(dao.getUserMultimediaCount(u));
        
        return wu;
    }
    
    public static Users convert(WanderUser wu) {
        if (wu == null) return null;
        Users u = new Users();
        u.setBigfont(wu.getUseBigFont());
        u.setCommentsList(new ArrayList<>());
        u.setEmail(wu.getEmail());
        u.setInterest(wu.getInterest());
        u.setLanguage(wu.getLanguage());
        u.setMultimediaList(new ArrayList<>());
        u.setName(wu.getName());
//        u.setPasswordHash(wu.getPasswordHash());
        u.setSearchesList(new ArrayList<>());
        u.setTodosList(new ArrayList<>());
        u.setUserid(ref(wu.getId()));
        u.setVerificated(wu.getIsVerified());
        u.setWalkingSpeed(wu.getWalkingSpeed());
        u.setWheelchair(wu.getIsWheelchairUser());
        u.setUserlevel(wu.getUserLevel());
        return u;
    }
    
    public static WanderMedia convert(Multimedia m) {
        if (m == null) return null;
        WanderMedia wm = new WanderMedia();
        wm.setData(m.getFile());
        wm.setId(deref(m.getIdMultimedia()));
        if (m.getIdUser() != null)
            wm.setIdAuthor(deref(m.getIdUser().getUserid()));
        if (m.getIdPlace() != null)
            wm.setIdPlace(deref(m.getIdPlace().getIdPlaces()));
        if (m.getIdRoute() != null)
            wm.setIdRoute(deref(m.getIdRoute().getIdRoutes()));
        wm.setMediaType(m.getType());
        wm.setTime(m.getTime());
        return wm;
    }
    
    public static Multimedia convert(WanderMedia wm) {
        if (wm == null) return null;
        Multimedia m = new Multimedia();
        m.setFile(wm.getData());
        m.setHeading(BigInteger.valueOf(42));
        m.setIdMultimedia(wm.getId());
        if (wm.getIdAuthor() != 0)
            m.setIdUser(dao.getUser(wm.getIdAuthor()));
        if (wm.getIdPlace() != 0)
            m.setIdPlace(dao.getPlace(wm.getIdPlace()));
        if (wm.getIdRoute() != 0)
            m.setIdRoute(dao.getRoute(wm.getIdRoute()));
        m.setLatitude(42.0);
        m.setLongitude(42.0);
        m.setType(wm.getMediaType());
        m.setIsVR(Boolean.FALSE);
        m.setTime(wm.getTime());
        return m;
    }
    
    private static int deref(Integer i) {
        if (i == null)
            return 0;
        else return i;
    }
    
    private static Integer ref(int i) {
        if (i == 0)
            return null;
        else return i;
    }
}
