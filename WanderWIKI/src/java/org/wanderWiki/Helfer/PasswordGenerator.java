//Klasse zum Senden von Mails
//beinhaltet alle Informationen des MailAcounts 
//des WanderWikis (Mailadresse / Passwort etc)
//erstellt: 02.04.2015
//Autor: Stephan Willmeroth + Andre Hantke 

package org.wanderWiki.Helfer;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Stephan
 */
public class PasswordGenerator {	
    private final static String CHARS_NUMERIC  = "1234567890";
    private final static String CHARS_ALPHA    = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private final static String CHARS_SPECIAL  = "@!#?";
    private final static Logger log = Logger.getLogger(PasswordGenerator.class.getName());
    
    public static enum PasswordMode {
        ALPHA, ALPHANUMERIC, NUMERIC, SPECIAL
    }

    /**
     * Generiert ein neues Passwort mit der angegeben Länge und im angegeben Modus mithilfe der Math.random() Methode. 
     * Dadurch nur pseudorandom, es handelt sich aber ja auch möglichst nur um ein pseudorandom Passwort.
     * Wenn der Modus SEPCIAL gewählt ist, wird sichergestellt, dass mindestens ein Sonderzeichen enthalten ist.
     * 
     * @param length Anzahl der Zeichen des zu generierenden Passwortes.
     * @param mode Enum, welches den Modus angibt. Aktuell existieren ALPHA (nur Buchstaben), NUMERIC (nur Zahlen), ALPHANUMERIC (Buchstaben und Zahlen) und SPECIAL (Buchstaben, Zahlen und Sonderzeichen)
     * @return generiertes Passwort als String.
     */
    public static String generateRandomPassword(int length, PasswordMode mode) 
    {
        StringBuilder builder = new StringBuilder();
        String characters = "";
        String password = "";

        switch(mode)
        {
            case ALPHA:
                characters = CHARS_ALPHA;
                break;
            case ALPHANUMERIC:
                characters = CHARS_ALPHA + CHARS_NUMERIC;
                break;
            case NUMERIC:
                characters = CHARS_NUMERIC;
                break;
            case SPECIAL:
                characters = CHARS_ALPHA + CHARS_NUMERIC + CHARS_SPECIAL;
                break;
        }

        int charactersLength = characters.length();

        for (int i = 0; i < length; i++) 
        {
            double index = Math.random() * charactersLength;
            builder.append(characters.charAt((int) index));
        }
        
        password = builder.toString();
        log.log(Level.FINE, "Generated password: {0}", password);
        
        // if SPECIAL, the password should have at least one special character
        if(mode == PasswordMode.SPECIAL && !hasSpecialCharacters(password))
        {
            log.log(Level.FINE, "password had no special characters, trying again!");
            return generateRandomPassword(length, mode);
        }

        return password;
    }
    
    private static boolean hasSpecialCharacters(String string)
    {
        // iterate through each special char
        for(int i = 0; i < CHARS_SPECIAL.length(); i++)
        {
            // check if string contains the char
            if( string.indexOf(CHARS_SPECIAL.charAt(i)) >= 0)
                return true;
        }
        
        return false;
    }
}
