package org.wanderWiki.Helfer;

import java.util.List;

/**
 *
 * @author MartinM
 */
public class Logic {
    private Logic() {
        throw new AssertionError("org.wanderWiki.Helfer.Logic is static.");
    }
    
    public static boolean equalsValid(Object a, Object b) {
        if ((a == null) || (b == null))
            return false;
        else return a.equals(b);
    }
    
    public static <T> boolean hasDupes(List<T> l) {
        return l.stream().anyMatch((o) -> (l.indexOf(o) != l.lastIndexOf(o)));
    }
}
