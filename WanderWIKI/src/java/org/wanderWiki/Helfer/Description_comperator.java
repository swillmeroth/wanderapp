/**
 * Klasse zum vergleichen und sortieren von Beschreibungen. Diese Klasse soll
 * anhand der übergebene Parameter Beschreibungen in eine natürliche Ordnung
 * bringen
 *
 * verantwortlich: Claas Schlonsok
 */
package org.wanderWiki.Helfer;

import java.util.Comparator;
import org.wanderWiki.entities.Descriptions;

/**
 * Klasse zum vergleichen und sortieren von Beschreibungen. Diese Klasse soll
 * anhand der übergebene Parameter Beschreibungen in eine natürliche Ordnung
 * bringen
 *
 * @author Claas
 */
public class Description_comperator implements Comparator<Object> {

    String interessen;
    String sprache;

    /**
     * Konstruktor welche die notwenigen Parameter erwartet, welche der Benutzer
     * als Vorlieben hat.
     *
     * @param interessen
     * @param sprache
     */
    public Description_comperator(String interessen, String sprache) {
        this.interessen = interessen;
        this.sprache = sprache;
    }

    @Override
    public int compare(Object o1, Object o2) {
        Descriptions d1 = (Descriptions) o1;
        Descriptions d2 = (Descriptions) o2;

        //als erste nach der Sprache sortieren
        //dann das eigene interesierte über alles andere
        //sonst 0;
        if (d1.getLanguage().equals(sprache) && !d2.getLanguage().equals(sprache)) {
            return 1;
        } else if (!d1.getLanguage().equals(sprache) && d2.getLanguage().equals(sprache)) {
            return -1;
        } else {
            //Beide haben die gleiche sprache

            if (d1.getInterest().equals(interessen) && !d2.getInterest().equals(interessen)) {
                return 1;
            } else if (!d1.getInterest().equals(interessen) && d2.getInterest().equals(interessen)) {
                return -1;
            } else {
                return -1;
            }

        }

    }

}
