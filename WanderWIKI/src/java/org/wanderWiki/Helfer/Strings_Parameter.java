/**
 * Klasse zur Verwaltung der über JSON bereitgestellen Parameter
 *
 * verantwortlich: Claas Schlonsok
 */
package org.wanderWiki.Helfer;

import java.lang.reflect.Field;

/**
 * VERALTET Klasse zur Verwaltung der über JSON bereitgestellen Parameter. 
 * Hier
 * werden alle Parameter gehalten
 *
 * @author Claas
 * @deprecated Wird nicht mehr verwendet
 */
public class Strings_Parameter {

    //Parameter
    public static final String EMAIL = "email";
    public static final String PASSWORD = "pwd";
    public static final String NAME = "name";

    /**
     * Diese Methode gibt alle in dieser Klasse vorhandenne Elemente zurück
     *
     * @return
     */
    public static String gibt_elemente() {
        String Strings = "";

        Field[] felder = Strings_Parameter.class.getFields();
        for (int i = 0; i < felder.length; i++) {
            Field feld = felder[i];
            try {
                Strings = Strings + ", " + feld.get(null).toString();
            } catch (Exception e) {
            }

        }

        return Strings;
    }

}
