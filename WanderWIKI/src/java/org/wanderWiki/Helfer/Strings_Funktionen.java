/**
 * Klasse zur Verwaltung der über JSON bereitgestellen Funktionen
 *
 * verantwortlich: Claas Schlonsok
 */
package org.wanderWiki.Helfer;

import java.lang.reflect.Field;

/**
 * VERALTET
 * Klasse zur Verwaltung der über JSON bereitgestellen Funktionen
 * Hier werden alle Funktionen gehalten
 *
 * @author Claas
 * @deprecated Wird nicht mehr verwendet
 */
public class Strings_Funktionen {

    //name der Funktionen für den Aufruf
    public static final String FUNC_LOGIN = "login";
    public static final String FUNC_REGISTRIEREN = "regis";
    
    
    /**
     * Diese Methode gibt alle in dieser Klasse vorhandenne Elemente zurück
     * @return 
     */
    public static String gibt_elemente() {
        String Strings = "";

        Field[] felder = Strings_Funktionen.class.getFields();
        for (int i = 0; i < felder.length; i++) {
            Field felder1 = felder[i];
            try {
                Strings = Strings + ", " + felder1.get(null).toString();
            } catch (Exception e) {
            }

        }

        return Strings;
    }

}
