/**
 * Klasse zur Verwaltung der über JSON bereitgestellen Session-Paramter
 *
 * verantwortlich: Claas Schlonsok
 */
package org.wanderWiki.Helfer;

import java.lang.reflect.Field;

/**
 * VERALTET
 * Klasse zur Verwaltung der über JSON bereitgestellen Session-Paramter
 * Hier werden alle Session-Parameter gehalten.
 * @author Claas
 * @deprecated Wird nicht mehr verwendet
 */
public class Strings_Session {

 
    public static final String SESSION_USER = "Session_user";

    
    /**
     * Diese Methode gibt alle in dieser Klasse vorhandenne Elemente zurück
     * @return 
     */
    public static String gibt_elemente() {
        String Strings = "";

        Field[] felder = Strings_Session.class.getFields();
        for (int i = 0; i < felder.length; i++) {
            Field feld = felder[i];
            try {
                Strings = Strings + ", " + feld.get(null).toString();
            } catch (Exception e) {
            }

        }

        return Strings;
    }

}
