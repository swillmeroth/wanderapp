//Klasse zum Senden von Mails
//beinhaltet alle Informationen des MailAcounts 
//des WanderWikis (Mailadresse / Passwort etc)
//erstellt: 02.04.2015
//Autor: Andre Hantke

package org.wanderWiki.Helfer;

import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.wanderWiki.entities.Users;

/**
 *
 * @author Andre
 */
public class MailMessage {

    private static MailMessage mailsender = null;
    
    
    //Verbindungsdaten zu GMail Account des WanderWIKI
    private final String USERNAME   = "wanderwikimail";
    private final String PASSWORD   = "w4nDERw1k1?";
    private final String MAIL_FROM  = "wanderwikimail@gmail.com";
    
    private final Authenticator authenticator;
    private Properties properties = new Properties();
    
    //Konstruktor, setzte Properties
    private MailMessage() {
        properties.put("mail.smtp.host", "smtp.gmail.com");
        properties.put("mail.smtp.socketFactory.port", "465");
        properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.port", "465");
        
        //authenticator 
        authenticator = new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(USERNAME, PASSWORD); 
            }
        };
    }
    
    public static synchronized MailMessage getInstance()
    {
        if( mailsender == null)
            mailsender = new MailMessage();
        
        return mailsender;
    }
    
    /**
     * Sendet eine Email mit dem angegebenen Betreff und Text an die angegebene Adresse.
     * @param recipient E-Mail-Adresse des Empfängers
     * @param subject Betreff der E-Mail
     * @param text Inhalt der Mail
     */
    public void sendGenericMail(String text, String subject, String recipient)
    {
        try {
            Session session = Session.getDefaultInstance(properties, authenticator);
            
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(MAIL_FROM));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipient));
            message.setSubject(subject);
            message.setText(text);
            Transport.send(message);
        } catch (MessagingException ex) {
            Logger.getLogger(MailMessage.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Sendet eine Email an den Nutzer mit seinem Passwort.
     * @param user Entityklasse des Benutzers
     */
    public void sendPasswordMail(Users user)
    {
        StringBuilder builder = new StringBuilder();
        builder.append("Hallo ");
        builder.append(user.getName());
        builder.append(",");
        builder.append("\n\nIhr neues Passwort lautet: ");
        builder.append(user.getPasswordHash());
        builder.append("\n\n");
        builder.append("Wir wünschen weiterhin viel Spaß mit dem WanderWIKI!");
        builder.append("\n\n");
        builder.append("Ihr WanderWIKI-Team");

        String subject = "Ihr Passwort für das WanderWIKI";
        String text = builder.toString();

        sendGenericMail(text, subject, user.getEmail());
    }
    
    /**
     * Sendet eine Willkommensmail an einen Benutzer. 
     * Diese Methode sollte nach der Registrierung aufgerufen werden und
     * enthält neben einer Begrüßung die Zugangsdaten des Nutzers.
     * @param user Entityklasse des Benutzers
     */
    public void sendWelcomeMail(Users user)
    {
        String subject = "Willkommen beim WanderWIKI";
        
        StringBuilder builder = new StringBuilder();
        builder.append("Herzlichen Willkommen ");
        builder.append(user.getName());
        builder.append(",\n\n");
        builder.append("Ihre Zugangsdaten sind: ");
        builder.append("\n\n");
        builder.append("E-Mail: ");
        builder.append(user.getEmail());
        builder.append("\n");
        builder.append("Passwort: ");
        builder.append(user.getPasswordHash());
        builder.append("\n\n");
        builder.append("Wir wünschen viel Spaß mit dem WanderWIKI!");
        builder.append("\n\n");
        builder.append("Weitere Einstellungen können nach erfolgreichem Einloggen,");
        builder.append("\nunter 'Mein Konto' getätigt werden.");
        builder.append("\n\n");
        builder.append("Ihr WanderWIKI-Team");
        String text = builder.toString();
        
        sendGenericMail(text, subject, user.getEmail());
    }
}

