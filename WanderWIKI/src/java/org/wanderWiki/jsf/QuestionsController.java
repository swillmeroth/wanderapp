/*******************************************************
* WanderApp
* erstellt am 18.03.2015
* JSF Controller für den Adminbereich von Fragen .
* Verantwortlich: Claas Schlonsok
* Version 0.1
********************************************************/
package org.wanderWiki.jsf;

import org.wanderWiki.entities.Questions;
import org.wanderWiki.jsf.util.JsfUtil;
import org.wanderWiki.jsf.util.JsfUtil.PersistAction;
import org.wanderWiki.jpa.QuestionsFacade;

import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@ManagedBean(name = "questionsController")
@SessionScoped
public class QuestionsController implements Serializable {


    private org.wanderWiki.jpa.QuestionsFacade ejbFacade;
    private List<Questions> items = null;
    private Questions selected;

    public QuestionsController() {
        ejbFacade = new QuestionsFacade();
    }

    public Questions getSelected() {
        return selected;
    }

    public void setSelected(Questions selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private QuestionsFacade getFacade() {
        return ejbFacade;
    }

    public Questions prepareCreate() {
        selected = new Questions();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/org.wanderWiki.language/messages").getString("QuestionsCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/org.wanderWiki.language/messages").getString("QuestionsUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/org.wanderWiki.language/messages").getString("QuestionsDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<Questions> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/org.wanderWiki.language/messages").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/org.wanderWiki.language/messages").getString("PersistenceErrorOccured"));
            }
        }
    }

    public Questions getQuestions(java.lang.Integer id) {
        return getFacade().find(id);
    }

    public List<Questions> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<Questions> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = Questions.class)
    public static class QuestionsControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            QuestionsController controller = (QuestionsController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "questionsController");
            return controller.getQuestions(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Questions) {
                Questions o = (Questions) object;
                return getStringKey(o.getIdquestions());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Questions.class.getName()});
                return null;
            }
        }

    }

}
