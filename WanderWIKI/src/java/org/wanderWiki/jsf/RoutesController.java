/*******************************************************
* WanderApp
* erstellt am 18.03.2015
* JSF Controller für den Adminbereich von Routen .
* Verantwortlich: Claas Schlonsok
* Version 0.1
********************************************************/
package org.wanderWiki.jsf;

import org.wanderWiki.entities.Routes;
import org.wanderWiki.jsf.util.JsfUtil;
import org.wanderWiki.jsf.util.JsfUtil.PersistAction;
import org.wanderWiki.jpa.RoutesFacade;

import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@ManagedBean(name = "routesController")
@SessionScoped
public class RoutesController implements Serializable {


    private org.wanderWiki.jpa.RoutesFacade ejbFacade;
    private List<Routes> items = null;
    private Routes selected;

    public RoutesController() {
        ejbFacade = new RoutesFacade();
    }

    public Routes getSelected() {
        return selected;
    }

    public void setSelected(Routes selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private RoutesFacade getFacade() {
        return ejbFacade;
    }

    public Routes prepareCreate() {
        selected = new Routes();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/org.wanderWiki.language/messages").getString("RoutesCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/org.wanderWiki.language/messages").getString("RoutesUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/org.wanderWiki.language/messages").getString("RoutesDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<Routes> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/org.wanderWiki.language/messages").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/org.wanderWiki.language/messages").getString("PersistenceErrorOccured"));
            }
        }
    }

    public Routes getRoutes(java.lang.Integer id) {
        return getFacade().find(id);
    }

    public List<Routes> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<Routes> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = Routes.class)
    public static class RoutesControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            RoutesController controller = (RoutesController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "routesController");
            return controller.getRoutes(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Routes) {
                Routes o = (Routes) object;
                return getStringKey(o.getIdRoutes());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Routes.class.getName()});
                return null;
            }
        }

    }

}
