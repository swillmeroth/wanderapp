/*******************************************************
* WanderApp
* erstellt am 18.03.2015
* JSF Controller für den Adminbereich von TODOS .
* Verantwortlich: Claas Schlonsok
* Version 0.1
********************************************************/
package org.wanderWiki.jsf;

import org.wanderWiki.entities.Todos;
import org.wanderWiki.jsf.util.JsfUtil;
import org.wanderWiki.jsf.util.JsfUtil.PersistAction;
import org.wanderWiki.jpa.TodosFacade;

import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@ManagedBean(name = "todosController")
@SessionScoped
public class TodosController implements Serializable {


    private org.wanderWiki.jpa.TodosFacade ejbFacade;
    private List<Todos> items = null;
    private Todos selected;

    public TodosController() {
        ejbFacade = new TodosFacade();
    }

    public Todos getSelected() {
        return selected;
    }

    public void setSelected(Todos selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private TodosFacade getFacade() {
        return ejbFacade;
    }

    public Todos prepareCreate() {
        selected = new Todos();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/org.wanderWiki.language/messages").getString("TodosCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/org.wanderWiki.language/messages").getString("TodosUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/org.wanderWiki.language/messages").getString("TodosDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<Todos> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/org.wanderWiki.language/messages").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/org.wanderWiki.language/messages").getString("PersistenceErrorOccured"));
            }
        }
    }

    public Todos getTodos(java.lang.Integer id) {
        return getFacade().find(id);
    }

    public List<Todos> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<Todos> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = Todos.class)
    public static class TodosControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            TodosController controller = (TodosController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "todosController");
            return controller.getTodos(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Todos) {
                Todos o = (Todos) object;
                return getStringKey(o.getIdToDos());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Todos.class.getName()});
                return null;
            }
        }

    }

}
