/*******************************************************
* WanderApp
* erstellt am 18.03.2015
* Fassade die die AbstractFassade für die Persistierung von Routenpunkten realisiert.
* Verantwortlich: Claas Schlonsok
* Version 0.1
********************************************************/
package org.wanderWiki.jpa;

import java.util.List;
import javax.ejb.Stateless;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.wanderWiki.beans.JPAResourceBean;
import org.wanderWiki.entities.Routepoints;

/**
 *
 * @author Claas
 */
@ManagedBean (name = "RoutepointsFacade")
@SessionScoped
public class RoutepointsFacade extends AbstractFacade<Routepoints> {
//    @PersistenceContext(unitName = "WanderWIKIPU")
    private EntityManager em;

    /**
     *
     * @return
     */
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    /**
     * Konstruktor. Stellt verbindung zu der JPA her und setzt den EntityManger.
     * 
     * @author Claas
     */
    public RoutepointsFacade() {
        super(Routepoints.class);
        
        JPAResourceBean jpaResourceBean=new JPAResourceBean();
        this.em=jpaResourceBean.getEMF().createEntityManager();
        System.out.println("Connected to DAOJPA RoutepointsFacade ");
    }
    
    /**
     * Selektiert alle Routepoints die der Anfang von akzeptierten Routen sind.
     * 
     * @return Liste der Routepoints
     * @author Claas
     */
    public List<Routepoints> find_Startpunkte_nicht_proposal() {
//         Query s = getEntityManager().createQuery("SELECT rp FROM Routes r, Routepoints rp WHERE rp.num = 0 AND r = rp.idRoute  AND r.isProposed = 0");

        Query s = getEntityManager().createNamedQuery("Routepoints.find_Startpunkt_nicht_proposal", Routepoints.class);
//                .setParameter("isProposed",new Boolean(false))
//                .setParameter("num", new Integer(0));

        System.out.println(s.toString());
        return s.getResultList();
    }
    
}
