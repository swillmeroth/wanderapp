/*******************************************************
* WanderApp
* erstellt am 07.03.2015
* AbstractFassade beschreibt die allgemeine Persistierung von Objekten
* Verantwortlich: Claas Schlonsok
* Version 0.1
********************************************************/
package org.wanderWiki.jpa;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

/**
 * Abstrakte Fasse für die Allgemeine Persistierung von Daten
 * @author Claas
 * @param <T>
 */
public abstract class AbstractFacade<T> {
    private Class<T> entityClass;

    /**
     * Instanzierung der realisierenden Klasse
     * @param entityClass
     */
    public AbstractFacade(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    /**
     * Soll einen EntitiyManager zurückliefern
     * @return
     */
    protected abstract EntityManager getEntityManager();

    /**
     * Persistiert eine Entity, egal welcher Klasse.
     * @param entity
     */
    public void create(T entity) {
        EntityTransaction et = getEntityManager().getTransaction();
        et.begin();
        System.out.println("create ausgeführt");
        getEntityManager().persist(entity);
        et.commit();
    }

    /**
     * Bearbeitet eine Klasse, egal welcher Klasse.
     * @param entity
     */
    public void edit(T entity) {
        EntityTransaction et = getEntityManager().getTransaction();
        et.begin();
        System.out.println("edit ausgeführt");
        getEntityManager().merge(entity);
        et.commit();
    }

    /**
     * Löscht eine Klasse, egal welcher Klasse.
     * @param entity
     */
    public void remove(T entity) {
        System.out.println("remove ausgeführt "+entity.toString());
//        getEntityManager().refresh(entity);
        EntityTransaction et = getEntityManager().getTransaction();
        et.begin();
        getEntityManager().remove(getEntityManager().merge(entity));
        et.commit();
//        getEntityManager().
//        getEntityManager().remove(getEntityManager().merge(entity));
    }

    /**
     * Sucht ein Objekt, egal welcher Klasse.
     * @param id Die id des Objekts.
     * @return
     */
    public T find(Object id) {
        System.out.println("find ausgeführt " + id);
        return getEntityManager().find(entityClass, id);
    }

    /**
     * Liefert alle Objekte einer Klasse Klasse.
     * @return Liste mit den Objekten.
     */
    public List<T> findAll() {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        return getEntityManager().createQuery(cq).getResultList();
    }

    /**
     * Findet alle Objekte deren id in einem bestimmten Bereich liegen.
     * @param range Bereich der Objekte.
     * @return Liste mit den Objekten.
     */
    public List<T> findRange(int[] range) {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0] + 1);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    /**
     * Zähl die Objekte.
     * @return
     */
    public int count() {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        javax.persistence.criteria.Root<T> rt = cq.from(entityClass);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    @Override
    protected void finalize() throws Throwable {
        getEntityManager().close();
    }
    
    
}
