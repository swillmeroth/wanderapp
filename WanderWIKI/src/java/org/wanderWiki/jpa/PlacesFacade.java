/**
 * *****************************************************
 * WanderApp erstellt am 18.03.2015 Fassade die die AbstractFassade für die
 * Persistierung von POIs realisiert. Verantwortlich: Claas Schlonsok Version
 * 0.1
*******************************************************
 */
package org.wanderWiki.jpa;

import java.util.List;
import javax.ejb.Stateless;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.eclipse.persistence.jpa.config.NamedQuery;
import org.wanderWiki.beans.JPAResourceBean;
import org.wanderWiki.entities.Places;
import org.wanderWiki.entities.Routes;

/**
 *
 * @author Claas
 */
@ManagedBean(name = "PlacesFacade")
@SessionScoped
public class PlacesFacade extends AbstractFacade<Places> {
//    @PersistenceContext(unitName = "WanderWIKIPU")

    private EntityManager em;

    /**
     *
     * @return
     */
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    /**
     * Konstruktor. Stellt verbindung zu der JPA her und setzt den EntityManger.
     * 
     * @author Claas
     */
    public PlacesFacade() {
        super(Places.class);

        JPAResourceBean jpaResourceBean = new JPAResourceBean();
        this.em = jpaResourceBean.getEMF().createEntityManager();
        System.out.println("Connected to DAOJPA PlacesFacade ");
    }

    /**
     * Erweiterung um die möglichkeit nur die Proposalen Places zu finden.
     * 
     * @return Liste mit den Proposalen Places
     * @author Claas
     */
    public List<Places> findAllProposal() {
        Query s = getEntityManager().createNamedQuery("Places.findByIsProposal",Places.class)
                .setParameter("isProposal",new Boolean(true));

        return s.getResultList();
    }

    /**
     * Erweiterung um die möglichkeit nur die NICHT Proposalen Places zu finden.
     * 
     * @return Liste mit den NICHT Proposalen Places.
     * @author Claas
     */
    public List<Places> findAllNOTProposal() {
        Query s = getEntityManager().createNamedQuery("Places.findByIsProposal", Places.class)
                .setParameter("isProposal", new Boolean(false));

        return s.getResultList();
    }
    
    /**
     * Selektiert nur die Places die in einer bestimmten Route vorkommen.
     * 
     * @param Route ID der Route.
     * @return Liste mit den POI die Teil der Route sind.
     */
    public List<Places> findbyIdRoute(int Route) {
        Integer r = new Integer(Route);
        Query s = getEntityManager().createNamedQuery("Places.findByRoute", Places.class)
                .setParameter("idRoute", r);

        return s.getResultList();
    }

}
