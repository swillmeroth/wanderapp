/*******************************************************
* WanderApp
* erstellt am 07.03.2015
* Fassade die die AbstractFassade für die Persistierung von Benutzer realisiert.
* Verantwortlich: Claas Schlonsok
* Version 0.1
********************************************************/
package org.wanderWiki.jpa;

import javax.ejb.ApplicationException;
import javax.ejb.Stateless;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.wanderWiki.beans.JPAResourceBean;
import org.wanderWiki.entities.Users;

/**
 *
 * @author Claas
 */
@ManagedBean (name = "UsersFacade")
@SessionScoped
public class UsersFacade extends AbstractFacade<Users> {
//    @PersistenceContext(unitName = "WanderWIKIPU")
    private EntityManager em;
    double erstellung;
    
    /**
     *
     * @return
     */
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    /**
     * Konstruktor. Stellt verbindung zu der JPA her und setzt den EntityManger.
     * 
     * @author Claas
     */
    public UsersFacade() {
        super(Users.class);
        JPAResourceBean jpaResourceBean=new JPAResourceBean();
        this.em=jpaResourceBean.getEMF().createEntityManager();
        erstellung = Math.random();
        System.out.println("Connected to DAOJPA USERFACADE "+erstellung);
    }
    
}
