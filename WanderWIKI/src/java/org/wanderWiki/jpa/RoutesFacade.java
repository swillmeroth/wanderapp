/*******************************************************
* WanderApp
* erstellt am 18.03.2015
* Fassade die die AbstractFassade für die Persistierung von Routen realisiert.
* Verantwortlich: Claas Schlonsok
* Version 0.1
********************************************************/
package org.wanderWiki.jpa;

import java.util.List;
import javax.ejb.Stateless;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.wanderWiki.beans.JPAResourceBean;
import org.wanderWiki.entities.Routes;

/**
 *
 * @author Claas
 */
@ManagedBean(name = "RoutesFacade")
@SessionScoped
public class RoutesFacade extends AbstractFacade<Routes> {
//    @PersistenceContext(unitName = "WanderWIKIPU")
    private EntityManager em;

    /**
     *
     * @return
     */
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    /**
     * Konstruktor. Stellt verbindung zu der JPA her und setzt den EntityManger.
     * 
     * @author Claas
     */
    public RoutesFacade() {
        super(Routes.class);

        JPAResourceBean jpaResourceBean = new JPAResourceBean();
        this.em = jpaResourceBean.getEMF().createEntityManager();
        System.out.println("Connected to DAOJPA RoutesFacade ");
    }

    /**
     * Selektiert alle Routen deren Startpunkte nicht Proposal sind.
     * 
     * @return Liste der Routen
     * @author Claas
     */
    public List<Routes> find_Startpunkte_nicht_proposal() {
//        Query s = getEntityManager().createQuery("SELECT rp FROM Routes r, Routepoints rp WHERE rp.num = 0 AND r = rp.idRoute  AND r.isProposed = 0");
        Query s = getEntityManager().createNamedQuery("Routes.find_Startpunkt_nicht_proposal", Routes.class);
//                .setParameter("isProposed",new Boolean(false))
//                .setParameter("num", new Integer(0));

        return s.getResultList();
    }

}
