/*******************************************************
* WanderApp
* erstellt am 18.03.2015
* Fassade die die AbstractFassade für die Persistierung von Multimedia realisiert.
* Verantwortlich: Claas Schlonsok
* Version 0.1
********************************************************/
package org.wanderWiki.jpa;

import javax.ejb.Stateless;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.wanderWiki.beans.JPAResourceBean;
import org.wanderWiki.entities.Multimedia;

/**
 *
 * @author Claas
 */
@ManagedBean (name = "MultimediaFacade")
@SessionScoped
public class MultimediaFacade extends AbstractFacade<Multimedia> {
//    @PersistenceContext(unitName = "WanderWIKIPU")
    private EntityManager em;

    /**
     *
     * @return
     */
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    /**
     * Konstruktor. Stellt verbindung zu der JPA her und setzt den EntityManger.
     * 
     * @author Claas
     */
    public MultimediaFacade() {
        super(Multimedia.class);
        
        JPAResourceBean jpaResourceBean=new JPAResourceBean();
        this.em=jpaResourceBean.getEMF().createEntityManager();
        System.out.println("Connected to DAOJPA MultimediaFacade ");
    }
    
}
