package org.wanderWiki.rest;

import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author MartinM
 */
@javax.ws.rs.ApplicationPath("webresources")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(org.wanderWiki.rest.CommentsResource.class);
        resources.add(org.wanderWiki.rest.DescriptionsResource.class);
        resources.add(org.wanderWiki.rest.MapResource.class);
        resources.add(org.wanderWiki.rest.MultimediaResource.class);
        resources.add(org.wanderWiki.rest.PlaceResource.class);
        resources.add(org.wanderWiki.rest.PlacesResource.class);
        resources.add(org.wanderWiki.rest.QuestionsResource.class);
        resources.add(org.wanderWiki.rest.RouteResource.class);
        resources.add(org.wanderWiki.rest.RoutesResource.class);
        resources.add(org.wanderWiki.rest.SearchesResource.class);
        resources.add(org.wanderWiki.rest.TodosResource.class);
        resources.add(org.wanderWiki.rest.UserResource.class);
        resources.add(org.wanderWiki.rest.UsersResource.class);
        resources.add(org.wanderWiki.rest.exceptions.AuthenticationExceptionMapper.class);
        resources.add(org.wanderWiki.rest.exceptions.NoResultExceptionMapper.class);
        resources.add(org.wanderWiki.rest.exceptions.QueryTimeoutExceptionMapper.class);
        resources.add(org.wanderWiki.rest.exceptions.ResourceNotFoundExceptionMapper.class);
    }
    
}
