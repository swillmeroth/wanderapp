package org.wanderWiki.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import org.wanderWiki.Helfer.EntityClassConverter;
import org.wanderWiki.entities.Comments;
import org.wanderWiki.entities.Users;
import org.wanderWiki.rest.exceptions.AuthenticationException;

/**
 *
 * @author MartinM
 */
@Path("comments")
public class CommentsResource extends GenericResource {
    
    @DELETE @Path("{id}")
    @Consumes("application/json;charset=utf-8") @Produces("application/json;charset=utf-8")
    public Response deleteComment(@PathParam("id") int id) {
        Users u = UsersResource.requireAuth(headers);
        if (id > 0) {
            Comments c = dao.getComment(id);
            if (c.getIdUser().equals(u) || u.isAdmin()) {
                dao.deleteComment(c);
                return Response.ok().build();
            } else {
                throw new AuthenticationException("This is not your comment.");
            }
        } else {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }
    
    @GET @Path("{id}")
    @Consumes("application/json;charset=utf-8") @Produces("application/json;charset=utf-8")
    public Response getComment(@PathParam("id") int id) {
        if (id > 0) {
            Comments c = dao.getComment(id);
            return Response.ok(getGson().toJson(EntityClassConverter.convert(c))).build();
        } else {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }
}
