package org.wanderWiki.rest;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import javax.persistence.NoResultException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import org.wanderWiki.Helfer.EntityClassConverter;
import org.wanderWiki.Helfer.Logic;
import org.wanderWiki.Helfer.MailMessage;
import org.wanderWiki.entities.Users;
import org.wanderWiki.rest.exceptions.AuthenticationException;
import org.wanderWiki.rest.exceptions.BadRequestException;
import regionale.whs.wanderapp.wspojos.Credentials;
import regionale.whs.wanderapp.wspojos.PasswordUpdate;
import regionale.whs.wanderapp.wspojos.RegistrationData;
import regionale.whs.wanderapp.wspojos.WanderUser;

/**
 *
 * @author MartinM
 */
@Path("users")
public class UsersResource extends GenericResource {
    
    private static final String HEADER_NAME_EMAIL = "x-wapp-email";
    private static final String HEADER_NAME_PASSWORD = "x-wapp-password";
    
    @POST @Path("password")
    @Consumes("application/json;charset=utf-8") @Produces("application/json;charset=utf-8")
    public Response changePassword(String puString) {
        PasswordUpdate pu = getGson().fromJson(puString, PasswordUpdate.class);
        PasswordUpdate.validate(pu);
        Users u = requireAuth(headers);
        u.setPasswordHash(pu.newPassword);
        dao.update(u);
        return Response.ok().build();
    }
    
    @POST
    @Consumes("application/json;charset=utf-8") @Produces("application/json;charset=utf-8")
    public Response register(String dString) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        RegistrationData d = getGson().fromJson(dString, RegistrationData.class);
//        System.out.println("String: " + dString);
//        System.out.println("Object: " + d.toString());
        RegistrationData.validate(d);
        
        try {
            Users u1 = dao.getUserByName(d.getDisplayname());
            // If this does not throw a NoResultException, that name is already taken.
            return Response.status(Response.Status.CONFLICT).build();
        } catch (NoResultException ex) {}
        
        try {
            Users u2 = dao.getUserByEmail(d.getCredentials().getEmail());
            // No special response for conflict with email! Otherwise attackers could sniff out email addresses.
            return Response.ok().build();
        } catch (NoResultException ex) {}
            
        Users u = new Users();
        u.setEmail(d.getCredentials().getEmail());
        u.setPasswordHash(d.getCredentials().getPassword());
        u.setName(d.getDisplayname());
//                MessageDigest d = MessageDigest.getInstance("SHA-256");
//                d.reset();
//                String base64PasswordHash = DatatypeConverter.printBase64Binary(d.digest(c.getPassword().getBytes("UTF-8")));
//                u.setPasswordHash(base64PasswordHash);

        u.setBigfont(Boolean.FALSE);
        u.setInterest("historisch");
        u.setLanguage("de");
        u.setVerificated(Boolean.TRUE);
        u.setWalkingSpeed("normal");
        u.setWheelchair(Boolean.FALSE);
        u.setUserlevel(0);
        dao.addUser(u);
        MailMessage.getInstance().sendWelcomeMail(u);
        return Response.ok().build();
    }
    
    @POST @Path("authentication")
    @Consumes("application/json;charset=utf-8") @Produces("application/json;charset=utf-8")
    public Response testCredentials(String cString) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        Credentials c = getGson().fromJson(cString, Credentials.class);
        Credentials.validate(c);
        Users u = auth(c.getEmail(), c.getPassword());
        if (u != null) {
            WanderUser wu = EntityClassConverter.convert(u);
            System.out.println(wu.toString());
            return Response.ok(getGson().toJson(wu)).build();
        }
        return Response.status(Response.Status.FORBIDDEN).build();
    }
    
    @Path("{id}")
    @Consumes("application/json;charset=utf-8") @Produces("application/json;charset=utf-8")
    public UserResource user(@PathParam("id") int id) {
        return resContext.initResource(new UserResource(dao.getUser(id)));
    }
    
    @GET @Path("{id}")
    @Consumes("application/json;charset=utf-8") @Produces("application/json;charset=utf-8")
    public Response getUser(@PathParam("id") int id) {
        Users requestingUser = auth(headers);
        WanderUser requestedUser = EntityClassConverter.convert(dao.getUser(id));
        if ((requestingUser == null) || (!Logic.equalsValid(requestingUser.getEmail(), requestedUser.getEmail()))) {
            requestedUser.setEmail("");
        }
        return Response.ok(getGson().toJson(requestedUser)).build();
    }
    
    @POST @Path("{id}")
    @Consumes("application/json;charset=utf-8") @Produces("application/json;charset=utf-8")
    public Response updateUser(String uString, @PathParam("id") int id) {
        Users u = requireAuth(headers);
        WanderUser wu = getGson().fromJson(uString, WanderUser.class);
        wu.setId(id);
        WanderUser.validate(wu);
        if (u.getUserid() != wu.getId()) {
            throw new AuthenticationException("Nope.");
        }
        if ((wu.getUserLevel() != u.getUserlevel()) && (!u.isAdmin())) {
            throw new AuthenticationException("Nope.");
        }
        if (!wu.getName().equals(u.getName())) {
            throw new BadRequestException("You cannot change your name.");
        }
        if (!wu.getEmail().equals(u.getEmail())) {
            try {
                Users existingUser = dao.getUserByEmail(wu.getEmail());
                return Response.status(Response.Status.CONFLICT).header("xxx-reason", "That E-Mail address is already in use.").build();
            } catch (NoResultException ex) {}
        }
        
        Users newU = EntityClassConverter.convert(wu);
        newU.setPasswordHash(u.getPasswordHash());
        dao.update(newU);
        return Response.ok().build();
    }
    
    @DELETE @Path("{id}")
    @Consumes("application/json;charset=utf-8") @Produces("application/json;charset=utf-8")
    public Response deleteUser(@PathParam("id") int id) {
        Users u = requireAuth(headers);
        if (id > 0) {
            Users deleteUser = dao.getUser(id);
            if (Logic.equalsValid(deleteUser.getUserid(), u.getUserid())) {
                dao.deleteUser(deleteUser);
                return Response.ok().build();
            } else {
                throw new AuthenticationException("Can not delete someone else's user account.");
            }
        }
        return Response.status(Response.Status.BAD_REQUEST).build();
    }
    
    static Users auth(String email, String password) {
        try {
            Users u = dao.getUserByEmail(email);
//                MessageDigest d = MessageDigest.getInstance("SHA-256");
//                d.reset();
//                String base64PasswordHash = DatatypeConverter.printBase64Binary(d.digest(password.getBytes("UTF-8")));
            if (password.equals(u.getPasswordHash())) {
                return u;
            }
        } catch (NoResultException ex) {
        }
        return null;
    }
    
    static Users auth(HttpHeaders headers) {
        Credentials c = parseCredentials(headers);
        try {
            Users u = dao.getUserByEmail(c.getEmail());
//                MessageDigest d = MessageDigest.getInstance("SHA-256");
//                d.reset();
//                String base64PasswordHash = DatatypeConverter.printBase64Binary(d.digest(password.getBytes("UTF-8")));
            if (c.getPassword().equals(u.getPasswordHash())) {
                return u;
            }
        } catch (NoResultException ex) {
        }
        return null;
    }
    
    static Users requireAuth(HttpHeaders headers) {
        Users u = auth(headers);
        if (u == null) throw new AuthenticationException("Bad email or password.");
        return u;
    }

    private static Credentials parseCredentials(HttpHeaders headers) {
        Credentials c = new Credentials();
        if (headers == null)
            return c;
        List<String> parsedEmailsList = headers.getRequestHeader(HEADER_NAME_EMAIL);
        List<String> parsedPasswordsList = headers.getRequestHeader(HEADER_NAME_PASSWORD);
        if ((parsedEmailsList != null) && (!parsedEmailsList.isEmpty())) {
            String parsedEmail = parsedEmailsList.get(0);
            c.setEmail((parsedEmail == null) ? "" : parsedEmail);
        }
        if ((parsedPasswordsList != null) && (!parsedPasswordsList.isEmpty())) {
            String parsedPassword = parsedPasswordsList.get(0);
            c.setPassword((parsedPassword == null) ? "" : parsedPassword);
        }
//        System.out.println("parseCredentials: " + c);
        return c;
    }
    
    private static boolean hasAuthAttempt(HttpHeaders headers) {
        Credentials c = parseCredentials(headers);
        return ((!c.getEmail().isEmpty()) && (!c.getPassword().isEmpty()));
    }
    
    static Users requireAdmin(HttpHeaders headers) {
        Users u = requireAuth(headers);
        if (!u.isAdmin())
            throw new AuthenticationException("Admin privileges (userlevel 2) are required, "
                    + "but you only got userlevel " + u.getUserlevel() + ".");
        return u;
    }
    
    static double getUserWalkingSpeed(Users u) {
        if (u == null) return 1.4;
        if ("langsam".equals(u.getWalkingSpeed())) return 0.3;
        if ("normal".equals(u.getWalkingSpeed())) return 1.4;
        if ("schnell".equals(u.getWalkingSpeed())) return 1.9;
        return 1.4;
    }
    
}
