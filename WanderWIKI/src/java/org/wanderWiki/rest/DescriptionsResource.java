package org.wanderWiki.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import org.wanderWiki.Helfer.EntityClassConverter;
import org.wanderWiki.entities.Descriptions;
import org.wanderWiki.entities.Users;

/**
 *
 * @author MartinM
 */
@Path("descriptions")
public class DescriptionsResource extends GenericResource {
    
    @DELETE @Path("{id}")
    @Consumes("application/json;charset=utf-8") @Produces("application/json;charset=utf-8")
    public Response deleteDescription(@PathParam("id") int id) {
        Users u = UsersResource.requireAdmin(headers);
        if (id > 0) {
            Descriptions d = dao.getDescription(id);
            dao.deleteDescription(d);
            return Response.ok().build();
        }
        return Response.status(Response.Status.BAD_REQUEST).build();
        
    }
    
    @GET @Path("{id}")
    @Consumes("application/json;charset=utf-8") @Produces("application/json;charset=utf-8")
    public Response getDescription(@PathParam("id") int id) {
        if (id > 0) {
            Descriptions d = dao.getDescription(id);
            return Response.ok(EntityClassConverter.convert(d)).build();
        }
        return Response.status(Response.Status.BAD_REQUEST).build();
    }
    
//    @POST @Path("{id}")
//    @Consumes("application/json;charset=utf-8") @Produces("application/json;charset=utf-8")
//    public Response updateDescription(String wdString, @PathParam("id") int id) {
//        WanderDescription wd = getGson().fromJson(wdString, WanderDescription.class);
//        WanderDescription.validate(wd);
//        wd.setId(id);
//        if (wd.isNewObject()) {
//            Descriptions d = dao.getDescription(wd.getId());
//            dao.updateDescription(d, EntityClassConverter.convert(wd));
//            return Response.ok(getGson().toJson(d.getIdDescriptions())).build();
//        }
//        return Response.status(Response.Status.BAD_REQUEST).build();
//    }
    
}
