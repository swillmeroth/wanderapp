package org.wanderWiki.rest;

import com.google.gson.reflect.TypeToken;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import org.wanderWiki.Helfer.EntityClassConverter;
import org.wanderWiki.Helfer.Logic;
import org.wanderWiki.entities.Places;
import org.wanderWiki.entities.Users;
import org.wanderWiki.rest.exceptions.AuthenticationException;
import org.wanderWiki.rest.exceptions.BadRequestException;
import regionale.whs.wanderapp.wspojos.WanderPOI;
import regionale.whs.wanderapp.wspojos.validation.Validator;

/**
 * REST Web Service
 *
 * @author MartinM
 */
@Path("places")
public class PlacesResource extends GenericResource {


    public PlacesResource() {
        System.out.println("PlacesResource()");
    }
    
    @POST @Path("types")
    @Consumes("application/json;charset=utf-8") @Produces("application/json;charset=utf-8")
    public Response byTypes(String tlString) {
        List<String> typeList = getGson().fromJson(tlString, new TypeToken<List<String>>(){}.getType());
        for (String type : typeList) {
            if (!Validator.isPOIType(type))
                Validator.complain("At least one type in the type list is null or not known.");
        }
        List<Places> pl = dao.getPlacesMatchingTypes(typeList, false);
        List<WanderPOI> wpl = new LinkedList<>();
        for (Places p : pl) {
            WanderPOI wp = EntityClassConverter.convert(p);
            wpl.add(wp);
        }
        return Response.ok(getGson().toJson(wpl)).build();
        
    }
    
    @POST @Path("bylist")
    @Consumes("application/json;charset=utf-8") @Produces("application/json;charset=utf-8")
    public Response byList(String lString) {
        List<Integer> il = getGson().fromJson(lString, new TypeToken<List<Integer>>(){}.getType());
        if (il == null)
            throw new BadRequestException("The list was null.");
        for (Integer i : il) {
            if (i == null)
                throw new BadRequestException("One or more elements of the list were null.");
        }
        if (Logic.hasDupes(il))
            throw new BadRequestException("One or more elements of the list appeared at least twice.");
        List<Places> pl = dao.getPlacesInList(il);
        List<WanderPOI> wpl = new LinkedList<>();
        for (Places p : pl) {
            wpl.add(EntityClassConverter.convert(p));
        }
        return Response.ok(getGson().toJson(wpl)).build();
    }
    
    
    @GET @Path("containing-{name}")
    @Consumes("application/json;charset=utf-8") @Produces("application/json;charset=utf-8")
    public Response byName(@PathParam("name") String name) {
        if (name == null)
            throw new BadRequestException("Requested name was null.");
        List<Places> pl = dao.getPlacesMatchingName(name, false);
        List<WanderPOI> wpl = new LinkedList<>();
        for (Places p : pl) {
            WanderPOI wp = EntityClassConverter.convert(p);
            wpl.add(wp);
        }
        return Response.ok(getGson().toJson(wpl)).build();
    }
    
    @GET @Path("typed-{type}")
    @Consumes("application/json;charset=utf-8") @Produces("application/json;charset=utf-8")
    public Response byType(@PathParam("type") String type) {
        if (!Validator.isPOIType(type))
            throw new BadRequestException("The request type is not known.");
        List<Places> pl = dao.getPlacesMatchingType(type, false);
        List<WanderPOI> wpl = new LinkedList<>();
        for (Places p : pl) {
            WanderPOI wp = EntityClassConverter.convert(p);
            wpl.add(wp);
        }
        return Response.ok(getGson().toJson(wpl)).build();
    }
    
    
    //@GET to get pagecount
    @GET
    @Consumes("application/json;charset=utf-8") @Produces("application/json;charset=utf-8")
    public Response getCount() {
        return Response.ok(getGson().toJson(dao.getPlaceCount())).build();
    }
    
    
    //@GET #x# for WanderPOIs

    @Path("{id}")
    @Consumes("application/json;charset=utf-8") @Produces("application/json;charset=utf-8")
    public PlaceResource place(@PathParam("id") int id) {
        System.out.println("place(" + id + ")");
        return resContext.initResource(new PlaceResource(dao.getPlace(id)));
    }
    
//    @GET @Path("{from}-{to}")
//    @Consumes("application/json;charset=utf-8") @Produces("application/json;charset=utf-8")
//    public List<WanderPOI> getPlaces(@PathParam("from") int fromNum, @PathParam("to") int toNum) {
//        
//    }
    
    @GET @Path("{id}")
    @Consumes("application/json;charset=utf-8") @Produces("application/json;charset=utf-8")
    public Response getPlace(@PathParam("id") int id) {
        System.out.println("getPlace(" + id + ")");
//        if (id == 42) {
//            WanderPOI wpoi = new WanderPOI();
//            wpoi.setId(42);
//            wpoi.setName("Truth");
//            wpoi.setCity("Truth City");
//            wpoi.setCommentCount(21);
//            wpoi.setDateAdded(new Date());
//            wpoi.setIdChanging(84);
//            wpoi.setIsChange(true);
//            wpoi.setIsProposal(false);
//            wpoi.setKoordinaten(new GeoLocation(42, 42, 42));
//            wpoi.setMultimediaCount(1337);
//            wpoi.setOpenTimes("Always");
//            wpoi.setPoiType("Wisdom");
//            wpoi.setPostalCode("09001");
//            wpoi.setRating(5);
//            wpoi.setSize(0x7FFFFFFF);
//            wpoi.setStreet("Laniakea");
//            wpoi.setTodosCount(1);
//            return Response.ok(getGson().toJson(wpoi)).build();
////            return Response.ok(wpoi).build();
//        }
        Places p = dao.getPlace(id);
        WanderPOI poi = EntityClassConverter.convert(p);
        return Response.ok(getGson().toJson(poi)).build();
    }

    /**
     * PUT method for updating or creating an instance of PlacesResource
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     */
    @POST
    @Consumes("application/json;charset=utf-8") @Produces("application/json;charset=utf-8")
    public Response addPlace(String poiString) {
        Users u = UsersResource.requireAuth(headers);
        WanderPOI poi = getGson().fromJson(poiString, WanderPOI.class);
        WanderPOI.validate(poi);
        if (!poi.getIsProposal())
            throw new AuthenticationException("Wander Web Service only supports adding proposals.");
        if (poi.getIsChange()) {
            Places p = dao.getPlace(poi.getIdChanging());
            if (p.getIsProposal())
                throw new BadRequestException("Change proposal on proposal is not a valid request.");
        }
        poi.setDateAdded(new Date());
        Places p = EntityClassConverter.convert(poi);
        dao.addPlace(p);
        return Response.ok(getGson().toJson(p.getIdPlaces())).build();
    }
    
//    @POST @Path("{id}")
//    @Consumes("application/json;charset=utf-8") @Produces("application/json;charset=utf-8")
//    public Response updatePlace(String poiString, @PathParam("id") int id) {
//        Users u = UsersResource.requireAuth(headers);
//        WanderPOI poi = getGson().fromJson(poiString, WanderPOI.class);
//        WanderPOI.validate(poi);
//        if (!poi.getIsProposal())
//            throw new AuthenticationException("Wander Web Service only supports adding proposals.");
//        if (!poi.getIsChange())
//            throw new BadRequestException("POST to /places/{id} is for changes to places only. Use /places/ for new places.");
//        poi.setId(id);
//        Places p = dao.getPlace(poi.getId());
//        if (p != null) {
//            dao.updatePlace(p, EntityClassConverter.convert(poi));
//            return Response.ok(getGson().toJson(p.getIdPlaces())).build();
//        } else {
//            return Response.status(Response.Status.NOT_FOUND).build();
//        }
//    }
    
//    @DELETE @Path("{id}")
//    @Consumes("application/json;charset=utf-8") @Produces("application/json;charset=utf-8")
//    public Response deletePlace(@PathParam("id") int id) {
//        System.out.println("deletePlace(" + id + ")");
//        if (id > 0) {
//            Places p = dao.getPlace(id);
//            if (p == null) {
//                return Response.status(Response.Status.NOT_FOUND).build();
//            } else {
//                dao.deletePlace(p);
//                return Response.ok().build();
//            }
//            
//        } else {
//            return Response.status(Response.Status.BAD_REQUEST).build();
//        }
//    }
}
