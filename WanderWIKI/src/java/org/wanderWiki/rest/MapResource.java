package org.wanderWiki.rest;

import java.io.IOException;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import org.wanderWiki.Helfer.EntityClassConverter;
import org.wanderWiki.entities.Places;
import org.wanderWiki.entities.Routes;
import org.wanderWiki.entities.Users;
import org.wanderWiki.ors.OpenRouteService;
import org.wanderWiki.ors.WayPoint;
import org.wanderWiki.rest.exceptions.BadRequestException;
import org.xml.sax.SAXException;
import regionale.whs.wanderapp.wspojos.GeoLocation;
import regionale.whs.wanderapp.wspojos.Kartenausschnitt;
import regionale.whs.wanderapp.wspojos.RouteRequest;
import regionale.whs.wanderapp.wspojos.WanderPOI;
import regionale.whs.wanderapp.wspojos.WanderRoute;

/**
 *
 * @author MartinM
 */
@Path("map")
public class MapResource extends GenericResource {
    
    private static final transient int MAP_REQUEST_TIMEOUT_SECONDS = 180;
    private static final transient String OSMOSIS_PATH = "path\\to\\osmosis-0.40.1\\bin\\osmosis";
    private static final transient String OSM_PBF_INPUT_FILE_PATH = "web/resources/map/muenster-regbez-latest.osm.pbf";
    private static final transient String MAP_OUTPUT_FOLDER_PREFIX = "tempmap-";
    private static final transient String MAP_OUTPUT_FILE_NAME = "section.map";
    
    @GET @Path("latitude-{latmin}-to-{latmax}-and-longitude-{longmin}-to-{longmax}")
    @Consumes("application/json;charset=utf-8") @Produces("application/json;charset=utf-8")
    public Response getWanderMapSection(
            @PathParam("latmin") double latMin,
            @PathParam("latmax") double latMax,
            @PathParam("longmin") double longMin,
            @PathParam("longmax") double longMax) {
        List<Routes> rl = dao.getRoutesInSection(latMin, latMax, longMin, longMax, false);
        List<Places> pl = dao.getPlacesInSection(latMin, latMax, longMin, longMax, false);
        Kartenausschnitt ka = new Kartenausschnitt();
        LinkedList<WanderRoute> wrl = new LinkedList<>();
        LinkedList<WanderPOI> wpl = new LinkedList<>();
        for (Routes r : rl) {
            WanderRoute wr = EntityClassConverter.convert(r);
            wrl.add(wr);
        }
        for (Places p : pl) {
            WanderPOI wp = EntityClassConverter.convert(p);
            wpl.add(wp);
        }
        ka.setRoutenListe(wrl);
        ka.setPoiListe(wpl);
        return Response.ok(getGson().toJson(ka)).build();
    }
    
    
    @GET @Path("osm/latitude-{latmin}-to-{latmax}-and-longitude-{longmin}-to-{longmax}")
    @Consumes("application/json;charset=utf-8") @Produces("application/json;charset=utf-8")
    public Response getMapSection(
            @PathParam("latmin") double latMin,
            @PathParam("latmax") double latMax,
            @PathParam("longmin") double longMin,
            @PathParam("longmax") double longMax) throws IOException, InterruptedException {
        double latSpan = latMax - latMin;
        double longSpan = longMax - longMin;
        if ((latSpan > 1.0) || (longSpan > 1.0))
            throw new BadRequestException("Requesting too large of a map section.");
//        return Response.ok(getMapSectionFile(latMin, latMax, longMin, longMax)).build();
        return Response.status(Response.Status.NOT_IMPLEMENTED).build();
    }
    
    @POST @Path("routegenerator")
    @Consumes("application/json;charset=utf-8") @Produces("application/json;charset=utf-8")
    public Response buildRoute(String rrString) throws SAXException {
        RouteRequest rr = getGson().fromJson(rrString, RouteRequest.class);
        RouteRequest.validate(rr);
        Users u = UsersResource.auth(headers);
        
        // Prepare ORS
        OpenRouteService ors = new OpenRouteService();
        ors.setRoutePreferenz("Pedestrian");
        ors.setSprache((u != null) ? u.getLanguage() : "de");

        // Get eligible Places
        GeoLocation destination = rr.isLoopRequest() ? rr.getStart() : rr.getEnd();
        double walkingSpeed = UsersResource.getUserWalkingSpeed(u);
        double targetDistance = rr.getDuration() * 60 * walkingSpeed;
//        double rawLength = GeoLocation.getDistance(rr.getStart(), destination);
        double degreeSpan = GeoLocation.getDegrees(targetDistance);
        if (rr.isLoopRequest())
            degreeSpan /= 2;
        GeoLocation lb = GeoLocation.between(rr.getStart(), destination);
        List<Places> pl = dao.getPlacesInSection(
                lb.latitude - degreeSpan, lb.latitude + degreeSpan,
                lb.longitude - degreeSpan, lb.longitude + degreeSpan,
                false);
        
        // Prepare search
        List<GeoLocation> gll = new LinkedList<>();
        List<Integer> il = new LinkedList<>();
        double uncertaintyFactor = 1.2;
        double guessedLength = 0;
        pl.sort(new Comparator<Places>() {
            @Override
            public int compare(Places o1, Places o2) {
                GeoLocation glo1 = new GeoLocation(o1.getLatitude(), o1.getLongitude());
                GeoLocation glo2 = new GeoLocation(o2.getLatitude(), o2.getLongitude());
                double dist1 = GeoLocation.getDistance(glo1, rr.getStart());
                double dist2 = GeoLocation.getDistance(glo2, rr.getStart());
                return (dist1 < dist2) ? -1 : (dist1 == dist2) ? 0 : 1;
            }
        });

        // DEBUG
        System.out.println("PLACES LIST, IN ORDER:");
        for (Places p : pl) {
            GeoLocation glp = new GeoLocation(p.getLatitude(), p.getLongitude());
            double dist = GeoLocation.getDistance(rr.getStart(), glp);
            System.out.println("Place #" + p.getIdPlaces() + " distance to start: " + dist + " meters.");
        }

        // Search and build route
        WanderRoute wr = new WanderRoute();
        gll.add(rr.getStart());
        GeoLocation glPrev = rr.getStart();
        double directDistance = GeoLocation.getDistance(glPrev, destination) * uncertaintyFactor;
        while ((guessedLength + directDistance) < targetDistance) {
            if (!pl.isEmpty()) {
                Places p = pl.remove(0);
                WanderPOI poi = EntityClassConverter.convert(p);
                double stepDistance = GeoLocation.getDistance(glPrev, poi.getKoordinaten());
                gll.add(poi.getKoordinaten());
                guessedLength += stepDistance * uncertaintyFactor;
                directDistance = GeoLocation.getDistance(poi.getKoordinaten(), destination) * uncertaintyFactor;
                glPrev = poi.getKoordinaten();
                il.add(p.getIdPlaces());
            } else {
                break;
            }
        }
        gll.add(destination);

        // DEBUG
        System.out.println("WAYPOINTS:");
        List<WayPoint> wpl = OpenRouteService.convertGeoLocations(gll);
        for (WayPoint wp : wpl) {
            System.out.println("WP: " + wp.toStringCompletely());
        }
        
        // Request route
        ors.runRequest(wpl);

        // Build route object
        wr.setRoutePoints(OpenRouteService.convertWayPoints(ors.getWegPunkte()));
        wr.setDistance((int) Math.round(1000 * ors.getTotalDistance()));
        wr.setDuration(ors.getTotalTime());
        wr.setRouteType(rr.getRouteType());
        wr.setRoutePlacesIds(il);
        String goalName = (pl.size() > 1) ? (pl.get(pl.size() - 1).getName()) : "Keine";
        wr.setName(goalName + " Route");
        
        System.out.println("Target distance was " + targetDistance + " and received route distance is " + wr.getDistance() + ".");

        return Response.ok(getGson().toJson(wr)).build();
    }

//    private String getMapSectionFile(
//            double latMin,
//            double latMax,
//            double longMin,
//            double longMax) throws IOException, InterruptedException {
//        StringBuilder sb = new StringBuilder();
//        Formatter f = new Formatter(sb, Locale.US);
//        f.format("%.9f,%.9f,%.9f,%.9f", longMin, latMin, longMax, latMax);
//        String stringBBox = f.toString();
//        java.nio.file.Path osmosisPath = Paths.get(OSMOSIS_PATH);
//        java.nio.file.Path inFilePath = Paths.get(OSM_PBF_INPUT_FILE_PATH);
//        java.nio.file.Path outFolderPathRaw = Files.createTempDirectory(MAP_OUTPUT_FOLDER_PREFIX);
//        try {
//            java.nio.file.Path outFolderPath = outFolderPathRaw.toAbsolutePath();
//            java.nio.file.Path outFilePath = outFolderPath.resolve(MAP_OUTPUT_FILE_NAME);
//            String commandLineString = osmosisPath.toString() + " "
//                    + "--read-xml file=" + inFilePath.toString() + " "
//                    + "--mapfile-writer file=" + outFilePath.toString() + " "
//                    + "bbox=" + stringBBox;
//            CommandLine commandLine = CommandLine.parse(commandLineString);
//            DefaultExecutor executor = new DefaultExecutor();
//            ExecuteWatchdog dog = new ExecuteWatchdog(1000 * MAP_REQUEST_TIMEOUT_SECONDS);
//            executor.setWatchdog(dog);
//            executor.setExitValue(0);
//            try {
//                executor.execute(commandLine);
//                if (dog.killedProcess())
//                    throw new TimeoutException("The request failed to complete within "
//                            + MAP_REQUEST_TIMEOUT_SECONDS + " seconds."); 
//            } catch (ExecuteException ex) {
//                throw new ExternalServiceFailedException("The Osmosis map conversion program "
//                        + "failed with non-zero exit code " + ex.getExitValue() + ".");
//            }
//            String mapContents = new String(Files.readAllBytes(outFilePath), Charset.forName("UTF-8"));
//            return mapContents;
//        } finally {
//            FileUtils.deleteDirectory(outFolderPathRaw.toFile());
//        }
//    }
}
