package org.wanderWiki.rest;

import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import org.wanderWiki.Helfer.EntityClassConverter;
import org.wanderWiki.entities.Comments;
import org.wanderWiki.entities.Users;
import org.wanderWiki.rest.exceptions.ResourceNotFoundException;
import regionale.whs.wanderapp.wspojos.WanderKommentar;

/**
 *
 * @author MartinM
 */
public class UserResource extends GenericResource {
    
    Users u;
    
    public UserResource(Users u) {
        this.u = u;
        if (u == null) {
            throw new ResourceNotFoundException("No such user was found.");
        }
    }
    
    @GET @Path("comments/{num}")
    @Consumes("application/json;charset=utf-8") @Produces("application/json;charset=utf-8")
    public Response getComment(@PathParam("num") int num) {
        return Response.ok(getGson().toJson(EntityClassConverter.convert(dao.getUserComment(u, num)))).build();
    }
    
    @GET @Path("comments/{from}-{to}")
    @Consumes("application/json;charset=utf-8") @Produces("application/json;charset=utf-8")
    public Response getComments(@PathParam("from") int fromNum, @PathParam("to") int toNum) {
//        int firstRow = (pageNum - 1) * perPageCount + 1;
        if ((fromNum < 1) || (toNum < fromNum))
            return Response.status(Response.Status.BAD_REQUEST).build();
        List<Comments> cl = dao.getUserComments(u, fromNum, toNum - fromNum + 1);
        List<WanderKommentar> wkl = new ArrayList<>(cl.size());
        for (Comments c : cl) {
            wkl.add(EntityClassConverter.convert(c));
        }
//        GenericEntity<List<WanderKommentar>> gewkl = new GenericEntity<List<WanderKommentar>>(wkl) {};
        return Response.ok(getGson().toJson(wkl)).build();
    }
}
