package org.wanderWiki.rest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import javax.ws.rs.container.ResourceContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author MartinM
 */
public class GenericResource extends GenericDBAccessor {

    @Context
    public UriInfo uriInfo;
    
    @Context
    public HttpHeaders headers;
    
    @Context
    public ResourceContext resContext;
    
    protected Gson getGson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.excludeFieldsWithoutExposeAnnotation().create();
        return gson;
    }
}
