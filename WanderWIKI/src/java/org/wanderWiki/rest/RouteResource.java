package org.wanderWiki.rest;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import org.wanderWiki.Helfer.EntityClassConverter;
import org.wanderWiki.entities.Comments;
import org.wanderWiki.entities.Multimedia;
import org.wanderWiki.entities.Places;
import org.wanderWiki.entities.Routes;
import org.wanderWiki.entities.Users;
import static org.wanderWiki.rest.GenericDBAccessor.dao;
import org.wanderWiki.rest.exceptions.BadRequestException;
import org.wanderWiki.rest.exceptions.ResourceNotFoundException;
import regionale.whs.wanderapp.wspojos.WanderKommentar;
import regionale.whs.wanderapp.wspojos.WanderMedia;
import regionale.whs.wanderapp.wspojos.WanderPOI;

/**
 *
 * @author MartinM
 */
public class RouteResource extends GenericResource {
    
    private Routes r;
    
    public RouteResource(Routes r) {
        this.r = r;
        if (r == null) {
            throw new ResourceNotFoundException("No such route was found.");
        }
    }
    
//    @GET
//    @Consumes("application/json;charset=utf-8") @Produces("application/json;charset=utf-8")
//    public Response getPOI() {
//        System.out.println("getPOI()");
//        return Response.ok(EntityClassConverter.convert(this.p)).build();
//    }
    
    
    @POST @Path("media")
    @Consumes("application/json;charset=utf-8") @Produces("application/json;charset=utf-8")
    public Response addMedia(String mmString) {
        Users u = UsersResource.requireAuth(headers);
        WanderMedia wm = getGson().fromJson(mmString, WanderMedia.class);
        WanderMedia.validate(wm);
        Multimedia m = EntityClassConverter.convert(wm);
        if (r.getIsProposed())
            throw new BadRequestException("Cannot add media to a proposal.");
        m.setIdRoute(r);
        m.setIdPlace(null);
        m.setTime(new Date());
        m.setIdUser(u);
        dao.addMultimedia(m);
        return Response.ok(getGson().toJson(m.getIdMultimedia())).build();
    }
    
    @GET @Path("media/{num}")
    @Consumes("application/json;charset=utf-8") @Produces("application/json;charset=utf-8")
    public Response getMedia(@PathParam("num") int num) {
        if (num > 0) {
            Multimedia m = dao.getRouteMultimedia(r, num);
            return Response.ok(getGson().toJson(EntityClassConverter.convert(m))).build();
        } else {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }
    
    @GET @Path("media/{from}-{to}")
    @Consumes("application/json;charset=utf-8") @Produces("application/json;charset=utf-8")
    public Response getMedia(@PathParam("from") int fromNum, @PathParam("to") int toNum) {
        
        if ((fromNum < 1) || (toNum < fromNum))
            return Response.status(Response.Status.BAD_REQUEST).build();
        List<Multimedia> ml = dao.getRouteMultimedia(r, fromNum, toNum - fromNum + 1);
        List<WanderMedia> wml = new ArrayList<>(ml.size());
        for (Multimedia m : ml) {
            wml.add(EntityClassConverter.convert(m));
        }
        return Response.ok(getGson().toJson(wml)).build();
    }
    
    @GET @Path("comments/{num}")
    @Consumes("application/json;charset=utf-8") @Produces("application/json;charset=utf-8")
    public Response getComment(@PathParam("num") int num) {
        Comments c = dao.getRouteComment(r, num);
        return Response.ok(getGson().toJson(EntityClassConverter.convert(c))).build();
    }
    
    @GET @Path("comments/{from}-{to}")
    @Consumes("application/json;charset=utf-8") @Produces("application/json;charset=utf-8")
    public Response getComments(@PathParam("from") int fromNum, @PathParam("to") int toNum) {
        System.out.println("getComments()");
        if ((fromNum < 1) || (toNum < fromNum))
            return Response.status(Response.Status.BAD_REQUEST).build();
        //int firstRow = (pageNum - 1) * perPageCount + 1;
        List<Comments> cl = dao.getRouteComments(r, fromNum, toNum - fromNum + 1);
        List<WanderKommentar> wkl = new ArrayList<>(cl.size());
        for (Comments c : cl) {
            wkl.add(EntityClassConverter.convert(c));
        }
//        GenericEntity<List<WanderKommentar>> gewkl = new GenericEntity<List<WanderKommentar>>(wkl) {};
        return Response.ok(getGson().toJson(wkl)).build();
    }
    
    @POST @Path("comments")
    @Consumes("application/json;charset=utf-8") @Produces("application/json;charset=utf-8")
    public Response addComment(String wkString) {
        Users u = UsersResource.requireAuth(headers);
        WanderKommentar wk = getGson().fromJson(wkString, WanderKommentar.class);
        System.out.println("addComment: " + wk.toString());
        WanderKommentar.validate(wk);
        Comments c = EntityClassConverter.convert(wk);
        if (r.getIsProposed())
            throw new BadRequestException("Cannot add comment to a proposal.");
        c.setIdPlace(null); // Do not link to a place.
        c.setTime(new Date());
        c.setIdRoute(r);
        c.setRating(wk.getRating());
        c.setIdUser(u);
        dao.addComment(c);
        return Response.ok(getGson().toJson(c.getIdComments())).build();
    }
    
    @GET @Path("places")
    @Consumes("application/json;charset=utf-8") @Produces("application/json;charset=utf-8")
    public Response getPlaces() {
        List<Places> pl = dao.getRoutePlaces(r);
        List<WanderPOI> wpl = new LinkedList<>();
        for (Places p : pl) {
            wpl.add(EntityClassConverter.convert(p));
        }
        return Response.ok(getGson().toJson(wpl)).build();
    }
    
    
//    @POST @Path("descriptions")
//    @Consumes("application/json;charset=utf-8") @Produces("application/json;charset=utf-8")
//    public Response addDescription(String wdString) {
//        WanderDescription wd = getGson().fromJson(wdString, WanderDescription.class);
//        WanderDescription.validate(wd);
//        if (wd.isNewObject()) {
//            Descriptions d = EntityClassConverter.convert(wd);
//            d.setIdRoute(r);
//            d.setIdPlace(null);
//            dao.addDescription(d);
//            return Response.ok(getGson().toJson(d.getIdDescriptions())).build();
//        }
//        return Response.status(Response.Status.BAD_REQUEST).build();
//    }
    
    @GET @Path("descriptions/{interest}-interest-in-{language}-language")
    @Consumes("application/json;charset=utf-8") @Produces("application/json;charset=utf-8")
    public Response getDescription(@PathParam("interest") String interest, @PathParam("language") String language) {
        return Response.ok(getGson().toJson(EntityClassConverter.convert(dao.getDescription(r, interest, language)))).build();
    }
    
}
