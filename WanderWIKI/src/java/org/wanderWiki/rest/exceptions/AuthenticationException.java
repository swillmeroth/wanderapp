package org.wanderWiki.rest.exceptions;

/**
 *
 * @author MartinM
 */
public class AuthenticationException extends RuntimeException {
    public AuthenticationException(String message) {
        super(message);
    }
}
