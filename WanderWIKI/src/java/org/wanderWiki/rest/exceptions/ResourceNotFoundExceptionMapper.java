package org.wanderWiki.rest.exceptions;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 *
 * @author MartinM
 */
@Provider
public class ResourceNotFoundExceptionMapper implements ExceptionMapper<ResourceNotFoundException> {

    @Override
    public Response toResponse(ResourceNotFoundException exception) {
        System.out.println("ResourceNotFoundExceptionMapper.toResponse()");
        return Response.status(Response.Status.NOT_FOUND).header("xxx-reason", exception.getMessage()).build();
    }
    
}
