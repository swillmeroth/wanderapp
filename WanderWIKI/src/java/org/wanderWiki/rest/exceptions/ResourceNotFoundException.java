package org.wanderWiki.rest.exceptions;

/**
 *
 * @author MartinM
 */
public class ResourceNotFoundException extends RuntimeException {
    public ResourceNotFoundException(String message) {
        super(message);
    }
}
