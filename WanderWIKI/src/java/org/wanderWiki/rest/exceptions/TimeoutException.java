package org.wanderWiki.rest.exceptions;

/**
 *
 * @author MartinM
 */
public class TimeoutException extends RuntimeException {
    public TimeoutException(String message) {
        super(message);
    }
}
