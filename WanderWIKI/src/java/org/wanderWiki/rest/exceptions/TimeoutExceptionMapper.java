package org.wanderWiki.rest.exceptions;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

/**
 *
 * @author MartinM
 */
public class TimeoutExceptionMapper implements ExceptionMapper<TimeoutException> {

    @Override
    public Response toResponse(TimeoutException exception) {
        return Response.status(Response.Status.REQUEST_TIMEOUT).header("xxx-reason", exception.getMessage()).build();
    }
    
}
