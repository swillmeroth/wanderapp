/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.wanderWiki.rest.exceptions;

/**
 *
 * @author Zyl
 */
public class ExternalServiceFailedException extends RuntimeException {
    public ExternalServiceFailedException(String message) {
        super(message);
    }
}
