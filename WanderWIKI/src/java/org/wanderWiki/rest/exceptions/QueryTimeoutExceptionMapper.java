package org.wanderWiki.rest.exceptions;

import javax.persistence.QueryTimeoutException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 *
 * @author MartinM
 */
@Provider
public class QueryTimeoutExceptionMapper implements ExceptionMapper<QueryTimeoutException> {

    @Override
    public Response toResponse(QueryTimeoutException exception) {
        return Response.status(Response.Status.REQUEST_TIMEOUT).build();
    }
    
}
