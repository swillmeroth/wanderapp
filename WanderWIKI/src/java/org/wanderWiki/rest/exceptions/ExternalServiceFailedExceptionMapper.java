package org.wanderWiki.rest.exceptions;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

/**
 *
 * @author MartinM
 */
public class ExternalServiceFailedExceptionMapper implements ExceptionMapper<ExternalServiceFailedException> {

    @Override
    public Response toResponse(ExternalServiceFailedException exception) {
        return Response.status(Response.Status.SERVICE_UNAVAILABLE).header("xxx-reason", exception.getMessage()).build();
    }
    
}
