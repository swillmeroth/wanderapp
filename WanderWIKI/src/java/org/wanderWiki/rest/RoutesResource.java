package org.wanderWiki.rest;

import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import org.wanderWiki.Helfer.EntityClassConverter;
import org.wanderWiki.entities.Routepoints;
import org.wanderWiki.entities.Routes;
import org.wanderWiki.entities.Users;
import static org.wanderWiki.rest.GenericDBAccessor.dao;
import org.wanderWiki.rest.exceptions.AuthenticationException;
import org.wanderWiki.rest.exceptions.BadRequestException;
import regionale.whs.wanderapp.wspojos.GeoLocation;
import regionale.whs.wanderapp.wspojos.WanderRoute;

/**
 *
 * @author MartinM
 */
@Path("routes")
public class RoutesResource extends GenericResource {
    
    @GET @Path("containing-{name}")
    @Consumes("application/json;charset=utf-8") @Produces("application/json;charset=utf-8")
    public Response byName(@PathParam("name") String name) {
        if (name == null)
            throw new BadRequestException("Requested name was null.");
        List<Routes> rl = dao.getRoutesMatchingName(name, false);
        List<WanderRoute> wrl = new LinkedList<>();
        for (Routes r : rl) {
            WanderRoute wr = EntityClassConverter.convert(r);
            wrl.add(wr);
        }
        return Response.ok(getGson().toJson(wrl)).build();
    }
    
    @GET @Path("{id}")
    @Consumes("application/json;charset=utf-8") @Produces("application/json;charset=utf-8")
    public Response getRoute(@PathParam("id") int id) {
//        if (id == 42) {
//            WanderRoute wroute = new WanderRoute();
//            wroute.setId(42);
//            wroute.setCommentCount(84);
//            wroute.setDifficulty(9001);
//            wroute.setDistance(2134);
//            wroute.setDuration(555);
//            wroute.setEndId(43);
//            wroute.setStartId(44);
//            wroute.setEquipment("Good Luck");
//            wroute.setIdChanging(424);
//            wroute.setIsChange(true);
//            wroute.setIsProposal(false);
//            wroute.setMultimediaCount(666);
//            wroute.setName("Path of truth");
//            wroute.setRating(5);
//            wroute.setRouteType("öäüß");
//            wroute.setStartId(23);
//            wroute.setTodosCount(6753);
//            wroute.setRoutePlacesIds(Arrays.asList(123, 124, 125, null, 126));
//            wroute.setRoutePoints(Arrays.asList(new GeoLocation(1, 2, 3), new GeoLocation(4, 5, 6), new GeoLocation(7, 8, 9)));
//            return Response.ok(getGson().toJson(wroute)).build();
////            return Response.ok(wroute).build();
//        }
        if (id <= 0)
            return Response.status(Response.Status.BAD_REQUEST).build();
        Routes r = dao.getRoute(id);
        if (r == null)
            return Response.status(Response.Status.NOT_FOUND).build();
        WanderRoute wr = EntityClassConverter.convert(r);
        List<Routepoints> rpl = dao.getRoutePoints(r);
        List<GeoLocation> gll = new LinkedList<>();
        ListIterator<Routepoints> rpli = rpl.listIterator();
        while (rpli.hasNext()) {
            Routepoints rp = rpli.next();
            GeoLocation gl = new GeoLocation(rp.getLatitude(), rp.getLongitude());
            gll.add(gl);
        }
        wr.setRoutePoints(gll);
        return Response.ok(getGson().toJson(wr)).build();
    }
    
    @Path("{id}")
    @Consumes("application/json;charset=utf-8") @Produces("application/json;charset=utf-8")
    public RouteResource route(@PathParam("id") int id) {
        return resContext.initResource(new RouteResource(dao.getRoute(id)));
    }
    
    @GET
    @Consumes("application/json;charset=utf-8") @Produces("application/json;charset=utf-8")
    public Response getCount() {
        return Response.ok(getGson().toJson(dao.getRouteCount())).build();
    }
    
    @POST
    @Consumes("application/json;charset=utf-8") @Produces("application/json;charset=utf-8")
    public Response addRoute(String wrString) {
        Users u = UsersResource.requireAuth(headers);
        WanderRoute wr = getGson().fromJson(wrString, WanderRoute.class);
        WanderRoute.validate(wr);
        if (!wr.getIsProposal())
            throw new AuthenticationException("Wander Web Service only supports adding proposals.");
        if (wr.getIsChange()) {
            Routes r = dao.getRoute(wr.getIdChanging());
            if (r.getIsProposed())
                throw new BadRequestException("Change proposal on proposal is not a valid request.");
        }
        //wr.setDateAdded(new Date()); // Routes have no date added field?
        Routes r = EntityClassConverter.convert(wr);
        dao.addRoute(r);
        return Response.ok(getGson().toJson(r.getIdRoutes())).build();
    }
    
//    @POST @Path("{id}")
//    @Consumes("application/json;charset=utf-8") @Produces("application/json;charset=utf-8")
//    public Response updateRoute(String wrString, @PathParam("id") int id) {
//        WanderRoute wr = getGson().fromJson(wrString, WanderRoute.class);
////        System.out.println("updatePlace(" + poi.getId() + "#" + poi.getName() + ", " + id + ")");
//        WanderRoute.validate(wr);
//        wr.setId(id);
//        Routes r = dao.getRoute(wr.getId());
//        if (r != null) {
//            dao.updateRoute(r, EntityClassConverter.convert(wr));
//            return Response.ok(getGson().toJson(r.getIdRoutes())).build();
//        } else {
//            return Response.status(Response.Status.NOT_FOUND).build();
//        }
//    }
    
//    @DELETE @Path("{id}")
//    @Consumes("application/json;charset=utf-8") @Produces("application/json;charset=utf-8")
//    public Response deleteRoute(@PathParam("id") int id) {
//        System.out.println("deleteRoute(" + id + ")");
//        if (id > 0) {
//            Routes r = dao.getRoute(id);
//            if (r == null) {
//                return Response.status(Response.Status.NOT_FOUND).build();
//            } else {
//                dao.deleteRoute(r);
//                return Response.ok().build();
//            }
//        } else {
//            return Response.status(Response.Status.BAD_REQUEST).build();
//        }
//    }
    
}
