/*******************************************************
* WanderApp
* erstellt am 21.11.2014
* Order Passwort Bean
* Hat ein Nutzer sein Passwort vergesssen,
* so kann er sich sein Passwort via MAil schicken lassen.
* Verantwortlich: Andre Hantke
* Version 0.5
********************************************************/
package org.wanderWiki.beans;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;

import org.wanderWiki.DAO.UsersDAOJPA;
/**
 *
 * @author Andre
 */
@ManagedBean
@SessionScoped
public class orderPasswordBean {
    
    //Email
    private String email;
    //Passwort
    private String password;
    
    
    public orderPasswordBean(){
    }

    //setter + getter
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    //Methode zum Holen des Passwortes des Users
    public void getEmailfromUser() throws Exception{
        FacesContext ctx = FacesContext.getCurrentInstance();
        RequestContext context = RequestContext.getCurrentInstance();
        UsersDAOJPA udj = new UsersDAOJPA();
        //in der Method changePassword wird dann auch die Mail verschickt,
        //sofern der Nutzer vorhanden ist
        Boolean ok = udj.changePasword(email);
        //Ausgabe auf der Seite
        if(ok){
            //Dialog schließen
            context.execute("PF('passwordNew').hide()");     
            //Ausgabe einer Nachricht
            ctx.addMessage("Nutzer gefunden.",new FacesMessage(FacesMessage.SEVERITY_INFO, "Nutzer gefunden!", "Email ist unterwegs mit ihren neuen Nutzerdaten.")); 
        }else{
            //Ausgabe einer Nachricht
            ctx.addMessage("Nutzer nicht gefunden.",new FacesMessage(FacesMessage.SEVERITY_ERROR, "Nutzer nicht gefunden!", "Bitte veruschen sie es erneut.")); 
        }
    }
}
