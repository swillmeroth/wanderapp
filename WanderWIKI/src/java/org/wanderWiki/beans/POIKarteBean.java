/*******************************************************
* WanderApp
* erstellt am 06.04.2015
* Poi Karte um die Pois auf einer Karte darzustellen
* Verantwortlich: Claas Schlonsok
* Version 0.1
 */
package org.wanderWiki.beans;

import com.googlecode.gmaps4jsf.component.icon.Icon;
import com.googlecode.gmaps4jsf.component.map.Map;
import com.googlecode.gmaps4jsf.component.marker.Marker;
import com.googlecode.gmaps4jsf.component.window.HTMLInformationWindow;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import org.primefaces.context.RequestContext;
import org.wanderWiki.Helfer.Description_comperator;
import org.wanderWiki.entities.Comments;
import org.wanderWiki.entities.Descriptions;
import org.wanderWiki.entities.Places;
import org.wanderWiki.entities.Users;
import org.wanderWiki.jpa.CommentsFacade;
import org.wanderWiki.jpa.PlacesFacade;

/**
 * Poi Karte um die Pois auf einer Karte darzustellen
 *
 * @author Claas
 */
@ManagedBean(name = "poiKarteBean")
@SessionScoped
public class POIKarteBean {

    private Map map;

//    private double latitude;
//    private double longitude;
    //radius in meter
//    private int radius;
    private Places selected;
    private List<Descriptions> selected_des;
    private int nr;
    private PlacesFacade place_fac;
    private Comments neuer_kommentar;

    //Pfad zu den Icons. BILD_TYPE_NAME wird ersetzt
    private final String ICON = "/resources/icons/BILD_TYPE_NAME.png";

    /**
     * Creates a new instance of POIKarteBean
     */
    public POIKarteBean() {

        place_fac = new PlacesFacade();
        System.out.println("lade karten pois");
        selected = new Places();
        neuer_kommentar = new Comments();

    }

    /**
     * Läd alle Places die keine vorschläge sind aus der Datenbank. Diese Places
     * werden dann als Marker mit den entsprechenden HTMLInfomationen der Karte
     * hinzugefügt. Die fertige Karte wird in map gespeichert.
     */
    public void loadCard() {

        HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        internationalButton interbutton = (internationalButton) req.getSession().getAttribute("internationalButton");

        Locale loc = interbutton.getLocale();
        System.out.println(loc);
        ResourceBundle rb = ResourceBundle.getBundle("org.wanderWiki.language/messages", loc);

//            Locale locale = Locale.getDefault();
//            rb = ResourceBundle.getBundle("org.wanderWiki.language/messages", locale);
//            System.out.println(rb.getString("karte_poi_anzeigen"));
//            props.load(this.getClass().getResourceAsStream("/org/wanderWiki/language/messages.properties"));
//        Resources.getResource("language/messages.properties");
        //hinzugefügt, um Komponentenim Facelet direkt zu updaten über
        //die ID
        RequestContext context = RequestContext.getCurrentInstance();
        //List<Places> results = place_fac.findAllNOTProposal();
        List<Places> results = new ArrayList();
        results.addAll(place_fac.findAllNOTProposal());

        this.map = new Map();
        map.setHeight("500");
        map.setWidth("800");

        String commandbutton = "";

        Marker marker;
        String request = FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath();
        for (Places poi : results) {
            System.out.println("aktueller_poi:" + poi.toString());

            marker = new Marker();
            marker.setDraggable("false");
            marker.setLatitude("" + poi.getLatitude());
            marker.setLongitude("" + poi.getLongitude());

            marker.setShowInformationEvent("click");

            //Link für die auswahl eines Places
            commandbutton = "<a href=poi_anzeigen.xhtml?poiId=" + poi.getIdPlaces() + "> " + rb.getString("karte_poi_anzeigen") + "</a>";

            HTMLInformationWindow info = new HTMLInformationWindow();
            info.setHtmlText("<b>" + poi.toString() + "</b><br>" + commandbutton);
            marker.getChildren().add(info);

            //Dynamische erstellung von Icons anhand des Types des POI
            String url_zum_bild = request + this.ICON.replace("BILD_TYPE_NAME", poi.getType());

            Icon icon = new Icon();
            icon.setImageURL(url_zum_bild);
            marker.getChildren().add(icon);

            map.getChildren().add(marker);
        }
        context.update("karten:gmap_karte");

    }

    /**
     * Läd einen bestimmten POI in selected anhand der ID die in nr steht.
     * Beschreibungen des selektierten POI werden in selected_des gesetzt.
     */
    public void lade_poi() {

        selected = place_fac.find(nr);
        System.out.println(selected);
        if (selected == null) {
            //Falls die ID ungültig ist wird einfach ein neuer Place erzeugt, der Proposal ist
            //dadurch wird nur ein leeres Feld angezeigt.
            System.out.println("[lade_poi] selected ist null!");
            selected = new Places();
            selected.setIsProposal(true);
        }

        //speichert die Beschreibungen in selected_des
        selected_des = (selected.getDescriptionsList());

        HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        loginBean loginbean = (loginBean) req.getSession().getAttribute("loginBean");
        Users aktueller_user = loginbean.getLoggedinUser();
        String interessen = aktueller_user.getInterest();
        String sprache = aktueller_user.getLanguage();

        //Verwendet den Komperator um die Beschreibungen zu sortieren.
        Description_comperator dc = new Description_comperator(interessen, sprache);
        selected_des.sort(dc);

        //wenn keine Beschreibungen vorhanden sind wir eine erzeugt damit diese angezeigt werden kann
        if (this.selected_des.isEmpty()) {
            List<Descriptions> keineEintr = new ArrayList<>();
            Descriptions kein_des = new Descriptions();
            kein_des.setText("KEINE Beschreibung vorhanden");
            //TODO: i18n
            keineEintr.add(kein_des);
            selected_des = keineEintr;
        }

    }

    /**
     * Liefert die Map zurück. Beim Aufrufen der Map wird loadCard() ausgeführt,
     * damit die Map immer aktuell gehalten wird.
     *
     * @return the map
     */
    public Map getMap() {
        loadCard();
        return map;
    }

    /**
     * @param map the map to set
     */
    public void setMap(Map map) {
        this.map = map;
    }

    /**
     * @return the selected
     */
    public Places getSelected() {
        return selected;
    }

    /**
     * @param selected the selected to set
     */
    public void setSelected(Places selected) {
        this.selected = selected;
    }

    /**
     * @return the nr
     */
    public int getNr() {
        return nr;
    }

    /**
     * @param nr the nr to set
     */
    public void setNr(int nr) {
        this.nr = nr;
        lade_poi();
    }

    /**
     * @return the neuer_kommentar
     */
    public Comments getNeuer_kommentar() {
        return neuer_kommentar;
    }

    /**
     * @param neuer_kommentar the neuer_kommentar to set
     */
    public void setNeuer_kommentar(Comments neuer_kommentar) {
        this.neuer_kommentar = neuer_kommentar;
    }

    /**
     * fügt dem aktuell POI, welcher sich in selected befindet den Kommentar aus
     * neuer_kommentar hinzu und persistiert dies. Anschließend wir neuer_kommentar
     * ein neuer Comment
     */
    public void speicher_neuen_kommentar() {

        HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        loginBean loginbean = (loginBean) req.getSession().getAttribute("loginBean");

        neuer_kommentar.setTime(new Date());
        neuer_kommentar.setIdPlace(selected);
        neuer_kommentar.setIdUser(loginbean.getLoggedinUser());

        this.selected.getCommentsList().add(neuer_kommentar);

        System.out.println("kommentar erzeug");
        CommentsFacade cf = new CommentsFacade();
        cf.create(neuer_kommentar);
        this.place_fac.edit(selected);

        this.neuer_kommentar = new Comments();

    }

    /**
     * @return the selected_des
     */
    public List<Descriptions> getSelected_des() {
        return selected_des;
    }

    /**
     * @param selected_des the selected_des to set
     */
    public void setSelected_des(List<Descriptions> selected_des) {
        this.selected_des = selected_des;
    }

}
