package org.wanderWiki.beans;

import java.net.Proxy;
import java.util.HashMap;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.wanderWiki.Helfer.Strings_Funktionen;
import org.wanderWiki.Helfer.Strings_Parameter;
import org.wanderWiki.Helfer.Strings_Session;
import org.wanderWiki.entities.Users;

/**
 * Diese Bean sollte einen möglichkeit bereitstellen normale HTTP anfragen zu
 * verabeiten und Datenbankabfragen durchzuführen. Anschließend sollte diese
 * Bean die Daten in JSON-Format an den Client weitergeben.
 * @deprecated Wird seit JAX nicht mehr verwendet
 * @author Claas
 */
@ManagedBean
@SessionScoped
public class requestJSON {

    private final boolean DEBUG = true;
    

    private final int AUTH_ERFORDERLICH = HttpServletResponse.SC_UNAUTHORIZED;

    private HashMap<String, String> mappe;
    private int counter;
//    private Claas_DAO dao;

    //Das hier ist ganz wichtig! 
    @ManagedProperty(value = "#{jpaResourceBean}")
    private static JPAResourceBean jpaResourceBean;

    public requestJSON() {
        counter = 1;
//        this.dao = new Claas_DAO();
       
    }

    
    

    /**
     * Diese Methode wird bei jedem Request an JSON.xhtml aufgerufen! Hier
     * sollen alle Funktionen die die Android app hat realisiert werden. Die
     * Funktionen sollen alle Folgende Übergabeparameter haben: Request Response
     * Zudem sollen sie keine Rückgabeparameter haben.
     *
     * Die Funktion selber muss den Repsonse selber abschicken. Nach dem
     * ausführen der bestimmen Funktion muss 'return "" ' ausgeführt werden
     * damit der Respone final zum Client geschickt wird.
     *
     *
     * @return
     * @author Claas
     */
    public String do_request() {
        counter++;
        HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        HttpServletResponse res = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();

        String funktionsaufruf = req.getParameter("func");
        //Hier wird der Request verarbeitet und die entsprechende Funktion aufgerufen
        try {

            //hier wird erstmal geprüft ob es ein Loginversuch ist!
            if (funktionsaufruf.equals(Strings_Funktionen.FUNC_LOGIN)) {
                USER_auth(req, res);
                return "";
            }

            if (funktionsaufruf.equals(Strings_Funktionen.FUNC_REGISTRIEREN)) {
                USER_registrierung(req, res);
                System.out.println("registrieren");
                return "";
            }

            //wenn der Benutzer sich bereits angemeldet hat
            System.out.println("Benutzer eingeloggt: " + req.getSession().getAttribute("auth").toString());
            if (req.getSession().getAttribute("auth").equals("true")) {

                String ausgabe = "MEIN JSON :))))) ";
//        ausgabe += counter + "";
//        for (Map.Entry<String, String[]> entrySet
//                : req.getParameterMap()
//                .entrySet()) {
//            ausgabe += "{ ";
//            ausgabe += entrySet.getKey();
//            ausgabe += ": ";
//            ausgabe += (entrySet.getValue())[0];
//            ausgabe += "}, ";
//        }
//        ausgabe += "}";
//                ausgabe = Strings_Funktionen.gibt_elemente();
//                sende_response(ausgabe, req, res);

                
               
                
                unbekannte_func(req, res);
            } else {
                //Wenn der Benutzer nicht eingeloggt ist
                Session_ungueltig(req, res);

            }

            //Hier wird geswichted zwischen den Funktionen!
//            switch (funktionsaufruf) {
//                case FUNC_REGISTRIEREN:
//                    
//                    
//                    break;
//
//                default:
//                    System.out.println("i liegt nicht zwischen null und drei");
//            }
        } catch (Exception e) {

            if (DEBUG) {
                e.printStackTrace();
            }
//            unbekannte_func(req,res);
            Session_ungueltig(req, res);
        }
        return "";
    }

    /**
     * Sende den Text an den Client
     *
     * @author Claas
     * @param text
     * @param req
     * @param res
     */
    public void sende_response(String text, HttpServletRequest req, HttpServletResponse res) {

        try {

            res.setStatus(HttpServletResponse.SC_OK);
            res.getWriter().append(text);
            res.flushBuffer();
            res = null;

        } catch (Exception e) {
        }

    }

    /**
     * Wird aufgerufen wenn eine Session ungültig ist.
     * @param req
     * @param res
     */
    public void Session_ungueltig(HttpServletRequest req, HttpServletResponse res) {

        try {

            res.sendError(this.AUTH_ERFORDERLICH, "!!!!Nicht Authentifizierter Zugriff!!!");

        } catch (Exception e) {
        }

    }

    /**
     * Sollte eine unbekannte Funktion angeforder werden.
     * @param req
     * @param res
     */
    public void unbekannte_func(HttpServletRequest req, HttpServletResponse res) {

        try {

            if (DEBUG) {
                res.sendError(404, "Funktion nicht bekannt!! Bitte eine der Folgenden nutzen:" + Strings_Funktionen.gibt_elemente());

            } else {
                res.sendError(404, "Funktion nicht bekannt!!");

            }

        } catch (Exception e) {
        }

    }

    
    
   /**
     * Hier wird die Auth durchgeführt
     *
     * @param req Der Request
     * @param res Der Response
     * @throws Exception
     */
    public void USER_update(HttpServletRequest req, HttpServletResponse res) throws Exception {
        String result = "NICHT eingeloggt";
        try {
            // req.getParameter(this.EMAIL);
            
            String email = req.getParameter(Strings_Parameter.EMAIL);
            String pwd = req.getParameter(Strings_Parameter.PASSWORD);
            System.out.println(email);
            System.out.println(pwd);

            //zugriff geändert!!!
            Users user = (Users) (jpaResourceBean.getEMF().createEntityManager().createNamedQuery("Users.findByEmail")
                    .setParameter("email", email)).getSingleResult();

//            Users user = (Users) (this.dao.getEm().createNamedQuery("Users.findByEmail")
//                    .setParameter("email", email)).getSingleResult();
            //password ok?
            System.out.println(user.toString());
            if (user.getPasswordHash().equals(pwd)) {
                req.getSession().setAttribute(Strings_Session.SESSION_USER, user);
                req.getSession().setAttribute("auth", "true");
                System.out.println("EINGELOGGT");
                result = "Eingeloggt";

                sende_response("Erfolgreich als User \"" + user.getName() + "\" eingeloggt", req, res);
            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            e.printStackTrace();
            res.sendError(HttpServletResponse.SC_FORBIDDEN, "Auth Fehlgeschlagen!!!" + e.getMessage());

        }
//        return result;

    }

    
    
    
    
    
    /**
     * Hier wird die Auth durchgeführt
     *
     * @param req Der Request
     * @param res Der Response
     * @throws Exception
     */
    public void USER_auth(HttpServletRequest req, HttpServletResponse res) throws Exception {
        String result = "NICHT eingeloggt";
        try {
            // req.getParameter(this.EMAIL);
            String email = req.getParameter(Strings_Parameter.EMAIL);
            String pwd = req.getParameter(Strings_Parameter.PASSWORD);
            System.out.println(email);
            System.out.println(pwd);

            
            
            //testen der Userbean
            usersBean user_bean = new usersBean();
            Users user = null;
            
           // user = user_bean.find_by_Email(email);
            
            
            //original
            user = (Users) (jpaResourceBean.getEMF().createEntityManager().createNamedQuery("Users.findByEmail")
                    .setParameter("email", email)).getSingleResult();

//            Users user = (Users) (this.dao.getEm().createNamedQuery("Users.findByEmail")
//                    .setParameter("email", email)).getSingleResult();
            //password ok?
            System.out.println(user.toString());
            if (user.getPasswordHash().equals(pwd)) {
                req.getSession().setAttribute(Strings_Session.SESSION_USER, user);
                req.getSession().setAttribute("auth", "true");
                System.out.println("EINGELOGGT");
                result = "Eingeloggt";

                sende_response("Erfolgreich als User \"" + user.getName() + "\" eingeloggt", req, res);
            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            e.printStackTrace();
            res.sendError(HttpServletResponse.SC_FORBIDDEN, "Auth Fehlgeschlagen!!!" + e.getMessage());

        }
//        return result;

    }

    
    
    /**
     * Hier wird ein neuer Benutzer angelegt. Benötigt: Email Name Passwort
     *
     *
     * @param req
     * @param res
     * @throws Exception
     */
    public void USER_registrierung(HttpServletRequest req, HttpServletResponse res) throws Exception {

        try {
//            Users neuerUser = new Users();

            String email = req.getParameter(Strings_Parameter.EMAIL);
            String passwort = req.getParameter(Strings_Parameter.PASSWORD);
            String name = req.getParameter(Strings_Parameter.NAME);

            System.out.println(email);
            System.out.println(passwort);
//            neuerUser.setEmail(email);
//            neuerUser.setPasswordHash(passwort);
//            neuerUser.setName(name);

//            DAOFactory base = DAOFactory.getInstance();
            // UsersDAO userDAO = base.getUserDAO();
            // UsersDAOJPA jpa = new UsersDAOJPA();
            //long wert = jpa.getAnzahl();
            Users user;

            user = new Users();
            user.setEmail(email);
            user.setName(passwort);
            user.setPasswordHash(passwort);

           //user pesistieren

            System.out.println("ALLES SUPER");

        } catch (Exception e) {
            System.out.println(e.getStackTrace());
            res.sendError(HttpServletResponse.SC_FORBIDDEN, "Registierung Fehlgeschlagen!!!" + e.getMessage());

        }

    }

    /**
     * @return the jpaResourceBean
     */
    public static JPAResourceBean getJpaResourceBean() {
        return jpaResourceBean;
    }

    /**
     * @param jpaResourceBean the jpaResourceBean to set
     */
    public void setJpaResourceBean(JPAResourceBean jpaResourceBean) {
        this.jpaResourceBean = jpaResourceBean;
    }

}
