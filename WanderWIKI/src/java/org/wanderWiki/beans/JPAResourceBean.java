/**
 *
 *
 * verantwortlich: Claas Schlonsok
 */
package org.wanderWiki.beans;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Default;
import javax.enterprise.inject.Produces;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;

/**
 * Stellt eine Verbindung zum Datenbank her. Die EntityManagerFactory ist im
 * Singletone Design. Sodass nur ein EntityManagerFactory für die Ganze
 * Anwendung existiert. Zudem ist dies eine ApplicationScoped Bean.
 *
 * @author Claas & Andre
 */
@ManagedBean(name = "jpaResourceBean")
@ApplicationScoped
public final class JPAResourceBean {

    public EntityManagerFactory emf;

    /**
     * Konstruktor. Beim erstellen wird eine Neue Factory erstellt.
     */
    public JPAResourceBean() {
        this.emf = getEMF();
    }

    /**
     * Gibt den EntityFaktory zurück. Singletone Desgin. Von diesem EMF kann ein
     * EntityManager erhalten werden.
     *
     * @return EntityManagerFactory
     */
    public EntityManagerFactory getEMF() {
        if (emf == null) {
            this.emf = Persistence.createEntityManagerFactory("WanderWIKIPU");
        }
        return this.emf;
    }

}
