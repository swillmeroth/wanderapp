/*******************************************************
* WanderApp
* erstellt am 21.07.2014
* Registrierung-Bean
* Aufnahme der eingegebene Werte, Übernahme in Datenbank
* inkl. Mailversand und der Übermittlung der eingegebenen
* Daten
* Verantwortlich: Andre Hantke
* Version 1.0
********************************************************/
package org.wanderWiki.beans;
import java.io.IOException;
import java.util.ResourceBundle;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import org.apache.commons.mail.*;
import org.primefaces.context.RequestContext;
import org.wanderWiki.DAO.UsersDAOJPA;
import org.wanderWiki.Helfer.MailMessage;
import org.wanderWiki.entities.Users;

@ManagedBean
@RequestScoped
public class registrationBean{
    
    //Attribute
    public ResourceBundle rb;
    private String name;
    private String email;
    private String email2;
    private String pass;
    private String pass2;
    
    private String value;
    
    //Konstruktor
    public registrationBean(){
    }    
    
    //getter + setter
    public ResourceBundle getRb() {
        return rb;
    }

    public void setRb(ResourceBundle rb) {
        
      
        this.rb = rb;
    }
    
    public String getValue() {
        return value;
    }
    
    public void setValue(String value) {
        this.value = value;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getPass2() {
        return pass2;
    }

    public void setPass2(String pass2) {
        this.pass2 = pass2;
    }    

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail2() {
        return email2;
    }

    public void setEmail2(String email2) {
        this.email2 = email2;
    }
  
    //Methode zum registrieren eines Users
    //@autor: Andre Hantke
    //anschließend erhält er eine Willkommensmail
    public void registerUser() throws EmailException, IOException, Exception{
        System.out.println("registerUser() aufgerufen");
        
        //aktueller Context
        FacesContext ctx = FacesContext.getCurrentInstance();
        //Externe Context des Requests
        RequestContext context = RequestContext.getCurrentInstance();
        this.setRb(ResourceBundle.getBundle("org.wanderWiki.language/messages",FacesContext.getCurrentInstance().getViewRoot().getLocale()));
        this.setValue(rb.getString("captcha1"));
        
        //Erstellung eines neuen Nutzers
        Users user = new Users();  
        user.setEmail(this.email);
        user.setName(this.name);
        user.setPasswordHash(this.pass);    
        user.setWheelchair(Boolean.FALSE);
        user.setWalkingSpeed("normal");
        user.setLanguage("de");
        user.setUserlevel(0);
        user.setInterest("kein Interesse");
        
        //Verbindung Richtung Datenbank
        UsersDAOJPA udj = new UsersDAOJPA();
        //User erstellen
        boolean ok=udj.create(user); 
        
        //wenn alles ok
        if(ok){
           //Verschicke willkommensmail 
           MailMessage mail = MailMessage.getInstance();
           mail.sendWelcomeMail(user);
           //Umleitung auf anders Facelet
           ctx.getExternalContext().redirect("registrationIndex.xhtml");
           //Dialog schließen
           context.execute("PF('regi').hide()");     
           //Ausgabe einer Nachricht
           ctx.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR, rb.getString("danke"),rb.getString("regerfolgreich"))); 
           
        }else{
               //Ausgabe einer Nachricht
               ctx.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, rb.getString("regnichterfolgreich"), rb.getString("regnichterolgreich2")));
        }
      
    }    
}
