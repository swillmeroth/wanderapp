/**
 * *****************************************************
 * WanderApp 
 * erstellt am 19.11.2014 
 * User verwalten
 * Verantwortlich: Claas Schlonsok 
 * Version 0.1 
 *******************************************************/
package org.wanderWiki.beans;

import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.faces.bean.ManagedProperty;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import org.wanderWiki.entities.Users;

/**
 * Bean für die verwaltung von Benutzern
 * @deprecated wird whol nicht mehr verwendet.
 * 
 * @author Claas
 */
@Named(value = "usersBean")
@Dependent
public class usersBean {

    /**
     * Creates a new instance of usersBean
     */
    public usersBean() {
        
    }

    //Das hier ist ganz wichtig! 
    @ManagedProperty(value = "#{jpaResourceBean}")
    private static JPAResourceBean jpaResourceBean;

    public void create(Users user) {
        EntityManager em = getJpaResourceBean().getEMF().createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.persist(user);
        tx.commit();
        em.close();
    }
    
    
    
      
    public boolean isgueltig(String email, String pass) {
        
       return this.find_by_Email(email).getPasswordHash().equals(pass);
        
    }
    

  
    public Users find(long userid) {
         Users user = (Users) (jpaResourceBean.getEMF().createEntityManager().createNamedQuery("Users.findByUserid")
                    .setParameter("userid", userid)).getSingleResult();
        return user;
    }

    
    public Users find_by_Email(String email) {
        Users user = (Users) (jpaResourceBean.getEMF().createEntityManager().createNamedQuery("Users.findByEmail")
                    .setParameter("email", email)).getSingleResult();
        return user;
    }

    
    public void update(Users user) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
    public void delete(Users user) {
        
//         Users user = (Users) (jpaResourceBean.getEMF().createEntityManager().createNamedQuery("Users.findByUserid")
//                    .setParameter("userid", user.getUserid())).getSingleResult();
        
        EntityManager em = getJpaResourceBean().getEMF().createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.remove(user);
        
//        em.persist(user);
        tx.commit();
        em.close();
        
        
    }
    
    
    
    
   /* ################################################# */
    
   

    /**
     * @return the jpaResourceBean
     */
    public JPAResourceBean getJpaResourceBean() {
        return jpaResourceBean;
    }

    /**
     * @param jpaResourceBean the jpaResourceBean to set
     */
    public void setJpaResourceBean(JPAResourceBean jpaResourceBean) {
        this.jpaResourceBean = jpaResourceBean;
    }

}
