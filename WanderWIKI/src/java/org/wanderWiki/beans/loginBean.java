/**
 * *****************************************************
 * WanderApp erstellt am 13.07.2014 Login eines Nutzers (Bean) - nach
 * erfolgreichen einloggen wird ein der Nutzer automatisch gegrüßt - nach nicht
 * erfolgreichen einloggen, wird der Nutzer darauf hingewiesen 
 * Verantwortlich: Andre Hantke  Editiert: Claas Schlonsok Version 1.0
 * 
 * Update: die Internationalisierung, wird schoan ausgeführt durch den Eintrag 
 * kkomend aus der Datenbank
 * TODO: JAVADOC
 * ******************************************************
 */
package org.wanderWiki.beans;


import java.util.Locale;
import java.util.ResourceBundle;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.wanderWiki.DAO.UsersDAOJPA;
import org.wanderWiki.entities.Users;

@ManagedBean
@SessionScoped
public class loginBean{
    
    
    //Verbindung für die Internationalsierung
    @ManagedProperty(value="#{internationalButton}")
    private internationalButton internationalButton;

    private final String EINGELOGGT = "/resources/basics/loggedHeader.xhtml";
    private final String AUSGELOGGT = "/resources/basics/loginHeader.xhtml";
    
    private String name;
    private String email;
    private String passwort;
    private Boolean logged;
    private String site;
    private String menubar = "/resources/basics/menubarHeader.xhtml";
    ResourceBundle rb;
    private String userlevel;
    private Users loggedinUser;
   
    //Konstruktor
    public loginBean() {
        this.logged = false;
        this.site = this.AUSGELOGGT;
    }
     
//    TODO: der Userlever muss beim aufrufen von gesicherten Seiten überprüft werden
//            wenn der userlevel nicht stimmt muss umgeleitet werden

    
    //getter + setter
    public Users getLoggedinUser() {
        return loggedinUser;
    }

    public void setLoggedinUser(Users loggedinUser) {
        this.loggedinUser = loggedinUser;
    }


    public String getMenubar() {
        return menubar;
    }

    public void setMenubar(String menubar) {
        this.menubar = menubar;
    }
    
    public String getSite() {
        return this.site;
    }
    
     public internationalButton getInternationalButton() {
        return internationalButton;
    }

    public void setInternationalButton(internationalButton internationalButton) {
        this.internationalButton = internationalButton;
    }

    public void setSite(String site) {
        this.site = site;
    }

    /**
     *
     * @return
     */
    public Boolean isLogged() {
        return logged;
    }

    
    public void setLogged(Boolean logged) {
        if (logged == true) {
            this.site = this.EINGELOGGT;
            this.logged = logged;
        } else {
            this.site = this.AUSGELOGGT;
            this.logged = logged;
            FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String Name) {
        this.name = Name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPasswort() {
        return passwort;
    }

    public void setPasswort(String passwort) {
        this.passwort = passwort;
    }

    
    //Methode des Einloggens
    //@autor: Andre Hantke
    public void checkUserLogin() {
        //aktueller Context
        FacesContext cxt = FacesContext.getCurrentInstance();
       
        //lade resourceBunde
        rb = ResourceBundle.getBundle("org.wanderWiki.language/messages", cxt.getViewRoot().getLocale());

        
        if ((this.email.equals("")) || (this.passwort.equals(""))) {
            setLogged(false);
            this.site = this.AUSGELOGGT;
            cxt.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", rb.getString("check")));
        } else {
            //finde USer mit email und passwort in DB
            UsersDAOJPA jpa2 = new UsersDAOJPA();
            System.out.println("Email : " + this.email + "Pass : "+ this.passwort);
            
            //der eingeloggteUser wird zwischengespeichert
            this.setLoggedinUser(jpa2.find(this.email, this.passwort));
            
            //System.out.println(userName);
            
            
            
            
            if (this.getLoggedinUser()==null) {
                this.site = this.AUSGELOGGT;
                cxt.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", rb.getString("logginfailed")));
            } else if (!this.getLoggedinUser().getVerificated()) {
                    System.out.println("User ist nicht verifiziert");
                    this.setLoggedinUser(null);
                    this.site = this.AUSGELOGGT;
                    cxt.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", rb.getString("logginfailed")));
                    
            } else {
                //durch den eintrag der Sprache des Users
                //wird das Locale gesetzt: a) deutsch
                //                         b) englisch
                if(loggedinUser.getLanguage().equals("de")){
                    cxt.getViewRoot().setLocale(Locale.GERMAN);
                    rb = ResourceBundle.getBundle("org.wanderWiki.language/messages", cxt.getViewRoot().getLocale());
                    internationalButton.setLoc("de");
                    internationalButton.setValue("org.wanderWiki.language/messages");
                    internationalButton.setLocale(Locale.GERMAN);
                }else{
                    if(loggedinUser.getLanguage().equals("en")){
                        cxt.getViewRoot().setLocale(Locale.ENGLISH);
                        rb = ResourceBundle.getBundle("org.wanderWiki.language/messages", cxt.getViewRoot().getLocale());
                        internationalButton.setLoc("en");
                        internationalButton.setValue("org.wanderWiki.language/messages_en");
                        internationalButton.setLocale(Locale.ENGLISH);
                    }
                }
                this.logged = true;
                this.site = this.EINGELOGGT;
                this.name = this.getLoggedinUser().getName();
                System.out.println("Name: " + this.getLoggedinUser().getName());
                System.out.println("level: " + this.getLoggedinUser().getUserlevel());
                cxt.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", rb.getString("logged")));
            }
        }
    }
    /**
     * gibt  verschiede menues zurück. 
     * Eines wenn nicht eingelogt und 
     * das andere wenn der Benutzer eingeloggt ist.
     * das letzte wenn ein admin eingeloggt ist
     *
     * @author Claas
     * @return Menu entsprechend des Userlevels
     */
    public String gibMenu() {
        if (this.logged) {
            if (istUserAdmin()){
                return "/resources/basics/menubarHeader_admin.xhtml";
            }else{
               return "/resources/basics/menubarHeader_logged_in.xhtml"; 
            }
            
        } else {
            return "/resources/basics/menubarHeader.xhtml";
        }

    }

    /**
     * Diese funktion muss bei jedem aufruf aufgerufen werden. Die Funktion
     * prüft in welche Ordner die Request geht und prüft anschließend ob der
     * Benutzer die erfoderlichen Rechte hat.
     *
     * @return true order false als String. Ob der Zugriff erlaubt war.
     */
    public String pruefe_zugriffsrechte() {
        //wenn DEBUG = true wird das weiterleiten unterdrückt
        boolean DEBUG = false;
        String admin_regex = "/admin";
        String users_regex = "/users";
        try {
            HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
            String url = req.getRequestURI();
//            Hier werden die zugriffsrechte beim request geprüft

            //immer erst auf Admin prüfen, damit z.b. der Ordner admin/users als admin erkannt wird
            if (url.contains(admin_regex)) {
//               ein request auf den Adminbereich ist erfolgt
//                System.out.println("ADMIN_bereich!");

                if (istUserAdmin()) {
                     return "true";
//                    return "ADMIN Alles OK";
                }
            } else if (url.contains(users_regex)) {
//               ein request auf den USERbereich ist erfolgt
//                System.out.println("USER_bereich!");
                if (this.isLogged()) {
//            TODO hier muss der userlevel nicht der name geprüft werden
                    return "true";
//                    return "USER Alles OK";
                }

            } else {
//                System.out.println("PUBLIC_bereich!");
                return "true";
//                return "PUBLIC Alles OK";
            }

        } catch (Exception e) {
        }

        // Wenn ein Request hier ankommt, hat der User nict die erfoderlichen Rechte
        try {
            if (DEBUG == false) {
                String pfad=FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath();
                FacesContext.getCurrentInstance().getExternalContext().redirect(pfad + "/please_login.xhtml");
            }

        } catch (Exception e) {
        }
        return "false";
//        return "NICHT AUTORISIERT!";

    }

    @Override
    public String toString() {
        return this.name + " - " + this.passwort;
    }
    
    private boolean istUserAdmin(){
        if (this.loggedinUser.getUserlevel()==2) {  
            return true;
        }
       return false;
      
    }
    
    /**
     * Einfache Funktion die die Session des Benutzer beendet und Ihn ausloggt.
     */
    public void user_ausloggen(){
        logged = false;
        FacesContext fc = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession)fc.getExternalContext().getSession(false);
        session.invalidate();

        
    }
}
