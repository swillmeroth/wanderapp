/*******************************************************
* WanderApp
* erstellt am 21.07.2014
* Dialog Bean, dient zum Anzeigen eines Dialogs via Bean
* Verantwortlich: Andre Hantke
* Version 1.0
********************************************************/

package org.wanderWiki.beans;

import javax.faces.bean.ManagedBean;
import org.primefaces.context.RequestContext;

@ManagedBean
public class dialogBean {
    RequestContext context;
    
    //Anzeige Dialog Registrierung
    //@autor: Andre Hantke
    public void showRegistrationDialog(){
        context = RequestContext.getCurrentInstance();
        context.execute("PF('regi').show()");
    }
    
    //Ausblenden Dialog Registrierung
    //@autor: Andre Hantke
    public void hideRegistrationDialog(){
        context.execute("PF('regi').hide()");
    }
    
    //Dialog zur Passwortanforderung
    //@autor: Andre Hantke
    public void showPasswordNewDialog(){
        context=RequestContext.getCurrentInstance();
        context.execute("PF('passwordNew').show()");
    }
    
    //Verstecke Dialog zur Passwortanforderung
    //@autor: Andre Hantke
    public void hidePasswordNewDialog(){
        context.execute("PF('passwordNew').hide()");
    }
    
    //Dialog mein Account
    //@autor: Andre Hantke
    public void showMyAccountDialog(){
        context=RequestContext.getCurrentInstance();
        context.execute("PF('myAccount').show()");
    }
    
    //Verstecke mein Account
    //@autor: Andre Hantke
    public void hideMyAccountDialog(){
        context.execute("PF('myAccount').hide()");
    }
    
    
    
}
