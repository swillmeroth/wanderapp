/**
 * *****************************************************
 * WanderApp 
 * erstellt am 11.04.2015 
 * Routen verwalten 
 * Verantwortlich: Claas Schlonsok 
 * Version 0.1 
 *******************************************************/
package org.wanderWiki.beans;

import com.googlecode.gmaps4jsf.component.common.Position;
import com.googlecode.gmaps4jsf.component.map.Map;
import com.googlecode.gmaps4jsf.component.marker.Marker;
import com.googlecode.gmaps4jsf.component.icon.Icon;
import com.googlecode.gmaps4jsf.component.polyline.Polyline;
import com.googlecode.gmaps4jsf.component.point.Point;
import com.googlecode.gmaps4jsf.component.window.HTMLInformationWindow;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Set;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.servlet.http.HttpServletRequest;
import org.wanderWiki.Helfer.Description_comperator;
import org.wanderWiki.entities.Comments;
import org.wanderWiki.entities.Descriptions;
import org.wanderWiki.entities.Places;
import org.wanderWiki.entities.Placesroutes;
import org.wanderWiki.entities.Routepoints;
import org.wanderWiki.entities.Routes;
import org.wanderWiki.entities.Users;
import org.wanderWiki.jpa.CommentsFacade;
import org.wanderWiki.jpa.PlacesFacade;
import org.wanderWiki.jpa.RoutepointsFacade;
import org.wanderWiki.jpa.RoutesFacade;

/**
 * Diese Bean verwaltet Routen. Das beinhaltet in erster Linie das erstellen von
 * neuen Routen
 * 
 * @author Claas
 */
@ManagedBean(name = "routeBean")
@SessionScoped
public class routeBean {

    private final String ICON = "/resources/icons/BILD_TYPE_NAME.png";

    private Routes route;
    private Routepoints routepoint;
    private double longitude;
    private double latitude;

    private Map map;

    private Map alle_routen;

    private Routes selektierte_route;
    private int selektierte_id;
    private Map selektierte_map;
    private final String ROUTENICON = "/resources/icons/route.png";
    private Comments neuer_kommentar;

//    @ManagedProperty(value = "#{RoutesFacade}")
    private RoutesFacade routes_fac;
    private RoutepointsFacade routePoint_fac;

    private Descriptions beschreibung;
    private List<Descriptions> beschreibung_liste;

    private List<Descriptions> selected_des;

    private int id_places;
    private Set<Places> places_Liste;
    private PlacesFacade place_fac;

    private Map poimap;

    /**
     * Konstruktor der Klasse. Instanziert die relevanten Werte.
     * 
     * @author Claas
     */
    public routeBean() {
//        dao = new PlacesDAOJPA();
        this.route = new Routes();
        this.routepoint = new Routepoints();
        routes_fac = new RoutesFacade();
        routePoint_fac = new RoutepointsFacade();
        place_fac = new PlacesFacade();
        this.map = new Map();
        map.setLatitude("51.5");
        map.setLongitude("7");
        neuer_kommentar = new Comments();
//        DEBUG_punkt_erstellen();
        beschreibung = new Descriptions();
        beschreibung_liste = new ArrayList<>();

    }

    /**
     * Testefunktion um eine Route zu erzeugen
     * 
     * @author Claas
     */
    public void DEBUG_punkt_erstellen() {
        Routepoints rp = new Routepoints();
        rp.setLatitude(51.5);
        rp.setLongitude(7.);
        rp.setNum(1);
        try {
            List<Routepoints> s = this.route.getRoutepointsList();
            s.add(rp);
            this.route.setRoutepointsList(s);
        } catch (Exception e) {
            List<Routepoints> s = new LinkedList<Routepoints>();
            s.add(rp);
            this.route.setRoutepointsList(s);
        }

    }

    /**
     * 
     * @return
     */
    public double getLongitude() {
        return longitude;
    }

    /**
     *
     * @param longitude
     */
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    /**
     *
     * @return
     */
    public double getLatitude() {
        return latitude;
    }

    /**
     *
     * @param latitude
     */
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    /**
     * @deprecated wird wohl nicht mehr verwendet
     */
    public void loadData() {
        System.out.println("loaddata wurde geklick");

        //Liste erstellen
    }

    /**
     * Erzeugt eine neue Map falls noch keine vorhanden ist.
     * @return the map
     */
    public Map getMap() {
        if (this.map == null) {
            map = new Map();
//            map.setWidth("600px");
//            map.setHeight("450px");
//            map.setId("gmaps_karte_route_uebersicht");
        }
//        aktualisiere_map();
        return map;
    }

    /**
     * @param map the map to set
     */
    public void setMap(Map map) {
        this.map = map;
    }

    /**
     * wird bei der erstelle route verwendet um die Map und die Map für die POIs
     * zu aktualisierte.
     * Die Inhalte der Map veränder sich nur wenn der aktuelle benutzer einen
     * neuen Routenpunkt hinzufügt.
     * Diese Funktion erstellt auf beiden die aktuell geplante route des Benutzer
     * zusätzlich werden am ende noch die POIs zur poimap hinzugefügt.
     * Das Doppelte erstellen jedes einzelnen Objekt ist NOTWENDIG, da beim
     * kopieren der MAP nach POIMAP die id erhalten bleiben. Anschließend sind 
     * die kopierten elemente leider doppelt vorhanden, was bei JSF zu Fehlern
     * bei der ausführung führt.
     * 
     * @author Claas
     */
    public void aktualisiere_map() {
        System.out.println("aktualiere map");
//        Marker marker1 = new Marker();
//            marker1.setDraggable("false");
//        marker1.setLatitude("51");
//            marker1.setLongitude("7");
////            map.getChildren()
//         
//             map.getChildren().add(marker1);

        this.map.getChildren().clear();

        this.poimap.getChildren().clear();

        try {
            Point[] liste = new Point[this.route.getRoutepointsList().size()];
            Point[] poi_liste = new Point[this.route.getRoutepointsList().size()];
            String request = FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath();

            for (Routepoints rpoints : this.route.getRoutepointsList()) {
                System.out.println("[routeBean] Teil des weges: " + rpoints.toString());

                //marker für map erstellen
                Marker marker = new Marker();
                System.out.println("nummer:" + rpoints.getNum());
                marker.setDraggable("false");
                marker.setLatitude("" + rpoints.getLatitude());
                marker.setLongitude("" + rpoints.getLongitude());
                this.map.getChildren().add(marker);

                //Marker für poimap erstellen
                Marker poi_marker = new Marker();
                poi_marker.setDraggable("false");
                poi_marker.setLatitude("" + rpoints.getLatitude());
                poi_marker.setLongitude("" + rpoints.getLongitude());
                this.poimap.getChildren().add(poi_marker);

                //zur erstellung der Linie auf der Map
                Point punkt = new Point();
//                punkt.setId("punkt_"+rpoints.getNum());
                punkt.setLatitude("" + rpoints.getLatitude());
                punkt.setLongitude("" + rpoints.getLongitude());

                liste[rpoints.getNum()] = punkt;

                //zur erstellung der Linie auf der POIMap
                Point poi_punkt = new Point();
//                punkt.setId("punkt_"+rpoints.getNum());
                poi_punkt.setLatitude("" + rpoints.getLatitude());
                poi_punkt.setLongitude("" + rpoints.getLongitude());

                poi_liste[rpoints.getNum()] = poi_punkt;

            }
            //Line für die Map erstellen
            Polyline line = new Polyline();
            line.setId("map_line");
            for (Point punkte : liste) {
                line.getChildren().add(punkte);
            }
            this.map.getChildren().add(line);

            //Line für die POIMap erstellen
            Polyline poi_line = new Polyline();
//            poi_line.setId("map_line");
            for (Point punkte : poi_liste) {
                poi_line.getChildren().add(punkte);
            }
            this.poimap.getChildren().add(poi_line);

        } catch (Exception e) {
        }

//       lade alle Pois. dieser werden dann in poimap hinzugefügt.
        lade_alle_pois_in_map(this.poimap);
    }

    /**
     * Wenn die Route persistiert werden soll wird diese Funktion ausgeführt.
     * Beschreibungen, POIs, Routenpunkte werden hinzugefügt.
     * Anschließend wird die Route zurückgesetzt.
     * 
     * @author Claas
     */
    public void persistiere() {
        ArrayList<Descriptions> neue = new ArrayList<>();

        //Beschreibungen hinzufügen
        for (Descriptions beschreibungs : beschreibung_liste) {
            beschreibungs.setIdRoute(route);
            neue.add(beschreibungs);

        }
        this.route.setDescriptionsList(neue);

        //Places und die aufgelöste m : n Beziehung hinzufügen
        List<Placesroutes> placeroutes_liste = new ArrayList<>();
        for (Places poi : this.getPlaces_Liste()) {
            Placesroutes plroute = new Placesroutes();
            plroute.setIdRoute(route);
            plroute.setIdPlace(poi);
            placeroutes_liste.add(plroute);

        }
        this.route.setPlacesroutesList(placeroutes_liste);



        routes_fac.create(this.route);
        
        
        //        TODO: i18n
        FacesContext cxt = FacesContext.getCurrentInstance();
        FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, "", this.getRoute().getName() + " wurde als Route erstellt");
        cxt.addMessage(null, fm);
        
        
        //Route leeren und MAP aktualiseren
        leere_Route();
        aktualisiere_map();

    }

    /**
     * Diese Methode ruf anhand der bereits gesetzten ID die restlichten Daten
     * ab und speichert die Daten in
     * selektierte_route, selektierte_map
     *
     * @author Claas
     */
    public void selektiert_lade_route() {

        this.selektierte_route = this.routes_fac.find(this.selektierte_id);
        this.routes_fac.edit(selektierte_route);
        this.setSelektierte_map(new Map());

        //ende wenn die selektierte proposed ist!
        if (selektierte_route.getIsProposed()) {
            this.selektierte_route = new Routes();
            return;
        }

        Polyline line = new Polyline();

        //liste ordnert die punkte 
        Point[] liste = new Point[selektierte_route.getRoutepointsList().size()];
        String request = FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath();
        try {
            for (Routepoints rpoints : getSelektierte_route().getRoutepointsList()) {
                System.out.println("[routeBean] Teil des weges: " + rpoints.toString());

                //fuegt nur Start und Ziel hinzu
                if (rpoints.getNum() == 0 || rpoints.getNum() == (selektierte_route.getRoutepointsList().size() - 1)) {

                    Marker marker = new Marker();
                    System.out.println("nummer:" + rpoints.getNum());
                    marker.setId("marker_" + rpoints.getNum());
                    marker.setDraggable("false");
                    marker.setLatitude("" + rpoints.getLatitude());
                    marker.setLongitude("" + rpoints.getLongitude());

                    marker.setShowInformationEvent("click");
                    HTMLInformationWindow info = new HTMLInformationWindow();
                    info.setHtmlText("punkt nr: " + rpoints.getNum());

                    Icon icon = new Icon();
                    icon.setImageURL(request + ROUTENICON);

                    marker.getChildren().add(icon);
                    marker.getChildren().add(info);

                    this.selektierte_map.getChildren().add(marker);
                }

                Point punkt = new Point();
                punkt.setLatitude("" + rpoints.getLatitude());
                punkt.setLongitude("" + rpoints.getLongitude());

                liste[rpoints.getNum()] = punkt;

            }
        } catch (Exception e) {

            System.out.println(e);
        }
//        System.out.println(liste);
        for (Point punkte : liste) {
            line.getChildren().add(punkte);
        }

        selektierte_map.getChildren().add(line);

        //Hier wird die Beschreibung geladen
        selected_des = (selektierte_route.getDescriptionsList());

        HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        loginBean loginbean = (loginBean) req.getSession().getAttribute("loginBean");
        if (loginbean == null) {
            return;
        }

        Users aktueller_user = loginbean.getLoggedinUser();
        String interessen = aktueller_user.getInterest();
        String sprache = aktueller_user.getLanguage();

        Description_comperator dc = new Description_comperator(interessen, sprache);
        selected_des.sort(dc);

        if (this.selected_des.isEmpty()) {
            List<Descriptions> keineEintr = new ArrayList<>();
            Descriptions kein_des = new Descriptions();
            kein_des.setText("KEINE Beschreibung vorhanden");
            //TODO: übersetzung
            keineEintr.add(kein_des);
            selected_des = keineEintr;
        }

        //Hier werden nun die relevanten POIs geladen
        List<Places> liste_places = this.place_fac.findbyIdRoute(selektierte_route.getIdRoutes());

        internationalButton interbutton = (internationalButton) req.getSession().getAttribute("internationalButton");

        Locale loc = interbutton.getLocale();
        System.out.println(loc);
        ResourceBundle rb = ResourceBundle.getBundle("org.wanderWiki.language/messages", loc);

        Marker marker;

        for (Places poi : liste_places) {
            System.out.println("aktueller_poi:" + poi.toString());

            marker = new Marker();
            marker.setDraggable("false");
            marker.setLatitude("" + poi.getLatitude());
            marker.setLongitude("" + poi.getLongitude());

            marker.setShowInformationEvent("click");

            String commandbutton = "<a href=../pois/poi_anzeigen.xhtml?poiId=" + poi.getIdPlaces() + " > " + rb.getString("karte_poi_anzeigen") + "</a>";

            HTMLInformationWindow info = new HTMLInformationWindow();
            info.setHtmlText("<b>" + poi.toString() + "</b><br>" + commandbutton);
            marker.getChildren().add(info);

            String url_zum_bild = request + this.ICON.replace("BILD_TYPE_NAME", poi.getType());

            Icon icon = new Icon();
            icon.setImageURL(url_zum_bild);
            marker.getChildren().add(icon);

//            marker.setonc
            selektierte_map.getChildren().add(marker);
        }

    }

    @Override
    public String toString() {
        return "routeBean: lat: " + latitude + " long: " + longitude;
    }

    /**
     * Diese Funktion ist ein Listener für den Marker auf der GoogleMap. Jedes
     * mal wenn dieser an eine neue Stelle bewegt wird löst es dieses Funktion
     * aus. Das event beinhaltet die neue Position des Marker. Diese wird 
     * ausgelesen und in die Variablen geschrieben.
     * 
     * @param event Wenn der Marker an eine neue Position bewegt wird
     * @throws AbortProcessingException
     * @author Claas
     */
    public void legePunktFest(ValueChangeEvent event) throws AbortProcessingException {
        Position value = (Position) event.getNewValue();

        Marker source = (Marker) event.getSource();
        String markerID = source.getJsVariable();

        this.getRoutepoint().setLatitude(Double.parseDouble(value.getLatitude()));
        this.getRoutepoint().setLongitude(Double.parseDouble(value.getLongitude()));
        String message = markerID + " is moved to the following location: " + value.toString();

        System.out.println(message);
        System.out.println("GNAMPF");
        System.out.println(this.getRoutepoint());

    }

    /**
     * @return the route
     */
    public Routes getRoute() {
        return route;
    }

    /**
     * @param route the route to set
     */
    public void setRoute(Routes route) {
        this.route = route;
    }

    /**
     * @return the routepoint
     */
    public Routepoints getRoutepoint() {
        return routepoint;
    }

    /**
     * @param routepoint the routepoint to set
     */
    public void setRoutepoint(Routepoints routepoint) {
        this.routepoint = routepoint;
    }

    /**
     * Speicher dern Routenpunkt der in routenpunkt ist in route.routenliste
     * 
     * @author Claas
     */
    public void speicher_routepoint_in_liste() {
//        routePoint_fac.create(routepoint);
        routepoint.setIdRoute(route);
        try {
            routepoint.setNum(route.getRoutepointsList().size());
            route.getRoutepointsList().add(this.routepoint);
        } catch (Exception e) {
            routepoint.setNum(0);
            List<Routepoints> s = new LinkedList<>();
            s.add(routepoint);
            this.route.setRoutepointsList(s);
        }
        System.out.println("[routeBean] Routepoint nr " + routepoint.getNum() + " wurde erstellt");
        System.out.println(route.getRoutepointsList());
        this.routepoint = new Routepoints();
        routepoint.setNum(route.getRoutepointsList().size());
        aktualisiere_map();
    }

    /**
     * leert die Route komplett.
     * 
     * @author Claas
     */
    public void leere_Route() {

        this.route = new Routes();
        this.routepoint = new Routepoints();
        this.map = new Map();
        map.setLatitude("51.5");
        map.setLongitude("7");
        beschreibung_liste = new ArrayList<>();
        beschreibung = new Descriptions();
        places_Liste = new HashSet<>();
        id_places = 0;
    }

    /**
     * Diese Funktion läd alle Routenanfänge aus der Datenbank.
     * Anschließend wird in die map alle_routen alle Routenanfänge hinzugefügt.
     * 
     * @author Claas
     */
    public void lade_anderen_routenanfaenge() {
        this.setAlle_routen(new Map());
        this.alle_routen.setHeight("500");
        this.alle_routen.setWidth("800");
//        this.alle_routen.setLatitude("51.5");
//        this.alle_routen.setLongitude("7");

        String commandbutton = "";

        List<Routepoints> results = this.routePoint_fac.find_Startpunkte_nicht_proposal();

        String request = FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath();
        for (Routepoints rp : results) {
            System.out.println("aktueller_poi:" + rp.toString());

            Marker marker = new Marker();
            marker.setDraggable("false");
            marker.setLatitude("" + rp.getLatitude());
            marker.setLongitude("" + rp.getLongitude());
            this.alle_routen.setLatitude("" + rp.getLatitude());
            this.alle_routen.setLongitude("" + rp.getLongitude());

            marker.setShowInformationEvent("click");
            //TODO: i18n
            commandbutton = "<h2>" + rp.getIdRoute().getName() + "</h2><br>";
            commandbutton = commandbutton + "<a href=routen_anzeigen.xhtml?routeId=" + rp.getIdRoute().getIdRoutes() + "> route ANZEIGEN</a>";

            HTMLInformationWindow info = new HTMLInformationWindow();
            info.setHtmlText("<b>" + rp.toString() + "</b><br>" + commandbutton);
            marker.getChildren().add(info);

            Icon icon = new Icon();
            icon.setImageURL(request + ROUTENICON);
            marker.getChildren().add(icon);

            this.alle_routen.getChildren().add(marker);
        }
    }

    /**
     * Bei jeden aufruf der Map wird die Karte aktualisiert.
     * 
     * @return the alle_routen
     * @author Claas
     */
    public Map getAlle_routen() {

        if (this.alle_routen == null) {
            System.out.println("ist null");

        }
        lade_anderen_routenanfaenge();
        System.out.println("getalle routen  trigger");
        System.out.println("alle routen kinder" + alle_routen.getChildren());
        return alle_routen;
    }

    /**
     * @param alle_routen the alle_routen to set
     */
    public void setAlle_routen(Map alle_routen) {
        System.out.println("set alle routen wurde aufgerufen");
        this.alle_routen = alle_routen;
    }

    /**
     * @return the selektierte_route
     */
    public Routes getSelektierte_route() {
        return selektierte_route;
    }

    /**
     * @param selektierte_route the selektierte_route to set
     */
    public void setSelektierte_route(Routes selektierte_route) {
        this.selektierte_route = selektierte_route;
    }

    /**
     * @return the selektierte_id
     */
    public int getSelektierte_id() {
        return selektierte_id;
    }

    /**
     * Beim aufruf der anzeigen.xhtml wird der Paramter übergeben der zum laden 
     * der Route führt.
     * 
     * @author Claas
     * @param selektierte_id the selektierte_id to set
     */
    public void setSelektierte_id(int selektierte_id) {
        this.selektierte_id = selektierte_id;
        selektiert_lade_route();
    }

    /**
     * @return the selektierte_map
     */
    public Map getSelektierte_map() {
//        selektiert_lade_route();
        return selektierte_map;
    }

    /**
     * @param selektierte_map the selektierte_map to set
     */
    public void setSelektierte_map(Map selektierte_map) {
        this.selektierte_map = selektierte_map;
    }

    /**
     * @return the neuer_kommentar
     */
    public Comments getNeuer_kommentar() {
        return neuer_kommentar;
    }

    /**
     * @param neuer_kommentar the neuer_kommentar to set
     */
    public void setNeuer_kommentar(Comments neuer_kommentar) {
        this.neuer_kommentar = neuer_kommentar;
    }

    /**
     *  neuer:kommentar wird zur Route hinzugefügt.
     * 
     * @author Claas
     */
    public void speicher_neuen_kommentar() {

        HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        loginBean loginbean = (loginBean) req.getSession().getAttribute("loginBean");

        neuer_kommentar.setTime(new Date());
        neuer_kommentar.setIdRoute(this.selektierte_route);
        neuer_kommentar.setIdUser(loginbean.getLoggedinUser());

        this.selektierte_route.getCommentsList().add(neuer_kommentar);

        System.out.println("kommentar erzeug");
        CommentsFacade cf = new CommentsFacade();
        cf.create(neuer_kommentar);
        this.routes_fac.edit(selektierte_route);

        this.neuer_kommentar = new Comments();

    }

    /**
     * @return the beschreibung
     */
    public Descriptions getBeschreibung() {
        return beschreibung;
    }

    /**
     * @param beschreibung the beschreibung to set
     */
    public void setBeschreibung(Descriptions beschreibung) {
        this.beschreibung = beschreibung;
    }

    /**
     * @return the beschreibung_liste
     */
    public List<Descriptions> getBeschreibung_liste() {
        return beschreibung_liste;
    }

    /**
     * @param beschreibung_liste the beschreibung_liste to set
     */
    public void setBeschreibung_liste(List<Descriptions> beschreibung_liste) {
        this.beschreibung_liste = beschreibung_liste;
    }

    /**
     * Beschreibung wird hinzugefügt und geleert.
     */
    public void erstelle_neue_beschreibung() {

        beschreibung_liste.add(beschreibung);
        beschreibung = new Descriptions();

    }

    /**
     * Beschreibung wird geleert.
     */
    public void leere_neue_beschreibung() {

        beschreibung = new Descriptions();

    }

    /**
     * @return the selected_des
     */
    public List<Descriptions> getSelected_des() {
        return selected_des;
    }

    /**
     * @param selected_des the selected_des to set
     */
    public void setSelected_des(List<Descriptions> selected_des) {
        this.selected_des = selected_des;
    }

    /**
     * @return the id_places
     */
    public int getId_places() {
        return id_places;
    }

    /**
     * @param id_places the id_places to set
     */
    public void setId_places(int id_places) {
        this.id_places = id_places;
    }

    /**
     * hier muss die id aus id_place ausgeleseen werden und anschließend der 
     * place geholt werden. Dieser place wird dann der aktuellen Place_liste
     * hinzugefügt.
     *
     * @author Claas
     */
    public void fuege_poi_hinzu() {
        int places_id = this.id_places;
        Places neuer_place = place_fac.find(places_id);

        //Prüft ob der Place mit placesid exisiter oder Proposal ist
        if (neuer_place == null || neuer_place.getIsProposal().equals(true)) {

            //erzeugt eine Fehlermeldung
            FacesContext cxt = FacesContext.getCurrentInstance();
            FacesMessage fm = new FacesMessage();
            fm.setSeverity(FacesMessage.SEVERITY_ERROR);
            //TODO i18n
            fm.setSummary("Ungültige POI ID");
            cxt.addMessage("", fm);

        } else {

            this.getPlaces_Liste().add(neuer_place);
        }

        id_places = 0;

    }

    /**
     * Standartaufruf für MAP
     * 
     * @author Claas
     */
    public void lade_alle_pois_in_map() {


        lade_alle_pois_in_map(this.map);

    }

    /**
     * Hier wird die übergebene zielmaps aktualisert und alle POIS hinzugeladen
     * 
     * @param zielmaps Die Map in welche die POIs hinzugefügt werden sollen
     * @author Claas
     */
    public void lade_alle_pois_in_map(Map zielmaps) {


        System.out.println("lade alle pois für map:" + zielmaps);

        Set<Places> results = new HashSet<>();
        results.addAll(place_fac.findAllNOTProposal());

        Marker marker;
        String request = FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath();
        for (Places poi : results) {
            System.out.println("aktueller_poi:" + poi.toString());

            marker = new Marker();
            marker.setDraggable("false");
            marker.setLatitude("" + poi.getLatitude());
            marker.setLongitude("" + poi.getLongitude());

            marker.setShowInformationEvent("click");

            HTMLInformationWindow info = new HTMLInformationWindow();
            info.setHtmlText("<h2>" + poi.getIdPlaces() + " " + poi.getName() + " </h2><br>");
            info.setId("marker_info_" + poi.getIdPlaces());
            marker.getChildren().add(info);

            String url_zum_bild = request + this.ICON.replace("BILD_TYPE_NAME", poi.getType());

            Icon icon = new Icon();
            icon.setImageURL(url_zum_bild);
            marker.getChildren().add(icon);

            zielmaps.getChildren().add(marker);
        }

    }

    /**
     * wird beim klick auf "Entferne POI" ausgeführt um einen POI aus der Liste
     * der für die Route relevanten POIS zu entfernen.
     * 
     * @param event
     * @author Claas
     */
    public void entferne_poi_aus_liste(ActionEvent event) {

        Integer id = (Integer) event.getComponent().getAttributes().get("poiid");
        System.out.println("entferne poi " + id);
        Places loeaschender = new Places(id);
        this.getPlaces_Liste().remove(loeaschender);

    }

    /**
     * @return the places_Liste
     */
    public Set<Places> getPlaces_Liste() {
        if (this.places_Liste == null) {
            places_Liste = new HashSet<>();
        }
        return places_Liste;
    }

    /**
     * @param places_Liste the places_Liste to set
     */
    public void setPlaces_Liste(Set<Places> places_Liste) {
        this.places_Liste = places_Liste;
    }

    /**
     * @return the poimap
     */
    public Map getPoimap() {

        if (this.poimap == null) {

            this.poimap = new Map();

        }

        return poimap;
    }

    /**
     * @param poimap the poimap to set
     */
    public void setPoimap(Map poimap) {
        this.poimap = poimap;
    }

    /**
     * Diese Methode soll ausgeführt werden wenn die Beschreibung in der
     * Variable beschreibung in die beschreibung_liste hinzugefügt werden soll.
     * Sollte der Übergebene Text leer sein wird auf der Webseite eine Meldung
     * ausgegeben das der Text nicht leer sein darf.
     * 
     * @author Claas
     */
    public void speicher_beschreibung_in_liste() {
        System.out.println("Beschreibung soll gespeichert werden");
        if ("".equals(beschreibung.getText())) {
            FacesContext cxt = FacesContext.getCurrentInstance();
            FacesMessage fm = new FacesMessage();
            fm.setSeverity(FacesMessage.SEVERITY_ERROR);
            //TODO i18n
            fm.setSummary("Bitte Text eingeben");

            cxt.addMessage("poi_hinzu_wizard:text", fm);
            cxt.validationFailed();

            return;
        }
        this.beschreibung_liste.add(beschreibung);
        beschreibung = new Descriptions();

    }

}
