/*******************************************************
* WanderApp
* erstellt am 22.06.2014
* dynamischer Bildwechsel auf der Homepage (Bean)
* Verantwortlich: Andre Hantke
* Version 1.0
********************************************************/

package org.wanderWiki.beans;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
 
@ManagedBean
public class imageView {
     
    private List<String> images;
     
    @PostConstruct
    public void init() {
        images = new ArrayList<>();
        
        //Lädt aus einer Resousrce die Bilder zum Anzeigen
        for (int i = 1; i <= 3; i++) {
           images.add("resources/Images/nature" + i + ".jpg");            
        }   
    }
 
    public List<String> getImages() {
        return images;
    }
}

