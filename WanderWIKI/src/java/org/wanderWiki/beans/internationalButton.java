/*******************************************************
* WanderApp
* erstellt am 13.07.2014
* Drop Down Menü Button für die Internationalisierung (Bean)
* Verantwortlich: Andre Hantke
* Version 1.0
********************************************************/

package org.wanderWiki.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ValueChangeEvent;
import javax.faces.event.ValueChangeListener;
import org.primefaces.context.RequestContext;


@ManagedBean
@SessionScoped
public class internationalButton implements Serializable, ValueChangeListener{

   //Bundele 
   ResourceBundle bundle;   
   private static final long serialVersionUID = 1L;
   //Defaultvalue
   private String value="org.wanderWiki.language/messages";
   //Länder die im DropDown zu sehen sind
   private List<String> country;
   //aktueller Wert der im DropDown ausgewählt wurde
   private String loc;
   //Locale
   private Locale locale;
   
   //Deafultwerte sind zunächst deutsch und englisch
   public internationalButton(){
       country = new ArrayList<>();  
       this.country.add("de");       
       this.country.add("en");
   }

   //setter + getter
   public String getLoc() {
       return loc;
   }

   public void setLoc(String loc) {
       this.loc = loc;
   }
   public List<String> getCountry() {
       return country;
   }

   public String getValue() {
        return value;
   }

   public void setValue(String value) {
        this.value = value;
   }
   
   
   //Methode die aufgerufen wird, wenn sich der Wert im Drop Down ändert
   //@Param: erhält das Event, das ausgelöst wird, bei Auswahl
   //        eines Eintrages des DropDowns
    @Override
    public void processValueChange(ValueChangeEvent event) throws AbortProcessingException {
       FacesContext context = FacesContext.getCurrentInstance();
          
       //System.out.println("current Locale" + context.getViewRoot().getLocale().getCountry());
       //System.out.println(event.toString());   
       
        //Reaktion auf das Item, das ausgewählt wurde
        //setzen des Locales etc.
        if(event.getNewValue().equals("de")){
           context.getViewRoot().setLocale(Locale.GERMANY);     
           setValue("org.wanderWiki.language/messages");   
           setLoc("de");
           setLocale(Locale.GERMAN);
            
        }else{
           if(event.getNewValue().equals("en")){
               context.getViewRoot().setLocale(Locale.ENGLISH);
               setValue("org.wanderWiki.language/messages_en");           
               setLoc("en");
               setLocale(Locale.ENGLISH);
            
           }           
       }
    }

    /**
     * @return the locale
     */
    public Locale getLocale() {
        return locale;
    }

    /**
     * @param locale the locale to set
     */
    public void setLocale(Locale locale) {
        this.locale = locale;
    }
}

