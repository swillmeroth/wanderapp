/**
 * *****************************************************
 * Bean zur Verwaltung des Accounts eines Users
 * erstellt: 05.12.2015 
 * Verantwortlich: Andre Hantke
 * 
 * ******************************************************
 */
package org.wanderWiki.beans;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ValueChangeEvent;
import javax.faces.event.ValueChangeListener;
import org.primefaces.context.RequestContext;
import org.wanderWiki.DAO.UsersDAOJPA;
import org.wanderWiki.entities.Users;

/**
 *
 * @author Andre
 */
@ManagedBean
@SessionScoped
public class myAccountBean{
    
    //um den eingeloggten User zu bekommen und alle 
    //seine Daten
    @ManagedProperty(value="#{loginBean}")
    private loginBean loginBean;
    
    //um die Internationalisierung auch hier zu nutzen
    @ManagedProperty(value="#{internationalButton}")
    private internationalButton internationalButton;

    //Sprache
    private List<String> language;
    //Rollstuhl
    private boolean wheelchair;    
    //Name
    private String name;
    //Email
    private String email;
    //Passwort
    private String pass;
    //Laufgeschwindiglkeit
    private List<String> walking;
    //Interessen
    private List<String> interests;
    
    //ausgewählte Laufgeschwindigkeit
    private String selectedwalking;
    private boolean renderpasswortaendern;
    private boolean renderpasswortinput;
    
    //Konstruktor
    public myAccountBean(){
        
        this.renderpasswortaendern=true;
        this.renderpasswortinput=false;
    }
    
    public List<String> getLanguage() {
        return language;
    }
    
    public internationalButton getInternationalButton() {
        return internationalButton;
    }

    public void setInternationalButton(internationalButton internationalButton) {
        this.internationalButton = internationalButton;
    }

    public void setLanguage(List<String> language) {
        this.language = language;
    }
    
    public List<String> getWalking() {
        return walking;
    }

    public List<String> getInterests() {
        return interests;
    }

    public void setInterests(List<String> interests) {
        this.interests = interests;
    }

    public void setWalking(List<String> walking) {
        this.walking = walking;
    }
    

    public String getSelectedwalking() {
        return selectedwalking;
    }

    public void setSelectedwalking(String selectedwalking) {
        this.selectedwalking = selectedwalking;
    }

    public boolean isRenderpasswortinput() {
        return renderpasswortinput;
    }

    public void setRenderpasswortinput(boolean renderpasswortinput) {
        this.renderpasswortinput = renderpasswortinput;
    }

    public boolean isRenderpasswortaendern() {
        return renderpasswortaendern;
    }

    public void setRenderpasswortaendern(boolean renderpasswortaendern) {
        this.renderpasswortaendern = renderpasswortaendern;
    }

    public loginBean getLoginBean() {
        return loginBean;
    }

    public void setLoginBean(loginBean loginBean) {
        this.loginBean = loginBean;
    }
    
    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

   
    
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLanguage(String language) {
        //nix setzen, erfolgt via Konstruktor
    }

    public boolean isWheelchair() {
        return wheelchair;
    }

    public void setWheelchair(boolean wheelchair) {
        this.wheelchair = wheelchair;
    }
    
    //Drowp Down Interesse füllen je nach Sprache 
    public void insertInterest(){
        Locale l = internationalButton.getLocale();
        FacesContext.getCurrentInstance().getViewRoot().setLocale(l);
        ResourceBundle rb = ResourceBundle.getBundle("org.wanderWiki.language/messages",FacesContext.getCurrentInstance().getViewRoot().getLocale());
            interests= new ArrayList();
            interests.add(rb.getString("keinInteresse"));
            interests.add(rb.getString("geographisch"));
            interests.add(rb.getString("historisch"));
            interests.add(rb.getString("kulturell"));
            interests.add(rb.getString("religiös"));
            this.wheelchair=false;
            language=new ArrayList();
            language.add("de");
            language.add("en");
    }
        
    //Drowp Down Laufgeschwindigkeit füllen je nach Sprache 
    public void walkingSpeed(){
        Locale l = internationalButton.getLocale();
        FacesContext.getCurrentInstance().getViewRoot().setLocale(l);
        ResourceBundle rb = ResourceBundle.getBundle("org.wanderWiki.language/messages",FacesContext.getCurrentInstance().getViewRoot().getLocale());
        walking = new ArrayList();
        walking.add(rb.getString("langsam"));
        walking.add(rb.getString("normal"));
        walking.add(rb.getString("schnell"));
    }    
    
    public void loadContent(){
        walkingSpeed();
        insertInterest();
    }

    //wenn Locale englisch -> deutsche Persisitierung
    public void updateUser(){
        FacesContext ctx = FacesContext.getCurrentInstance();
        ResourceBundle rb = ResourceBundle.getBundle("org.wanderWiki.language/messages", ctx.getViewRoot().getLocale());
        System.out.println("Locale:.:::" + ctx.getViewRoot().getLocale());
        UsersDAOJPA udj = new UsersDAOJPA();
        if(loginBean.getLoggedinUser().getInterest().equals("no interest")){
            loginBean.getLoggedinUser().setInterest("kein Interesse");
        }else{
            if(loginBean.getLoggedinUser().getInterest().equals("geographical")){
                loginBean.getLoggedinUser().setInterest("geographisch");
            }else{
                if(loginBean.getLoggedinUser().getInterest().equals("historical")){
                    loginBean.getLoggedinUser().setInterest("historisch");
                }else{
                    if(loginBean.getLoggedinUser().getInterest().equals("cultural")){
                        loginBean.getLoggedinUser().setInterest("kulturell");
                    }else{
                        if(loginBean.getLoggedinUser().getInterest().equals("religious")){
                            loginBean.getLoggedinUser().setInterest("religiös");
                        }
                    }
                }
            }
        }
        
        if(loginBean.getLoggedinUser().getWalkingSpeed().equals("slow")){
            loginBean.getLoggedinUser().setWalkingSpeed("langsam");
        }else{
            if(loginBean.getLoggedinUser().getWalkingSpeed().equals("normal")){
                loginBean.getLoggedinUser().setWalkingSpeed("normal");
            }else{
                 if(loginBean.getLoggedinUser().getWalkingSpeed().equals("fast")){
                    loginBean.getLoggedinUser().setWalkingSpeed("schnell");
                 }
            }
        }
        
        //Update der Daten des eingeloggten Users
        udj.update(loginBean.getLoggedinUser());
        
        ctx.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, rb.getString("datenchanged"), null));
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('myAccount').hide()");
    }
    
    //Passwort wechseln, anzeigen seperater Komponenten
    public void changePassword(){
        this.renderpasswortaendern=false;
        this.renderpasswortinput=true;
    }
    
    //Abbrechen
    public void abbort(){
        this.renderpasswortaendern=true;
        this.renderpasswortinput=false;
    }
    
    //Dialog schließen
    public void closeDialog(){
       FacesContext c = FacesContext.getCurrentInstance();
        
        if(internationalButton.getLoc().equals("de")){
            c.getViewRoot().setLocale(Locale.GERMAN);    
            ResourceBundle rb = ResourceBundle.getBundle("org.wanderWiki.language/messages", c.getViewRoot().getLocale());
        }else{
            if(internationalButton.getLoc().equals("en")){
                c.getViewRoot().setLocale(Locale.ENGLISH);    
                ResourceBundle rb = ResourceBundle.getBundle("org.wanderWiki.language/messages", c.getViewRoot().getLocale());
            }
        }
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('myAccount').hide()");
    }
    
    public void changeNewPassword(){
        loginBean.getLoggedinUser().setPasswordHash(pass);
        this.renderpasswortaendern=true;
        this.renderpasswortinput=false;
    }
    
    
   
    
    
    
}
