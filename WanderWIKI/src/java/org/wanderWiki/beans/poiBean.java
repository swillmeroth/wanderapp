/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.wanderWiki.beans;

import com.googlecode.gmaps4jsf.component.common.Position;
import com.googlecode.gmaps4jsf.component.marker.Marker;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ValueChangeEvent;
import org.primefaces.context.RequestContext;
import org.wanderWiki.entities.Descriptions;
import org.wanderWiki.entities.Places;
import org.wanderWiki.jpa.PlacesFacade;

/**
 * Diese Bean ist für die verwaltung für POIs zuständig. Besonder wegen dem 
 * erstellen solcher.
 * 
 * 
 * @author Andre Claas
 */
@ManagedBean
@SessionScoped
public class poiBean {

    private int ID;
    private String name;
    private String type;
    private double longitude;
    private double latitude;
    private int altitude;
    private String plz;
    private String city;
    private String street;
    private List<String> moegliche_type;

    private PlacesFacade dao;

    private List<Places> liste;

    private Descriptions beschreibung;
    private List<Descriptions> beschreibung_liste;

    public poiBean() {
        dao = new PlacesFacade();
        liste = new ArrayList();
        this.moegliche_type = new ArrayList<>();

        this.moegliche_type.add("abbey");
        this.moegliche_type.add("bouddha");
        this.moegliche_type.add("cemetary");
        this.moegliche_type.add("chapel");
        this.moegliche_type.add("church");
        this.moegliche_type.add("convent");
        this.moegliche_type.add("cross");

        this.moegliche_type.add("graveyard");
        this.moegliche_type.add("headstone");
        this.moegliche_type.add("japanese_temple");
        this.moegliche_type.add("jewish_graveyard");
        this.moegliche_type.add("monument");
        this.moegliche_type.add("statue");

        beschreibung = new Descriptions();
        beschreibung_liste = new ArrayList<>();
    }

    /**
     * @return the dao
     */
    public PlacesFacade getDao() {
        return dao;
    }

    /**
     * @param aDao the dao to set
     */
    public void setDao(PlacesFacade aDao) {
        dao = aDao;
    }

    public List getListe() {
        return liste;
    }

    public void setListe(List liste) {
        this.liste = liste;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public int getAltitude() {
        return altitude;
    }

    public void setAltitude(int altitude) {
        this.altitude = altitude;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public void loadData() {
        System.out.println("wurde geklick");
        liste = getDao().findAll();
        setStreet(liste.get(0).getStreet());
        //Liste erstellen

    }

    /**
     * @return the plz
     */
    public String getPlz() {
        return plz;
    }

    /**
     * @param plz the plz to set
     */
    public void setPlz(String plz) {
        this.plz = plz;
    }

    /**
     * Hauptaufgabe dieser Funktion ist die ordnungsgemäßge Persistierung eines 
     * Places. Hier werden die in den instanzvariablen gespeicherten Werte in 
     * einen neuen Place kopiert. Die Beschreibungen aus beschreibung_liste 
     * werden diesem neuen POI hinzugefügt. Anschließend wird der POI an die 
     * Datenbank übermittelt und persistiert. Abschließend wird alles 
     * zurückgesetzt, damit ein neuer POI angelegt werden kann
     * @author Claas
     */
    public void persistiere() {
        FacesContext cxt = FacesContext.getCurrentInstance();
        Places ort = new Places();
        ArrayList<Descriptions> neue_besch = new ArrayList<>();
        for (Descriptions beschreibung : beschreibung_liste) {
            beschreibung.setIdPlace(ort);
            neue_besch.add(beschreibung);
        }

        ort.setDateAdded(new Date());
        ort.setName(name);
        ort.setCity(city);
        ort.setAltitude(altitude);
        ort.setLatitude(latitude);
        ort.setLongitude(longitude);
        ort.setPostalCode(getPlz());
        ort.setStreet(street);
        ort.setType(type);

        ort.setDescriptionsList(neue_besch);

//        TODO: i18n
        this.dao.create(ort);
        FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, "", this.getName() + " wurde als Poi eingetragen");
        cxt.addMessage(null, fm);
        this.setName("");
        this.setStreet("");
        this.setCity("");
        this.setStreet("");
//        this.setAltitude(0);
//        this.setLatitude(0);
//        this.setLongitude(0);
        this.setPlz("");
        this.setType("");
        beschreibung_liste = new ArrayList<>();
        beschreibung = new Descriptions();

    }

    /**
     * Diese Methode ruf anhand der bereits gesetzten ID die restlichten Daten
     * zu dem POI ab.
     *
     * @author Claas
     */
    public void lade_user() {
        try {
            Places geladen = this.dao.find(this.ID);
            name = geladen.getName();
            city = geladen.getCity();
            altitude = geladen.getAltitude();
            latitude = geladen.getLatitude();
            longitude = geladen.getLongitude();
            plz = geladen.getPostalCode();
            street = geladen.getStreet();
            type = geladen.getType();
        } catch (Exception e) {
        }

    }

    @Override
    public String toString() {
        return "poiBean: id:" + this.ID + " name:" + this.name + " lat: " + latitude + " long: " + longitude;
    }

    /**
     * Diese Funktion ist ein Listener für den Marker auf der GoogleMap. Jedes
     * mal wenn dieser an eine neue Stelle bewegt wird löst es dieses Funktion
     * aus. Das event beinhaltet die neue Position des Marker. Diese wird 
     * ausgelesen und in die Variablen geschrieben.
     * 
     * @param event Wenn der Marker an eine neue Position bewegt wird
     * @throws AbortProcessingException
     * @author Claas
     */
    public void legePunktFest(ValueChangeEvent event) throws AbortProcessingException {
        Position value = (Position) event.getNewValue();

        Marker source = (Marker) event.getSource();
        String markerID = source.getJsVariable();
        this.setLatitude(Double.parseDouble(value.getLatitude()));
        this.setLongitude(Double.parseDouble(value.getLongitude()));
        String message = markerID + " is moved to the following location: " + value.toString();

        System.out.println(message);
        System.out.println("lat:" + this.latitude + " long:" + this.longitude);

    }

    /**
     * Geplante Funktion die anhand einer GPS Koordinate und eines Radius die
     * relevante POI zurückliefert.
     * @param gps_position
     * @param radius_in_meter
     * @deprecated noch nicht implementiert
     */
    public void gib_punkte_im_umkreis(String gps_position, int radius_in_meter) {

//         TODO: diese funktion soll alle POIS in einem bestimmn umkreis heraussuche.
    }

    /**
     * @return the moegliche_type
     */
    public List<String> getMoegliche_type() {
        return moegliche_type;
    }

    /**
     * @param moegliche_type the moegliche_type to set
     */
    public void setMoegliche_type(List<String> moegliche_type) {
        this.moegliche_type = moegliche_type;
    }

    /**
     * @return the beschreibung
     */
    public Descriptions getBeschreibung() {
        return beschreibung;
    }

    /**
     * @param beschreibung the beschreibung to set
     */
    public void setBeschreibung(Descriptions beschreibung) {
        this.beschreibung = beschreibung;
    }

    /**
     * @return the beschreibung_liste
     */
    public List<Descriptions> getBeschreibung_liste() {
        return beschreibung_liste;
    }

    /**
     * @param beschreibung_liste the beschreibung_liste to set
     */
    public void setBeschreibung_liste(List<Descriptions> beschreibung_liste) {
        this.beschreibung_liste = beschreibung_liste;
    }

    /**
     * Diese Funktion fügt eine Beschreibung hinzu und erstellt eine neue.
     * 
     * @deprecated wird wohl nicht mehr verwendet
     * @author Claas
     */
    public void erstelle_neue_beschreibung() {

        beschreibung_liste.add(beschreibung);
        beschreibung = new Descriptions();

    }

    public void leere_neue_beschreibung() {

        beschreibung = new Descriptions();

    }

    /**
     * Diese Funktion wir beim "hinzufügen ausgeführt".
     * Der Text wird überprüft ob er leer ist.
     * Wenn dem nicht so ist wird die neue Beschreibung zu beschreibung_liste
     * hinzugefügt und beschreibung wird geleert.
     * 
     * @author Claas
     */
    public void speicher_beschreibung_in_liste() {
        System.out.println("Beschreibung soll gespeichert werden");
        if ("".equals(beschreibung.getText())) {
            FacesContext cxt = FacesContext.getCurrentInstance();
            FacesMessage fm = new FacesMessage();
            fm.setSeverity(FacesMessage.SEVERITY_ERROR);
            fm.setSummary("Bitte Text eingeben");

            cxt.addMessage("poi_hinzu_wizard:text", fm);
            cxt.validationFailed();

            return;
        }
        this.beschreibung_liste.add(beschreibung);
        this.beschreibung = new Descriptions();
        RequestContext context = RequestContext.getCurrentInstance();
        ArrayList<String> update = new ArrayList<>();
        update.add(":poi_hinzu_wizard:text");

        context.update(update);
    }

}
