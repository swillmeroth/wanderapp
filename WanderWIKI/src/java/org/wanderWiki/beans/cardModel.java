/*  Bean zur Ermittlung von Geodaten zu einem eingegebebn Punkt
    und Anzeige der Karte via gmaps4JSF
    erstellt: Andre Hantke
*/
package org.wanderWiki.beans;

import com.google.code.geocoder.Geocoder;
import com.google.code.geocoder.GeocoderRequestBuilder;
import com.google.code.geocoder.model.GeocodeResponse;
import com.google.code.geocoder.model.GeocoderRequest;
import com.google.code.geocoder.model.GeocoderResult;
import com.google.code.geocoder.model.GeocoderStatus;
import com.googlecode.gmaps4jsf.component.common.Position;
import com.googlecode.gmaps4jsf.component.map.Map;
import com.googlecode.gmaps4jsf.component.marker.Marker;
import java.io.IOException;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ValueChangeEvent;

@ManagedBean
@SessionScoped
@RequestScoped
public class cardModel {
    
    private String Stadt;
    private double latitude;
    private double longitude;
    private int zoom;
    
    private Map map;   
    
    public cardModel(){
       
        this.Stadt="Münster";
        this.latitude=51.960453;
        this.longitude=7.625470;
        
        
    }
    
    public void gib_alle_marker(){
        
     
              
        for (UIComponent kind : map.getChildren()) {
            Marker marker = (Marker) kind;
            System.out.println("marker: "+marker.getLatitude());
        }

        
        
    }
    
     public Map getMap() {
        return map;
    }

    //setzen der Defaultwerte für die Karte
    //hier Münster 
    public void setMap(Map map) {
        this.map = map;
        map.setLatitude("51.960453");
        map.setLongitude("7.625470");
    }

    public String getStadt() {
        return Stadt;
    }

    public void setStadt(String Stadt) {
        this.Stadt = Stadt;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public int getZoom() {
        return zoom;
    }

    public void setZoom(int zoom) {
        this.zoom = zoom;
    }
   

    //Lade Karte entsprechend der eingabe in ein Input Feld
    //@autor: Andre Hantke
    //der eingegebene Wert in ein Inputfeld
    //wird hier mit Hilfe des Geocoders in Latitude und Longitude umgewandelt
    //anschließend kann die Karte aktualisiert werden
    //der Zoom kann gesetzt werden und 
    //der Kartenausschnitt kann aktualisiert werden
    public void loadCard(){             
        Geocoder geocoder = new Geocoder();  
        //Request
        GeocoderRequest geocoderRequest = new GeocoderRequestBuilder()
                                                .setAddress(this.Stadt) // location
                                                .setLanguage("de") // language
                                                .getGeocoderRequest();
        //response
        GeocodeResponse geocoderResponse;
        //Response Auflösung
        try {
            geocoderResponse = geocoder.geocode(geocoderRequest);
            if (geocoderResponse.getStatus() == GeocoderStatus.OK
              & !geocoderResponse.getResults().isEmpty()) {
              GeocoderResult geocoderResult = 
                geocoderResponse.getResults().iterator().next();
                com.google.code.geocoder.model.LatLng latitudeLongitude =
                geocoderResult.getGeometry().getLocation();
              Float[] coords = new Float[2];
              this.latitude = latitudeLongitude.getLat().doubleValue();
              this.setLatitude(latitudeLongitude.getLat().doubleValue());
              this.longitude = latitudeLongitude.getLng().doubleValue();
              this.setLongitude(latitudeLongitude.getLng().doubleValue());
              this.setZoom(10);
        }
     } catch (IOException ex) {
       ex.printStackTrace();
     }
        String lati = new Double(this.latitude).toString();
        String longi = new Double(this.longitude).toString();
        
        //setzen des Models der Karte
        map.setLatitude(lati);
        map.setLongitude(longi);
        map.setZoom("13");
       
        //Setzen des Markers
        Marker m1 = new Marker();
        m1.setDraggable("true");
        m1.setLatitude(lati);
        m1.setLongitude(longi);
        
        //clear Karte und füge die Kinder neu hinzu
        map.getChildren().clear();
        map.getChildren().add(m1);
        
//        RequestContext ctx = RequestContext.getCurrentInstance();
//        ctx.update("contentform:karte");
        //Ausgabe
        System.out.println("(Stadt: " + this.Stadt + "Lat: " + this.latitude + "Lon: " + this.longitude);
    }
    
    public void legePunktFest(ValueChangeEvent event)throws AbortProcessingException {
        Position value = (Position) event.getNewValue();
    	
        Marker source = (Marker) event.getSource();
        String markerID = source.getJsVariable();
        
        String message = markerID + " is moved to the following location: " + value.toString();
        
        System.out.println(message);
        System.out.println("GNAMPF");
        System.out.println("lat:"+this.latitude+" long:"+this.longitude);  
        
    }
}
