/*******************************************************
* WanderApp
* erstellt am 01.08.2014
* Menubar Bean
* liefert die entsprechende Seite zurück und dient der Navigation
* Verantwortlich: Andre Hantke
* Version 1.0
********************************************************/

package org.wanderWiki.beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean
@SessionScoped
public class navigationBean {
    
    
    //@autor : Andre Hantke
    //liefert die Startseite zurück
    public String goToStartseite(){
        return "/index.xhtml";
    }
    
    
//    nicht genutzte Struktur!
//    public String goToWiki(){
//        return "/wiki.xhtml";
//    }
    
//    public String goToRoutenplanung(){
//        return "/routenplanung.xhtml";
//    }
    
//    public String goToKarte(){
//        return "/karte.xhtml";
//    }
    
    //@autor : Andre Hantke
    //liefert die Hilfeseite zurück
    public String goToHilfe(){
        return "/hilfe.xhtml";
    }
    
    
    
    
}
