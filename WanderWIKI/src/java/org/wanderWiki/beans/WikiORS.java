/*
 * Bean zur Ermiitlung von Routeninformatione via ORS
 * Erstellt am: 20.04.2015, 
 * verantwortlich: Andre Hantke
 */
package org.wanderWiki.beans;

import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import org.primefaces.context.RequestContext;
import org.wanderWiki.entities.Routepoints;
import org.wanderWiki.ors.GeoPoint;
import org.wanderWiki.ors.Instruction;
import org.wanderWiki.ors.OpenRouteService;
import org.wanderWiki.ors.WayPoint;
import org.wanderWiki.ors.WayPoint.PointType;
import org.xml.sax.SAXException;

/**
 *
 * @author Andre
 */
@ManagedBean
@SessionScoped
public class WikiORS {
    
    //um an die Routeninformationen zu kommen
    @ManagedProperty(value="#{routeBean}")
    private routeBean routeBean;
    
    //um an die aktuelle Einstellung -> Internationalisierung zu kommen
    @ManagedProperty(value="#{internationalButton}")
    private internationalButton internationalButton;

    //Dauer
    private int duration;
    //Distanz
    private double distance;
    //Wegpunkte
    private List<WayPoint> punkte;
    //Geopunkte
    private List<GeoPoint> geoPoints;
    //Instruktionen
    private List<Instruction> instructions;
    //Ausgabe der Distan Localabängig!!
    private String ausgabeentfernunginkm;
    private String ausgabeentfernunginmeilen;
    private String ausgabe;

    //Konstruktor
    public WikiORS(){
    }
    
    //setter + getter
    public String getAusgabe() {
        return ausgabe;
    }

    public void setAusgabe(String ausgabe) {
        this.ausgabe = ausgabe;
    }

    public String getAusgabeentfernunginkm() {
        return ausgabeentfernunginkm;
    }

    public void setAusgabeentfernunginkm(String ausgabeentfernunginkm) {
        this.ausgabeentfernunginkm = ausgabeentfernunginkm;
    }

    public String getAusgabeentfernunginmeilen() {
        return ausgabeentfernunginmeilen;
    }

    public void setAusgabeentfernunginmeilen(String ausgabeentfernunginmeilen) {
        this.ausgabeentfernunginmeilen = ausgabeentfernunginmeilen;
    }
   
    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public internationalButton getInternationalButton() {
        return internationalButton;
    }

    public void setInternationalButton(internationalButton internationalButton) {
        this.internationalButton = internationalButton;
    }

    public List<Instruction> getInstructions() {
        return instructions;
    }

    public void setInstructions(List<Instruction> instructions) {
        this.instructions = instructions;
    }
   
    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public List<GeoPoint> getGeoPoints() {
        return geoPoints;
    }

    public void setGeoPoints(List<GeoPoint> geoPoints) {
        this.geoPoints = geoPoints;
    }

    public List<WayPoint> getPunkte() {
        return punkte;
    }

    public void setPunkte(List<WayPoint> punkte) {
        this.punkte = punkte;
    }
    
    public routeBean getRouteBean() {
        return routeBean;
    }

    public void setRouteBean(routeBean routeBean) {
        this.routeBean = routeBean;
    } 
    
    
    //Ausgabe aller gesetzten Routenpunkte in der Karte
    public void routenAusgabe(){
        List<Routepoints> l = new ArrayList();
        l.addAll(routeBean.getRoute().getRoutepointsList());
        
        for(int i=0; i<l.size();i++){
            System.out.println(l.get(i).getIdRoute());
            System.out.println("Lati: " + l.get(i).getLatitude());
            System.out.println("Longi: " +l.get(i).getLongitude());
        }
    }
    
    
    //Methode zur Ermittlung von Routeninformationen via ORS
    public void routenInfosviaORSService(){
        //Steurung von Komponenten aus der Bean
        RequestContext context = RequestContext.getCurrentInstance();
        
        //Geopunkte & Wegpunkte
        geoPoints = new ArrayList();
        punkte = new ArrayList();
        
        //Liste der gesetzten Punkte in der Karte
        List<Routepoints> l = new ArrayList();
        l.addAll(routeBean.getRoute().getRoutepointsList());
        
        //Ausgabe der gesetzten Punkte
        for(int i=0; i<l.size();i++){
            System.out.println(l.get(i).getIdRoute());
            System.out.println("Lati: " + l.get(i).getLatitude());
            System.out.println("Longi: " +l.get(i).getLongitude());
        }
        
        
        GeoPoint g;
        WayPoint w;
        try {
            //Aufruf des Services
            OpenRouteService ors = new OpenRouteService();
            
            //Setzen von Geopunkten Longitude / Latitude
            for(int i=0; i< l.size(); i++){
                g=new GeoPoint();
                g.setLongitude((l.get(i).getLongitude()));
                g.setLatitude(l.get(i).getLatitude());
                this.geoPoints.add(g);
                //setzen des Wegpunktes, (order ID, GeoPoint, Type)
                w=new WayPoint(i,g,PointType.WAY_POINT);
                //hinzufügen zur Liste der Wegpunkte
                this.punkte.add(w);
            }
            //direkter Aufruf des ORS Services
            //@Param Wegpunkte, Type der Fortbewegung,  Locale
                //DropDown Menü erstellen!
                //value=1 : fastest
                //value=2 : pedestrian
                //value=3 : BicycleSafety
                //value=4 : Bicycle
                //value=5 : BicycleRacer
                //de:deutsch, it:italienisch
            ors.TestOSR(punkte, 2, internationalButton.getLoc());
            System.out.println(ors.getTotalDistance());
           
            //Setzen von Dauer/Distanz und Instruktionen
            this.setDuration(ors.getTotalTime());
            this.setDistance(ors.getTotalDistance());
            this.instructions=ors.getInstructions();
        }catch (IOException | SAXException ex) {
            Logger.getLogger(WikiORS.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        // routeBean.getRoute().setDuration(this.duration);
        //Umrechnung je nach Locale!
        this.ausgabeentfernunginkm=this.distance + " km";
        NumberFormat n = NumberFormat.getInstance();
        n.setMaximumFractionDigits(2);
        this.ausgabeentfernunginmeilen=n.format(this.distance*0.621371192) + " miles";
        
        //Ausgabe
        if(internationalButton.getLoc().equals("de")){
              this.ausgabe=this.ausgabeentfernunginkm;
          }else{
              if(internationalButton.getLoc().equals("en")){
                this.ausgabe=this.ausgabeentfernunginmeilen;
              }
        }
        //Update der Komponente!      
        context.update("route_hinzu_wizard:resultORS");
        }
    

}
