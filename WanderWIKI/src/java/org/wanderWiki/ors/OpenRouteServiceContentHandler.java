// dient dem ORS Service seitens Uni Heidelberg
// der im Package aufgeführten Klassen
// sind im groben und ganzen die aus der uns zur Verfügung gestellten
// Bachelorarbeit mit leichten Modifikationen
// verantwortlich: Andre Hantke

package org.wanderWiki.ors;

import java.io.Serializable;
import org.wanderWiki.ors.GeoPoint;
import org.wanderWiki.ors.WayPoint;
import org.wanderWiki.ors.WayPoint.PointType;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;

/**
 * Liefert den Inhalt des von OpenRouteService erhaltenen XML-Dokuments.
 */
public class OpenRouteServiceContentHandler implements ContentHandler{

    /* ====================================================================== */
    /*                       Tags                                             */
    /* ====================================================================== */
    /** Tag für die Gesamtzeit. */
    public static final String TAG_TIME = "TotalTime";
    /** Tag für die Gesamtdistanz. */
    public static final String TAG_TOTAL_DISTANCE = "TotalDistance";
    /** Tag für die Wegpunkte. */
    public static final String TAG_POINTS = "RouteGeometry";
    /** Tag für einen Wegpunkt. */
    public static final String TAG_POINT = "pos";
    /** Tag für die Wegbeschreibung. */
    public static final String TAG_ROUTE_INSTRUCTIONS = "RouteInstructionsList";
    /** Tag für einen Teil der Wegbeschreibung */
    public static final String TAG_ROUTE_INSTRUCTION = "RouteInstruction";
    /** Tag für die Anweisung eines Teils der Wegbeschreibung */
    public static final String TAG_INSTRUCTION = "Instruction";
    /** Tag für die Länge einer Strecke */
    public static final String TAG_DISTANCE = "distance";
    /* ====================================================================== */
    /*                       Attribute                                        */
    /* ====================================================================== */
    /** Attribut für Einheit der Distanz. */
    private static final String ATTRIBUTE_UOM = "uom";
    /** Attribut für die Distanz. */
    private static final String ATTRIBUTE_VALUE = "value";
    /** Attribut für die Sprache. */
    private static final String ATTRIBUTE_LANGUAGE = "xls:lang";
    /** Attribut für die zeitliche Dauer. */
    private static final String ATTRIBUTE_DURATION = "duration";
    /** Attribut für die Beschreibung der Wegbschreibung. */
    private static final String ATTRIBUTE_DESCRIPTION = "description";
    /* ====================================================================== */
    /*                       Zeichen                                          */
    /* ====================================================================== */
    /** Zeichen für alle in der Zeit vorkommenden Buchstaben. */
    private static final String STRING_TIME = "PDTHMS";
    /** Zeichen für Tage. */
    private static final String CHAR_DAYS = "D";
    /** Zeichen für Stunden. */
    private static final String CHAR_HOURS = "H";
    /** Zeichen für Minuten. */
    private static final String CHAR_MINUTES = "M";
    /** Zeichen für Sekunden. */
    private static final String CHAR_SECONDS = "S";
    /* ====================================================================== */
    /*                       Instanzvariable                                  */
    /* ====================================================================== */
    /** altueller Tag-Inhalt */
    private String value;
    /** Benötigte Gesamtzeit der Route in Minuten. */
    private int totalTime;
    /** Gesamtstrecke der Route. */
    private double totalDistance;
    /** Wegpunkte die Erreicht werden */
    private List<WayPoint> wayPoints;
    /** Anweisunge der Route. */
    private List<Instruction> instructions;
    /** Sprache der Anweisungen. */
    private String language;
    /** Aktuelle Instruktion. */
    private Instruction currentInstruction;

    /* ====================================================================== */
    /*                       Konstruktor                                      */
    /* ====================================================================== */
    /** 
     * Erzeugt einen neune Content Handler 
     */
    public OpenRouteServiceContentHandler() {
        this.value = "";
    }

    /* ====================================================================== */
    /*                       geerbte Methoden                                 */
    /* ====================================================================== */
    @Override
    public void setDocumentLocator(Locator locator) {
        /* wird für diese Zwecke nicht benötigt. */
    }

    @Override
    public void startDocument() throws SAXException {
        this.wayPoints = null;
        this.instructions = null;
    }

    @Override
    public void endDocument() throws SAXException {
        /* wird für diese Zwecke nicht benötigt. */
    }

    @Override
    public void startPrefixMapping(String prefix, String uri)
            throws SAXException {
        /* wird für diese Zwecke nicht benötigt. */
    }

    @Override
    public void endPrefixMapping(String prefix) throws SAXException {
        /* wird für diese Zwecke nicht benötigt. */
    }

    /**
     * Aufruf bei Anfangs-Tag.
     * @param uri
     * @param localName Tag-Name
     * @param qName
     * @param atts Attribute
     * @throws SAXException 
     */
    @Override
    public void startElement(String uri, String localName, String qName,
            Attributes atts) throws SAXException {

        if (localName.equals(TAG_TOTAL_DISTANCE)) {

            this.totalDistance = Double.valueOf(atts.getValue(ATTRIBUTE_VALUE));

        } else if (localName.equals(TAG_POINTS)) {

            this.wayPoints = new ArrayList<WayPoint>();

        } else if (localName.equals(TAG_ROUTE_INSTRUCTIONS)) {

            this.instructions = new ArrayList<Instruction>();
            this.language = atts.getValue(ATTRIBUTE_LANGUAGE);

        } else if (localName.equals(TAG_ROUTE_INSTRUCTION)) {

            this.currentInstruction = new Instruction();
            this.currentInstruction.setDuration(
                    parseDuration(atts.getValue(ATTRIBUTE_DURATION)));
            this.currentInstruction.setDescription(
                    atts.getValue(ATTRIBUTE_DESCRIPTION));

        } else if (localName.equals(TAG_DISTANCE)) {

            this.currentInstruction.setDistance(Double.valueOf(
                    atts.getValue(ATTRIBUTE_VALUE)));

        }
    }

    /**
     * Aufruf bei Tag-Ende.
     * @param uri
     * @param localName Name des Tags.
     * @param qName
     * @throws SAXException 
     */
    @Override
    public void endElement(String uri, String localName, String qName)
            throws SAXException {

        if (localName.equals(TAG_TIME)) {

            this.totalTime = parseDuration(this.value);
            this.value = "";

        } else if (this.wayPoints != null && localName.equals(TAG_POINT)) {

            String[] coordinates = this.value.split(" ");

            if (coordinates.length >= 2) {

                GeoPoint geoPoint = new GeoPoint(Double.valueOf(coordinates[1]),
                        Double.valueOf(coordinates[0]), 0);
                
                addWayPoint(new WayPoint(this.wayPoints.size() + 1, geoPoint, 
                        PointType.WAY_POINT));
            }

            this.value = "";

        } else if (localName.equals(TAG_INSTRUCTION)) {

            this.currentInstruction.setInstruction(this.value);
            this.value = "";

        } else if (localName.equals(TAG_ROUTE_INSTRUCTION)) {

            this.instructions.add(this.currentInstruction);

        } else {

            this.value = "";
        }
    }

    /**
     * Fügt eine Wegpunkt der Wegpunktliste hinzu, sofern er nicht mit dem 
     * letzten Punkt übereinstimmt.
     * @param wayPoint Wegpunkt
     */
    private void addWayPoint(WayPoint wayPoint) {
        
        if(this.wayPoints.isEmpty() 
                || !this.wayPoints.get(this.wayPoints.size() - 1).equals(wayPoint)) {
            this.wayPoints.add(wayPoint);
        }
    }
    
    /**
     * Verarbeitung der einzelnen Zeichen.
     * @param ch Zeichen
     * @param start Anfang
     * @param length Länge
     * @throws SAXException 
     */
    @Override
    public void characters(char[] ch, int start, int length)
            throws SAXException {

        this.value = this.value + new String(ch, start, length).trim();
    }

    @Override
    public void ignorableWhitespace(char[] ch, int start, int length)
            throws SAXException {
        /* wird für diese Zwecke nicht benötigt. */
    }

    @Override
    public void processingInstruction(String target, String data)
            throws SAXException {
        /* wird für diese Zwecke nicht benötigt. */
    }

    @Override
    public void skippedEntity(String name) throws SAXException {
        /* wird für diese Zwecke nicht benötigt. */
    }

    /* ====================================================================== */
    /*                       Hilfsethoden                                     */
    /* ====================================================================== */
    /**
     * Parst die erhaltene Zeit in Minuten.
     * @param time Zeit
     * @return Zeit in Minuten
     */
    private int parseDuration(String time) {

        int minutes = 0;
        StringTokenizer timeTokenizer = new StringTokenizer(time, STRING_TIME);

        if (time.contains(CHAR_DAYS) && timeTokenizer.hasMoreTokens()) {
            minutes += Integer.valueOf(timeTokenizer.nextToken()) * 60 * 60;
        }
        if (time.contains(CHAR_HOURS) && timeTokenizer.hasMoreTokens()) {
            minutes += Integer.valueOf(timeTokenizer.nextToken()) * 60;
        }
        if (time.contains(CHAR_MINUTES) && timeTokenizer.hasMoreTokens()) {
            minutes += Integer.valueOf(timeTokenizer.nextToken());
        }
        if (time.contains(CHAR_SECONDS) && timeTokenizer.hasMoreTokens()) {
            /* Sekunden werden nicht mit berechnet. */
        }

        return minutes;
    }

    /* ====================================================================== */
    /*                       Getter-Methoden                                  */
    /* ====================================================================== */
    /**
     * Gibt die Gesamtstrecke der Route.
     * @return Gesamtstrecke
     */
    public double getTotalDistance() {
        return totalDistance;
    }

    /**
     * Gibt die Dauer der Route in Minuten.
     * @return Dauer
     */
    public int getTotalTime() {
        return totalTime;
    }

    /**
     * Gibt die Wegpunkte der Route.
     * @return Wegpunkte
     */
    public List<WayPoint> getWayPoints() {
        return wayPoints;
    }

    /**
     * Gibt die Sprache der Wegbeschreibung.
     * @return Sprache der Wegbeschreibung
     */
    public String getLanguage() {
        return language;
    }

    /**
     * Gibt die Wegbeschreibung der Route.
     * @return Wegbeschreibung
     */
    public List<Instruction> getInstructions() {
        return instructions;
    }
}
