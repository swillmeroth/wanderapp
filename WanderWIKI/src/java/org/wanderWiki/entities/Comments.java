/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.wanderWiki.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Stephan
 */
@Entity
@Table(name = "comments")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Comments.findAll", query = "SELECT c FROM Comments c"),
    @NamedQuery(name = "Comments.findByIdComments", query = "SELECT c FROM Comments c WHERE c.idComments = :idComments"),
    @NamedQuery(name = "Comments.findByRating", query = "SELECT c FROM Comments c WHERE c.rating = :rating"),
    @NamedQuery(name = "Comments.findByTime", query = "SELECT c FROM Comments c WHERE c.time = :time")})
public class Comments implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idComments")
    private Integer idComments;
    @Column(name = "rating")
    @Min(0) @Max(5)
    private long rating;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "text")
    private String text;
    @Column(name = "time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date time;
    @JoinColumn(name = "idPlace", referencedColumnName = "idPlaces")
    @ManyToOne
    private Places idPlace;
    @JoinColumn(name = "idRoute", referencedColumnName = "idRoutes")
    @ManyToOne
    private Routes idRoute;
    @JoinColumn(name = "idUser", referencedColumnName = "userid")
    @ManyToOne
    private Users idUser;

    public Comments() {
        this.time = new Date();
    }

    public Comments(Integer idComments) {
        this.idComments = idComments;
    }

    public Integer getIdComments() {
        return idComments;
    }

    public void setIdComments(Integer idComments) {
        this.idComments = idComments;
    }

    public long getRating() {
        return rating;
    }

    public void setRating(long rating) {
        this.rating = rating;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Places getIdPlace() {
        return idPlace;
    }

    public void setIdPlace(Places idPlace) {
        this.idPlace = idPlace;
    }

    public Routes getIdRoute() {
        return idRoute;
    }

    public void setIdRoute(Routes idRoute) {
        this.idRoute = idRoute;
    }

    public Users getIdUser() {
        return idUser;
    }

    public void setIdUser(Users idUser) {
        this.idUser = idUser;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idComments != null ? idComments.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Comments)) {
            return false;
        }
        Comments other = (Comments) object;
        if ((this.idComments == null && other.idComments != null) || (this.idComments != null && !this.idComments.equals(other.idComments))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.wanderWiki.entities.Comments[ idComments=" + idComments + " ]";
    }
    
}
