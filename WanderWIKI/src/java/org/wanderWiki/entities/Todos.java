/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.wanderWiki.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Stephan
 */
@Entity
@Table(name = "todos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Todos.findAll", query = "SELECT t FROM Todos t"),
    @NamedQuery(name = "Todos.findByIdToDos", query = "SELECT t FROM Todos t WHERE t.idToDos = :idToDos")})
public class Todos implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idToDos")
    private Integer idToDos;
    @JoinColumn(name = "idPlace", referencedColumnName = "idPlaces")
    @ManyToOne
    private Places idPlace;
    @JoinColumn(name = "idRoute", referencedColumnName = "idRoutes")
    @ManyToOne
    private Routes idRoute;
    @JoinColumn(name = "idUser", referencedColumnName = "userid")
    @ManyToOne
    private Users idUser;

    public Todos() {
    }

    public Todos(Integer idToDos) {
        this.idToDos = idToDos;
    }

    public Integer getIdToDos() {
        return idToDos;
    }

    public void setIdToDos(Integer idToDos) {
        this.idToDos = idToDos;
    }

    public Places getIdPlace() {
        return idPlace;
    }

    public void setIdPlace(Places idPlace) {
        this.idPlace = idPlace;
    }

    public Routes getIdRoute() {
        return idRoute;
    }

    public void setIdRoute(Routes idRoute) {
        this.idRoute = idRoute;
    }

    public Users getIdUser() {
        return idUser;
    }

    public void setIdUser(Users idUser) {
        this.idUser = idUser;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idToDos != null ? idToDos.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Todos)) {
            return false;
        }
        Todos other = (Todos) object;
        if ((this.idToDos == null && other.idToDos != null) || (this.idToDos != null && !this.idToDos.equals(other.idToDos))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.wanderWiki.entities.Todos[ idToDos=" + idToDos + " ]";
    }
    
}
