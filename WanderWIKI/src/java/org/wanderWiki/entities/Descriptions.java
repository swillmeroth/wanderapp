/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.wanderWiki.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Stephan
 */
@Entity
@Table(name = "descriptions")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Descriptions.findAll", query = "SELECT d FROM Descriptions d"),
    @NamedQuery(name = "Descriptions.findByIdDescriptions", query = "SELECT d FROM Descriptions d WHERE d.idDescriptions = :idDescriptions"),
    @NamedQuery(name = "Descriptions.findByInterest", query = "SELECT d FROM Descriptions d WHERE d.interest = :interest"),
    @NamedQuery(name = "Descriptions.findByLanguage", query = "SELECT d FROM Descriptions d WHERE d.language = :language")})
public class Descriptions implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idDescriptions")
    private Integer idDescriptions;
    @Size(max = 255)
    @Column(name = "interest")
    private String interest;
    @Size(max = 255)
    @Column(name = "language")
    private String language;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "text")
    private String text;
    @JoinColumn(name = "idPlace", referencedColumnName = "idPlaces")
    @ManyToOne
    private Places idPlace;
    @JoinColumn(name = "idRoute", referencedColumnName = "idRoutes")
    @ManyToOne
    private Routes idRoute;

    public Descriptions() {
    }

    public Descriptions(Integer idDescriptions) {
        this.idDescriptions = idDescriptions;
    }

    public Integer getIdDescriptions() {
        return idDescriptions;
    }

    public void setIdDescriptions(Integer idDescriptions) {
        this.idDescriptions = idDescriptions;
    }

    public String getInterest() {
        return interest;
    }

    public void setInterest(String interest) {
        this.interest = interest;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Places getIdPlace() {
        return idPlace;
    }

    public void setIdPlace(Places idPlace) {
        this.idPlace = idPlace;
    }

    public Routes getIdRoute() {
        return idRoute;
    }

    public void setIdRoute(Routes idRoute) {
        this.idRoute = idRoute;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDescriptions != null ? idDescriptions.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Descriptions)) {
            return false;
        }
        Descriptions other = (Descriptions) object;
        if ((this.idDescriptions == null && other.idDescriptions != null) || (this.idDescriptions != null && !this.idDescriptions.equals(other.idDescriptions))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.wanderWiki.entities.Descriptions[ idDescriptions=" + idDescriptions + " ]";
    }
    
}
