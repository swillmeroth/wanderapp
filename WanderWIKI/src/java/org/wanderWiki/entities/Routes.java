/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.wanderWiki.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Stephan
 */
@Entity
@Table(name = "routes")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Routes.findAll", query = "SELECT r FROM Routes r"),
    @NamedQuery(name = "Routes.findByIdRoutes", query = "SELECT r FROM Routes r WHERE r.idRoutes = :idRoutes"),
    @NamedQuery(name = "Routes.findByDifficulty", query = "SELECT r FROM Routes r WHERE r.difficulty = :difficulty"),
    @NamedQuery(name = "Routes.findByDistance", query = "SELECT r FROM Routes r WHERE r.distance = :distance"),
    @NamedQuery(name = "Routes.findByDuration", query = "SELECT r FROM Routes r WHERE r.duration = :duration"),
    @NamedQuery(name = "Routes.findByEnd", query = "SELECT r FROM Routes r WHERE r.end = :end"),
    @NamedQuery(name = "Routes.findByIsChange", query = "SELECT r FROM Routes r WHERE r.isChange = :isChange"),
    @NamedQuery(name = "Routes.findByIsProposed", query = "SELECT r FROM Routes r WHERE r.isProposed = :isProposed"),
    @NamedQuery(name = "Routes.findByName", query = "SELECT r FROM Routes r WHERE r.name = :name"),
    @NamedQuery(name = "Routes.findByStart", query = "SELECT r FROM Routes r WHERE r.start = :start"),
    @NamedQuery(name = "Routes.find_Startpunkt_nicht_proposal", query = "SELECT r FROM Routes r, Routepoints rp WHERE rp.num = 0 AND rp.idRoute = r AND r.isProposed = false"),
    @NamedQuery(name = "Routes.findByType", query = "SELECT r FROM Routes r WHERE r.type = :type")})
public class Routes implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idRoutes")
    private Integer idRoutes;
    @Column(name = "difficulty")
    private Integer difficulty;
    @Column(name = "distance")
    private Integer distance;
    @Column(name = "duration")
    private Integer duration;
    @Column(name = "end")
    private Integer end;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "equipment")
    private String equipment;
    @Column(name = "isChange")
    private Boolean isChange = false;
    @Column(name = "isProposed")
    private Boolean isProposed = true;
    @Size(max = 255)
    @Column(name = "name")
    private String name;
    @Column(name = "start")
    private Integer start;
    @Size(max = 255)
    @Column(name = "type")
    private String type;
    @OneToMany(mappedBy = "idChanging")
    private List<Routes> routesList;
    @JoinColumn(name = "idChanging", referencedColumnName = "idRoutes")
    @ManyToOne
    private Routes idChanging;
    @OneToMany(mappedBy = "idRoute")
    private List<Multimedia> multimediaList;
    @OneToMany(mappedBy = "idRoute")
    private List<Comments> commentsList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idRoute")
    private List<Placesroutes> placesroutesList;
    @OneToMany(mappedBy = "idRoute")
    private List<Todos> todosList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idRoute")
    private List<Routepoints> routepointsList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idRoute")
    private List<Descriptions> descriptionsList;

    public Routes() {
    }

    public Routes(Integer idRoutes) {
        this.idRoutes = idRoutes;
    }

    public Integer getIdRoutes() {
        return idRoutes;
    }

    public void setIdRoutes(Integer idRoutes) {
        this.idRoutes = idRoutes;
    }

    public Integer getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(Integer difficulty) {
        this.difficulty = difficulty;
    }

    public Integer getDistance() {
        return distance;
    }

    public void setDistance(Integer distance) {
        this.distance = distance;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Integer getEnd() {
        return end;
    }

    public void setEnd(Integer end) {
        this.end = end;
    }

    public String getEquipment() {
        return equipment;
    }

    public void setEquipment(String equipment) {
        this.equipment = equipment;
    }

    public Boolean getIsChange() {
        return isChange;
    }

    public void setIsChange(Boolean isChange) {
        this.isChange = isChange;
    }

    public Boolean getIsProposed() {
        return isProposed;
    }

    public void setIsProposed(Boolean isProposed) {
        this.isProposed = isProposed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @XmlTransient
    public List<Routes> getRoutesList() {
        return routesList;
    }

    public void setRoutesList(List<Routes> routesList) {
        this.routesList = routesList;
    }

    public Routes getIdChanging() {
        return idChanging;
    }

    public void setIdChanging(Routes idChanging) {
        this.idChanging = idChanging;
    }

    @XmlTransient
    public List<Multimedia> getMultimediaList() {
        return multimediaList;
    }

    public void setMultimediaList(List<Multimedia> multimediaList) {
        this.multimediaList = multimediaList;
    }

    @XmlTransient
    public List<Comments> getCommentsList() {
        return commentsList;
    }

    public void setCommentsList(List<Comments> commentsList) {
        this.commentsList = commentsList;
    }

    @XmlTransient
    public List<Placesroutes> getPlacesroutesList() {
        return placesroutesList;
    }

    public void setPlacesroutesList(List<Placesroutes> placesroutesList) {
        this.placesroutesList = placesroutesList;
    }

    @XmlTransient
    public List<Todos> getTodosList() {
        return todosList;
    }

    public void setTodosList(List<Todos> todosList) {
        this.todosList = todosList;
    }

    @XmlTransient
    public List<Routepoints> getRoutepointsList() {
        return routepointsList;
    }

    public void setRoutepointsList(List<Routepoints> routepointsList) {
        this.routepointsList = routepointsList;
    }

    @XmlTransient
    public List<Descriptions> getDescriptionsList() {
        return descriptionsList;
    }

    public void setDescriptionsList(List<Descriptions> descriptionsList) {
        this.descriptionsList = descriptionsList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRoutes != null ? idRoutes.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Routes)) {
            return false;
        }
        Routes other = (Routes) object;
        if ((this.idRoutes == null && other.idRoutes != null) || (this.idRoutes != null && !this.idRoutes.equals(other.idRoutes))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Routes[ id =" + idRoutes + " name =" + name + " ]";
    }

}
