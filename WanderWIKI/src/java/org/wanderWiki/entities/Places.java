/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.wanderWiki.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Stephan
 */
@Entity
@Table(name = "places")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Places.findAll", query = "SELECT p FROM Places p"),
    @NamedQuery(name = "Places.findByIdPlaces", query = "SELECT p FROM Places p WHERE p.idPlaces = :idPlaces"),
    @NamedQuery(name = "Places.findByAltitude", query = "SELECT p FROM Places p WHERE p.altitude = :altitude"),
    @NamedQuery(name = "Places.findByCity", query = "SELECT p FROM Places p WHERE p.city = :city"),
    @NamedQuery(name = "Places.findByDateAdded", query = "SELECT p FROM Places p WHERE p.dateAdded = :dateAdded"),
    @NamedQuery(name = "Places.findByIsChange", query = "SELECT p FROM Places p WHERE p.isChange = :isChange"),
    @NamedQuery(name = "Places.findByIsProposal", query = "SELECT p FROM Places p WHERE p.isProposal = :isProposal"),
    @NamedQuery(name = "Places.findByLatitude", query = "SELECT p FROM Places p WHERE p.latitude = :latitude"),
    @NamedQuery(name = "Places.findByLongitude", query = "SELECT p FROM Places p WHERE p.longitude = :longitude"),
    @NamedQuery(name = "Places.findByName", query = "SELECT p FROM Places p WHERE p.name = :name"),
    @NamedQuery(name = "Places.findByPostalCode", query = "SELECT p FROM Places p WHERE p.postalCode = :postalCode"),
    @NamedQuery(name = "Places.findBySize", query = "SELECT p FROM Places p WHERE p.size = :size"),
    @NamedQuery(name = "Places.findByStreet", query = "SELECT p FROM Places p WHERE p.street = :street"),
    @NamedQuery(name = "Places.findByType", query = "SELECT p FROM Places p WHERE p.type = :type"),
    @NamedQuery(name = "Places.findByRoute", query = "SELECT p FROM Placesroutes pr, Routes r, Places p WHERE pr.idRoute = r AND pr.idPlace = p AND p.isProposal = 0 AND r.idRoutes = :idRoute")
    })
public class Places implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idPlaces")
    private Integer idPlaces;
    @Column(name = "altitude")
    private Integer altitude;
    @Size(max = 255)
    @Column(name = "city")
    private String city;
    @Column(name = "date_added")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateAdded;
    @Column(name = "isChange")
    private Boolean isChange = false;;
    @Column(name = "isProposal")
    private Boolean isProposal = true;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "latitude")
    private Double latitude;
    @Column(name = "longitude")
    private Double longitude;
    @Size(max = 255)
    @Column(name = "name")
    private String name;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "open_times")
    private String openTimes;
    @Size(max = 255)
    @Column(name = "postal_code")
    private String postalCode;
    @Column(name = "size")
    private Integer size = 1;
    @Size(max = 255)
    @Column(name = "street")
    private String street;
    @Size(max = 255)
    @Column(name = "type")
    private String type;
    @OneToMany(mappedBy = "idPlace")
    private List<Multimedia> multimediaList;
    @OneToMany(mappedBy = "idChanging")
    private List<Places> placesList;
    @JoinColumn(name = "idChanging", referencedColumnName = "idPlaces")
    @ManyToOne
    private Places idChanging;
    @OneToMany(mappedBy = "idPlace")
    private List<Comments> commentsList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idPlace")
    private List<Questions> questionsList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idPlace")
    private List<Placesroutes> placesroutesList;
    @OneToMany(mappedBy = "idPlace")
    private List<Todos> todosList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idPlace")
    private List<Descriptions> descriptionsList;

    public Places() {
    }

    public Places(Integer idPlaces) {
        this.idPlaces = idPlaces;
    }

    public Integer getIdPlaces() {
        return idPlaces;
    }

    public void setIdPlaces(Integer idPlaces) {
        this.idPlaces = idPlaces;
    }

    public Integer getAltitude() {
        return altitude;
    }

    public void setAltitude(Integer altitude) {
        this.altitude = altitude;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Date getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(Date dateAdded) {
        this.dateAdded = dateAdded;
    }

    public Boolean getIsChange() {
        return isChange;
    }

    public void setIsChange(Boolean isChange) {
        this.isChange = isChange;
    }

    public Boolean getIsProposal() {
        return isProposal;
    }

    public void setIsProposal(Boolean isProposal) {
        this.isProposal = isProposal;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOpenTimes() {
        return openTimes;
    }

    public void setOpenTimes(String openTimes) {
        this.openTimes = openTimes;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @XmlTransient
    public List<Multimedia> getMultimediaList() {
        return multimediaList;
    }

    public void setMultimediaList(List<Multimedia> multimediaList) {
        this.multimediaList = multimediaList;
    }

    @XmlTransient
    public List<Places> getPlacesList() {
        return placesList;
    }

    public void setPlacesList(List<Places> placesList) {
        this.placesList = placesList;
    }

    public Places getIdChanging() {
        return idChanging;
    }

    public void setIdChanging(Places idChanging) {
        this.idChanging = idChanging;
    }

    @XmlTransient
    public List<Comments> getCommentsList() {
        return commentsList;
    }

    public void setCommentsList(List<Comments> commentsList) {
        this.commentsList = commentsList;
    }

    @XmlTransient
    public List<Questions> getQuestionsList() {
        return questionsList;
    }

    public void setQuestionsList(List<Questions> questionsList) {
        this.questionsList = questionsList;
    }

    @XmlTransient
    public List<Placesroutes> getPlacesroutesList() {
        return placesroutesList;
    }

    public void setPlacesroutesList(List<Placesroutes> placesroutesList) {
        this.placesroutesList = placesroutesList;
    }

    @XmlTransient
    public List<Todos> getTodosList() {
        return todosList;
    }

    public void setTodosList(List<Todos> todosList) {
        this.todosList = todosList;
    }

    @XmlTransient
    public List<Descriptions> getDescriptionsList() {
        return descriptionsList;
    }

    public void setDescriptionsList(List<Descriptions> descriptionsList) {
        this.descriptionsList = descriptionsList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPlaces != null ? idPlaces.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Places)) {
            return false;
        }
        Places other = (Places) object;
        if ((this.idPlaces == null && other.idPlaces != null) || (this.idPlaces != null && !this.idPlaces.equals(other.idPlaces))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Places[ id=" + idPlaces + " name=" + name + " ]";
    }
    
}
