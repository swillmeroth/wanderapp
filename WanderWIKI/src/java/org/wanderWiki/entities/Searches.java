/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.wanderWiki.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Stephan
 */
@Entity
@Table(name = "searches")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Searches.findAll", query = "SELECT s FROM Searches s"),
    @NamedQuery(name = "Searches.findByIdSearches", query = "SELECT s FROM Searches s WHERE s.idSearches = :idSearches")})
public class Searches implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idSearches")
    private Integer idSearches;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "query")
    private String query;
    @JoinColumn(name = "idUser", referencedColumnName = "userid")
    @ManyToOne
    private Users idUser;

    public Searches() {
    }

    public Searches(Integer idSearches) {
        this.idSearches = idSearches;
    }

    public Integer getIdSearches() {
        return idSearches;
    }

    public void setIdSearches(Integer idSearches) {
        this.idSearches = idSearches;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public Users getIdUser() {
        return idUser;
    }

    public void setIdUser(Users idUser) {
        this.idUser = idUser;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSearches != null ? idSearches.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Searches)) {
            return false;
        }
        Searches other = (Searches) object;
        if ((this.idSearches == null && other.idSearches != null) || (this.idSearches != null && !this.idSearches.equals(other.idSearches))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.wanderWiki.entities.Searches[ idSearches=" + idSearches + " ]";
    }
    
}
