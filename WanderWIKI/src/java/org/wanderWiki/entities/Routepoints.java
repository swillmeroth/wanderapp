/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.wanderWiki.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Stephan
 */
@Entity
@Table(name = "routepoints")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Routepoints.findAll", query = "SELECT r FROM Routepoints r"),
    @NamedQuery(name = "Routepoints.findByIdRoutePoints", query = "SELECT r FROM Routepoints r WHERE r.idRoutePoints = :idRoutePoints"),
    @NamedQuery(name = "Routepoints.findByLatitude", query = "SELECT r FROM Routepoints r WHERE r.latitude = :latitude"),
    @NamedQuery(name = "Routepoints.findByLongitude", query = "SELECT r FROM Routepoints r WHERE r.longitude = :longitude"),
    @NamedQuery(name = "Routepoints.find_Startpunkt_nicht_proposal", query = "SELECT rp FROM Routes r, Routepoints rp WHERE rp.num = 0 AND r = rp.idRoute  AND r.isProposed = false"),
    @NamedQuery(name = "Routepoints.findByNum", query = "SELECT r FROM Routepoints r WHERE r.num = :num")})
public class Routepoints implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idRoutePoints")
    private Integer idRoutePoints;
    @Column(name = "latitude")
    private Double latitude;
    @Column(name = "longitude")
    private Double longitude;
    @Column(name = "num")
    private Integer num;
    @JoinColumn(name = "idRoute", referencedColumnName = "idRoutes")
    @ManyToOne(optional = false)
    private Routes idRoute;

    public Routepoints() {
    }

    public Routepoints(Integer idRoutePoints) {
        this.idRoutePoints = idRoutePoints;
    }

    public Integer getIdRoutePoints() {
        return idRoutePoints;
    }

    public void setIdRoutePoints(Integer idRoutePoints) {
        this.idRoutePoints = idRoutePoints;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Routes getIdRoute() {
        return idRoute;
    }

    public void setIdRoute(Routes idRoute) {
        this.idRoute = idRoute;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRoutePoints != null ? idRoutePoints.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Routepoints)) {
            return false;
        }
        Routepoints other = (Routepoints) object;
        if ((this.idRoutePoints == null && other.idRoutePoints != null) || (this.idRoutePoints != null && !this.idRoutePoints.equals(other.idRoutePoints))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.wanderWiki.entities.Routepoints[ idRoutePoints=" + idRoutePoints + " lat=" + latitude + " long=" + longitude + " ]";
    }

    }
