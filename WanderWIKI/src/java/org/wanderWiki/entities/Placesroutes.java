/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.wanderWiki.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Stephan
 */
@Entity
@Table(name = "placesroutes")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Placesroutes.findAll", query = "SELECT p FROM Placesroutes p"),
    @NamedQuery(name = "Placesroutes.findByIdPlacesRoutes", query = "SELECT p FROM Placesroutes p WHERE p.idPlacesRoutes = :idPlacesRoutes")})
public class Placesroutes implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idPlacesRoutes")
    private Integer idPlacesRoutes;
    @JoinColumn(name = "idPlace", referencedColumnName = "idPlaces")
    @ManyToOne
    private Places idPlace;
    @JoinColumn(name = "idRoute", referencedColumnName = "idRoutes")
    @ManyToOne
    private Routes idRoute;

    public Placesroutes() {
    }

    public Placesroutes(Integer idPlacesRoutes) {
        this.idPlacesRoutes = idPlacesRoutes;
    }

    public Integer getIdPlacesRoutes() {
        return idPlacesRoutes;
    }

    public void setIdPlacesRoutes(Integer idPlacesRoutes) {
        this.idPlacesRoutes = idPlacesRoutes;
    }

    public Places getIdPlace() {
        return idPlace;
    }

    public void setIdPlace(Places idPlace) {
        this.idPlace = idPlace;
    }

    public Routes getIdRoute() {
        return idRoute;
    }

    public void setIdRoute(Routes idRoute) {
        this.idRoute = idRoute;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPlacesRoutes != null ? idPlacesRoutes.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Placesroutes)) {
            return false;
        }
        Placesroutes other = (Placesroutes) object;
        if ((this.idPlacesRoutes == null && other.idPlacesRoutes != null) || (this.idPlacesRoutes != null && !this.idPlacesRoutes.equals(other.idPlacesRoutes))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.wanderWiki.entities.Placesroutes[ idPlacesRoutes=" + idPlacesRoutes + " ]";
    }
    
}
