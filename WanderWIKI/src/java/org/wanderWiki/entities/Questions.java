/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.wanderWiki.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Stephan
 */
@Entity
@Table(name = "questions")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Questions.findAll", query = "SELECT q FROM Questions q"),
    @NamedQuery(name = "Questions.findByIdquestions", query = "SELECT q FROM Questions q WHERE q.idquestions = :idquestions"),
    @NamedQuery(name = "Questions.findByRightAns", query = "SELECT q FROM Questions q WHERE q.rightAns = :rightAns"),
    @NamedQuery(name = "Questions.findByWrong1", query = "SELECT q FROM Questions q WHERE q.wrong1 = :wrong1"),
    @NamedQuery(name = "Questions.findByWrong2", query = "SELECT q FROM Questions q WHERE q.wrong2 = :wrong2"),
    @NamedQuery(name = "Questions.findByWrong3", query = "SELECT q FROM Questions q WHERE q.wrong3 = :wrong3")})
public class Questions implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idquestions")
    private Integer idquestions;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "question")
    private String question;
    @Size(max = 255)
    @Column(name = "right_ans")
    private String rightAns;
    @Size(max = 255)
    @Column(name = "wrong_1")
    private String wrong1;
    @Size(max = 255)
    @Column(name = "wrong_2")
    private String wrong2;
    @Size(max = 255)
    @Column(name = "wrong_3")
    private String wrong3;
    @JoinColumn(name = "idPlace", referencedColumnName = "idPlaces")
    @ManyToOne(optional = false)
    private Places idPlace;

    public Questions() {
    }

    public Questions(Integer idquestions) {
        this.idquestions = idquestions;
    }

    public Integer getIdquestions() {
        return idquestions;
    }

    public void setIdquestions(Integer idquestions) {
        this.idquestions = idquestions;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getRightAns() {
        return rightAns;
    }

    public void setRightAns(String rightAns) {
        this.rightAns = rightAns;
    }

    public String getWrong1() {
        return wrong1;
    }

    public void setWrong1(String wrong1) {
        this.wrong1 = wrong1;
    }

    public String getWrong2() {
        return wrong2;
    }

    public void setWrong2(String wrong2) {
        this.wrong2 = wrong2;
    }

    public String getWrong3() {
        return wrong3;
    }

    public void setWrong3(String wrong3) {
        this.wrong3 = wrong3;
    }

    public Places getIdPlace() {
        return idPlace;
    }

    public void setIdPlace(Places idPlace) {
        this.idPlace = idPlace;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idquestions != null ? idquestions.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Questions)) {
            return false;
        }
        Questions other = (Questions) object;
        if ((this.idquestions == null && other.idquestions != null) || (this.idquestions != null && !this.idquestions.equals(other.idquestions))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.wanderWiki.entities.Questions[ idquestions=" + idquestions + " ]";
    }
    
}
