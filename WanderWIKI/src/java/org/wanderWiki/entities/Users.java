/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.wanderWiki.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Stephan
 */
@Entity
@Table(name = "users")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Users.findAll", query = "SELECT u FROM Users u"),
    @NamedQuery(name = "Users.findByUserid", query = "SELECT u FROM Users u WHERE u.userid = :userid"),
    @NamedQuery(name = "Users.findByBigfont", query = "SELECT u FROM Users u WHERE u.bigfont = :bigfont"),
    @NamedQuery(name = "Users.findByEmail", query = "SELECT u FROM Users u WHERE u.email = :email"),
    @NamedQuery(name = "Users.findByInterest", query = "SELECT u FROM Users u WHERE u.interest = :interest"),
    @NamedQuery(name = "Users.findByLanguage", query = "SELECT u FROM Users u WHERE u.language = :language"),
    @NamedQuery(name = "Users.findByName", query = "SELECT u FROM Users u WHERE u.name = :name"),
    @NamedQuery(name = "Users.findByPasswordHash", query = "SELECT u FROM Users u WHERE u.passwordHash = :passwordHash"),
    @NamedQuery(name = "Users.findByVerificated", query = "SELECT u FROM Users u WHERE u.verificated = :verificated"),
    @NamedQuery(name = "Users.findByWalkingSpeed", query = "SELECT u FROM Users u WHERE u.walkingSpeed = :walkingSpeed"),
    @NamedQuery(name = "Users.findByWheelchair", query = "SELECT u FROM Users u WHERE u.wheelchair = :wheelchair"),
    @NamedQuery(name = "Users.findByUserlevel", query = "SELECT u FROM Users u WHERE u.userlevel = :userlevel")})
public class Users implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "userid")
    private Integer userid;
    @Column(name = "bigfont")
    private Boolean bigfont = false;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 255)
    @Column(name = "email")
    private String email;
    @Size(max = 255)
    @Column(name = "interest")
    private String interest;
    @Size(max = 255)
    @Column(name = "language")
    private String language;
    @Size(max = 255)
    @Column(name = "name")
    private String name;
    @Size(max = 255)
    @Column(name = "password_hash")
    private String passwordHash;
    @Column(name = "verificated")
    private Boolean verificated = false;
    @Size(max = 255)
    @Column(name = "walking_speed")
    private String walkingSpeed;
    @Column(name = "wheelchair")
    private Boolean wheelchair;

    //userlevel aufteilung:
    //0 - User
    //1 - moderator -noch nicht implementiert!
    //2 - admin
    @Basic(optional = false)
    @NotNull
    @Column(name = "userlevel")
    private int userlevel = 0;
    @OneToMany(mappedBy = "idUser")
    private List<Searches> searchesList;
    @OneToMany(mappedBy = "idUser")
    private List<Multimedia> multimediaList;
    @OneToMany(mappedBy = "idUser")
    private List<Comments> commentsList;
    @OneToMany(mappedBy = "idUser")
    private List<Todos> todosList;

    public Users() {
    }

    public Users(Integer userid) {
        this.userid = userid;
    }

    public Users(Integer userid, int userlevel) {
        this.userid = userid;
        this.userlevel = userlevel;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public Boolean getBigfont() {
        return bigfont;
    }

    public void setBigfont(Boolean bigfont) {
        this.bigfont = bigfont;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getInterest() {
        return interest;
    }

    public void setInterest(String interest) {
        this.interest = interest;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public Boolean getVerificated() {
        return verificated;
    }

    public void setVerificated(Boolean verificated) {
        this.verificated = verificated;
    }

    public String getWalkingSpeed() {
        return walkingSpeed;
    }

    public void setWalkingSpeed(String walkingSpeed) {
        this.walkingSpeed = walkingSpeed;
    }

    public Boolean getWheelchair() {
        return wheelchair;
    }

    public void setWheelchair(Boolean wheelchair) {
        this.wheelchair = wheelchair;
    }

    public int getUserlevel() {
        return userlevel;
    }

    public void setUserlevel(int userlevel) {
        this.userlevel = userlevel;
    }
    
    public boolean isAdmin() {
        return this.userlevel >= 2;
    }

    @XmlTransient
    public List<Searches> getSearchesList() {
        return searchesList;
    }

    public void setSearchesList(List<Searches> searchesList) {
        this.searchesList = searchesList;
    }

    @XmlTransient
    public List<Multimedia> getMultimediaList() {
        return multimediaList;
    }

    public void setMultimediaList(List<Multimedia> multimediaList) {
        this.multimediaList = multimediaList;
    }

    @XmlTransient
    public List<Comments> getCommentsList() {
        return commentsList;
    }

    public void setCommentsList(List<Comments> commentsList) {
        this.commentsList = commentsList;
    }

    @XmlTransient
    public List<Todos> getTodosList() {
        return todosList;
    }

    public void setTodosList(List<Todos> todosList) {
        this.todosList = todosList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userid != null ? userid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Users)) {
            return false;
        }
        Users other = (Users) object;
        if ((this.userid == null && other.userid != null) || (this.userid != null && !this.userid.equals(other.userid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.wanderWiki.entities.Users[ userid=" + userid + " ]";
    }
    
}
