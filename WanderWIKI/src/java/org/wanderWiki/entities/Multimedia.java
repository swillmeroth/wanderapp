/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.wanderWiki.entities;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Stephan
 */
@Entity
@Table(name = "multimedia")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Multimedia.findAll", query = "SELECT m FROM Multimedia m"),
    @NamedQuery(name = "Multimedia.findByIdMultimedia", query = "SELECT m FROM Multimedia m WHERE m.idMultimedia = :idMultimedia"),
    @NamedQuery(name = "Multimedia.findByHeading", query = "SELECT m FROM Multimedia m WHERE m.heading = :heading"),
    @NamedQuery(name = "Multimedia.findByIsVR", query = "SELECT m FROM Multimedia m WHERE m.isVR = :isVR"),
    @NamedQuery(name = "Multimedia.findByLatitude", query = "SELECT m FROM Multimedia m WHERE m.latitude = :latitude"),
    @NamedQuery(name = "Multimedia.findByLongitude", query = "SELECT m FROM Multimedia m WHERE m.longitude = :longitude"),
    @NamedQuery(name = "Multimedia.findByTime", query = "SELECT m FROM Multimedia m WHERE m.time = :time"),
    @NamedQuery(name = "Multimedia.findByType", query = "SELECT m FROM Multimedia m WHERE m.type = :type")})
public class Multimedia implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idMultimedia")
    private Integer idMultimedia;
    @Lob
    @Column(name = "file")
    private byte[] file;
    @Column(name = "heading")
    private BigInteger heading;
    @Column(name = "isVR")
    private Boolean isVR = false;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "latitude")
    private Double latitude;
    @Column(name = "longitude")
    private Double longitude;
    @Column(name = "time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date time;
    @Size(max = 255)
    @Column(name = "type")
    private String type;
    @JoinColumn(name = "idPlace", referencedColumnName = "idPlaces")
    @ManyToOne
    private Places idPlace;
    @JoinColumn(name = "idRoute", referencedColumnName = "idRoutes")
    @ManyToOne
    private Routes idRoute;
    @JoinColumn(name = "idUser", referencedColumnName = "userid")
    @ManyToOne
    private Users idUser;

    public Multimedia() {
    }

    public Multimedia(Integer idMultimedia) {
        this.idMultimedia = idMultimedia;
    }

    public Integer getIdMultimedia() {
        return idMultimedia;
    }

    public void setIdMultimedia(Integer idMultimedia) {
        this.idMultimedia = idMultimedia;
    }

    public byte[] getFile() {
        return file;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }

    public BigInteger getHeading() {
        return heading;
    }

    public void setHeading(BigInteger heading) {
        this.heading = heading;
    }

    public Boolean getIsVR() {
        return isVR;
    }

    public void setIsVR(Boolean isVR) {
        this.isVR = isVR;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Places getIdPlace() {
        return idPlace;
    }

    public void setIdPlace(Places idPlace) {
        this.idPlace = idPlace;
    }

    public Routes getIdRoute() {
        return idRoute;
    }

    public void setIdRoute(Routes idRoute) {
        this.idRoute = idRoute;
    }

    public Users getIdUser() {
        return idUser;
    }

    public void setIdUser(Users idUser) {
        this.idUser = idUser;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMultimedia != null ? idMultimedia.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Multimedia)) {
            return false;
        }
        Multimedia other = (Multimedia) object;
        if ((this.idMultimedia == null && other.idMultimedia != null) || (this.idMultimedia != null && !this.idMultimedia.equals(other.idMultimedia))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.wanderWiki.entities.Multimedia[ idMultimedia=" + idMultimedia + " ]";
    }
    
}
