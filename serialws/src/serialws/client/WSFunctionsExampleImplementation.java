package serialws.client;

import serialws.client.exceptions.BadResponseException;
import serialws.client.exceptions.SessionException;
import serialws.shared.FunctionIdentifierStoreExample;
import serialws.shared.RequestContainer;

/**
 *
 * @author MartinM
 */
public class WSFunctionsExampleImplementation extends AbstractWSFunctions {
    
    /**
     * An example: Add two Integers together.
     * 
     * @param a An integer.
     * @param b Another integer.
     * @return The sum of a and b.
     * @throws BadResponseException If the server had a problem processing the request.
     * @throws SessionException If the server associates a different session id with you than requested.
     */
    public Integer exampleAdd(Integer a, Integer b) throws BadResponseException, SessionException {
        
        RequestContainer request = new RequestContainer(FunctionIdentifierStoreExample.EXAMPLE_ADD, 2);
        request.putParameter(a);
        request.putParameter(b);
        
        return (Integer) queryBackend(request, Integer.class, false);
    }
    
    /**
     * An example: Add four Integers together.
     * 
     * @param a An integer.
     * @param b Another integer.
     * @param c Another integer.
     * @param d Another integer.
     * @return The sum of a, b, c and d.
     * @throws BadResponseException If the server had a problem processing the request.
     * @throws SessionException If the server associates a different session id with you than requested.
     */
    public Integer exampleAdd4(Integer a, Integer b, Integer c, Integer d) throws BadResponseException, SessionException {
        
        RequestContainer request = new RequestContainer(FunctionIdentifierStoreExample.EXAMPLE_ADD_4, 4);
        request.putParameter(a);
        request.putParameter(b);
        request.putParameter(c);
        request.putParameter(d);
        
        return (Integer) queryBackend(request, Integer.class, false);
    }
}
