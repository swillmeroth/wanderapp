package serialws.client;

import java.io.Serializable;
import serialws.client.exceptions.BadRequestException;
import serialws.client.exceptions.BadResponseException;
import serialws.client.exceptions.SessionException;
import serialws.shared.RequestContainer;
import serialws.shared.ResponseContainer;

/**
 * An abstract class with which implementing classes can be handed to a WSClient
 * object in its constructor, so that its package-private functionality for
 * server communication and response validation can be used.
 * 
 * RPC functions written in an implementation of this class should call
 * queryBackend() with an appropriate RequestContainer object and cast the
 * returned object to the expected type.
 * 
 * @author MartinM
 */
public abstract class AbstractWSFunctions {
    
    private WSClient client;
    
    /**
     * Sets the client implementations of this class should use to query
     * the backend. This method is package-private as it is only called
     * by WSClient.
     * 
     * @param client A usable WSClient.
     */
    void setClient(WSClient client) {
        this.client = client;
    }
    
    /**
     * Makes a request to the URI specified in the associated WSClient object
     * and returns the response object as a Serializable which can be safely
     * cast to the expected type. You may want to obtainNewSessionId() on the
     * WSClient before first calling this function.
     * 
     * @param request The RequestContainer formulating the request.
     * @param responseObjectClass The expected class of the response object.
     * @param isNullAllowed Whether the response object may be null.
     * @return The response object, or null if allowed.
     * @throws BadResponseException If the response container is null, its
     * response code is neither SUCCESS nor SESSION_ID, nor any known response
     * code.
     * @throws SessionException If the response code was of type SESSION_ID.
     * The SessionException thrown then contains a new session id that can be
     * used. It is not assigned to the WSClient automatically.
     */
    protected Serializable queryBackend(
            RequestContainer request, Class responseObjectClass, boolean isNullAllowed)
            throws BadResponseException, SessionException {
        return validateResponse(client.queryBackend(request), responseObjectClass, isNullAllowed).getResponseObject();
    }
    
    /**
     * Helper-function to throw appropriate exceptions for the given ResponseContainer,
     * or return normally if response.getResponseCode() returns ResponseCode.SUCCESS.
     * 
     * @param response The ResponseContainer to validate, or null.
     * @param responseObjectClass The class that the response object must match.
     * @param isNullAllowed Whether the response object may be a null.
     * @return The ResponseContainer itself.
     * @throws BadResponseException If response is null, its response code is neither
     * SUCCESS nor SESSION_ID, nor any known response code.
     * @throws SessionException If the response code was of type SESSION_ID.
     * The SessionException thrown then contains a new session id to be used.
     */
    private ResponseContainer validateResponse(ResponseContainer response, Class responseObjectClass, boolean isNullAllowed)
            throws BadResponseException, SessionException {
        if (response == null) {
            throw new BadResponseException(null, "The response container was null.");
        }
        ResponseContainer.ResponseCode code = response.getResponseCode();
        Object responseObject = response.getResponseObject();
        if (code == ResponseContainer.ResponseCode.SUCCESS) {
            if ((responseObject == null)) {
                if (!isNullAllowed) {
                    throw new BadResponseException(response, "Response object was null when it should not have been.");
                }
            } else if (responseObject.getClass() != responseObjectClass) {
                throw new BadResponseException(response, "Response object was of type "
                        + responseObject.getClass().getName() + " when " + responseObjectClass.getName() + " was expected.");
            }
        } else {
            String responseMessage = "";
            try {
                if (responseObject != null) {
                    responseMessage = (String) responseObject;
                }
            } catch (ClassCastException ex) {}
                
            if (code == ResponseContainer.ResponseCode.SESSION_ID)
                throw new SessionException(responseMessage);

            if ((code == ResponseContainer.ResponseCode.BAD_PARAMETER_COUNT)
                    || (code == ResponseContainer.ResponseCode.BAD_PARAMETER_DATA)
                    || (code == ResponseContainer.ResponseCode.BAD_PARAMETER_TYPE)
                    || (code == ResponseContainer.ResponseCode.BAD_SERIAL)
                    || (code == ResponseContainer.ResponseCode.INSUFFICIENT_PERMISSIONS)
                    || (code == ResponseContainer.ResponseCode.UNKNOWN_CLASS)
                    || (code == ResponseContainer.ResponseCode.UNKNOWN_ERROR)
                    || (code == ResponseContainer.ResponseCode.UNKNOWN_FUNCTION))
                throw new BadRequestException(response, responseMessage);

            if (code == ResponseContainer.ResponseCode.CONNECTION_FAILED)
                throw new BadResponseException(response, responseMessage);
            
            throw new BadResponseException(response, "The given response code is not known.");
        }
        return response;
    }
}
