package serialws.client;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;
import serialws.shared.RequestContainer;
import serialws.shared.ResponseContainer;
import org.apache.http.client.CookieStore;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.cookie.BasicClientCookie;
import serialws.client.exceptions.SerializationFailureException;
import serialws.shared.ResponseContainer.ResponseCode;

/**
 * A Serial Web Service client which is to be initialized with some implementation
 * of AbstractWSFunctions, as well as a URI to query for web service functionality.
 * 
 * @author MartinM
 * @param <T> An implementation of AbstractWSFunctions, which uses WSClient.queryBackend()
 * and WSClient.
 */
public class WSClient<T extends AbstractWSFunctions> {
    
    private final URI queryUri;
    private final T functions;
    private String sessionId;
    
    /**
     * Constructs the Client with a final URI where the web service can
     * be accessed.
     * 
     * @param queryUri URI of the web service.
     * @param functions An implementation of AbstractWSFunctions that users
     * of the WSClient can retrieve to call functions through.
     */
    public WSClient(URI queryUri, T functions) {
        this.queryUri = queryUri;
        functions.setClient(this);
        this.functions = functions;
        sessionId = null;
    }
    
    public T functions() {
        return functions;
    }
    
    /**
     * Retrieves the JSESSIONID currently being used by the WSClient.
     * 
     * @return The JSESSIONID currently being used.
     */
    public String getSessionId() {
        return sessionId;
    }
    
    /**
     * Sets the JSESSIONID to be used by the WSClient.
     * 
     * @param id The new JSESSIONID to be used by the WSClient
     */
    public void setSessionId(String id) {
        sessionId = id;
    }
    
    /**
     * Attempts to obtain a new session id from the server. If a new session
     * id is obtained, it replaces the current one. If the operation fails,
     * the current id is being kept.
     * 
     * @return True on success, false otherwise.
     */
    public boolean obtainNewSessionId() {
        // Request a new session id.
        ResponseContainer response = queryBackend(null);
        if (response.getResponseCode() == ResponseCode.SESSION_ID) {
            // Store it on success.
            sessionId = (String) response.getResponseObject();
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * Sends a HTTP GET request containing the RequestContainer as an encoded
     * argument. You may want to obtainNewSessionId() before first calling
     * this function.
     * 
     * @param request A valid RequestContainer formulating a request to call a
     * known function, or null if you want to retrieve a session id. Use the
     * obtainNewSessionId() function if you want to do the later.
     * @return A ResponseContainer containing a response code and a response
     * object. See ResponseContainer.ResponseCode for information on how to
     * handle the response object.
     * If the server fails to respond with a ResponseContainer, this method
     * will still return a ResponseContainer describing the problem.
     */
    ResponseContainer queryBackend(RequestContainer request) {
        
        URI wsUri = queryUri;
        
        if (request != null) {
            // Serialize request.
            byte[] binaryRequestObject = null;
            try {
                ByteArrayOutputStream byteOutStream = new ByteArrayOutputStream();
                ObjectOutputStream objectOutStream = new ObjectOutputStream(byteOutStream);
                objectOutStream.writeObject(request);
                binaryRequestObject = byteOutStream.toByteArray();
            } catch (IOException ex) {
                throw new SerializationFailureException(ex);
            }

            // Base64-encode serialized request for use in URLs.
            String requestBase64Encode = Base64.getUrlEncoder().encodeToString(binaryRequestObject);
            //System.out.println("Request size: " + requestBase64Encode.length() + " characters");

            // Build URI with HTTP GET argument.
            String uriString = queryUri + "?" + RequestContainer.QUERY_ARGUMENT_NAME + "=" + requestBase64Encode;
            try {
                wsUri = new URI(uriString);
            } catch (URISyntaxException ex) {
                // This should not happen as we confirmed URI validity in the constructor.
                Logger.getLogger(WSClient.class.getName()).log(Level.SEVERE, null, ex);
                return new ResponseContainer(ResponseCode.UNKNOWN_ERROR,
                        "Client encountered unrecoverable URISyntaxException: " + ex.getMessage());
            }
        }
        
        // Execute query to URL.
        RequestConfig globalConfig = RequestConfig.custom().setCookieSpec(CookieSpecs.BEST_MATCH).build();
        HttpClientContext context = HttpClientContext.create();
        
        // Create the cookie containing the session id.
        CookieStore cookieStore = new BasicCookieStore();
        BasicClientCookie cookie = new BasicClientCookie("JSESSIONID", sessionId);
        
        // Make sure the cookie scopes the correct domain and path.
        cookie.setDomain(wsUri.getHost());
        cookie.setPath(wsUri.getPath());
        
        // Set the cookie.
        cookieStore.addCookie(cookie);
        context.setCookieStore(cookieStore);
        
        CloseableHttpClient httpClient = null;
        CloseableHttpResponse httpResponse = null;
        
        try {
            // Create a HTTP client object and execute a HttpGet on it, with the context which has our cookie set.
            httpClient = HttpClients.custom()
                    .setDefaultRequestConfig(globalConfig).setDefaultCookieStore(cookieStore).build();
            HttpGet httpGet = new HttpGet(wsUri);
            
            try {
                httpResponse = httpClient.execute(httpGet, context);
            } catch (HttpHostConnectException ex) {
                return new ResponseContainer(ResponseCode.CONNECTION_FAILED, 
                        "Client encountered HttpHostConnectionException: " + ex.getMessage());
            }

            // Read bytes of base64-encode straight from the page.
            ByteArrayOutputStream responseBytes = new ByteArrayOutputStream();
            httpResponse.getEntity().writeTo(responseBytes);
            
            // Decode base64-encoded data into actual binary data.
            // No charset conversion here because it should be UTF-8 and base64 consists
            // of the most basic of readable characters.
            byte[] decodedResponse = Base64.getUrlDecoder().decode(responseBytes.toByteArray());
            
            // Deserialize bytes into ResponseContainer<T> and return it.
            ByteArrayInputStream byteInStream = new ByteArrayInputStream(decodedResponse);
            ObjectInputStream objectInStream = new ObjectInputStream(byteInStream);
            ResponseContainer response = (ResponseContainer) objectInStream.readObject();
            
            return response;
            
        } catch (IOException | ClassNotFoundException | IllegalArgumentException | ClassCastException ex) {
            Logger.getLogger(WSClient.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (httpClient != null) {
                    if (httpResponse != null) {
                        httpResponse.close();
                    }
                    httpClient.close();
                } else if (httpResponse != null) {
                    httpResponse.close();
                }
            } catch (IOException ex) {}
        }
        
        return null;
    }
}
