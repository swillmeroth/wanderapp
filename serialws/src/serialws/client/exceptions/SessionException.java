package serialws.client.exceptions;

/**
 *
 * @author MartinM
 */
public class SessionException extends SerialWSException {
    
    private String newSessionId;
    
    public SessionException() {
        newSessionId = null;
    }
    
    public SessionException(String newSessionId) {
        this.newSessionId = newSessionId;
    }
    
    public SessionException(String message, String newSessionId) {
        super(message);
        this.newSessionId = newSessionId;
    }
    
    public SessionException(Throwable cause, String newSessionId) {
        super(cause);
        this.newSessionId = newSessionId;
    }
    
    public SessionException(String message, Throwable cause, String newSessionId) {
        super(message, cause);
        this.newSessionId = newSessionId;
    }

    public String getNewSessionId() {
        return newSessionId;
    }

    public void setNewSessionId(String newSessionId) {
        this.newSessionId = newSessionId;
    }
    
}
