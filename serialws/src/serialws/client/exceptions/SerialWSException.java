package serialws.client.exceptions;

/**
 * Base class for all client-side checked exceptions of this class library.
 * 
 * @author MartinM
 */
public class SerialWSException extends Exception {
    
    public SerialWSException() {
        
    }
    
    public SerialWSException(String message) {
        super(message);
    }
    
    public SerialWSException(Throwable cause) {
        super(cause);
    }
    
    public SerialWSException(String message, Throwable cause) {
        super(message, cause);
    }
}
