package serialws.client.exceptions;

/**
 * Base class for all client-side unchecked exceptions of this class library.
 * 
 * @author MartinM
 */
public class SerialWSRuntimeException extends RuntimeException {
    
    public SerialWSRuntimeException() {
        
    }
    
    public SerialWSRuntimeException(String message) {
        super(message);
    }
    
    public SerialWSRuntimeException(Throwable cause) {
        super(cause);
    }
    
    public SerialWSRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }
}
