package serialws.client.exceptions;

import serialws.shared.ResponseContainer;

/**
 * An exception which is thrown when the server encountered some kind of error
 * with the request. You may call getResponseContainer() to further inspect
 * the response container which caused the exception.
 * 
 * @author MartinM
 */
public class BadResponseException extends SerialWSException {
    
    private ResponseContainer responseContainer;
    
    public BadResponseException(ResponseContainer responseContainer, String message) {
        super(message);
        this.responseContainer = responseContainer;
    }
    
    public BadResponseException(ResponseContainer responseContainer, Throwable cause) {
        super(cause);
        this.responseContainer = responseContainer;
    }
    
    public BadResponseException(ResponseContainer responseContainer, String message, Throwable cause) {
        super(message, cause);
        this.responseContainer = responseContainer;
    }

    /**
     * Returns the ResponseContainer associated with this Exception.
     * 
     * @return The ResponseContainer which caused this exception. Can be null.
     */
    public ResponseContainer getResponseContainer() {
        return responseContainer;
    }
    
    
}
