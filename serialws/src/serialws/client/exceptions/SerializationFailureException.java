package serialws.client.exceptions;

/**
 *
 * @author MartinM
 */
public class SerializationFailureException extends SerialWSRuntimeException {
    
    public SerializationFailureException(String message) {
        super(message);
    }
    
    public SerializationFailureException(Throwable cause) {
        super(cause);
    }
    
    public SerializationFailureException(String message, Throwable cause) {
        super(message, cause);
    }
}
