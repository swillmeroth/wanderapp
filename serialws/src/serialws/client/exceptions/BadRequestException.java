package serialws.client.exceptions;

import serialws.shared.ResponseContainer;

/**
 * Exception to be thrown if the server sent a response indicating that the
 * request, although well-formed, could not be executed.
 * 
 * @author MartinM
 */
public class BadRequestException extends SerialWSRuntimeException {
    
    private ResponseContainer responseContainer;
    
    public BadRequestException() {
        responseContainer = null;
    }
    
    public BadRequestException(ResponseContainer responseContainer, String message) {
        super(message);
        this.responseContainer = responseContainer;
    }
    
    public BadRequestException(ResponseContainer responseContainer, Throwable cause) {
        super(cause);
        this.responseContainer = responseContainer;
    }
    
    public BadRequestException(ResponseContainer responseContainer, String message, Throwable cause) {
        super(message, cause);
        this.responseContainer = responseContainer;
    }

    public ResponseContainer getResponseContainer() {
        return responseContainer;
    }

    public void setResponseContainer(ResponseContainer responseContainer) {
        this.responseContainer = responseContainer;
    }
    
}
