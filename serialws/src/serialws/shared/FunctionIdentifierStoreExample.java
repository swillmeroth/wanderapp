package serialws.shared;

/**
 * Class containing identifier values for the example implementation.
 * This method was chosen over making RequestContainer abstract because
 * of the added serialization data overhead for type information.
 * 
 * @author MartinM
 */
public final class FunctionIdentifierStoreExample {
    /**
     * Identifier for method:
     * Integer exampleAdd(Integer a, Integer b);
     * Returns the sum of a and b.
     */
    public static final int EXAMPLE_ADD = 1;
    
    /**
     * Identifier for method:
     * Integer exampleAdd4(Integer a, Integer b, Integer c, Integer d);
     * Returns the sum of a, b, c and d.
     */
    public static final int EXAMPLE_ADD_4 = 2;
}
