package serialws.shared;

import java.io.Serializable;

/**
 * Represents a response consisting of a ResponseCode and a response object
 * of type Object. Implementations of AbstractRequestProcessor use this class
 * to formulate a valid response to the client. Depending on the response
 * code, certain guarantees on the type of the response object should be given.
 * See the documentation of ResponseContainer.ResponseCode for more.
 * 
 * @author MartinM
 */
public final class ResponseContainer implements Serializable {

    /**
     * Enum consisting of various possible response codes, which may describe
     * a kind of error, or some kind of success. Depending on the ResponseCode,
     * the client should be able to make certain assumptions on the type of
     * the response object.
     */
    public static enum ResponseCode {
        /**
         * Indicates that either client or server encountered a critical error.
         */
        UNKNOWN_ERROR,
        
        /**
         * Indicates a function identifier that was requested is not known. The
         * response object should be a String further explaining the error.
         */
        UNKNOWN_FUNCTION,
        
        /**
         * Indicates you somehow sent a valid object which is not of class
         * RequestContainer. The response object should be a String further
         * explaining the error.
         */
        UNKNOWN_CLASS,
        
        /**
         * Indicates you sent something the server could either not decode or
         * deserialize. The response object should be a String further
         * explaining the error.
         */
        BAD_SERIAL,
        
        /**
         * Indicates you sent too many or too few parameters for executing the
         * requested function. The response object should be a String further
         * explaining the error.
         */
        BAD_PARAMETER_COUNT,
        
        /**
         * Indicates that one ore more parameters were of an incorrect type.
         * The response object should be a String further explaining the error.
         */
        BAD_PARAMETER_TYPE,
        
        /**
         * Indicates that one ore more parameters, despite being syntactically
         * well-formed, were semantically unusable. The response object should
         * be a String further explaining the error.
         */
        BAD_PARAMETER_DATA,
        
        /**
         * Indicates that although your request may have been valid, you do not
         * have the sufficient authorization to have the server execute it.
         * The response object should be a String further explaining the error.
         */
        INSUFFICIENT_PERMISSIONS,
        
        /**
         * Indicates that the session id you tried to use was not recognized by
         * the server. This means your original request was ignored entirely,
         * and you were sent a new session id instead. The response object
         * should be a String containing this new session id. Remember that
         * if you have any sort of authorization going on, you will now need
         * to do it again.
         */
        SESSION_ID,
        
        /**
         * Indicates that the remote procedure call executed successfully, and
         * the response object should be of the type expected to be returned
         * by the called function.
         */
        SUCCESS,
        
        /**
         * Indicates that the client could not connect to the server or the
         * connection timed out.
         */
        CONNECTION_FAILED
    };
    
    /**
     * The response code, which gives information on success and failure of
     * the request, as well as the type of the response object.
     */
    private ResponseCode responseCode;
    
    /**
     * The object which matches the RPC function return-value on success, or
     * another type depending on the response code.
     */
    private Serializable responseObject;
    
    /**
     * Constructs a new ResponseContainer with response code
     * ResponseCode.UNKNOWN_ERROR and response object set to null.
     */
    public ResponseContainer() {
        responseCode = ResponseCode.UNKNOWN_ERROR;
        responseObject = null;
    }
    
    /**
     * Constructs a new ResponseContainer with given response code and object.
     * 
     * @param responseCode The response code to be set.
     * @param responseObject The response object to be set.
     */
    public ResponseContainer(ResponseCode responseCode, Serializable responseObject) {
        this.responseCode = responseCode;
        this.responseObject = responseObject;
    }
    
    /**
     * Returns the response code of this ResponseContainer.
     * 
     * @return The response code as set by the originator of this
     * ResponseContainer.
     */
    public ResponseCode getResponseCode() {
        return responseCode;
    }
    
    /**
     * Sets the response code to given value.
     * 
     * @param code The response code to use.
     */
    public void setResponseCode(ResponseCode code) {
        responseCode = code;
    }
    
    /**
     * Returns the object which represents the RPC-response, or null on failure.
     * If the request was successful, i.e. getResponseCode() returns
     * ResponseCode.SUCCESS, the object returned here can be safely cast to the
     * type expected for the performed RPC.
     * 
     * @return Object of the expected class on success, or null if that is
     * what the function returned. For other response codes, see
     * ResponseCode documentation.
     */
    public Serializable getResponseObject() {
        return responseObject;
    }
    
    /**
     * Sets the response object to the specified object.
     * 
     * @param object A serializable object to respond with.
     */
    public void setResponseObject(Serializable object) {
        responseObject = object;
    }
}
