package serialws.shared;

import java.io.Serializable;

/**
 * Represents a request consisting of a function id and an array of parameters.
 * This class should be used by implementations of AbstractWSFunctions to
 * formulate a valid request to the server.
 * 
 * @author MartinM
 */
public final class RequestContainer implements Serializable {
    
    /**
     * The name of the HTTP GET argument to which the Client should write
     * a RequestContainer object, and where the server should try to read
     * it from.
     */
    public static final transient String QUERY_ARGUMENT_NAME = "q";
    
    /**
     * An integer uniquely identifying the function to be called. It is the
     * responsibility of users of this class library to ensure that client
     * and server have a matching understanding of functions ids and headers.
     */
    private int functionIdentifier;
    
    /**
     * The array of parameters, initialized with a fixed length by the
     * constructor.
     */
    private Serializable[] parameters;
    
    /**
     * Helper variable for putParameter(). Do not send.
     */
    private transient int nextParameterIndex;
    
    /**
     * Creates a RequestContainer representing a request for a call to the
     * function identified by rpcFunctionIdentifier having parameterCount
     * parameters. Unset parameters default to null.
     * 
     * @param rpcFunctionIdentifier The id of the known function.
     * @param parameterCount The amount of parameters required.
     */
    public RequestContainer(int rpcFunctionIdentifier, int parameterCount) {
        functionIdentifier = rpcFunctionIdentifier;
        parameters = new Serializable[parameterCount];
        for (int i = 0; i < parameterCount; i++) {
            parameters[i] = null;
        }
        nextParameterIndex = 0;
    }
    
    /**
     * Returns the function id specified in the constructor.
     * 
     * @return Working function id.
     */
    public int getFunction() {
        return functionIdentifier;
    }
    
    /**
     * Sets a parameter by index. The index should be &ge; 0 and less than the
     * count of parameters specified in the constructor-call.
     * 
     * @param index The index of the parameter to set.
     * @param parameterObject A serializable object to represent the parameter.
     */
    public void setParameter(int index, Serializable parameterObject) {
        if ((index < 0) || (index >= parameters.length)) {
            throw new ArrayIndexOutOfBoundsException("Tried to set parameter #" + index
                    + " when highest index is #" + (parameters.length - 1) + ".");
        }
        
        parameters[index] = parameterObject;
        
        if (index >= nextParameterIndex) {
            nextParameterIndex = index + 1;
        }
    }
    
    /**
     * Returns the parameter at the given index. The index should be &ge; 0 and
     * less than the count of parameters specified in the constructor-call.
     * 
     * @param index The index of the parameter to returns.
     * @return Parameter at given index as an object of class Object.
     */
    public Serializable getParameter(int index) {
        if ((index < 0) || (index >= parameters.length))
            return null;
        
        return parameters[index];
    }
    
    /**
     * Sets the first parameter of the RequestContainer. In the next call, the
     * second parameter is set, and so on. This requires that you have made no
     * previous calls to setParameter().
     * 
     * @param parameterObject A serializable object to represent the parameter.
     */
    public void putParameter(Serializable parameterObject) {
        if ((nextParameterIndex < 0) || (nextParameterIndex >= parameters.length)) {
            throw new ArrayIndexOutOfBoundsException("Tried to put parameter #" + nextParameterIndex
                    + " when highest index is #" + (parameters.length - 1) + ".");
        }
        
        parameters[nextParameterIndex] = parameterObject;
        nextParameterIndex++;
    }
    
    /**
     * Returns the amount of parameters this RequestContainer can hold.
     * 
     * @return The parameterCount given in the constructor.
     */
    public int getParameterCount() {
        return parameters.length;
    }
}
