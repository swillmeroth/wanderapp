package serialws.server;

import serialws.shared.RequestContainer;
import serialws.shared.ResponseContainer;

/**
 * The FunctionProcessorExample offers functions for use by the
 * RequestProcessorExampleImplementation. You may choose to not use
 * this paradigm when implementing your own RequestProcessor, but splitting
 * the concerns of function-selection and function-execution from each
 * other is advised.
 * 
 * This specific example shows two web service methods to add up either two
 * or four Integers and return the sum.
 * 
 * The FunctionProcessorExample may manage client state, such as user role,
 * through instance variables.
 * 
 * @author MartinM
 */
public class FunctionProcessorExample {
    
    /**
     * Adds two Integer parameters from the RequestContainer and writes the
     * result into the returned ResponseContainer's response object field.
     * 
     * @param request A request with two Integer-type parameters.
     * @return The sum of the two integers, contained within a ResponseContainer.
     */
    ResponseContainer exampleAdd(RequestContainer request) {
        ResponseContainer response = new ResponseContainer();
        
        if (request.getParameterCount() != 2) {
            response.setResponseCode(ResponseContainer.ResponseCode.BAD_PARAMETER_COUNT);
        } else {
        
            Object p1 = request.getParameter(0);
            Object p2 = request.getParameter(1);
            
            // Must be exactly the Integer class, not an instance of it! If it is a subclass,
            // we cannot predict its behaviour!
            if ((p1.getClass() != Integer.class) || (p2.getClass() != Integer.class)) {
                response.setResponseCode(ResponseContainer.ResponseCode.BAD_PARAMETER_TYPE);
            } else {
                Integer a = (Integer) p1;
                Integer b = (Integer) p2;
                Integer result = a + b;
                
                //System.out.println("Calculating for client: " + a + " + " + b + " = " + result);
                
                response.setResponseCode(ResponseContainer.ResponseCode.SUCCESS);
                response.setResponseObject(result);
            }
        }
        return response;
    }
    
    /**
     * Adds four Integer parameters from the RequestContainer and writes the
     * result into the returned ResponseContainer's response object field.
     * 
     * @param request A request with four Integer-type parameters.
     * @return The sum of the four integers, contained within a ResponseContainer.
     */
    ResponseContainer exampleAdd4(RequestContainer request) {
        ResponseContainer response = new ResponseContainer();
        
        if (request.getParameterCount() != 4) {
            response.setResponseCode(ResponseContainer.ResponseCode.BAD_PARAMETER_COUNT);
        } else {
        
            Object p1 = request.getParameter(0);
            Object p2 = request.getParameter(1);
            Object p3 = request.getParameter(2);
            Object p4 = request.getParameter(3);
            
            // Must be exactly the Integer class, not an instance of it! If it is a subclass,
            // we cannot predict its behaviour!
            if ((p1.getClass() != Integer.class) || (p2.getClass() != Integer.class)
                    || (p3.getClass() !=Integer.class) || (p4.getClass() != Integer.class)) {
                response.setResponseCode(ResponseContainer.ResponseCode.BAD_PARAMETER_TYPE);
            } else {
                Integer a = (Integer) p1;
                Integer b = (Integer) p2;
                Integer c = (Integer) p3;
                Integer d = (Integer) p4;
                Integer result = a + b + c + d;
                
                //System.out.println("Calculating for client: " + a + " + " + b + " + "
                //        + c + " + " + d + " = " + result);
                
                response.setResponseCode(ResponseContainer.ResponseCode.SUCCESS);
                response.setResponseObject(result);
            }
        }
        return response;
    }
    
}
