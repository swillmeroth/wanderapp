package serialws.server;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Base64;
import java.util.Map;
import java.util.Map.Entry;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import serialws.shared.RequestContainer;
import serialws.shared.ResponseContainer;

/**
 * The AbstractRequestProcessor is a box of web service functionality available to a
 * client. Implementing classes might want to be @SessionScoped @ManagedBeans.
 * 
 * @author MartinM
 */
public abstract class AbstractRequestProcessor {
    
    private FunctionProcessorExample functionProcessor;
    
    public AbstractRequestProcessor() {
        functionProcessor = new FunctionProcessorExample();
    }
    
    /**
     * This function maps the RequestContainer's requested function to an
     * actual function, executes it and responds with a ResponseContainer.
     * The mapped functions are responsible for type-checking the parameters
     * contained within the RequestContainer.
     * 
     * @param request The request to respond to.
     * @return The ResponseContainer to send to the client.
     * @throws IOException If respond() throws it.
     */
    public abstract ResponseContainer serveRequest(RequestContainer request) throws IOException;
    
    /**
     * This function gets called by the web service facelet when a client sends
     * a request to it, deserializes a base64-encoded Java object from the HTTP
     * GET argument list and, if a valid RequestContainer object is obtained,
     * calls the serverRequest function with the RequestContainer as the function
     * parameter. If no valid RequestObject is obtained, an error ResponseObject<>
     * is written to the page as a base64-encoded Java object serial.
     * 
     * @throws IOException If FacesContext.getCurrentInstance()
     * .getExternalContext().getResponseOutputWriter().write(),
     * serveRequest() or respond() throw it, indicating a problem that JSF
     * must handle.
     */
    public void processRequest() throws IOException {
        // Prepare context for response
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExternalContext externalContext = facesContext.getExternalContext();
        externalContext.setResponseContentType("application/octet-stream");
        externalContext.setResponseCharacterEncoding("UTF-8");
        
        // Verify that client sent a valid JSESSIONID.
        String proposedId = getCookieSessionId(externalContext);
        String assignedId = getAssignedSessionId(externalContext);
        
        if ((assignedId == null) || (!assignedId.equalsIgnoreCase(proposedId))) {
            //System.out.println("Ids differ! Proposed: " + proposedId + "; Assigned: " + assignedId);
            respond(new ResponseContainer(
                    ResponseContainer.ResponseCode.SESSION_ID, assignedId));
        } else {
            // This map represents the HTTP GET argument list.
            Map<String, String[]> argumentMap = externalContext.getRequestParameterValuesMap();

            // Parameter "q" (ideally) contains serialized, base64-encoded RequestContainer object.
            String[] valueArray = argumentMap.get(RequestContainer.QUERY_ARGUMENT_NAME);

            // No RequestContainer available? The user probably queries for a session id.
            boolean doProcessRequest = true;
            if (valueArray == null || valueArray.length == 0)
                doProcessRequest = false;

            if (doProcessRequest) {
                // Obtain base64-encoded, serialized Java object.
                String requestBase64Encode = valueArray[0];
                //System.out.println("BASE64 Request: " + requestBase64Encode);

                RequestContainer request = null;
                try {
                    // Decode the object into the request variable.
                    byte[] decodedResponse = Base64.getUrlDecoder().decode(requestBase64Encode);
                    ByteArrayInputStream byteInStream = new ByteArrayInputStream(decodedResponse);
                    ObjectInputStream objectInStream = new ObjectInputStream(byteInStream);
                    request = (RequestContainer) objectInStream.readObject();
                } catch (IOException | ClassNotFoundException | IllegalArgumentException | ClassCastException ex) {
                    // There is a lot that can go wrong here, and a lot of unreported Exceptions may be thrown,
                    // which we however want to catch.
                    respond(new ResponseContainer(
                            ResponseContainer.ResponseCode.BAD_SERIAL, "Failed to deserialize you request: "
                                    + ex.getMessage()));
                }

                if (request != null) {
                    // Do not allow unknown subtypes.
                    if (request.getClass() != RequestContainer.class) {
                        respond(new ResponseContainer(
                                ResponseContainer.ResponseCode.UNKNOWN_CLASS, "You sent a subtype of ResponseContainer, "
                                        + "which is not allowed."));
                    } else {
                        // If the RequestContainer is safe to use, find the desired function and get working.
                        respond(serveRequest(request));
                    }
                }
            } else {
                respond(new ResponseContainer(
                        ResponseContainer.ResponseCode.SESSION_ID, assignedId));
            }
        }
        
        // Clean up!
        facesContext.responseComplete();
    }
    
    /**
     * Writes ResponseContainer to the document as a base64-encoded
     * serialized object.
     * 
     * @param response The ResponseContainer to communicate.
     * @throws IOException If writing to the page failed. JSF should handle this.
     */
    protected void respond(ResponseContainer response) throws IOException {
        
        // Serialize response
        ByteArrayOutputStream byteOutStream = new ByteArrayOutputStream();
        ObjectOutputStream objectOutStream = new ObjectOutputStream(byteOutStream);
        objectOutStream.writeObject(response);
        byte[] binaryResponseObject = byteOutStream.toByteArray();

        // Base64-encode serialized response
        String responseBase64Encode = Base64.getUrlEncoder().encodeToString(binaryResponseObject);

        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExternalContext externalContext = facesContext.getExternalContext();
        externalContext.getResponseOutputWriter().write(responseBase64Encode);
    }
    
    /**
     * Returns the JSESSIONID assigned to the current client by the server.
     * 
     * @param externalContext
     * @return Client's current session id
     */
    protected static String getAssignedSessionId(ExternalContext externalContext) {
        // Session SHOULD already have been created, so provide false as argument.
        // If it has not been created yet, that means there is a bug somewhere else down the pipe.
        HttpSession session = (HttpSession) externalContext.getSession(false);
        return session != null ? session.getId() : null;
    }
    
    /**
     * Returns the JSESSIONID as proposed by the querying client.
     * 
     * @param externalContext
     * @return The JSESSIONID the client wants us to use.
     */
    protected static String getCookieSessionId(ExternalContext externalContext) {
        Cookie[] cookies = ((HttpServletRequest) externalContext.getRequest()).getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equalsIgnoreCase("JSESSIONID")) {
                    return cookie.getValue();
                }
            }
        }
        // Nothing found? Maybe the server is using HTTP GET session id. Look for it!
        Map<String, String[]> argumentMap = externalContext.getRequestParameterValuesMap();
        for (Entry<String, String[]> entry : argumentMap.entrySet()) {
            String key = entry.getKey();
            if ((key != null) && (key.equalsIgnoreCase("JSESSIONID"))) {
                String[] strings = entry.getValue();
                if ((strings != null) && (strings.length >= 1)) {
                    return strings[0];
                }
            }
        }
        return null;
    }
}
