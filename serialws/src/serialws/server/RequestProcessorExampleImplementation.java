package serialws.server;

import java.io.IOException;
import serialws.shared.FunctionIdentifierStoreExample;
import serialws.shared.RequestContainer;
import serialws.shared.ResponseContainer;

/**
 *
 * @author MartinM
 */
public class RequestProcessorExampleImplementation extends AbstractRequestProcessor {
    
    private FunctionProcessorExample functionProcessor;
    
    public RequestProcessorExampleImplementation() {
        functionProcessor = new FunctionProcessorExample();
    }
    
    /**
     * This function maps the RequestContainer's requested function to an
     * actual function, executes it and responds with a ResponseContainer.
     * 
     * @param request The request to respond to.
     * @return 
     * @throws IOException If respond() throws it.
     */
    @Override
    public ResponseContainer serveRequest(RequestContainer request) throws IOException {
        
        // The functions called here are responsible for type-checking the
        // parameters contained withing the RequestContainer!
        switch (request.getFunction()) {
            case FunctionIdentifierStoreExample.EXAMPLE_ADD:
                return functionProcessor.exampleAdd(request);
            case FunctionIdentifierStoreExample.EXAMPLE_ADD_4:
                return functionProcessor.exampleAdd4(request);
            default: {
                // This should never happen.
                return new ResponseContainer(
                        ResponseContainer.ResponseCode.UNKNOWN_FUNCTION, "The requested function "
                                + "id could not be matched to any known function.");
            }
        }
    }
}
